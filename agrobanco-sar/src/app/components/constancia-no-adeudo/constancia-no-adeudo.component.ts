import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import { RequerimientoService } from '../../services/requerimiento.service';
import { ConstanciaNoAdeudoService } from '../../services/constancia-no-adeudo.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Constantes } from 'src/app/comun/constantes';
import { Router } from '@angular/router';
import { SecurityService } from 'src/app/services/security.service';
import { DialogService } from 'src/app/services/comun/dialog.service';

@Component({
  selector: 'app-constancia-no-adeudo',
  templateUrl: './constancia-no-adeudo.component.html',
  styleUrls: ['./constancia-no-adeudo.component.css']
})
export class ConstanciaNoAdeudoComponent implements OnInit {

  formSubmitted:boolean = false;

  form!: FormGroup;
  agencias!:any[];
  parametros!:any[];

  displayedColumns: string[] = ['CodCred', 'Estado', 'Saldo'];
  requerimientoColumns: string[] = ['Requerimiento', 'FechaRegistro', 'NroDocumento', 'Nombres','Estado','Accion'];

  creditosClientes!:any[];
  requerimientos!:any[];

  loading!:boolean;

  nombresCompletos!:string;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private requerimientoServices : RequerimientoService,
    private constanciaNoAdeudoService: ConstanciaNoAdeudoService,
    private securityService : SecurityService,
    private dialogService: DialogService
  ) { }

  ngOnInit(): void {
    this.initForm();
    this.loadPage();

  }


  loadPage(): void {
    this.loading = true;

    forkJoin([
      this.requerimientoServices.ObtenerParametro()
    ]).subscribe( (data: any) => {
      debugger;
        console.log(data);
        this.parametros = data[0].response;
        this.loading = false;

        this.form.controls['TipoDocumento'].setValue("1");
    }, (err) => {
      this.loading = false;
      console.error(err);
    });

  }

  initForm() : void {
    this.form = this.formBuilder.group({
      TipoDocumento: [null, Validators.required],
      NroDocumento: [null, Validators.required],
      nombresCompletos: [{value: null, disabled: true}]
    });
  }

  buscar():void {
    this.formSubmitted = true;
    if (this.form.invalid) {
      return;
    }

    const TipoDocumento = +this.form.controls['TipoDocumento'].value;
    const NroDocumento = this.form.controls['NroDocumento'].value;
    var TipoIDN = '';
    var Documentos = this.parametros.filter(x=>x.grupo==300);

    if(Documentos!=null){
      if(Documentos.length>0){
        for (let index = 0; index < Documentos.length; index++) {
          const element = Documentos[index];
          if(element.parametro==TipoDocumento){
            TipoIDN = element.descripcion;
          }
        }
      }
    }


    var oBE = {
      TipoDocumento:TipoDocumento,
      Identificacion:NroDocumento,
      TipoIDN:TipoIDN
    };
    this.constanciaNoAdeudoService.ObtenerConstanciaNoAdeudo(oBE).subscribe( (result) => {
      console.log(result);

      this.form.controls["nombresCompletos"].setValue(result.response.nombresCompletos);
      //this.nombresCompletos = result.response.nombresCompletos;7
      this.creditosClientes = result.response.creditosClienteBE;
      this.requerimientos = result.response.requerimientoBE;


    }, (err) => {

      console.error(err);
    });

  }

  onChangeTipoDocumento():void{

    const TipoDocumento = +this.form.controls['TipoDocumento'].value;

    this.creditosClientes = [];
    this.requerimientos = [];

  }
  revisar(element:any){
    let usuario = this.securityService.leerUsuarioWeb();
    let parametro = {
      nroReclamo : element.nroReclamo
    };

    this.requerimientoServices.ObtenerBloqueado(parametro).subscribe((result:any) =>{

    if (result.response[0].estaBloqueado == 1 && result.response[0].usuarioBloqueo != usuario)
    {

      this.openModalError(element.nroReclamo,result.response[0].nombresApellidos);
    }
    else{

      const params ={
        NroReclamo : element.nroReclamo,
        UsuarioRegistro : usuario,
        EstaBloqueado : Constantes.EstaBloqueado

        };

          this.requerimientoServices.ActualizarRequerimientoBloqueado(params).subscribe((result:any) =>{
            console.log("response",result.response);
          });

          this.router.navigate(['/gestion/requerimiento-detalle', element.nroReclamo]);

       }

    });

  }


  openModalError(nroReclamo: string,nombresApellidos:string):void
  {
    if (nroReclamo != "") {

      this.dialogService.openMsgErrorDialog('Requerimiento seleccionado esta siendo revisado por el usuario : ' + nombresApellidos)
      .afterClosed().subscribe(res => {

      });

    }

  }
}
