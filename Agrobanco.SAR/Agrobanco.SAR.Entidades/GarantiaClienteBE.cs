﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades
{
    public class GarantiaClienteBE
    {
        public string Descripcion { get; set; }
        public string NombreGarante { get; set; }
        public string DNI { get; set; }
        public string MontoGravamen { get; set; }
        public string NroPartida { get; set; }
        public string OficinaRegistral { get; set; }
        public string NroPrestamo { get; set; }

    }
}
