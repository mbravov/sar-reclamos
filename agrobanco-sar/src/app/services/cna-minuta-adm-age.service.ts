import { Injectable } from '@angular/core';
import { AppService } from '../app.service';
import { SecurityService } from './security.service';

@Injectable({
  providedIn: 'root'
})
export class CnaMinutaAdmAgeService extends AppService {

  constructor(_securityService : SecurityService) {
    super(_securityService);
  }


}
