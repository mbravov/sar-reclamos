import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import jsPDF from 'jspdf';
import { Constantes } from 'src/app/comun/constantes';
import { DialogService } from 'src/app/services/comun/dialog.service';
import { RequerimientoService } from 'src/app/services/requerimiento.service';
import { TerminosCondicionesComponent } from '../terminos-condiciones/terminos-condiciones.component';
import { forkJoin } from 'rxjs';
import { environment } from 'src/environments/environment';
import * as EmailValidator from 'email-validator';
import { AppService } from 'src/app/app.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-requerimiento',
  templateUrl: './requerimiento.component.html',
  styleUrls: ['./requerimiento.component.css']
})
export class RequerimientoComponent implements OnInit {
  @ViewChild('myInputArchivo1', { static: false }) myInputArchivo1!: ElementRef;
  @ViewChild('myInputArchivo2', { static: false }) myInputArchivo2!: ElementRef;
  @ViewChild('myInputArchivo3', { static: false }) myInputArchivo3!: ElementRef;
  @ViewChild('myInputArchivo4', { static: false }) myInputArchivo4!: ElementRef;
  @ViewChild('myInputTextArchivo1', { static: false }) myInputTextArchivo1!: ElementRef;
  @ViewChild('myInputTextArchivo2', { static: false }) myInputTextArchivo2!: ElementRef;
  @ViewChild('myInputTextArchivo3', { static: false }) myInputTextArchivo3!: ElementRef;
  @ViewChild('myInputTextArchivo4', { static: false }) myInputTextArchivo4!: ElementRef;

  form!: FormGroup;
  parametros: any[] = [];
  agencias: any[] = [];
  departamentos: any[] = [];
  provincias: any[] = [];
  distritos: any[] = [];

  motivos: any[] = [];

  recursos!: any[];
  LaserFicheID!: string;

  checkedTerminos: boolean = false;
  esRuc: boolean = false;

  lengthDescripcion: number = 1500;
  MaxlengthDireccion: number = 200;
  MinlengthDireccion: number = 10;
  MaxlengthReferencia: number = 200;
  MinlengthReferencia: number = 10;

  MaxLengthRazonSocial: number = 200;
  MaxLengthNombres: number = 100;
  MaxLengthPrimerApellido: number = 100;
  MaxLengthSegundoApellido: number = 100;

  MaxlengthCorreo: number = 100;
  MaxlengthTelefono: number = 20;
  MaxlengthCelular: number = 9;


  Archivo1: any;
  Archivo2: any;
  Archivo3: any;
  Archivo4: any;

  NombreArchivo1: any;
  NombreArchivo2: any;
  NombreArchivo3: any;
  NombreArchivo4: any;

  MAXIMO_TAMANIO_BYTES = 1024000; // 1MB = 1 millón de bytes

  formSubmitted: boolean = false;

  EsMenorEdad: boolean = false;
  siteKey!: string;
  recaptcha!: string;

  bRegistroRequerimiento: boolean = false;
  NroRequerimiento!: string;
  number_whatsapp!: string;

  reenviar_correo!: string;

  base64TrimmedURL!: string;

  Archivo: any;
  NombreArchivo: any;
  loading: boolean = false;

  urlPreguntasFrecuentes: string;

  disabled: boolean = false;

  MaxLengthDocumento: number = 0;
  patternDocumentos: string = '';

  bActiveOficina: boolean = false;

  bMostrarArchvo1: boolean = false;
  bMostrarArchvo2: boolean = false;
  bMostrarArchvo3: boolean = false;
  bMostrarArchvo4: boolean = false;

  constructor(
    private dialogService: DialogService,
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private requerimientoServices: RequerimientoService,
    private appService: AppService,
    private router: Router
  ) {
    this.siteKey = environment.recaptcha_siteKey;
    this.number_whatsapp = environment.number_whatsapp;
    this.urlPreguntasFrecuentes = environment.preguntas_frecuentes;

    console.log("this.siteKey", this.siteKey);
  }

  ngOnInit(): void {
    this.initForm();

    this.loadPage();
    // this.downloadPDF();

    this.bRegistroRequerimiento = false;
    // this.getImage("https://www.agrobanco.com.pe/wp-content/uploads/2017/07/logo-ab.png");

    //this.getBase64ImageFromUrl()
    this.getBase64ImageFromUrl('assets/img/logo-ab.png')
      .then((result: any) =>

        this.base64TrimmedURL = result
      )
      .catch(err => console.error(err));

  }

  onChangeAgencia(): void {
    //const Agencia = this.form.controls['Agencia'].value;
    // this.form.controls['AgenciaDireccion'].setValue(Agencia.direccion);
    //this.form.controls['CodAgencia'].setValue(Agencia.codAgencia);
    debugger;
    const CodAgencia = this.form.controls['CodAgencia'].value;

    const Agencia = this.agencias.filter(x => x.codAgencia == CodAgencia);
    if (Agencia != null) {
      this.form.controls['AgenciaDireccion'].setValue(Agencia[0].direccion);
      this.bActiveOficina = true;
    }
    else {
      this.bActiveOficina = false;
    }

  }
  onChangeDepartamento(): void {
    const objDepartamento = this.form.controls['CodDepartamento'].value;
    if (objDepartamento != null) {
      if (objDepartamento != "") {
        this.appService.generateTokenReclamacionesAPI().subscribe((data: any) => {
          let token = data.headers.get('authorization');
          this.requerimientoServices.listarProvincia(objDepartamento.codDepartamento, token).subscribe((result: any) => {
            console.log(result);
            this.provincias = result.response;
            this.distritos = []
          }, (err) => {

            console.error(err);
          });
        }, (err) => {

          console.error(err);
        });

      }
      else {
        this.provincias = [];
        this.distritos = []
      }
    }
    else {
      this.provincias = [];
      this.distritos = []
    }

  }
  onChangeProvincia(): void {
    const objDepartamento = this.form.controls['CodDepartamento'].value;
    const objProvincia = this.form.controls['CodProvincia'].value;

    if (objProvincia != "") {
      this.appService.generateTokenReclamacionesAPI().subscribe((data: any) => {
        let token = data.headers.get('authorization');
        this.requerimientoServices.listarDistrito(objDepartamento.codDepartamento, objProvincia.codProvincia, token).subscribe((result: any) => {
          console.log("Distritos", result.response);
          this.distritos = result.response;
        });
      }, (err) => {

        console.error(err);
      });

    }
    else {
      this.distritos = []
    }

  }
  onChangeProducto(): void {
    const objProducto = this.form.controls['CodProducto'].value;
    console.log("objProducto", objProducto);
    this.motivos = this.parametros.filter(x => x.grupo == 500 && x.valor == objProducto);
    this.form.controls['CodTipoRequerimiento'].setValue(null);
  }
  onChangeMotivo(): void {
    const codMotivo = this.form.controls['CodMotivo'].value;
    const objMotivo = this.parametros.filter(x => x.grupo == 500 && x.parametro == codMotivo);
    console.log("objMotivo", objMotivo);
    this.form.controls['CodTipoRequerimiento'].setValue(objMotivo[0].referencia);
  }
  onChangeTipoDocumento(): void {
    const TipoDocumento = +this.form.controls['TipoDocumento'].value;
    this.form.controls['Nombres'].setValue(null);
    this.form.controls['PrimerApellido'].setValue(null);
    this.form.controls['SegundoApellido'].setValue(null);
    this.form.controls['RazonSocial'].setValue(null);
    //this.form.controls['NroDocumento'].setValue(null);

    if (TipoDocumento == Constantes.RUC) {
      this.esRuc = true;
    }
    else {
      this.esRuc = false;
    }



  }

  onChangeMenor(e: any): void {
    console.log(e.target.value);

    this.EsMenorEdad = (e.target.value == 1 ? true : false);
  }
  openDialogoTerminosCondiciones(): void {
    //   const dialogRef = this.dialog.open(TerminosCondicionesComponent);
    const Terminos = this.parametros.filter(x => x.grupo == 800);

    const dialogRef = this.dialog.open(TerminosCondicionesComponent, {
      data: { parametros: Terminos }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result.data}`);

      if (result.data == 1) {
        this.checkedTerminos = true;
      }
      else {
        this.checkedTerminos = false;
      }
    });
  }


  descargarPDF() {


    const oBE = {
      nroReclamo: this.NroRequerimiento
    };
    this.loading = true;
    this.appService.generateTokenReclamacionesAPI().subscribe((data: any) => {
      let token = data.headers.get('authorization');
      this.requerimientoServices.ObtenerRequerimientoDetalle(oBE, token).subscribe(result => {
        console.log("response", result.response);
        if (result.response != null) {
          this.loading = false;
          this.downloadPDF(result.response, oBE.nroReclamo);
        }
      });
    }, (err) => {

      console.error(err);
    });

  }


  async getBase64ImageFromUrl(imageUrl: string) {
    var res = await fetch(imageUrl);
    var blob = await res.blob();

    return new Promise((resolve, reject) => {
      var reader = new FileReader();
      reader.addEventListener("load", function () {
        resolve(reader.result);
      }, false);

      reader.onerror = () => {
        return reject(this);
      };
      reader.readAsDataURL(blob);
    })
  }

  downloadPDF(requerimiento: any, filename: string): void {
    console.log(requerimiento);
    const doc = new jsPDF();

    const AGENCIA = (requerimiento.agencia == null ? '' : requerimiento.agencia);
    const AGENCIA_DIRECCION = (requerimiento.direccionAgencia == null ? '' : requerimiento.direccionAgencia);

    const ES_MENOR = (requerimiento.esMenor == 0 ? 'No' : 'Si');
    const TIPO_DOCUMENTO = (requerimiento.tipoDocumentoDescripcion == null ? '' : requerimiento.tipoDocumentoDescripcion);
    const NUMERO_DOCUMENTO = (requerimiento.nroDocumento == null ? '' : requerimiento.nroDocumento);

    const NOMBRES = (requerimiento.nombres == null ? '' : requerimiento.nombres);
    const PRIMER_APELLIDO = (requerimiento.primerApellido == null ? '' : requerimiento.primerApellido);
    const SEGUNDO_APELLIDO = (requerimiento.segundoApellido == null ? '' : requerimiento.segundoApellido);
    const RAZON_SOCIAL = (requerimiento.razonSocial == null ? '' : requerimiento.razonSocial);

    const DEPARTAMENTO = (requerimiento.departamento == null ? '' : requerimiento.departamento);
    const PROVINCIA = (requerimiento.provincia == null ? '' : requerimiento.provincia);
    const DISTRITO = (requerimiento.distrito == null ? '' : requerimiento.distrito);

    const DIRECCION = (requerimiento.direccion == null ? '' : requerimiento.direccion);
    const REFERENCIA = (requerimiento.referencia == null ? '' : requerimiento.referencia);

    const CORREO_ELECTRONICO = (requerimiento.correoElectronico == null ? '' : requerimiento.correoElectronico);
    const TELEFONO = (requerimiento.telefono == null ? '' : requerimiento.telefono);
    const CELULAR = (requerimiento.celular == null ? '' : requerimiento.celular);

    const PRODUCTO = (requerimiento.producto == null ? '' : requerimiento.producto);
    const MOTIVO = (requerimiento.motivo == null ? '' : requerimiento.motivo);
    const TIPO_REQUERIMIENTO = (requerimiento.tipoRequerimiento == null ? '' : requerimiento.tipoRequerimiento);

    const DESCRIPCION = (requerimiento.descripcion == null ? '' : requerimiento.descripcion);
    const FORMA_ENVIO = (requerimiento.formaEnvio == null ? '' : requerimiento.formaEnvio);

    var PosicionX = 25;
    var PosicionY = 21;
    var Altura = 6;

    doc.addImage(this.base64TrimmedURL, "JPEG", 77, 10, 55, 12);
    PosicionY = PosicionY + 14;


    doc.setFont("bold");
    doc.setTextColor(0, 0, 0);

    doc.setFontSize(16);

    doc.text("Libro de reclamaciones", 105, PosicionY, {
      align: 'center'
    });

    doc.setTextColor(38, 166, 154);
    doc.setFont("bold");
    PosicionY = PosicionY + Altura * 2;
    doc.text(TIPO_REQUERIMIENTO + " " + filename, 105, PosicionY, {
      align: 'center'
    });
    PosicionY = PosicionY + Altura;
    doc.setDrawColor(180, 180, 180); // draw gray lines
    doc.line(2, PosicionY, 206, PosicionY); // divider
    PosicionY = PosicionY + Altura;

    doc.setFontSize(14);
    //doc.setFont("calibri", "bold");

    doc.setFont("bold");
    doc.text("Datos de la oficina", PosicionX, PosicionY);
    PosicionY = PosicionY + Altura;

    doc.setTextColor(0, 0, 0);
    doc.setFontSize(10);
    //doc.setFont("calibri", "bold");
    doc.setFont("bold");
    doc.text("Agencia", PosicionX, PosicionY);
    doc.text("Dirección", PosicionX + 70, PosicionY);

    PosicionY = PosicionY + Altura;

    doc.setFontSize(14);
    // doc.setFont("calibri", "normal");
    doc.setFont("normal");
    doc.text(AGENCIA, PosicionX, PosicionY);
    doc.text(AGENCIA_DIRECCION, PosicionX + 70, PosicionY, {
      maxWidth: 95
    });

    var width = doc.getTextWidth(AGENCIA_DIRECCION);

    var line = width / 95;
    var linealtura = 1;

    if (line > 0) {
      linealtura = linealtura + line;
    }
    PosicionY = PosicionY + 5 * linealtura;
    doc.line(2, PosicionY, 206, PosicionY); // divider
    PosicionY = PosicionY + Altura;

    doc.setTextColor(38, 166, 154);
    doc.setFontSize(14);
    //doc.setFont("calibri", "bold");
    doc.setFont("bold");
    doc.text("Datos del Cliente", PosicionX, PosicionY);

    PosicionY = PosicionY + Altura;

    doc.setTextColor(0, 0, 0);
    doc.setFontSize(10);
    // doc.setFont("calibri", "bold");
    doc.setFont("bold");
    doc.text("¿Es menor de edad?", PosicionX, PosicionY);

    doc.setFontSize(14);
    //doc.setFont("calibri", "normal");
    doc.setFont("normal");
    doc.text(ES_MENOR, PosicionX + 70, PosicionY);

    PosicionY = PosicionY + Altura;

    doc.setFontSize(10);
    //doc.setFont("calibri", "bold");
    doc.setFont("bold");
    doc.text("Tipo de Documento", PosicionX, PosicionY);
    doc.text("Número de Documento", PosicionX + 120, PosicionY);

    PosicionY = PosicionY + Altura;


    doc.setFontSize(14);
    //doc.setFont("calibri", "normal");
    doc.setFont("normal");
    doc.text(TIPO_DOCUMENTO, PosicionX, PosicionY);
    doc.text(NUMERO_DOCUMENTO, PosicionX + 120, PosicionY);

    PosicionY = PosicionY + Altura;
    doc.setFontSize(10);
    //doc.setFont("calibri", "bold");
    doc.setFont("bold");
    if (this.esRuc == false) {

      doc.text("Nombres", PosicionX, PosicionY);
      doc.text("Primer Apellido", PosicionX + 70, PosicionY);
      doc.text("Segundo Apellido", PosicionX + 120, PosicionY);



      PosicionY = PosicionY + Altura;
      doc.setFontSize(14);
      //doc.setFont("calibri", "normal");
      doc.setFont("normal");
      doc.text(NOMBRES, PosicionX, PosicionY, {
        maxWidth: 60
      });
      doc.text(PRIMER_APELLIDO, PosicionX + 70, PosicionY, {
        maxWidth: 50
      });
      doc.text(SEGUNDO_APELLIDO, PosicionX + 120, PosicionY, {
        maxWidth: 50
      });

      width = doc.getTextWidth(NOMBRES);
      var width2 = doc.getTextWidth(PRIMER_APELLIDO);
      var width3 = doc.getTextWidth(SEGUNDO_APELLIDO);

      line = width / 60;
      var line2 = width2 / 45;
      var line3 = width3 / 35;
      linealtura = 1;

      var lineactual = linealtura + parseInt(Math.max(line, line2, line3) + "");
      if (lineactual > 0) {
        linealtura = linealtura + lineactual;
      }

      PosicionY = PosicionY + Altura * linealtura;
      doc.line(2, PosicionY, 206, PosicionY); // divider
      PosicionY = PosicionY + Altura;

    }
    else {
      doc.text("Razón Social", PosicionX, PosicionY);
      PosicionY = PosicionY + Altura;

      doc.setFontSize(14);
      //doc.setFont("calibri", "normal");
      doc.setFont("normal");
      doc.text(RAZON_SOCIAL, PosicionX, PosicionY);
      PosicionY = PosicionY + Altura * 2;
      doc.line(2, PosicionY, 206, PosicionY); // divider
      PosicionY = PosicionY + Altura;
    }


    doc.setTextColor(38, 166, 154);
    doc.setFontSize(14);
    // doc.setFont("calibri", "bold");
    doc.setFont("bold");
    doc.text("Datos del contacto", PosicionX, PosicionY);

    PosicionY = PosicionY + Altura;

    doc.setTextColor(0, 0, 0);
    doc.setFontSize(10);
    //doc.setFont("calibri", "bold");
    doc.setFont("bold");
    doc.text("Departamento", PosicionX, PosicionY);
    doc.text("Provincia", PosicionX + 70, PosicionY);
    doc.text("Distrito", PosicionX + 120, PosicionY);

    PosicionY = PosicionY + Altura;
    doc.setFontSize(14);
    //doc.setFont("calibri", "normal");
    doc.setFont("normal");
    doc.text(DEPARTAMENTO, PosicionX, PosicionY);
    doc.text(PROVINCIA, PosicionX + 70, PosicionY);
    doc.text(DISTRITO, PosicionX + 120, PosicionY);

    PosicionY = PosicionY + Altura;
    doc.setFontSize(10);
    //doc.setFont("calibri", "bold");
    doc.setFont("bold");
    doc.text("Dirección", PosicionX, PosicionY);
    doc.text("Referencia", PosicionX + 120, PosicionY);

    PosicionY = PosicionY + Altura;

    doc.setFontSize(14);
    //doc.setFont("calibri", "normal");
    doc.setFont("normal");
    doc.text(DIRECCION, PosicionX, PosicionY, {
      maxWidth: 100
    });
    doc.text(REFERENCIA, PosicionX + 120, PosicionY, {
      maxWidth: 45
    });

    width = doc.getTextWidth(DIRECCION);
    width2 = doc.getTextWidth(REFERENCIA);

    line = width / 100;
    line2 = width2 / 45;

    linealtura = 1;

    var lineactual = linealtura + parseInt(Math.max(line, line2) + "");
    if (lineactual > 0) {
      linealtura = linealtura + lineactual;
    }


    PosicionY = PosicionY + Altura * linealtura;

    doc.setFontSize(10);
    //doc.setFont("calibri", "bold");
    doc.setFont("bold");
    doc.text("Correo electrónico", PosicionX, PosicionY);
    doc.text("Teléfono Fijo", PosicionX + 70, PosicionY);
    doc.text("Celular", PosicionX + 120, PosicionY);

    PosicionY = PosicionY + Altura;

    doc.setFontSize(14);
    //doc.setFont("calibri", "normal");
    doc.setFont("normal");
    doc.text(CORREO_ELECTRONICO, PosicionX, PosicionY);
    doc.text(TELEFONO, PosicionX + 70, PosicionY);
    doc.text(CELULAR, PosicionX + 120, PosicionY);

    PosicionY = PosicionY + Altura;
    doc.line(2, PosicionY, 206, PosicionY); // divider
    PosicionY = PosicionY + Altura;

    if (PosicionY + Altura  > 230) {
      doc.addPage();
      PosicionY = 30 // Restart height position
    }

    doc.setTextColor(38, 166, 154);
    doc.setFontSize(14);
    //doc.setFont("calibri", "bold");
    doc.setFont("bold");
    doc.text("Datos del requerimiento, queja o solicitud", PosicionX, PosicionY);

    PosicionY = PosicionY + Altura;



    doc.setTextColor(0, 0, 0);
    doc.setFontSize(10);
    //doc.setFont("calibri", "bold");
    doc.setFont("bold");
    doc.text("Producto", PosicionX, PosicionY);
    doc.text("Motivo", PosicionX + 60, PosicionY);
    doc.text("Tipo requerimiento", PosicionX + 120, PosicionY);
    PosicionY = PosicionY + Altura;
    doc.setFontSize(14);
    //doc.setFont("calibri", "normal");
    doc.setFont("normal");
    doc.text(PRODUCTO, PosicionX, PosicionY, {
      maxWidth: 50
    });
    doc.text(MOTIVO, PosicionX + 60, PosicionY, {
      maxWidth: 50
    });
    doc.text(TIPO_REQUERIMIENTO, PosicionX + 120, PosicionY);

    width = doc.getTextWidth(PRODUCTO);
    width2 = doc.getTextWidth(MOTIVO);

    line = width / 100;
    line2 = width2 / 50;

    linealtura = 1;
    var lineactual = linealtura + parseInt(Math.max(line, line2) + "");
    if (lineactual > 0) {
      linealtura = linealtura + lineactual;
    }

    PosicionY = PosicionY + Altura * linealtura;

    if (PosicionY + Altura * linealtura > 230) {
      doc.addPage();
      PosicionY = 30 // Restart height position
    }

    doc.setFontSize(10);
    //doc.setFont("calibri", "bold");
    doc.setFont("bold");
    doc.text("¿Donde desea que se le envie la carta de respuesta?", PosicionX, PosicionY);

    PosicionY = PosicionY + Altura;



    doc.setFontSize(14);
    //doc.setFont("calibri", "normal");
    doc.setFont("normal");
    doc.text(FORMA_ENVIO, PosicionX, PosicionY);

    PosicionY = PosicionY + Altura;

    width = doc.getTextWidth(DESCRIPCION);
    line = width / 160;

    if (PosicionY + Altura * line > 230) {
      doc.addPage();
      PosicionY = 30 // Restart height position
    }

    doc.setFontSize(10);
    //doc.setFont("calibri", "bold");
    doc.setFont("bold");
    doc.text("Descripción", PosicionX, PosicionY);

    PosicionY = PosicionY + Altura;
    doc.setFontSize(14);
    //doc.setFont("calibri", "normal");
    doc.setFont("normal");
    doc.text(DESCRIPCION, PosicionX, PosicionY, {
      maxWidth: 160,
      align: 'justify'
    });




    doc.save(filename + '.pdf');
  }

  loadPage(): void {
    this.loading = true;
    var oRecursoBE = {
      "CodTabla": 5,
      "ReferenciaID": 1,
      "LaserficheID": null
    }
    this.appService.generateTokenReclamacionesAPI().subscribe((data: any) => {
      let token = data.headers.get('authorization');
      console.log("obteneragencia");
      console.log(token)
      forkJoin([
        this.requerimientoServices.ObtenerAgencia(token),
        this.requerimientoServices.ObtenerParametro(token),
        this.requerimientoServices.listarDepartamento(token),
        this.requerimientoServices.ObtenerRecursos(oRecursoBE, token)
      ]).subscribe((data: any) => {
        debugger;
        console.log(data);
        this.agencias = data[0].response;
        this.parametros = data[1].response;
        this.departamentos = data[2].response;
        console.log("departamentos");
        console.log(this.departamentos)
        this.recursos = data[3].response;

        console.log(this.recursos);
        this.loading = false;
      }, (err) => {
        this.loading = false;
        console.error(err);
      });
    });


  }


  initForm(): void {
    this.form = this.formBuilder.group({
      CodRequerimiento: [0],
      CodRequerimientoDetalle: [0],
      NroRequerimiento: [null],
      //  Agencia: [null, Validators.required],
      AgenciaDireccion: [{ value: null, disabled: true }],
      CodAgencia: [null, Validators.required],
      EsMenor: [0],
      TipoDocumento: [null, Validators.required],
      NroDocumento: [null, Validators.required],
      Nombres: [null],
      PrimerApellido: [null],
      SegundoApellido: [null],
      RazonSocial: [null],
      CodDepartamento: [null, Validators.required],
      CodProvincia: [null, Validators.required],
      CodDistrito: [null],
      CodUbigeo: [null, Validators.required],
      Direccion: [null, [Validators.required, Validators.minLength(10), Validators.maxLength(200)]],
      Referencia: [null, [Validators.required, Validators.minLength(10), Validators.maxLength(200)]],
      CorreoElectronico: [null, [Validators.maxLength(100)]],
      Telefono: [null],
      Celular: [null, [Validators.required, Validators.pattern('^[0-9]{9}$')]],
      CodProducto: [null, Validators.required],
      CodMotivo: [null, Validators.required],
      CodTipoRequerimiento: [null],
      CodOrigen: [1],
      Descripcion: [null, Validators.required],
      CodFormaEnvio: [null, Validators.required],
      Estado: [1],
      Archivo1: [null],
      Archivo2: [null],
      Archivo3: [null],
      Archivo4: [null]
      //Estado: [{value: this.route.snapshot.paramMap.get("estado"), disabled: false}, Validators.required],


    });
  }


  registrar(): void {

    this.formSubmitted = true;

    if (this.form.invalid) {
      return;
    }
    const Celular = this.form.controls['Celular'].value;
    const CodAgencia = +this.form.controls['CodAgencia'].value;
    const CodFormaEnvio = +this.form.controls['CodFormaEnvio'].value;
    const CodMotivo = +this.form.controls['CodMotivo'].value;
    const CodOrigen = +this.form.controls['CodOrigen'].value;
    const CodProducto = +this.form.controls['CodProducto'].value;
    const CodRequerimiento = +this.form.controls['CodRequerimiento'].value;
    const CodRequerimientoDetalle = +this.form.controls['CodRequerimientoDetalle'].value;
    const CodTipoRequerimiento = +this.form.controls['CodTipoRequerimiento'].value;
    const CodUbigeo = this.form.controls['CodUbigeo'].value;
    const CorreoElectronico = this.form.controls['CorreoElectronico'].value;
    const Descripcion = this.form.controls['Descripcion'].value;
    const Direccion = this.form.controls['Direccion'].value;
    const EsMenor = +this.form.controls['EsMenor'].value;
    const Estado = +this.form.controls['Estado'].value;
    const Nombres = this.form.controls['Nombres'].value;
    const NroDocumento = this.form.controls['NroDocumento'].value;
    const NroRequerimiento = this.form.controls['NroRequerimiento'].value;
    const PrimerApellido = this.form.controls['PrimerApellido'].value;
    const RazonSocial = this.form.controls['RazonSocial'].value;
    const Referencia = this.form.controls['Referencia'].value;
    const SegundoApellido = this.form.controls['SegundoApellido'].value;
    const Telefono = this.form.controls['Telefono'].value;
    const TipoDocumento = +this.form.controls['TipoDocumento'].value;

    if (TipoDocumento == Constantes.RUC) {
      if (RazonSocial == null || RazonSocial == "") {
        this.dialogService.openMsgErrorDialog("Ingrese la razón social");
        return;
      }

      if (this.Archivo1 == null && this.Archivo2 == null && this.Archivo3 == null && this.Archivo4 == null) {
        this.dialogService.openMsgErrorDialog("Ingrese subir por lo menos 1 archivo (Vigencia de poder actualizado)");
        return;
      }
    }
    else {

      if(TipoDocumento==Constantes.DNI){
        if(NroDocumento.length!=8){
          this.dialogService.openMsgErrorDialog("El número de dni es invalido");
            return;
        }
      }


      if(TipoDocumento==Constantes.PASAPORTE){
        if(NroDocumento.length>12){
          this.dialogService.openMsgErrorDialog("El número de pasaporte es invalido");
            return;
        }
      }

      if(TipoDocumento==Constantes.CE){
        if(NroDocumento.length>15){
          this.dialogService.openMsgErrorDialog("El número de carne de extranjería es invalido");
            return;
        }
      }

      if(TipoDocumento==Constantes.OTROS){
        if(NroDocumento.length>12){
          this.dialogService.openMsgErrorDialog("El número de carne de extranjería es invalido");
            return;
        }
      }

      if (Nombres == null || Nombres == "") {
        this.dialogService.openMsgErrorDialog("Ingrese su nombre");
        return;
      }
      if (PrimerApellido == null || PrimerApellido == "") {
        this.dialogService.openMsgErrorDialog("Ingrese su primer apellido");
        return;
      }
      if (SegundoApellido == null || SegundoApellido == "") {
        this.dialogService.openMsgErrorDialog("Ingrese su segundo apellido");
        return;
      }
    }

    if (CodFormaEnvio == Constantes.Whatsapp) {
      if (Celular == null || Celular == "") {
        this.dialogService.openMsgErrorDialog("Ingrese el numero de celular");
        return;
      }
    }

    if (CodFormaEnvio == Constantes.Email) {
      if (Celular == null || Celular == "") {
        this.dialogService.openMsgErrorDialog("Ingrese el numero de celular");
        return;
      }
    }
    if (CodFormaEnvio == Constantes.Email) {


      if (CorreoElectronico == null) {
        this.dialogService.openMsgErrorDialog("Ingrese un correo electronico");
        return;
      }

      if (CorreoElectronico.trim().length == 0) {
        this.dialogService.openMsgErrorDialog("Ingrese un correo electronico");
        return;
      }

      if (!EmailValidator.validate(CorreoElectronico)) {
        this.dialogService.openMsgErrorDialog("formato de correo invalido");
        return;
      }

    }
    if (CodFormaEnvio == Constantes.Agencia) {

    }

    if (CodFormaEnvio == Constantes.Domicilio) {

    }


    if (!this.checkedTerminos) {
      this.dialogService.openMsgErrorDialog("Debe aceptar los terminos y condiciones");
      return;
    }

    if (this.recaptcha == null) {
      this.dialogService.openMsgErrorDialog("debe indicar que ud. no es un robot");
      return;
    }


    this.dialogService.openConfirmDialog('¿Desea registrar su reclamo?')
      .afterClosed().subscribe(res => {
        if (res) {
          this.loading = true;
          const Entity = {
            Celular: this.form.controls['Celular'].value,
            CodAgencia: +this.form.controls['CodAgencia'].value,
            CodFormaEnvio: +this.form.controls['CodFormaEnvio'].value,
            CodMotivo: +this.form.controls['CodMotivo'].value,
            CodOrigen: +Constantes.OrigenExterno,
            CodProducto: +this.form.controls['CodProducto'].value,
            CodRequerimiento: +this.form.controls['CodRequerimiento'].value,
            CodRequerimientoDetalle: +this.form.controls['CodRequerimientoDetalle'].value,
            CodTipoRequerimiento: +this.form.controls['CodTipoRequerimiento'].value,
            CodUbigeo: this.form.controls['CodUbigeo'].value,
            CorreoElectronico: this.form.controls['CorreoElectronico'].value,
            Descripcion: this.form.controls['Descripcion'].value,
            Direccion: this.form.controls['Direccion'].value,
            EsMenor: (this.EsMenorEdad ? 1 : 0), // +this.form.controls['EsMenor'].value,
            Estado: +this.form.controls['Estado'].value,
            Nombres: this.form.controls['Nombres'].value,
            NroDocumento: this.form.controls['NroDocumento'].value,
            NroRequerimiento: this.form.controls['NroRequerimiento'].value,
            PrimerApellido: this.form.controls['PrimerApellido'].value,
            RazonSocial: this.form.controls['RazonSocial'].value,
            Referencia: this.form.controls['Referencia'].value,
            SegundoApellido: this.form.controls['SegundoApellido'].value,
            Telefono: this.form.controls['Telefono'].value,
            TipoDocumento: +this.form.controls['TipoDocumento'].value,
            UsuarioRegistro: "", //this.securityService.leerUsuarioWeb(),

            Archivo1: this.Archivo1,
            Archivo2: this.Archivo2,
            Archivo3: this.Archivo3,
            Archivo4: this.Archivo4,

            NombreArchivo1: this.NombreArchivo1,
            NombreArchivo2: this.NombreArchivo2,
            NombreArchivo3: this.NombreArchivo3,
            NombreArchivo4: this.NombreArchivo4

          }


          console.log("request", Entity);
          this.appService.generateTokenReclamacionesAPI().subscribe((data: any) => {
            let token = data.headers.get('authorization');
            this.requerimientoServices.Registrar(Entity, token).subscribe(result => {
              debugger;
              console.log("response", result);

              this.loading = false;
              if (result.response.success.resultado) {
                this.form.disable();
                this.bRegistroRequerimiento = true;
                this.disabled = true;
                this.dialogService.openMsgSuccessDialog("Se registró correctamente");

                this.NroRequerimiento = result.response.success.codigo;
                this.reenviar_correo = this.form.controls['CorreoElectronico'].value;
              }
              else {
                this.dialogService.openMsgErrorDialog("Hubo un error al intentar registrar el requerimiento");
              }


            }, (err) => {
              this.loading = false;
              console.log(err);
            });
          }, (err) => {

            console.error(err);
          });
        }
      });
  }


  enviar_correo(): void {
    if (this.bRegistroRequerimiento) {

      if (this.reenviar_correo == null) {
        this.dialogService.openMsgErrorDialog("Ingrese un correo electronico");
        return;
      }

      if (this.reenviar_correo.trim().length == 0) {
        this.dialogService.openMsgErrorDialog("Ingrese un correo electronico");
        return;
      }

      if (!EmailValidator.validate(this.reenviar_correo)) {
        this.dialogService.openMsgErrorDialog("formato de correo invalido");
        return;
      }

      var Entity = {
        'CorreoElectronico': this.reenviar_correo,
        'nroReclamo': this.NroRequerimiento
      };
      this.loading = true;
      this.appService.generateTokenReclamacionesAPI().subscribe((data: any) => {
        let token = data.headers.get('authorization');
        this.requerimientoServices.reenviar_correo(Entity, token).subscribe(result => {
          this.loading = false;

          if (result.response.success.resultado) {
            this.dialogService.openMsgSuccessDialog("Se envio el correo correctamente");
          }
          else {
            this.dialogService.openMsgErrorDialog("Hubo un error al intentar enviar el correo");
          }
        });
      }, (err) => {

        console.error(err);
      });

    }
    else {
      this.dialogService.openMsgErrorDialog("hubo un error al intentar registrar el requerimiento");
    }
  }

  handleUpload(event: any, numfile: number) {
    debugger;


    const file = event.target.files[0];
    if (file == null || file.length <= 0) {
      switch (numfile) {
        case 1:
          this.Archivo1 = null;
          this.NombreArchivo1 = null;
          this.form.controls['Archivo1'].setValue(null);
          this.bMostrarArchvo1 = false;
          break;
        case 2:
          this.Archivo2 = null;
          this.NombreArchivo2 = null;
          this.form.controls['Archivo2'].setValue(null);
          this.bMostrarArchvo2 = false;
          break;
        case 3:
          this.Archivo3 = null;
          this.NombreArchivo3 = null;
          this.form.controls['Archivo3'].setValue(null);
          this.bMostrarArchvo3 = false;
          break;
        case 4:
          this.Archivo4 = null;
          this.NombreArchivo4 = null;
          this.form.controls['Archivo4'].setValue(null);
          this.bMostrarArchvo4 = false;
          break;

        default:
          break;
      }

      return;
    }

    var allowedExtensions = /(.jpg|.jpeg|.png|.pdf)$/i;
    if (!allowedExtensions.exec(file.name)) {
      this.dialogService.openMsgErrorDialog("Formato no válido");
      switch (numfile) {
        case 1:
          this.Archivo1 = null;
          this.NombreArchivo1 = null;
          this.form.controls['Archivo1'].setValue(null);
          this.bMostrarArchvo1 = false;
          break;
        case 2:
          this.Archivo2 = null;
          this.NombreArchivo2 = null;
          this.form.controls['Archivo2'].setValue(null);
          this.bMostrarArchvo2 = false;
          break;
        case 3:
          this.Archivo3 = null;
          this.NombreArchivo3 = null;
          this.form.controls['Archivo3'].setValue(null);
          this.bMostrarArchvo3 = false;
          break;
        case 4:
          this.Archivo4 = null;
          this.NombreArchivo4 = null;
          this.form.controls['Archivo4'].setValue(null);
          this.bMostrarArchvo4 = false;
          break;

        default:
          break;
      }
      return;
    }
    if (file.size > this.MAXIMO_TAMANIO_BYTES) {
      const tamanioEnMb = this.MAXIMO_TAMANIO_BYTES / 1000000;

      switch (numfile) {
        case 1:
          this.Archivo1 = null;
          this.NombreArchivo1 = null;
          this.form.controls['Archivo1'].setValue(null);
          this.bMostrarArchvo1 = false;
          break;
        case 2:
          this.Archivo2 = null;
          this.NombreArchivo2 = null;
          this.form.controls['Archivo2'].setValue(null);
          this.bMostrarArchvo2 = false;
          break;
        case 3:
          this.Archivo3 = null;
          this.NombreArchivo3 = null;
          this.form.controls['Archivo3'].setValue(null);
          this.bMostrarArchvo3 = false;
          break;
        case 4:
          this.Archivo4 = null;
          this.NombreArchivo4 = null;
          this.form.controls['Archivo4'].setValue(null);
          this.bMostrarArchvo4 = false;
          break;

        default:
          break;
      }
      this.dialogService.openMsgErrorDialog(`El tamaño máximo es ${tamanioEnMb} MB`);


      // Limpiar
      // $miInput.value = "";
      return;
    } else {
      // Validación pasada. Envía el formulario o haz lo que tengas que hacer
    }

    const FileNamme = file.name;
    switch (numfile) {
      case 1:
        this.NombreArchivo1 = FileNamme;
        this.bMostrarArchvo1 = true;
        break;
      case 2:
        this.NombreArchivo2 = FileNamme;
        this.bMostrarArchvo2 = true;
        break;
      case 3:
        this.NombreArchivo3 = FileNamme;
        this.bMostrarArchvo3 = true;
        break;
      case 4:
        this.NombreArchivo4 = FileNamme;
        this.bMostrarArchvo4 = true;
        break;

      default:
        break;
    }

    const reader = new FileReader();
    console.log("file", file);
    reader.readAsDataURL(file);
    reader.onload = () => {
      console.log("reader", reader);
      switch (numfile) {
        case 1:
          this.Archivo1 = reader.result || null;
          break;
        case 2:
          this.Archivo2 = reader.result || null;
          break;
        case 3:
          this.Archivo3 = reader.result || null;
          break;
        case 4:
          this.Archivo4 = reader.result || null;
          break;
        default:
          break;
      }

      console.log(reader.result);
    };
  }

  resolved(captchaResponse: string) {

    this.recaptcha = captchaResponse;
    console.log(`Resolved captcha with response: ${captchaResponse}`);
  }

  LimpiarUpload(numfile: number) {

    switch (numfile) {
      case 1:
        this.myInputArchivo1.nativeElement.value = "";
        this.Archivo1 = null;
        this.NombreArchivo1 = null;
        this.form.controls['Archivo1'].setValue(null);
        this.bMostrarArchvo1 = false;
        this.myInputTextArchivo1.nativeElement.value = "";

        break;
      case 2:
        this.myInputArchivo2.nativeElement.value = "";
        this.myInputTextArchivo2.nativeElement.value = "";
        this.Archivo2 = null;
        this.NombreArchivo2 = null;
        this.form.controls['Archivo2'].setValue(null);
        this.bMostrarArchvo2 = false;

        break;
      case 3:
        this.myInputArchivo3.nativeElement.value = "";
        this.myInputTextArchivo3.nativeElement.value = "";
        this.Archivo3 = null;
        this.NombreArchivo3 = null;
        this.form.controls['Archivo3'].setValue(null);
        this.bMostrarArchvo3 = false;

        break;
      case 4:
        debugger;
        this.myInputArchivo4.nativeElement.value = "";
        this.myInputTextArchivo4.nativeElement.value = "";

        this.Archivo4 = null;
        this.NombreArchivo4 = null;
        this.form.controls['Archivo4'].setValue(null);
        this.bMostrarArchvo4 = false;

        break;

      default:
        break;
    }


  }

  reload():void {
    window.scrollTo(0, 0);
    window.location.reload();
    /*
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this.router.navigate([currentUrl]);
        console.log(currentUrl);
    });
    */
  }
}
