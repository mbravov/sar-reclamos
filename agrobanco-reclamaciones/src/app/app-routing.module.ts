import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PreguntasFrecuentesComponent } from './components/preguntas-frecuentes/preguntas-frecuentes.component';
import { RequerimientoComponent } from './components/requerimiento/requerimiento.component';


const routes: Routes = [

  { path:'libro-reclamaciones', component:RequerimientoComponent },

  { path:'preguntas-frecuentes', component: PreguntasFrecuentesComponent },
  { path: '**' , pathMatch: 'full', redirectTo: 'libro-reclamaciones'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
