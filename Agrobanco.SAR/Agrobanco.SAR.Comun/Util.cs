﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Agrobanco.SAR.Comun
{
    public static class Util
    {
        public static DateTime ConvertStringToDatetime(string strFecha)
        {
            DateTime tempDate;
            try
            {
               // CultureInfo culture = new CultureInfo("en-US");
                tempDate = DateTime.ParseExact(strFecha, "dd/MM/yyyy", CultureInfo.InvariantCulture);
    //Convert.ToDateTime(strFecha, culture);
            }
            catch (Exception)
            {
                tempDate = DateTime.Now;

            }
            return tempDate;
        }
    }
}
