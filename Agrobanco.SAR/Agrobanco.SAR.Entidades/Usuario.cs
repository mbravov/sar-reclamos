﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades
{
    public class Usuario
    {
        public int CodUsuario { get; set; }
        public string UsuarioWeb { get; set; }
        public string NombresApellidos { get; set; }
        public string Perfil { get; set; }
        public string CorreoElectronico { get; set; }

        public bool? Estado { get; set; }
        public int? CodAgencia { get; set; }
    }
}
