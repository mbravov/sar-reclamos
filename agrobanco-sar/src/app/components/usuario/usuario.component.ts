import { Component, OnInit , ViewChild} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Usuario } from '../models/usuario.interface';
import { RequerimientoService } from 'src/app/services/requerimiento.service';
import { ParametroService } from 'src/app/services/parametro.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Router } from "@angular/router";
import { forkJoin} from 'rxjs';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {


  busquedaFiltro! : FormGroup;
  ELEMENT_DATA! : Usuario[];
  displayedColumns: string[] = ["codUsuario", "perfil", "correoElectronico", "usuarioWeb", "nombresApellidos",
  "estado", "usuarioRegistro","fechaRegistro","editar"];

  listaUsuarios = new MatTableDataSource<Usuario>(this.ELEMENT_DATA);
  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;

  usuarios: any[] = [];

  loading!:boolean;

  parametros:any[] = [];


  constructor(
    private requerimientoService : RequerimientoService,
    private parametroService: ParametroService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.inicializarFormulario();       
      this.loadPage();   
      this.listaUsuarios.paginator = this.paginator; 
  }
  
  loadPage(): void {
    
    this.loading = false;
    let params = this.busquedaFiltro.value;
    console.log(params);

    forkJoin([   
      this.parametroService.ObtenerParametro() ,
      this.requerimientoService.ObtenerListaUsuarios(params)       
      
    ]).subscribe( (data:any) => {
      
      console.log(data);  
      this.parametros = data[0].response; 
      this.listaUsuarios.data = data[1].response as Usuario[]; 
        
        ///this.loading = true;
    }, (error) =>{
      console.log(error);
      ///this.loading = true;
    });

  }

  private inicializarFormulario()
  {
    this.busquedaFiltro = new FormGroup({
      Perfil : new FormControl(''),      
      NombresApellidos : new FormControl(''),     
      UsuarioWeb : new FormControl('') 

    });

  }

  buscarListaUsuarios():void 
  {
    debugger;
    if(this.busquedaFiltro.invalid)
    {
      return;
    }
    
    this.loading = false;
    /*const perfil = this.busquedaFiltro.controls['Perfil'].value;
    this.busquedaFiltro.controls['Perfil'].setValue(perfil);*/

    let params = this.busquedaFiltro.value;    
    console.log(params);
    
  
    this.requerimientoService.ObtenerListaUsuarios(params).subscribe((result:any) =>{
      this.listaUsuarios.data = result.response as Usuario[];
     
      this.loading = false;
      

    }, (error)=> {
      console.log(error);
      this.loading = false;
    });

  }

  editarUsuario(codUsuario: string):void  {
 
    this.router.navigate(['/usuario/usuario-detalle', codUsuario]);

  }

}

