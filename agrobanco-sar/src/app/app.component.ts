import { Component, Injectable } from '@angular/core';
import { AuthGuard } from './guards/auth.guard';
import { DomService } from './services/dom.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'agrobanco-sar';
  menuOpen:boolean = true;

  constructor(public authGuard: AuthGuard, public domService : DomService) {
    
  }

  hideSideBar(){
    this.domService.HideSideBar();    
  }
}
