export interface CreditoInterface {
    nroUltimoPrestamo : string;
    estado : string;
    saldototal : string;
    fechapago : string;
}
