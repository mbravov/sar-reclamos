﻿using Agrobanco.SAR.API.Models;
using Agrobanco.SAR.Comun;
using Agrobanco.SAR.Comun.Laserfiche;
using Agrobanco.SAR.Control.Filters;
using Agrobanco.SAR.Datos;
using Agrobanco.SAR.Entidades;
using Agrobanco.SAR.LogicaNegocio;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.SAR.API.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/laserfiche")]
    [ApiController]
    public class LaserFicheController : ControllerBase
    {
        [HttpPost("obtener")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object ObtenerLista([Required][FromHeader(Name = "Token")] string token, [FromBody] LaserFicheBE request)
        {
            LaserficheProxy laserfiche = new LaserficheProxy();

            var bytes = laserfiche.ConsultarDocumentoLista(request.LaserficheID);
            return StatusCode(200, bytes);
        }

        [HttpPost("registrar")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object RegistrarAchivos([Required][FromHeader(Name = "Token")] string token, [FromBody] LaserFicheBE request)
        {
            LaserficheProxy laserfiche = new LaserficheProxy();
            DocumentoModel DocumentoModelRequest = new DocumentoModel();
            DocumentoModel DocumentoModelResponse = new DocumentoModel();

            DocumentoModelRequest.Folder = request.NroReclamo;
            //List<ItemImagen> listIma = new List<ItemImagen>();
            List<Documento> lstDocumento = new List<Documento>();
            int item = 1;
            string base64 = string.Empty;
            if (!string.IsNullOrEmpty(request.Archivo))
            {
                base64 = string.Empty;
                base64 = request.Archivo.Split(',')[1];

                Documento oDocumento = new Documento();
                oDocumento.Extension = System.IO.Path.GetExtension(request.NombreArchivo).Split('.')[1];
                oDocumento.ArchivoBytes = base64;
                if (request.CodTabla == 6)
                    oDocumento.Nombre = request.NroReclamo;
                else if (request.CodTabla == 7)
                    oDocumento.Nombre = request.NroReclamo + "_OCM";
                else
                    oDocumento.Nombre = request.NroReclamo + "_ARCHIVO" + item.ToString().PadLeft(2, '0');


                oDocumento.Folder = request.NroReclamo;
                lstDocumento.Add(oDocumento);
                item++;
            }

            if (lstDocumento.Count > 0)
            {
                DocumentoModelRequest.ListaDocumentos = lstDocumento;
                DocumentoModelResponse = laserfiche.UploadListaDocumento(DocumentoModelRequest);

                RecursoDatos oRecursoDatos = null;
                RecursoBE oRecursoBE = null;
                foreach (var image in DocumentoModelResponse.ListaDocumentos)
                {
                    if (image.ResultadoOK)
                    {
                        oRecursoDatos = new RecursoDatos();
                        oRecursoBE = new RecursoBE();

                        oRecursoBE.CodTabla = request.CodTabla; //ConstantesTabla.RequerimientoMovimiento;
                        oRecursoBE.ReferenciaID = request.CodRequerimiento;
                        oRecursoBE.LaserficheID = image.CodigoLaserfiche;
                        oRecursoBE.Estado = true;
                        oRecursoBE.UsuarioRegistro = request.UsuarioRegistro;
                        oRecursoBE.Descripcion = image.Nombre;
                        Boolean resultad = oRecursoDatos.Registrar(oRecursoBE);

                        if (!resultad)
                            throw new Exception("Se presento un error al intentar registrar los archivos adjuntos");


                    }
                    else
                    {
                        throw new Exception("Se presento un error al subir el archivo");
                    }
                }
                DocumentoModelResponse.LaserficheID = oRecursoBE.LaserficheID;
                DocumentoModelResponse.ResultadoOK = true;
            }


            return StatusCode(200, DocumentoModelResponse);
        }



        [HttpPost("eliminar")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        // [ValidateAuthorizationRequest]
        public object Eliminar([Required][FromHeader(Name = "Token")] string token, [FromBody] LaserFicheBE request)
        {
            LaserficheProxy laserfiche = new LaserficheProxy();
            DocumentoModel DocumentoModelRequest = new DocumentoModel();
            DocumentoModel DocumentoModelResponse = new DocumentoModel();

            Documento oDocumento = new Documento();
            oDocumento.CodigoLaserfiche = Convert.ToInt32(request.LaserficheID);
            var documento = laserfiche.RemoveFile(oDocumento);

            RecursoDatos oRecursoDatos = new RecursoDatos();
            RecursoBE oRecursoBE = new RecursoBE();
            oRecursoBE.LaserficheID = oDocumento.CodigoLaserfiche;
            Boolean resultado = oRecursoDatos.Eliminar(oRecursoBE);

            if (!resultado)
                throw new Exception("Se presento un error al intentar eliminar los archivos adjuntos");



            return StatusCode(200, documento);
        }


        [HttpPost("registrar-plantilla")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object RegistrarPlanilla([Required][FromHeader(Name = "Token")] string token, [FromBody] LaserFicheBE request)
        {
            LaserficheProxy laserfiche = new LaserficheProxy();
            DocumentoModel DocumentoModelRequest = new DocumentoModel();
            DocumentoModel DocumentoModelResponse = new DocumentoModel();
            DocumentoModelRequest.Folder = ApplicationKeys.FolderPlantillaLaserfiche;
            List<Documento> lstDocumento = new List<Documento>();
      
            string base64 = string.Empty;
            if (!string.IsNullOrEmpty(request.Archivo))
            {
                base64 = string.Empty;
                base64 = request.Archivo.Split(',')[1];

                Documento oDocumento = new Documento();
                oDocumento.Extension = System.IO.Path.GetExtension(request.NombreArchivo).Split('.')[1];
                oDocumento.ArchivoBytes = base64;
                oDocumento.Nombre = System.IO.Path.GetFileNameWithoutExtension(request.NombreArchivo);
              //  GetFileNameWithoutExtension

                oDocumento.Folder = ApplicationKeys.FolderPlantillaLaserfiche;
                lstDocumento.Add(oDocumento);
                 
            }

            if (lstDocumento.Count > 0)
            {
                DocumentoModelRequest.ListaDocumentos = lstDocumento;
                DocumentoModelResponse = laserfiche.UploadListaDocumento(DocumentoModelRequest);

                RecursoDatos oRecursoDatos = null;
                RecursoBE oRecursoBE = null;
                foreach (var image in DocumentoModelResponse.ListaDocumentos)
                {
                    if (image.ResultadoOK)
                    {
                        PlantillaBE oPlantillaBE = new PlantillaBE();
                        PlantillaLogica oPlantillaLogica = new PlantillaLogica();

                        oPlantillaBE.CodTipoPlantilla = request.CodTipoPlantilla.ToString();
                        oPlantillaBE.UsuarioRegistro = request.UsuarioRegistro;
                        Int64 CodPlantilla = oPlantillaLogica.Registrar(oPlantillaBE);

                        if (CodPlantilla > 0)
                        {
                            oRecursoDatos = new RecursoDatos();
                            oRecursoBE = new RecursoBE();

                            oRecursoBE.CodTabla = request.CodTabla; //ConstantesTabla.RequerimientoMovimiento;
                            oRecursoBE.ReferenciaID = CodPlantilla;
                            oRecursoBE.LaserficheID = image.CodigoLaserfiche;
                            oRecursoBE.Estado = true;
                            oRecursoBE.UsuarioRegistro = request.UsuarioRegistro;
                            oRecursoBE.Descripcion = System.IO.Path.GetFileNameWithoutExtension(image.Nombre);
                            Boolean resultad = oRecursoDatos.Registrar(oRecursoBE);

                            if (!resultad)
                                throw new Exception("Se presento un error al intentar registrar los archivos adjuntos");
                        }
                        


                    }
                    else
                    {
                        throw new Exception("Se presento un error al subir el archivo");
                    }
                }
                DocumentoModelResponse.LaserficheID = oRecursoBE.LaserficheID;
                DocumentoModelResponse.ResultadoOK = true;
            }


            return StatusCode(200, DocumentoModelResponse);
        }
    }
}
