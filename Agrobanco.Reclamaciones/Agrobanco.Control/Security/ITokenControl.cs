﻿using Agrobanco.Control.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.Control.Security
{
    public interface ITokenControl
    {
        ResponseContainerModel GenerateJwtToken(Dictionary<string, string> dictTokenParam, Dictionary<string, string> dictClaims);
        string IsTokenJWTValid(string paramkeytoken, string token);
        string GetClaimValueByToken(string paramkeytoken, string tipoclaim, string token);
    }
}
