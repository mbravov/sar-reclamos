﻿using Agrobanco.SAR.API.Models;
using Agrobanco.SAR.Control.Filters;
using Agrobanco.SAR.Entidades;
using Agrobanco.SAR.LogicaNegocio;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.SAR.API.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/Parametro")]
    [ApiController]
    public class ParametroController : ControllerBase
    {
        private readonly ParametroLogica parametroLogica;
        
        public ParametroController()
        {
            parametroLogica = new ParametroLogica();
        }

        [HttpPost("lista")]        
        [ProducesResponseType(200, Type = typeof(List<ParametroBE>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult Parametro([Required][FromHeader(Name = "Token")] string token, [FromBody] Parametro oParametro)
        {
            try
            {

                var listaResultado = parametroLogica.ObtenerListaParametro(oParametro.Grupo);

                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("lista-grupo")]
        [ProducesResponseType(200, Type = typeof(List<ParametroBE>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult ParametroGrupo([Required][FromHeader(Name = "Token")] string token, [FromBody] Parametro oParametro)
        {
            try
            {

                var listaResultado = parametroLogica.ObtenerListarParametroGrupo(oParametro.strGrupo);

                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
