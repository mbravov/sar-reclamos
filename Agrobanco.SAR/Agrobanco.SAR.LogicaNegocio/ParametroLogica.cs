﻿using Agrobanco.SAR.Datos;
using Agrobanco.SAR.Entidades;
using System;
using System.Collections.Generic;

namespace Agrobanco.SAR.LogicaNegocio
{
    public class ParametroLogica
    {
        private readonly ParametroDatos parametroDatos;     

        public ParametroLogica()
        {
            parametroDatos = new ParametroDatos();
        }
        public List<ParametroBE> ObtenerListaParametro(int Grupo)
        {
            return parametroDatos.ListarParametro(Grupo);
        }

        public List<ParametroBE> ObtenerListarParametroGrupo(string Grupo)
        {
            return parametroDatos.ListarParametroGrupo(Grupo);
        }

        public List<ParametroBE> PreguntasFrecuentes(string Grupo)
        {
            return parametroDatos.ListarPreguntasFrecuentes(Grupo);
        }
    }
}
