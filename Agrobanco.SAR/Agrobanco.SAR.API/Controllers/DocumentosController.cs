﻿using Agrobanco.SAR.API.Models;
using Agrobanco.SAR.Comun;
using Agrobanco.SAR.Comun.GeneradorDocumento;
using Agrobanco.SAR.Entidades;
using Agrobanco.SAR.Entidades.CNA_Minuta;
using Agrobanco.SAR.LogicaNegocio;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.SAR.API.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/documentos")]
    [ApiController]
    public class DocumentosController : ControllerBase
    {

        private readonly ParametroLogica parametroLogica;
        private readonly RequerimientoLogica requerimientoLogica;
        private readonly CNALogica cnaLogica;

        public DocumentosController()
        {
            parametroLogica = new ParametroLogica();
            requerimientoLogica = new RequerimientoLogica();
            cnaLogica = new CNALogica();
        }

        [HttpPost("generador")]
        [ProducesResponseType(200, Type = typeof(string))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        public IActionResult BeneficioLey30893ultimo([Required][FromHeader(Name = "Token")] string token, [FromBody] LaserFicheBE request)
        {
            try
            {
                GeneradorDocumento oGeneradorDocumento = new GeneradorDocumento();
                Parametro oParametro = new Parametro();
                RequerimientoBE oRequerimientoBE = new RequerimientoBE();

                oRequerimientoBE.NroReclamo = request.NroReclamo;
                oParametro.Grupo = 1000;
                var listParametro = parametroLogica.ObtenerListaParametro(oParametro.Grupo);
                var oRequerimientoDetalle = requerimientoLogica.RequerimientoDetalle(oRequerimientoBE);
                if (oRequerimientoDetalle.CodTipoRequerimiento == ConstantesTipoRequerimiento.Solicitud)
                {
                    if (!string.IsNullOrEmpty(request.EstadoCredito))
                        oRequerimientoDetalle.EstadoCredito = request.EstadoCredito;

                    if (request.SaldoCredito!=null)
                        oRequerimientoDetalle.SaldoCredito = request.SaldoCredito;

                    if (!string.IsNullOrEmpty(request.NroCredito))
                        oRequerimientoDetalle.NroCredito = request.NroCredito;

                }
                var listaResultado = oGeneradorDocumento.Generador(request.LaserficheID, oRequerimientoDetalle, listParametro);

                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost("generador-gestion-cna")]
        [ProducesResponseType(200, Type = typeof(string))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        public async Task<IActionResult> GeneradorGestionCNA([Required][FromHeader(Name = "Token")] string token, [FromBody] LaserFicheBE request)
        {
            try
            {
                GeneradorDocumento oGeneradorDocumento = new GeneradorDocumento();
                Parametro oParametro = new Parametro();
                CNADetalleResponse oCNADetalleResponse = new CNADetalleResponse();
                CNADetalleRequest oCNADetalleRequest = new CNADetalleRequest();
                oCNADetalleRequest.IdEmision = request.IdEmision;
                oCNADetalleRequest.Perfil = request.Perfil;
                oParametro.Grupo = 1000;
                var listParametro = parametroLogica.ObtenerListaParametro(oParametro.Grupo);
                oCNADetalleResponse = await cnaLogica.ObtenerPorNumeroDocumento(oCNADetalleRequest);
                
                var listaResultado = oGeneradorDocumento.GeneradorGestionCNA(request.LaserficheID, oCNADetalleResponse, listParametro);

                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
