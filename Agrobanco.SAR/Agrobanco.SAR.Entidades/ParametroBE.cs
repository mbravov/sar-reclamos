﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades
{
    public class ParametroBE
    {
        public int? CodParametro { get; set; }
        public int? Parametro { get; set; }
        public int? Grupo { get; set; }
        public string? Descripcion { get; set; }
        public string? Valor { get; set; }
        public string? Referencia { get; set; }
        public string? Referencia2 { get; set; }
    }
}
