import { Injectable } from '@angular/core';
import { AppService } from '../app.service';
import { SecurityService } from './security.service';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CnaMinutaGarantiaService extends AppService {

  constructor(_securityService : SecurityService) {
    super(_securityService);
  }

  derivar<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${environment.UrlBase_SARAPI}/ocm/derivar`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }
  rechazar<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${environment.UrlBase_SARAPI}/ocm/rechazar`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }
}
