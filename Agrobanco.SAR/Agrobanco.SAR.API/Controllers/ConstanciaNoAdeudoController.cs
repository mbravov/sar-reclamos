﻿using Agrobanco.SAR.API.Models;
using Agrobanco.SAR.Control.Filters;
using Agrobanco.SAR.Entidades.ConstanciaNoAdeudo;
using Agrobanco.SAR.LogicaNegocio;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.SAR.API.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/constancia-no-adeudo")]
    [ApiController]
    public class ConstanciaNoAdeudoController : ControllerBase
    {
        private readonly ConstanciaNoAdeudoLogica constanciaNoAdeudoLogica;
        public ConstanciaNoAdeudoController()
        {
            constanciaNoAdeudoLogica = new ConstanciaNoAdeudoLogica();

        }

        [HttpPost("listar")]
        [ProducesResponseType(200, Type = typeof(ConstanciaNoAdeudoResponse))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult ConstanciaNoAdeudo([Required][FromHeader(Name = "Token")] string token, [FromBody] ConstanciaNoAdeudoRequest oConstanciaNoAdeudoRequest)
        {
            try
            {

                var listaResultado = constanciaNoAdeudoLogica.Obtener(oConstanciaNoAdeudoRequest);

                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
