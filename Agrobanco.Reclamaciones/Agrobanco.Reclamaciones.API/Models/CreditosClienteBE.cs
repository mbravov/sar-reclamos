﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.Reclamaciones.API.Models
{
    public class CreditosClienteBE
    {
		public int CodCredito { get; set; }
		public string CodCred { get; set; }
		public decimal Saldo { get; set; }
		public string Moneda { get; set; }
		public string Estado { get; set; }
		public string FechaPago { get; set; }
		public string CodCliente { get; set; }
		public string TipoIDN { get; set; }
		public string Identificacion { get; set; }
		public string NomCliente { get; set; }
	}
}
