﻿using Agrobanco.Control.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.Control.Interface
{
    public interface IAccessControl
    {
       AccessDTO generateToken(AccessDTO usuariodto, string appkey, string appcode, ref string token);
    }
}