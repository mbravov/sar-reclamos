﻿using Agrobanco.SAR.Comun;
using Agrobanco.SAR.Comun.Laserfiche;
using Agrobanco.SAR.Datos.SqlServer.Acceso;
using Agrobanco.SAR.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net.Mime;
using System.Text;

namespace Agrobanco.SAR.Datos
{
    public class ConsultaDatos
    {
        public string cadenaConexion;


        public List<ConsultaBE> ConsultasListar(ConsultaBE oBE)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            ConsultaBE objConsulta = null;
            List<ConsultaBE> lstConsulta = new List<ConsultaBE>();

            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_CONSULTA_OBTENER]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;                       

                        cmd.Parameters.Add("@fecharegistroini", SqlDbType.VarChar).Value = oBE.FechaRegistroIni == null ? string.Empty : oBE.FechaRegistroIni;
                        cmd.Parameters.Add("@fecharegistrofin", SqlDbType.VarChar).Value = oBE.FechaRegistroFin == null ? string.Empty : oBE.FechaRegistroFin;                       

                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    objConsulta = new ConsultaBE();
                                    if (!Convert.IsDBNull(reader["CodConsulta"])) objConsulta.CodConsulta = Convert.ToString(reader["CodConsulta"]);
                                    if (!Convert.IsDBNull(reader["FechaRegistro"])) objConsulta.FechaRegistro = reader.GetDateTime("FechaRegistro");
                                    if (!Convert.IsDBNull(reader["NroDocumento"])) objConsulta.NroDocumento = Convert.ToString(reader["NroDocumento"]);
                                    if (!Convert.IsDBNull(reader["NombresCompletos"])) objConsulta.NombresCompletos = Convert.ToString(reader["NombresCompletos"]);                                   
                                    if (!Convert.IsDBNull(reader["CorreoElectronico"])) objConsulta.CorreoElectronico = Convert.ToString(reader["CorreoElectronico"]);
                                    if (!Convert.IsDBNull(reader["Celular"])) objConsulta.Celular = Convert.ToString(reader["Celular"]);
                                    if (!Convert.IsDBNull(reader["TipoDocumentoDescripcion"])) objConsulta.TipoDocumentoDescripcion = Convert.ToString(reader["TipoDocumentoDescripcion"]);

                                    lstConsulta.Add(objConsulta);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();                          
                            throw (ex);
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw (ex);
            }

            return lstConsulta;
        }


        public ConsultaBE ConsultaDetalle(ConsultaBE oBE)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            ConsultaBE objConsulta = null;            

            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_CONSULTA_OBTENER_DETALLE]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@CodConsulta", SqlDbType.VarChar).Value = oBE.CodConsulta;
                        cmd.Parameters.Add("@CodTabla", SqlDbType.VarChar).Value = oBE.CodTabla;

                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    objConsulta = new ConsultaBE();
                                    if (!Convert.IsDBNull(reader["CodConsulta"])) objConsulta.CodConsulta = Convert.ToString(reader["CodConsulta"]);
                                    if (!Convert.IsDBNull(reader["FechaRegistro"])) objConsulta.FechaRegistro = reader.GetDateTime("FechaRegistro");
                                    if (!Convert.IsDBNull(reader["NroDocumento"])) objConsulta.NroDocumento = Convert.ToString(reader["NroDocumento"]);
                                    if (!Convert.IsDBNull(reader["NombresCompletos"])) objConsulta.NombresCompletos = Convert.ToString(reader["NombresCompletos"]);
                                    if (!Convert.IsDBNull(reader["CorreoElectronico"])) objConsulta.CorreoElectronico = Convert.ToString(reader["CorreoElectronico"]);
                                    if (!Convert.IsDBNull(reader["Celular"])) objConsulta.Celular = Convert.ToString(reader["Celular"]);
                                    if (!Convert.IsDBNull(reader["Direccion"])) objConsulta.Direccion = Convert.ToString(reader["Direccion"]);
                                    if (!Convert.IsDBNull(reader["Descripcion"])) objConsulta.Descripcion = Convert.ToString(reader["Descripcion"]);
                                 

                                    RecursoDatos oRecursoDatos = new RecursoDatos();
                                    RecursoBE oRecursoBE = new RecursoBE();
                                    oRecursoBE.CodTabla = ConstantesTabla.Consultas;
                                    oRecursoBE.ReferenciaID = Convert.ToInt32(objConsulta.CodConsulta);
                                    List<RecursoBE> lstRecursoBE = oRecursoDatos.RecursoObtener(oRecursoBE);

                                    if (lstRecursoBE != null && lstRecursoBE.Count > 0)
                                    {
                                        string LaserFicheAnidados = string.Empty;
                                        foreach (var item in lstRecursoBE)
                                        {
                                            LaserFicheAnidados += "|" + item.LaserficheID;
                                        }
                                        LaserFicheAnidados = LaserFicheAnidados.Substring(1, LaserFicheAnidados.Length - 1);
                                        objConsulta.LaserFicheConsultas = LaserFicheAnidados;
                                    }

                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            throw (ex);
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw (ex);
            }

            return objConsulta;
        }

        public bool ConsultaRegistrar(ConsultaBE oBE, string webRootPath)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            //int CodConsulta = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {

                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_CONSULTA_REGISTRAR]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                      
                        cmd.Parameters.Add("@Tipo_Documento", SqlDbType.Int).Value = oBE.TipoDocumento;
                        cmd.Parameters.Add("@NroDocumento", SqlDbType.VarChar).Value = oBE.NroDocumento;
                        cmd.Parameters.Add("@NombresCompletos", SqlDbType.VarChar).Value = oBE.NombresCompletos;
                        cmd.Parameters.Add("@Direccion", SqlDbType.VarChar).Value = oBE.Direccion;
                        cmd.Parameters.Add("@CorreoElectronico", SqlDbType.VarChar).Value = oBE.CorreoElectronico;
                        cmd.Parameters.Add("@Celular", SqlDbType.VarChar).Value = oBE.Celular;
                        cmd.Parameters.Add("@Descripcion", SqlDbType.VarChar).Value = oBE.Descripcion;
                        cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = oBE.Estado;
                        cmd.Parameters.Add("@UsuarioRegistro", SqlDbType.VarChar).Value = oBE.UsuarioRegistro;

                        SqlParameter paramId = new SqlParameter("@CodConsulta", SqlDbType.Int);
                        paramId.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(paramId);

                        int rowsAffected = cmd.ExecuteNonQuery();
                        if (rowsAffected > 0)
                        {
                            oBE.CodConsulta = Convert.ToString(cmd.Parameters["@CodConsulta"].Value);

                        }

                        EnvioCorreo objEnvioCorreo = new EnvioCorreo();
                        string cuerpocorreo = EnvioCorreoCuerpo.CuerpoCorreoConsulta(oBE, webRootPath);

                        List<string> listaCorreo = new List<string>();
                        listaCorreo.Add(ApplicationKeys.CorreoBuzonAgrobancoConsultas);

                        List<string> listaCorreocopia = new List<string>();
                       

                        //cmd.ExecuteNonQuery();

                        LaserficheProxy laserfiche = new LaserficheProxy();
                        DocumentoModel DocumentoModelRequest = new DocumentoModel();
                        DocumentoModel DocumentoModelResponse = new DocumentoModel();

                        DocumentoModelRequest.Folder = $@"\{ApplicationKeys.FolderConsultaLaserfiche}\{"Consulta"}" + "_" + oBE.CodConsulta;
                       
                        //DocumentoModelRequest.Folder = ApplicationKeys.FolderConsultaLaserfiche + "/" + "Consulta" + "_" + oBE.CodConsulta;
                        List<Documento> lstDocumento = new List<Documento>();

                        int item = 1;
                        string base64 = string.Empty;
                        if (!string.IsNullOrEmpty(oBE.Archivo1))
                        {
                            base64 = string.Empty;
                            base64 = oBE.Archivo1.Split(',')[1];

                            Documento oDocumento = new Documento();
                            oDocumento.Extension = System.IO.Path.GetExtension(oBE.NombreArchivo1).Split('.')[1];
                            oDocumento.ArchivoBytes = base64;
                            oDocumento.Nombre = oBE.CodConsulta + "-ARCHIVO" + item.ToString().PadLeft(2, '0');
                            oDocumento.Folder = DocumentoModelRequest.Folder;
                            lstDocumento.Add(oDocumento);
                            item++;
                        }

                        if (!string.IsNullOrEmpty(oBE.Archivo2))
                        {
                            base64 = string.Empty;
                            base64 = oBE.Archivo2.Split(',')[1];

                            Documento oDocumento = new Documento();
                            oDocumento.Extension = System.IO.Path.GetExtension(oBE.NombreArchivo2).Split('.')[1];
                            oDocumento.ArchivoBytes = base64;
                            oDocumento.Nombre = oBE.CodConsulta + "-ARCHIVO" + item.ToString().PadLeft(2, '0');
                            oDocumento.Folder = DocumentoModelRequest.Folder;
                            lstDocumento.Add(oDocumento);
                            item++;
                        }


                        if (lstDocumento.Count == 0)
                        {
                            objEnvioCorreo.EnviarCorreo(listaCorreo, listaCorreocopia, cuerpocorreo, ApplicationKeys.CorreoAsuntoRegistrarConsulta);
                        }

                        if (lstDocumento.Count > 0)
                        {
                            DocumentoModelRequest.ListaDocumentos = lstDocumento;
                            DocumentoModelResponse = laserfiche.UploadListaDocumento(DocumentoModelRequest);

                            RecursoDatos oRecursoDatos = null;
                            RecursoBE oRecursoBE = null;
                            foreach (var image in DocumentoModelResponse.ListaDocumentos)
                            {
                                if (image.ResultadoOK)
                                {
                                    oRecursoDatos = new RecursoDatos();
                                    oRecursoBE = new RecursoBE();

                                    oRecursoBE.CodTabla = ConstantesTabla.Consultas;
                                    oRecursoBE.ReferenciaID = Convert.ToInt32(oBE.CodConsulta);
                                    oRecursoBE.LaserficheID = image.CodigoLaserfiche;
                                    oRecursoBE.Estado = true;
                                    oRecursoBE.UsuarioRegistro = oBE.UsuarioRegistro;
                                    oRecursoBE.Descripcion = image.Nombre;
                                    Boolean resultad = oRecursoDatos.RegistrarTrans(oRecursoBE, conn,null);


                                    if (!resultad)
                                        throw new Exception("Se presento un error al intentar registrar los archivos adjuntos");
                                   
                                }
                                else
                                {
                                    throw new Exception("Se presento un error al subir el archivo");
                                }
                            }                                                     

                            objEnvioCorreo.EnviarCorreoConsulta(listaCorreo, listaCorreocopia, cuerpocorreo, ApplicationKeys.CorreoAsuntoRegistrarConsulta, lstDocumento);
                        }


                    }
                }
            }
            catch (Exception ex)
            {

                throw (ex);
            }

            return false;
        }

    }
}
