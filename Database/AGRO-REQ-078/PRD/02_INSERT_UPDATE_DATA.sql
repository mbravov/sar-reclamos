USE DBAgrSAR
GO

DELETE FROM Recurso where CodTabla in (8,9,10)
GO

DELETE FROM dbo.Parametro WHERE Grupo = 1400 AND Parametro IN (3, 4, 5, 6)
GO
IF NOT EXISTS(SELECT TOP 1 * FROM dbo.Parametro WHERE Grupo = 1400 AND Descripcion = 'GAR')
BEGIN
	
	INSERT INTO dbo.Parametro
	(Grupo, Parametro, Descripcion, Valor, Referencia, Referencia2, Orden, Estado) 
	VALUES
	(1400, 3, 'GAR', 'Garant�as', 3, NULL, 3, 1);
END
GO

IF NOT EXISTS(SELECT TOP 1 CodParametro FROM dbo.Parametro WHERE Grupo = 1400 AND Descripcion = 'LEG')
BEGIN
	
	INSERT INTO dbo.Parametro
	(Grupo, Parametro, Descripcion, Valor, Referencia, Referencia2, Orden, Estado) 
	VALUES
	(1400, 4, 'LEG', 'Legal', 4, NULL, 4, 1)
END
GO

IF NOT EXISTS(SELECT TOP 1 CodParametro FROM dbo.Parametro WHERE Grupo = 1400 AND Descripcion = 'ADMAG')
BEGIN
	
	INSERT INTO dbo.Parametro
	(Grupo, Parametro, Descripcion, Valor, Referencia, Referencia2, Orden, Estado) 
	VALUES
	(1400, 5, 'ADMAG', 'Administrador de Agencia', 5, NULL, 5, 1)
END
GO

IF NOT EXISTS(SELECT TOP 1 CodParametro FROM dbo.Parametro WHERE Grupo = 1400 AND Descripcion = 'UDA')
BEGIN
	
	INSERT INTO dbo.Parametro
	(Grupo, Parametro, Descripcion, Valor, Referencia, Referencia2, Orden, Estado) 
	VALUES
	(1400, 6, 'UDA', 'Usuario de Agencia', 6, NULL, 6, 1)
END
GO

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Parametro = 1500 OR Grupo = 1500)
BEGIN
	DELETE FROM dbo.Parametro WHERE Parametro = 1500 OR Grupo = 1500
END
GO

INSERT INTO dbo.Parametro
(Grupo, Parametro, Descripcion, Valor, Referencia, Referencia2, Orden, Estado) 
VALUES 
(0, 1500, 'Tipo Procesamiento CNA', NULL, NULL, NULL, 0, 1),
(1500, 1, 'Manual', NULL, NULL, NULL, 1, 1),
(1500, 2, 'Autom�tico a Garant�as', NULL, NULL, NULL, 2, 1),
(1500, 3, 'Autom�tico a Adm. de Age.', NULL, NULL, NULL, 3, 1);

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Parametro = 1600 OR Grupo = 1600)
BEGIN
	DELETE FROM Parametro WHERE Parametro = 1600 OR Grupo = 1600
END
GO

INSERT INTO dbo.Parametro
(Grupo, Parametro, Descripcion, Valor, Referencia, Referencia2, Orden, Estado) 
VALUES
(0, 1600, 'Tipo Flujo', NULL, NULL, NULL, 0, 1),
(1600, 1, 'CNA', NULL, NULL, NULL, 1, 1),
(1600, 2, 'CR', NULL, NULL, NULL, 2, 1);

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Parametro = 1700 OR Grupo = 1700)
BEGIN
	DELETE FROM Parametro WHERE Parametro = 1700 OR Grupo = 1700
END
GO

INSERT INTO dbo.Parametro
(Grupo, Parametro, Descripcion, Valor, Referencia, Referencia2, Orden, Estado) 
VALUES
(0, 1700, 'Medio de Notificaci�n', NULL, NULL, NULL, 0, 1),
(1700, 1, 'F�sico', NULL, NULL, NULL, 1, 1),
(1700, 2, 'Email', NULL, NULL, NULL, 2, 1),
(1700, 3, 'WhatsApp', NULL, NULL, NULL, 3, 1);

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Parametro = 1800 OR Grupo = 1800)
BEGIN
	DELETE FROM Parametro WHERE Parametro = 1800 OR Grupo = 1800
END
GO

INSERT INTO dbo.Parametro
(Grupo, Parametro, Descripcion, Valor, Referencia, Referencia2, Orden, Estado) 
VALUES
(0, 1800, 'Tipo Recurso Emisi�n', NULL, NULL, NULL, 0, 1),
(1800, 1, 'Carta de No Adeudo', NULL, NULL, NULL, 1, 1),
(1800, 2, 'Carta de No Adeudo de ADM. AGE.', NULL, NULL, NULL, 2, 1),
(1800, 3, 'Carta de Respuesta', NULL, NULL, NULL, 3, 1),
(1800, 4, 'Documentos Evaluados por Garant�a', NULL, NULL, NULL, 4, 1),
(1800, 5, 'Minuta', NULL, NULL, NULL, 5, 1),
(1800, 6, 'Carta de Entrega Minuta', NULL, NULL, NULL, 6, 1),
(1800, 7, 'Cargo de Entrega', NULL, NULL, NULL, 7, 1),

(1800, 8, 'Carta de No Adeudo', 'Documento historico de reapertura', NULL, NULL, 1, 1),
(1800, 9, 'Carta de No Adeudo de ADM. AGE.', 'Documento historico de reapertura', NULL, NULL, 2, 1),
(1800, 10, 'Carta de Respuesta', 'Documento historico de reapertura', NULL, NULL, 3, 1),
(1800, 11, 'Documentos Evaluados por Garant�a', NULL, NULL, NULL, 4, 1),
(1800, 12, 'Minuta', 'Documento historico de reapertura', NULL, NULL, 5, 1),
(1800, 13, 'Carta de Entrega Minuta', 'Documento historico de reapertura', NULL, NULL, 6, 1),
(1800, 14, 'Cargo de Entrega', 'Documento historico de reapertura', NULL, NULL, 7, 1);

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Parametro = 1900 OR Grupo = 1900)
BEGIN
	DELETE FROM Parametro WHERE Parametro = 1900 OR Grupo = 1900
END
GO

INSERT INTO dbo.Parametro
(Grupo, Parametro, Descripcion, Valor, Referencia, Referencia2, Orden, Estado) 
VALUES
(0, 1900, 'Motivo Rechazo CNA - Garant�as', NULL, NULL, NULL, 0, 1),
(1900, 1, 'Cr�dito mantiene gastos judiciales', NULL, NULL, NULL, 1, 1),
(1900, 2, 'Hipoteca garantiza cr�ditos de terceros', NULL, NULL, NULL, 2, 1),
(1900, 3, 'Cliente garantiza cr�ditos a terceros', NULL, NULL, NULL, 3, 1),
(1900, 4, 'Otros', NULL, NULL, NULL, 4, 1);

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Parametro = 2000 OR Grupo = 2000)
BEGIN
	DELETE FROM Parametro WHERE Parametro = 2000 OR Grupo = 2000
END
GO

INSERT INTO dbo.Parametro
(Grupo, Parametro, Descripcion, Valor, Referencia, Referencia2, Orden, Estado) 
VALUES
(0, 2000, 'Tipo Estado Emisi�n', NULL, NULL, NULL, 0, 1),
(2000, 1, 'Estado General', 'G', NULL, NULL, 1, 1),
(2000, 2, 'Estado de Perfil', 'P', NULL, NULL, 2, 1)
GO

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Parametro = 2100 OR Grupo = 2100)
BEGIN
	DELETE FROM Parametro WHERE Parametro = 2100 OR Grupo = 2100
END
GO

INSERT INTO dbo.Parametro
(Grupo, Parametro, Descripcion, Valor, Referencia, Referencia2, Orden, Estado) 
VALUES
(0, 2100, 'Tipo Filtro', NULL, NULL, NULL, 0, 1),
(2100, 1, 'Perfil', NULL, NULL, NULL, 1, 1),
(2100, 2, 'Tipo Procesamiento', NULL, NULL, NULL, 2, 1)
GO

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Parametro = 2200 OR Grupo = 2200)
BEGIN
	DELETE FROM Parametro WHERE Parametro = 2200 OR Grupo = 2200
END
GO

INSERT INTO dbo.Parametro
(Grupo, Parametro, Descripcion, Valor, Referencia, Referencia2, Orden, Estado) 
VALUES
(0, 2200, 'Tipo Evento', NULL, NULL, NULL, 0, 1),
(2200, 1, 'Registro Emisi�n', NULL, NULL, NULL, 1, 1),
(2200, 2, 'Registro Derivaci�n', NULL, NULL, NULL, 2, 1),
(2200, 3, 'Actualizaci�n Emisi�n', NULL, NULL, NULL, 3, 1),
(2200, 4, 'Actualizaci�n Derivaci�n', NULL, NULL, NULL, 4, 1),
(2200, 5, 'Notificaci�n', NULL, NULL, NULL, 5, 1),
(2200, 6, 'Registro Comentario Emision', NULL, NULL, NULL, 5, 1);

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Parametro = 2300 OR Grupo = 2300)
BEGIN
	DELETE FROM Parametro WHERE Parametro = 2300 OR Grupo = 2300
END
GO

INSERT INTO dbo.Parametro
(Grupo, Parametro, Descripcion, Valor, Referencia, Referencia2, Orden, Estado) 
VALUES
(0, 2300, 'Tipo Procesamiento Evento', NULL, NULL, NULL, 0, 1),
(2300, 1, 'Manual', NULL, NULL, NULL, 1, 1),
(2300, 2, 'Autom�tico', NULL, NULL, NULL, 2, 1);

GO

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Parametro = 2400 OR Grupo = 2400)
BEGIN
	DELETE FROM dbo.Parametro WHERE Parametro = 2400 OR Grupo = 2400
END
GO

INSERT INTO dbo.Parametro
(Grupo, Parametro, Descripcion, Valor, Referencia, Referencia2, Orden, Estado) 
VALUES 
(0, 2400, 'Botones Acciones', NULL, NULL, NULL, 0, 1),
(2400, 1, 'Derivar', 'abrir_popup()', '1', '1', 1, 1),
(2400, 2, 'Agregar Comentario', 'AgregarComentarioAccionDerivacion()', '4', '2', 2, 1),
(2400, 3, 'Rechazar', 'rechazar()', '2', '1', 3, 1),
(2400, 4, 'Enviar al UDA','enviarUDA()','3','1',4,1),
(2400, 5, 'Derivar a Legal','popup_derivar_legal()','7','1',4,1),
(2400, 6, 'Subir Archivo','subirCartaNoAdeudo()','100','5',4,1),
(2400, 7, 'Eliminar CNA','eliminarArchivoLaserFiche()','104','5',4,1),
(2400, 8, 'Subir Archivo','subirCartaRespuesta()','101','5',4,1),
(2400, 9, 'Eliminar CR','eliminarArchivoLaserFiche()','107','5',4,1),
--(2400, 10, 'Agregar Comentario','AgregarComentarioAccionDerivacion()','106','2',8,1),
(2400, 11, 'Derivar','popup_derivar_ADMAGE()','109','1',9,1),
(2400, 12, 'Subir Minuta Legal','subirMinuta()','110','5',9,1),
(2400, 13, 'Subir Carta Entrega Legal','subirCartaEntrega()','110','6',9,1),
(2400, 14, 'Derivar', 'popup_derivar_usuario_agencia()', '112', '1', 1, 1),
(2400, 15, 'Enviar para su notificaci�n', 'enviarParaNotificacion()', '121', '1', 15, 1),
(2400, 16, 'Confirmar Entrega', 'confirmarCargoEntrega()', '8', '1', 15, 1),
(2400, 17, 'Finalizar', 'finalizarAtencion()', '9', '1', 15, 1),
(2400, 18, 'Reaperturar', 'reaperturar()', '10', '1', 15, 1)
GO

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Grupo = 200 AND Parametro IN (8, 9, 10))
BEGIN
	DELETE FROM dbo.Parametro WHERE Grupo = 200 AND Parametro IN (8, 9, 10)
END
GO

/*Nuevas tablas de Recursos*/
INSERT INTO dbo.Parametro
(Grupo, Parametro, Descripcion, Valor, Referencia, Referencia2, Orden, Estado) 
VALUES
(200, 8, 'Recurso Emisi�n', NULL, NULL, NULL, 8, 1),
(200, 9, 'Comentario Emisi�n', NULL, NULL, NULL, 9, 1),
(200, 10, 'Firma Usuario', NULL, NULL, NULL, 10, 1);
GO

 

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Parametro = 2500 OR Grupo = 2500)
BEGIN
	DELETE FROM dbo.Parametro WHERE Parametro = 2500 OR Grupo = 2500
END
GO

INSERT INTO dbo.Parametro
(Grupo, Parametro, Descripcion, Valor, Referencia, Referencia2, Orden, Estado) 
VALUES 
(0, 2500, 'Tipo Accione', NULL, NULL, NULL, 0, 1),
(2500, 1, 'Botones', NULL, NULL, NULL, 1, 1),
(2500, 2, 'File',NULL, NULL, NULL, 2, 1) 
GO

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Parametro in (4,5,6) AND Grupo = 1200)
BEGIN
	DELETE FROM dbo.Parametro WHERE Parametro in (4,5,6) AND Grupo = 1200
END
GO

INSERT INTO dbo.Parametro
(Grupo, Parametro, Descripcion, Valor, Referencia, Referencia2, Orden, Estado) 
VALUES 
(1200, 4, 'Gesti�n CNA', NULL, NULL, NULL, 4, 1),
(1200, 5, 'Minuta', NULL, NULL, NULL, 5, 1) ,
(1200, 6, 'Carta Respuesta', NULL, NULL, NULL, 6, 1) 
GO


IF EXISTS(SELECT * FROM dbo.Parametro WHERE Parametro in (13,14,15,16,17) AND Grupo = 1000)
BEGIN
	DELETE FROM dbo.Parametro WHERE Parametro in (13,14,15,16,17) AND Grupo = 1000
END
GO

INSERT INTO dbo.Parametro
(Grupo, Parametro, Descripcion, Valor, Referencia, Referencia2, Orden, Estado) 
VALUES 
(1000, 13, '[X_NOMBRES_APELLIDOS_X]', 'Nombres', '2', 'Tipo de documento', 1, 1),
(1000, 14, '[X_NRO_DOCUMENTO_X]','NumeroDocumento','2' , 'N�mero documento', 2, 1),
(1000, 15, '[X_TIPO_DOCUMENTO_X]','TipoDocumento', '2', 'Tipo de documento', 2, 1),
(1000, 16, '[X_MOTIVO_RECHAZO_X]','MotivoRechazo', '2', 'Motivo Rechazo', 2, 1),
(1000, 17, '[X_FECHA_RECHAZO_X]','FechaRechazo', '2', 'Fecha Rechazo', 2, 1)
GO

DELETE FROM [dbo].[EstadoEmision]

INSERT INTO [dbo].[EstadoEmision] ([IdEstado],[TipoEstado],[Perfil],[Descripcion],[FechaRegistro],[UsuarioRegistro])
VALUES 
(1,'G',NULL,'PENDIENTE OCM',GETDATE(),'USER'),
(2,'G',NULL,'EN PROCESO OCM',GETDATE(),'USER'),
(3,'G',NULL,'PENDIENTE GARANT�AS',GETDATE(),'USER'),
(4,'G',NULL,'EN PROCESO GARANT�AS',GETDATE(),'USER'),
(5,'G',NULL,'PENDIENTE LEGAL',GETDATE(),'USER'),
(6,'G',NULL,'EN PROCESO LEGAL',GETDATE(),'USER'),
(7,'G',NULL,'PENDIENTE ADM',GETDATE(),'USER'),
(8,'G',NULL,'EN PROCESO ADM',GETDATE(),'USER'),
(9,'G',NULL,'PENDIENTE UDA',GETDATE(),'USER'),
(41, 'G', NULL, 'EN PROCESO UDA', GETDATE(), 'USER'),
(10,'G',NULL,'POR NOTIFICAR',GETDATE(),'USER'),
(11,'G',NULL,'NOTIFICADO',GETDATE(),'USER'),
(12,'G',NULL,'ATENDIDO',GETDATE(),'USER'),
(13,'G',NULL,'OBSERVADO',GETDATE(),'USER'),
(14,'P','OCM','PENDIENTE',GETDATE(),'USER'),
(15,'P','OCM','EN PROCESO',GETDATE(),'USER'),
(16,'P','OCM','DERIVADO A GARANT�AS',GETDATE(),'USER'),
(17,'P','OCM','DERIVADO A ADM. AGE',GETDATE(),'USER'),
(18,'P','OCM','RECHAZADO',GETDATE(),'USER'),
(19,'P','OCM','DERIVADO A UDA',GETDATE(),'USER'),
(20,'P','OCM','POR NOTIFICAR',GETDATE(),'USER'),
(21,'P','OCM','NOTIFICADO',GETDATE(),'USER'),
(22,'P','OCM','ATENDIDO',GETDATE(),'USER'),
(23,'P','OCM','OBSERVADO',GETDATE(),'USER'),
(24,'P','GAR','PENDIENTE',GETDATE(),'USER'),
(25,'P','GAR','EN PROCESO',GETDATE(),'USER'),
(26,'P','GAR','RECHAZADO',GETDATE(),'USER'),
(27,'P','GAR','DERIVADO A LEGAL',GETDATE(),'USER'),
(28,'P','LEG','PENDIENTE',GETDATE(),'USER'),
(29,'P','LEG','EN PROCESO',GETDATE(),'USER'),
(30,'P','LEG','RECHAZADO',GETDATE(),'USER'),
(31,'P','LEG','DERIVADO A ADM. AGE.',GETDATE(),'USER'),
(32,'P','ADMAG','PENDIENTE',GETDATE(),'USER'),
(33,'P','ADMAG','EN PROCESO',GETDATE(),'USER'),
(34,'P','ADMAG','RECHAZADO',GETDATE(),'USER'),
(35,'P','ADMAG','DERIVADO A UDA',GETDATE(),'USER'),
(36,'P','UDA','PENDIENTE',GETDATE(),'USER'),
(37,'P','UDA','EN PROCESO',GETDATE(),'USER'),
(38,'P','UDA','POR NOTIFICAR',GETDATE(),'USER'),
(39,'P','UDA','NOTIFICADO',GETDATE(),'USER'),
(40,'P','UDA','RECHAZADO',GETDATE(),'USER')
GO

DELETE FROM ConfiguracionDerivacion

DELETE FROM FiltroAccion

DELETE FROM ConfiguracionAccion

DELETE FROM dbo.Accion

INSERT INTO Accion(IdAccion, TipoAccion, Descripcion) VALUES 
 (1, 1,'Derivar'),
 (2, 1,'Rechazar'),
 (3, 1,'Enviar al UDA'),
 (4, 1,'Agregar Comentario'),
 (5, 1,'Derivar a GAR'),
 (6, 1,'Derivar a ADMAGE'),
 (7, 1,'Derivar a Legal'),
 (8, 1,'Confimar Entrega'),
 (9, 1,'Finalizar'),
 (10, 1,'Reaperturar'),

 (100, 2,'Subir Carta de no adeudo'),
 (101, 3,'Subir Carta de respuesta'),
 (102, 4,'Subir Archivo Garantia'),
 (103, 5,'Descargar Archivo CNA'),
 (104, 6,'Eliminar CNA'),
 (105, 7,'Descargar Archivo Garantia'),
 --(106, 1,'Agregar Comentario config'),
 (107, 8,'Eliminar CR'),
 (108, 9,'Descargar CR'),
 (109, 1,'Derivar Administrador de Agencia'),
 (110, 10,'Seccion de Legal'),
 (111, 11,'descargar Minuta, CE'),
 (112, 1,'Derivar a Usuario de Agencia'),
 (121, 1, 'Enviar para su notificaci�n'),
 (122, 12,'Subir Cargo de entrega'),
 (123, 13,'Descargar Cargo de entrega'),
 (124, 14,'Eliminar Cargo de entrega'),
 (125, 15,'Ingresar Notificacion'),
 (127, 17,'Mostrar Notificacion'),
 (126, 16,'Descargar Archivo CNA AGE'),

 (90, 1, 'Procesamiento autom�tico'),
 (91, 1, 'Proc. Auto. Derivar OFN')
 GO

 /*Perfil OCM*/
 INSERT INTO ConfiguracionAccion (IdConfiguracionAccion, IdAccion, Perfil, IdEstado, Activo) VALUES 
 /*Estado PENDIENTE*/
 (101, 1,'OCM',14,1),
 (102,2,'OCM',14,1),
 (103,4,'OCM',14,1),
 (104,104,'OCM',14,1),
 (105,100,'OCM',14,1),
 
 /*Estado EN PROCESO*/
 (106,1,'OCM',15,1),
 (107,2,'OCM',15,1), 
 (108,4,'OCM',15,1),
 (109,100,'OCM',15,1),
 (110,104,'OCM',15,1),
 (146,107,'OCM',15,1), ---Eliminar CR

 /*Estado DERIVADO A GARANT�AS*/
 (111,103,'OCM',16,1),--'Descargar Archivo CNA'
 (151,105,'OCM',16,1),--'Descargar Archivo Garantia'
 (152,111,'OCM',16,1),--'descargar Minuta, CE'

 /*Estado DERIVADO A ADM. AGE*/
 (112,103,'OCM',17,1),
 
 /*Estado RECHAZADO*/
 (113,3,'OCM',18,1),
 (114,1,'OCM',18,1),
 (115,100,'OCM',18,1),
 (116,104,'OCM',18,1),
 (117,101,'OCM',18,1),
 (118,107,'OCM',18,1),
 
 /*Estado DERIVADO A UDA*/
 (119,108,'OCM',19,1),
 /*Estado DERIVADO A OFN*/
 

 /*Estado Por Notificar*/
 (120,8,'OCM',20,1),
 (121,103,'OCM',20,1),
 (122,105,'OCM',20,1),
 (123,108,'OCM',20,1),
 (124,111,'OCM',20,1),
 (125,122,'OCM',20,1),
 (126,124,'OCM',20,1),
 (148,127,'OCM',20,1), -- Mostar Notificaciones

  /*Estado Notificado*/
 (128,9,'OCM',21,1), 
 (129,123,'OCM',21,1),
 (130,103,'OCM',21,1),
 (131,105,'OCM',21,1),
 (132,108,'OCM',21,1),
 (133,111,'OCM',21,1),
 (149,127,'OCM',21,1), -- Mostar Notificaciones

 /*Estado Atendido*/
 (134,103,'OCM',22,1),
 (135,105,'OCM',22,1),
 (136,108,'OCM',22,1),
 (137,111,'OCM',22,1),
 (138,123,'OCM',22,1),
 
  
 


  /*Estado Observado*/
 (139,10, 'OCM',23,1),
 (140,103,'OCM',23,1),
 (141,105,'OCM',23,1),
 (142,108,'OCM',23,1),
 (143,111,'OCM',23,1),
 (144,123,'OCM',23,1)

 GO
 
 /*Perfil GAR*/
 INSERT INTO ConfiguracionAccion (IdConfiguracionAccion, IdAccion, Perfil, IdEstado, Activo) VALUES 
 /*Estado PENDIENTE*/
 (301,4,'GAR',24,1),-- agregar comentario
 (302,7,'GAR',24,1), --bot�n derivar a legal
 (303,2,'GAR',24,1), --bot�n rechazar
 (304,103,'GAR',24,1), -- descargar archivo CNA
 (305,102,'GAR',24,1), --subir archivo garantia

 /*Estado EN PROCESO*/
 (306,103,'GAR',25,1), -- descargar archivo CNA
 (307,2,'GAR',25,1),--bot�n rechazar
 (308,102,'GAR',25,1),--subir archivo garantia
 (309,7,'GAR',25,1), --bot�n derivar a legal
 (310, 4,'GAR',25,1),-- agregar comentario

 /*RECHAZADO*/
 (311,105,'GAR',26,1), --Descargar Archivo Garantia
 (312,103,'GAR',26,1),-- descargar archivo CNA

 /*DERIVADO A LEGAL*/
 (313,103,'GAR',27,1),-- descargar archivo CNA
 (314,105,'GAR',27,1) --Descargar Archivo Garantia
 GO

  /*Perfil LEG*/
 INSERT INTO ConfiguracionAccion (IdConfiguracionAccion, IdAccion, Perfil, IdEstado, Activo) VALUES 
 /*PENDIENTE*/
 (401,2,'LEG',28,1),--Rechazar
 (402,4,'LEG',28,1),--agregar comentario
 (403,109,'LEG',28,1),--Derivar Administrador de Agencia
 (404,110,'LEG',28,1), --Seccion de Legal
 (405,103,'LEG',28,1),--Descargar Archivo CNA
 (406,105,'LEG',28,1),--Descargar Archivo Garantia
 

 /*EN PROCESO*/
 (407,2,'LEG',29,1), --Rechazar
 (408,4,'LEG',29,1),--agregar comentario
 (409,109,'LEG',29,1),--Derivar Administrador de Agencia
 (410,110,'LEG',29,1),--Seccion de Legal
 (411,103,'LEG',29,1),--Descargar Archivo CNA 
 (412,105,'LEG',29,1),--Descargar Archivo Garantia
 
 /*RECHAZADO*/
 (413,103,'LEG',30,1),--Descargar Archivo CNA
 (414,105,'LEG',30,1),--Descargar Archivo Garantia
 (415,111,'LEG',30,1), -- Descargar Minuta, CE y mostrar F. Olva

 /*DERIVADO A ADM. AGE.*/
 (416,103,'LEG',31,1),--Descargar Archivo CNA
 (417,105,'LEG',31,1),--Descargar Archivo Garantia
 (418,111,'LEG',31,1) -- Descargar Minuta, CE y mostrar F. Olva
 GO
 
 /*Perfil ADMAG*/
 INSERT INTO ConfiguracionAccion 
 (IdConfiguracionAccion, IdAccion, Perfil, IdEstado, Activo) VALUES
 /*PENDIENTE*/
 (501, 2,	'ADMAG', 32,	1), -- bot�n Rechazar
 (502, 103,	'ADMAG', 32,	1), -- descargar CNA
 (503, 105,	'ADMAG', 32,	1), -- documentos evaluados GAR
 (504, 111,	'ADMAG', 32,	1), -- documentos: minuta, carta de entrega y fecha de entrega Olva
 (505, 112,	'ADMAG', 32,	1), -- bot�n derivar a UDA
 (506, 4, 'ADMAG', 32,	1), --agregar comentario
  
 /*EN PROCESO*/
 (507, 2,	'ADMAG', 33,	1),-- bot�n Rechazar
 (508, 103,	'ADMAG', 33,	1),-- descargar CNA
 (509, 105,	'ADMAG', 33,	1),-- documentos evaluados GAR
 (510, 111,	'ADMAG', 33,	1),-- documentos: minuta, carta de entrega y fecha de entrega Olva
 (511, 112,	'ADMAG', 33,	1),-- bot�n derivar a UDA
 (512, 4,	'ADMAG', 33,	1),--agregar comentario

  /*EN DERIVADO UDA */
 
 (513, 126,	'ADMAG', 35,	1), -- descargar CNA ADMAGE
 (514, 105,	'ADMAG', 35,	1), -- documentos evaluados GAR
 (515, 111,	'ADMAG', 35,	1), -- documentos: minuta, carta de entrega y fecha de entrega Olva  
 
     /*RECHAZADO*/
 (516,103,'ADMAG',34,1),--Descargar Archivo CNA
 (517,105,'ADMAG',34,1),--Descargar Archivo Garantia
 (518,111,'ADMAG',34,1) -- Descargar Minuta, CE y mostrar F. Olva

 GO
 
 /*PERFIL UDA*/
 INSERT INTO ConfiguracionAccion (IdConfiguracionAccion, IdAccion, Perfil, IdEstado, Activo) VALUES 
 /*PENDIENTE*/
 (601,2,'UDA',36,1),--bot�n Rechazar
 (602,108,'UDA',36,1),--Descargar CR
 (605, 4, 'UDA', 36, 1),--agregar comentario
 (607, 121, 'UDA', 36, 1),--bot�n enviar para su notificaci�n
 (609,126,'UDA',36,1),-- Descargar Archivo CNA
 (610,105,'UDA',36,1),-- Descargar Archivo Garantia
 (611,111,'UDA',36,1),-- Descargar Minuta
 (620,125,'UDA',36,1), -- Ingresar Notificaciones

 /*EN PROCESO*/
 (603, 2,'UDA',37,1),--bot�n Rechazar
 (604, 108, 'UDA', 37, 1),--Descargar CR
 (606, 4, 'UDA', 37, 1),--agregar comentario
 (608, 121, 'UDA', 37, 1),--bot�n enviar para su notificaci�n
 (612,126,'UDA',37,1),-- Descargar Archivo CNA ADM
 (613,105,'UDA',37,1),-- Descargar Archivo Garantia
 (614,111,'UDA',37,1),-- Descargar Minuta
 (621,125,'UDA',37,1), -- Ingresar Notificaciones

  /*Enviar para su notificaci�n*/
 (615, 8,'UDA',38,1),--bot�n confirmar Entrega
 (616, 108, 'UDA', 38, 1),--Descargar CR
 (617,126,'UDA',38,1),-- Descargar Archivo CNA
 (618,105,'UDA',38,1),-- Descargar Archivo Garantia
 (619,111,'UDA',38,1),-- Descargar Minuta
 (622,122,'UDA',38,1),-- Subir CE
 (623,124,'UDA',38,1), -- Eliminar CE
 (624,127,'UDA',38,1), -- Mostar Notificaciones

  /*ESTADO NOTIFICADO*/
(625,108,'UDA',39, 1),--Descargar CR
(626,126,'UDA',39,1),-- Descargar Archivo CNA
(627,105,'UDA',39,1),-- Descargar Archivo Garantia
(628,111,'UDA',39,1),-- Descargar Minuta 
(629,123,'UDA',39,1),-- Descargar CE
(630,127,'UDA',39,1), -- Mostar Notificaciones

  /*ESTADO RECHAZADO*/
(631,108,'UDA',40, 1),--Descargar CR
(632,126,'UDA',40,1),-- Descargar Archivo CNA
(633,105,'UDA',40,1),-- Descargar Archivo Garantia
(634,111,'UDA',40,1)-- Descargar Minuta 
 GO
 
 /*OTROS*/
 INSERT INTO ConfiguracionAccion (IdConfiguracionAccion, IdAccion, Perfil, IdEstado, Activo) VALUES 
 (900, 90, NULL, NULL, 1),
 (901, 91, NULL, NULL, 1)
 GO
 
 /*Perfil OCM*/
  -- OCM  => CAMBIAR A EN PROCESO SUBIR O ELIMINAR LA CARTA DE RESPUESTA
 INSERT INTO ConfiguracionDerivacion (IdConfiguracionAccion,GrupoFiltro,TipoProcesamiento, TipoEvento, Perfil,IdEstado) VALUES 
 (105,NULL,1,4,'OCM',15),
 (105,NULL,1,3,NULL,2)
 GO
 -- OCM  => CAMBIAR A EN PROCESO SUBIR O ELIMINAR LA CARTA DE RESPUESTA
 INSERT INTO ConfiguracionDerivacion (IdConfiguracionAccion,GrupoFiltro,TipoProcesamiento, TipoEvento, Perfil,IdEstado) VALUES 
 (110,NULL,1,4,'OCM',15),
 (110,NULL,1,3,NULL,2)
 GO
 -- OCM  => CAMBIAR A EN PROCESO SUBIR O ELIMINAR LA CARTA DE RESPUESTA
 INSERT INTO ConfiguracionDerivacion (IdConfiguracionAccion,GrupoFiltro,TipoProcesamiento, TipoEvento, Perfil,IdEstado) VALUES 
 (104,NULL,1,4,'OCM',15),
 (104,NULL,1,3,NULL,2)
 GO
 
 -- OCM  => CAMBIAR A EN PROCESO SUBIR UN COMENTARIO
 INSERT INTO ConfiguracionDerivacion (IdConfiguracionAccion,GrupoFiltro,TipoProcesamiento, TipoEvento, Perfil,IdEstado) VALUES 
 (108,NULL,1,4,'OCM',15),
 (108,NULL,1,3,NULL,2),
 (108,NULL,1,6,'OCM',NULL)
 GO

 -- OCM  => CAMBIAR A EN PROCESO SUBIR UN COMENTARIO cuando este en pendiente
 INSERT INTO ConfiguracionDerivacion (IdConfiguracionAccion,GrupoFiltro,TipoProcesamiento, TipoEvento, Perfil,IdEstado) VALUES 
 (103,NULL,1,4,'OCM',15),
 (103,NULL,1,3,NULL,2),
 (103,NULL,1,6,'OCM',15)
 GO
 
 INSERT INTO ConfiguracionDerivacion (IdConfiguracionAccion,GrupoFiltro,TipoProcesamiento, TipoEvento, Perfil,IdEstado) VALUES 
 (101,10,1,4,'OCM', 16),
 (101,10,1,2,'GAR', 24),
 (101,10,1,3, NULL, 3),
 (101,10,1,5,'GAR', 16),
 (101,10,1,6,'OCM', 16),
 (101,20,1,4, 'OCM', 17),
 (101,20,1,2, 'ADMAG', 32),
 (101,20,1,3, NULL, 7),
 (101,20,1,5,'ADMAG', 17),
 (101,20,1,6,'OCM', 32),
 
 (102,NULL,1,3, NULL,2),
 (102,NULL,1,4,'OCM',18),
 (102,NULL,1,6,'OCM',18), 

 (106,10,1,4,'OCM', 16),
 (106,10,1,2,'GAR', 24),
 (106,10,1,3, NULL, 3),
 (106,10,1,5,'GAR', 16),
 (106,10,1,6,'OCM', 16),
 (106,20,1,4,'OCM', 17),
 (106,20,1,2,'ADMAG', 32),
 (106,20,1,3, NULL, 7),
 (106,20,1,5,'ADMAG', 17),
 (106,20,1,6,'OCM', 32),

 (107,NULL,1,3, NULL,2),
 (107,NULL,1,4,'OCM',18),
 (107,NULL,1,6,'OCM',18),
 
 (113,null,1,3, NULL,9),
 (113,null,1,4,'OCM',19),
 (113,null,1,2,'UDA',36),
 (113,null,1,5,'UDA',19),

 (114,10,1,4,'OCM', 16),
 (114,10,1,2,'GAR', 24),
 (114,10,1,3, NULL, 3),
 (114,10,1,5,'GAR', 16),
 (114,10,1,6,'OCM', 16),

 (114,20,1,4,'OCM', 17),
 (114,20,1,2,'ADMAG', 32),
 (114,20,1,3, NULL, 7),
 (114,20,1,5,'ADMAG', 17),
 (114,20,1,6,'OCM', 32),

 (120,NULL,1,3, NULL,11),
 (120,NULL,1,4,'OCM',21),
 (120,NULL,1,4,'UDA',39),

 (128,NULL,1,3, NULL,12),
 (128,NULL,1,3, NULL,13),
 (128,NULL,1,4,'OCM',22),
 (128,NULL,1,4,'OCM',23),
 (128,NULL,1,5,'GAR',23),
 (128,NULL,1,5,'LEG',23),

 (141,NULL,1,3, NULL,2),
 (141,NULL,1,4, 'OCM',15),

 (139,NULL,1,3, NULL,2),
 (139,NULL,1,4, 'OCM',15),
 (139,NULL,1,8, NULL,NULL)

 GO

  /*Perfil GAR*/
  -- GAR  => CAMBIAR A EN PROCESO SUBIR UN COMENTARIO cuando este en pendiente
 INSERT INTO ConfiguracionDerivacion (IdConfiguracionAccion,GrupoFiltro,TipoProcesamiento, TipoEvento, Perfil,IdEstado) VALUES 
 (301,NULL,1,4,'GAR',25),
 (301,NULL,1,3,NULL,4),
 (301,NULL,1,6,'GAR',25) 

 GO

 INSERT INTO ConfiguracionDerivacion (IdConfiguracionAccion,GrupoFiltro,TipoProcesamiento, TipoEvento, Perfil,IdEstado) VALUES 

 (302,NULL,1,4,'GAR',27),
 (302,NULL,1,2,'LEG',28),
 (302,NULL,1,3,NULL,5),
 (302,NULL,1,5,'LEG',27),
 (302,NULL,1,6,'GAR',5),

 (303,NULL,1,3, NULL,2),
 (303,NULL,1,4,'OCM',18), 
 (303,NULL,1,4,'GAR',26),
 (303,NULL,1,5,'OCM',26),   
 (303,NULL,1,6,'GAR',26)
 GO
 
 -- GAR  => CAMBIAR A EN PROCESO  
 INSERT INTO ConfiguracionDerivacion (IdConfiguracionAccion,GrupoFiltro,TipoProcesamiento, TipoEvento, Perfil,IdEstado) VALUES 
 (305,NULL,1,4,'GAR',25),
 (305,NULL,1,3,NULL,4) 

 GO
  
 INSERT INTO ConfiguracionDerivacion (IdConfiguracionAccion,GrupoFiltro,TipoProcesamiento, TipoEvento, Perfil,IdEstado) VALUES 
 (307,NULL,1,3,'OCM',2),
 (307,NULL,1,4,'OCM',18),
 (307,NULL,1,4,'GAR',26),
 (307,NULL,1,5,'OCM',26),
 (307,NULL,1,6,'GAR',26)
 GO

   -- GAR  => CAMBIAR A EN PROCESO  
 INSERT INTO ConfiguracionDerivacion (IdConfiguracionAccion,GrupoFiltro,TipoProcesamiento, TipoEvento, Perfil,IdEstado) VALUES 
 (308,NULL,1,4,'GAR',25),
 (308,NULL,1,3,NULL,4) 

  GO

  -- GAR  => CAMBIAR A EN DERIVADO A LEGAL  
 INSERT INTO ConfiguracionDerivacion (IdConfiguracionAccion,GrupoFiltro,TipoProcesamiento, TipoEvento, Perfil,IdEstado) VALUES 
 (309,NULL,1,4,'GAR',27),
 (309,NULL,1,2,'LEG',28),
 (309,NULL,1,3,NULL,5),
 (309,NULL,1,5,'LEG',27),
 (309,NULL,1,6,'GAR',27)

 GO
   -- GAR  => CAMBIAR A EN PROCESO SUBIR UN COMENTARIO cuando este en pendiente
 INSERT INTO ConfiguracionDerivacion (IdConfiguracionAccion,GrupoFiltro,TipoProcesamiento, TipoEvento, Perfil,IdEstado) VALUES 
 (310,NULL,1,4,'GAR',25),
 (310,NULL,1,3,NULL,4),
 (310,NULL,1,6,'GAR',25)
 GO

 /*PERFIL LEG*/
  INSERT INTO ConfiguracionDerivacion (IdConfiguracionAccion,GrupoFiltro,TipoProcesamiento, TipoEvento, Perfil,IdEstado) VALUES 
 (401,NULL,1,3,NULL,2),
 (401,NULL,1,4,'OCM',18),
 (401,NULL,1,4,'LEG',30),
 (401,NULL,1,5,'OCM',30),
 (401,NULL,1,6,'LEG',30),

 (407,NULL,1,3,NULL,2),
 (407,NULL,1,4,'OCM',18),
 (407,NULL,1,4,'LEG',30),
 (407,NULL,1,5,'OCM',30),
 (407,NULL,1,6,'LEG',30)
 GO

    -- leg  => CAMBIAR A EN PROCESO SUBIR UN COMENTARIO cuando este en En proceso
 INSERT INTO ConfiguracionDerivacion (IdConfiguracionAccion,GrupoFiltro,TipoProcesamiento, TipoEvento, Perfil,IdEstado) VALUES 
 (402,NULL,1,4,'LEG',29),
 (402,NULL,1,3,NULL,6),
 (402,NULL,1,6,'LEG',29) 

   -- LEG  => CAMBIAR A EN DERIVADO A ADMAG  (pendiente)
 INSERT INTO ConfiguracionDerivacion (IdConfiguracionAccion,GrupoFiltro,TipoProcesamiento, TipoEvento, Perfil,IdEstado) VALUES 
 (403,NULL,1,4,'LEG',31),
 (403,NULL,1,2,'ADMAG',32),
 (403,NULL,1,3,NULL,7),
 (403,NULL,1,5,'ADMAG',31),
 (403,NULL,1,6,'LEG',31)
 
-- leg  => CAMBIAR A EN PROCESO SUBIR MINUTA 
 INSERT INTO ConfiguracionDerivacion (IdConfiguracionAccion,GrupoFiltro,TipoProcesamiento, TipoEvento, Perfil,IdEstado) VALUES 
 (404,NULL,1,4,'LEG',29),
 (404,NULL,1,3,NULL,6) 

   -- leg  => CAMBIAR A EN PROCESO SUBIR UN COMENTARIO cuando este en En proceso
 INSERT INTO ConfiguracionDerivacion (IdConfiguracionAccion,GrupoFiltro,TipoProcesamiento, TipoEvento, Perfil,IdEstado) VALUES 
 (408,NULL,1,4,'LEG',29),
 (408,NULL,1,3,NULL,6),
 (408,NULL,1,6,'LEG',29) 

     -- LEG  => CAMBIAR A EN DERIVADO A ADMAG   (En proceso)
 INSERT INTO ConfiguracionDerivacion (IdConfiguracionAccion,GrupoFiltro,TipoProcesamiento, TipoEvento, Perfil,IdEstado) VALUES 
 (409,NULL,1,4,'LEG',31),
 (409,NULL,1,2,'ADMAG',32),
 (409,NULL,1,3,NULL,7),
 (409,NULL,1,5,'ADMAG',31),
 (409,NULL,1,6,'LEG',31)

  -- leg  => CAMBIAR A EN PROCESO SUBIR CARTA DE ENTREGA 
 INSERT INTO ConfiguracionDerivacion (IdConfiguracionAccion,GrupoFiltro,TipoProcesamiento, TipoEvento, Perfil,IdEstado) VALUES 
 (410,NULL,1,4,'LEG',29),
 (410,NULL,1,3,NULL,6)

 /*Perfil ADMAG*/
   -- ADMAG  => CAMBIAR A EN PROCESO SUBIR UN COMENTARIO
 INSERT INTO ConfiguracionDerivacion (IdConfiguracionAccion,GrupoFiltro,TipoProcesamiento, TipoEvento, Perfil,IdEstado) VALUES 
 (506,NULL,1,4,'ADMAG',33),
 (506,NULL,1,3,NULL,8),
 (506,NULL,1,6,'ADMAG',NULL) 

    -- ADMAG  => CAMBIAR A EN PROCESO SUBIR UN COMENTARIO
 INSERT INTO ConfiguracionDerivacion (IdConfiguracionAccion,GrupoFiltro,TipoProcesamiento, TipoEvento, Perfil,IdEstado) VALUES 
 --(512,NULL,1,4,'ADMAG',33),
 --(512,NULL,1,3,NULL,8),
 (512,NULL,1,6,'ADMAG',NULL) 
 
 GO
 
  INSERT INTO ConfiguracionDerivacion (IdConfiguracionAccion,GrupoFiltro,TipoProcesamiento, TipoEvento, Perfil,IdEstado) VALUES 
 (501,NULL,1,3,NULL,2),
 (501,NULL,1,4,'OCM',18),
 (501,NULL,1,4,'ADMAG',34),
 (501,NULL,1,5,'OCM',34),
 (501,NULL,1,6,'ADMAG',34),

 (507,NULL,1,3,NULL,2),
 (507,NULL,1,4,'OCM',18),
 (507,NULL,1,4,'ADMAG',34),
 (507,NULL,1,5,'OCM',34),
 (507,NULL,1,6,'ADMAG',34),
 
 (505,NULL,1,4,'ADMAG',35),
 (505,NULL,1,2,'UDA',36),
 (505,NULL,1,3,NULL,9),
 (505,NULL,1,5,'UDA',35),
 (505,NULL,1,6,'ADMAG',NULL),
 (505,NULL,1,7,NULL,NULL),

 (511,NULL,1,4,'ADMAG',35),
 (511,NULL,1,2,'UDA',36),
 (511,NULL,1,3,NULL,9),
 (511,NULL,1,5,'UDA',35),
 (511,NULL,1,6,'ADMAG',NULL),
 (511,NULL,1,7,NULL,NULL)

 GO
 
  /*Perfil UDA*/
 INSERT INTO ConfiguracionDerivacion (IdConfiguracionAccion,GrupoFiltro,TipoProcesamiento, TipoEvento, Perfil,IdEstado) VALUES 
 (601,NULL,1,3,NULL,2),
 (601,NULL,1,4,'OCM',18),
 (601,NULL,1,4,'UDA',40),
 (601,NULL,1,5,'OCM',40),
 (601,NULL,1,6,'UDA',40),

 (603,NULL,1,3,NULL,2),
 (603,NULL,1,4,'OCM',18),
 (603,NULL,1,4,'UDA',40),
 (603,NULL,1,5,'OCM',40),
 (603,NULL,1,6,'UDA',40),
 
 /*PENDIENTE - Agregar comentario*/
 (605, NULL, 2, 3, NULL, 41),
 (605, NULL, 2, 4, 'UDA', 37),
 (605, NULL, 2, 6, 'UDA', 37),
 
 /*EN PROCESO - Agregar comentario*/
 (606, NULL, 2, 6, 'UDA', 37),

 /*ENVIAR PARA SU NOTIFICACION*/ 
(607,NULL,1,3, NULL,10),
(607,NULL,1,4, 'OCM',20),
(607,NULL,1,4, 'UDA',38),
(607,NULL,1,5, 'OCM',2),
(607,NULL,1,5, 'OCM',3),

(608,NULL,1,3, NULL,10),
(608,NULL,1,4, 'OCM',20),
(608,NULL,1,4, 'UDA',38),
(608,NULL,1,5, 'OCM',2),
(608,NULL,1,5, 'OCM',3),

 /*NOTIFICADO*/ 
(615,NULL,1,3, NULL,11),
(615,NULL,1,4, 'OCM',21),
(615,NULL,1,4, 'UDA',39)

 GO
   

 /*OTROS*/
 INSERT INTO ConfiguracionDerivacion (IdConfiguracionAccion,GrupoFiltro,TipoProcesamiento, TipoEvento, Perfil,IdEstado) VALUES 
 /*Configuraci�n Procesamiento Autom�tico JOB*/
 --/*TipoProcesamiento 1 Manual*/
 (900, 910, 1, 1, NULL, 1),
 (900, 910, 1, 2, 'OCM', 14),
 /*TipoProcesamiento 2 Autom. GAR*/
 (900, 920, 1, 1, NULL, 3),
 (900, 920, 1, 2, 'OCM', 16),
 (900, 920, 1, 2, 'GAR', 24),
 --/*TipoProcesamiento 3 Autom. ADMAG*/
 (900, 930, 1, 1, NULL, 7),
 (900, 930, 1, 2, 'OCM', 17),
 (900, 930, 1, 2, 'ADMAG', 32)
GO
 
 /*PROCESO AUTOMATICO ADMAG DERIVAR A OFN*/
  INSERT INTO ConfiguracionDerivacion (IdConfiguracionAccion,GrupoFiltro,TipoProcesamiento, TipoEvento, Perfil,IdEstado) VALUES 
 /*Configuraci�n Procesamiento Autom�tico JOB*/
 --/*TipoProcesamiento 1 Manual*/
 (901,NULL,1,4,'ADMAG',35),
 (901,NULL,1,2,'UDA',36),
 (901,NULL,1,3,NULL,9),
 (901,NULL,1,5,'UDA',35),
 (901,NULL,1,6,'ADMAG',NULL),
 (901,NULL,1,7,NULL,NULL)

 GO
INSERT INTO FiltroAccion 
(IdConfiguracionAccion, Grupo, TipoFiltro, Valor1, Valor2) VALUES
(101, 10, 1, 'GAR', NULL),
(101, 20, 1, 'ADMAG', NULL),
(900, 910, 2, NULL, 1),
(900, 920, 2, NULL, 2),
(900, 930, 2, NULL, 3)
GO

DELETE FROM DerivacionMovimiento

DELETE FROM EmisionMovimiento

DELETE FROM Derivacion

DELETE FROM Emision

DELETE FROM CreditoCliente

	
SET IDENTITY_INSERT DBO.AGENCIA ON

IF NOT EXISTS(select  * from Agencia where Oficina LIKE '%PUERTO INCA%')
	BEGIN
	
		INSERT INTO dbo.Agencia
		(CodAgencia,Oficina, Estado, FechaRegistro, FlagAgencia) 
		VALUES
		(59,'PUERTO INCA', 1, GETDATE(),1);
	END
	GO


IF NOT EXISTS(select  * from Agencia where Oficina LIKE '%PASCO%')
	BEGIN
	
		INSERT INTO dbo.Agencia
		(CodAgencia,Oficina, Estado, FechaRegistro, FlagAgencia) 
		VALUES
		(60,'PASCO', 1, GETDATE(),1);
	END
	GO

IF NOT EXISTS(select  * from Agencia where Oficina LIKE '%OLMOS%')
	BEGIN
	
		INSERT INTO dbo.Agencia
		(CodAgencia,Oficina, Estado, FechaRegistro, FlagAgencia) 
		VALUES
		(61,'OLMOS', 1, GETDATE(),1);
	END
	GO

IF NOT EXISTS(select  * from Agencia where Oficina LIKE '%CA�ETE%')
	BEGIN
	
		INSERT INTO dbo.Agencia
		(CodAgencia,Oficina, Estado, FechaRegistro,FlagAgencia) 
		VALUES
		(62,'CA�ETE', 1, GETDATE(),1);
	END
	GO

IF NOT EXISTS(select  * from Agencia where Oficina LIKE '%CHEPEN%')
	BEGIN
	
		INSERT INTO dbo.Agencia
		(CodAgencia,Oficina, Estado, FechaRegistro,FlagAgencia)
		VALUES
		(63,'CHEPEN', 1, GETDATE(),1);
	END
	GO

IF NOT EXISTS(select  * from Agencia where Oficina LIKE '%YURIMAGUAS%')
	BEGIN
	
		INSERT INTO dbo.Agencia
		(CodAgencia,Oficina, Estado, FechaRegistro, FlagAgencia) 
		VALUES
		(64,'YURIMAGUAS', 1, GETDATE(),1);
	END
	GO

IF NOT EXISTS(select  * from Agencia where Oficina LIKE '%MOYOBAMBA%')
	BEGIN
	
		INSERT INTO dbo.Agencia
		(CodAgencia,Oficina, Estado, FechaRegistro, FlagAgencia) 
		VALUES
		(65,'MOYOBAMBA', 1, GETDATE(),1);
	END
	GO

IF NOT EXISTS(select  * from Agencia where Oficina LIKE '%BARRANCA%')
	BEGIN
	
		INSERT INTO dbo.Agencia
		(CodAgencia,Oficina, Estado, FechaRegistro,FlagAgencia)
		VALUES
		(66,'BARRANCA', 1, GETDATE(),1);
	END
	GO

IF NOT EXISTS(select  * from Agencia where Oficina LIKE '%TOCACHE%')
	BEGIN
	
		INSERT INTO dbo.Agencia
		(CodAgencia,Oficina, Estado, FechaRegistro,FlagAgencia)
		VALUES
		(67,'TOCACHE', 1, GETDATE(),1);
	END
	GO

	IF NOT EXISTS(select  * from Agencia where Oficina LIKE '%ATALAYA%')
	BEGIN
	
		INSERT INTO dbo.Agencia
		(CodAgencia,Oficina, Estado, FechaRegistro,FlagAgencia)
		VALUES
		(68,'ATALAYA', 1, GETDATE(),1);
	END
	GO

	SET IDENTITY_INSERT DBO.AGENCIA OFF
	
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia='26' WHERE CodAgencia='60'
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia='30' WHERE CodAgencia='59'
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia='32' WHERE CodAgencia='61'
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia='37' WHERE CodAgencia='62'
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia='38' WHERE CodAgencia='63'
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia='43' WHERE CodAgencia='65'
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia='45' WHERE CodAgencia='66'
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia='46' WHERE CodAgencia='64'
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia='53' WHERE CodAgencia='67'
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia='56' WHERE CodAgencia='68'

	
	----------------------Inicio: Secci�n actualizada en base a lo enviado por AGROBANCO ---------------------------------- 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='40' WHERE CodAgencia='41' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='9' WHERE CodAgencia='3' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='24' WHERE CodAgencia='9' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='12' WHERE CodAgencia='8' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='39' WHERE CodAgencia='36' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='20' WHERE CodAgencia='15' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='49' WHERE CodAgencia='31' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='4' WHERE CodAgencia='11' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='31' WHERE CodAgencia='4' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='27' WHERE CodAgencia='47' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='11' WHERE CodAgencia='16' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='36' WHERE CodAgencia='52' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='14' WHERE CodAgencia='17' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='61' WHERE CodAgencia='43' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='8' WHERE CodAgencia='19' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='7' WHERE CodAgencia='24' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='6' WHERE CodAgencia='27' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='54' WHERE CodAgencia='25' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='25' WHERE CodAgencia='50' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='41' WHERE CodAgencia='29' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='21' WHERE CodAgencia='14' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='52' WHERE CodAgencia='49' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='15' WHERE CodAgencia='28' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='59' WHERE CodAgencia='5' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='60' WHERE CodAgencia='33' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='18' WHERE CodAgencia='37' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='22' WHERE CodAgencia='40' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='58' WHERE CodAgencia='18' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='2' WHERE CodAgencia='45' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='16' WHERE CodAgencia='32' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='51' WHERE CodAgencia='20' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='10' WHERE CodAgencia='35' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='29' WHERE CodAgencia='22' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='17' WHERE CodAgencia='10' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='23' WHERE CodAgencia='39' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia = NULL WHERE CodAgencia='30'
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='50' WHERE CodAgencia='46'
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='19' WHERE CodAgencia='7' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='5' WHERE CodAgencia='48' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='44' WHERE CodAgencia='26' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='33' WHERE CodAgencia='34' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='3' WHERE CodAgencia='51' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='35' WHERE CodAgencia='42' 
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia ='34' where CodAgencia ='38' and Oficina='OXAPAMPA' 

	----------------------FIN: Secci�n actualizada en base a lo enviado por AGROBANCO ---------------------------------- 
	
IF NOT EXISTS(SELECT * FROM  Usuario  WHERE Perfil IN ('UDA','ADMAG','LEG','UDA'))
BEGIN
	INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','nfrias@agrobanco.com.pe','AGRNFC','Nilton Frias',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Oxapampa%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_misuiza@agrobanco.com.pe','AGRMIL','Isui Medina',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Pangoa%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','pgregorio@agrobanco.com.pe','AGRPGS','Percy Gregorio',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Oxapampa%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','eiturry@agrobanco.com.pe','AGREIG','Efrain Iturry',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Padre Abad%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','ediaz@agrobanco.com.pe','AGREDL','Edgar Diaz',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Padre Abad%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','emejico@agrobanco.com.pe','AGREMM','Eduardo Mejico',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Satipo%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','dcuritima@agrobanco.com.pe','AGRDCY','Diana Curitima',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Caballococha%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','alemos@agrobanco.com.pe','AGRALCH','Angelo Lemos',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Iquitos%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','frojas@agrobanco.com.pe','AGRFRVE','Felipe Rojas',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Iquitos%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','tpena@agrobanco.com.pe','AGRTPF','Tula Pe�a',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Caballococha%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','rgutierrez@agrobanco.com.pe','AGRRGCU','Roxana Gutierrez',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Tacna%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','rcasaverde@agrobanco.com.pe','AGRRCCU','Rino Casaverde',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Cusco%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','jano@agrobanco.com.pe','AGRJAE','Jack A�o',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Cusco%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','jpillco@agrobanco.com.pe','AGRJPOR','Jimmy Pillco',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Puerto Maldonado%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','jhancco@agrobanco.com.pe','AGRJHM','Javier Hancco',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Puno%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','wchavez@agrobanco.com.pe','AGRWCR','Willy Chavez',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AREQUIPA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','mvargas@agrobanco.com.pe','AGRMVPI','Meilyn Vargas',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Andahuaylas%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','rmamanic@agrobanco.com.pe','AGRRMCA','Richar Mamani',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Tacna%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','cdiazs@agrobanco.com.pe','AGRCDS','Cristian Diaz',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Chiclayo%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_eperez@agrobanco.com.pe','AGREPA','Esau Perez',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Huancayo%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','rdelacruz@agrobanco.com.pe','AGRRDS','Robert Jesus de la Cruz',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Chincha%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','aquispe@agrobanco.com.pe','AGRAQM','Deivis Quispe Mejia',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Chincha%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','jcelestino@agrobanco.com.pe','AGRJCPC','Jose Miguel Celestino',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Huacho%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','jvasquezc@agrobanco.com.pe','AGRJVCO','Jose Vasquez',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Huaral%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','lhuaman@agrobanco.com.pe','AGRLHC','Luis Huaman',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Huancayo%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','jdelacruz@agrobanco.com.pe','AGRJDH','Jhan Miguel de la Cruz Henostroza',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Huaraz%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','jcabanillasr@agrobanco.com.pe','AGRJCRU','Jose Cabanillas',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Chiclayo%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','llima@agrobanco.com.pe','AGRLLA','Luis Vidal Lima',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Tarma%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','mcachuan@agrobanco.com.pe','AGRMCBA','Miguel Cachuan',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Tarma%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','hvasquez@agrobanco.com.pe','AGRHVV','Hugo Raul Vasquez',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Chimbote%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','jcastillo@agrobanco.com.pe','AGRJCS','Jesus Castillo',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Sullana%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_rsanchezd@agrobanco.com.pe','AGRRSDC','Ricky Saavedra',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Tarapoto%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','wfarfan@agrobanco.com.pe','AGRWFF','William Farfan',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Chulucanas%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','pfasabi@agrobanco.com.pe','AGRPFS','Pedro Fasabi',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Tarapoto%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','fgonza@agrobanco.com.pe','AGRFGJ','Franklin Gonza',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Jaen%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','sgonzaga@agrobanco.com.pe','AGRSGR','Sergio Gonzaga',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Juanjui%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','dsinarahua@agrobanco.com.pe','AGRDSB','David Sinarahua',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Juanjui%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','ipaz@agrobanco.com.pe','AGRIPC','Ireno Paz',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Juanjui%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','bgarcia@agrobanco.com.pe','AGRBGA','Bernardo Garcia',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Juanjui%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','aespinoza@agrobanco.com.pe','AGRAER','Ayraldi Espinoza',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Tumbes%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','yespinoza@agrobanco.com.pe','AGRYED','Yan Espinoza',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Tarapoto%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','wtorres@agrobanco.com.pe','AGRWTP','Walter Torres',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%BAGUA GRANDE%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','halves@agrobanco.com.pe','AGRHAO','Harry Alves',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Tarapoto%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_lacosta@agrobanco.com.pe','AGRLAA','ACOSTA AGUILAR, LUIS EDUARDO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%HUANUCO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_aaguilar@agrobanco.com.pe','AGRAAV','AGUILAR VILLENA, ALEX ALFREDO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%BAGUA GRANDE%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_maguirre@agrobanco.com.pe','AGRMASI','AGUIRRE SIMBALA, MICHAEL WILLIAMS',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PIURA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_cajahuana@agrobanco.com.pe','AGRCAM','AJAHUANA MAMANI, CARLOS ABIMAEL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TINGO MARIA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_aalcalde@agrobanco.com.pe','AGRAASAR','ALCALDE SARMIENTO, ARTEMIO SEGUNDO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHICLAYO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jalvarado@agrobanco.com.pe','AGRJARE','ALVARADO REQUEJO , JOSE HOMER',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%JAEN%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_ranamuro@agrobanco.com.pe','AGRRACC','ANAMURO CCALLO, ROLANDO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUNO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_kapaza@agrobanco.com.pe','AGRNAC','APAZA CUNO, NATALY KATIA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUERTO MALDONADO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_waquino@agrobanco.com.pe','AGRWAC','AQUINO CASTA�EDA, WILDER',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%HUANUCO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jarce@agrobanco.com.pe','AGRJAAP','ARCE APAZA, JUAN ORLANDO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUNO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_zarriola@agrobanco.com.pe','AGRZAT','ARRIOLA TALAVERANO, ZOSIMO YOVER',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%RICA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_lascama@agrobanco.com.pe','AGRLAV','ASCAMA VILCA, LUIS ANTONIO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%ICA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_katuncar@agrobanco.com.pe','AGRKAM','ATUNCAR MARTINEZ, KENYI CESAR',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHINCHA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_sayunta@agrobanco.com.pe','AGRSAM','AYUNTA MAQUERA, SONIA MERCEDES',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUNO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jbecerra@agrobanco.com.pe','AGRJBSA','BECERRA SAMANIEGO , JHOSSIMAR ADERLYN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SULLANA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jbellota@agrobanco.com.pe','AGRJBE','BELLOTA ESCALANTE, JESUS ARMANDO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SICUANI%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_lbendezu@agrobanco.com.pe','AGRLBAR','BENDEZU ARISTIZABAL, LUIS FERNANDO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SATIPO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_eberrocal@agrobanco.com.pe','AGREBGA','BERROCAL GAVILAN, EVER JHONY',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SAN FRANCISCO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_hberru@agrobanco.com.pe','AGRHBS','BERRU SAUCEDO, HETZER ALLISON',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%JAEN%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_wbrito@agrobanco.com.pe','AGRWBS','BRITO SALVADOR , WILMER ENRIQUE',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%HUARAZ%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_ccabrera@agrobanco.com.pe','AGRCCGU','CABRERA GUEVARA, CARLOS',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHOTA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jcaceres@agrobanco.com.pe','AGRJCCC','CACERES CCAMA, JHONATAN STEVEN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AREQUIPA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jcaceres@agrobanco.com.pe','AGRJCCC','CACERES CCAMA, JORGE ROLANDO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUNO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_lcalderon@agrobanco.com.pe','AGRLCM','CALDERON MORALES, LUIS ARTURO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%HUARAZ%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_gcalderon@agrobanco.com.pe','AGRGCMA','CALDERON MAMANI, GERSON HERNAN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CUSCO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_fcalixto@agrobanco.com.pe','AGRFCLU','CALIXTO LUJAN, FRANDO SHERLY',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TINGO MARIA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jquispec@agrobanco.com.pe','AGRJQCA','CALIZAYA GUTIERREZ, YINA ZENAIDA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%MOQUEGUA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_acallata@agrobanco.com.pe','AGRACTU','CALLATA TUDELA , ALIDA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AYACUCHO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','jcalle@agrobanco.com.pe','AGRJCN','CALLE NEIRA, JORGE LUIS',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SULLANA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_gcarbajal@agrobanco.com.pe','AGRGCS','CARBAJAL SOTO, GUSTAVO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SATIPO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_lcardenas@agrobanco.com.pe','AGRLCQU','CARDENAS QUENTA, LENNON JHOSER',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TACNA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jcari@agrobanco.com.pe','AGRJCAR','CARI ARI, JUAN CARLOS',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUNO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_dcasallo@agrobanco.com.pe','AGRDCR','CASALLO RODRIGUEZ, DANIEL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AREQUIPA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_eccari@agrobanco.com.pe','AGRECCL','CCARI CALCINA, ESPERANZA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AYAVIRI%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jceopa@agrobanco.com.pe','AGRJCTO','CEOPA TORRES , JUAN LUIS',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%BAGUA GRANDE%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_gcercado@agrobanco.com.pe','AGRGCA','CERCADO ACU�A, GEINER',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%BAMBAMARCA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_echavez@agrobanco.com.pe','AGRECES','CHAVEZ ESPINOZA, ELVER YONEL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%BAMBAMARCA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_rchavezv@agrobanco.com.pe','AGRRCV','CHAVEZ VERGARA , ROLY',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PICHANAKI%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_pchavezb@agrobanco.com.pe','AGRPCB','CHAVEZ BAUTISTA, PERCY ARMANDO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PISCO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jchavezz@agrobanco.com.pe','AGRJCZR','CHAVEZ ZORRILA, JAVIER GREGORY',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUERTO MALDONADO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_cchichipe@agrobanco.com.pe','AGRCCCA','CHICHIPE CAHUAZA, CARLOS ALBERTO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%YURIMAGUAS%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_mchiclla@agrobanco.com.pe','AGRMCN','CHICLLA NAVARRO, MIGUEL ANGEL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUQUIO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_echoqueh@agrobanco.com.pe','AGRECH','CHOQUE HITO, ELDER JAVIER',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUERTO MALDONADO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jcondori@agrobanco.com.pe','AGRJCA','CONDORI ARIZACA, JULIO CESAR',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AYAVIRI%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_lcontreras@agrobanco.com.pe','AGRLCG','CONTRERAS GARCIA , LEIDY LISSYN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PICHANAKI%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_hcordova@agrobanco.com.pe','AGRHCRO','CORDOVA ROJAS, HOLAF JUAN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUERTO INCA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_mcornejo@agrobanco.com.pe','AGRMCEP','CORNEJO ESPINOZA, MOISES ANTONIO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHINCHA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','sdavila@agrobanco.com.pe','AGRSDF','DAVILA FLORES, SHEYLA LORENA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CABALLOCOCHA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jdavila@agrobanco.com.pe','AGRJDG','DAVILA GUIMAC , JEFFERSON VIDAL JOSE',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%BAGUA GRANDE%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_edavila@agrobanco.com.pe','AGREDT','DAVILA TARRILLO, EDGAR ALEXANDER',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHOTA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_adelacruz@agrobanco.com.pe','AGRADRA','DE LA CRUZ RAMOS, ANDERSON YOEL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%HUANCAYO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_wdelcastillo@agrobanco.com.pe','AGRWDP','DEL CASTILLO PEREZ, WILLY JOEL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TARAPOTO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_kdelgado@agrobanco.com.pe','AGRKDB','DELGADO BRAVO, KHALIL NOEL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%OLMOS%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_ldiazc@agrobanco.com.pe','AGRLDCA','DIAZ CAMPOS, LUZ MAGALY',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%HUARAZ%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','jespinoza@agrobanco.com.pe','AGRJEB','ESPINOZA BENANCIO, JONAS DARWIN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%HUANUCO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_wflores@agrobanco.com.pe','AGRWFFE','FLORES FERNANDEZ, WINSTON',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%ABANCAY%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jgalvezs@agrobanco.com.pe','AGRJGSO','GALVEZ SOLDADO , JHONY RICHARD',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TUMBES%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_cgarcia@agrobanco.com.pe','AGRCGOL','GARCIA OLIVARES , CHRISTIAN JOEL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SULLANA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','jgarciar@agrobanco.com.pe','AGRJGRO','GARCIA RODRIGUEZ, JHON JHAYRO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TUMBES%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_egarciav@agrobanco.com.pe','AGREGVA','GARCIA VASQUEZ , EDUARD FRANKLIN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CAJAMARCA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_vgarrido@agrobanco.com.pe','AGRVGB','GARRIDO BARRIENTOS, VANESSA KARINA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TUMBES%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_kgoicochea@agrobanco.com.pe','AGRKGRO','GOICOCHEA RODRIGUEZ, KARINA JUDITH',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CAJAMARCA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_mgomez@agrobanco.com.pe','AGRMGY','GOMEZ YAPO, MELISA YOSELIN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUNO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jgonzales@agrobanco.com.pe','AGRJGAR','GONZALES AREVALO, JAVIER',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PADRE ABAD%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jgonzalesm@agrobanco.com.pe','AGRJGMI','GONZALES MINAURO, JULITA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%JUANJUI%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_ygoyzueta@agrobanco.com.pe','AGRYGH','GOYZUETA HANCCO, YENY',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUNO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jguerra@agrobanco.com.pe','AGRJGCO','GUERRA CORDOVA , JHORD ANTHONY',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TOCACHE%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_vguerra@agrobanco.com.pe','AGRVGL','GUERRA LAGOS , VIDAL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUCALLPA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_cguerra@agrobanco.com.pe','AGRCGMA','GUERRA MARTINEZ, CESAR ERNESTO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AYAVIRI%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_eguevara@agrobanco.com.pe','AGREGF','GUEVARA FLORES, EDGAR ALBERTO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CA�ETE%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_eherrera@agrobanco.com.pe','AGREHAL','HERRERA ALVARADO, ESGAR ENRIQUE',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHICLAYO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_khidalgo@agrobanco.com.pe','AGRKHC','HIDALGO CCAHUANTICO, KENNER STEVE',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUERTO MALDONADO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_rhilario@agrobanco.com.pe','AGRRHO','HILARIO ORTEGA, ROMEO MARCELO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Chimbote%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_whilario@agrobanco.com.pe','AGRWHV','HILARIO VILLAR , WIDER RENZO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SATIPO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_phorna@agrobanco.com.pe','AGRPHL','HORNA LANARES, PATERNO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TOCACHE%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_ghoyos@agrobanco.com.pe','AGRGHB','HOYOS BARDALES, GEORGINA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%IQUITOS%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jhuamanf@agrobanco.com.pe','AGRJHF','HUAMAN FERNANDEZ, JULIO CESAR',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AYAVIRI%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jhuamanid@agrobanco.com.pe','AGRJHDZ','HUAMANI DIAZ, JUAN CARLOS',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TRUJILLO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_ehuancahuari@agrobanco.com.pe','AGREHL','HUANCAHUARI LLIUYA , ELIAS',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AYACUCHO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_ihuanco@agrobanco.com.pe','AGRIHS','HUANCO SUCASACA, IVAN CELESTINO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AYAVIRI%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_fhuaranja@agrobanco.com.pe','AGRFHM','HUARANJA MARTINEZ, FREDERIX',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SAN FRANCISCO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_lhuaricallo@agrobanco.com.pe','AGRLHCH','HUARICALLO CHECCA, LELIA ROXANA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AREQUIPA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jhuarocv@agrobanco.com.pe','AGRJHV','HUAROC VILA , JAIME EDWIN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PICHANAKI%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_oignacio@agrobanco.com.pe','AGROIV','IGNACIO VELASQUEZ, ORLANDO JOEL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Chimbote%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_ririgoyen@agrobanco.com.pe','AGRRIM','IRIGOYEN MAMANI, ROBERT WHENZELD',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%MOQUEGUA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_isantos@agrobanco.com.pe','AGRSIH','ISIDRO HUAMAN, SANTOS JULIO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%JAEN%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_gjimenez@agrobanco.com.pe','AGRGJV','JIMENEZ VILCHEZ , GRECIA LUCIA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHULUCANAS%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_kjulca@agrobanco.com.pe','AGRKJQ','JULCA QUIROZ , KEN JHONSON',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Chimbote%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_blaura@agrobanco.com.pe','AGRBLSO','LAURA SOTO, BRANDON LUIS',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SATIPO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_llivisi@agrobanco.com.pe','AGRLLC','LIVISI CALCINA, LISLAM CHARMELY',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUNO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jllagas@agrobanco.com.pe','AGRJLCA','LLAGAS CASAS , JOSE ELMER',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%BAGUA GRANDE%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_dllocclla@agrobanco.com.pe','AGRDLG','LLOCCLLA GUZMAN, DAVID',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AYACUCHO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_vlloclla@agrobanco.com.pe','AGRVLH','LLOCLLA HUAMANI , VICTOR ELI',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AYACUCHO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_mlolay@agrobanco.com.pe','AGRMLAL','LOLAY ALBORNOZ, MIRIAM',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PICHANAKI%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jlopezc@agrobanco.com.pe','AGRJLCR','LOPEZ CRISOSTOMO , JHUSSEMA YAKELYNE',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PADRE ABAD%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jlopez@agrobanco.com.pe','AGRJLM','LOPEZ MEZA , JHANPIER',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TOCACHE%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_nlopez@agrobanco.com.pe','AGRNLS','LOPEZ SUCATICONA, NIMER',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUNO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_emamaniq@agrobanco.com.pe','AGREMQ','MAMANI QUI�ONEZ, ELMER',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SICUANI%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_smanosalva@agrobanco.com.pe','AGRSMME','MANOSALVA MEDINA, SANTOS ISMAEL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%BAMBAMARCA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jmanrique@agrobanco.com.pe','AGRJMC','MANRIQUE CASTELLARES, JAVIER WILLIAM',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PASCO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_cmarcelo@agrobanco.com.pe','AGRCMP','MARCELO POMIANO, CHRISTIAN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%HUACHO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_cmelendez@agrobanco.com.pe','AGRCMHI','MELENDEZ HIDALGO, CARLOS ROBIN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%ATALAYA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_hmendoza@agrobanco.com.pe','AGRHMME','MENDOZA MENDOZA , HEBERT LEONARDO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHINCHA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jmiguel@agrobanco.com.pe','AGRJMQ','MIGUEL QUISPE, JORGE ADOLFO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SAN FRANCISCO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_vmorales@agrobanco.com.pe','AGRVME','MORALES ECHEVARRIA, VICTOR JESUS',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%HUANUCO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jmorales@agrobanco.com.pe','AGRJMAT','MORALES ANTON, JAIRO ANDRES',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHULUCANAS%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_amozombite@agrobanco.com.pe','AGRAMI','MOZOMBITE INUMA , ALDO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TARAPOTO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_aobando@agrobanco.com.pe','AGRAOBE','OBANDO BENAVENTE, ALFREDO FERMIN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AREQUIPA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jolivares@agrobanco.com.pe','AGRJOE','OLIVARES ESCAMILO, JUANA EDITH',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CAJABAMBA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_aorosco@agrobanco.com.pe','AGRAOJ','OROSCO JULCA, ANTHONY NICOLAS',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%VILLA RICA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_kortiz@agrobanco.com.pe','AGRKOO','ORTIZ OTINIANO, KRISTI NICOLE',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SATIPO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_apaico@agrobanco.com.pe','AGRAPN','PAICO NI�O, ANGELICA MARIA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHICLAYO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jpari@agrobanco.com.pe','AGRJPPA','PARI PACHECO , JORGE VIDAL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUNO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_epari@agrobanco.com.pe','AGREPP','PARI PUMACOTA , EDDIE ANIBAL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUNO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_cperalta@agrobanco.com.pe','AGRCPB','PERALTA BARREDA, CRISTHIAN MILTHON',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AREQUIPA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_aperez@agrobanco.com.pe','AGRAPM','PEREZ MOORE, ANGEL GUALBERTO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Chimbote%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_apilco@agrobanco.com.pe','AGRAPMA','PILCO M�S, ANDRES ABELINO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CAJAMARCA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_spilco@agrobanco.com.pe','AGRRMCA','PILCO GUTIERREZ, SILVIA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TACNA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jpineda@agrobanco.com.pe','AGRJPL','PINEDA LUJANO, JOHAN DAVID',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUNO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_npinedo@agrobanco.com.pe','AGRNPF','PINEDO FLORES, NELSON',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PADRE ABAD%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_wpoma@agrobanco.com.pe','AGRWPT','POMA TINTAYA, WILBER',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUNO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_sprado@agrobanco.com.pe','AGRSPT','PRADO TAQUIRE, SUSANA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AYACUCHO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_aquea@agrobanco.com.pe','AGRAQF','QUEA FLORES , ALEJO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUNO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_cquezada@agrobanco.com.pe','AGRCQE','QUEZADA ESCOBAR , CARLOS ALBERTO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TAMBOGRANDE%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jquiroz@agrobanco.com.pe','AGRJQA','QUIROZ ALDAY , JOSE MANUEL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHICLAYO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jquispec@agrobanco.com.pe','AGRJQCA','QUISPE CALIZAYA , JORGE PAUL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AYAVIRI%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_rquispec@agrobanco.com.pe','AGRRQC','QUISPE CONDORI, RICARDO PASCUAL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AREQUIPA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jquispep@agrobanco.com.pe','AGRJQP','QUISPE PE�A, JOSE LUIS',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%QUILLABAMBA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_yquispe@agrobanco.com.pe','AGRYQS','QUISPE SOTO, YAIR ANTONI',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SAN FRANCISCO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_dramos@agrobanco.com.pe','AGRDRSA','RAMOS SALAS, DIEGO GIOVANNY',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AREQUIPA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_nramos@agrobanco.com.pe','AGRNRG','RAMOS GONZALES, NELLY MILAGROS',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%HUARAZ%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_prioja@agrobanco.com.pe','AGRPRD','RIOJA DIAZ, POOL BIVIANO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TARAPOTO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_krios@agrobanco.com.pe','AGRKRR','RIOS RIBEIRO , KEMPNER LEAO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUCALLPA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_wrivadeneira@agrobanco.com.pe','AGRWRG','RIVADENEIRA GARCIA, WILMER',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TRUJILLO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jrojasg@agrobanco.com.pe','AGRJRGV','ROJAS GUEVARA, JAMER LEONAR',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%MOYOBAMBA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_crojas@agrobanco.com.pe','AGRCRU','ROJAS URPI, CARLOS JONY',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%LA MERCED%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_erojas@agrobanco.com.pe','AGRERO','ROJAS ORIZANO , EDWIN WILLIAMS',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%HUANUCO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_oruiz@agrobanco.com.pe','AGRORS','RUIZ SALDA�A, OMAR',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%JUANJUI%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_gsalas@agrobanco.com.pe','AGRGST','SALAS TAPULLIMA , GILDER',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TARAPOTO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_fsamame@agrobanco.com.pe','AGRFSG','SAMAME GOMEZ, FRANZ IGOR',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PADRE ABAD%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_dsamaniego@agrobanco.com.pe','AGRDSF','SAMANIEGO FIDEL, DIEGO ORLANDO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CA�ETE%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_wsanchez@agrobanco.com.pe','AGRWSG','SANCHEZ GALARRETA , WILDER LEONEL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TRUJILLO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jsanchez@agrobanco.com.pe','AGRJSCC','SANCHEZ CACHAY, JOE JACOB',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CAJAMARCA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jsembrera@agrobanco.com.pe','AGRJSCR','SEMBRERA CRUZ, JAIMITO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%JAEN%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_rsoto@agrobanco.com.pe','AGRRST','SOTO TRUJILLO, RUYER',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TINGO MARIA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_ytacca@agrobanco.com.pe','AGRYTM','TACCA MAMANI, YETMY ELVA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUNO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_gtaipe@agrobanco.com.pe','AGRGTGU','TAIPE GUZMAN, GAVI JUDITH',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUNO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_eterreros@agrobanco.com.pe','AGRETC','TERREROS CAMAC, ELVIS GREGORIO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TOCACHE%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jtolentino@agrobanco.com.pe','AGRJTS','TOLENTINO SORIA, JUNNIOR FERNANDO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TOCACHE%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_vtorres@agrobanco.com.pe','AGRVTR','TORRES ROJAS, VICTOR RAUL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%ANDAHUAYLAS%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_ttuesta@agrobanco.com.pe','AGRTTP','TUESTA PAREDES, TERCERO SALOMON',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TOCACHE%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_rvaca@agrobanco.com.pe','AGRRVLO','VACA LOPEZ, ROBER NELSON',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TOCACHE%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_mvaldivia@agrobanco.com.pe','AGRMVRO','VALDIVIA RODRIGUEZ, MAXWELL FELIX',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AREQUIPA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_dvalentin@agrobanco.com.pe','AGRDVL','VALENTIN LAZO, DARSIN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SATIPO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_gvargas@agrobanco.com.pe','AGRGVFI','VARGAS FIGUEROA , GERSON',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%QUILLABAMBA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jvasquez@agrobanco.com.pe','AGRJVD','VASQUEZ DIAZ, JEFFERSON GIANCARLO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%JAEN%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_pvasquez@agrobanco.com.pe','AGRPVGA','VASQUEZ GARCIA , PERCY EDUARDO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TARAPOTO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_fvelasquezm@agrobanco.com.pe','AGRFVMA','VELASQUEZ MALDONADO , FRANK JAIRO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%JUANJUI%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_kventura@agrobanco.com.pe','AGRKVCH','VENTURA CHAMOCHUMBI, KEVIN ORLANDO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHEPEN%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_gverdeh@agrobanco.com.pe','AGRGVH','VERDE HUAMAN, GEDER ROLY',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%OXAPAMPA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_mvictorio@agrobanco.com.pe','AGRMVP','VICTORIO POLO , MICHAEL NOE',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TRUJILLO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_mvilcaa@agrobanco.com.pe','AGRMVA','VILCA APAZA, MARIO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUNO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_kvilela@agrobanco.com.pe','AGRKVQ','VILELA QUI�ONES, KAROL YOHANA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%JAEN%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jvillanueva@agrobanco.com.pe','AGRJVMU','VILLANUEVA MA�UICO, JUAN CARLOS',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%ANDAHUAYLAS%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jyahua@agrobanco.com.pe','AGRJYQ','YAHUA QUISPE , JUAN LUIS',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AREQUIPA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jyallico@agrobanco.com.pe','AGRJYR','YALLICO REYES, JOSE ADRIAN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TARMA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_hyanque@agrobanco.com.pe','AGRHYCU','YANQUE CUELA, HEYDI MARIELA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUERTO MALDONADO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jyauri@agrobanco.com.pe','AGRJYG','YAURI GUTIERREZ, JACK KEVIN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SATIPO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_fzapata@agrobanco.com.pe','AGRFZS','ZAPATA SILVA , FRANCK EDUARD',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHULUCANAS%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_lchupillon@agrobanco.com.pe','AGRLCR','CHUPILLON ROBLEDO, LUIS FERNANDO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TINGO MARIA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_spurizaca@agrobanco.com.pe','AGRSPC','PURIZACA CISNEROS, SANDRA MILAGROS',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%OLMOS%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_erodriguez@agrobanco.com.pe','AGRERYA','RODRIGUEZ YANGUA, EXMILDER',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TAMBOGRANDE%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_rrufino@agrobanco.com.pe','AGRRRCS','RUFINO CASTRO, ROSAURA CAROL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TAMBOGRANDE%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_psalazar@agrobanco.com.pe','AGRPSA','SALAZAR ALIAGA, PERCY',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUNO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_csanchez@agrobanco.com.pe','AGRCSCA','SANCHEZ CAMACHO, CARLOS ENRIQUE',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%ABANCAY%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jsehuincha@agrobanco.com.pe','AGRJSRJ','SEHUINCHA ROJAS, JHONNY',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%HUANCAYO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_osilva@agrobanco.com.pe','AGROSP','SILVA PEREZ, OSBARVIER',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%JUANJUI%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_nvillanueva@agrobanco.com.pe','AGRNVG','VILLANUEVA GALLARDO, NILSON JHOAN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%BAMBAMARCA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_cdelacruzg@agrobanco.com.pe','AGRCDGR','DE LA CRUZ GRANADOS, CARLOS',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%HUANCAYO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','tazorza@agrobanco.com.pe','AGRTAA','AZORZA AVILA, TANIA LISETH',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%LA MERCED%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_cllamoctanta@agrobanco.com.pe','AGRCLT','LLAMOCTANTA TERRONES, CARMEN ROSA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%MOYOBAMBA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_wromero@agrobanco.com.pe','AGRWRAL','ROMERO ALVAREZ, WILLY JIBAN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TARAPOTO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jrosado@agrobanco.com.pe','AGRJRQU','ROSADO QUEVEDO, JUAN WILFREDO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHICLAYO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_asantacruz@agrobanco.com.pe','AGRASR','SANTA CRUZ ROJAS, ALVARO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHICLAYO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jsu@agrobanco.com.pe','AGRJSS','SU SERRUCHE, KARLA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUCALLPA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_ltello@agrobanco.com.pe','AGRLTH','TELLO HUAMAN, LEONARDO FABIO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%FERRE�AFE%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_frojas@agrobanco.com.pe','AGRFRLA','ROJAS LANCHIPA, FERNANDO MARTIN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TACNA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_rascon@agrobanco.com.pe','AGRRAQ','ASCON QUEVEDO, RICARDO EDSON',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%JAEN%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jbarboza@agrobanco.com.pe','AGRJBMA','BARBOZA MARIN, JORGE LUIS',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%MOYOBAMBA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_hrefulio@agrobanco.com.pe','AGRHRC','REFULIO CASIMIRO, HUMBERTO RENZO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AMBO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_wbarrantes@agrobanco.com.pe','AGRWBD','BARRANTES DAVILA, WALTER NOE',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHICLAYO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_ncaman@agrobanco.com.pe','AGRNCO','CAMAN OYARCE, NELVIN REYNALDO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHACHAPOYAS%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_dgomez@agrobanco.com.pe','AGRDGC','GOMEZ CRISOLES, DANNY SERGIO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%ANDAHUAYLAS%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_wllanos@agrobanco.com.pe','AGRWLFI','LLANOS FIGUEROA, WILLY',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CAJAMARCA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jlopeza@agrobanco.com.pe','AGRJLAC','LOPEZ ACHARTE, JHOMARA MILUSKA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%HUARAL%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_gcalderonr@agrobanco.com.pe','AGRGCR','CALDERON RAMOS, GRECIA LIZBETH',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%VALLE CHICAMA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_gpanduro@agrobanco.com.pe','AGRGPF','PANDURO FABIAN, GRECIA MONICA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%ATALAYA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jamao@agrobanco.com.pe','AGRJARO','AMAO ROJAS, JAVIER EUSEBIO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%ANDAHUAYLAS%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jacuna@agrobanco.com.pe','AGRJAOS','ACU�A OSCCO, JIMMY HERBERT',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%ANDAHUAYLAS%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_sruiz@agrobanco.com.pe','AGRSRIN','RUIZ INGA , SILVERIO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PANGOA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_rparhuay@agrobanco.com.pe','AGRRPLN','PARHUAY LEANDRO , RAIL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SAN FRANCISCO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','nvasquezq@agrobanco.com.pe','AGRNVQ','VASQUEZ QUINTICUARI, NEAL WILSON',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%OXAPAMPA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','cdiazb@agrobanco.com.pe','AGRCDB','DIAZ BENAVIDES, CHENY YULEISY',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHEPEN%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_hjauregui@agrobanco.com.pe','AGRHJA','JAUREGUI ASCOY , HECTOR ABEL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TRUJILLO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_chuallanca@agrobanco.com.pe','AGRCHSE','HUALLANCA SERAPIO, CYNDI',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHINCHA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_wbarrantes@agrobanco.com.pe','AGRWBD','ALBINO RAMIREZ, SUSAN WENDY',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TINGO MARIA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_karestegui@agrobanco.com.pe','AGRKAT','ARESTEGUI TELLO, KELLY ANDREA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%ANDAHUAYLAS%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_layasta@agrobanco.com.pe','AGRLAE','AYASTA ELGUERA, LESLY BRILLIT',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SAN FRANCISCO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_kbisso@agrobanco.com.pe','AGRKBL','BISSO LUNA , KARLA STEFANNY',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%HUACHO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_rbujaico@agrobanco.com.pe','AGRRBL','BUJAICO LLACUA, ROXANA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%HUANCAYO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_icajachagua@agrobanco.com.pe','AGRICL','CAJACHAGUA LOPEZ, IVAN EFRA�N',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%VILLA RICA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_acardenas@agrobanco.com.pe','AGRACCR','CARDENAS CRUZ, ALEXANDRA BERENICE',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TACNA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_acarrion@agrobanco.com.pe','AGRACAL','CARRION ALCOCER, ANGIE ZENAIDA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AREQUIPA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_yduran@agrobanco.com.pe','AGRYDM','DURAN MORENO , YESSENIA JESSICA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%HUARAZ%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_sfarfan@agrobanco.com.pe','AGRSFC','FARFAN CRUZ , SANDRA DEL CARMEN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHULUCANAS%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_vhuamani@agrobanco.com.pe','AGRVHP','HUAMANI PEREZ , VALENTINA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUERTO MALDONADO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_eleon@agrobanco.com.pe','AGRELF','LEON FABIAN, ESTUAR ORSSON',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AYACUCHO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_lpapel@agrobanco.com.pe','AGRLPPE','PAPEL PEZO, LUZ CYNTHIA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CUSCO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_lvilca@agrobanco.com.pe','AGRLVCO','VILCA CONDORI, LUCY',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AYAVIRI%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_rvivas@agrobanco.com.pe','AGRRVC','VIVAS CHAYAN, ROCIO DEL CARMEN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%JAEN%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_saparicio@agrobanco.com.pe','AGRSAB','APARICIO BARBOZA, SIDNEY YESENIA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUNO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_lcubas@agrobanco.com.pe','AGRLCPR','CUBAS PEREZ, LILI YULISA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%JAEN%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_rrafaela@agrobanco.com.pe','AGRRRAL','RAFAEL ALLCCA, RONAL MARLON',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PICHANAKI%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','fsaldarriaga@agrobanco.com.pe','AGRFSM','FLOR SALDARRIAGA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SULLANA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','cpalacios@agrobanco.com.pe','AGRCPE','CARMEN PALACIOS',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PIURA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','gsangay@agrobanco.com.pe','AGRGSG','GISELA SANGAY / ANA CUEVA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CAJAMARCA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','falarcon@agrobanco.com.pe','AGRFAR','FLOR ALARCON',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHICLAYO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','mdpchavez@agrobanco.com.pe','AGRMCT','MILAGROS CHAVEZ',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHEPEN%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_rguamuro@agrobanco.com.pe','AGRRGCA','ROXANA GUAMURO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%OLMOS%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_rvivas@agrobanco.com.pe','AGRRVC','ROCIO VIVAS',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%BAGUA GRANDE%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','napestegui@agrobanco.com.pe','AGRNAA','NATALY APESTEGUI',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Chimbote%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','agutierrez@agrobanco.com.pe','AGRAGD','ANA MARIA GUTIERREZ',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TRUJILLO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','wvizarreta@agrobanco.com.pe','AGRWVM','WENDY VIZARRETA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHINCHA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_chuallanca@agrobanco.com.pe','AGRLHCH','CYNDI HUALLANCA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CA�ETE%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_chuallanca@agrobanco.com.pe','AGRCHSE','',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PISCO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','nroman@agrobanco.com.pe','AGRNRQ','NANCY ROMAN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%HUACHO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_kbisso@agrobanco.com.pe','AGRKBL','KARLA BISSO LUNA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%BARRANCA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','bmachuca@agrobanco.com.pe','AGRBMR','BIANCA MACHUCA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TARMA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','mcentenoh@agrobanco.com.pe','AGRMCH','MONICA CENTENO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%OXAPAMPA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','mcenteno@agrobanco.com.pe','AGRMCE','MAIDA CENTENO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%VILLA RICA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_icajachagua@agrobanco.com.pe','AGRICL','IVAN CAJACHAGUA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PANGOA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','','','',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PICHANAKI%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_icajachagua@agrobanco.com.pe','AGRICL','',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%LA MERCED%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_mhenderson@agrobanco.com.pe','AGRMLHP','MARIA HENDERSON',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PADRE ABAD%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_mhenderson@agrobanco.com.pe','AGRMLHP','',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUCALLPA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','','','',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUERTO INCA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_salbino@agrobanco.com.pe','AGRSAR','SUSAN WENDY ALBINO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TINGO MARIA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','','','',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TOCACHE%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','jtobies@agrobanco.com.pe','AGRJTL','JULIO TOBIES',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%IQUITOS%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','jtobies@agrobanco.com.pe','AGRJTL','',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CABALLOCOCHA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','dhuaringa@agrobanco.com.pe','AGRDHA','DEYSI HUARINGA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TARAPOTO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_lgaray@agrobanco.com.pe','AGRLGM','LUCY GARAY',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHACHAPOYAS%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','rquispe@agrobanco.com.pe','AGRRQP','ROSA QUISPE',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AREQUIPA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_acarrion@agrobanco.com.pe','AGRACAL','ANGIE CARRION',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CAMANA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_acardenas@agrobanco.com.pe','AGRACCR','ALEXANDRA CARDENAS',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TACNA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_acardenas@agrobanco.com.pe','AGRACCR','',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%MOQUEGUA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','gpilco@agrobanco.com.pe','AGRGPC','GLADYS PILCO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUNO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','onavarro@agrobanco.com.pe','AGRONA','OBDULIA NAVARRO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CUSCO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','partica@agrobanco.com.pe','AGRPAP','ARTICA PINEDA, PAULO CESAR',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%HUANCAYO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jmoreno@agrobanco.com.pe','AGRJMV','MORENO VALVERDE, JUNIOR ESTHIBEN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%HUARAZ%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('ADMAG','Administrador de Agencia','asaucedo@agrobanco.com.pe','AGRASD','SAUCEDO, DIAZ ADALI',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CAJAMARCA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','jhuillca@agrobanco.com.pe','AGRJHVI','HUILLCA VILLALVA, JUDITH MARLENY',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CUSCO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','cbernardo@agrobanco.com.pe','AGRCBM','BERNARDO MEDRANO, CESAR ILLESCA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PADRE ABAD%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','mlopez@agrobanco.com.pe','AGRMLRU','LOPEZ RUIZ, MEEKERT MACNOVIER',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SULLANA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','jcaso@agrobanco.com.pe','AGRJCCH','CASO CANCHUMANYA, JHUNIOR GREGORIO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%HUANCAYO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jbuchhammer@agrobanco.com.pe','AGRJBP','BUCHHAMMER PALACIOS, JOSUE JOACHIM',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AREQUIPA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_htamayo@agrobanco.com.pe','AGRHTB','TAMAYO BACA, HERNAN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TRUJILLO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jticona@agrobanco.com.pe','AGRJTB','TICONA BARAZORDA, JERAMIL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CUSCO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_dvelazque@agrobanco.com.pe','AGRDVCA','VELAZQUE CALDERON, DANIEL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%ANDAHUAYLAS%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jzevallos@agrobanco.com.pe','AGRJZTO','ZEVALLOS TORIBIO, JIMMY LUIGI',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%HUANCAYO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_tguevaram@agrobanco.com.pe','AGRTGM','GUEVARA MILLIAN, TANIA MARIA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%JAEN%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_wsalazar@agrobanco.com.pe','AGRWSA','SALAZAR ALJAHUAMAN, WILFREDO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%OXAPAMPA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_hlaura@agrobanco.com.pe','AGRHLC','LAURA CONDORI, HUBER ADOLFO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUERTO MALDONADO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_bsalazar@agrobanco.com.pe','AGRASD','SALAZAR GOMEZ, BRIGGITE',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHINCHA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_lhuamane@agrobanco.com.pe','AGRLHE','HUAMAN ESPLANA, LUIS',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%HUANCAYO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','JHONATAN GOMEZ CHALCO','AGRJGCH','GOMEZ CHALCO, JHONATAN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AYAVIRI%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_mneira@agrobanco.com.pe','AGRMNR','NEIRA RIVEROS, MIGUEL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AREQUIPA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jaquinop@agrobanco.com.pe','AGRJAPC','AQUINO PACHERREZ, JONATHAN STEVEN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHICLAYO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_ngalvez@agrobanco.com.pe','AGRNGG','GALVEZ GUTIERREZ, NAHUM',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SAN FRANCISCO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','hfaccio@agrobanco.com.pe','AGRHFR','FACCIO RAMIREZ, HECTOR JOSE',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TRUJILLO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','jmarquez@agrobanco.com.pe','AGRJME','MARQUEZ ESPINOZA, JOSE ENRIQUE',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AREQUIPA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_lmazzo@agrobanco.com.pe','AGRLMVZ','MAZZO VELASCO, LUIS ALBERTO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Chimbote%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','wdiaz@agrobanco.com.pe','AGRWDPE','DIAZ PEREZ, WILLIAM',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%JAEN%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_flima@agrobanco.com.pe','AGRFLW','LIMA WASHUALDO, FRANKLIN WILLIAM',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUNO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_lhuaman@agrobanco.com.pe','AGRLHP','HUAMAN PONCE, LEONIDAS DANIEL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%OXAPAMPA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_rfrancia@agrobanco.com.pe','AGRRFCH','FRANCIA CHUMPITAZ, RICARDO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%HUACHO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_mrique@agrobanco.com.pe','AGRMRQ','RIQUE QUISPE, MARCO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%HUACHO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_rcuenca@agrobanco.com.pe','AGRRCRA','CUENCA RAMIREZ, RENZO SAUL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TARAPOTO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jhuanacuni@agrobanco.com.pe','AGRJHA','HUANACUNI ALFARO, JUAN CARLOS',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AYAVIRI%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_hvalle@agrobanco.com.pe','AGRHVG','VALLE GUERRERO, HENRRY',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHICLAYO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jchavez@agrobanco.com.pe','AGRJCD','CHAVEZ DEL AGUILA, JORGE LUIS',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TINGO MARIA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_rcondor@agrobanco.com.pe','AGRRCCH','CONDOR CACHIQUE, ROY BERTIN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CAJAMARCA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_cchavez@agrobanco.com.pe','AGRCCM','CHAVEZ MOSCOL, CESAR LUCIANO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHULUCANAS%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_hguevara@agrobanco.com.pe','AGRHGF','GUEVARA FLORES, HIGOBERTO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TARAPOTO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jalva@agrobanco.com.pe','AGRJAMA','ALVA MATUTE, JAVIER',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TRUJILLO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_dfloresh@agrobanco.com.pe','AGRDFH','FLORES HUARACHA, DANNY',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CUSCO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_ecoras@agrobanco.com.pe','AGRECD','CORAS DE LA CRUZ, ELIAS ROLANDO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AYACUCHO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_jmamania@agrobanco.com.pe','AGRJMAP','MAMANI APAZA, JHON NIMER',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUNO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','nhuayama@agrobanco.com.pe','AGRNHCO','HUAYAMA COCA, NILCER',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CAJAMARCA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','rsanchez@agrobanco.com.pe','AGRRSCA','SANCHEZ CAMPOS, RONALD ELVIS',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CAJAMARCA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','jfloreso@agrobanco.com.pe','AGRJFO','FLORES OREJUELA, JONATHAN DAVID',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHULUCANAS%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','cmartos@agrobanco.com.pe','AGRCMLI','MARTOS LINARES, CLUBERT',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TRUJILLO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','equispe@agrobanco.com.pe','AGREQRI','QUISPE RIOS, EMERIC GREGORIO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PADRE ABAD%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('ADMAG','Administrador de Agencia','gguillen@agrobanco.com.pe','AGRGGQ','GUILLEN QUISPE, GILDER',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUERTO MALDONADO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','rvidal@agrobanco.com.pe','AGRRVU','VIDAL UNTIVEROS, ROGER ARSENIO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SATIPO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('ADMAG','Administrador de Agencia','aanchi@agrobanco.com.pe','AGRAAN','ANCHI TORRES, ALVARO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%ANDAHUAYLAS%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','hcamayo@agrobanco.com.pe','AGRHCSE','CAMAYO SEDANO, HECTOR RAUL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SATIPO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','rchota@agrobanco.com.pe','AGRRCSA','CHOTA SALAS, ROCKY KANISIO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TARAPOTO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','hespinoza@agrobanco.com.pe','AGRHEA','ESPINOZA APONTE HECTOR',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SAN FRANCISCO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','lespinoza@agrobanco.com.pe','AGRLEH','ESPINOZA HARO, LESLY VIVIANA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Chimbote%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','zmamani@agrobanco.com.pe','AGRZMA','ZEIDA MAMANI A.',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CUSCO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','epumah@agrobanco.com.pe','AGREPH','PUMA HUAMAN, ERWIN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CUSCO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','fyupanqui@agrobanco.com.pe','AGRFYC','YUPANQUI CCAYACC, FREDY OSCAR',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AYACUCHO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('ADMAG','Administrador de Agencia','rsantiago@agrobanco.com.pe','AGRRSAP','SANTIAGO APOLINARIO, ROSALIA PATRICIA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SATIPO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('ADMAG','Administrador de Agencia','loc_msumen@agrobanco.com.pe','AGRMSHE','SUMEN HERNANDEZ, MANUEL JESUS',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHINCHA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','echoquehuanca@agrobanco.com.pe','AGRECMC','CHOQUEHUANCA MACHACA, EDGAR RENE',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TACNA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('ADMAG','Administrador de Agencia','lpaucar@agrobanco.com.pe','AGRLPL','PAUCAR LLAPAPASCA, LUIS RUBYL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SULLANA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','ngomez@agrobanco.com.pe','AGRNGF','GOMEZ FERNANDEZ, NEYSER ANTONIO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%JAEN%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('ADMAG','Administrador de Agencia','wmas@agrobanco.com.pe','AGRWMG','MAS GOLAC WERTHER EXEQUIEL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TARAPOTO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','jcordovar@agrobanco.com.pe','AGRJCRO','CORDOVA ROJAS JORGE WILTER',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%JAEN%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_hmamani@agrobanco.com.pe','AGRHMQ','MAMANI QUISPE, HENRY WILBER',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUNO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('ADMAG','Administrador de Agencia','nrojasl@agrobanco.com.pe','AGRNRL','ROJAS LIMA, NORKA ISABEL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%HUANCAYO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','amorales@agrobanco.com.pe','AGRAMA','MORALES AREDO, ALEX WALDEMAR',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CAJAMARCA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','thuaman@agrobanco.com.pe','AGRTHI','HUAMAN IGARAZA, THOBIN ROBERT',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CUSCO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','sgarcia@agrobanco.com.pe','AGRSGP','GARCIA PAZ OVIEDO, SHERILY DEL CARMEN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AREQUIPA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','dchavez@agrobanco.com.pe','AGRDCD','CHAVEZ DAZA, DAVID ENRIQUE',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TARAPOTO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','jnoblecillar@agrobanco.com.pe','AGRJNR','NOBLECILLA ROMERO, JULIO YESMANY',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SULLANA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','jrimac@agrobanco.com.pe','AGRJRVE','RIMAC VEGA, JOSE EFRAIN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TINGO MARIA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','edominguez@agrobanco.com.pe','AGREDCA','DOMINGUEZ CARTOLIN, ELVIS CESAR',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SAN FRANCISCO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','gdelgado@agrobanco.com.pe','AGRGDV','DELGADO VEGA, GISELLA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%HUACHO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('ADMAG','Administrador de Agencia','svasquez@agrobanco.com.pe','AGRSVT','VASQUEZ TACILLA, SAUL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TRUJILLO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','mpalacios@agrobanco.com.pe','AGRMPBE','PALACIOS BERMEO, MIGUEL ANGEL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CAJAMARCA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','hceliz@agrobanco.com.pe','AGRHCM','CELIZ MUERRIETA, HANS MILJAN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TINGO MARIA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','zhuaraca@agrobanco.com.pe','AGRZHM','HUARACA MOSCOSO, ZINTIA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%ANDAHUAYLAS%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','matao@agrobanco.com.pe','AGRMAT','ATAO TELLO, MARIO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SATIPO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','ecornejo@agrobanco.com.pe','AGRECM','CORNEJO MENDIGURI, ERIKA PATRICIA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CUSCO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','jordaya@agrobanco.com.pe','AGRJOR','ORDAYA ROJAS, JOSE LUIS',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%HUANCAYO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','hvillalobos@agrobanco.com.pe','AGRHVH','VILLALOBOS HERNANDEZ, HUGO LLOVER',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHICLAYO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('ADMAG','Administrador de Agencia','abarreda@agrobanco.com.pe','AGRABD','BARREDA DELGADO, ALEX',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AREQUIPA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('ADMAG','Administrador de Agencia','rramirezf@agrobanco.com.pe','AGRRRF','RAMIREZ FLORES, RAFAEL RUSO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PADRE ABAD%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','atesillo@agrobanco.com.pe','AGRATM','TESILLO MITA, ANA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TACNA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('ADMAG','Administrador de Agencia','eestofanero@agrobanco.com.pe','AGREEH','ESTOFANERO HUANCOLLO, EDGAR LEONIDAS',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUNO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','pcardenas@agrobanco.com.pe','AGRPCA','CARDENAS ALARCON, PIERRE ANDERSON',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AYACUCHO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('ADMAG','Administrador de Agencia','scano@agrobanco.com.pe','AGRSCA','CANO CONTRERAS, SAMUEL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%JAEN%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','jzuloeta@agrobanco.com.pe','AGRJZS','ZULOETA SANCHEZ, JESUS ONAN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CAJAMARCA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','kdiaz@agrobanco.com.pe','AGRKDL','DIAZ LIZAMA, KAREN BRIGITTE',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SULLANA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','pyvargas@agrobanco.com.pe','AGRPVG','VARGAS GUAYLUPO, PATRICIA YESENIA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SULLANA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','jccallo@agrobanco.com.pe','AGRJCBUS','CCALLO BUSTAMANTE, JAIME',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TACNA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','jyeren@agrobanco.com.pe','AGRJYS','YEREN SARAVIA, JOSE ANTONIO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHINCHA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','smedina@agrobanco.com.pe','AGRSMU','MEDINA UMERES, SAUL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AREQUIPA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','descate@agrobanco.com.pe','AGRDEP','ESCATE PARIONA, DIEGO ARMANDO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHINCHA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','ehuacre@agrobanco.com.pe','AGREHP','HUACRE PILLACA, EDITH LUZ',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SAN FRANCISCO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','parroyo@agrobanco.com.pe','AGRPAME','ARROYO MEDINA, PERCY EDWIN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%HUANCAYO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('ADMAG','Administrador de Agencia','rwong@agrobanco.com.pe','AGRRWC','WONG CASTILLO, FELIX RICARDO MARTIN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Chimbote%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','ngarcia@agrobanco.com.pe','AGRNGS','GARCIA SURICHAQUI, NELCY ROSMERY',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SATIPO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','erojas@agrobanco.com.pe','AGRERP','ROJAS PEREZ, ELOY',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TARAPOTO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','gflorentine@agrobanco.com.pe','AGRGFF','FLORENTINE FLORIAN, GRACE IVET',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%HUACHO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_ttuesta@agrobanco.com.pe','AGRTTP','TUESTA OLAYA, FELIX ARMANDO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PADRE ABAD%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','lmac@agrobanco.com.pe','AGRLMV','MAC VALDIVIESO, LUIS ALONSO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TRUJILLO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','cjuarez@agrobanco.com.pe','AGRCJH','JUAREZ HURTADO, CARLOS ALBERTO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%ANDAHUAYLAS%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('ADMAG','Administrador de Agencia','jflorese@agrobanco.com.pe','AGRJFE','FLORES ESPINOZA, JANETT JESSICA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TINGO MARIA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','rtapia@agrobanco.com.pe','AGRRTP','TAPIA PACHECO, ROBERTO JULIO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%JAEN%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','crivera@agrobanco.com.pe','AGRCRP','RIVERA PE�A, CARLOS ALBERTO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHULUCANAS%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','echique@agrobanco.com.pe','AGREVCM','CHIQUE MAMANI, EDWIN VALERIO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUNO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','jcruz@agrobanco.com.pe','AGRJCV','CRUZ VARGAS, JASMIN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AREQUIPA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','jjaimes@agrobanco.com.pe','AGRJJC','JAIMES CRUZ, JESSICA LUCY',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CUSCO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','jmadrid@agrobanco.com.pe','AGRJPMA','MADRID ATALAYA, JUAN PABLO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHICLAYO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('ADMAG','Administrador de Agencia','avivar@agrobanco.com.pe','AGRAVA','VIVAR ANGELES, ANTONIO JESUS',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%HUACHO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','aclavijo@agrobanco.com.pe','AGRACP','CLAVIJO PUELLES, ADRIAN ALONSO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHICLAYO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','aguevara@agrobanco.com.pe','AGRAGCA','GUEVARA CAMPOS, ALFREDO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHICLAYO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('ADMAG','Administrador de Agencia','icastillo@agrobanco.com.pe','AGRICI','CASTILLO INGA, ISABEL LUISA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Chimbote%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','ryahuana@agrobanco.com.pe','AGRRYR','YAHUANA ROJAS, ROBERTO CARLOS',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SULLANA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','ychacon@agrobanco.com.pe','AGRYCM','CHACON MAQUERA, YEMIRA MILAGROS',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%PUNO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','dmeza@agrobanco.com.pe','AGRDMJ','MEZA JARUFE, DANTE HUGO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AREQUIPA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('ADMAG','Administrador de Agencia','vpari@agrobanco.com.pe','AGRRMCA','PARI APAZA, VALOIS YORDAN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TACNA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','DIANA PEREZ CERVERA','AGRDPC','PEREZ CERVERA, DIANA CAROLINA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%JAEN%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','msalvatierra@agrobanco.com.pe','AGRMSD','SALVATIERRA DE LOS RIOS, MARIA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHINCHA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('ADMAG','Administrador de Agencia','hmorales@agrobanco.com.pe','AGRHMA','MORALES ANCCO, HERBER PAUL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SAN FRANCISCO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','avargas@agrobanco.com.pe','AGRAVT','VARGAS TICSE, ABIGAIL RUTH',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SATIPO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','elivasquez@agrobanco.com.pe','AGREVV','VASQUEZ VASQUEZ, ELICIDA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%JAEN%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('ADMAG','Administrador de Agencia','gtitto@agrobanco.com.pe','AGRGTG1','TTITO GUERRA, GIANFRANCO ENZO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AYAVIRI%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','chuanacuni@agrobanco.com.pe','AGRCHM','HUANACUNI MAMANI, CINTHYA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%TACNA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','dchamorro@agrobanco.com.pe','AGRDCH','CHAMORRO CAPCHA, DENISSE',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%HUANCAYO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('ADMAG','Administrador de Agencia','jgomez@agrobanco.com.pe','AGRJGR','GOMEZ REZZA, JOSUE MISAEL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%OXAPAMPA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','rjordan@agrobanco.com.pe','AGRRJA','JORDAN ACOSTA, RUBALY MARCELA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AREQUIPA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','apalomino@agrobanco.com.pe','AGRAPB','PALOMINO BOLIVAR, ANA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AYACUCHO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('ADMAG','Administrador de Agencia','hcrisanto@agrobanco.com.pe','AGRHCR','CRISANTO RETO, HERNAN WILLIAN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHULUCANAS%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('ADMAG','Administrador de Agencia','jhuaman@agrobanco.com.pe','AGRJHC','HUAMAN CACERES, JORGE',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CUSCO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('ADMAG','Administrador de Agencia','rurbina@agrobanco.com.pe','AGRRUC','URBINA CARDENAS, ROMAN ABEL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AYACUCHO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','schacon@agrobanco.com.pe','AGRSCP','CHACON PAREDES, SEGUNDO MISAEL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CAJAMARCA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','jgutierrez@agrobanco.com.pe','AGRJGC','GUTIERREZ COTRINA, JEYNER PERCY',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CAJAMARCA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('ADMAG','Administrador de Agencia','ccastaneda@agrobanco.com.pe','AGRCCA1','CASTA�EDA AGUILAR, CESAR AUGUSTO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%CHICLAYO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','mverde@agrobanco.com.pe','AGRMVR','VERDE ROSAS, MIGUEL ANGEL',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%Chimbote%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','loc_pchavezb@agrobanco.com.pe','AGRPCB','CHAVEZ CORREA, PERCY EDGAR',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%JAEN%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','rcanales@agrobanco.com.pe','AGRRCB','CANALES BERNAL, RAFAEL ALFREDO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%HUACHO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','jguerrero@agrobanco.com.pe','AGRJGS','GUERRERO SAUCEDO, JOHNNY CHRISTIAN',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SULLANA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','hchambi@agrobanco.com.pe','AGRHCC','CHAMBI CONDORI, HECTOR MARIO',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%AYAVIRI%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','karevalo@agrobanco.com.pe','AGRKAG','AREVALO GUERRA, KETTY VICTORIA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%SULLANA%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('UDA','Usuario de Agencia','ovasquez@agrobanco.com.pe','AGROVG','VASQUEZ GONZALES, MARIA OLGA',1,USER,GETDATE(),(select TOP 1 CodAgencia from Agencia WHERE UPPER(REPLACE(Oficina,'  ',' ')) like UPPER('%HUANCAYO%')))
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('GAR','Garant�as','aarenas@agrobanco.com.pe','AGRAAL','ANA ARENAS LOPEZ',1,USER,GETDATE(), NULL)
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('GAR','Garant�as','tpacco@agrobanco.com.pe','AGRTPL','TERESA PACCO LLERENA',1,USER,GETDATE(), NULL)
INSERT INTO Usuario (Perfil, DescripcionPerfil, CorreoElectronico, UsuarioWeb, NombresApellidos, Estado, UsuarioRegistro, FechaRegistro, CodAgencia) VALUES ('LEG','Legal','ljara@agrobanco.com.pe','AGRLJA','LAURA JARA',1,USER,GETDATE(), NULL)



END

GO