import { Component, OnInit, Input, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { RequerimientoService } from '../../../services/requerimiento.service';
import { DialogService } from '../../../services/comun/dialog.service';
import { SecurityService } from '../../../services/security.service';
import { RecursoEmisionService } from '../../../services/recurso-emision.service';
import { Subject } from 'rxjs';
import { ConfiguracionAccionService } from '../../../services/configuracion-accion.service';

@Component({
  selector: 'app-cna-garantia-seccion',
  templateUrl: './cna-garantia-seccion.component.html',
  styleUrls: ['./cna-garantia-seccion.component.css']
})
export class CnaGarantiaSeccionComponent implements OnInit {

  @ViewChild('File1', {static: false}) File1!: ElementRef;
  @ViewChild('File2', {static: false}) File2!: ElementRef;
  @ViewChild('File3', {static: false}) File3!: ElementRef;
  @ViewChild('File4', {static: false}) File4!: ElementRef;

  @ViewChild('Input1', {static: false}) Input1!: ElementRef;
  @ViewChild('Input2', {static: false}) Input2!: ElementRef;
  @ViewChild('Input3', {static: false}) Input3!: ElementRef;
  @ViewChild('Input4', {static: false}) Input4!: ElementRef;

  @Input() creditoClienteDetalle!: any;
  @Input() item : any;
  @Output() cargarPaginaOut = new EventEmitter();

  loading:boolean = false;

  Archivo1:any;
  Archivo2:any;
  Archivo3:any;
  Archivo4:any;

  NombreArchivo1:any;
  NombreArchivo2:any;
  NombreArchivo3:any;
  NombreArchivo4:any;

  bMostrarArchvo1:boolean = false;
  bMostrarArchvo2:boolean = false;
  bMostrarArchvo3:boolean = false;
  bMostrarArchvo4:boolean = false;

  MAXIMO_TAMANIO_BYTES = 2560000; // 1MB = 1 millón de bytes

  TotalArchivoSubidos:number = 0;
  list  = new Array();
  list$ = new Subject<any[]>();

  documentosEvaluadosGarantia!:any[];

  IdConfiguracionAccion:any;
  constructor(
    private _requerimientoService : RequerimientoService,
    private _recursoEmisionService: RecursoEmisionService,
    private _securityService : SecurityService,
    private _dialogService: DialogService,
    private _configuracionAccion : ConfiguracionAccionService,
  ) {



   }

  ngOnInit(): void {
    debugger;
    this.TotalArchivoSubidos = 4 - this.creditoClienteDetalle.recursoEmision.filter((x:any)=>x.tipoRecursoID==4).length;
    this.documentosEvaluadosGarantia = this.creditoClienteDetalle.recursoEmision.filter((x:any)=>x.tipoRecursoID==4);

    for (let index = 0; index < this.TotalArchivoSubidos; index++) {
      this.list.push(index);
      this.list$.next(this.list);
    }
    console.log(" this.TotalArchivoSubidos", this.list);
  }


  DescargarArchivoLaserFiche(LaserficheID:any,NameDocumento:any): void {
    this.loading = true;
    const params = {
      LaserficheID: ""+LaserficheID
    };
    this._requerimientoService.DescagarArchivos(params).subscribe((result) => {
      console.log('response DescargarArchivoLaserFiche', result.response);
      if(result.response!=null){
        if(result.response.length>0){
          for (let index = 0; index < result.response.length; index++) {
            const element = result.response[index];

            this.Descargar(element.archivoBytes,element.nombreDocumento,element.contentType);
          }
        }
      }
      this.loading = false;
    },
    (err)=>{
      this.loading = false;
      console.log(err);
    }
    );
  }

  Descargar(base64String:any, fileName:any,contentType:any) {

    const blobData = this.convertBase64ToBlobData(base64String,contentType);
    const blob = new Blob([blobData], { type: contentType });
        const url = window.URL.createObjectURL(blob);
        // window.open(url);
        const link = document.createElement('a');
        link.href = url;
        link.download = fileName;
        link.click();
  }
  convertBase64ToBlobData(base64Data: string, contentType: string = 'image/png', sliceSize = 1024) {
    const byteCharacters = atob(base64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);
      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  eliminarArchivoLaserFiche(idLaserficheCNA:string) : void {

    this._dialogService
      .openConfirmDialog('¿Seguro que desea eliminar el documento?')
      .afterClosed()
      .subscribe((res) => {
        if (res) {
          const oBE = {
            "LaserficheID": ""+idLaserficheCNA
            };
            this.loading = true;
            this._requerimientoService.eliminarArchivo(oBE).subscribe(result=>{

              this.loading = false;
              console.log("eliminarArchivo",result);
              if(result.response.resultado==1){
                //this.bCartaRespuesta = false;

                this._dialogService.openMsgSuccessDialog("Se eliminó correctamente").afterClosed().subscribe(res=>{
                  if(res){

                    this.cargarPaginaOut.emit();
                    // this.ObtenerDetalle();
                  }
                });
              }
              else{
               // this.bCartaRespuesta = false;

                this._dialogService.openMsgErrorDialog("Se presentó un problema para eliminar el documento").afterClosed().subscribe(res=>{
                  if(res){

                  }
                });
              }

            },
            (err)=>{
              this.loading = false;
              console.log(err);
            }
            );

        }
   });

   }



   subirArchivo(num:number){
    let Archivo : any;
    let ArchivoNombre: any;
    switch (num) {
      case 1:
        Archivo = this.Archivo1;
        ArchivoNombre = this.NombreArchivo1;
      break;
      case 2:
        Archivo = this.Archivo2;
        ArchivoNombre = this.NombreArchivo2;
      break;
      case 3:
        Archivo = this.Archivo3;
        ArchivoNombre = this.NombreArchivo3;
      break;
      case 4:
        Archivo = this.Archivo4;
        ArchivoNombre = this.NombreArchivo4;
      break;
      default:
        break;
    }

    console.log("item",this.item);

    if(!Archivo){
      this._dialogService.openMsgErrorDialog("Debe de seleccionar una archivo");
      return;
    }

    if(Archivo==""){
      this._dialogService.openMsgErrorDialog("Debe de seleccionar una archivo");
      return;
    }



    let request = {
      "CodRecurso" : 0,
      "CodTabla" : 8,
      "ReferenciaID" : 0,
      "LaserficheID" : 0,
      "IdRecursoEmision" : 0,
      "IdEmision" : +this.creditoClienteDetalle.idEmision,
      "TipoRecursoID" : 4,
      "UsuarioRegistro" : this._securityService.leerUsuarioWeb(),
      "DNI" : ""+this.creditoClienteDetalle.numeroDocumento,
      "Archivo" :  Archivo,
      "NombreArchivo" : ArchivoNombre
    };

    this._dialogService
      .openConfirmDialog('¿Seguro que desea subir el documento?')
      .afterClosed()
      .subscribe((res) => {
        if (res) {
          this.loading = true;
            this._recursoEmisionService.registrar(request).subscribe((data:any)=>{

              if(data.response.success.resultado){

                var request = {
                  "IdConfiguracionAccion" : this.item.idConfiguracionAccion,
                  "GrupoFiltro" : null,
                  "emision": {
                    "IdEmision" : +this.creditoClienteDetalle.idEmision,
                    "IdCreditoCliente"  : +this.creditoClienteDetalle.idCreditoCliente,
                    "Estado"  : this.creditoClienteDetalle.idEstado,
                    "TipoFlujo"  :1,
                    "FechaEntregaOlva" : null,
                    "MedioNotificacion": null,
                    "Email" : null,
                    "Whatsapp" : null,
                    "FechaCargoEntrega" : null,
                    "UsuarioRegistro" :  this._securityService.leerUsuarioWeb(),
                    "UsuarioModificacion" :  this._securityService.leerUsuarioWeb()
                  },
                  "derivacion": {
                    "IdDerivacion" : +this.creditoClienteDetalle.idDerivacion,
                    "IdEmision" : +this.creditoClienteDetalle.idEmision,
                    "Perfil" : this._securityService.leerPerfil(),
                    "Estado" : this.creditoClienteDetalle.idEstado,
                    "PerfilDerivado" : null,
                    "FechaDerivado" : null,
                    "CodAgencia" : null,
                    "UsuarioRegistro" : this._securityService.leerUsuarioWeb(),
                    "UsuarioModificacion":  this._securityService.leerUsuarioWeb()
                  },
                  "comentarioEmision": {

                  }
                };

                this._configuracionAccion.accion(request).subscribe((data:any)=>{
                  console.log("this._configuracionAccion.accion - Subir Archivo",data);
                  this.loading = false;
                  this.cargarPaginaOut.emit();
                  this._dialogService.openMsgSuccessDialog('Se subió correctamente el archivo');
                }, (error) =>{
                  console.log(error);
                  this.loading = false;
                  this.cargarPaginaOut.emit();
                  this._dialogService.openMsgSuccessDialog('Se subió correctamente el archivo');
                } );

              }
              else{
                this.loading = false;
                this._dialogService.openMsgErrorDialog("Hubo un problema al intentar subir el archivo")
              }


              console.log("_recursoEmisionService.registrar",data);
              //this.ObtenerDetalle();
            });
        }
      },
      (err)=>{
        this.loading = false;
        console.log(err);
      }
      );

  }

   handleUpload(event:any,numfile:number) {
    debugger;
        const file = event.target.files[0];
        if(file==null || file.length <=0){
          switch (numfile) {
            case 1:
            this.Archivo1 = null;
            this.NombreArchivo1 = null;

            this.bMostrarArchvo1 = false;

            this.Input1.nativeElement.value = "";
            this.File1.nativeElement.value = null;

            break;
            case 2:
              this.Archivo2 =  null;
              this.NombreArchivo2 = null;

              this.bMostrarArchvo2 = false;

              this.Input2.nativeElement.value = "";
              this.File2.nativeElement.value = null;
            break;
            case 3:
              this.Archivo3 =  null;
              this.NombreArchivo3 = null;
              this.Input3.nativeElement.value = "";
              this.File3.nativeElement.value = null;

              this.bMostrarArchvo3 = false;
            break;
            case 4:
              this.Archivo4 =  null;
              this.NombreArchivo4 = null;
              this.Input4.nativeElement.value = "";
              this.File4.nativeElement.value = null;
              this.bMostrarArchvo4 = false;
            break;

            default:
              break;
        }

         return;
       }

       var allowedExtensions = /(.jpg|.jpeg|.pdf)$/i;
       if(!allowedExtensions.exec(file.name)){
        this._dialogService.openMsgErrorDialog("Formato no válido");
        switch (numfile) {
          case 1:
          this.Archivo1 = null;
          this.NombreArchivo1 = null;

          // this.File1.nativeElement.value = "";
          // this.Input1.nativeElement.value = "";
          this.bMostrarArchvo1 = false;
          break;
          case 2:
            this.Archivo2 =  null;
            this.NombreArchivo2 = null;

            this.bMostrarArchvo2 = false;

            // this.File2.nativeElement.value = "";
            // this.Input2.nativeElement.value = "";

          break;
          case 3:
            this.Archivo3 =  null;
            this.NombreArchivo3 = null;

            this.bMostrarArchvo3 = false;

            // this.File3.nativeElement.value = "";
            // this.Input3.nativeElement.value = "";

          break;
          case 4:
            this.Archivo4 =  null;
            this.NombreArchivo4 = null;
            this.bMostrarArchvo4 = false;

            // this.File4.nativeElement.value = "";
            // this.Input4.nativeElement.value = "";

          break;

          default:
            break;
      }
      this.cargarPaginaOut.emit();
        return;
       }
        if (file.size > this.MAXIMO_TAMANIO_BYTES) {
          const tamanioEnMb = this.MAXIMO_TAMANIO_BYTES / 1024000;

          switch (numfile) {
            case 1:
              this.Archivo1 =  null;
              this.NombreArchivo1 = null;

             this.bMostrarArchvo1 = false;

            //  this.File1.nativeElement.value = "";
            //  this.Input1.nativeElement.value = "";

            break;
            case 2:
              this.Archivo2 =  null;
              this.NombreArchivo2 = null;

              this.bMostrarArchvo2 = false;

              // this.File2.nativeElement.value = "";
              // this.Input2.nativeElement.value = "";
            break;
            case 3:
              this.Archivo3 =  null;
              this.NombreArchivo3 = null;

              this.bMostrarArchvo3 = false;

              // this.File3.nativeElement.value = "";
              // this.Input3.nativeElement.value = "";
            break;
            case 4:
              this.Archivo4 =  null;
              this.NombreArchivo4 = null;

              this.bMostrarArchvo4 = false;

              // this.File4.nativeElement.value = "";
              // this.Input4.nativeElement.value = "";
            break;

            default:
              break;
          }
          this._dialogService.openMsgErrorDialog(`El tamaño máximo es ${tamanioEnMb} MB`);

          this.cargarPaginaOut.emit();
          // Limpiar
         // $miInput.value = "";
          return;
        } else {
          // Validación pasada. Envía el formulario o haz lo que tengas que hacer
        }

        const FileNamme = file.name;
        switch (numfile) {
          case 1:
           this.NombreArchivo1 = FileNamme;
           this.bMostrarArchvo1 = true;
          break;
          case 2:
            this.NombreArchivo2 = FileNamme;
            this.bMostrarArchvo2 = true;
          break;
          case 3:
            this.NombreArchivo3 = FileNamme;
            this.bMostrarArchvo3 = true;
          break;
          case 4:
            this.NombreArchivo4 = FileNamme;
            this.bMostrarArchvo4 = true;
          break;

          default:
            break;
        }

        const reader = new FileReader();
        console.log("file",file);
        reader.readAsDataURL(file);
        reader.onload = () => {
          console.log("reader",reader);
          switch (numfile) {
            case 1:
             this.Archivo1 = reader.result || null;
            break;
            case 2:
              this.Archivo2 = reader.result || null;
            break;
            case 3:
              this.Archivo3 = reader.result || null;
            break;
            case 4:
              this.Archivo4 = reader.result || null;
            break;
            default:
              break;
          }

            console.log(reader.result);
        };
    }



}
