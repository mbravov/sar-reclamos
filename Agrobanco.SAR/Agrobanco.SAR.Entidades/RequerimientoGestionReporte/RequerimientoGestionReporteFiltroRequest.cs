﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades.RequerimientoGestionReporte
{
    public class RequerimientoGestionReporteFiltroRequest
    {
        public string FechaRegistroInicial { get; set; }
        public string FechaRegistroFin { get; set; }

        public DateTime? FechaAsignacionInicial { get; set; }
        public DateTime? FechaAsignacionFin { get; set; }

        public DateTime? FechaGestionInicial { get; set; }
        public DateTime? FechaGestionFin { get; set; }

        public DateTime? FechaAtencionInicial { get; set; }
        public DateTime? FechaAtencionFin { get; set; }

        public int? TipoRequerimiento { get; set; }
        public int? Estado { get; set; }
    }
}
