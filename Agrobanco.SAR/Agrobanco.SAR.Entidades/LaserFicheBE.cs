﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades
{
    public class LaserFicheBE
    {
        public string LaserficheID { get; set; }
        public string NroReclamo { get; set; }
        public string Archivo { get; set; }
        public string NombreArchivo { get; set; }
        public Int64? CodRequerimiento { get; set; }
        public string UsuarioRegistro { get; set; }
        public int CodTabla { get; set; }

        public int? SaldoCredito { get; set; }
        public string NroCredito { get; set; }
        public string EstadoCredito { get; set; }
        public int CodTipoPlantilla { get; set; }
        public int IdEmision { get; set; }
        public string Perfil { get; set; }




    }
}
