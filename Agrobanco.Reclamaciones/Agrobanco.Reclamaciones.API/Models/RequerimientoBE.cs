﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.Reclamaciones.API.Models
{
    public class RequerimientoBE : RequerimientoDetalleBE
    {
        public string NroRequerimiento { get; set; }
        public Int64? CodRequerimiento { get; set; }
        public int CodAgencia { get; set; }
        public string Agencia { get; set; }
        public string DireccionAgencia { get; set; }
        public int EsMenor { get; set; }
        public int CodProducto { get; set; }
        public string Producto { get; set; }
        public int CodMotivo { get; set; }
        public string Motivo { get; set; }
        public int CodTipoRequerimiento { get; set; }
        public string TipoRequerimiento { get; set; }
        public int CodOrigen { get; set; }
        public int CodFormaEnvio { get; set; }
        public string FormaEnvio { get; set; }
        public string Descripcion { get; set; }
        public int Estado { get; set; }
        public string DescripcionEstado { get; set; }
        public string UsuarioBloqueo { get; set; }
        public int? EstaBloqueado { get; set; }
        public string FechaEntrega { get; set; }
        public string UsuarioRegistro { get; set; }
        public string NroReclamo { get; set; }


        public DateTime? FechaRegistro { get; set; }

        public string Archivo1 { get; set; }
        public string Archivo2 { get; set; }
        public string Archivo3 { get; set; }
        public string Archivo4 { get; set; }

        public string NombreArchivo1 { get; set; }
        public string NombreArchivo2 { get; set; }
        public string NombreArchivo3 { get; set; }
        public string NombreArchivo4 { get; set; }

        public string Origen { get; set; }

        public string LaserFicheRequerimiento { get; set; }
        public string LaserFicheIDCartaRespuesta { get; set; }
        public string LaserFicheIDCartaRespuestaOCM { get; set; }


        public int CodRequerimientoAsignacion { get; set; }
        public string LaserficheID { get; set; }
        public int EstadoAnterior { get; set; }

        public int? SaldoCredito { get; set; }
        public string NroCredito { get; set; }
        public string EstadoCredito { get; set; }


        public List<ComentarioBE> Comentarios { get; set; }
        public List<CreditosClienteBE> creditosClientes { get; set; }

    }
}
