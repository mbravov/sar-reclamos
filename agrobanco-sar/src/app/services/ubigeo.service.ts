import { Injectable } from '@angular/core';
import { AppService } from '../app.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { SecurityService } from 'src/app/services/security.service';

const UrlBase_SARAPI = environment.UrlBase_SARAPI;

@Injectable({
  providedIn: 'root'
})
export class UbigeoService extends AppService {

  constructor(securityService : SecurityService) {
    super(securityService);
  }

  listarDepartamento(){
    this.endpoint = `${UrlBase_SARAPI}/ubigeo/listar/departamento`;
    return this.post(this.endpoint,null);
  }

  listarProvincia(CodDepartamento:string){
    var oBE = {
      "CodDepartamento": CodDepartamento
    };
    this.endpoint = `${UrlBase_SARAPI}/ubigeo/listar/provincia`;
    return this.post(this.endpoint,oBE);
  }

  listarDistrito(CodDepartamento:string,CodProvincia:string){
    var oBE = {
      "CodDepartamento": CodDepartamento,
      "CodProvincia": CodProvincia
    };
    this.endpoint = `${UrlBase_SARAPI}/ubigeo/listar/distrito`;
    return this.post(this.endpoint,oBE);
  }
}
