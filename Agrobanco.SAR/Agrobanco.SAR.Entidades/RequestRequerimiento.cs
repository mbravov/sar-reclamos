﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades
{
    public class RequestRequerimiento
    {
        public string UsuarioAsignado { get; set; }
        public string CodigoEstado { get; set; }
        public string CodAgencia { get; set; }
        public string NroDocumento { get; set; }
        public string NombresApellidos { get; set; }         
        public string FechaRegistroIni { get; set; }
        public string FechaRegistroFin { get; set; }
        public string FechaAtencionIni { get; set; }
        public string FechaAtencionFin { get; set; }
        public int TipoDocumento { get; set; }

    }
}
