﻿using Agrobanco.SAR.API.Models;
using Agrobanco.SAR.Control.Filters;
using Agrobanco.SAR.Entidades;
using Agrobanco.SAR.Entidades.CNA_Minuta;
using Agrobanco.SAR.LogicaNegocio;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.SAR.API.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/cna-minuta")]
    [ApiController]
    public class CNAController : ControllerBase
    {
        private readonly CNALogica cnaLogica;        
        public CNAController()
        {
            cnaLogica = new CNALogica();
        }
        [HttpPost("listarCnaMinuta")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult CnaMinutaLista([Required][FromHeader(Name = "Token")] string token, [FromBody] CNAFilterRequest oRequest)
        {
            try
            {
                ResponseSuccess oResponseSuccess = new ResponseSuccess();
                var Resultado = cnaLogica.Listar(oRequest); 
                return StatusCode(200, Resultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost("listarEstados")]
        [ProducesResponseType(200, Type = typeof(List<EstadosCNAMinBE>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult Estados([Required][FromHeader(Name = "Token")] string token, [FromBody] CNAFilterRequest oRequest)
        {
            try
            { 
                var listaResultado = cnaLogica.ListarEstados(oRequest);

                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("listarSeguimiento")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult ListarSeguimiento([Required][FromHeader(Name = "Token")] string token, [FromBody] CNAFilterRequest oRequest)
        {
            try
            {
                ResponseSuccess oResponseSuccess = new ResponseSuccess();
                var Resultado = cnaLogica.ListarSeguimiento(oRequest);
                return StatusCode(200, Resultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("listarEstadosGen")]
        [ProducesResponseType(200, Type = typeof(List<EstadosCNAMinBE>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult EstadosGeneral([Required][FromHeader(Name = "Token")] string token)
        {
            try
            {
                var listaResultado = cnaLogica.ListarEstadosGeneral();

                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("listarAgencias")]
        [ProducesResponseType(200, Type = typeof(List<AgenciaBE>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult Agencia([Required][FromHeader(Name = "Token")] string token, [FromBody] CNAFilterRequest oRequest)
        {
            try
            {
                var listaResultado = cnaLogica.ListarAgenciaCNA(oRequest);

                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        [HttpPost("obtener-por-numero-documento")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public async Task<IActionResult> ObtenerPorNumeroDocumento([Required][FromHeader(Name = "Token")] string token, [FromBody] CNADetalleRequest oRequest)
        {
            try
            {
                ResponseSuccess oResponseSuccess = new ResponseSuccess();
                var Resultado = await cnaLogica.ObtenerPorNumeroDocumento(oRequest);
                return StatusCode(200, Resultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




                
    }
}
