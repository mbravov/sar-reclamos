import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DialogService } from '../../../services/comun/dialog.service';
import { SecurityService } from '../../../services/security.service';
import { ConfiguracionAccionService } from '../../../services/configuracion-accion.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { forkJoin } from 'rxjs';
import { Constantes } from '../../../comun/constantes';

@Component({
  selector: 'app-cna-derivar-legal',
  templateUrl: './cna-derivar-legal.component.html',
  styleUrls: ['./cna-derivar-legal.component.css']
})
export class CnaDerivarLegalComponent implements OnInit {

  form!: FormGroup;
  formSubmitted:boolean = false;

  lengthDescripcion: number  = 300;
  MaxlengthDescripcion: number  = 300;
  MinlengthDescripcion: number  = 5;

  IdConfiguracionAccion:any;
  emision:any;

  loading:any;

  constructor(
    private _formBuilder: FormBuilder,
    private _dialogService: DialogService,
    private _securityService: SecurityService,
   // private _cnaMinutaOcmService : CnaMinutaOcmService,
   // private _filtroAccionService : FiltroAccionService,
    private _configuracionAccion : ConfiguracionAccionService,
    public dialogRef: MatDialogRef<CnaDerivarLegalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.emision = data.CreditoClienteDetalle;
    this.IdConfiguracionAccion = data.IdConfiguracionAccion;
   }

  ngOnInit(): void {
    this.inicializarFormulario();
  }

  inicializarFormulario() : void {


    this.form = this._formBuilder.group({
      NombreCompletos:[this.emision.nombres],
      nroDocumento:[this.emision.numeroDocumento],

      Descripcion: [null,  [Validators.required, Validators.minLength(5)]],

    });

    this.form.controls['NombreCompletos'].disable();
    this.form.controls['nroDocumento'].disable();

  }

  derivar(){

    this.formSubmitted = true;

    if (this.form.invalid) {
      return;
    }

    var request = {
      "IdConfiguracionAccion" : this.IdConfiguracionAccion,
      "GrupoFiltro" : null,
      "Nombres": this.emision.nombres,
      "TipoDocumento": this.emision.tipoDocumento,
      "NroDocumento": this.emision.numeroDocumento,
      "emision": {
        "IdEmision" : +this.emision.idEmision,
        "IdCreditoCliente"  : +this.emision.idCreditoCliente,
        "Estado"  : this.emision.idEstado,
        "TipoFlujo"  :1,
        "FechaEntregaOlva" : null,
        "MedioNotificacion": null,
        "Email" : null,
        "Whatsapp" : null,
        "FechaCargoEntrega" : null,
        "UsuarioRegistro" :  this._securityService.leerUsuarioWeb(),
        "UsuarioModificacion" :  this._securityService.leerUsuarioWeb()
      },
      "derivacion": {
        "IdDerivacion" : +this.emision.idDerivacion,
        "IdEmision" : +this.emision.idEmision,
        "Perfil" : this._securityService.leerPerfil(),
        "Estado" : this.emision.idEstado,
        "PerfilDerivado" : Constantes.LEG.toUpperCase(),
        "FechaDerivado" : null,
        "CodAgencia" : null,  //+this.form.controls["CodAgencia"].value,
        "UsuarioRegistro" : this._securityService.leerUsuarioWeb(),
        "UsuarioModificacion":  this._securityService.leerUsuarioWeb()
      },
      "comentarioEmision":
      {
          "IdEmision" : this.emision.idEmision,
          "IdDerivacion" : this.emision.idDerivacion,
          "IdDerivacionMovimiento" :  this.emision.idDerivacionMovimiento,
          "Texto" :  this.form.controls['Descripcion'].value,
          "UsuarioRegistro" :  this._securityService.leerUsuarioWeb(),
          "Archivo" : null,
          "NombreArchivo" : null,
          "CodTabla" : Constantes.ComentarioEmision,
          "DNI": this.emision.numeroDocumento,
          "Perfil": this._securityService.leerPerfil(),
          "CodAgencia":  this.emision.codAgencia,
          "Estado": this.emision.idEstado,
          "IdMotivo" : ''

      }
    };

    this._dialogService
    .openConfirmDialog('¿Está seguro de Derivar los documentos evaluados a Legal?')
    .afterClosed()
    .subscribe((res) => {
      if (res) {
          this.loading = true;
          this._configuracionAccion.accion(request).subscribe((data:any)=>{
            console.log(data);
            this.loading = false;
            if(data.response.success.resultado){
              this._dialogService.openMsgSuccessDialog("Se derivó correctamente");
              this.dialogRef.close({ data: true });
            }
            else{
              this._dialogService.openMsgSuccessDialog("Hubo un problema al derivar la emisión de la carta de no adeudo");
            }
          });
      }
    });




  }
}
