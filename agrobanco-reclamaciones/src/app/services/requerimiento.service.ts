import { Injectable } from '@angular/core';
import { AppService } from '../app.service';
import { environment } from 'src/environments/environment';
import { SecurityService } from './security.service';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class RequerimientoService extends AppService {

  constructor(securityService : SecurityService, http: HttpClient) {
    super(securityService,http);
  }

  listarDepartamento(token:string){
    this.endpoint = `${environment.UrlBase_ReclamosAPI}/agrobanco/reclamos/departamento`;
    console.log("departamentolistardeparta")
    console.log(token)
    return this.post(this.endpoint,null,token);
  }

  listarProvincia(CodDepartamento:string,token:string){
    var oBE = {
      "CodDepartamento": CodDepartamento
    };
    this.endpoint = `${environment.UrlBase_ReclamosAPI}/agrobanco/reclamos/provincia`;
    return this.post(this.endpoint,oBE,token);
  }

  listarDistrito(CodDepartamento:string,CodProvincia:string, token:string){
    var oBE = {
      "CodDepartamento": CodDepartamento,
      "CodProvincia": CodProvincia
    };
    this.endpoint = `${environment.UrlBase_ReclamosAPI}/agrobanco/reclamos/distrito`;
    return this.post(this.endpoint,oBE, token);
  }
  ObtenerRequerimientoDetalle<T>(datos:any,token:string):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${environment.UrlBase_ReclamosAPI}/agrobanco/reclamos/requerimiento/detalle`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos,token);
  }

  ObtenerAgencia<T>(token:string):Observable<any[]>{
    this.endpoint = `${environment.UrlBase_ReclamosAPI}/agrobanco/reclamos/agencia`;
    return this.post(this.endpoint,null,token);
  }

  ObtenerParametro<T>(token:string):Observable<any[]>{

    const datos = {
      "strGrupo" : '300,400,500,600,700'
    };
    this.endpoint = `${environment.UrlBase_ReclamosAPI}/agrobanco/reclamos/parametros`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos,token);
  }
  ObtenerRecursos<T>(datos:any,token:string):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${environment.UrlBase_ReclamosAPI}/agrobanco/reclamos/recurso`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos,token);
  }

  Registrar<T>(datos:any,token:string):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${environment.UrlBase_ReclamosAPI}/agrobanco/reclamos/requerimiento`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos,token);
  }

  reenviar_correo<T>(datos:any,token:string):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${environment.UrlBase_ReclamosAPI}/agrobanco/reclamos/requerimiento/reenviar/correo`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos,token);
  }

  /*ObtenerParametroPreguntasFrecuentes<T>():Observable<any[]>{

    const datos = {
      "strGrupo" : '1300'
    };


    this.endpoint = `${environment.UrlBase_ReclamosAPI}/agrobanco/reclamos/parametros`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }*/

  ObtenerParametroPreguntasFrecuentes<T>(token:string):Observable<any[]>{

    const datos = {
      "strGrupo" : '1300'
    };


    this.endpoint = `${environment.UrlBase_ReclamosAPI}/agrobanco/reclamos/preguntasfrecuentes`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos,token);
  }

  ConsultaRegistrar<T>(datos:any,token:string):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${environment.UrlBase_ReclamosAPI}/agrobanco/reclamos/consultaregistrar`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos,token);
  }
 
}
