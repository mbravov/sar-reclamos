﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.Reclamaciones.API.Models
{
    public class Usuario
    {
        public int CodUsuario { get; set; }
        public string UsuarioWeb { get; set; }
        public string NombresApellidos { get; set; }
        public string Perfil { get; set; }
        public string CorreoElectronico { get; set; }

        public bool? Estado { get; set; }
    }
}
