﻿using Agrobanco.SAR.Control.Base;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SAR.Control.Handlers
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate next;

        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context)
        {

            try { context.Request.EnableBuffering(); } catch { }

            try
            {
                await next(context);
            }
            catch (Exception ex)
            {

                await HandleExceptionAsync(context, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception ex)
        {
            var exceptioncontent = ex.Message;
            var arrexcepcion = exceptioncontent.Split("|");
            string exceptionCodigo;
            string exceptionMensaje;

            if (exceptioncontent.Contains(ConstantesToken.ExpiradoCodigoJWT))
            {
                exceptionCodigo = ConstantesError.ERROR_TOKEN_EXPIRADO_CODIGO;
                exceptionMensaje = exceptioncontent;
            }
            else
            {
                exceptionCodigo = arrexcepcion.Length == 1 ? ConstantesError.ERROR_NO_CONTROLADO_CODIGO : arrexcepcion[0];
                exceptionMensaje = arrexcepcion.Length == 1 ? ex.Message : arrexcepcion[1];
            }

            string mensaje = exceptionMensaje;

            ErroresControl.ManejarErrores(exceptionCodigo, out HttpStatusCode httpstatuscode, out string titulo);

            var result = JsonConvert.SerializeObject(new RespuestaError
            {
                error = new RespuestaErrorDetalle
                {
                    codigo = exceptionCodigo,
                    mensaje = mensaje,
                    titulo = titulo
                }
            });

            var mensajeexcepcion = string.Format("Mensaje: {0} , Detalle: {1}", ex.Message, ex.ToString());

            var resultexcepcioncompleta = JsonConvert.SerializeObject(new RespuestaError
            {
                error = new RespuestaErrorDetalle
                {
                    codigo = exceptionCodigo,
                    mensaje = mensajeexcepcion,
                    titulo = titulo
                }
            });

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)httpstatuscode;

            dynamic jsonrquest = null;

            if (context.Request.Body.CanSeek)
            {
                using (var body = new StreamReader(context.Request.Body))
                {
                    body.BaseStream.Seek(0, SeekOrigin.Begin);
                    var requestBody = body.ReadToEnd();
                    jsonrquest = JsonConvert.DeserializeObject(requestBody);
                }
            }



            var request = new
            {
                headers = context.Request.Headers,
                body = jsonrquest
            };

            GrabarLogError(context.Request.Path, JsonConvert.SerializeObject(request), resultexcepcioncompleta);

            return context.Response.WriteAsync(result);
        }

        private static void GrabarLogError(string api, string request, string response)
        {
            //AQUI GRABAMOS LOS LOGS DE ERROR           

            string detalleError = "SAR.API: " + " - " +
                    DateTime.Now + "\n" + "\n" +
                    api + "\n" +
                    request + "\n" +
                    response + "\n" +
                    "-------------------------------------------------------------------------------------------------------------";
            GenerarArchivoLog(detalleError);
        }

        private static void GenerarArchivoLog(string strLog)
        {
            StreamWriter log;
            FileStream fileStream = null;
            DirectoryInfo logDirInfo = null;
            FileInfo logFileInfo;

            string logFilePath = ObtenerValorParametro("APIrutaLogsError");
            logFilePath = logFilePath + "Log_SARAPI-" + System.DateTime.Today.ToString("dd-MM-yyyy") + "." + "txt";
            logFileInfo = new FileInfo(logFilePath);
            logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
            if (!logDirInfo.Exists) logDirInfo.Create();
            if (!logFileInfo.Exists)
            {
                fileStream = logFileInfo.Create();
            }
            else
            {
                fileStream = new FileStream(logFilePath, FileMode.Append);
            }
            log = new StreamWriter(fileStream);
            log.WriteLine(strLog);
            log.Close();
        }


        public static string ObtenerValorParametro(string parametro)
        {
            var parametros = ObtenerParametros();

            var valorParametro = parametros.Find(x => x.llave == parametro) == null ? "" : parametros.Find(x => x.llave == parametro).valor;
            return valorParametro;
        }

        public static List<ParametroControlDto> ObtenerParametros()
        {
            try
            {
                var configuration = new ConfigurationBuilder()
                   .SetBasePath(Directory.GetCurrentDirectory())
                   .AddJsonFile("appsettings.json", false)
                   .Build();

                var responseParametros = new List<ParametroControlDto> {
                                                        new ParametroControlDto {llave = "TokenClave" , valor = configuration["TokenClave"] },
                                                        new ParametroControlDto {llave = "TokenMinutos" , valor = configuration["TokenMinutos"] },
                                                        new ParametroControlDto {llave = "AppKey" , valor = configuration["AppKey"] },
                                                        new ParametroControlDto {llave = "AppCode" , valor = configuration["AppCode"] },
                                                        new ParametroControlDto {llave = "APIrutaLogsError" , valor = configuration["APIrutaLogsError"] },

                };

                return responseParametros;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
