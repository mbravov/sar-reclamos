export interface Requerimientoconsulta {         
    UsuarioAsignado : string;    
    CodAgencia : string;
    NroDocumento : string;
    NombresApellidos : string;
    CodTipoRequerimiento : string;
    TipoRequerimiento : string;
    Agencia : string;   
    Estado : string;
    FechaRegistroIni : string;
    FechaRegistroFin : string;
    FechaAtencionIni : string;
    FechaAtencionFin : string;
    pagina : number;
    pageSize : number;
    editar: string;  
   
}
