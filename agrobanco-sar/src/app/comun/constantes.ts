export class Constantes {
    //PARAMETROS
static readonly parametroEstado = 100;
static readonly parametroTablas = 200;
static readonly parametroTipoDocumento = 300;
static readonly parametroTipoRequerimeinto = 600;
static readonly parametroMotivoAnulacion = 800;



static readonly DNI = 1;
static readonly RUC = 2;
static readonly PASAPORTE = 3;
static readonly CE = 4;
static readonly OTROS = 5;

static readonly WHATSAPP = 1;
static readonly EMAIL = 2;
static readonly AGENCIA = 3;
static readonly DOMICILIO = 4;

static readonly Registrado = 1;
static readonly Anulado = 2;
static readonly Asignado = 3;
static readonly EnProceso = 4;
static readonly DerivadoaOCM = 5;
static readonly Aprobado = 6;
static readonly SinNotificar = 7;
static readonly Atendido = 8;
static readonly Pendiente = 9;
static readonly SinAtencion = 10;

static readonly Solicitud = 1;
static readonly Reclamo = 2;
static readonly Quejas = 3;

static readonly Requerimiento = 1;
static readonly RequerimientoMovimiento = 2;
static readonly Comentario = 3;
static readonly Consultas = 4;
static readonly Plantilla = 5;
static readonly CartaRespuesta = 6;
static readonly CartaRespuestaOCM = 7;
static readonly ComentarioEmision = 9;

static readonly OCM = 'ocm';
static readonly ACM = 'acm';
static readonly GAR = 'gar';
static readonly ADMAG = 'admag';
static readonly UDA = 'uda';
static readonly LEG = 'leg';

static readonly Whatsapp = 1;
static readonly Email = 2;
static readonly Agencia = 3;
static readonly Domicilio = 4;

static readonly CNAFisico = 1;
static readonly CNAEmail = 2;
static readonly CNAWhatsapp = 3;

static readonly EstadoEmisionAtendido = 12;
static readonly EstadoEmisionObservado = 13;
static readonly EstadoDerivacionAtendido = 22;
static readonly EstadoDerivacionObservado = 23;

static readonly EstaBloqueado = 1;
static readonly NoEstaBloqueado = 0;

static readonly PlantillaCartaRespuesta = 1; //Solicitud
static readonly PlantillaConstanciaNoAdeudo = 2;
static readonly PlantillaConstanciaDeudaVigente = 3;
static readonly PlantillaGestionCNA = 4;
static readonly PlantillaGestionCNAMinuta = 5;
static readonly PlantillaGestionCNACartaRespuesta = 6;

static readonly TipoFiltroGAR = 10;
static readonly TipoFiltroADMAG = 20;

static readonly EstadoRechado = 18;

static readonly ConfiguracionAccion_Derivacion_Masiva_ADMAG_a_UsuAge = 901; //Derivacion
}
