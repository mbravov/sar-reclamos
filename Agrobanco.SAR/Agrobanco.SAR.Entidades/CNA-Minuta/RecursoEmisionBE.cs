﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades.CNA_Minuta
{
    public class RecursoEmisionBE
    {
		public int CodRecurso { get; set; }
		public int CodTabla { get; set; }
		public Int64  ReferenciaID { get; set; }
		public int  LaserficheID { get; set; }
		public int  IdRecursoEmision { get; set; }
		public int  IdEmision { get; set; }
		public string TipoRecurso { get; set; }
		public int TipoRecursoID { get; set; }
		public string Descripcion { get; set; }
		public string UsuarioRegistro { get; set; }
		public string DNI { get; set; }
		public string TipoDocumento { get; set; }
		public string Archivo { get; set; }
		public string NombreArchivo { get; set; }
	}
}
