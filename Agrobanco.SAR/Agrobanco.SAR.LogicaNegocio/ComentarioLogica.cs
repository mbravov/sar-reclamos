﻿using Agrobanco.SAR.Comun.Laserfiche;
using Agrobanco.SAR.Datos;
using Agrobanco.SAR.Entidades;
using System;
using System.Collections.Generic;
using System.Text;


namespace Agrobanco.SAR.LogicaNegocio
{
    public class ComentarioLogica
    {
        private readonly ComentarioDatos comentarioDatos;

        public ComentarioLogica()
        {
            comentarioDatos = new ComentarioDatos();
        }

        public bool ComentarioRegistrar(ComentarioBE oBE)
        {
            return comentarioDatos.ComentarioRegistrar(oBE);
        }
        public List<ComentarioBE> ComentarioObtener(ComentarioBE oBE)
        {
            return comentarioDatos.ComentarioObtener(oBE);
        }

        public bool ComentarioRegistrarAnular(ComentarioBE oBE)
        {
            return comentarioDatos.ComentarioRegistrarAnular(oBE);
        }
        
    }
}
