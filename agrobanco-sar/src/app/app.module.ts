import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { MatSelectModule } from '@angular/material/select';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { SidebarComponent } from './components/shared/sidebar/sidebar.component';

import { AuthGuard } from './guards/auth.guard';
import { HomeComponent } from './components/home/home.component';
import { IndexComponent } from './components/redirect/index/index.component';
import { ErrorComponent } from './components/error/error.component';
import { LoadingComponent } from './components/shared/loading/loading.component';
import { GestionRequerimientoComponent } from './components/gestion-requerimiento/gestion-requerimiento.component';
import { UsuarioComponent } from './components/usuario/usuario.component';
import { MsgErrorComponent } from './components/shared/msg-error/msg-error.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RequerimientoComponent } from './components/requerimiento/requerimiento.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';


import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';

import { MatDatepickerModule } from '@angular/material/datepicker';
import { MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { HttpClientModule } from '@angular/common/http';
import { TerminosCondicionesComponent } from './components/terminos-condiciones/terminos-condiciones.component';


import {MatDialogModule} from '@angular/material/dialog';
import { FormatoFechasPipe } from './pipes/formato-fechas.pipe';
import { DialogConfirmComponent } from './components/shared/dialog-confirm/dialog-confirm.component';
import { MsgSucessComponent } from './components/shared/msg-sucess/msg-sucess.component';

import {MatRadioModule} from '@angular/material/radio';
import {MatTooltipModule} from '@angular/material/tooltip';

import { RequerimientoDetalleComponent } from './components/requerimiento-detalle/requerimiento-detalle.component';
import { MatMomentDateModule } from '@angular/material-moment-adapter';

import { RecaptchaModule } from "ng-recaptcha";
import { PopupComentarioComponent } from './components/popup-comentario/popup-comentario.component';
import { AnularComponent } from './components/anular/anular.component';
import { DerivadoAOcmComponent } from './components/derivado-a-ocm/derivado-a-ocm.component';
import { BandejaConsultaComponent } from './components/bandeja-consulta/bandeja-consulta.component';
import { ConsultaDetalleComponent } from './components/consulta-detalle/consulta-detalle.component';
import { RequerimientoSeguimientoComponent } from './components/requerimiento-seguimiento/requerimiento-seguimiento.component';
import { RequerimientoSeguimientoDetalleComponent } from './components/requerimiento-seguimiento-detalle/requerimiento-seguimiento-detalle.component';

import {MatStepperModule} from '@angular/material/stepper';
import { MglTimelineModule } from 'angular-mgl-timeline';
import { ReporteRquerimientoComponent } from './components/reporte-requerimiento/reporte-requerimiento.component';
import { ConstanciaNoAdeudoComponent } from './components/constancia-no-adeudo/constancia-no-adeudo.component';
import { ReporteGestionComponent } from './components/reporte-gestion/reporte-gestion.component';
import { UsuarioDetalleComponent } from './components/usuario-detalle/usuario-detalle.component';
import { UsuarioNuevoComponent } from './components/usuario-nuevo/usuario-nuevo.component';
import { GestionPlantillaComponent } from './components/gestion-plantilla/gestion-plantilla.component';
import { NumbersOnlyDirective } from './directivas/numbers-only.directive';
import { CnaDetalleComponent } from './components/gestion-cna-minuta/cna-detalle/cna-detalle.component';
import { GestionCnaMinutaComponent } from './components/gestion-cna-minuta/gestion-cna-minuta.component';
import { CnaComentarioComponent } from './components/gestion-cna-minuta/cna-comentario/cna-comentario.component';
import { CnaMinutaSeguimientoComponent } from './components/cna-minuta-seguimiento/cna-minuta-seguimiento.component';
import { CnaRechazarComponent } from './components/gestion-cna-minuta/cna-rechazar/cna-rechazar.component';
import { CnaDerivacionOcmComponent } from './components/gestion-cna-minuta/cna-derivacion-ocm/cna-derivacion-ocm.component';
import { CnaGarantiaSeccionComponent } from './components/gestion-cna-minuta/cna-garantia-seccion/cna-garantia-seccion.component';
import { CnaDerivarLegalComponent } from './components/gestion-cna-minuta/cna-derivar-legal/cna-derivar-legal.component';
import { CnaLegalSeccionComponent } from './components/gestion-cna-minuta/cna-legal-seccion/cna-legal-seccion.component';
import { CnaDerivarAdmageComponent } from './components/gestion-cna-minuta/cna-derivar-admage/cna-derivar-admage.component';
import { CnaDerivarComponent } from './components/gestion-cna-minuta/cna-derivar/cna-derivar.component';
import { CnaDerivarMasivaAdmageAUsuageComponent } from './components/gestion-cna-minuta/cna-derivar-masiva-admage-a-usuage/cna-derivar-masiva-admage-a-usuage.component';
import { CnaReaperturaComponent } from './components/gestion-cna-minuta/cna-reapertura/cna-reapertura.component';
import { CnaDerivarUdaComponent } from './components/gestion-cna-minuta/cna-derivar-uda/cna-derivar-uda.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    HomeComponent,
    IndexComponent,
    ErrorComponent,
    LoadingComponent,
    GestionRequerimientoComponent,
    UsuarioComponent,
    MsgErrorComponent,
    RequerimientoComponent,
    TerminosCondicionesComponent,
    FormatoFechasPipe,
    DialogConfirmComponent,
    MsgSucessComponent,
    RequerimientoDetalleComponent,
    PopupComentarioComponent,
    AnularComponent,
    DerivadoAOcmComponent,
    BandejaConsultaComponent,
    ConsultaDetalleComponent,
    RequerimientoSeguimientoComponent,
    RequerimientoSeguimientoDetalleComponent,

    ReporteRquerimientoComponent,
      ConstanciaNoAdeudoComponent,
      ReporteGestionComponent,
      UsuarioDetalleComponent,
      UsuarioNuevoComponent,

      GestionPlantillaComponent,
        NumbersOnlyDirective,

        GestionCnaMinutaComponent,
        CnaDetalleComponent,
        CnaComentarioComponent,
        CnaMinutaSeguimientoComponent,
        CnaRechazarComponent,
        CnaDerivacionOcmComponent,
        CnaGarantiaSeccionComponent,
        CnaDerivarLegalComponent,
        CnaLegalSeccionComponent,
        CnaDerivarAdmageComponent,
        CnaDerivarComponent,
        CnaDerivarMasivaAdmageAUsuageComponent,
        CnaReaperturaComponent,
        CnaDerivarUdaComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    MatPaginatorModule,
    MatTableModule,

    MatFormFieldModule,
    MatInputModule,

    MatDatepickerModule,
    MatMomentDateModule,

    HttpClientModule,
    MatDialogModule,
    MatRadioModule,
    MatTooltipModule,

    RecaptchaModule,
    MatStepperModule,
    MglTimelineModule

  ],
  providers: [AuthGuard,
    {
      provide: MAT_DATE_FORMATS,
      useValue: {
        parse: {dateInput: ['l','L']},
        display: {
          dateInput: 'L',
          monthYearLabel: 'MMM YYYY',
          dateA11yLabel: 'LL',
          monthYearA11yLabel: 'MMMM YYYY'
        }
      }
    }
  ],
  bootstrap: [AppComponent],
  entryComponents:[
    DialogConfirmComponent
  ]
})
export class AppModule { }
