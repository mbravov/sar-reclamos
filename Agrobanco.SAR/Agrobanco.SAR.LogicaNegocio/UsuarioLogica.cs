﻿using Agrobanco.SAR.Datos;
using Agrobanco.SAR.Entidades;
using Agrobanco.SAR.Entidades.UsuarioMantenimiento;
using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.LogicaNegocio
{
    public class UsuarioLogica
    {
        private readonly ParametroDatos parametroDatos;

        public UsuarioLogica()
        {
            parametroDatos = new ParametroDatos();
        }
      
        public List<Usuario> ObtenerListaUsuarios(Usuario usuario)
        {
            return parametroDatos.ListarUsuario(usuario);
        }

        public List<UsuarioMantenimientoResponse> ObtenerListaUsuariosMantenimiento(UsuarioMantenimientoFiltroResquest usuario)
        {
            return parametroDatos.ListarUsuarioMantenimiento(usuario);
        }

        public string UsuarioRegistrar(UsuarioMantenimientoResponse usuario)
        {
            return parametroDatos.UsuarioRegistrar(usuario);
        }

        public UsuarioMantenimientoResponse UsuarioDetalle(UsuarioMantenimientoResponse usuario)
        {
            return parametroDatos.UsuarioDetalle(usuario);
        }

        public string UsuarioActualizar(UsuarioMantenimientoResponse usuario)
        {
            return parametroDatos.UsuarioActualizar(usuario);
        }
                
    }
}
