﻿using Agrobanco.SAR.Datos;
using Agrobanco.SAR.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.LogicaNegocio
{
    public class AreaLogica
    {
        private readonly ParametroDatos parametroDatos;

        public AreaLogica()
        {
            parametroDatos = new ParametroDatos();
        }

        public List<AreaBE> ListarArea()
        {
            return parametroDatos.ListarArea();
        }

    }
}
