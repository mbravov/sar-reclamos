USE DBAgrSAR
GO

IF OBJECT_ID('dbo.Parametro', 'U') IS NOT NULL 
  DROP TABLE dbo.Parametro; 
GO

IF OBJECT_ID('dbo.Plantilla', 'U') IS NOT NULL 
  DROP TABLE dbo.Plantilla; 
GO

IF OBJECT_ID('dbo.Comentario', 'U') IS NOT NULL 
  DROP TABLE dbo.Comentario; 
GO

IF OBJECT_ID('dbo.RequerimientoAsignacion', 'U') IS NOT NULL 
  DROP TABLE dbo.RequerimientoAsignacion; 
GO

IF OBJECT_ID('dbo.Usuario', 'U') IS NOT NULL 
  DROP TABLE dbo.Usuario; 
GO

IF OBJECT_ID('dbo.RequerimientoDetalle', 'U') IS NOT NULL 
  DROP TABLE dbo.RequerimientoDetalle; 
GO

IF OBJECT_ID('dbo.RequerimientoMovimiento', 'U') IS NOT NULL 
  DROP TABLE dbo.RequerimientoMovimiento; 
GO

IF OBJECT_ID('dbo.Requerimiento', 'U') IS NOT NULL 
  DROP TABLE dbo.Requerimiento; 
GO

IF OBJECT_ID('dbo.Agencia', 'U') IS NOT NULL 
  DROP TABLE dbo.Agencia; 
GO

IF OBJECT_ID('dbo.Consulta', 'U') IS NOT NULL 
  DROP TABLE dbo.Consulta; 
GO

IF OBJECT_ID('dbo.Ubigeo', 'U') IS NOT NULL 
  DROP TABLE dbo.Ubigeo; 
GO

IF OBJECT_ID('dbo.Recurso', 'U') IS NOT NULL 
  DROP TABLE dbo.Recurso; 
GO

IF OBJECT_ID('dbo.Area', 'U') IS NOT NULL 
  DROP TABLE dbo.Area; 
GO

CREATE TABLE Usuario  
(
    CodUsuario INT IDENTITY(1,1),
	Perfil VARCHAR(5) NOT NULL,
    DescripcionPerfil VARCHAR(200) NOT NULL, 
    CorreoElectronico VARCHAR(100) NOT NULL,
    UsuarioWeb VARCHAR(50) NOT NULL,  
	NombresApellidos VARCHAR(200) NOT NULL,	
	Estado BIT NOT NULL,
    UsuarioRegistro VARCHAR(50) NOT NULL,
    UsuarioModificacion VARCHAR(50),
    FechaRegistro DATETIME NOT NULL,
    FechaModificacion DATETIME,
	CONSTRAINT PK_USUARIO_CODUSUARIO PRIMARY KEY (CodUsuario)
)
GO

CREATE TABLE Agencia  
(
    CodAgencia INT IDENTITY(1,1),
	CodAgenciaReferencia VARCHAR(2),
    TipoAgencia VARCHAR(100),    
    Oficina VARCHAR(100),
	Departamento VARCHAR(150),
	Provincia VARCHAR(150),
	Distrito VARCHAR(150),
	Direccion VARCHAR(200),
	Estado BIT NOT NULL,
    UsuarioRegistro VARCHAR(50),
    UsuarioModificacion VARCHAR(50),
    FechaRegistro DATETIME,
    FechaModificacion DATETIME,
	CONSTRAINT PK_AGENCIA_CODAGENCIA PRIMARY KEY (CodAgencia)
)
GO

CREATE TABLE Parametro
(
	CodParametro INT IDENTITY(1,1),
	Grupo INT NOT NULL,
	Parametro INT NOT NULL,
	Descripcion VARCHAR(200) NULL,
	Valor VARCHAR(4000) NULL,
	Referencia VARCHAR(200) NULL,
	Referencia2 VARCHAR(200) NULL,
	Orden INT NULL,
	Estado BIT NULL,
	CONSTRAINT PK_PARAMETRO_CODPARAMETRO PRIMARY KEY (CodParametro),
	INDEX IX_GRUPO_PARAMETRO UNIQUE (Grupo,Parametro) 
)
GO


CREATE TABLE Ubigeo
(
	CodUbigeo VARCHAR(6) NOT NULL,
	CodDistrito VARCHAR(6) NOT NULL,
	CodProvincia VARCHAR(4) NOT NULL,
	CodDepartamento VARCHAR(2) NOT NULL,
	Distrito VARCHAR(150) NOT NULL,
	Provincia VARCHAR(150) NOT NULL,
	Departamento VARCHAR(150) NOT NULL,
	CONSTRAINT PK_UBIGEO_CODUBIGEO PRIMARY KEY (CodUbigeo),
	 
)

GO

CREATE TABLE Requerimiento
(
	CodRequerimiento BIGINT IDENTITY(1,1),
	NroRequerimiento VARCHAR(20) NOT NULL,
	CodAgencia INT NOT NULL,
	EsMenor INT DEFAULT 0,	
	CodProducto INT NOT NULL,
	CodMotivo INT NOT NULL,
	CodTipoRequerimiento INT NOT NULL,
	CodOrigen INT NOT NULL,
	CodFormaEnvio INT NOT NULL,
	Descripcion VARCHAR(1500) NOT NULL,
	Estado INT NOT NULL,
	UsuarioBloqueo VARCHAR(50),
	EstaBloqueado BIT,
	FechaEntrega DATETIME,
	UsuarioRegistro VARCHAR(50) NOT NULL,
	UsuarioModificacion VARCHAR(50),
	FechaRegistro DATETIME NOT NULL,
	FechaModificacion DATETIME,
	CONSTRAINT PK_REQUERIMIENTO_CODREQUERIMIENTO PRIMARY KEY (CodRequerimiento),
	CONSTRAINT FK_REQUERIMIENTO_AGENCIA_CODAGENCIA FOREIGN KEY (CodAgencia) REFERENCES Agencia (CodAgencia)
)
 
GO

CREATE TABLE RequerimientoDetalle
(
	CodRequerimientoDetalle BIGINT IDENTITY(1,1),
	CodRequerimiento BIGINT NOT NULL,
	TipoDocumento INT NOT NULL,
	NroDocumento VARCHAR(20) NOT NULL,
	PrimerApellido VARCHAR(200) NULL,
	SegundoApellido VARCHAR(200) NULL,
	Nombres VARCHAR(200) NULL,
	NombresApellidos VARCHAR(200) NULL,
	RazonSocial  VARCHAR(200),
	CodUbigeo VARCHAR(6) NOT NULL,
	Direccion VARCHAR(200) NOT NULL,
	Referencia VARCHAR(200) NOT NULL,
	CorreoElectronico VARCHAR(100) NULL,
	Celular VARCHAR(20) NOT NULL,
	Telefono VARCHAR(20) NULL,
	UsuarioRegistro VARCHAR(50) NOT NULL,
	UsuarioModificacion VARCHAR(50),
	FechaRegistro DATETIME NOT NULL,
	FechaModificacion DATETIME,
	CONSTRAINT PK_CLIENTE_CODREQUERIMIENTODETALLE PRIMARY KEY (CodRequerimientoDetalle),
	INDEX IX_TIPO_DOCUMENTO (TipoDocumento),
	INDEX IX_NRO_DOCUMENTO (NroDocumento),
	CONSTRAINT FK_CLIENTE_UBIGEO_CODUBIGEO FOREIGN KEY (CodUbigeo) REFERENCES Ubigeo (CodUbigeo),
		CONSTRAINT FK_REQUERIMINTO_DETALLE_REQUERIMIENTO_CODREQUERIMIENTO FOREIGN KEY (CodRequerimiento) REFERENCES Requerimiento (CodRequerimiento)
)

GO



CREATE TABLE RequerimientoMovimiento
(
	CodRequerimientoMovimiento BIGINT IDENTITY(1,1),
	CodRequerimiento BIGINT NOT NULL,
	Estado INT NOT NULL,
	UsuarioRegistro VARCHAR(50) NOT NULL,
	UsuarioModificacion VARCHAR(50),
	FechaRegistro DATETIME NOT NULL,
	FechaModificacion DATETIME,
	CONSTRAINT PK_REQUERIMIENTO_MOVIMIENTO_CODREQUERIMIENTOMOVIMIENTO PRIMARY KEY (CodRequerimientoMovimiento),
	CONSTRAINT FK_REQUERIMIENTO_MOVIMIENTO_REQUERIMIENTO_CODREQUERIMIENTO FOREIGN KEY (CodRequerimiento) REFERENCES Requerimiento (CodRequerimiento)
)
GO
CREATE TABLE RequerimientoAsignacion
(
	CodRequerimientoAsignacion BIGINT IDENTITY(1,1),
	CodRequerimiento BIGINT NOT NULL,
	CodRequerimientoMovimiento BIGINT NULL,
	UsuarioAsignado  VARCHAR(50) NOT NULL,
	UsuarioRegistro VARCHAR(50) NOT NULL,
	UsuarioModificacion VARCHAR(50) NULL,
	FechaRegistro DATETIME NOT NULL,
	FechaModificacion DATETIME NULL,
	CONSTRAINT PK_REQUERIMIENTO_MOVIMIENTO_ASIGNACION_CODREQUERIMIENTOASIGNACION PRIMARY KEY (CodRequerimientoAsignacion),
	CONSTRAINT FK_REQUERIMIENTO_MOVIMIENTO_ASIGNACION_REQUERIMIENTO_CODREQUERIMIENTO FOREIGN KEY (CodRequerimiento) REFERENCES Requerimiento (CodRequerimiento)
)
GO

CREATE TABLE Comentario
(
	CodComentario BIGINT IDENTITY(1,1),
	CodRequerimiento BIGINT NOT NULL,
	CodRequerimientoMovimiento BIGINT NULL,
	CodMotivo INT NULL,
	CodArea INT NULL,
	Descripcion VARCHAR(300) NOT NULL,
	FechaSolicitud DATE,
	UsuarioRegistro VARCHAR(50) NOT NULL,
	UsuarioModificacion VARCHAR(50),
	FechaRegistro DATETIME NOT NULL,
	FechaModificacion DATETIME,
	CONSTRAINT PK_COMENTARIO_CODCOMENTARIO PRIMARY KEY (CodComentario),
	CONSTRAINT FK_COMENTARIO_REQUERIMIENTO_CODREQUERIMIENTO FOREIGN KEY (CodRequerimiento) REFERENCES Requerimiento (CodRequerimiento)
)


GO

CREATE TABLE Plantilla
(
	CodPlantilla BIGINT IDENTITY(1,1),
	CodTipoPlantilla INT NOT NULL,
	Estado BIT NOT NULL,
	UsuarioRegistro VARCHAR(50),
	UsuarioModificacion VARCHAR(50),
	FechaRegistro DATETIME,
	FechaModificacion DATETIME,
	CONSTRAINT PK_GESTION_PLANTILLA_CODPLANTILLA PRIMARY KEY (CodPlantilla) 
)

GO

CREATE TABLE Consulta
(
	CodConsulta INT IDENTITY(1,1),
	TipoDocumento INT NOT NULL,
	NroDocumento VARCHAR(20) NOT NULL,
	NombresCompletos VARCHAR(200) NOT NULL,
	Direccion VARCHAR(200),
	CorreoElectronico VARCHAR(100) NOT NULL,
	Celular VARCHAR(20) NOT NULL,
	Descripcion VARCHAR(1000) NOT NULL,
	Estado BIT NOT NULL,
	UsuarioRegistro VARCHAR(50) NOT NULL,
	UsuarioModificacion VARCHAR(50),
	FechaRegistro DATETIME NOT NULL,
	FechaModificacion DATETIME,
	CONSTRAINT PK_CONSULTA_CODCONSULTA PRIMARY KEY (CodConsulta)
 
)

GO

CREATE TABLE Recurso
(
	CodRecurso INT IDENTITY(1,1),
	CodTabla INT NOT NULL,
	ReferenciaID BIGINT,
	LaserficheID INT,
	Descripcion VARCHAR(100),
	Estado BIT NOT NULL,
	UsuarioRegistro VARCHAR(50),
	UsuarioModificacion VARCHAR(50),
	FechaRegistro DATETIME,
	FechaModificacion DATETIME,
	CONSTRAINT PK_RECURSO_CODRECURSO PRIMARY KEY (CodRecurso)
	 
)
GO
 
 CREATE TABLE Area  
(
    Codigo INT IDENTITY(1,1),	
    Descripcion VARCHAR(200) NOT NULL,
	UsuarioRegistro VARCHAR(50),
	UsuarioModificacion VARCHAR(50),
	FechaRegistro DATETIME,
	FechaModificacion DATETIME,
	CONSTRAINT PK_AREA_CODIGO PRIMARY KEY (Codigo)
)
GO
