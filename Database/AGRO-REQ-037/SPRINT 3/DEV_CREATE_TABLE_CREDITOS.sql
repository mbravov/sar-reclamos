USE [DBAgrSAR]
GO
 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Credito]') AND type in (N'U'))
DROP TABLE [dbo].[Credito]
GO
 

CREATE TABLE [dbo].[Credito](
	[CodCredito] [int] IDENTITY(1,1) NOT NULL,
	[CodCred] [varchar](20) NULL,
	[Saldo] [decimal](10, 4) NULL,
	[Moneda] [varchar](20) NULL,
	[Estado] [varchar](20) NULL,
	[FechaPago] [varchar](20) NULL,
	[CodCliente] [varchar](20) NULL,
	[TipoIDN] [varchar](20) NULL,
	[Identificacion] [varchar](20) NULL,
	[NomCliente] [varchar](200) NULL
) ON [PRIMARY]
GO


