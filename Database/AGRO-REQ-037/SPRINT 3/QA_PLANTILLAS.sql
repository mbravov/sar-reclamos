  USE DBAgrSARCalidad3
  GO
  --
  INSERT INTO Plantilla (CodTipoPlantilla, Estado, UsuarioRegistro, FechaRegistro )
  VALUES (1, 1, USER, GETDATE());

  INSERT INTO Recurso (CodTabla, ReferenciaID, LaserficheID, Descripcion, Estado, UsuarioRegistro, FechaRegistro)
  VALUES (5, @@IDENTITY, 35021, 'Resoluci�n 424-2019-MINAGRI �ltimo', 1, USER, GETDATE());
  --
  INSERT INTO Plantilla (CodTipoPlantilla, Estado, UsuarioRegistro, FechaRegistro )
  VALUES (1, 1, USER, GETDATE());

  INSERT INTO Recurso (CodTabla, ReferenciaID, LaserficheID, Descripcion, Estado, UsuarioRegistro, FechaRegistro)
  VALUES (5, @@IDENTITY, 35020, 'Resoluci�n 059-2020-MINAGRI �ltimo', 1, USER, GETDATE());
  --
  INSERT INTO Plantilla (CodTipoPlantilla, Estado, UsuarioRegistro, FechaRegistro )
  VALUES (1, 1, USER, GETDATE());

  INSERT INTO Recurso (CodTabla, ReferenciaID, LaserficheID, Descripcion, Estado, UsuarioRegistro, FechaRegistro)
  VALUES (5, @@IDENTITY, 35019, 'No procede levantamiento de hipoteca', 1, USER, GETDATE());
  --
  INSERT INTO Plantilla (CodTipoPlantilla, Estado, UsuarioRegistro, FechaRegistro )
  VALUES (1, 1, USER, GETDATE());

  INSERT INTO Recurso (CodTabla, ReferenciaID, LaserficheID, Descripcion, Estado, UsuarioRegistro, FechaRegistro)
  VALUES (5, @@IDENTITY, 35018, 'En blanco', 1, USER, GETDATE());
  --
  INSERT INTO Plantilla (CodTipoPlantilla, Estado, UsuarioRegistro, FechaRegistro )
  VALUES (1, 1, USER, GETDATE());

  INSERT INTO Recurso (CodTabla, ReferenciaID, LaserficheID, Descripcion, Estado, UsuarioRegistro, FechaRegistro)
  VALUES (5, @@IDENTITY, 35017, 'Campa�a de Refinanciaci�n de Condiciones Especiales', 1, USER, GETDATE());
  --
  INSERT INTO Plantilla (CodTipoPlantilla, Estado, UsuarioRegistro, FechaRegistro )
  VALUES (1, 1, USER, GETDATE());

  INSERT INTO Recurso (CodTabla, ReferenciaID, LaserficheID, Descripcion, Estado, UsuarioRegistro, FechaRegistro)
  VALUES (5, @@IDENTITY, 35016, 'Beneficiario de la Ley 30893 �ltimo', 1, USER, GETDATE());

INSERT INTO Plantilla (CodTipoPlantilla, Estado, UsuarioRegistro, FechaRegistro )
VALUES (2, 1, USER, GETDATE());

INSERT INTO Recurso (CodTabla, ReferenciaID, LaserficheID, Descripcion, Estado, UsuarioRegistro, FechaRegistro)
VALUES (5, @@IDENTITY, 35028, '01-Constancia de No Adeudo �ltimo', 1, USER, GETDATE());

--
INSERT INTO Plantilla (CodTipoPlantilla, Estado, UsuarioRegistro, FechaRegistro )
VALUES (3, 1, USER, GETDATE());

INSERT INTO Recurso (CodTabla, ReferenciaID, LaserficheID, Descripcion, Estado, UsuarioRegistro, FechaRegistro)
VALUES (5, @@IDENTITY, 35029, '02-No procede Constancia de No Adeudo �ltimo', 1, USER, GETDATE());