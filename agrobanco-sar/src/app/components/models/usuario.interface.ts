export interface Usuario{
    UsuarioWeb: string;
    Token: string;
    idPerfil: string;

    CodUsuario: string;
    NombresApellidos: string;
    Perfil: string;
    CorreoElectronico : string;
    Estado : string;
    UsuarioRegistro : string;
    FechaRegistro : string;
}