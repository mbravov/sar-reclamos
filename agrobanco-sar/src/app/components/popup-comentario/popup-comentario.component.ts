import { Component,Inject, EventEmitter, OnInit, Input,Output } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { DialogService } from 'src/app/services/comun/dialog.service';
import { RequerimientoService } from 'src/app/services/requerimiento.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Data, Router } from "@angular/router";
import { RequerimientoInterface } from '../models/requerimiento.interface';
import { RequerimientoDetalleComponent } from 'src/app/components/requerimiento-detalle/requerimiento-detalle.component';
import { SecurityService } from 'src/app/services/security.service';
import { forkJoin} from 'rxjs';
import { ParametroService } from 'src/app/services/parametro.service';

import { subscribeOn } from 'rxjs/operators';
import { Constantes } from 'src/app/comun/constantes';

declare var M:any;

@Component({
  selector: 'app-popup-comentario',
  templateUrl: './popup-comentario.component.html',
  styleUrls: ['./popup-comentario.component.css']
})
export class PopupComentarioComponent implements OnInit {

  form!: FormGroup;
  formSubmitted:boolean = false;

  lengthDescripcion: number  = 300;
  MaxlengthDescripcion: number  = 300;
  MinlengthDescripcion: number  = 5;
  MAXIMO_TAMANIO_BYTES = 5000000; // 5MB = 5 millón de bytes
  Archivo1:any;

  NombreArchivo1:any;
  fechaRegistroFinValor! : Date;
  fechaAtencionInicioValor!: Date;

  loading:boolean = false;

  parametros:any[0];
  areas: any[] = [];

  constructor(
  private formBuilder: FormBuilder,
  private dialogService: DialogService,
  private parametroService: ParametroService,
  private dialog: MatDialog,
  private requerimientoServices : RequerimientoService,
  private securityService:SecurityService,
  public dialogRef: MatDialogRef<PopupComentarioComponent>,
  @Inject(MAT_DIALOG_DATA) public data: any[],
  private router: Router
  ) {
    this.parametros = data;
  }

  ngOnInit(): void {
    this.inicializarFormulario();
    this.loadPage();
  }

  loadPage(): void {

    this.loading = true;
    forkJoin([

  this.parametroService.ObtenerAreas()
    ]).subscribe( (data: any) => {
      debugger;
        console.log(data);
        this.areas = data[0].response;

      this.loading = false;
    }, (err) => {
      this.loading = false;
      console.error(err);
    });

  }


   inicializarFormulario() : void {
    let fechaHoy = new Date();
    this.fechaRegistroFinValor = fechaHoy;
    this.fechaAtencionInicioValor = fechaHoy;

    this.form = this.formBuilder.group({
    CodArea:[null],
    Descripcion: [null,  [Validators.required, Validators.minLength(5)]],
    Fecharegistroini: new FormControl(fechaHoy, [Validators.max(this.fechaRegistroFinValor.getTime())]),
    FechaSolicitud: new FormControl(null, [Validators.min(this.fechaAtencionInicioValor.getTime())]),
    Archivo1:[null],
    NombreArchivo1:this.NombreArchivo1,

  });

  }

  ConvertStringToNumber(input: string) {
    var numeric = Number(input);
    return numeric;
}

  registrarComentario():void {
   debugger;
    this.formSubmitted = true;

    if (this.form.invalid) {
      return;
    }

    this.dialogService.openConfirmDialog('¿Está seguro de agregar este comentario?')
    .afterClosed().subscribe(res => {
      if (res) {
        const perfil = this.securityService.leerUsuarioWeb();

        const data =   {
          CodRequerimiento : this.parametros.comentario.CodRequerimiento,
          CodRequerimientoMovimiento :  this.parametros.comentario.CodRequerimientoMovimiento,
          NroRequerimiento :  this.parametros.comentario.NroRequerimiento,
          UsuarioRegistro :  perfil,
          Descripcion :  this.form.controls['Descripcion'].value,
          CodArea :  +this.form.controls['CodArea'].value,
          FechaSolicitud : this.form.controls['FechaSolicitud'].value,
          Archivo : this.Archivo1 ,
          NombreArchivo : this.NombreArchivo1,
          Estado :  this.parametros.comentario.Estado,
          CodTabla : Constantes.Comentario
        }

        console.log("request",data);
        this.loading = true;
        this.requerimientoServices.RegistrarComentario(data).subscribe(resultado=>{
          console.log("response",resultado);
          if(resultado.response){
            this.dialogService
          .openMsgSuccessDialog('Se registró correctamente el comentario')
          .afterClosed()
          .subscribe((res) => {
            if(res){
             /*
                if(this.parametros.comentario.Estado==Constantes.Asignado)
                this.router.navigate(['/gestion']);
              */
              this.onClick(data);
              //this.router.navigate(['/gestion/requerimiento-detalle',this.parametros.comentario.NroRequerimiento]);

            }
          });

          }
          else{

          }


        });

        /*
            this.requerimientoServices.ObtenerComentario(data).subscribe(res => {
              console.log("response",res.response);

              const param ={
                CodRequerimientoMovimiento :  0,
                CodRequerimiento : this.parametros.comentario.CodRequerimiento,
                Estado : 4,
                UsuarioRegistro :  perfil

                };

              if(res.response.length == 0)
              {
                this.requerimientoServices.RegistrarMovimiento(param).subscribe(res => {
                  console.log("response",res.response);

                  data.CodRequerimientoMovimiento = res.response;



                  //if(this.parametros.comentario.Estado == 3){

                    this.requerimientoServices.ActualizarEstados(param).subscribe(result => {
                    console.log("response",result.response);
                    });
               /// }

                });

              }
              if(res.response.length >= 1){

                 data.CodRequerimientoMovimiento = res.response[0].codRequerimientoMovimiento;

                 this.loading = true;
                 this.requerimientoServices.RegistrarComentario(data).subscribe(resultado=>{
                  debugger;
                  console.log("response",resultado);

                  //this.dialogService.openMsgSuccessDialog("Se registro correctamente.");

                  this.onClick(data);

                });

              }

            });

        */
      }

    });

  }

  onClick(data: Data): void {
    this.dialogRef.close({ data: data });
  }

  handleUpload(event:any,numfile:number) {
    debugger;


        const file = event.target.files[0];
        if(file==null || file.length <=0){
          switch (numfile) {
            case 1:
            this.Archivo1 = null;
            this.NombreArchivo1 = null;
            break;

            default:
              break;
        }

         return;
       }

        if (file.size > this.MAXIMO_TAMANIO_BYTES) {
          const tamanioEnMb = this.MAXIMO_TAMANIO_BYTES / 1000000;

          switch (numfile) {
            case 1:
              this.Archivo1 =  null;
              this.NombreArchivo1 = null;
             this.form.controls['Archivo1'].setValue(null);
            break;

            default:
              break;
          }
          this.dialogService.openMsgErrorDialog(`El tamaño máximo es ${tamanioEnMb} MB`);


          return;
        } else {
          // Validación pasada. Envía el formulario o haz lo que tengas que hacer
        }

        const FileNamme = file.name;
        switch (numfile) {
          case 1:
           this.NombreArchivo1 = FileNamme;
          break;

          default:
            break;
        }

        const reader = new FileReader();
        console.log("file",file);
        reader.readAsDataURL(file);
        reader.onload = () => {
          console.log("reader",reader);
          switch (numfile) {
            case 1:
             this.Archivo1 = reader.result || null;
            break;

            default:
              break;
          }

            console.log(reader.result);
        };
  }

  onChangeFechaSolicitudIni(){
      this.fechaAtencionInicioValor = this.form.controls['FechaSolicitud'].value;
  }

}
