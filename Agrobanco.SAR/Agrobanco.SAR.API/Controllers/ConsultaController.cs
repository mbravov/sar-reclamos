﻿using Agrobanco.SAR.API.Models;
using Agrobanco.SAR.Control.Filters;
using Agrobanco.SAR.Entidades;
using Agrobanco.SAR.LogicaNegocio;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.SAR.API.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/Consulta")]
    [ApiController]

    public class ConsultaController : ControllerBase
    {
        private readonly ConsultaLogica consultaLogica;
        public ConsultaController()
        {
            consultaLogica = new ConsultaLogica();

        }

        [HttpPost("listar")]
        [ProducesResponseType(200, Type = typeof(List<Usuario>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult ConsultasListar([Required][FromHeader(Name = "Token")] string token, [FromBody] ConsultaBE oConsulta)
        {
            try
            {                
             
                var listaResultado = consultaLogica.ConsultasListar(oConsulta);

                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("consultadetalle")]
        [ProducesResponseType(200, Type = typeof(List<Usuario>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult ConsultaDetalle([Required][FromHeader(Name = "Token")] string token, [FromBody] ConsultaBE oConsulta)
        {
            try
            {

                var listaResultado = consultaLogica.ConsultaDetalle(oConsulta);

                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }       
        

    }
}
