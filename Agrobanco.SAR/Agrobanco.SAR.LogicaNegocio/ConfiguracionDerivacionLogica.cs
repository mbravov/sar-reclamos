﻿using Agrobanco.SAR.Comun;
using Agrobanco.SAR.Comun.GeneradorDocumento;
using Agrobanco.SAR.Comun.Laserfiche;
using Agrobanco.SAR.Datos;
using Agrobanco.SAR.Entidades;
using Agrobanco.SAR.Entidades.CNA_Minuta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SAR.LogicaNegocio
{
    public class ConfiguracionDerivacionLogica
    {
        private readonly ConfiguracionDerivacionDatos _configuracionDerivacionDatos;
        private readonly DerivacionDatos _derivacionDatos;
        private readonly EmisionDatos _emisionDatos;
        private readonly ComentarioEmisionDatos _comentarioEmisionDatos;
        private readonly CNAMasivoDatos _CNAMasivoDatos;
        private readonly RecursoEmisionDatos _recursoEmisionDatos;
        public ConfiguracionDerivacionLogica()
        {
            _configuracionDerivacionDatos = new ConfiguracionDerivacionDatos();
            _derivacionDatos = new DerivacionDatos();
            _emisionDatos = new EmisionDatos();
            _comentarioEmisionDatos = new ComentarioEmisionDatos();
            _CNAMasivoDatos = new CNAMasivoDatos();
            _recursoEmisionDatos = new RecursoEmisionDatos();
        }

        public async Task<bool> Accion(ConfiguracionDerivacionBE request,string webRootPath)
        {
            bool resultado = false;
            try
            {
                var listado = await _configuracionDerivacionDatos.Obtener(request);
                

                if (listado != null || listado.Count > 0)
                {
                    List<Task> tasks = new List<Task>();
                    foreach (var item in listado)
                    {
                        if (item.TipoEvento == ConstantesTipoEvento.RegistroEmision)
                        {
                            request.emision.Estado = item.IdEstado;                           

                            resultado = await _emisionDatos.Insertar(request.emision);
                        }

                        if (item.TipoEvento == ConstantesTipoEvento.RegistroDerivacion)
                        {
                            request.derivacion.Estado = item.IdEstado;
                            request.derivacion.Perfil = item.Perfil;

                            DerivacionBE derivacionBE = new DerivacionBE();
                            derivacionBE.IdEmision = request.derivacion.IdEmision;
                            derivacionBE.Perfil = request.derivacion.Perfil;
                            derivacionBE.Estado = request.derivacion.Estado;
                            derivacionBE.CodUsuarioDerivado = (item.Perfil != ConstantesPerfil.UDA.ToString())?null:request.derivacion.CodUsuarioDerivado;
                            derivacionBE.CodAgencia = request.derivacion.CodAgencia;
                            derivacionBE.UsuarioRegistro = request.derivacion.UsuarioRegistro;


                            resultado = await _derivacionDatos.Insertar(derivacionBE);
                        }

                        if (item.TipoEvento == ConstantesTipoEvento.ActualizacionEmision)
                        {
                            request.emision.Estado = item.IdEstado;
                            resultado = await _emisionDatos.Actualizar(request.emision);
                        }

                        if (item.TipoEvento == ConstantesTipoEvento.ActualizacionDerivacion)
                        {
                            request.derivacion.Estado = item.IdEstado;
                            request.derivacion.Perfil = item.Perfil;
                            request.derivacion.IdConfiguracionDerivacion = item.IdConfiguracionDerivacion;

                            DerivacionBE derivacionBE = new DerivacionBE();
                            derivacionBE.IdDerivacion = request.derivacion.IdDerivacion;
                            derivacionBE.IdEmision = request.derivacion.IdEmision;
                            derivacionBE.Perfil = request.derivacion.Perfil;
                            derivacionBE.Estado = request.derivacion.Estado;
                            derivacionBE.PerfilDerivado = request.derivacion.PerfilDerivado;
                            derivacionBE.CodAgencia = request.derivacion.CodAgencia;
                            derivacionBE.UsuarioModificacion = request.derivacion.UsuarioModificacion;
                            derivacionBE.IdConfiguracionDerivacion = request.derivacion.IdConfiguracionDerivacion;
                            derivacionBE.CodUsuarioDerivado = (item.Perfil != ConstantesPerfil.UDA.ToString()) ? null : request.derivacion.CodUsuarioDerivado;

                            resultado = await _derivacionDatos.Actualizar(derivacionBE);
                         
                        }                        
                        
                        if (item.TipoEvento == ConstantesTipoEvento.NotificacionCorreo)
                        {
                            EnvioCorreoCNA oEnviarCorreo = new EnvioCorreoCNA();
                            List<string> listaCorreo = new List<string>();
                            List<string> listaCorreocopia = new List<string>();

                             
                            string cuerpocorreo = string.Empty;

                            string strBody = string.Empty;
                            string strAsunto = string.Empty;
                            bool blRechazo = false;

 
                            if(item.IdEstado== ConstantesEstadoGestionCNA.DerivarADMAG || item.IdEstado == ConstantesEstadoGestionCNA.DerivarGAR)
                            {
                                strAsunto = "La OCM ha derivado la atención del cliente " + request.Nombres + " - " + request.TipoDocumento + " " + request.NroDocumento;
                                strBody = "La OCM ha derivado la atención del cliente " + request.Nombres + " - " + request.TipoDocumento + " " + request.NroDocumento + " el cual se encuentra en estado Pendiente en su bandeja del Sistema de Atención de Reclamos. ";
                            }

                            if (item.IdEstado == ConstantesEstadoGestionCNA.DerivarLEG)
                            {
                                strAsunto = "La División de Garantías ha derivado la atención del cliente " + request.Nombres;
                                strBody = "La División de Garantías ha derivado para la atención de emisión de minuta de levantamiento de garantía del cliente " + request.Nombres + " por encontrarse conforme para su emisión el cual se encuentra en estado Pendiente en su bandeja del Sistema de Atención de Reclamos.";
                            }

                            if (item.IdEstado == ConstantesEstadoGestionCNA.RechazarGAR )
                            {
                                strAsunto = "La División de Garantías ha rechazado la atención del cliente " + request.Nombres + " - " + request.TipoDocumento + " " + request.NroDocumento;
                                strBody = "La División de Garantías ha rechazado la atención del cliente " + request.Nombres + " - " + request.TipoDocumento + " " + request.NroDocumento + " el cual se encuentra en estado Rechazado en su bandeja del Sistema de Atención de Reclamos.";
                                blRechazo = true;
                            }
                            if (item.IdEstado == ConstantesEstadoGestionCNA.RechazarLEG)
                            {
                                strAsunto = "Legal ha rechazado la atención del cliente " + request.Nombres + " - " + request.TipoDocumento + " " + request.NroDocumento;
                                strBody = "Legal ha rechazado la atención del cliente " + request.Nombres + " - " + request.TipoDocumento + " " + request.NroDocumento + " el cual se encuentra en estado Rechazado en su bandeja del Sistema de Atención de Reclamos.";
                                blRechazo = true;
                            }
                            if (item.IdEstado == ConstantesEstadoGestionCNA.RechazarADMAGE)
                            {
                                strAsunto = "El Administrador de Agencias ha rechazado la atención del cliente " + request.Nombres + " - " + request.TipoDocumento + " " + request.NroDocumento;
                                strBody = "El Administrador de Agencias ha rechazado la atención del cliente " + request.Nombres + " - " + request.TipoDocumento + " " + request.NroDocumento + " el cual se encuentra en estado Rechazado en su bandeja del Sistema de Atención de Reclamos";
                                blRechazo = true;
                            }
                            if (item.IdEstado == ConstantesEstadoGestionCNA.RechazarUDA)
                            {
                                strAsunto = "El Usuario de Agencia ha rechazado la atención del cliente " + request.Nombres + " - " + request.TipoDocumento + " " + request.NroDocumento;
                                strBody = "El Usuario de Agencia ha rechazado la atención del cliente " + request.Nombres + " - " + request.TipoDocumento + " " + request.NroDocumento + " el cual se encuentra en estado Rechazado en su bandeja del Sistema de Atención de Reclamos";
                                blRechazo = true;
                            }
                            if (item.IdEstado == ConstantesEstadoGestionCNA.EnvarCRUDA)
                            {
                                strAsunto = "La OCM ha rechazado la atención del cliente " + request.Nombres + " - " + request.TipoDocumento + " " + request.NroDocumento;
                                strBody = "La OCM ha rechazado la atención del cliente " + request.Nombres + " - " + request.TipoDocumento + " " + request.NroDocumento + " el cual se encuentra en estado Rechazado en su bandeja del Sistema de Atención de Reclamos";
                            
                            }
                            if (item.IdEstado == ConstantesEstadoGestionCNA.DerivarADMAGDesdeLegal)
                            {
                                strAsunto = "Legal ha derivado la atención del cliente " + request.Nombres + " - " + request.TipoDocumento + " " + request.NroDocumento + " - Agencia " + request.derivacion.Agencia;
                                strBody = "Legal ha realizado el envío de la minuta de levantamiento de garantía por Olva del cliente: " + request.Nombres + " - " + request.TipoDocumento + " " + request.NroDocumento + " para su entrega. Adicionalmente, la Constancia de No Adeudo se encuentra en estado pendiente en su bandeja del Sistema de Atención de Reclamos para su notificación.";
                                
                            }

                            if(item.IdEstado == ConstantesEstadoGestionCNA.DerivarAFN)
                            {
                                strAsunto = "El Administrador de Agencia ha derivado la gestión para su notificación al cliente " + request.Nombres + " - " + request.TipoDocumento + " " + request.NroDocumento + " - Agencia " + request.derivacion.Agencia;
                                strBody = "El Administrador de Agencia ha derivado la atención del cliente: " + request.Nombres + " - " + request.TipoDocumento + " " + request.NroDocumento + " el cual se encuentra en estado Pendiente en su bandeja del Sistema de Atención de Reclamos.";
                            }

                            if (request.emision.MedioNotificacion == ConstantesMedioNotificacion.Whatsapp && request.emision.MedioNotificacion == item.IdEstado)
                            {
                                strAsunto = "El Usuario de Agencia ha registrado que la notificación se efectúe por un medio electrónico.";
                                strBody = "El Usuario de Agencia proponente desea que la notificación al cliente " + request.Nombres + " se efectúe mediante Whatsapp al número telefónico " + request.emision.Whatsapp;
                                blRechazo = true;
                            }

                            if (request.emision.MedioNotificacion == ConstantesMedioNotificacion.Email && request.emision.MedioNotificacion == item.IdEstado)
                            {
                                strAsunto = "El Usuario de Agencia ha registrado que la notificación se efectúe por un medio electrónico.";
                                strBody = "El Usuario de Agencia proponente desea que la notificación al cliente  " + request.Nombres + "  se efectúe mediante correo electrónico a la siguiente casilla electrónica: " + request.emision.Email;
                                blRechazo = true;
                            }

                            cuerpocorreo = EnvioCorreoCuerpo.GenerarCorreoConfiguracionAccion(
                                strBody,
                                webRootPath);

                            UsuarioLogica usuarioLogica = new UsuarioLogica();
                            Usuario oRequesUsuario = new Usuario();
                            oRequesUsuario.Perfil = item.Perfil;
                            oRequesUsuario.Estado = true;
                            var usuarios = usuarioLogica.ObtenerListaUsuarios(oRequesUsuario);
                            var newusuarios = new List<Usuario>();

                            if (request.derivacion.CodUsuarioDerivado != null)
                            {
                                newusuarios = usuarios.Where(x => x.CodUsuario == request.derivacion.CodUsuarioDerivado).ToList();
                            }
                            else
                            {
                                newusuarios = usuarios.Where(x => x.CodAgencia == request.derivacion.CodAgencia).ToList();
                            }


                            if (blRechazo) newusuarios = usuarios;
                            
                            foreach (var usuario in newusuarios)
                            {
                                listaCorreo.Add(usuario.CorreoElectronico);
                            }
                          
                            resultado = oEnviarCorreo.EnviarCorreo(listaCorreo, listaCorreocopia, cuerpocorreo, strAsunto);
                           
                        }
                        if (item.TipoEvento == ConstantesTipoEvento.RegistroComentarioEmision)
                        {
                            request.comentarioEmision.Estado = item.IdEstado;
                            request.comentarioEmision.Perfil = item.Perfil;
                            await _comentarioEmisionDatos.InsertarAccion(request.comentarioEmision);
                        }
                        if(item.TipoEvento == ConstantesTipoEvento.CNAConvertirWordToPDF)
                        {
                            RecursoEmisionBE oRecursoEmisionBE = new RecursoEmisionBE();
                            oRecursoEmisionBE.IdEmision = request.emision.IdEmision;
                            List<RecursoEmisionBE> lstRecursoEmisionBE = await _recursoEmisionDatos.Obtener(oRecursoEmisionBE);


                            GeneradorDocumento oGeneradorDocumento = new GeneradorDocumento();

                            RecursoEmisionBE oBE = lstRecursoEmisionBE.Where(x => x.TipoRecursoID == ConstantesTipoRecursoEmision.CartadeNoAdeudo).FirstOrDefault();
                            if (oBE != null || oBE.LaserficheID>0)
                            {
                                string LaserFicheNombre = string.Empty;
                                var bytes =   oGeneradorDocumento.ConvertWordtoPDF(oBE.LaserficheID.ToString(), out LaserFicheNombre);

                                if (!string.IsNullOrEmpty(bytes))
                                {
                                    RecursoEmisionBE RecursoEmisionrequest = new RecursoEmisionBE();

                                    RecursoEmisionrequest.CodRecurso = 0;
                                    RecursoEmisionrequest.CodTabla = 8;
                                    RecursoEmisionrequest.ReferenciaID = 0;
                                    RecursoEmisionrequest.LaserficheID = 0;
                                    RecursoEmisionrequest.IdRecursoEmision = 0;
                                    RecursoEmisionrequest.IdEmision = request.emision.IdEmision;
                                    RecursoEmisionrequest.TipoRecursoID = 2;
                                    RecursoEmisionrequest.UsuarioRegistro = request.emision.UsuarioRegistro;
                                    RecursoEmisionrequest.DNI = request.NroDocumento; // + this.CreditoClienteDetalle.numeroDocumento,
                                    RecursoEmisionrequest.Archivo = bytes;
                                    RecursoEmisionrequest.NombreArchivo = LaserFicheNombre;
                                    RecursoEmisionrequest.TipoDocumento = request.TipoDocumento;

                                    await _recursoEmisionDatos.Insert(RecursoEmisionrequest);

                                    Documento oDocumento = new Documento();
                                    LaserficheProxy laserfiche = new LaserficheProxy();

                                    oDocumento.CodigoLaserfiche = Convert.ToInt32(oBE.LaserficheID);
                                    var documento = laserfiche.RemoveFile(oDocumento);

                                    RecursoDatos oRecursoDatos = new RecursoDatos();
                                    RecursoBE oRecursoBE = new RecursoBE();
                                    oRecursoBE.LaserficheID = oDocumento.CodigoLaserfiche;
                                    oRecursoDatos.Eliminar(oRecursoBE);
                                }
                               
                            }
                            //foreach (RecursoEmisionBE oBE in lstRecursoEmisionBE)
                            //{
                            //    if(oBE.TipoRecursoID== ConstantesTipoRecursoEmision.CartadeNoAdeudo)
                            //    {
                                 

                                  

                            //        break;
                            //    }
                            //}
                        }
                        
                        if(item.TipoEvento == ConstantesTipoEvento.ActualizarRecursoEmision)
                        {
                            RecursoEmisionBE oRecursoEmisionBE = new RecursoEmisionBE();
                            oRecursoEmisionBE.IdEmision = request.emision.IdEmision;
                            List<RecursoEmisionBE> lstRecursoEmisionBE = await _recursoEmisionDatos.Obtener(oRecursoEmisionBE);

                            foreach (var recursoemision in lstRecursoEmisionBE)
                            {
                               recursoemision.TipoRecursoID = recursoemision.TipoRecursoID+7;
                               await  _recursoEmisionDatos.Insert(recursoemision);
                            }
                        }
                    }

                    resultado = true;
                }
            }
            catch (AggregateException aex)
            {
                throw aex;
            }
            catch (Exception ex)
            {

                throw ex;
            }
           

            return await Task.Run(() => resultado);
        }


        public async Task<bool> AccionFinal(ConfiguracionDerivacionBE request, string webRootPath)
        {
            bool resultado = false;
            try
            {
                var listado = await _configuracionDerivacionDatos.Obtener(request);

                if (listado != null || listado.Count > 0)
                {
                    List<Task> tasks = new List<Task>();
                    foreach (var item in listado)
                    {                        

                        if (item.TipoEvento == ConstantesTipoEvento.ActualizacionEmision )
                        {
                           if (request.emision.Estado == item.IdEstado)
                                resultado = await _emisionDatos.Actualizar(request.emision);
                        }

                        if (item.TipoEvento == ConstantesTipoEvento.ActualizacionDerivacion)
                        {
                            if (request.derivacion.Estado == item.IdEstado)
                            {
                                request.derivacion.Perfil = item.Perfil;
                                request.derivacion.IdConfiguracionDerivacion = item.IdConfiguracionDerivacion;
                                resultado = await _derivacionDatos.Actualizar(request.derivacion);
                            }
                        }
                        
                        if (item.TipoEvento == ConstantesTipoEvento.NotificacionCorreo 
                            && request.EsAsociadoGarantia == 1
                            && request.emision.Estado == ConstantesEstadoGestionCNA.EstadoAtendidoEmision)
                        {
                            EnvioCorreoCNA oEnviarCorreo = new EnvioCorreoCNA();
                            List<string> listaCorreo = new List<string>();
                            List<string> listaCorreocopia = new List<string>();


                            string cuerpocorreo = string.Empty;

                            string strBody = string.Empty;
                            string strAsunto = string.Empty;

                            strAsunto = "La OCM ha finalizado la atención del cliente " + request.Nombres;
                            strBody = "El Usuario de Agencia proponente ha realizado la entrega de la Carta de No Adeudo y de la minuta de Levantamiento de Hipoteca correspondiente al cliente  " + request.Nombres;
                            
                            cuerpocorreo = EnvioCorreoCuerpo.GenerarCorreoConfiguracionAccion(
                                strBody,
                                webRootPath);

                            UsuarioLogica usuarioLogica = new UsuarioLogica();
                            Usuario oRequesUsuario = new Usuario();
                            oRequesUsuario.Perfil = item.Perfil;
                            oRequesUsuario.Estado = true;
                            var usuarios = usuarioLogica.ObtenerListaUsuarios(oRequesUsuario);                           

                            foreach (var usuario in usuarios)
                            {
                                listaCorreo.Add(usuario.CorreoElectronico);
                            }

                            resultado = oEnviarCorreo.EnviarCorreo(listaCorreo, listaCorreocopia, cuerpocorreo, strAsunto);

                        }

                    }

                    resultado = true;
                }
            }
            catch (AggregateException aex)
            {
                throw aex;
            }
            catch (Exception ex)
            {

                throw ex;
            }


            return await Task.Run(() => resultado);
        }

        public async Task<bool> AccionMasivaADMAG(CNAMasivoBE request, string webRootPath)
        {
            bool resultado = false;
            try
            {
                ConfiguracionDerivacionBE oConfiguracionDerivacionBE = null;
                EmisionBE oEmisionBE = null;
                DerivacionBE oDerivacionBE = null;
                ComentarioEmisionBE oComentarioEmisionBE = null;
                var listado = await _CNAMasivoDatos.Listar(request);

                if (listado != null)
                {
                    if (listado.Count > 0)
                    {
                        foreach (var item in listado)
                        {
                            try
                            {
                                oConfiguracionDerivacionBE = new ConfiguracionDerivacionBE();
                                oConfiguracionDerivacionBE.IdConfiguracionAccion = request.IdConfiguracionAccion;

                                oEmisionBE = new EmisionBE();
                                oEmisionBE.IdEmision = item.IdEmision;
                                oEmisionBE.IdCreditoCliente = item.IdCreditoCliente;
                                oEmisionBE.Estado = item.IdEstado;
                                oEmisionBE.TipoFlujo = 1;
                                oEmisionBE.UsuarioRegistro = request.UsuarioRegistro;
                                oEmisionBE.UsuarioModificacion = request.UsuarioRegistro;

                                oDerivacionBE = new DerivacionBE();
                                oDerivacionBE.IdDerivacion = item.IdDerivacion;
                                oDerivacionBE.IdEmision = item.IdEmision;
                                oDerivacionBE.Perfil = item.Perfil;
                                oDerivacionBE.Estado = item.IdEstado;
                                oDerivacionBE.PerfilDerivado = request.PerfilDerivado;
                                oDerivacionBE.UsuarioRegistro = request.UsuarioRegistro;
                                oDerivacionBE.UsuarioModificacion = request.UsuarioRegistro;
                                oDerivacionBE.Agencia = item.Agencia;
                                oDerivacionBE.CodAgencia = item.CodAgencia;
                                oDerivacionBE.IdConfiguracionDerivacion = request.IdConfiguracionAccion;
                                oDerivacionBE.CodUsuarioDerivado = request.CodUsuarioDerivado;

                                oComentarioEmisionBE = new ComentarioEmisionBE();
                                oComentarioEmisionBE.IdEmision = item.IdEmision;
                                oComentarioEmisionBE.IdDerivacion = item.IdDerivacion;
                                oComentarioEmisionBE.Texto = request.Texto;
                                oComentarioEmisionBE.UsuarioRegistro = request.UsuarioRegistro;
                                oComentarioEmisionBE.DNI = item.NroDocumento;
                                oComentarioEmisionBE.Perfil = item.Perfil;


                                oConfiguracionDerivacionBE.emision = oEmisionBE;
                                oConfiguracionDerivacionBE.derivacion = oDerivacionBE;
                                oConfiguracionDerivacionBE.comentarioEmision = oComentarioEmisionBE;

                                oConfiguracionDerivacionBE.NroDocumento = item.NroDocumento;
                                oConfiguracionDerivacionBE.TipoDocumento = item.TipoDocumento;
                                oConfiguracionDerivacionBE.Nombres = item.NombreApellidos;

                                await Accion(oConfiguracionDerivacionBE, webRootPath);


          


                                resultado = true;
                            }
                            catch (Exception ex)
                            {

                                throw ex;
                            }
                           
                        }
                    }
                }
                
            }
            catch (AggregateException aex)
            {
                throw aex;
            }
            catch (Exception ex)
            {

                throw ex;
            }


            return await Task.Run(() => resultado);
        }

    }
}
