export const environment = {
  production: true,
  UrlBase_SGSAPI:  (window as any)["env"]["UrlBase_SGSAPI"],
  UrlBase_SSA: (window as any)["env"]["UrlBase_SSA"],
  UrlBase_SARAPI:  (window as any)["env"]["UrlBase_SARAPI"],

  AppKey:  (window as any)["env"]["AppKey"],
  AppCode :  (window as any)["env"]["AppCode"],
  recaptcha_siteKey :  (window as any)["env"]["recaptcha_siteKey"],
  number_whatsapp :  (window as any)["env"]["number_whatsapp"],
  preguntas_frecuentes: (window as any)["env"]["preguntas_frecuentes"],
  redireccion: (window as any)["env"]["redireccion"]
};
