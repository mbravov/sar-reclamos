import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogService } from '../../../services/comun/dialog.service';
import { SecurityService } from '../../../services/security.service';
import { CnaMinutaOcmService } from '../../../services/cna-minuta-ocm.service';
import { FiltroAccionService } from '../../../services/filtro-accion.service';
import { ConfiguracionAccionService } from '../../../services/configuracion-accion.service';
import { forkJoin } from 'rxjs';
import { Constantes } from 'src/app/comun/constantes';

@Component({
  selector: 'app-cna-derivar-admage',
  templateUrl: './cna-derivar-admage.component.html',
  styleUrls: ['./cna-derivar-admage.component.css']
})
export class CnaDerivarAdmageComponent implements OnInit {

  form!: FormGroup;
  formSubmitted:boolean = false;

  lengthDescripcion: number  = 300;
  MaxlengthDescripcion: number  = 300;
  MinlengthDescripcion: number  = 5;

  IdConfiguracionAccion:any;
  emision:any;
  agencias!:any[];
  FechaEntregaOlva:any;

  loading:boolean = false;

  filtroPerfil!:any[];

  constructor(
    private _formBuilder: FormBuilder,
    private _dialogService: DialogService,
    private _securityService: SecurityService,
    private _cnaMinutaOcmService : CnaMinutaOcmService,
    private _filtroAccionService : FiltroAccionService,
    private _configuracionAccion : ConfiguracionAccionService,
    public dialogRef: MatDialogRef<CnaDerivarAdmageComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.emision = data.CreditoClienteDetalle;
    this.IdConfiguracionAccion = data.IdConfiguracionAccion;
    this.FechaEntregaOlva = data.FechaEntregaOlva;

   }

  ngOnInit(): void {
    this.inicializarFormulario();
    this.pageLoad();
  }

  pageLoad():void{
    let params = {
      Perfil : this._securityService.leerPerfil(),
      UsuarioWeb : this._securityService.leerUsuarioWeb()
    }
    this.loading = true;
    forkJoin([
      this._cnaMinutaOcmService.ObtenerAgencias(params),
      this._filtroAccionService.obtener()

    ]).subscribe( (data:any) => {
        this.agencias = data[0].response;
        this.filtroPerfil = data[1].response;
        this.loading = false;
    }, (error) =>{
      this.loading = false;
      console.log(error);
    });

  }
  inicializarFormulario() : void {
    console.log(this.emision.codAgencia);

    this.form = this._formBuilder.group({
      NombreCompletos:[this.emision.nombres],
      nroDocumento:[this.emision.numeroDocumento],
      CodAgencia:[""+this.emision.codAgencia,  [Validators.required]],
      Descripcion: [null,  [Validators.required, Validators.minLength(5)]],

    });

    this.form.controls['NombreCompletos'].disable();
    this.form.controls['nroDocumento'].disable();

}

derivar(){

  this.formSubmitted = true;

  if (this.form.invalid) {
    return;
  }


  var request = {
    "IdConfiguracionAccion" : this.IdConfiguracionAccion,
    "GrupoFiltro" : null,
    "Nombres": this.emision.nombres,
    "TipoDocumento": this.emision.tipoDocumento,
    "NroDocumento": this.emision.numeroDocumento,
    "emision": {
      "IdEmision" : +this.emision.idEmision,
      "IdCreditoCliente"  : +this.emision.idCreditoCliente,
      "Estado"  : this.emision.idEstado,
      "TipoFlujo"  :1,
      "strFechaEntregaOlva" : this.FechaEntregaOlva,
      "MedioNotificacion": null,
      "Email" : null,
      "Whatsapp" : null,
      "FechaCargoEntrega" : null,
      "UsuarioRegistro" :  this._securityService.leerUsuarioWeb(),
      "UsuarioModificacion" :  this._securityService.leerUsuarioWeb()
    },
    "derivacion": {
      "IdDerivacion" : +this.emision.idDerivacion,
      "IdEmision" : +this.emision.idEmision,
      "Perfil" : this._securityService.leerPerfil(),
      "Estado" : this.emision.idEstado,
      "PerfilDerivado" : Constantes.ADMAG.toUpperCase(),
      "FechaDerivado" : null,
      "CodAgencia" : +this.form.controls["CodAgencia"].value,
      "Agencia": this.emision.agencias,
      "UsuarioRegistro" : this._securityService.leerUsuarioWeb(),
      "UsuarioModificacion":  this._securityService.leerUsuarioWeb()
    },
    "comentarioEmision":
    {
        "IdEmision" : this.emision.idEmision,
        "IdDerivacion" : this.emision.idDerivacion,
        "IdDerivacionMovimiento" :  this.emision.idDerivacionMovimiento,
        "Texto" :  this.form.controls['Descripcion'].value,
        "UsuarioRegistro" :  this._securityService.leerUsuarioWeb(),
        "Archivo" : null,
        "NombreArchivo" : null,
        "CodTabla" : Constantes.ComentarioEmision,
        "DNI": this.emision.numeroDocumento,
        "Perfil": this._securityService.leerPerfil(),
        "CodAgencia":  this.emision.codAgencia,
        "Estado": this.emision.idEstado,
        "IdMotivo" : ''

    }
  };

  this._dialogService
  .openConfirmDialog('¿Está seguro de Derivar a Administrador de Agencia?')
  .afterClosed()
  .subscribe((res) => {
    if (res) {
        this.loading = true;
        this._configuracionAccion.accion(request).subscribe((data:any)=>{
          this.loading = false;
          debugger;
          if(data.response.success.resultado){
            this._dialogService.openMsgSuccessDialog("se derivó correctamente");
            this.dialogRef.close({ data: true });
          }
          else{
            this._dialogService.openMsgSuccessDialog("Hubo un problema al derivar la emisión de la carta de no adeudo");
          }
        });
    }
  });




}


}
