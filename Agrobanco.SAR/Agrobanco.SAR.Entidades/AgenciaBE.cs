﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades
{
    public class AgenciaBE
    {
        public int CodAgencia { get; set; }
        public string Oficina { get; set; }
        public string Direccion { get; set; }
    }
}
