﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades
{
    public class ConsultaBE
    {
        public string CodConsulta { get; set; }
        public DateTime FechaRegistro { get; set; }
        public string FechaRegistroIni { get; set; }
        public string FechaRegistroFin { get; set; }
        public int TipoDocumento { get; set; }
        public string TipoDocumentoDescripcion { get; set; }
        public string NroDocumento { get; set; }
        public string NombresCompletos { get; set; }
        public string Direccion { get; set; }
        public string Descripcion { get; set; }
        public int Estado { get; set; }
        public string UsuarioRegistro { get; set; }
        public string CorreoElectronico { get; set; }
        public string Celular { get; set; }
        public int CodTabla { get; set; }
        public int LaserficheID { get; set; }
        public string Archivo1 { get; set; }
        public string Archivo2 { get; set; }
        public string NombreArchivo1 { get; set; }
        public string NombreArchivo2 { get; set; }

        public string LaserFicheConsultas{ get; set; }


    }
}
