﻿using Agrobanco.SAR.BLL;
using Agrobanco.SAR.DAL.Laserfiche;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SAR.CNAService
{
    public partial class SARCNAService : ServiceBase
    {
        bool bandera = false;
        public SARCNAService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            stLapso.Start();
        }

        protected override void OnStop()
        {
            stLapso.Stop();
        }

        private void timer1_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (bandera) return;
            string proceso = string.Empty;

            try
            {
                bandera = true;
                EventLog.WriteEntry("Se inicia el proceso generador de CNA de SAR", EventLogEntryType.Information);

                TimeSpan t = DateTime.Now.TimeOfDay;

                string rangoInicio = ConfigurationManager.AppSettings["rangoInicio"];
                string rangoFin = ConfigurationManager.AppSettings["rangoFin"];

                if (t.Hours >= Convert.ToInt32(rangoInicio) && t.Hours < Convert.ToInt32(rangoFin))
                {
                    LaserficheProxy laserfiche = new LaserficheProxy();
                    CNABLL objBLL = new CNABLL();


                    /*1. Otener idlaserfiche de la plantilla dedicada para esta tarea de la tabla parámetro (ToDo)*/
                    var LaserFicheTemplate = objBLL.ConsultarDocumentoLista(ConfigurationManager.AppSettings["idlaserfichePlantilla"]);

                    /*2. se consume el Laserfiche API para obtener el archivo de Laserfiche(ToDo)*/

                    /*3. Obtener información de clientes que se les generará la carta*/
                    var listaCreditoCliente = objBLL.ListarCreditoClienteGeneracionCNA();

                    var listParametro = objBLL.ObtenerListaParametro(1000);

                    foreach (var item in listaCreditoCliente)
                    {                        
                        EventLog.WriteEntry(string.Format("Se inicia generación de CNA de Cliente {0} con {1} {2}", item.Nombres, item.TipoDocumento, item.NumeroDocumento), EventLogEntryType.Information);

                        var base64 = objBLL.GeneradorGestionCNA(LaserFicheTemplate[0].ArchivoBytes, item, listParametro);
                        /*4. Proceso de generación de carta desde plantilla con datos del cliente (ToDo)*/
                        /*5. guardar en Laserfiche la CNA con la respectiva estructura y se obtiene el idlaserfiche(ToDo), para esto tmb se consume el Laserfiche API*/
                       // LaserficheProxy laserfiche = new LaserficheProxy();
                        DocumentoModel DocumentoModelRequest = new DocumentoModel();
                        DocumentoModel DocumentoModelResponse = new DocumentoModel();
                        DocumentoModelRequest.Folder = $@"\GESTION-CNA\" + item.NumeroDocumento;
                        List<Documento> lstDocumento = new List<Documento>();
                        string nombreArchivo = "CNA_" + item.TipoDocumento + item.NumeroDocumento;

                        if (!string.IsNullOrEmpty(base64))
                        {
                            DateTime fechaActual = DateTime.Now;
                            string Dia = fechaActual.Day.ToString().PadLeft(2, '0');
                            string Mes = fechaActual.Month.ToString().PadLeft(2, '0');
                            string Anio = fechaActual.Year.ToString().PadLeft(2, '0');

                            string Hora = fechaActual.Hour.ToString().PadLeft(2, '0');
                            string Minutos = fechaActual.Minute.ToString().PadLeft(2, '0');
                            string Segundos = fechaActual.Second.ToString().PadLeft(2, '0');

                            Documento oDocumento = new Documento();
                            oDocumento.Extension = "docx";
                            oDocumento.ArchivoBytes = base64;



                            oDocumento.Nombre = nombreArchivo + "_" + Anio + Mes + Dia + "T" + Hora + Minutos + Segundos;
                            // oDocumento.Folder = "COMENTARIO-EMISION";
                            lstDocumento.Add(oDocumento);
                        }


                        if (lstDocumento.Count > 0)
                        {
                            DocumentoModelRequest.ListaDocumentos = lstDocumento;
                            DocumentoModelResponse = laserfiche.UploadListaDocumento(DocumentoModelRequest);
                            DocumentoModelResponse.ResultadoOK = true;

                        }

                        if (DocumentoModelResponse.ResultadoOK)
                        {
                            item.IdLaserficheCNA = Convert.ToInt32(DocumentoModelResponse.ListaDocumentos[0].CodigoLaserfiche);
                            objBLL.ActualizarCreditoClienteGeneracionCNA(item);
                        }
                        


                        
                        /*6. El Idlaserfiche es almacenado en un array o lista junto con el idcreditocliente del item actual (ToDo)*/
                    }

                    /*7. Actualización masiva en SQL, enviando la lista de idcreditocliente junto con su idlaserfichecna (ToDo)*/
                }


                EventLog.WriteEntry("Se finaliza el proceso generador de CNA de SAR", EventLogEntryType.Information);
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Proceso: " + proceso + " - Exception Message: " + ex.Message, EventLogEntryType.Error);
            }

            bandera = false;
        }
    }
}
