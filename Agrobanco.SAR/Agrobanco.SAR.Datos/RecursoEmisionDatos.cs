﻿using Agrobanco.SAR.Comun.Laserfiche;
using Agrobanco.SAR.Datos.SqlServer.Acceso;
using Agrobanco.SAR.Entidades.CNA_Minuta;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SAR.Datos
{
    public class RecursoEmisionDatos
    {
        public string cadenaConexion;
        public async Task<List<RecursoEmisionBE>> Obtener(RecursoEmisionBE request)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            RecursoEmisionBE oRecursoEmisionBE = null;
            List<RecursoEmisionBE> LstRecursoEmisionBE = new List<RecursoEmisionBE>();
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("SP_RECURSO_EMISION_OBTENER", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@IdEmision", SqlDbType.Int).Value = request.IdEmision;

                        try
                        {
                            using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                            {
                                while (reader.Read())
                                {
                                    oRecursoEmisionBE = new RecursoEmisionBE();

                                    if (!reader.IsDBNull(0))
                                        oRecursoEmisionBE.CodRecurso = reader.GetInt32(0);

                                    if (!reader.IsDBNull(1))
                                        oRecursoEmisionBE.CodTabla = reader.GetInt32(1);

                                    if (!reader.IsDBNull(2))
                                        oRecursoEmisionBE.ReferenciaID = reader.GetInt64(2);

                                    if (!reader.IsDBNull(3))
                                        oRecursoEmisionBE.LaserficheID = reader.GetInt32(3);

                                    if (!reader.IsDBNull(4))
                                        oRecursoEmisionBE.IdRecursoEmision = reader.GetInt32(4);

                                    if (!reader.IsDBNull(5))
                                        oRecursoEmisionBE.IdEmision = reader.GetInt32(5);

                                    if (!reader.IsDBNull(6))
                                        oRecursoEmisionBE.TipoRecurso = reader.GetString(6);

                                    if (!reader.IsDBNull(7))
                                        oRecursoEmisionBE.TipoRecursoID = reader.GetInt32(7);

                                    LstRecursoEmisionBE.Add(oRecursoEmisionBE);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            throw ex;
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return await Task.Run(() => LstRecursoEmisionBE);
        }
        public async Task<bool> Insertar(RecursoEmisionBE request)
        {
            bool resultado = false;

            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            try
            {
             
                DocumentoModel DocumentoModelRequest = new DocumentoModel();
                DocumentoModel DocumentoModelResponse = new DocumentoModel();

               // DocumentoModelRequest.Folder = "GESTION-CNA/" + request.DNI + "";
                DocumentoModelRequest.Folder = $@"\GESTION-CNA\" + request.DNI;
                List<Documento> lstDocumento = new List<Documento>();

                string base64 = string.Empty;
                string nombreArchivo = string.Empty;
                if (request.TipoRecursoID == 1) nombreArchivo = "CNA_" + request.TipoDocumento + request.DNI;
                if (request.TipoRecursoID == 2) nombreArchivo = request.NombreArchivo; //"CartadeNoAdeudoAdmAge";
                if (request.TipoRecursoID == 3) nombreArchivo = "CartadeRespuesta";
                if (request.TipoRecursoID == 4) nombreArchivo = "Archivo";
                if (request.TipoRecursoID == 5) nombreArchivo = "Minuta_" + request.TipoDocumento + request.DNI;
                if (request.TipoRecursoID == 6) nombreArchivo = "CartaEntrega";
                if (request.TipoRecursoID == 7) nombreArchivo = "CargodeEntrega";

                if (!string.IsNullOrEmpty(request.Archivo))
                {
                    base64 = string.Empty;
                    base64 = request.Archivo.Split(',')[1];

                    DateTime fechaActual = DateTime.Now;
                    string Dia = fechaActual.Day.ToString().PadLeft(2, '0');
                    string Mes = fechaActual.Month.ToString().PadLeft(2, '0');
                    string Anio = fechaActual.Year.ToString().PadLeft(2, '0');

                    string Hora = fechaActual.Hour.ToString().PadLeft(2, '0');
                    string Minutos = fechaActual.Minute.ToString().PadLeft(2, '0');
                    string Segundos = fechaActual.Second.ToString().PadLeft(2, '0');

                    Documento oDocumento = new Documento();
                    oDocumento.ArchivoBytes = base64;
                    if (request.TipoRecursoID == 2)
                    {
                        oDocumento.Extension = TypesConstants.PdfExtension;
                        oDocumento.Nombre = nombreArchivo;
                    }
                    else
                    {
                        oDocumento.Extension = System.IO.Path.GetExtension(request.NombreArchivo).Split('.')[1];
                        oDocumento.Nombre = nombreArchivo + "_" + Anio + Mes + Dia + "T" + Hora + Minutos + Segundos;
                    }
 
                    lstDocumento.Add(oDocumento);
                }


                if (lstDocumento.Count > 0)
                {
                    LaserficheProxy laserfiche = new LaserficheProxy();
                    DocumentoModelRequest.ListaDocumentos = lstDocumento;
                    DocumentoModelResponse = laserfiche.UploadListaDocumento(DocumentoModelRequest);
                   
                    foreach (var image in DocumentoModelResponse.ListaDocumentos)
                    {
                        if (image.ResultadoOK)
                        {
                            using (SqlConnection conn = new SqlConnection(cadenaConexion))
                            {
                                conn.Open();
                                using (SqlCommand cmd = new SqlCommand("SP_RECURSO_EMISION_INSERTAR", conn))
                                {

                                    try
                                    {
                                        cmd.CommandType = CommandType.StoredProcedure;

                                        cmd.Parameters.Add("@CodTabla", SqlDbType.Int).Value = request.CodTabla;
                                        cmd.Parameters.Add("@LaserficheID", SqlDbType.Int).Value = image.CodigoLaserfiche;
                                        cmd.Parameters.Add("@TipoRecurso", SqlDbType.Int).Value = request.TipoRecursoID;
                                        cmd.Parameters.Add("@Descripcion", SqlDbType.VarChar, 100).Value = image.Nombre;
                                        cmd.Parameters.Add("@IdEmision", SqlDbType.Int).Value = request.IdEmision;
                                        cmd.Parameters.Add("@UsuarioRegistro", SqlDbType.VarChar, 50).Value = request.UsuarioRegistro;

                                        await cmd.ExecuteNonQueryAsync();

                                        resultado = true;

                                    }

                                    catch (Exception ex)
                                    {

                                        conn.Close();
                                        conn.Dispose();
                                        cmd.Dispose();
                                        resultado = false;
                                        throw (ex);
                                    }
                                    finally
                                    {
                                        conn.Close();
                                        conn.Dispose();
                                        cmd.Dispose();
                                    }


                                }
                            }

                            if (!resultado)
                                throw new Exception("Se presento un error al intentar registrar los archivos adjuntos");

                        }
                        else
                        {
                            throw new Exception("Se presento un error al subir el archivo");
                        }
                    }
                    DocumentoModelResponse.ResultadoOK = true;

                }


               
            }
            catch (Exception ex)
            {
                resultado = false;
                throw ex;
            }
            return await Task.Run(() => resultado);
        }

        public async Task<bool> Insert(RecursoEmisionBE request)
        {
            bool resultado = false;

            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            try
            {
               
                DocumentoModel DocumentoModelRequest = new DocumentoModel();
                DocumentoModel DocumentoModelResponse = new DocumentoModel();

                // DocumentoModelRequest.Folder = "GESTION-CNA/" + request.DNI + "";
                DocumentoModelRequest.Folder = $@"\GESTION-CNA\" + request.DNI;
                List<Documento> lstDocumento = new List<Documento>();

                string base64 = string.Empty;
                string nombreArchivo = string.Empty;
                //if (request.TipoRecursoID == 1) nombreArchivo = "CartadeNoAdeudo";
                //if (request.TipoRecursoID == 2) nombreArchivo = "CartadeNoAdeudoAdmAge";
                //if (request.TipoRecursoID == 3) nombreArchivo = "CartadeRespuesta";
                //if (request.TipoRecursoID == 7) nombreArchivo = "CargodeEntrega";

                if (request.TipoRecursoID == 1) nombreArchivo = "CNA_" + request.TipoDocumento + request.DNI;
                if (request.TipoRecursoID == 2) nombreArchivo = request.NombreArchivo; //"CartadeNoAdeudoAdmAge";
                if (request.TipoRecursoID == 3) nombreArchivo = "CartadeRespuesta";
                if (request.TipoRecursoID == 4) nombreArchivo = "Archivo";
                if (request.TipoRecursoID == 5) nombreArchivo = "Minuta_" + request.TipoDocumento + request.DNI;
                if (request.TipoRecursoID == 6) nombreArchivo = "CartaEntrega";
                if (request.TipoRecursoID == 7) nombreArchivo = "CargodeEntrega";

                if (!string.IsNullOrEmpty(request.Archivo))
                {
                    base64 = string.Empty;
                    base64 = request.Archivo;

                    DateTime fechaActual = DateTime.Now;
                    string Dia = fechaActual.Day.ToString().PadLeft(2, '0');
                    string Mes = fechaActual.Month.ToString().PadLeft(2, '0');
                    string Anio = fechaActual.Year.ToString().PadLeft(2, '0');

                    string Hora = fechaActual.Hour.ToString().PadLeft(2, '0');
                    string Minutos = fechaActual.Minute.ToString().PadLeft(2, '0');
                    string Segundos = fechaActual.Second.ToString().PadLeft(2, '0');

                    Documento oDocumento = new Documento();

                    if (request.TipoRecursoID == 2)
                    {
                        oDocumento.Extension = TypesConstants.PdfExtension;
                        oDocumento.Nombre = nombreArchivo;
                    }
                    else
                    {
                        oDocumento.Extension = System.IO.Path.GetExtension(request.NombreArchivo).Split('.')[1];
                        oDocumento.Nombre = nombreArchivo + "_" + Anio + Mes + Dia + "T" + Hora + Minutos + Segundos;
                    }
                       

                    oDocumento.ArchivoBytes = base64;


                    

                    // oDocumento.Folder = "COMENTARIO-EMISION";
                    lstDocumento.Add(oDocumento);
                }


                if (lstDocumento.Count > 0)
                {
                    LaserficheProxy laserfiche = new LaserficheProxy();
                    DocumentoModelRequest.ListaDocumentos = lstDocumento;
                    DocumentoModelResponse = laserfiche.UploadListaDocumento(DocumentoModelRequest);

                    foreach (var image in DocumentoModelResponse.ListaDocumentos)
                    {
                        if (image.ResultadoOK)
                        {
                            using (SqlConnection conn = new SqlConnection(cadenaConexion))
                            {
                                conn.Open();
                                using (SqlCommand cmd = new SqlCommand("SP_RECURSO_EMISION_INSERTAR", conn))
                                {

                                    try
                                    {
                                        cmd.CommandType = CommandType.StoredProcedure;

                                        cmd.Parameters.Add("@CodTabla", SqlDbType.Int).Value = request.CodTabla;
                                        cmd.Parameters.Add("@LaserficheID", SqlDbType.Int).Value = image.CodigoLaserfiche;
                                        cmd.Parameters.Add("@TipoRecurso", SqlDbType.Int).Value = request.TipoRecursoID;
                                        cmd.Parameters.Add("@Descripcion", SqlDbType.VarChar, 100).Value = image.Nombre;
                                        cmd.Parameters.Add("@IdEmision", SqlDbType.Int).Value = request.IdEmision;
                                        cmd.Parameters.Add("@UsuarioRegistro", SqlDbType.VarChar, 50).Value = request.UsuarioRegistro;

                                        await cmd.ExecuteNonQueryAsync();

                                        resultado = true;

                                    }

                                    catch (Exception ex)
                                    {

                                        conn.Close();
                                        conn.Dispose();
                                        cmd.Dispose();
                                        resultado = false;
                                        throw (ex);
                                    }
                                    finally
                                    {
                                        conn.Close();
                                        conn.Dispose();
                                        cmd.Dispose();
                                    }


                                }
                            }

                            if (!resultado)
                                throw new Exception("Se presento un error al intentar registrar los archivos adjuntos");

                        }
                        else
                        {
                            throw new Exception("Se presento un error al subir el archivo");
                        }
                    }
                    DocumentoModelResponse.ResultadoOK = true;

                }



            }
            catch (Exception ex)
            {
                resultado = false;
                throw ex;
            }
            return await Task.Run(() => resultado);
        }


        public async Task<bool> Actualizar(RecursoEmisionBE request)
        {
            bool resultado = false;

            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("SP_RECURSO_EMISION_ACTUALIZAR_TIPO_RECURSO", conn))
                    {
                        try
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@IdRecursoEmision", SqlDbType.Int).Value = request.IdRecursoEmision;
                            cmd.Parameters.Add("@TipoRecurso", SqlDbType.Int).Value = request.TipoRecursoID;
                            await cmd.ExecuteNonQueryAsync();
                            resultado = true;
                        }
                        catch (Exception ex)
                        {

                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            resultado = false;
                            throw (ex);
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }


                    }
                }

                if (!resultado)
                    throw new Exception("Se presento un error al intentar registrar los archivos adjuntos");






            }
            catch (Exception ex)
            {
                resultado = false;
                throw ex;
            }
            return await Task.Run(() => resultado);
        }
    }
}
