﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades.CNA_Minuta
{
    public class CNADetalleRequest
    {
        public int IdEmision { get; set; }
        public string TipoDocumentoDescripcion { get; set; }
        public string NroDocumento { get; set; }
        public string Perfil { get; set; }
    }
}
