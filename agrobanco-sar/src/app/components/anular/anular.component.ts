import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { forkJoin, Observable, Observer } from 'rxjs';
import { RequerimientoService } from 'src/app/services/requerimiento.service';
import { DialogService } from 'src/app/services/comun/dialog.service';
import { RequerimientoDetalleComponent } from 'src/app/components/requerimiento-detalle/requerimiento-detalle.component';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ParametroService } from 'src/app/services/parametro.service';

import { Output, Input,ViewChild, EventEmitter } from '@angular/core';

import {  Subject } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { RequerimientoInterface } from '../models/requerimiento.interface';
import { Router,ActivatedRoute } from '@angular/router';
import { SecurityService } from 'src/app/services/security.service';
import { GestionRequerimientoComponent } from 'src/app/components/gestion-requerimiento/gestion-requerimiento.component';
import { CreditoInterface } from '../models/credito.interface';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { UbigeoService } from '../../services/ubigeo.service';
import { PopupComentarioComponent } from 'src/app/components/popup-comentario/popup-comentario.component';


import { subscribeOn } from 'rxjs/operators';

@Component({
  selector: 'app-anular',
  templateUrl: './anular.component.html',
  styleUrls: ['./anular.component.css']
})
export class AnularComponent implements OnInit {

  form!: FormGroup;
  formSubmitted:boolean = false;
  parametro:any[] = [];
  motivos:any[] = [];

  parametros:any[0] = [];

  lengthDescripcion: number  = 150;
  MaxlengthDescripcion: number  = 150;
  MinlengthDescripcion: number  = 5;

  constructor(
    private parametroService: ParametroService,
    private securityService:SecurityService,
    private formBuilder: FormBuilder,
    private requerimientoServices : RequerimientoService,
    private dialogService: DialogService,
    public dialogRef: MatDialogRef<AnularComponent>,
  @Inject(MAT_DIALOG_DATA) public data: any[]
  ) {
    this.parametros = data;
   }

  ngOnInit(): void {
    this.inicializarFormulario();
    this.loadPage();
  }


  loadPage(): void {

    forkJoin([
      this.parametroService.ObtenerParametro()
    ]).subscribe( (data: any) => {
      debugger;
        console.log(data);
        this.parametro = data[0].response;

    }, (err) => {
      console.error(err);
    });

  }

  inicializarFormulario() : void {
    this.form = this.formBuilder.group({
      Descripcion: [null,  [Validators.required, Validators.minLength(5)]],
      CodMotivo: [null,[Validators.required]]

    });
  }

  anularRequerimiento():void {
    debugger;
        this.formSubmitted = true;

        if (this.form.invalid) {
          return;
        }

        let Estado = this.parametros.data.Estado;

        if(Estado = 1)
        {

        this.dialogService.openConfirmDialog('Seguro que desea Anular este requerimiento?')
        .afterClosed().subscribe(res => {
          if (res) {

            let perfil = this.securityService.leerUsuarioWeb();
            let DescripcionEstado = this.parametros.data.DescripcionEstado;

            const data =   {
              CodRequerimiento : this.parametros.data.CodRequerimiento,
              CodRequerimientoMovimiento :  this.parametros.data.CodRequerimiento,
              NroRequerimiento :  this.parametros.data.NroRequerimiento,
              UsuarioRegistro :  perfil,
              Descripcion :this.form.controls['Descripcion'].value,
              CodMotivo : this.form.controls['CodMotivo'].value,
              Estado :  2,
              Area :  null,
              FechaSolicitud : null

            }

            console.log("request",data);
            if(data.CodMotivo == null){ 
              data.CodMotivo = 0;
            } 
            else{
              data.CodMotivo = parseInt(this.form.controls['CodMotivo'].value);
            }   

            this.requerimientoServices.RegistrarMovimiento(data).subscribe(result => {
              console.log("response",result.response);

              data.CodRequerimientoMovimiento = result.response;            

                this.requerimientoServices.RegistrarComentarioAnular(data).subscribe(res=>{
                  debugger;
                  console.log("response",res);                

                this.requerimientoServices.ActualizarEstados(data).subscribe(resultado  => {
                  console.log("response",resultado.response);

                  this.dialogService.openMsgSuccessDialog("El estado del requerimiento se ha cambiado de " + DescripcionEstado.toUpperCase() + " a ANULADO")
                  .afterClosed().subscribe(res => {
    
                    this.onClick("OK");
    
                  });
  
                });

              });  
            
            });
          }
        });

      }

    }


    onClick(data: any): void {
      this.dialogRef.close({ data: data });
    }
}
