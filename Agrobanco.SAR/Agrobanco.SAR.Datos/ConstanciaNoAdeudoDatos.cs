﻿using Agrobanco.SAR.Datos.SqlServer.Acceso;
using Agrobanco.SAR.Entidades;
using Agrobanco.SAR.Entidades.ConstanciaNoAdeudo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Agrobanco.SAR.Datos
{
    public class ConstanciaNoAdeudoDatos
    {
        public string cadenaConexion;
        public ConstanciaNoAdeudoDatos()
        {

        }


        public ConstanciaNoAdeudoResponse Obtener(ConstanciaNoAdeudoRequest request)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            ConstanciaNoAdeudoResponse oConstanciaNoAdeudoResponse = new ConstanciaNoAdeudoResponse();

            CreditoClienteDatos oCreditoClienteDatos = new CreditoClienteDatos();
            CreditosClienteBE oCreditosClienteBE = new CreditosClienteBE();

            oCreditosClienteBE.TipoIDN = request.TipoIDN;
            oCreditosClienteBE.Identificacion = request.Identificacion;

            oConstanciaNoAdeudoResponse.CreditosClienteBE = oCreditoClienteDatos.ObtenerCreditos(oCreditosClienteBE);


            RequerimientoDatos oRequerimientoDatos = new RequerimientoDatos();
            RequestRequerimiento objRequerimiento = new RequestRequerimiento();
            List<RequerimientoBE> lstRequerimiento = new List<RequerimientoBE>();

            objRequerimiento.TipoDocumento =  request.TipoDocumento;
            objRequerimiento.NroDocumento = request.Identificacion;
            lstRequerimiento = oRequerimientoDatos.RequerimientosListarCartaNoAdeudo(objRequerimiento);
            oConstanciaNoAdeudoResponse.RequerimientoBE = lstRequerimiento;


            if (oConstanciaNoAdeudoResponse != null)
            {
                if (oConstanciaNoAdeudoResponse.CreditosClienteBE != null)
                {
                    foreach (var item in oConstanciaNoAdeudoResponse.CreditosClienteBE)
                    {
                        if(!string.IsNullOrEmpty(item.NomCliente))
                            oConstanciaNoAdeudoResponse.NombresCompletos = item.NomCliente;

                        break;
                    }
                }
            }

            if (oConstanciaNoAdeudoResponse != null)
            {
                if (oConstanciaNoAdeudoResponse.RequerimientoBE != null)
                {
                    foreach (var item in oConstanciaNoAdeudoResponse.RequerimientoBE)
                    {
                        if (!string.IsNullOrEmpty(item.NombresApellidos))
                            oConstanciaNoAdeudoResponse.NombresCompletos = item.NombresApellidos;

                        break;
                    }
                }
            }
            return oConstanciaNoAdeudoResponse;
        }



    }
}
