﻿using Agrobanco.SAR.Comun;
using Agrobanco.SAR.Datos;
using Agrobanco.SAR.Entidades.CNA_Minuta;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SAR.LogicaNegocio
{
    public class ConfiguracionAccionLogica
    {
        private readonly ConfiguracionAccionDatos _configuracionAccionDatos;
        private readonly ConfiguracionDerivacionDatos _configuracionDerivacionDatos;
        private readonly DerivacionDatos _derivacionDatos;
        private readonly EmisionDatos _emisionDatos;

        public ConfiguracionAccionLogica()
        {
            _configuracionAccionDatos = new ConfiguracionAccionDatos();
            _configuracionDerivacionDatos = new ConfiguracionDerivacionDatos();
            _derivacionDatos = new DerivacionDatos();
            _emisionDatos = new EmisionDatos();
        }

         
        public async Task<List<ConfiguracionAccionBE>> Obtener(ConfiguracionAccionBE request)
        {
            return await _configuracionAccionDatos.Obtener(request);
        }

 
    }
}
