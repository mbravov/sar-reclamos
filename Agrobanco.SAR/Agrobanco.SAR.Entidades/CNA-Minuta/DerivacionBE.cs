﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades.CNA_Minuta
{
    public class DerivacionBE
    {
        public int IdDerivacion { get; set; }
        public int IdEmision { get; set; }
        public string Perfil { get; set; }
        public int Estado { get; set; }
        public string PerfilDerivado { get; set; }
        public DateTime? FechaDerivado { get; set; }
        public int? CodAgencia { get; set; }
        public string Agencia { get; set; }
        public DateTime? FechaRegistro { get; set; }
        public string UsuarioRegistro { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public string UsuarioModificacion { get; set; }
        public int IdConfiguracionDerivacion { get; set; }
        public int? CodUsuarioDerivado { get; set; }

    }
}
