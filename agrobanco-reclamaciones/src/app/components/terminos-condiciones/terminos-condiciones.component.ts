import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-terminos-condiciones',
  templateUrl: './terminos-condiciones.component.html',
  styleUrls: ['./terminos-condiciones.component.css']
})
export class TerminosCondicionesComponent implements OnInit {

  terminocondiciones!:any[];
  ok:boolean = true;
  nook:boolean = false;

  constructor(
    public dialogRef: MatDialogRef<TerminosCondicionesComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any[]
  ) {
    this.terminocondiciones = data;
  }

  ngOnInit(): void {
  }

  onClick(value:number): void {
    this.dialogRef.close({ data: value });
  }

}
