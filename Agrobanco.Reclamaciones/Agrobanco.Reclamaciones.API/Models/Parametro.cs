﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.Reclamaciones.API.Models
{
    public class Parametro
    {
        public int CodParametro { get; set; }
        public int Parametro_ { get; set; }
        public int Grupo { get; set; }
        public string strGrupo { get; set; }

        public string Descripcion { get; set; }
    }
}
