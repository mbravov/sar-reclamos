USE DBAgrSAR
GO

DROP PROCEDURE IF EXISTS dbo.SP_COMENTARIO_OBTENER
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 26/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- PARAMETOS				:
--							  @CodRequerimientoMovimiento
--							  @CodRequerimiento
--							  @Estado
-- FUNCIONALIDAD			: Obtener los comentarios registrado por el movimiento, requerimiento o estado del requerimiento
--================================================================================================================================

CREATE PROCEDURE dbo.SP_COMENTARIO_OBTENER
@CodRequerimientoMovimiento BIGINT,
@CodRequerimiento BIGINT,
@Estado INT,
@CodTabla INT
AS
BEGIN
	SELECT DISTINCT  C.CodComentario
		  ,C.CodRequerimientoMovimiento
		  ,C.CodRequerimiento
		  ,C.CodMotivo
		  ,C.CodArea
		  ,A.Descripcion as Area
		  ,C.Descripcion
		  ,C.FechaSolicitud
		  ,C.UsuarioRegistro
		  ,C.FechaRegistro
		  ,R.LaserficheID
		  ,R.CodTabla
		  ,R.ReferenciaID
		  ,RM.Estado
		  ,U.NombresApellidos
		  ,PM.Descripcion Motivo
	FROM  Comentario C
		LEFT JOIN  RequerimientoMovimiento RM ON C.CodRequerimientoMovimiento = RM.CodRequerimientoMovimiento		
		LEFT JOIN Area A ON C.CodArea = A.Codigo 
		LEFT JOIN Recurso R ON R.ReferenciaID =  C.CodComentario AND R.CodTabla = @CodTabla
		LEFT JOIN Usuario U ON C.UsuarioRegistro = U.UsuarioWeb
		LEFT JOIN Parametro PM ON PM.Parametro = C.CodMotivo AND PM.Grupo = 1100
	WHERE (ISNULL(@CodRequerimientoMovimiento,0) = 0 OR C.CodRequerimientoMovimiento = @CodRequerimientoMovimiento) 
		AND	(ISNULL(@CodRequerimiento,0) = 0 OR RM.CodRequerimiento = @CodRequerimiento)  
		AND	(ISNULL(@Estado,0) = 0 OR RM.Estado = @Estado) 
	
END
GO

-----SP_COMENTARIO_OBTENER 0, '', '4', '3'
	
DROP PROCEDURE IF EXISTS dbo.SP_REQUERIMIENTO_MOVIMIENTO_OBTENER_DETALLE  
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 26/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_REQUERIMIENTO_MOVIMIENTO_OBTENER_DETALLE
-- PARAMETOS				: @CodRequerimiento
-- FUNCIONALIDAD			: 
--================================================================================================================================

CREATE PROCEDURE dbo.SP_REQUERIMIENTO_MOVIMIENTO_OBTENER_DETALLE
@NroReclamo VARCHAR(20)
AS
BEGIN

;WITH CTE AS (
	SELECT 
	  ROW_NUMBER() OVER( ORDER BY RM.FechaRegistro) - 1 as rn
    , RM.CodRequerimientoMovimiento
	, RM.CodRequerimiento
	, RM.Estado
	, P.Descripcion  
    , RM.FechaRegistro
	from RequerimientoMovimiento RM 
	INNER JOIN Requerimiento RQ ON RQ.CodRequerimiento = RM.CodRequerimiento
	LEFT JOIN Parametro P ON P.GRUPO = 100 AND P.PARAMETRO = RM.ESTADO
	WHERE RQ.NroRequerimiento = @NroReclamo
)
 

SELECT 
	 TEMP.CodRequerimientoMovimiento
   , TEMP.CodRequerimiento
   , TEMP.Estado  
   , TEMP.FechaEntrega
   , TEMP.FechaCartaRespuesta
   , TEMP.FechaCartaRespuestaOCM
   , TEMP.UsuarioRegistro 
  
   , TEMP.FechaRegistro
   , CASE WHEN TEMP.CodEstado = 2 THEN 0 ELSE ( DATEDIFF (DAY,TEMP.FechaRegistro, CASE WHEN CTE.Estado!= 8 AND CTE.FechaRegistro IS NULL THEN GETDATE() ELSE CTE.FechaRegistro END )) END DiasTranscurrido
 
FROM (
SELECT 
	 ROW_NUMBER() OVER( ORDER BY RM.FechaRegistro) as item
   , RM.CodRequerimientoMovimiento
   , RM.CodRequerimiento
   , P.Descripcion Estado
   , RQT.FechaEntrega
   , RC.FechaRegistro FechaCartaRespuesta
   , RCO.FechaRegistro FechaCartaRespuestaOCM
   , U.NombresApellidos UsuarioRegistro
   , RM.UsuarioModificacion
   , RM.FechaRegistro
   , RM.Estado CodEstado
 
	FROM  RequerimientoMovimiento RM
		INNER JOIN Requerimiento RQ ON RQ.CodRequerimiento = RM.CodRequerimiento
		LEFT JOIN Parametro P ON P.GRUPO = 100 AND P.PARAMETRO = RM.ESTADO
		LEFT JOIN Recurso RC ON  RC.CodTabla = 6 AND RC.ReferenciaID = RM.CodRequerimiento AND RM.Estado = 5
		LEFT JOIN Recurso RCO ON  RCO.CodTabla = 7 AND RCO.ReferenciaID = RM.CodRequerimiento AND RM.Estado = 6
		LEFT JOIN Usuario U ON U.UsuarioWeb = RM.UsuarioRegistro
		LEFT JOIN Requerimiento RQT ON RQT.CodRequerimiento = RM.CodRequerimiento AND RQT.Estado = RM.Estado AND RQT.Estado = 8  
	WHERE RQ.NroRequerimiento = @NroReclamo
		 
	GROUP BY
		    RM.CodRequerimientoMovimiento
		  , RM.CodRequerimiento
		  , P.Descripcion
		  , RQT.FechaEntrega
		  , RC.FechaRegistro
		  , RCO.FechaRegistro
		  , U.NombresApellidos
		  , RM.UsuarioModificacion
		  , RM.FechaRegistro
		  , RM.Estado
		 
		 
)TEMP 
LEFT JOIN CTE ON TEMP.item = CTE.rn
 
GROUP BY 
	 TEMP.CodRequerimientoMovimiento
   , TEMP.CodRequerimiento
   , TEMP.Estado  
   , TEMP.FechaEntrega
   , TEMP.FechaCartaRespuesta
   , TEMP.FechaCartaRespuestaOCM
   , TEMP.UsuarioRegistro 
   , TEMP.UsuarioModificacion
   , TEMP.FechaRegistro
   , CTE.FechaRegistro
   , TEMP.CodEstado
   , CTE.Estado

END
GO

DROP PROCEDURE IF EXISTS dbo.SP_REQUERIMIENTO_ASIGNACION_OBTENER
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 26/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_REQUERIMIENTO_ASIGNACION_OBTENER
-- PARAMETOS				: @CodRequerimientoMovimiento
-- FUNCIONALIDAD			: 
--================================================================================================================================

CREATE PROCEDURE dbo.SP_REQUERIMIENTO_ASIGNACION_OBTENER
@CodRequerimientoAsignacion BIGINT
AS
BEGIN
	SELECT CodRequerimientoAsignacion
	  ,CodRequerimiento
      ,CodRequerimientoMovimiento
      ,UsuarioAsignado
      ,UsuarioRegistro
      ,UsuarioModificacion
      ,FechaRegistro
      ,FechaModificacion
	FROM RequerimientoAsignacion RMA
	WHERE	RMA.CodRequerimientoAsignacion = @CodRequerimientoAsignacion
END
GO

DROP PROCEDURE IF EXISTS dbo.SP_REQUERIMIENTO_MOVIMIENTO_OBTENER
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 26/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_REQUERIMIENTO_MOVIMIENTO_OBTENER
-- PARAMETOS				: @CodRequerimientoMovimiento BIGINT, @CodRequerimiento BIGINT, @Estado INT
-- FUNCIONALIDAD			: Query para obtener el estado anterior diferente a pendiente o sin atención
--================================================================================================================================

CREATE PROCEDURE dbo.SP_REQUERIMIENTO_MOVIMIENTO_OBTENER
@CodRequerimientoMovimiento BIGINT,
@CodRequerimiento BIGINT,
@Estado INT
AS
BEGIN
	 
	SELECT TOP (1) RM.CodRequerimientoMovimiento
		  ,RM.CodRequerimiento
		  ,P.Descripcion Estado
		--  ,RM.UsuarioAsignado
		  ,RM.UsuarioRegistro
		  ,RM.UsuarioModificacion
		  ,RM.FechaRegistro
		  ,RM.FechaModificacion
	FROM   RequerimientoMovimiento RM
		LEFT JOIN Parametro P ON P.GRUPO = 100 AND P.PARAMETRO = RM.ESTADO
	
	WHERE (ISNULL(@CodRequerimientoMovimiento,0) = 0 OR RM.CodRequerimientoMovimiento = @CodRequerimientoMovimiento) AND
		(ISNULL(@CodRequerimiento,0) = 0 OR RM.CodRequerimiento = @CodRequerimiento)  AND
		RM.Estado NOT IN (9,10)
	ORDER BY RM.CodRequerimientoMovimiento DESC

END
GO

DROP PROCEDURE IF EXISTS dbo.SP_REQUERIMIENTO_DETALLE_INSERTAR
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 26/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_REQUERIMIENTO_DETALLE_INSERTAR
-- PARAMETOS				: @TipoDocumento INT, @NroDocumento VARCHAR(20), @PrimerApellido  VARCHAR(200),
--							  @SegundoApellido  VARCHAR(200), @Nombres VARCHAR(200), @RazonSocial VARCHAR(200),
--							  @CodUbigeo VARCHAR(6), @Direccion VARCHAR(200), @Referencia VARCHAR(200), 
--							  @CorreoElectronico VARCHAR(100), @Celular VARCHAR(20), @Telefono VARCHAR(20),
--							  @UsuarioRegistro VARCHAR(50)
-- FUNCIONALIDAD			: Genera un registro en la tabla requerimiento detalle
--================================================================================================================================

CREATE PROCEDURE dbo.SP_REQUERIMIENTO_DETALLE_INSERTAR
@CodRequerimiento BIGINT,
@TipoDocumento INT,
@NroDocumento VARCHAR(20),
@PrimerApellido  VARCHAR(200),
@SegundoApellido  VARCHAR(200),
@Nombres VARCHAR(200),
@RazonSocial VARCHAR(200),
@CodUbigeo VARCHAR(6),
@Direccion VARCHAR(200),
@Referencia VARCHAR(200),
@CorreoElectronico VARCHAR(100),
@Celular VARCHAR(20),
@Telefono VARCHAR(20),
@UsuarioRegistro VARCHAR(50) 

AS
BEGIN
	 
	INSERT INTO RequerimientoDetalle (CodRequerimiento
		   ,TipoDocumento
           ,NroDocumento
           ,PrimerApellido
           ,SegundoApellido
           ,Nombres
           ,NombresApellidos
           ,RazonSocial
           ,CodUbigeo
           ,Direccion
           ,Referencia
           ,CorreoElectronico
           ,Celular
           ,Telefono
           ,UsuarioRegistro
           ,FechaRegistro)
     VALUES (@CodRequerimiento
		   ,@TipoDocumento
           ,@NroDocumento
           ,@PrimerApellido
           ,@SegundoApellido
           ,@Nombres
           ,CONCAT(@Nombres, ' ', @PrimerApellido, ' ', @SegundoApellido)
           ,@RazonSocial
           ,@CodUbigeo
           ,@Direccion
           ,@Referencia
           ,@CorreoElectronico
           ,@Celular
           ,@Telefono
           ,@UsuarioRegistro
           ,GETDATE())

END
GO

DROP PROCEDURE IF EXISTS dbo.SP_REQUERIMIENTO_MOVIMIENTO_INSERTAR
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 26/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: Jose Caycho Garcia
-- STORE PROCEDURE			: SP_REQUERIMIENTO_MOVIMIENTO_INSERTAR
-- PARAMETOS				: @CodRequerimiento BIGINT, @Estado INT, @UsuarioRegistro VARCHAR(50) 
-- FUNCIONALIDAD			:  
--================================================================================================================================

CREATE PROCEDURE dbo.SP_REQUERIMIENTO_MOVIMIENTO_INSERTAR
@CodRequerimiento BIGINT,
@Estado INT,
@UsuarioRegistro VARCHAR(50),
@CodRequerimientoMovimiento int OUTPUT

AS
BEGIN

	 INSERT INTO RequerimientoMovimiento (CodRequerimiento,Estado,UsuarioRegistro,FechaRegistro)
								  VALUES (@CodRequerimiento,@Estado,@UsuarioRegistro,GETDATE())

     SELECT @CodRequerimientoMovimiento = SCOPE_IDENTITY()

END
GO

DROP PROCEDURE IF EXISTS dbo.SP_REQUERIMIENTO_REGISTRAR
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 26/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: Jose Caycho Garcia
-- STORE PROCEDURE			: SP_REQUERIMIENTO_REGISTRAR
-- PARAMETOS				: @NroRequerimiento VARCHAR(20), @CodOficina  INT, @EsMenor INT,
--							  @CodRequerimientoDetalle BIGINT, @CodProducto INT, @CodMotivo INT,
--							  @CodTipoRequerimiento INT, @CodOrigen INT, @CodFormaEnvio INT,
--							  @Descripcion VARCHAR(1500), @Estado INT, @UsuarioRegistro VARCHAR(50)
-- FUNCIONALIDAD			:  
--================================================================================================================================

CREATE PROCEDURE dbo.SP_REQUERIMIENTO_REGISTRAR
@CodRequerimiento BIGINT OUTPUT ,
@NroRequerimiento VARCHAR(20) OUTPUT,
@CodAgencia  INT,
@EsMenor INT,
@CodProducto INT,
@CodMotivo INT,
@CodTipoRequerimiento INT,
@CodOrigen INT,
@CodFormaEnvio INT,
@Descripcion VARCHAR(1500),
@Estado INT,
@UsuarioRegistro VARCHAR(50), 
@TipoDocumento INT,
@NroDocumento VARCHAR(20),
@PrimerApellido  VARCHAR(200),
@SegundoApellido  VARCHAR(200),
@Nombres VARCHAR(200),
@RazonSocial VARCHAR(200) NULL,
@CodUbigeo VARCHAR(6),
@Direccion VARCHAR(200),
@Referencia VARCHAR(200),
@CorreoElectronico VARCHAR(100),
@Celular VARCHAR(20),
@Telefono VARCHAR(20)
AS
BEGIN
	 DECLARE @vNRO_REQUERIMIENTO VARCHAR(20) = ''
	 DECLARE @ABREVIATURA VARCHAR(1) = '' 
	 DECLARE @CORRELATIVO VARCHAR(10) = '0'

 	 SET @ABREVIATURA = (SELECT TOP(1) Referencia   FROM Parametro WHERE Grupo = 600 AND Parametro = @CodTipoRequerimiento)
 	 SET @CORRELATIVO = (SELECT TOP(1) Valor   FROM Parametro WHERE Grupo = 600 AND Parametro = @CodTipoRequerimiento)
 	 SET @CORRELATIVO = CAST((CAST(@CORRELATIVO AS INT) + 1) AS VARCHAR(4))

 	 UPDATE Parametro SET Valor = @CORRELATIVO  WHERE Grupo = 600 AND Parametro = @CodTipoRequerimiento
	 
	 SET @vNRO_REQUERIMIENTO = @ABREVIATURA  + '-'+ RIGHT('0000' + Ltrim(Rtrim(@CORRELATIVO)),4) +'-'+ CAST( YEAR(GETDATE()) AS VARCHAR(4))
	 
	 INSERT INTO Requerimiento (NroRequerimiento
           ,CodAgencia
           ,EsMenor        
           ,CodProducto
           ,CodMotivo
           ,CodTipoRequerimiento
           ,CodOrigen
           ,CodFormaEnvio
           ,Descripcion
           ,Estado
           ,UsuarioRegistro
           ,FechaRegistro)
     VALUES (@vNRO_REQUERIMIENTO
           ,@CodAgencia
           ,@EsMenor         
           ,@CodProducto
           ,@CodMotivo
           ,@CodTipoRequerimiento
           ,@CodOrigen
           ,@CodFormaEnvio
           ,@Descripcion
           ,@Estado
           ,@UsuarioRegistro
		   , GETDATE())
 
 
	SET @CodRequerimiento = @@IDENTITY
	SET @NroRequerimiento = @vNRO_REQUERIMIENTO
	EXEC SP_REQUERIMIENTO_DETALLE_INSERTAR @CodRequerimiento,@TipoDocumento,@NroDocumento,@PrimerApellido,@SegundoApellido,@Nombres,@RazonSocial,@CodUbigeo,@Direccion, @Referencia,@CorreoElectronico,@Celular,@Telefono,@UsuarioRegistro  
	EXEC SP_REQUERIMIENTO_MOVIMIENTO_INSERTAR  @CodRequerimiento,@Estado, @UsuarioRegistro, NULL
	
END
GO
 
DROP PROCEDURE IF EXISTS dbo.SP_PARAMETRO_OBTENER
GO
---=================================================================================================================================
-- FECHA DE CREACIÓN		: 26/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: SP_PARAMETRO_LISTAR
-- PARAMETROS				: @Grupo
-- FUNCIONALIDAD			: Obtener los parametros
--================================================================================================================================

CREATE PROCEDURE dbo.SP_PARAMETRO_OBTENER	
@Grupo INT NULL
As
BEGIN

	SELECT Grupo
		,Parametro
		,Descripcion 
		,Valor
		,Referencia
		,Referencia2
	FROM Parametro
	WHERE (ISNULL(@Grupo,0)=0 OR Grupo = @Grupo )
		AND Estado = 1
	ORDER BY Orden

END
GO

DROP PROCEDURE IF EXISTS dbo.SP_AGENCIA_OBTENER
GO
---=================================================================================================================================
-- FECHA DE CREACIÓN		: 26/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: SP_AGENCIA_LISTAR
-- PARAMETROS				:							  
-- FUNCIONALIDAD			: Obtener la lista de agencias
--==================================================================================================================================

CREATE PROCEDURE dbo.SP_AGENCIA_OBTENER	

AS
BEGIN

	SELECT CodAgencia
		,Oficina
		,Direccion
	FROM Agencia
	ORDER BY codAgencia

END
GO	

DROP PROCEDURE IF EXISTS dbo.SP_REQUERIMIENTO_BUSCAR_BLOQUEADO
GO
---=================================================================================================================================
-- FECHA DE CREACIÓN		: 28/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: SP_REQUERIMIENTO_BUSCAR_BLOQUEADO
-- PARAMETROS				: @CodRequerimiento
-- FUNCIONALIDAD			: Obtener el indicador  si otro usuario se encuentre revisando el requerimiento seleccionado.
--==================================================================================================================================

CREATE PROCEDURE dbo.SP_REQUERIMIENTO_BUSCAR_BLOQUEADO	
@NroRequerimiento VARCHAR(20)

AS

BEGIN

	SELECT CASE WHEN ISNULL(UsuarioBloqueo,null) = '' THEN UsuarioBloqueo ELSE UsuarioBloqueo END AS UsuarioBloqueo	
		,EstaBloqueado AS EstaBloqueado
		,CodTipoRequerimiento
		,CodRequerimiento
	FROM Requerimiento
	WHERE NroRequerimiento = @NroRequerimiento

END
GO
---EXEC SP_REQUERIMIENTO_BUSCAR_BLOQUEADO 'R-0006-2021'


DROP PROCEDURE IF EXISTS dbo.SP_REQUERIMIENTO_ACTUALIZAR_BLOQUEADO
GO
---==============================================================================================================================================
-- FECHA DE CREACIÓN		: 28/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: SP_REQUERIMIENTO_ACTUALIZAR_BLOQUEADO
-- PARAMETROS				: @CodRequerimiento, @UsuarioRegistro
-- FUNCIONALIDAD			: Actualiza el campo EstaBloqueado  para indicar que otro usuario se encuentre revisando el requerimiento seleccionado.
--                            0 = Sin Bloqueo , 1 = Bloqueado 
--================================================================================================================================================

CREATE PROCEDURE dbo.SP_REQUERIMIENTO_ACTUALIZAR_BLOQUEADO	
@Nrorequerimiento VarChar(20),
@UsuarioRegistro VARCHAR(50),
@EstaBloqueado BIT

AS

BEGIN
	 
	UPDATE Requerimiento
		SET UsuarioBloqueo = @UsuarioRegistro,
			EstaBloqueado = @EstaBloqueado
	WHERE NroRequerimiento = @Nrorequerimiento

END
GO

DROP PROCEDURE IF EXISTS dbo.SP_REQUERIMIENTO_UPDATE_ESTADO
GO
---=================================================================================================================================
-- FECHA DE CREACIÓN		: 28/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: SP_REQUERIMIENTO_UPDATE_ESTADO
-- PARAMETROS				: @CodRequerimiento, @Estado
-- FUNCIONALIDAD			: Actualizar el estado del requerimiento.
--==================================================================================================================================

CREATE PROCEDURE dbo.SP_REQUERIMIENTO_UPDATE_ESTADO	
@CodRequerimiento BIGINT,
@Estado INT

AS

BEGIN

	UPDATE Requerimiento
		SET Estado = @Estado	
	WHERE CodRequerimiento = @CodRequerimiento

END
GO

DROP PROCEDURE IF EXISTS dbo.SP_RECURSO_INSERTAR
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 28/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_RECURSO_INSERTAR
-- PARAMETOS				: @CodTabla INT, @ReferenciaID INT, @LaserficheID INT, 
--							  @Descripcion VARCHAR(50), @Estado BIT, @UsuarioRegistro VARCHAR(50)						  
-- FUNCIONALIDAD			: Genera un registro en la tabla recurso
--================================================================================================================================

CREATE PROCEDURE dbo.SP_RECURSO_INSERTAR	
@CodTabla INT,
@ReferenciaID INT,
@LaserficheID INT,
@Descripcion VARCHAR(50),
@Estado BIT,
@UsuarioRegistro VARCHAR(50)
AS
BEGIN

	INSERT INTO Recurso 
		   (CodTabla
           ,ReferenciaID
           ,LaserficheID
           ,Descripcion
           ,Estado
           ,UsuarioRegistro
           ,FechaRegistro
           )
     VALUES
           (@CodTabla
           ,@ReferenciaID
           ,@LaserficheID
           ,@Descripcion
           ,@Estado
           ,@UsuarioRegistro
           ,GETDATE())
		
END
GO

DROP PROCEDURE IF EXISTS dbo.SP_CONSULTA_INSERTAR
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 28/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_CONSULTA_INSERTAR
-- PARAMETOS				: @TipoDocumento INT, @NroDocumento VARCHAR(20), @NombresCompletos VARCHAR(200),
--							  @Direccion VARCHAR(200), @CorreoElectronico VARCHAR(100), @Celular VARCHAR(20),
--							  @Descripcion VARCHAR(1000), @Estado BIT, @UsuarioRegistro VARCHAR(50)						  
-- FUNCIONALIDAD			: Genera un registro en la tabla consulta
--================================================================================================================================

CREATE PROCEDURE dbo.SP_CONSULTA_INSERTAR	
@TipoDocumento INT,
@NroDocumento VARCHAR(20),
@NombresCompletos VARCHAR(200),
@Direccion VARCHAR(200),
@CorreoElectronico VARCHAR(100),
@Celular VARCHAR(20),
@Descripcion VARCHAR(1000),
@Estado BIT,
@UsuarioRegistro VARCHAR(50)
AS
BEGIN

	INSERT INTO Consulta
           (TipoDocumento
           ,NroDocumento
           ,NombresCompletos
           ,Direccion
           ,CorreoElectronico
           ,Celular
           ,Descripcion
           ,Estado
           ,UsuarioRegistro
           ,FechaRegistro
           )
     VALUES
           (@TipoDocumento
           ,@NroDocumento
           ,@NombresCompletos
           ,@Direccion
           ,@CorreoElectronico
           ,@Celular
           ,@Descripcion
           ,@Estado
           ,@UsuarioRegistro
           ,GETDATE())
		
END
GO

DROP PROCEDURE IF EXISTS dbo.SP_COMENTARIO_INSERTAR
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 28/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WIILIAM QUIROZ
-- STORE PROCEDURE			: SP_COMENTARIO_INSERTAR
-- PARAMETOS				: @CodRequerimiento BIGINT,@CodRequerimientoMovimiento BIGINT,@CodMotivo INT, 
--							  @CodArea INT,@Descripcion VARCHAR(300),@FechaSolicitud DATE,@UsuarioRegistro VARCHAR(50)						  
-- FUNCIONALIDAD			: Genera un registro en la tabla comentario
--=================================================================================================================================

CREATE PROCEDURE dbo.SP_COMENTARIO_INSERTAR	
@CodRequerimiento BIGINT,
@Estado INT,
@CodMotivo INT,
@CodArea INT,
@Descripcion VARCHAR(300),
@FechaSolicitud DATE,
@UsuarioRegistro VARCHAR(50),
@CodComentario INT OUTPUT

AS
BEGIN
	DECLARE @EXISTE_COMENTARIO INT = 0
	DECLARE @EXISTE_MOVIMIENTO BIGINT = 0

	DECLARE @vCodRequerimientoMovimiento BIGINT = 0

	SET @EXISTE_COMENTARIO = (SELECT COUNT(CodComentario) FROM Comentario WHERE CodRequerimiento = @CodRequerimiento)

	IF @EXISTE_COMENTARIO = 0
	BEGIN
			INSERT INTO RequerimientoMovimiento (CodRequerimiento,Estado,UsuarioRegistro,FechaRegistro)
								  VALUES (@CodRequerimiento,4,@UsuarioRegistro,GETDATE())

			SET @vCodRequerimientoMovimiento = SCOPE_IDENTITY()

			UPDATE Requerimiento SET
			Estado = 4
			WHERE CodRequerimiento = @CodRequerimiento
	END

	IF @EXISTE_COMENTARIO>0 
	BEGIN 
		SET @vCodRequerimientoMovimiento = (SELECT CodRequerimientoMovimiento FROM RequerimientoMovimiento WHERE CodRequerimiento = @CodRequerimiento AND Estado = @Estado)
		 
	END

	INSERT INTO Comentario 
		   (CodRequerimiento
           ,CodRequerimientoMovimiento
           ,CodMotivo
           ,CodArea
           ,Descripcion
		   ,FechaSolicitud
           ,UsuarioRegistro
           ,FechaRegistro
           )
     VALUES
           (@CodRequerimiento
           ,@vCodRequerimientoMovimiento
           ,@CodMotivo
           ,@CodArea
           ,@Descripcion
		   ,@FechaSolicitud
           ,@UsuarioRegistro
           ,GETDATE())

	SELECT @CodComentario = SCOPE_IDENTITY()
		
END
GO
  
DROP PROCEDURE IF EXISTS dbo.SP_UBIGEO_OBTENER_DEPARTAMENTO
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 30/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_UBIGEO_OBTENER_DEPARTAMENTO
-- PARAMETOS				:  
-- FUNCIONALIDAD			: 
--================================================================================================================================

CREATE PROCEDURE dbo.SP_UBIGEO_OBTENER_DEPARTAMENTO
 
AS
BEGIN
	 SELECT CodDepartamento
		, Departamento
	 FROM Ubigeo
	 GROUP BY 
		  CodDepartamento
		, Departamento

END
GO

DROP PROCEDURE IF EXISTS dbo.SP_UBIGEO_OBTENER_PROVINCIA
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 30/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_UBIGEO_OBTENER_PROVINCIA
-- PARAMETOS				: @CodDepartamento VARCHAR(2)
-- FUNCIONALIDAD			: 
--================================================================================================================================

CREATE PROCEDURE dbo.SP_UBIGEO_OBTENER_PROVINCIA
 @CodDepartamento VARCHAR(2)
AS
BEGIN
	 SELECT CodProvincia
		, Provincia
	 FROM Ubigeo
	 WHERE CodDepartamento = @CodDepartamento
	 GROUP BY 
		  CodProvincia
		, Provincia

END
GO

DROP PROCEDURE IF EXISTS dbo.SP_UBIGEO_OBTENER_DISTRITO
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 30/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_UBIGEO_OBTENER_DISTRITO
-- PARAMETOS				: @CodDepartamento VARCHAR(2) ,  @CodProvincia VARCHAR(2)
-- FUNCIONALIDAD			: 
--================================================================================================================================

CREATE PROCEDURE dbo.SP_UBIGEO_OBTENER_DISTRITO
 @CodDepartamento VARCHAR(2),
 @CodProvincia VARCHAR(2)
AS
BEGIN
	 SELECT CodUbigeo
		, Distrito
	 FROM Ubigeo
	 WHERE CodDepartamento = @CodDepartamento
		AND CodProvincia = @CodProvincia
	 GROUP BY 
		  CodUbigeo
		, Distrito

END
GO

DROP PROCEDURE IF EXISTS dbo.SP_REQUERIMIENTO_ASIGNACION_INSERTAR
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 30/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: SP_REQUERIMIENTO_ASIGNACION_INSERTAR
-- PARAMETOS				: @CodRequerimientoMovimiento
-- FUNCIONALIDAD			: 
--================================================================================================================================

CREATE PROCEDURE dbo.SP_REQUERIMIENTO_ASIGNACION_INSERTAR
@CodRequerimiento BIGINT,
@Estado INT,
@UsuarioAsignado VARCHAR(50),
@UsuarioRegistro VARCHAR(50)
AS
BEGIN
	DECLARE @EXISTE_USUARIO_ASIGNADO INT = 0
	DECLARE @EXITE_MOVIMIENTO INT = 0

	DECLARE @vCodRequerimientoMovimiento BIGINT = 0

	SET @EXISTE_USUARIO_ASIGNADO = (SELECT COUNT(CodRequerimientoAsignacion) FROM RequerimientoAsignacion WHERE CodRequerimiento = @CodRequerimiento)
	
	IF @EXISTE_USUARIO_ASIGNADO=0
	BEGIN
		INSERT INTO RequerimientoMovimiento (CodRequerimiento,Estado,UsuarioRegistro,FechaRegistro)
								  VALUES (@CodRequerimiento,3,@UsuarioRegistro,GETDATE())
								  
		SET @vCodRequerimientoMovimiento = SCOPE_IDENTITY()

		UPDATE Requerimiento SET 
		Estado = 3
		WHERE CodRequerimiento = @CodRequerimiento
			
	END

	IF @EXISTE_USUARIO_ASIGNADO>0
	BEGIN
		SET @EXITE_MOVIMIENTO = (SELECT COUNT(CodRequerimientoMovimiento) FROM RequerimientoMovimiento WHERE CodRequerimiento = @CodRequerimiento AND Estado = @Estado)
		IF @EXITE_MOVIMIENTO = 0 
		BEGIN
		INSERT INTO RequerimientoMovimiento (CodRequerimiento,Estado,UsuarioRegistro,FechaRegistro)
								  VALUES (@CodRequerimiento,@Estado,@UsuarioRegistro,GETDATE())
								   
			SET @vCodRequerimientoMovimiento = SCOPE_IDENTITY()
		END
		IF @EXITE_MOVIMIENTO > 0 
		BEGIN
			SET @vCodRequerimientoMovimiento = (SELECT CodRequerimientoMovimiento FROM RequerimientoMovimiento WHERE CodRequerimiento = @CodRequerimiento AND Estado = @Estado)
		END
		
	END	   

	INSERT INTO RequerimientoAsignacion
		   (CodRequerimiento
           ,CodRequerimientoMovimiento
           ,UsuarioAsignado
           ,UsuarioRegistro
           ,UsuarioModificacion
		   ,FechaRegistro
		   ,FechaModificacion
           )
     VALUES
           (@CodRequerimiento
			,@vCodRequerimientoMovimiento			
			,@UsuarioAsignado
			,@UsuarioRegistro 
			,NULL
           ,GETDATE()
		   ,NULL)
		
END
GO

DROP PROCEDURE IF EXISTS dbo.SP_PARAMETRO_OBTENER_GRUPO
GO
 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 26/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_PARAMETRO_OBTENER_GRUPO
-- PARAMETROS				: @Grupo
-- FUNCIONALIDAD			: Obtener los parametros por grupos
--================================================================================================================================

CREATE PROCEDURE dbo.SP_PARAMETRO_OBTENER_GRUPO	
@Grupo VARCHAR(50) = NULL

AS
BEGIN

	SELECT Grupo
		,Parametro
		,Descripcion 
		,Valor
		,Referencia
		,Referencia2
	FROM Parametro
	WHERE Grupo in (select value from STRING_SPLIT(@grupo,','))
		AND Estado = 1
	ORDER BY Grupo,Orden

END
GO

DROP PROCEDURE IF EXISTS dbo.SP_REQUERIMIENTO_MOVIMIENTO_PENULTIMO
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 30/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: SP_REQUERIMIENTO_MOVIMIENTO_PENULTIMO
-- PARAMETOS				: @CodRequerimiento
-- FUNCIONALIDAD			: Obtener el estado del penultimo registro 
--================================================================================================================================

CREATE PROCEDURE dbo.SP_REQUERIMIENTO_MOVIMIENTO_PENULTIMO
@CodRequerimiento BIGINT
AS
BEGIN
	SELECT TOP 1 RM.Estado 
	FROM RequerimientoMovimiento RM	
	WHERE RM.CodRequerimiento=@CodRequerimiento 
		AND RM.Estado NOT IN(9 ,10) 
	ORDER BY RM.CodRequerimientoMovimiento DESC

END
GO

DROP PROCEDURE IF EXISTS dbo.SP_RECURSO_OBTENER_IMAGENES
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 30/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: SP_RECURSO_OBTENER_IMAGENES
-- PARAMETOS				: @CodRequerimiento
-- FUNCIONALIDAD			: Obtener el estado del penultimo registro 
--================================================================================================================================

CREATE PROCEDURE dbo.SP_RECURSO_OBTENER_IMAGENES	
@CodRequerimiento BIGINT
AS
BEGIN

	SELECT R.CodTabla, R.ReferenciaID, R.LaserficheID FROM Recurso R
	INNER JOIN Requerimiento RE ON R.ReferenciaID = RE.CodRequerimiento
	LEFT JOIN Parametro PR ON PR.CodParametro = R.CodTabla
	WHERE R.ReferenciaID = @CodRequerimiento 
		
END
GO

DROP PROCEDURE IF EXISTS dbo.SP_RECURSO_OBTENER
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 30/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_RECURSO_OBTENER
-- PARAMETOS				: @CodRequerimiento
-- FUNCIONALIDAD			: Obtener el estado del penultimo registro 
--================================================================================================================================

CREATE PROCEDURE dbo.SP_RECURSO_OBTENER	
@CodTabla BIGINT,
@ReferenciaID BIGINT,
@LaserficheID INT
AS
BEGIN

	SELECT 
		R.CodTabla, 
		R.ReferenciaID, 
		R.LaserficheID,
		R.Descripcion
	FROM Recurso R
	-- INNER JOIN Requerimiento RE ON R.ReferenciaID = RE.CodRequerimiento
	LEFT JOIN Parametro PR ON PR.CodParametro = R.CodTabla
	WHERE R.CodTabla = @CodTabla AND
	(@ReferenciaID IS NULL OR R.ReferenciaID = @ReferenciaID) AND
	(@LaserficheID IS NULL OR R.LaserficheID = @LaserficheID)
		
END
GO

DROP PROCEDURE IF EXISTS dbo.SP_AREA_OBTENER
GO
---=================================================================================================================================
-- FECHA DE CREACIÓN		: 30/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: SP_AREA_OBTENER
-- PARAMETROS				:							  
-- FUNCIONALIDAD			: Obtener lista de las areas
--================================================================================================================================

CREATE PROCEDURE dbo.SP_AREA_OBTENER	

AS
BEGIN

	SELECT A.Codigo
		,A.Descripcion	
	FROM Area A
END
GO

DROP PROCEDURE IF EXISTS dbo.SP_RECURSO_ELIMINAR
GO

--=================================================================================================================================
-- FECHA DE CREACIÓN		: 30/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: SP_RECURSO_ELIMINAR
-- PARAMETOS				: @LaserficheID
-- FUNCIONALIDAD			: Eliminar  registro de recurso 
--================================================================================================================================

CREATE PROCEDURE dbo.SP_RECURSO_ELIMINAR
@LaserficheID INT
AS
BEGIN

	DELETE
	FROM Recurso
	WHERE LaserficheID= @LaserficheID	
			
END
GO

DROP PROCEDURE IF EXISTS dbo.SP_REQUERIMIENTO_DERIVADO_A_OCM_INSERTAR
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 30/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_REQUERIMIENTO_DERIVADO_A_OCM_INSERTAR
-- PARAMETOS				: @CodRequerimientoMovimiento
-- FUNCIONALIDAD			: 
--================================================================================================================================

CREATE PROCEDURE dbo.SP_REQUERIMIENTO_DERIVADO_A_OCM_INSERTAR
@CodRequerimientoMovimiento int OUTPUT,
@CodRequerimiento BIGINT,
@Estado INT,
@UsuarioRegistro VARCHAR(50)

AS
BEGIN
	 INSERT INTO RequerimientoMovimiento (CodRequerimiento,Estado,UsuarioRegistro,FechaRegistro)
								  VALUES (@CodRequerimiento,@Estado,@UsuarioRegistro,GETDATE())

     SELECT @CodRequerimientoMovimiento = SCOPE_IDENTITY()


	 UPDATE Requerimiento SET 
	 Estado = @Estado,
	 UsuarioRegistro = @UsuarioRegistro,
	 FechaModificacion = GETDATE()
	 WHERE CodRequerimiento = @CodRequerimiento
		
END
GO

DROP PROCEDURE IF EXISTS dbo.SP_REQUERIMIENTO_DERIVAR_PARA_SU_NOTIFICACION_INSERTAR
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 24/05/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_REQUERIMIENTO_DERIVAR_PARA_SU_NOTIFICACION_INSERTAR
-- PARAMETOS				: @CodRequerimientoMovimiento int OUTPUT,
--							  @CodRequerimiento BIGINT,
--							  @Estado INT,
--							  @UsuarioAsignado VARCHAR(50),
--							  @UsuarioRegistro VARCHAR(50)
-- FUNCIONALIDAD			: 
--================================================================================================================================

CREATE PROCEDURE dbo.SP_REQUERIMIENTO_DERIVAR_PARA_SU_NOTIFICACION_INSERTAR
@CodRequerimientoMovimiento int OUTPUT,
@CodRequerimiento BIGINT,
@Estado INT,
@UsuarioAsignado VARCHAR(50),
@UsuarioRegistro VARCHAR(50)

AS
BEGIN
	 INSERT INTO RequerimientoMovimiento (CodRequerimiento,Estado,UsuarioRegistro,FechaRegistro)
								  VALUES (@CodRequerimiento,@Estado,@UsuarioRegistro,GETDATE())

     SELECT @CodRequerimientoMovimiento = SCOPE_IDENTITY()

	 INSERT INTO RequerimientoAsignacion (CodRequerimiento,CodRequerimientoMovimiento,UsuarioAsignado,UsuarioRegistro,FechaRegistro) VALUES
										(@CodRequerimiento,@CodRequerimientoMovimiento,@UsuarioAsignado,@UsuarioRegistro,GETDATE())
	 
	 UPDATE Requerimiento SET 
	 Estado = @Estado,
	 UsuarioModificacion = @UsuarioRegistro,
	 FechaModificacion = GETDATE()
	 WHERE CodRequerimiento = @CodRequerimiento
		
END
GO


DROP PROCEDURE IF EXISTS dbo.SP_REQUERIMIENTO_APROBAR_INSERTAR
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 24/05/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_REQUERIMIENTO_APROBAR_INSERTAR
-- PARAMETOS				: @CodRequerimientoMovimiento int OUTPUT,
--							  @CodRequerimiento BIGINT,
--							  @Estado INT,
--							  @UsuarioRegistro VARCHAR(50)
-- FUNCIONALIDAD			: 
--================================================================================================================================

CREATE PROCEDURE dbo.SP_REQUERIMIENTO_APROBAR_INSERTAR
@CodRequerimientoMovimiento int OUTPUT,
@CodRequerimiento BIGINT,
@Estado INT,
@UsuarioRegistro VARCHAR(50)

AS
BEGIN

	 INSERT INTO RequerimientoMovimiento (CodRequerimiento,Estado,UsuarioRegistro,FechaRegistro)
								  VALUES (@CodRequerimiento,@Estado,@UsuarioRegistro,GETDATE())

     SELECT @CodRequerimientoMovimiento = SCOPE_IDENTITY()

	 UPDATE Requerimiento SET 
	 Estado = @Estado,
	 UsuarioModificacion = @UsuarioRegistro,
	 FechaModificacion = GETDATE()
	 WHERE CodRequerimiento = @CodRequerimiento
END
GO

DROP PROCEDURE IF EXISTS dbo.SP_REQUERIMIENTO_ATENDIDO_INSERTAR
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 24/05/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_REQUERIMIENTO_ATENDIDO_INSERTAR
-- PARAMETOS				: @CodRequerimientoMovimiento int OUTPUT,
--							  @CodRequerimiento BIGINT,
--							  @Estado INT,
--							  @UsuarioRegistro VARCHAR(50)
--							  @FechaEntrega DATETIME
-- FUNCIONALIDAD			: 
--================================================================================================================================

CREATE PROCEDURE dbo.SP_REQUERIMIENTO_ATENDIDO_INSERTAR
@CodRequerimientoMovimiento int OUTPUT,
@CodRequerimiento BIGINT,
@Estado INT,
@UsuarioRegistro VARCHAR(50),
@FechaEntrega DATETIME
AS
BEGIN

	 INSERT INTO RequerimientoMovimiento (CodRequerimiento,Estado,UsuarioRegistro,FechaRegistro)
								  VALUES (@CodRequerimiento,@Estado,@UsuarioRegistro,GETDATE())

     SELECT @CodRequerimientoMovimiento = SCOPE_IDENTITY()

	 UPDATE Requerimiento SET 
	 Estado = @Estado,
	 UsuarioModificacion = @UsuarioRegistro,
	 FechaModificacion = GETDATE(),
	 FechaEntrega = @FechaEntrega
	 WHERE CodRequerimiento = @CodRequerimiento
END
GO

DROP PROCEDURE IF EXISTS dbo.SP_CONSULTA_OBTENER_DETALLE
GO
 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 4/06/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: SP_CONSULTA_OBTENER_DETALLE
-- PARAMETROS				: @CodConsulta, @CodTabla
-- FUNCIONALIDAD			: Obtener el detalle de la consulta
--================================================================================================================================

CREATE PROCEDURE dbo.SP_CONSULTA_OBTENER_DETALLE
@CodConsulta INT,
@CodTabla INT

AS
BEGIN

	SELECT	C.CodConsulta
			,C.FechaRegistro
			,C.NroDocumento
			,C.NombresCompletos
			,C.CorreoElectronico
			,C.Celular
			,C.Direccion
			,C.Descripcion
			--,R.LaserficheID
			--,R.CodTabla
			--,R.ReferenciaID
	FROM Consulta C
	--LEFT JOIN Recurso R ON R.ReferenciaID =  C.CodConsulta AND R.CodTabla = @CodTabla
	WHERE
		(ISNULL(@CodConsulta,'') = '' OR C.CodConsulta = @CodConsulta)  

END
GO

----SP_CONSULTA_OBTENER_DETALLE '9', '4'

DROP PROCEDURE IF EXISTS dbo.SP_CONSULTA_REGISTRAR
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 14/06/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: SP_CONSULTA_REGISTRAR
-- PARAMETOS				: @Tipo_Documento INT,
--							  @NroDocumento VARCHAR(20),
--							  @NombresCompletos VARCHAR(200),
--							  @Direccion VARCHAR(200),
--							  @CorreoElectronico VARCHAR(100),
--                            @Celular VARCHAR(20),
--                            @Descripcion VARCHAR(1000),
--                            @Estado BIT,
--                            @UsuarioRegistro VARCHAR(50)
-- FUNCIONALIDAD			: 
--================================================================================================================================

CREATE PROCEDURE dbo.SP_CONSULTA_REGISTRAR
@Tipo_Documento INT,
@NroDocumento VARCHAR(20),
@NombresCompletos VARCHAR(200),
@Direccion VARCHAR(200),
@CorreoElectronico VARCHAR(100),
@Celular VARCHAR(20),
@Descripcion VARCHAR(1000),
@Estado BIT,
@UsuarioRegistro VARCHAR(50),
@CodConsulta INT OUTPUT
AS
BEGIN

	 INSERT INTO Consulta(TipoDocumento
						,NroDocumento
						,NombresCompletos
						,Direccion,CorreoElectronico
						,Celular
						,Descripcion
						,Estado
						,UsuarioRegistro
						,FechaRegistro)
				VALUES (@Tipo_Documento
						,@NroDocumento
						,@NombresCompletos
						,@Direccion
						,@CorreoElectronico
						,@Celular
						,@Descripcion
						,@Estado
						,@UsuarioRegistro
						,GETDATE())  

	 SELECT @CodConsulta = SCOPE_IDENTITY()
	
END
GO

DROP PROCEDURE IF EXISTS dbo.SP_REQUERIMIENTO_SEGUIMIENTO
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 07/06/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_REQUERIMIENTO_SEGUIMIENTO
-- PARAMETOS				: @TipoIDN VARCHAR(20),
--							  @Identificacion VARCHAR(20)
-- FUNCIONALIDAD			:  
--================================================================================================================================

CREATE PROCEDURE dbo.SP_REQUERIMIENTO_SEGUIMIENTO	 
@FechaRegistroInicio DATE NULL,
@FechaRegistroFin DATE NULL,
@FechaAsignacionInicio DATE NULL,
@FechaAsignacionFin DATE NULL,

@FechaGestionInicio DATE NULL,
@FechaGestionFin DATE NULL,
@FechaAtencionInicio DATE NULL,
@FechaAtencionFin DATE NULL,

@TipoRequeriminto INT NULL,
@Estado INT NULL

AS
BEGIN

	SELECT 
		  TEMP.FechaRegistro
		, TEMP.FechaAsignacion
		, TEMP.FechaGestion
		, TEMP.FechaAtencion
		, TEMP.Estado
		, TEMP.TipoRequerimiento
		, TEMP.UsuarioAsignado
		, TEMP.DiasTranscurrido
		, TEMP.NroRequerimiento
		, TEMP.FechaRegistro
		

	FROM
	(
			SELECT
				  CAST( RMR.FechaRegistro AS DATE) FechaRegistro
				, CAST( RMA.FechaRegistro AS DATE) FechaAsignacion
				, CAST( RMG.FechaRegistro AS DATE) FechaGestion
				, CAST( RMAT.FechaRegistro AS DATE) FechaAtencion
				, PE.Descripcion Estado
				, PT.Descripcion TipoRequerimiento
				, MAX(U.NombresApellidos) UsuarioAsignado
				, DATEDIFF (DAY,RE.FechaRegistro, ( CASE WHEN RE.Estado = 8 THEN   RMAT.FechaRegistro	ELSE  CASE WHEN RE.Estado = 2 THEN  RMANULAR.FechaRegistro ELSE  GETDATE()	END  END)) DiasTranscurrido
 				, RE.NroRequerimiento
		
		
			FROM   Requerimiento RE
			LEFT JOIN RequerimientoMovimiento RMR ON RE.CodRequerimiento = RMR.CodRequerimiento AND RMR.Estado = 1
			LEFT JOIN RequerimientoMovimiento RMA ON RE.CodRequerimiento = RMA.CodRequerimiento AND RMA.Estado = 3
			LEFT JOIN RequerimientoMovimiento RMG ON RE.CodRequerimiento = RMG.CodRequerimiento AND RMG.Estado = 4
			LEFT JOIN RequerimientoMovimiento RMAT ON RE.CodRequerimiento = RMAT.CodRequerimiento AND RMAT.Estado = 8
			LEFT JOIN RequerimientoAsignacion RA ON RE.CodRequerimiento = RA.CodRequerimiento 
			LEFT JOIN RequerimientoMovimiento RMANULAR ON RE.CodRequerimiento = RMANULAR.CodRequerimiento AND RMANULAR.Estado = 2
			INNER JOIN Parametro PE ON PE.Grupo = 100 AND PE.Parametro = RE.Estado
			INNER JOIN Parametro PT ON PT.Grupo = 900 AND PT.Parametro = RE.CodOrigen
			LEFT JOIN Usuario U ON U.UsuarioWeb = RA.UsuarioAsignado
			WHERE 
			((@FechaRegistroInicio IS NULL AND @FechaRegistroFin IS NULL) OR (CAST(RMR.FechaRegistro AS DATE) BETWEEN @FechaRegistroInicio AND @FechaRegistroFin )) AND
			((@FechaAsignacionInicio IS NULL AND @FechaAsignacionFin IS NULL) OR (CAST(RMA.FechaRegistro AS DATE) BETWEEN @FechaAsignacionInicio AND @FechaAsignacionFin)) AND
			((@FechaGestionInicio IS NULL AND @FechaGestionFin IS NULL) OR  (CAST(RMG.FechaRegistro AS DATE) BETWEEN @FechaGestionInicio AND @FechaGestionFin)) AND
 			((@FechaAtencionInicio IS NULL AND @FechaAtencionFin IS NULL) OR  (CAST(RMAT.FechaRegistro AS DATE) BETWEEN @FechaAtencionInicio AND @FechaAtencionFin)) AND
			(@TipoRequeriminto IS NULL OR RE.CodOrigen = @TipoRequeriminto) AND
 			(@Estado IS NULL OR RE.Estado = @Estado)
			GROUP BY
				  RMR.FechaRegistro
				, RMA.FechaRegistro
				, RMG.FechaRegistro
				, RMAT.FechaRegistro
				, PE.Descripcion
				, PT.Descripcion
				, RE.NroRequerimiento
				, RE.FechaRegistro
				, RE.Estado
				, RMAT.FechaRegistro
				, RMANULAR.FechaRegistro

	)TEMP
			GROUP BY
			  TEMP.FechaRegistro
			, TEMP.FechaAsignacion
			, TEMP.FechaGestion
			, TEMP.FechaAtencion
			, TEMP.Estado
			, TEMP.TipoRequerimiento
			, TEMP.NroRequerimiento
			, TEMP.FechaRegistro
			, TEMP.UsuarioAsignado
			, TEMP.DiasTranscurrido


END
GO

DROP PROCEDURE IF EXISTS dbo.SP_RQUERIMEINTO_ASIGNACION_OBTENER_COD_MOVIMIENTO
GO

CREATE PROCEDURE dbo.SP_RQUERIMEINTO_ASIGNACION_OBTENER_COD_MOVIMIENTO
@CodRequerimientoMovimiento BIGINT,
@CodRequerimiento BIGINT
AS
BEGIN
	DECLARE @EXISTES INT = 0
	SET @EXISTES = (SELECT COUNT(RMA.CodRequerimientoAsignacion) FROM RequerimientoAsignacion RMA WHERE	RMA.CodRequerimientoMovimiento = @CodRequerimientoMovimiento)
	 
	IF @EXISTES > 0
	BEGIN
		
		;WITH CTE AS (
			SELECT 
			   TOP 1
			   RMA.CodRequerimientoAsignacion
			  ,RMA.CodRequerimiento
			  ,RMA.CodRequerimientoMovimiento
			  ,U.NombresApellidos UsuarioAsignado
			  ,UR.NombresApellidos UsuarioRegistro
			  ,RMA.FechaRegistro
     
			FROM RequerimientoAsignacion RMA
			LEFT JOIN Usuario U ON U.UsuarioWeb = RMA.UsuarioAsignado
			LEFT JOIN Usuario UR ON UR.UsuarioWeb = RMA.UsuarioRegistro
			WHERE	RMA.CodRequerimiento = @CodRequerimiento   AND RMA.CodRequerimientoMovimiento < @CodRequerimientoMovimiento
			ORDER BY RMA.CodRequerimientoAsignacion DESC
		)

	 
		SELECT
			  CodRequerimientoAsignacion
			  ,CodRequerimiento
			  ,CodRequerimientoMovimiento
			  ,UsuarioAsignado
			  ,UsuarioRegistro
			  ,FechaRegistro
		FROM
		CTE
		UNION ALL
		SELECT RMA.CodRequerimientoAsignacion
		  ,RMA.CodRequerimiento
		  ,CodRequerimientoMovimiento
		  ,U.NombresApellidos UsuarioAsignado
		  ,UR.NombresApellidos UsuarioRegistro
		  ,RMA.FechaRegistro
     
		FROM RequerimientoAsignacion RMA
		LEFT JOIN Usuario U ON U.UsuarioWeb = RMA.UsuarioAsignado
		LEFT JOIN Usuario UR ON UR.UsuarioWeb = RMA.UsuarioRegistro
		WHERE	RMA.CodRequerimientoMovimiento = @CodRequerimientoMovimiento
	END
	ELSE
	BEGIN
		SELECT 
		   TOP 1
		   RMA.CodRequerimientoAsignacion
		  ,RMA.CodRequerimiento
		  ,RMA.CodRequerimientoMovimiento
		  ,U.NombresApellidos UsuarioAsignado
		  ,UR.NombresApellidos UsuarioRegistro
		  ,RMA.FechaRegistro
     
		FROM RequerimientoAsignacion RMA
		LEFT JOIN Usuario U ON U.UsuarioWeb = RMA.UsuarioAsignado
		LEFT JOIN Usuario UR ON UR.UsuarioWeb = RMA.UsuarioRegistro
		WHERE	RMA.CodRequerimiento = @CodRequerimiento   AND RMA.CodRequerimientoMovimiento < @CodRequerimientoMovimiento
		ORDER BY
		RMA.FechaRegistro DESC
	END

END
GO

DROP PROCEDURE IF EXISTS dbo.SP_COMENTARIO_INSERTAR_ANULAR
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 28/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WIILIAM QUIROZ
-- STORE PROCEDURE			: SP_COMENTARIO_INSERTAR_ANULAR
-- PARAMETOS				: @CodRequerimiento BIGINT,@CodRequerimientoMovimiento BIGINT,@CodMotivo INT, 
--							  @CodArea INT,@Descripcion VARCHAR(300),@FechaSolicitud DATE,@UsuarioRegistro VARCHAR(50)						  
-- FUNCIONALIDAD			: Genera un registro en la tabla comentario
--=================================================================================================================================

CREATE PROCEDURE dbo.SP_COMENTARIO_INSERTAR_ANULAR	
@CodRequerimiento BIGINT,
@CodRequerimientoMovimiento BIGINT,
@CodMotivo INT,
@CodArea INT,
@Descripcion VARCHAR(300),
@FechaSolicitud DATE,
@UsuarioRegistro VARCHAR(50),
@CodComentario INT OUTPUT

AS
BEGIN

	INSERT INTO Comentario 
		   (CodRequerimiento
           ,CodRequerimientoMovimiento
           ,CodMotivo
           ,CodArea
           ,Descripcion
		   ,FechaSolicitud
           ,UsuarioRegistro
           ,FechaRegistro
           )
     VALUES
           (@CodRequerimiento
           ,@CodRequerimientoMovimiento
           ,@CodMotivo
           ,@CodArea
           ,@Descripcion
		   ,@FechaSolicitud
           ,@UsuarioRegistro
           ,GETDATE())

SELECT @CodComentario = SCOPE_IDENTITY()
		
END
GO

DROP PROCEDURE IF EXISTS dbo.SP_REQUERIMIENTO_ACTUALIZAR_PENDIENTE_SIN_ATENCION
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 31/05/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: SP_REQUERIMIENTO_ACTUALIZAR_PENDIENTE_SIN_ATENCION
-- PARAMETOS				: 
-- FUNCIONALIDAD			: 
--================================================================================================================================

CREATE PROCEDURE dbo.SP_REQUERIMIENTO_ACTUALIZAR_PENDIENTE_SIN_ATENCION
AS
BEGIN		

--------1. Requerimiento ha pasado 7 días----------------------------------------------------------------------------------------

		CREATE TABLE #TRequerimientoMovimiento (CodRequerimiento BIGINT, FechaRegistro DATETIME)

		INSERT INTO #TRequerimientoMovimiento
		SELECT  RM.CodRequerimiento, MAX(RM.FechaRegistro) AS FechaRegistro
		FROM  Requerimiento RE
		INNER JOIN RequerimientoMovimiento RM ON RM.CodRequerimiento = RE.CodRequerimiento
		WHERE RE.Estado NOT IN (8, 2, 9, 10)					
		GROUP BY RM.CodRequerimiento

		;WITH cte_data(CodRequerimiento, FechaRegistro) AS
		(
			SELECT C.CodRequerimiento, MAX( C.FechaRegistro)
			FROM #TRequerimientoMovimiento D
			INNER JOIN  Comentario C ON C.CodRequerimiento = D.CodRequerimiento
			GROUP BY C.CodRequerimiento
		)

		UPDATE RM
		SET fechaRegistro =  CASE WHEN RM.FechaRegistro > C.FechaRegistro THEN RM.FechaRegistro ELSE C.FechaRegistro END
		FROM #TRequerimientoMovimiento RM
		INNER JOIN cte_data C ON C.CodRequerimiento = RM.CodRequerimiento

		DELETE #TRequerimientoMovimiento
		WHERE CONVERT(DATE,DATEADD(d,7,FechaRegistro),103)  >= CONVERT(DATE,GETDATE(),103)

		--SELECT CONVERT(DATE,DATEADD(d,7,FechaRegistro),103), * FROM #TRequerimientoMovimiento

		INSERT INTO RequerimientoMovimiento (CodRequerimiento,Estado,UsuarioRegistro,FechaRegistro)
		SELECT CodRequerimiento, 9, 'SAR',GETDATE()
		FROM #TRequerimientoMovimiento

		UPDATE R 
		SET Estado = 9,			
		FechaModificacion = GETDATE()			
		FROM #TRequerimientoMovimiento RM
		INNER JOIN Requerimiento R ON RM.CodRequerimiento = R.CodRequerimiento		

		DROP TABLE #TRequerimientoMovimiento


--------2. Requerimiento transcurrido 15 días----------------------------------------------------------------------------------------

		CREATE TABLE #TRequerimiento (CodRequerimiento BIGINT, FechaRegistro DATETIME)

		INSERT INTO #TRequerimiento
		SELECT  Re.CodRequerimiento, MAX(Re.FechaRegistro) AS FechaRegistro
		FROM  Requerimiento RE
		INNER JOIN RequerimientoMovimiento RM ON RM.CodRequerimiento = RE.CodRequerimiento
		WHERE RE.Estado NOT IN (8, 2, 10)						
		GROUP BY Re.CodRequerimiento

		DELETE #TRequerimiento
		WHERE CONVERT(DATE,DATEADD(d,15,FechaRegistro),103) >= CONVERT(DATE,GETDATE(),103)

		--SELECT CONVERT(DATE,DATEADD(d,15,FechaRegistro),103), * FROM #TRequerimiento

		INSERT INTO RequerimientoMovimiento (CodRequerimiento,Estado,UsuarioRegistro,FechaRegistro)
		SELECT CodRequerimiento, 10, 'SAR',GETDATE()
		FROM #TRequerimiento
			
		UPDATE RE 
		SET Estado = 10,			
		FechaModificacion = GETDATE()			
		FROM #TRequerimiento TR
		INNER JOIN Requerimiento RE ON TR.CodRequerimiento = RE.CodRequerimiento	

		DROP TABLE #TRequerimiento

END
GO

DROP PROCEDURE IF EXISTS dbo.SP_REQUERIMIENTO_OBTENER_REPORTE
GO
 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 28/06/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: SP_REQUERIMIENTO_OBTENER_REPORTE
-- PARAMETROS				: @CodTipoRequerimiento, @Estado, @FechaRegistroIni, @FechaRegistroFin
-- FUNCIONALIDAD			: Obtener la lista de requerimientos para generación de reportes 
--================================================================================================================================

CREATE PROCEDURE dbo.SP_REQUERIMIENTO_OBTENER_REPORTE
@CodTipoRequerimiento INT NULL,
@Estado INT NULL,
@FechaRegistroInicio DATETIME NULL,
@FechaRegistroFin DATETIME NULL

AS
BEGIN

SELECT	RE.NroRequerimiento AS NroRequerimiento	
		,(CASE WHEN RE.EsMenor = 0 THEN 'NO' ELSE 'SI' END) AS EsMenor
		,PTI.Descripcion AS TipoDocumento
		,RD.NroDocumento
		,RD.NombresApellidos
		,RD.RazonSocial
		,RE.Descripcion	
		,UB.Departamento
		,UB.Provincia
		,UB.Distrito
		,RD.Telefono
		,RD.Celular
		,PRP.Descripcion AS Producto
		,PRM.Descripcion AS Motivo
		,PRT.Descripcion AS TipoRequerimiento
		,PRO.Descripcion As Origen
		,AG.Oficina AS Agencia
		,RE.FechaRegistro		
		,PRE.Descripcion AS Estado
		,PTR.Descripcion As FormaEnvio
	FROM Requerimiento RE
	INNER JOIN RequerimientoDetalle RD ON RE.CodRequerimiento = RD.CodRequerimiento
	LEFT JOIN UBIGEO UB ON UB.CodUbigeo = RD.CodUbigeo	
	LEFT JOIN Parametro PRE ON PRE.Parametro = RE.Estado AND PRE.Grupo = 100
	LEFT JOIN Parametro PTI ON PTI.Parametro = RD.TipoDocumento AND PTI.Grupo = 300	
	LEFT JOIN Parametro PRP ON PRP.Parametro = RE.CodProducto AND PRP.Grupo = 400
	LEFT JOIN Parametro PRM ON PRM.Parametro = RE.CodMotivo AND PRM.Grupo = 500
	LEFT JOIN Parametro PRT ON PRT.Parametro = RE.CodTipoRequerimiento AND PRT.Grupo = 600	
	LEFT JOIN Parametro PTR ON PTR.Parametro = RE.CodFormaEnvio AND PTR.Grupo = 700
	LEFT JOIN Parametro PRO ON PRO.Parametro = RE.CodOrigen AND PRO.Grupo = 900
	LEFT JOIN Agencia AG ON AG.CodAgencia = RE.CodAgencia		
	WHERE	
		(@CodTipoRequerimiento IS NULL OR RE.CodTipoRequerimiento = @CodTipoRequerimiento)	
		AND (@Estado IS NULL OR RE.Estado = @Estado)
		AND ((@FechaRegistroInicio IS NULL OR @FechaRegistroInicio = '') OR CAST(RE.FechaRegistro AS DATE) >= CAST(@FechaRegistroInicio AS DATE)) AND
		((@FechaRegistroFin IS NULL OR @FechaRegistroFin = '') OR CAST(RE.FechaRegistro AS DATE) <= CAST(@FechaRegistroFin AS DATE)) 
		--AND ((@FechaRegistroInicio IS NULL AND @FechaRegistroFin IS NULL) OR (CAST(RE.FechaRegistro AS DATE) BETWEEN @FechaRegistroInicio AND @FechaRegistroFin ))		

END
GO

--EXEC SP_REQUERIMIENTO_OBTENER_REPORTE NULL, NULL,'20210707',''


DROP PROCEDURE IF EXISTS dbo.SP_REQUERIMIENTO_GESTION_REPORTE
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 01/07/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: SP_REQUERIMIENTO_GESTION_REPORTE
-- PARAMETOS				: @FechaRegistroInicio DATE NULL, @FechaRegistroFin DATE NULL, @FechaAsignacionInicio DATE NULL,
--                          : @FechaAsignacionFin DATE NULL, @FechaGestionInicio DATE NULL, @FechaGestionFin DATE NULL,
--                          : @FechaAtencionInicio DATE NULL, @FechaAtencionFin DATE NULL, @TipoRequeriminto INT NULL, @Estado INT NULL
-- FUNCIONALIDAD			:  
--================================================================================================================================

CREATE PROCEDURE dbo.SP_REQUERIMIENTO_GESTION_REPORTE	 
@FechaRegistroInicio DATETIME NULL,
@FechaRegistroFin DATETIME NULL,
@FechaAsignacionInicio DATE NULL,
@FechaAsignacionFin DATE NULL,

@FechaGestionInicio DATE NULL,
@FechaGestionFin DATE NULL,
@FechaAtencionInicio DATE NULL,
@FechaAtencionFin DATE NULL,

@TipoRequeriminto INT NULL,
@Estado INT NULL

AS
BEGIN

	SELECT 
			TEMP.NroRequerimiento
		, TEMP.FechaRegistro
		, TEMP.FechaAsignacion
		, TEMP.FechaGestion	
		, TEMP.FechaDerivacionOCM
		, TEMP.FechaSinNotificacion
		, TEMP.FechaAtencion
		, TEMP.DetalleReclamo
		, TEMP.Estado
		, TEMP.Origen
		, TEMP.UsuarioAsignado
		, TEMP.DiasTranscurrido			

	FROM
	(
			SELECT				
					 CAST( RMR.FechaRegistro AS DATE) FechaRegistro
				, CAST( RMA.FechaRegistro AS DATE) FechaAsignacion
				, CAST( RMG.FechaRegistro AS DATE) FechaGestion
				, CAST( RMD.FechaRegistro AS DATE) FechaDerivacionOCM
				, CAST( RMN.FechaRegistro AS DATE) FechaSinNotificacion
				, CAST( RMAT.FechaRegistro AS DATE) FechaAtencion
				, RE.Descripcion DetalleReclamo
				, PE.Descripcion Estado	
				, PT.Descripcion Origen
				, MAX(U.NombresApellidos) UsuarioAsignado
				, DATEDIFF (DAY,RE.FechaRegistro, ( CASE WHEN RE.Estado = 8 THEN   RMAT.FechaRegistro	ELSE  CASE WHEN RE.Estado = 2 THEN  RMANULAR.FechaRegistro ELSE  GETDATE()	END  END)) DiasTranscurrido
 				, RE.NroRequerimiento		
		
			FROM   Requerimiento RE
			LEFT JOIN RequerimientoMovimiento RMR ON RE.CodRequerimiento = RMR.CodRequerimiento AND RMR.Estado = 1
			LEFT JOIN RequerimientoMovimiento RMA ON RE.CodRequerimiento = RMA.CodRequerimiento AND RMA.Estado = 3
			LEFT JOIN RequerimientoMovimiento RMG ON RE.CodRequerimiento = RMG.CodRequerimiento AND RMG.Estado = 4
			LEFT JOIN RequerimientoMovimiento RMD ON RE.CodRequerimiento = RMD.CodRequerimiento AND RMD.Estado = 5
			LEFT JOIN RequerimientoMovimiento RMN ON RE.CodRequerimiento = RMN.CodRequerimiento AND RMN.Estado = 7
			LEFT JOIN RequerimientoMovimiento RMAT ON RE.CodRequerimiento = RMAT.CodRequerimiento AND RMAT.Estado = 8
			LEFT JOIN RequerimientoAsignacion RA ON RE.CodRequerimiento = RA.CodRequerimiento 
			LEFT JOIN RequerimientoMovimiento RMANULAR ON RE.CodRequerimiento = RMANULAR.CodRequerimiento AND RMANULAR.Estado = 2
			INNER JOIN Parametro PE ON PE.Grupo = 100 AND PE.Parametro = RE.Estado
			INNER JOIN Parametro PT ON PT.Grupo = 900 AND PT.Parametro = RE.CodOrigen
			LEFT JOIN Usuario U ON U.UsuarioWeb = RA.UsuarioAsignado
			WHERE 
			--((@FechaRegistroInicio IS NULL AND @FechaRegistroFin IS NULL) OR (CAST(RMR.FechaRegistro AS DATE) BETWEEN @FechaRegistroInicio AND @FechaRegistroFin )) AND
			((@FechaRegistroInicio IS NULL OR @FechaRegistroInicio = '') OR CAST(RE.FechaRegistro AS DATE) >= CAST(@FechaRegistroInicio AS DATE)) AND
			((@FechaRegistroFin IS NULL OR @FechaRegistroFin = '') OR CAST(RE.FechaRegistro AS DATE) <= CAST(@FechaRegistroFin AS DATE))  AND
			((@FechaAsignacionInicio IS NULL AND @FechaAsignacionFin IS NULL) OR (CAST(RMA.FechaRegistro AS DATE) BETWEEN @FechaAsignacionInicio AND @FechaAsignacionFin)) AND
			((@FechaGestionInicio IS NULL AND @FechaGestionFin IS NULL) OR  (CAST(RMG.FechaRegistro AS DATE) BETWEEN @FechaGestionInicio AND @FechaGestionFin)) AND
 			((@FechaAtencionInicio IS NULL AND @FechaAtencionFin IS NULL) OR  (CAST(RMAT.FechaRegistro AS DATE) BETWEEN @FechaAtencionInicio AND @FechaAtencionFin)) AND
			--(@TipoRequeriminto IS NULL OR RE.CodOrigen = @TipoRequeriminto) AND
			(@TipoRequeriminto IS NULL OR RE.CodTipoRequerimiento = @TipoRequeriminto) AND
 			(@Estado IS NULL OR RE.Estado = @Estado)
			GROUP BY
				  RMR.FechaRegistro
				, RMA.FechaRegistro
				, RMG.FechaRegistro
				, RMD.FechaRegistro
				, RMN.FechaRegistro
				, RMAT.FechaRegistro
				, RE.Descripcion
				, PE.Descripcion
				, PT.Descripcion
				, RE.NroRequerimiento
				, RE.FechaRegistro
				, RE.Estado				
				, RMANULAR.FechaRegistro

	)TEMP
			GROUP BY			
				TEMP.FechaRegistro
			, TEMP.FechaAsignacion
			, TEMP.FechaGestion
			, TEMP.FechaDerivacionOCM
			, TEMP.FechaSinNotificacion
			, TEMP.FechaAtencion
			, TEMP.DetalleReclamo
			, TEMP.Estado
			, TEMP.Origen
			, TEMP.NroRequerimiento
			, TEMP.UsuarioAsignado
			, TEMP.DiasTranscurrido


END
GO

DROP PROCEDURE IF EXISTS dbo.SP_REQUERIMIENTO_OBTENER_CARTA_NO_ADEUDO
GO
 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 26/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: SP_REQUERIMIENTO_LISTAR
-- PARAMETROS				: @UsuarioAsignado, @Estado, @CodAgencia, @NroDocumento, @NombresApellidos
--							  @FechaRegistroIni, @FechaRegistroFin, @FechaIniAtencion, @FechaFinAtencion 
-- FUNCIONALIDAD			: Obtener la lista de requerimiento
--================================================================================================================================

CREATE PROCEDURE [dbo].SP_REQUERIMIENTO_OBTENER_CARTA_NO_ADEUDO
@TipoDocumento INT,
@NroDocumento VARCHAR(20)
AS
BEGIN

;WITH cte_data(CodRequerimiento, Codigo) AS
	(
			SELECT CodRequerimiento, MAX(CodRequerimientoAsignacion) AS Codigo
			FROM  RequerimientoAsignacion RA			
			GROUP BY CodRequerimiento
)
SELECT	RE.CodRequerimiento
		,RE.NroRequerimiento As NroReclamo
		,RE.FechaRegistro
		,(PR.Descripcion + ' - ' + RD.NroDocumento) AS NroDocumento
		,CASE WHEN RD.TipoDocumento <> 2 THEN (RD.Nombres + ' ' + RD.PrimerApellido + ' ' + RD.SegundoApellido) ELSE RD.RazonSocial END  AS NombreApellidos
		,URA.NombresApellidos As UsuarioAsignado
		,MAX(CASE WHEN RM.Estado = 8 THEN CONVERT(VARCHAR,RM.FechaRegistro,103) ELSE NULL END) AS FechaAtencion		
		,RE.CodTipoRequerimiento
		,PRT.Descripcion AS TipoRequerimiento
		,AG.Oficina AS Agencia
		,PRE.Descripcion AS Estado
	FROM Requerimiento RE		
		INNER JOIN RequerimientoDetalle RD ON RD.CodRequerimiento = RE.CodRequerimiento		
		INNER JOIN Agencia AG  ON AG.CodAgencia = RE.CodAgencia 
		LEFT JOIN Parametro PR ON PR.Parametro = RD.TipoDocumento AND PR.Grupo = 300
		LEFT JOIN Parametro PRE ON PRE.Parametro = RE.Estado AND PRE.Grupo = 100
		LEFT JOIN Parametro PRT ON PRT.Parametro = RE.CodTipoRequerimiento AND PRT.Grupo = 600		
		LEFT JOIN cte_data cte  ON cte.CodRequerimiento = RE.CodRequerimiento 
		LEFT JOIN RequerimientoAsignacion RA  ON RA.CodRequerimiento = RE.CodRequerimiento AND RA.CodRequerimientoAsignacion = cte.Codigo
		LEFT JOIN Usuario URA ON URA.UsuarioWeb = RA.UsuarioAsignado
		LEFT JOIN RequerimientoMovimiento RM ON RM.CodRequerimiento = RE.CodRequerimiento AND RM.Estado = 8 --Estado Atendido
		--INNER JOIN Usuario U
		--ON RA.UsuarioAsignado = U.UsuarioWeb
	WHERE
		  ((@TipoDocumento IS NULL OR @TipoDocumento = 0) OR RD.TipoDocumento = @TipoDocumento) AND
		  ((@NroDocumento IS NULL OR @NroDocumento = '') OR RD.NroDocumento LIKE CONCAT('%', @NroDocumento, '%')) AND
		  RE.CodTipoRequerimiento = 1
		 
	GROUP BY RE.NroRequerimiento 
		,RE.CodRequerimiento
		,RE.FechaRegistro 
		,PR.Descripcion
		,RD.NroDocumento
		,RD.TipoDocumento
		,RD.Nombres 
		,RD.RazonSocial
		,RD.PrimerApellido
		,RD.SegundoApellido
		,URA.NombresApellidos		
		,RE.CodTipoRequerimiento
		,PRT.Descripcion
		,AG.Oficina
		,PRE.Descripcion
	ORDER BY RE.FechaRegistro ASC, RE.NroRequerimiento

END
GO

--EXEC SP_REQUERIMIENTO_OBTENER_CARTA_NO_ADEUDO 1,'74185299'

DROP PROCEDURE IF EXISTS dbo.SP_PREGUNTAS_FRECUENTES
GO
 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 28/06/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: SP_PREGUNTAS_FRECUENTES
-- PARAMETROS				:
-- FUNCIONALIDAD			: 
--================================================================================================================================

CREATE PROCEDURE dbo.SP_PREGUNTAS_FRECUENTES
@Grupo VARCHAR(50)

AS
BEGIN

	SELECT Parametro
		, Descripcion	
		,REPLACE(Valor,'[valor_referencia]', Referencia ) AS Valor	
		, Referencia
	FROM Parametro 
	WHERE Grupo = @Grupo

END
GO

--EXEC SP_PREGUNTAS_FRECUENTES '1300'

DROP PROCEDURE IF EXISTS dbo.SP_USUARIO_MANTENIMIENTO_OBTENER
GO
 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 7/07/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: SP_USUARIO_MANTENIMIENTO_OBTENER
-- PARAMETROS				:
-- FUNCIONALIDAD			: 
--================================================================================================================================

CREATE PROCEDURE dbo.SP_USUARIO_MANTENIMIENTO_OBTENER
@Perfil VARCHAR(5),
@NombresApellidos VARCHAR(200),
@UsuarioWeb VARCHAR(50)
AS
BEGIN

	SELECT CodUsuario
		, US.Perfil
		, US.DescripcionPerfil
		, US.CorreoElectronico
		, US.UsuarioWeb
		, US.NombresApellidos		
		,(CASE WHEN US.Estado = 1 THEN 'Activo' ELSE 'Inactivo' END) AS Estado
		, US.UsuarioRegistro
		, US.FechaRegistro
	FROM Usuario US	
	WHERE
	((@Perfil IS NULL OR @Perfil = '') OR US.Perfil LIKE CONCAT('%', @Perfil, '%'))
	AND ((@NombresApellidos IS NULL OR @NombresApellidos = '') OR US.NombresApellidos LIKE CONCAT('%', @NombresApellidos, '%'))
	AND ((@UsuarioWeb IS NULL OR @UsuarioWeb = '') OR US.UsuarioWeb LIKE CONCAT('%', @UsuarioWeb, '%'))

END
GO

----EXEC SP_USUARIO_MANTENIMIENTO_OBTENER '' , '', ''

DROP PROCEDURE IF EXISTS dbo.SP_USUARIO_REGISTRAR
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 08/07/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: William Quiroz Salazar
-- STORE PROCEDURE			: SP_USUARIO_REGISTRAR
-- PARAMETOS				: @Perfil VARCHAR(5) ,@DescripcionPerfil VARCHAR(200),@CorreoElectronico VARCHAR(100),
--							: @UsuarioWeb  VARCHAR(50),@NombresApellidos VARCHAR(200),@Estado INT,
--							: @UsuarioRegistro VARCHAR(50),@MensajeOutput VARCHAR(200) OUTPUT
-- FUNCIONALIDAD			:  
--================================================================================================================================

CREATE PROCEDURE dbo.SP_USUARIO_REGISTRAR
@Perfil VARCHAR(5) ,
@DescripcionPerfil VARCHAR(200),
@CorreoElectronico VARCHAR(100),
@UsuarioWeb  VARCHAR(50),
@NombresApellidos VARCHAR(200),
@UsuarioRegistro VARCHAR(50),
@MensajeOutput VARCHAR(100) OUTPUT

AS
BEGIN
	 DECLARE @Mensaje VARCHAR(100)= ''	
	 DECLARE @ExisteUsuarioWeb INT = 0
	 DECLARE @ExistePerfilOCM INT = 0
	 DECLARE @PerfilOCM VARCHAR(5)= 'OCM'
	 DECLARE @UsuarioActivo INT = 0

	 SET @ExisteUsuarioWeb = (SELECT COUNT(UsuarioWeb) FROM Usuario WHERE UsuarioWeb = @UsuarioWeb)

	 SET @UsuarioActivo = (SELECT COUNT(Estado) FROM Usuario WHERE Perfil = @Perfil)

	 IF @PerfilOCM = @Perfil
	 BEGIN
		SET @ExistePerfilOCM = (SELECT COUNT(Perfil) FROM Usuario WHERE Perfil = @Perfil AND Estado = 1)
	 END

	 IF @ExistePerfilOCM > 0
	 BEGIN
		SET @Mensaje = 'Ya existe un usuario con perfil OCM en estado Activo'
	 END
	 
	 IF @ExisteUsuarioWeb > 0
	 BEGIN
		SET @Mensaje = 'El usuario web ya existe'
	 END

	
	 IF @ExistePerfilOCM = 0 AND @ExisteUsuarioWeb = 0 OR @PerfilOCM = 'ACM'
	 BEGIN
		
		INSERT INTO Usuario(Perfil
			   ,DescripcionPerfil
			   ,CorreoElectronico        
			   ,UsuarioWeb
			   ,NombresApellidos
			   ,Estado
			   ,UsuarioRegistro         
			   ,FechaRegistro
			  )
		 VALUES (@Perfil
			   ,@DescripcionPerfil
			   ,@CorreoElectronico         
			   ,@UsuarioWeb
			   ,@NombresApellidos
			   ,1       
			   ,@UsuarioRegistro
			   , GETDATE())

		 SET @Mensaje = 'Se registró correctamente el usuario.'
	 END 		 
	  

   SET @MensajeOutput = @Mensaje  	

END
GO

-- EXEC SP_USUARIO_REGISTRAR 'ACM','','','AGRWQS','','', ''

DROP PROCEDURE IF EXISTS dbo.SP_USUARIO_OBTENER_DETALLE
GO
---=================================================================================================================================
-- FECHA DE CREACIÓN		: 08/07/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: SP_USUARIO_OBTENER_DETALLE
-- PARAMETOS				: @CodUsuario
-- FUNCIONALIDAD			: 
--==================================================================================================================================

CREATE PROCEDURE dbo.SP_USUARIO_OBTENER_DETALLE
@CodUsuario INT
AS
BEGIN

	SELECT US.CodUsuario
		  ,PRP.Parametro  AS CodPerfil	
		  ,US.DescripcionPerfil
		  ,US.CorreoElectronico
		  ,US.UsuarioWeb
		  ,US.NombresApellidos		  
		  ,(CASE WHEN US.Estado = 1 THEN '1' ELSE '0' END) AS Estado
		  ,US.UsuarioRegistro		 
	FROM   Usuario US	
	LEFT JOIN Parametro PRP ON PRP.Descripcion = US.Perfil AND PRP.Grupo = 1400
	WHERE	(ISNULL(@CodUsuario,'') = '' OR US.CodUsuario = @CodUsuario) 
END
GO

---EXEC SP_USUARIO_OBTENER_DETALLE '36'

DROP PROCEDURE IF EXISTS dbo.SP_USUARIO_ACTUALIZAR
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 08/07/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: William Quiroz Salazar
-- STORE PROCEDURE			: SP_USUARIO_ACTUALIZAR
-- PARAMETOS				: @Perfil VARCHAR(5) ,@DescripcionPerfil VARCHAR(200),@CorreoElectronico VARCHAR(100),
--							: @UsuarioWeb  VARCHAR(50),@NombresApellidos VARCHAR(200),@Estado INT,
--							: @UsuarioRegistro VARCHAR(50),@Estado INT,@MensajeOutput VARCHAR(200) OUTPUT
-- FUNCIONALIDAD			:  
--================================================================================================================================

CREATE PROCEDURE dbo.SP_USUARIO_ACTUALIZAR
@CodUsuario INT,
@Perfil VARCHAR(5) ,
@DescripcionPerfil VARCHAR(200),
@CorreoElectronico VARCHAR(100),
@UsuarioWeb  VARCHAR(50),
@NombresApellidos VARCHAR(200),
@UsuarioRegistro VARCHAR(50),
@Estado INT,
@MensajeOutput VARCHAR(100) OUTPUT

AS
BEGIN
	 DECLARE @Mensaje VARCHAR(100)= ''		
	 DECLARE @ExistePerfilOCM INT = 0
	 DECLARE @ExistePerfilACM INT = 0
	 DECLARE @PerfilOCM VARCHAR(5)= 'OCM'
	 DECLARE @PerfilACM VARCHAR(5)= 'ACM'
	 DECLARE @EstadoActual INT = 0
	 DECLARE @ExisteUsuarioWeb INT = 0

	SET @ExisteUsuarioWeb = (SELECT COUNT(UsuarioWeb) FROM Usuario WHERE UsuarioWeb = @UsuarioWeb AND CodUsuario <> @CodUsuario )


	 SET @EstadoActual = (SELECT Estado FROM Usuario WHERE Perfil = @Perfil AND CodUsuario=@CodUsuario)	 
	 	 
	 IF @PerfilOCM = @Perfil
	 BEGIN
		SET @ExistePerfilOCM = (SELECT COUNT(Perfil) FROM Usuario WHERE Perfil = @Perfil AND Estado = 1)		
	 END
	 
	  IF @PerfilACM = @Perfil
	 BEGIN
		SET @ExistePerfilACM = (SELECT COUNT(Perfil) FROM Usuario WHERE Perfil = @Perfil AND CodUsuario=@CodUsuario )		
	 END	


	 IF @ExistePerfilOCM > 0 
	 BEGIN
		SET @Mensaje = 'Ya existe un usuario con perfil OCM en estado Activo'
	 END

	  IF @EstadoActual = 0 AND @ExistePerfilOCM = 1
	 BEGIN
		SET @Mensaje = 'Ya existe un usuario con perfil OCM en estado Activo'
	 END 

	 IF @ExisteUsuarioWeb > 0
	 BEGIN
		SET @Mensaje = 'El usuario web ya existe'
	 END

	 IF( @ExisteUsuarioWeb = 0 )
	 BEGIN
	 IF (@ExistePerfilOCM = 1 AND @EstadoActual = 1 ) OR @ExistePerfilACM = 1
	 BEGIN
		 
		 UPDATE Usuario
			SET Perfil = @Perfil
				,DescripcionPerfil = @DescripcionPerfil
				,CorreoElectronico = @CorreoElectronico
				,UsuarioWeb = @UsuarioWeb
				,NombresApellidos = @NombresApellidos
				,UsuarioModificacion = @UsuarioRegistro
				,FechaModificacion = GETDATE()
				,Estado = @Estado
		 WHERE CodUsuario = @CodUsuario				
		
		 SET @Mensaje = 'Se actualizó correctamente el usuario.'
	 END 
	 END
	  
   SET @MensajeOutput = @Mensaje  	

END
GO

DROP PROCEDURE IF EXISTS dbo.SP_PLANTILLA_OBTENER
GO 
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 30/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_PLANTILLA_OBTENER
-- PARAMETOS				: @CodTabla INT,
--							  @CodTipoPlantilla INT
-- FUNCIONALIDAD			:  
--================================================================================================================================

CREATE PROCEDURE dbo.SP_PLANTILLA_OBTENER	
@CodTabla INT,
@CodTipoPlantilla VARCHAR(50)
AS
BEGIN
    SELECT 
		R.CodTabla, 
		R.ReferenciaID, 
		R.LaserficheID,
		R.Descripcion,
		PP.Descripcion TipoPlantilla,
		P.CodPlantilla
	FROM Recurso R
		INNER JOIN Plantilla P ON P.CodPlantilla = R.ReferenciaID
		LEFT JOIN Parametro PP ON PP.Grupo = 1200 AND PP.Parametro = P.CodTipoPlantilla
	WHERE R.CodTabla = @CodTabla  AND 
	(@CodTipoPlantilla is null OR	P.CodTipoPlantilla in  (select value from STRING_SPLIT(@CodTipoPlantilla,','))) AND
	P.Estado = 1
	GROUP BY
		R.CodTabla, 
		R.ReferenciaID, 
		R.LaserficheID,
		R.Descripcion,
		PP.Descripcion,
		P.CodPlantilla
END
GO


DROP PROCEDURE IF EXISTS dbo.SP_PLANTILLA_REGISTRAR
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 30/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_PLANTILLA_REGISTRAR
-- PARAMETOS				: @CodTabla INT,
--							  @CodTipoPlantilla INT
-- FUNCIONALIDAD			:  
--================================================================================================================================

CREATE PROCEDURE dbo.SP_PLANTILLA_REGISTRAR	
@CodPlatilla BIGINT OUTPUT,
@CodTipoPlantilla INT,
@UsuarioRegistro VARCHAR(50)
AS
BEGIN
     INSERT INTO Plantilla (CodTipoPlantilla,Estado,UsuarioRegistro,FechaRegistro) VALUES 
						(@CodTipoPlantilla,1,@UsuarioRegistro,GETDATE())

	 SET @CodPlatilla = @@IDENTITY

END
GO


DROP PROCEDURE IF EXISTS dbo.SP_PLANTILLA_DESACTIVAR
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 30/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_PLANTILLA_DESACTIVAR
-- PARAMETOS				: @CodTabla INT,
--							  @CodTipoPlantilla INT
-- FUNCIONALIDAD			:  
--================================================================================================================================

CREATE PROCEDURE dbo.SP_PLANTILLA_DESACTIVAR	
@CodPlatilla BIGINT, 
@UsuarioModificacion VARCHAR(50)
AS
BEGIN
    UPDATE Plantilla SET
	Estado  = 0,
	UsuarioModificacion = @UsuarioModificacion,
	FechaModificacion = GETDATE()
	WHERE CodPlantilla = @CodPlatilla
END
GO

DROP PROCEDURE IF EXISTS [dbo].[SP_REQUERIMIENTO_OBTENER]
GO
 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 26/04/2021
-- FECHA DE MODIFICACIÓN	: 12/07/2021
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: SP_REQUERIMIENTO_OBTENER
-- PARAMETROS				: @UsuarioAsignado, @Estado, @CodAgencia, @NroDocumento, @NombresApellidos
--							  @FechaRegistroIni, @FechaRegistroFin, @FechaIniAtencion, @FechaFinAtencion 
-- FUNCIONALIDAD			: Obtener la lista de requerimiento
--================================================================================================================================

CREATE PROCEDURE [dbo].[SP_REQUERIMIENTO_OBTENER]
@UsuarioAsignado VARCHAR(50),
@Estado VARCHAR(10),
@CodAgencia VARCHAR(10),
@NroDocumento VARCHAR(20),
@NombresApellidos VARCHAR(100),
@FechaRegistroIni DATETIME,
@FechaRegistroFin DATETIME,
@FechaAtencionIni DATE,
@FechaAtencionFin DATE

AS
BEGIN

;WITH cte_data(CodRequerimiento, Codigo) AS
	(
			SELECT CodRequerimiento, MAX(CodRequerimientoAsignacion) AS Codigo
			FROM  RequerimientoAsignacion RA			
			GROUP BY CodRequerimiento
)
SELECT	RE.CodRequerimiento
		,RE.NroRequerimiento As NroReclamo
		,RE.FechaRegistro
		,(PR.Descripcion + ' - ' + RD.NroDocumento) AS NroDocumento
		,CASE WHEN RD.TipoDocumento <> 2 THEN (RD.Nombres + ' ' + RD.PrimerApellido + ' ' + RD.SegundoApellido) ELSE RD.RazonSocial END  AS NombreApellidos
		,URA.NombresApellidos As UsuarioAsignado
		,MAX(CASE WHEN RM.Estado = 8 THEN CONVERT(VARCHAR,RM.FechaRegistro,103) ELSE NULL END) AS FechaAtencion		
		,RE.CodTipoRequerimiento
		,PRT.Descripcion AS TipoRequerimiento
		,AG.Oficina AS Agencia
		,PRE.Descripcion AS Estado
		,(CASE RE.Estado 
		WHEN 9 THEN 0 --PENDIENTE
		WHEN 10 THEN 1 -- SIN ATENCIÓN
		WHEN 1 THEN 2 -- REGISTRADO		
		WHEN 2 THEN 9 -- ANULADO
		ELSE RE.Estado END /* ASIGNADO, EN PROCESO, DERIVADO OCM, APROBADO, SIN NOTIFICAR Y ATENDIDO */) AS OrdenEstado --Columna 12 para ordenar
	FROM Requerimiento RE		
		INNER JOIN RequerimientoDetalle RD ON RD.CodRequerimiento = RE.CodRequerimiento		
		INNER JOIN Agencia AG  ON AG.CodAgencia = RE.CodAgencia 
		LEFT JOIN Parametro PR ON PR.Parametro = RD.TipoDocumento AND PR.Grupo = 300
		LEFT JOIN Parametro PRE ON PRE.Parametro = RE.Estado AND PRE.Grupo = 100
		LEFT JOIN Parametro PRT ON PRT.Parametro = RE.CodTipoRequerimiento AND PRT.Grupo = 600		
		LEFT JOIN cte_data cte  ON cte.CodRequerimiento = RE.CodRequerimiento 
		LEFT JOIN RequerimientoAsignacion RA  ON RA.CodRequerimiento = RE.CodRequerimiento AND RA.CodRequerimientoAsignacion = cte.Codigo
		LEFT JOIN Usuario URA ON URA.UsuarioWeb = RA.UsuarioAsignado
		LEFT JOIN RequerimientoMovimiento RM ON RM.CodRequerimiento = RE.CodRequerimiento AND RM.Estado = 8 --Estado Atendido
		--INNER JOIN Usuario U
		--ON RA.UsuarioAsignado = U.UsuarioWeb
	WHERE
		((@UsuarioAsignado IS NULL OR @UsuarioAsignado = '') OR RA.UsuarioAsignado =  @UsuarioAsignado)		
		AND ((@Estado IS NULL OR @Estado = '') OR RE.Estado = @Estado)
		AND ((@CodAgencia IS NULL OR @CodAgencia = '') OR RE.CodAgencia = CAST(@CodAgencia AS INT))
		AND ((@NroDocumento IS NULL OR @NroDocumento = '') OR RD.NroDocumento LIKE CONCAT('%', @NroDocumento, '%'))
		AND ((@NombresApellidos IS NULL OR @NombresApellidos = '') OR RD.NombresApellidos LIKE CONCAT('%', @NombresApellidos, '%'))
		AND ((@FechaRegistroIni IS NULL OR @FechaRegistroIni = '') OR CAST(RE.FechaRegistro AS DATE) >= CAST(@FechaRegistroIni AS DATE))
		AND ((@FechaRegistroFin IS NULL OR @FechaRegistroFin = '') OR CAST(RE.FechaRegistro AS DATE) <= CAST(@FechaRegistroFin AS DATE))
		AND ((@FechaAtencionIni IS NULL OR @FechaAtencionIni = '') OR (RM.FechaRegistro IS NOT NULL AND CAST(RM.FechaRegistro AS DATE) >= CAST(@FechaAtencionIni AS DATE)))
		AND ((@FechaAtencionIni IS NULL OR @FechaAtencionIni = '') OR (RM.FechaRegistro IS NOT NULL AND CAST(RM.FechaRegistro AS DATE) <= CAST(@FechaAtencionIni AS DATE)))			
	GROUP BY RE.NroRequerimiento 
		,RE.CodRequerimiento
		,RE.FechaRegistro 
		,PR.Descripcion
		,RD.NroDocumento
		,RD.TipoDocumento
		,RD.Nombres 
		,RD.RazonSocial
		,RD.PrimerApellido
		,RD.SegundoApellido
		,URA.NombresApellidos		
		,RE.CodTipoRequerimiento
		,PRT.Descripcion
		,AG.Oficina
		,PRE.Descripcion
		,RE.Estado
	ORDER BY CAST(RE.FechaRegistro AS DATE) ASC
		, 12 ASC	
		,RE.NroRequerimiento ASC

END
GO

DROP PROCEDURE IF EXISTS [dbo].[SP_USUARIO_OBTENER]
GO
---=================================================================================================================================
-- FECHA DE CREACIÓN		: 26/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: [dbo].[SP_USUARIO_OBTENER]
-- PARAMETROS				:							  
-- FUNCIONALIDAD			: Obtener la lista de usuarios
--================================================================================================================================

CREATE PROCEDURE [dbo].[SP_USUARIO_OBTENER]	
@Perfil VARCHAR(5),
@UsuarioWeb  VARCHAR(20),
@Estado BIT = NULL
AS
BEGIN

	SELECT US.CodUsuario
		,US.UsuarioWeb
		,US.NombresApellidos
		,US.Perfil
		,US.CorreoElectronico
		,US.Estado
	FROM Usuario US
	WHERE  ((@Perfil IS NULL OR @Perfil = '') OR (@Perfil IS NOT NULL AND US.Perfil = @Perfil))
		AND ((@UsuarioWeb IS NULL OR @UsuarioWeb = '') OR (@UsuarioWeb IS NOT NULL AND US.UsuarioWeb = @UsuarioWeb))
		AND ((@Estado IS NULL) OR (@Estado IS NOT NULL AND US.Estado = @Estado))
	ORDER BY US.CodUsuario

END
GO

DROP PROCEDURE IF EXISTS [dbo].[SP_CONSULTA_OBTENER]
GO
 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 3/06/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: SP_CONSULTA_OBTENER
-- PARAMETROS				: @FechaRegistroIni, @FechaRegistroFin
-- FUNCIONALIDAD			: Obtener la lista de consultas
--================================================================================================================================

CREATE PROCEDURE [dbo].[SP_CONSULTA_OBTENER]
@FechaRegistroIni DATETIME,
@FechaRegistroFin DATETIME
AS
BEGIN

SELECT	C.CodConsulta
		,C.FechaRegistro
		,C.NroDocumento
		,C.NombresCompletos
		,C.CorreoElectronico
		,C.Celular
		,P.Descripcion AS TipoDocumentoDescripcion
	FROM Consulta C			
	LEFT JOIN Parametro P ON C.TipoDocumento = P.Parametro AND P.Grupo = 300
	WHERE		
		((@FechaRegistroIni IS NULL OR @FechaRegistroIni = '') OR CAST(C.FechaRegistro AS DATE) >= CAST(@FechaRegistroIni AS DATE))
		AND ((@FechaRegistroFin IS NULL OR @FechaRegistroFin = '') OR CAST(C.FechaRegistro AS DATE) <= CAST(@FechaRegistroFin AS DATE))			
	GROUP BY C.CodConsulta
		,C.FechaRegistro
		,C.NroDocumento
		,C.NombresCompletos
		,C.CorreoElectronico
		,C.Celular
		,P.Descripcion
	ORDER BY C.CodConsulta DESC

END
GO

DROP PROCEDURE IF EXISTS [dbo].[SP_REQUERIMIENTO_OBTENER_DETALLE]
GO
---=================================================================================================================================
-- FECHA DE CREACIÓN		: 30/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: SP_REQUERIMIENTO_OBTENER_DETALLE
-- PARAMETOS				: @CodRequerimiento
-- FUNCIONALIDAD			: 
--==================================================================================================================================

CREATE PROCEDURE [dbo].[SP_REQUERIMIENTO_OBTENER_DETALLE]
@NroReclamo VARCHAR(20)
AS
BEGIN

Declare @CodRequerimiento BIGINT
SELECT @CodRequerimiento = CodRequerimiento FROM Requerimiento WHERE  NroRequerimiento= @NroReclamo

CREATE TABLE #TEstadoAnterior (Estado INT)
INSERT INTO #TEstadoAnterior
EXEC SP_REQUERIMIENTO_MOVIMIENTO_PENULTIMO @CodRequerimiento;
	
;WITH cte_data(CodRequerimiento, Codigo) AS
	(
			SELECT CodRequerimiento, MAX(CodRequerimientoAsignacion) AS Codigo
			FROM  RequerimientoAsignacion RA			
			GROUP BY CodRequerimiento
)

	SELECT RE.CodAgencia
		  ,AG.Oficina
		  ,AG.Direccion AS DireccionAgencia	
		  , RE.EsMenor
		  ,RM.CodRequerimiento
		  ,RE.NroRequerimiento		 
		  ,MAX(RM.CodRequerimientoMovimiento) AS CodRequerimientoMovimiento	
		  ,MAX(RA.CodRequerimientoAsignacion) AS CodRequerimientoAsignacion
		  ,RD.TipoDocumento
		  ,PTI.Descripcion TipoDocumentoDescripcion
		  ,RD.NroDocumento 
		  ,RD.Nombres
		  ,RD.PrimerApellido
		  ,RD.SegundoApellido
		  ,RD.NombresApellidos
		  ,RD.RazonSocial
		  ,RD.CodUbigeo		
		  ,RD.Direccion
		  ,RD.Referencia
		  ,RD.CorreoElectronico
		  ,RD.Telefono
		  ,RD.Celular
		  ,RE.CodMotivo
		  ,PRM.Descripcion AS Motivo
		  ,PRT.Descripcion AS TipoReclamo
		  ,RE.Descripcion AS DecripcionReclamo
		  ,RE.CodTipoRequerimiento
		  ,RE.CodProducto
		  ,PRP.Descripcion AS Producto
		  ,RM.UsuarioRegistro
		  ,UB.CodDepartamento
		  ,UB.Departamento
		  ,UB.CodProvincia
		  ,UB.Provincia
		  ,UB.CodDistrito
		  ,UB.Distrito
		  ,RE.Estado
		  ,PRE.Descripcion As DescripcionEstado
		  ,PTR.Descripcion As FormaEnvio
		  ,PRO.Descripcion As Origen		
		  ,RE.FechaRegistro
		  ,URA.NombresApellidos UsuarioAsignado
		  ,(SELECT Estado FROM #TEstadoAnterior) AS EstadoAnterior
		  ,RA.UsuarioAsignado AS UsuarioWebAsignado
	FROM   Requerimiento RE
		INNER JOIN RequerimientoDetalle RD ON RD.CodRequerimiento = RE.CodRequerimiento		
		INNER JOIN RequerimientoMovimiento RM ON RM.CodRequerimiento = RE.CodRequerimiento
		LEFT JOIN Agencia AG ON AG.CodAgencia = RE.CodAgencia
		LEFT JOIN Parametro PRE ON PRE.Parametro = RE.Estado AND PRE.Grupo = 100
		LEFT JOIN Parametro PRP ON PRP.Parametro = RE.CodProducto AND PRP.Grupo = 400
		LEFT JOIN Parametro PRM ON PRM.Parametro = RE.CodMotivo AND PRM.Grupo = 500
		LEFT JOIN Parametro PRT ON PRT.Parametro = RE.CodTipoRequerimiento AND PRT.Grupo = 600	
		LEFT JOIN Parametro PTI ON PTI.Parametro = RD.TipoDocumento AND PTI.Grupo = 300	
		LEFT JOIN Parametro PTR ON PTR.Parametro = RE.CodFormaEnvio AND PTR.Grupo = 700	
		LEFT JOIN Parametro PRO ON PRO.Parametro = RE.CodOrigen AND PRO.Grupo = 900
		LEFT JOIN UBIGEO UB ON UB.CodUbigeo = RD.CodUbigeo	
		LEFT JOIN cte_data cte ON cte.CodRequerimiento = RE.CodRequerimiento 
		LEFT JOIN RequerimientoAsignacion RA 	ON RA.CodRequerimiento = RE.CodRequerimiento AND RA.CodRequerimientoAsignacion = cte.Codigo
		LEFT JOIN Usuario URA ON RA.UsuarioAsignado = URA.UsuarioWeb
	WHERE	(ISNULL(@NroReclamo,'') = '' OR RE.NroRequerimiento = @NroReclamo)   
	AND RE.Estado = RM.Estado	
	AND RM.CodRequerimientoMovimiento = (SELECT MAX(CodRequerimientoMovimiento) AS Codigo
										 FROM  RequerimientoMovimiento RM where RM.CodRequerimiento = RE.CodRequerimiento)
	
	GROUP BY
		   RE.CodAgencia
		  ,AG.Oficina
		  ,AG.Direccion 	
		  , RE.EsMenor
		  ,RM.CodRequerimiento
		  ,RE.NroRequerimiento		 
		  ,RM.CodRequerimientoMovimiento
		  ,RD.TipoDocumento
		  ,PTI.Descripcion
		  ,RD.NroDocumento
		  ,RD.Nombres
		  ,RD.PrimerApellido
		  ,RD.SegundoApellido
		  ,RD.NombresApellidos
		  ,RD.RazonSocial
		  ,RD.CodUbigeo		  
		  ,RD.Direccion
		  ,RD.Referencia
		  ,RD.CorreoElectronico
		  ,RD.Telefono
		  ,RD.Celular
		  ,RE.CodMotivo
		  ,PRM.Descripcion
		  ,PRT.Descripcion
		  ,RE.Descripcion
		  ,RE.CodProducto
		  ,PRP.Descripcion
		  ,RE.CodTipoRequerimiento
		  ,RM.UsuarioRegistro
		  ,UB.CodDepartamento
		  ,UB.Departamento
		  ,UB.CodProvincia
		  ,UB.Provincia
		  ,UB.CodDistrito
		  ,UB.Distrito
		  ,RE.Estado
		  ,PRE.Descripcion
		  ,PRP.Descripcion
		  ,PTR.Descripcion
		  ,PRO.Descripcion
		  ,RE.FechaRegistro
		  ,RA.CodRequerimientoAsignacion
		  , URA.NombresApellidos
		  ,RA.UsuarioAsignado
END
GO