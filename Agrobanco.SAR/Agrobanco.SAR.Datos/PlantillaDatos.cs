﻿using Agrobanco.SAR.Datos.SqlServer.Acceso;
using Agrobanco.SAR.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Agrobanco.SAR.Datos
{
    public class PlantillaDatos
    {
        public string cadenaConexion;
        public PlantillaDatos()
        {

        }
        public List<PlantillaBE> PlantillaObtener(PlantillaBE oBE)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            PlantillaBE oPlantillaBE = null;
            List<PlantillaBE> LstPlantillaBE = new List<PlantillaBE>();

            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("SP_PLANTILLA_OBTENER", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@codTabla", SqlDbType.Int).Value = oBE.CodTabla;

                        if (oBE.CodTipoPlantilla == null)
                            cmd.Parameters.Add("@CodTipoPlantilla", SqlDbType.Int).Value = DBNull.Value;
                        else
                            cmd.Parameters.Add("@CodTipoPlantilla", SqlDbType.VarChar,50).Value = oBE.CodTipoPlantilla;

                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    oPlantillaBE = new PlantillaBE();

                                    if (!reader.IsDBNull(0))
                                        oPlantillaBE.CodTabla = reader.GetInt32(0);

                                    if (!reader.IsDBNull(1))
                                        oPlantillaBE.ReferenciaID = reader.GetInt64(1);

                                    if (!reader.IsDBNull(2))
                                        oPlantillaBE.LaserficheID = reader.GetInt32(2);

                                    if (!reader.IsDBNull(3))
                                        oPlantillaBE.Descripcion = reader.GetString(3);

                                    if (!reader.IsDBNull(4))
                                        oPlantillaBE.TipoPlantilla = reader.GetString(4);

                                    if (!reader.IsDBNull(5))
                                        oPlantillaBE.CodPlantilla = reader.GetInt64(5);

                                    LstPlantillaBE.Add(oPlantillaBE);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw new Exception("Error al Obtener el codigo de Laserfiche");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception("Error al Obtener el codigo de Laserfiche");
            }

            return LstPlantillaBE;
        }


        public Int64 Registrar(PlantillaBE oBE)
        {
            Int64 CodPlantilla = 0;
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            using (SqlConnection cnn = new SqlConnection(cadenaConexion))
            {
                cnn.Open();
                using (SqlCommand cmd = new SqlCommand("SP_PLANTILLA_REGISTRAR", cnn))
                {
                    try
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        SqlParameter pCodPlantilla = new SqlParameter("@CodPlatilla", SqlDbType.BigInt);
                        pCodPlantilla.Direction = System.Data.ParameterDirection.Output;
                        pCodPlantilla.Value = 0;
                        cmd.Parameters.Add(pCodPlantilla);

                        cmd.Parameters.Add("@CodTipoPlantilla", SqlDbType.Int).Value = oBE.CodTipoPlantilla;
                        cmd.Parameters.Add("@UsuarioRegistro", SqlDbType.VarChar, 50).Value = oBE.UsuarioRegistro;

                        cmd.ExecuteNonQuery();

                        CodPlantilla = Convert.ToInt64(pCodPlantilla.Value);
                        

                        return CodPlantilla;
                    }

                    catch (Exception ex)
                    {
                        cnn.Close();
                        cnn.Dispose();
                        cmd.Dispose();
                        throw (ex);
                    }
                    finally
                    {
                        cnn.Close();
                        cnn.Dispose();
                        cmd.Dispose();
                    }

                }

            }

            return CodPlantilla;
        }


        public bool Desactivar(PlantillaBE oBE)
        {
            bool resultado = false;
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            using (SqlConnection cnn = new SqlConnection(cadenaConexion))
            {
                cnn.Open();
                using (SqlCommand cmd = new SqlCommand("SP_PLANTILLA_DESACTIVAR", cnn))
                {
                    try
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@CodPlatilla", SqlDbType.BigInt).Value = oBE.CodPlantilla;
                        cmd.Parameters.Add("@UsuarioModificacion", SqlDbType.VarChar, 50).Value = oBE.UsuarioModificacion;

                        cmd.ExecuteNonQuery();
                        resultado = true;
                        return resultado;
                    }

                    catch (Exception ex)
                    {
                        cnn.Close();
                        cnn.Dispose();
                        cmd.Dispose();
                        throw (ex);
                    }
                    finally
                    {
                        cnn.Close();
                        cnn.Dispose();
                        cmd.Dispose();
                    }

                }

            }

            return resultado;
        }
    }
}
