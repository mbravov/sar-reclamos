﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.Reclamaciones.API.Models
{
    public class ComentarioBE
    {
        public Int64? CodComentario { get; set; }
        public Int64? CodRequerimiento { get; set; }
        public Int64? CodRequerimientoMovimiento { get; set; }
        public int? CodMotivo { get; set; }
        public int? CodArea { get; set; }
        public string Area { get; set; }
        public string Descripcion { get; set; }
        public DateTime? FechaSolicitud { get; set; }
        public string UsuarioRegistro { get; set; }
        public string UsuarioModificacion { get; set; }
        public DateTime? FechaRegistro { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public int? Estado { get; set; }
        public int? CodTabla { get; set; }
        public string LaserficheID { get; set; }
        public string NroRequerimiento { get; set; }
        public string Archivo { get; set; }
        public string NombreArchivo { get; set; }
        public string NombresApellidos { get; set; }
        public string Motivo { get; set; }
    }
}
