﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades.CNA_Minuta
{
    public class ComentarioEmisionBE
    {
        public int IdComentarioEmision { get; set; }
        public int IdEmision { get; set; }
        public int? IdDerivacion { get; set; }
        public int? IdDerivacionMovimiento { get; set; }
        public string Texto { get; set; }
        public string UsuarioRegistro { get; set; }
        public string Archivo { get; set; }
        public string NombreArchivo { get; set; }
        public int CodTabla { get; set; }
        public string DNI { get; set; }

        public string LaserficheID { get; set; }
        public DateTime? FechaRegistro { get; set; }

        public string Perfil { get; set; }
        public int CodAgencia { get; set; }

        public int Estado { get; set; }
        public string IdMotivo { get; set; }
        public string Motivo { get; set; }
    }
}
