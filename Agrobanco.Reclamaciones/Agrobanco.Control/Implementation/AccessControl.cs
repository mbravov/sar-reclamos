﻿using Agrobanco.Control.Base;
using Agrobanco.Control.DTO;
using Agrobanco.Control.Interface;
using Agrobanco.Control.Model;
using Agrobanco.Control.Security;
using System;
using System.Collections.Generic;
using System.Text;
using static Agrobanco.Control.Util.Constants;

namespace Agrobanco.Control.Implementation
{
    public class AccessControl : BaseControl, IAccessControl
    {
        private readonly ITokenControl _tokencontrol;

        public string TokenSesion { get; set; }

        public AccessControl()
        {
            _tokencontrol = new TokenControl();
        }

        public AccessDTO generateToken(AccessDTO accessdto, string appkey, string appcode, ref string token)
        {
            try
            {

                if (string.IsNullOrEmpty(accessdto.CorreoElectronico))
                {
                    var mensaje = string.Format("{0}|{1}", ConstantesError.ERROR_VALOR_NULO_CODIGO, string.Format("{0}", "Correo electronico no puede ser nulo o vacío."));
                    throw new Exception(mensaje);
                }

                if (string.IsNullOrEmpty(accessdto.NombreUsuario))
                {
                    var mensaje = string.Format("{0}|{1}", ConstantesError.ERROR_VALOR_NULO_CODIGO, string.Format("{0}", "Nombre de usuario no puede ser nulo o vacío."));
                    throw new Exception(mensaje);
                }

                if (string.IsNullOrEmpty(accessdto.CodigoUsuario))
                {
                    var mensaje = string.Format("{0}|{1}", ConstantesError.ERROR_VALOR_NULO_CODIGO, string.Format("{0}", "Codigo de usuario no puede ser nulo o vacío."));
                    throw new Exception(mensaje);
                }

                if (string.IsNullOrEmpty(appkey))
                {
                    var mensaje = string.Format("{0}|{1}", ConstantesError.ERROR_TOKEN_VALOR_INCORRECTO, string.Format("{0}", "Parametro de validación incorrecto."));
                    throw new Exception(mensaje);
                }

                if (string.IsNullOrEmpty(appcode))
                {
                    var mensaje = string.Format("{0}|{1}", ConstantesError.ERROR_TOKEN_VALOR_INCORRECTO, string.Format("{0}", "Parametro de validación incorrecto."));
                    throw new Exception(mensaje);
                }


                //VALIDACION DE APPCODE Y APPKEY

                var tokenresponse = GenerarTokenJWT(accessdto);

                token = tokenresponse.Token;
                accessdto.FechaInicioVigencia = tokenresponse.FechaInicioVigencia;
                accessdto.FechaFinVigencia = tokenresponse.FechaFinVigencia;

                return accessdto;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        #region private methods
        private ResponseContainerModel GenerarTokenJWT(AccessDTO oaccessdto)
        {
            var paramkeytoken = Util.Util.ObtenerValueFromKey(ConstantesParametros.TokenClave);
            var paramtokenminutes = Util.Util.ObtenerValueFromKey(ConstantesParametros.TokenMinutos);

            var dicttokenparam = new Dictionary<string, string>
            {
                { ConstantesToken.Key, paramkeytoken },
                { ConstantesToken.Minutes, paramtokenminutes }
            };

            var dictclaims = new Dictionary<string, string>
            {
                { ConstantesUsuario.CodigoUsuario, oaccessdto.CodigoUsuario.ToString() },
                { ConstantesUsuario.CorreoElectronico, oaccessdto.CorreoElectronico },
                { ConstantesUsuario.NombreUsuario, oaccessdto.NombreUsuario },
                { ConstantesUsuario.NumeroDocumento, oaccessdto.NumeroDocumento },
                { ConstantesGenerico.IdentificadorUnico, Guid.NewGuid().ToString() },
            };

            var responseToken = _tokencontrol.GenerateJwtToken(dicttokenparam, dictclaims);

            return responseToken;
        }

        #endregion

    }
}
