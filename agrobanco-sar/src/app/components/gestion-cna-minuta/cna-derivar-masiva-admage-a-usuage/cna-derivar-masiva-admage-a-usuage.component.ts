import { Constantes } from 'src/app/comun/constantes';

import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DialogService } from '../../../services/comun/dialog.service';
import { SecurityService } from '../../../services/security.service';
import { CnaMinutaOcmService } from '../../../services/cna-minuta-ocm.service';
import { FiltroAccionService } from '../../../services/filtro-accion.service';
import { ConfiguracionAccionService } from '../../../services/configuracion-accion.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RequerimientoService } from 'src/app/services/requerimiento.service';

@Component({
  selector: 'app-cna-derivar-masiva-admage-a-usuage',
  templateUrl: './cna-derivar-masiva-admage-a-usuage.component.html',
  styleUrls: ['./cna-derivar-masiva-admage-a-usuage.component.css']
})
export class CnaDerivarMasivaAdmageAUsuageComponent implements OnInit {

  form!: FormGroup;
  formSubmitted:boolean = false;

  lengthDescripcion: number  = 300;
  MaxlengthDescripcion: number  = 300;
  MinlengthDescripcion: number  = 5;



  loading:boolean = false;
  usuariosPorAgencia : any;
  Filter:any;
  constructor(
    private _formBuilder: FormBuilder,
    private _dialogService: DialogService,
    private _securityService: SecurityService,
    private _cnaMinutaOcmService : CnaMinutaOcmService,
    private _filtroAccionService : FiltroAccionService,
    private _configuracionAccion : ConfiguracionAccionService,
    private _requerimientoService : RequerimientoService,
    public dialogRef: MatDialogRef<CnaDerivarMasivaAdmageAUsuageComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.Filter = data;
    // this.emision = data.CreditoClienteDetalle;
    // this.IdConfiguracionAccion = data.IdConfiguracionAccion;
    // this.FechaEntregaOlva = data.FechaEntregaOlva;
  }

  ngOnInit(): void {
    this.inicializarFormulario();
    this.obtenerUsuarios();
  }


  inicializarFormulario() : void {

    this.form = this._formBuilder.group({
      Descripcion: [null,  [Validators.required]],
      CodUsuarioDerivado : ['', [Validators.required]]
    });


  }

  obtenerUsuarios(){
    var Perfil = {
      Perfil: Constantes.UDA.toUpperCase(),
      UsuarioWeb: '',
      Estado: true
    };

    this.loading = true;
    this._cnaMinutaOcmService.ObtenerUsuarios(Perfil).subscribe((data:any) => {
      this.usuariosPorAgencia = data.response.filter((x:any) => x.codAgencia == this.Filter.CodAgencia);
      console.log(this.usuariosPorAgencia);
      this.loading = false;
    },  (err)=>{
      this.loading = false;
      console.log(err);
    });
  }


  derivar(){

    this.formSubmitted = true;

    if (this.form.invalid) {
      return;
    }

    var request = {
      CodAgencia : this.Filter.CodAgencia,
      CodUsuarioDerivado : +this.form.controls['CodUsuarioDerivado'].value,
      CodEstado : this.Filter.CodEstado,
      NroDocumento: this.Filter.NroDocumento,
      FechaCancelacionIni : this.Filter.FechaCancelacionIni,
      FechaCancelacionFin: this.Filter.FechaCancelacionFin,
      UsuarioWeb : this.Filter.UsuarioWeb,
      Perfil : this.Filter.Perfil,
      IdConfiguracionAccion: Constantes.ConfiguracionAccion_Derivacion_Masiva_ADMAG_a_UsuAge,
      PerfilDerivado:"UDA",
      Texto: this.form.controls['Descripcion'].value,
      UsuarioRegistro: this._securityService.leerUsuarioWeb(),
      UsuarioModificacion: this._securityService.leerUsuarioWeb()
  };

    this._dialogService
    .openConfirmDialog('¿Está seguro de derivar de forma masiva los créditos al Usuario de Agencia?')
    .afterClosed()
    .subscribe((res) => {
      if (res) {
        this.loading = true;
          this._cnaMinutaOcmService.DerivarMasicoADMAGaUsuAge(request).subscribe((data:any)=>{
            this.loading = false;
            if(data.response.success.resultado){

              this._dialogService.openMsgSuccessDialog("se realizó correctamente la derivación masiva");
              this.dialogRef.close({ data: true });
            }
            else{
              this._dialogService.openMsgErrorDialog("No se han encontrado créditos que cumplan con los requisitos para la derivación masiva");
            }
          });
      }
    },(err)=>{
      this.loading = false;
      this._dialogService.openMsgErrorDialog("Hubo errores en la derivación masiva. Vuelva a intentarlo");
    });




  }



  eliminarArchivoLaserFiche(idLaserficheCNA:any = null) : void {

    const oBE = {
      "LaserficheID": ""+idLaserficheCNA
      };
      this.loading = true;
      this._requerimientoService.eliminarArchivo(oBE).subscribe(result=>{
        this.loading = false;
        console.log("eliminarArchivo",result);
      },
      (err)=>{
        this.loading = false;
        console.log(err);
      }
      );

   }
}
