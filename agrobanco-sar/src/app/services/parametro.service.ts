
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AppService } from '../app.service';
import { SecurityService } from './security.service';

const UrlBase_SARAPI = environment.UrlBase_SARAPI;

@Injectable({
  providedIn: 'root'
})
export class ParametroService extends AppService{

   constructor(securityService: SecurityService) {
    super(securityService);
  }

  ObtenerParametro<T>():Observable<any[]>{
    const datos = {
      "strGrupo" : '100,300,400,500,600,700,900,1100,1400'
    };

    //this.endpoint = `${UrlBase_SARAPI}/parametro/lista`;
    this.endpoint = `${UrlBase_SARAPI}/parametro/lista-grupo`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  ObtenerUsuarios<T>():Observable<any[]>{
    const datos = {
      "Perfil" : "",
      "UsuarioWeb" : ""
    };
    //console.log("input",datos);
    this.endpoint = `${UrlBase_SARAPI}/Usuario/listar`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  ObtenerAgencias<T>():Observable<any[]>{
   // this.endpoint = `${UrlBase_SARAPI}/Agencia/listar`;
    this.endpoint = `${UrlBase_SARAPI}/Agencia/listar`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint);
  }

  ObtenerAreas<T>():Observable<any[]>{
     this.endpoint = `${UrlBase_SARAPI}/Agencia/listararea`;
     console.log("endpoint",this.endpoint);
     return this.post(this.endpoint);
   }

   ObtenerParametroByGrupo<T>(strGrupo:string):Observable<any[]>{
    const datos = {
      "strGrupo" :strGrupo
    };
    this.endpoint = `${UrlBase_SARAPI}/parametro/lista-grupo`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  ObtenerAgenciasTodos<T>():Observable<any[]>{
    // this.endpoint = `${UrlBase_SARAPI}/Agencia/listar`;
     this.endpoint = `${UrlBase_SARAPI}/Agencia/listar-todos`;
     console.log("endpoint",this.endpoint);
     return this.post(this.endpoint);
   }
}


