import { Component, Inject, Input, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
@Component({
  selector: 'app-msg-success',
  templateUrl: './msg-sucess.component.html',
  styleUrls: ['./msg-sucess.component.css']
})
export class MsgSucessComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data : any,
    public dialogRef: MatDialogRef<MsgSucessComponent>
  ) { }

  ngOnInit(): void {
  }

}
