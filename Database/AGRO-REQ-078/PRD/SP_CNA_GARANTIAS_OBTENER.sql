USE DBAgrSAR
GO

DROP PROCEDURE IF EXISTS SP_CNA_GARANTIAS_OBTENER
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 15/12/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: CECILIA MARRUFO
-- STORE PROCEDURE			: SP_CNA_GARANTIAS_OBTENER
-- PARAMETOS				: @NumeroDocumento VARCHAR(20),
--							  @TipoDocumento VARCHAR(20) 
-- FUNCIONALIDAD			:  Obtiene lista de garantias
--================================================================================================================================

	CREATE PROC SP_CNA_GARANTIAS_OBTENER

	@NumeroDocumento VARCHAR(20) ,
	@TipoDocumento VARCHAR(20) 


	 AS

	 BEGIN
	  SELECT --ROCREF CODGARANTIA , 
	   --ROCTYP TIPOGARANTIA,
	   A.CNODSC DescripcionGarantia,
	   ROCNAM NombreGarante,
	   ROEID2 DNIGarante, 
	   --ROCAPA IMPORTEGRAVAMEN ,
	   CAST(format(ROCFAA,'N') as varchar) MontoGravamen ,
	   CAST(ROCRF3 AS varchar) NumeroPartida,
	   CAST(COALESCE(B.CNODSC,'')AS varchar) OficinaRegistral,
	   CAST(COALESCE(RCLACA,'0')AS varchar) NumeroPrestamo
		FROM  PROPBD425.DBAgrIBS.dbo.ROCOL INNER JOIN PROPBD425.DBAgrIBS.dbo.CUMST ON ROCCUN=CUSCUN 
			LEFT JOIN PROPBD425.DBAgrIBS.dbo.ROCOLCRO ON ROEREF=ROCREF
			LEFT JOIN PROPBD425.DBAgrIBS.dbo.CNOFC A ON A.CNOCFL='05' AND A.CNORCD = ROCTYP
			LEFT JOIN PROPBD425.DBAgrIBS.dbo.CNOFC B ON B.CNOCFL='2A' AND B.CNORCD = ROEORE
			LEFT JOIN PROPBD425.DBAgrIBS.dbo.RCOLL ON RCLACB = ROCREF
		WHERE CUSIDN=@NumeroDocumento AND CUSTID=@TipoDocumento


	 END