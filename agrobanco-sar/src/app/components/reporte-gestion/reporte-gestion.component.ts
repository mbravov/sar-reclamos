import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { RequerimientoSeguimiento } from '../models/RequerimientoSeguimiento.interface';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { ParametroService } from 'src/app/services/parametro.service';
import { ReporteService } from 'src/app/services/reporte.service';
import { ExcelJsService } from 'src/app/services/comun/excel-js.service';
import { forkJoin } from 'rxjs';
import * as moment from 'moment';

@Component({
  selector: 'app-reporte-gestion',
  templateUrl: './reporte-gestion.component.html',
  styleUrls: ['./reporte-gestion.component.css']
})
export class ReporteGestionComponent implements OnInit {

  busquedaFiltro! : FormGroup;

  parametros!:any[];


  loading!:boolean;
  ELEMENT_DATA! : RequerimientoSeguimiento[];
  displayedColumns: string[] = ['NroRequerimiento','FechaRegistro', 'FechaAsignacion', 'FechaGestion', 'FechaAtencion','Estado','TipoRequerimiento','UsuarioAsignado','DiasTranscurrido','accion'];
  //requerimientoSeguimiento!:any[];
  requerimientoSeguimiento = new MatTableDataSource<RequerimientoSeguimiento>(this.ELEMENT_DATA);
  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;

  fechaRegistroInicioValor! : Date;
  fechaRegistroFinValor! : Date;
  fechaAtencionInicioValor!: Date;
  fechaAtencionFinValor!: Date;
  mensaje!: boolean;

  constructor(
    private formBuilder: FormBuilder,
    //private router: Router,
    private parametroService: ParametroService,
    private reporteService:ReporteService,
    private excelJsService:ExcelJsService
  ) { }

  ngOnInit(): void {
    this.initFormGroup();
    this.loadPage();  

  }

  loadPage(): void {
   
     this.loading = true;
     forkJoin([
       this.parametroService.ObtenerParametro()
     ]).subscribe( (data:any) => {
 
         console.log(data);
 
         this.parametros = data[0].response;
 
 
       this.loading = false;
 
     }, (error) =>{
       console.log(error);
       this.loading = false;
     });
 
   }

   initFormGroup():void {

    let fechaInicioMes = new Date(new Date().getFullYear(), new Date().getMonth(), 1);
    let fechaHoy = new Date();

    this.fechaRegistroInicioValor = fechaInicioMes;
    this.fechaRegistroFinValor = fechaHoy;
    this.fechaAtencionInicioValor = fechaHoy;
    this.fechaAtencionFinValor = fechaHoy;

    this.busquedaFiltro = new FormGroup({

      FRegistroInicial: new FormControl(null, [Validators.max(this.fechaRegistroFinValor.getTime())]),
      FRegistroFin: new FormControl(null, [Validators.min(this.fechaRegistroInicioValor.getTime())]),
      FAsignacionInicio: new FormControl(null, [Validators.max(this.fechaAtencionFinValor.getTime())]),
      FAsignacionFin: new FormControl(null, [Validators.min(this.fechaAtencionInicioValor.getTime())]),

      FGestionInicio: new FormControl(null, [Validators.max(this.fechaRegistroFinValor.getTime())]),
      FGestionFin: new FormControl(null, [Validators.min(this.fechaRegistroInicioValor.getTime())]),
      FAtencionInicio: new FormControl(null, [Validators.max(this.fechaAtencionFinValor.getTime())]),
      FAtencionFin: new FormControl(null, [Validators.min(this.fechaAtencionInicioValor.getTime())]),
      TipoRequerimiento : new FormControl(0) ,
      Estado : new FormControl(0) ,
      Mensaje: new FormControl('')   
    });
  }

  ExportarExcel():void {

    this.loading = true;
    this.mensaje = false;

    var request = {
      FechaRegistroInicial : this.busquedaFiltro.controls["FRegistroInicial"].value,
      FechaRegistroFin : this.busquedaFiltro.controls["FRegistroFin"].value,
      FechaAsignacionInicial: this.busquedaFiltro.controls["FAsignacionInicio"].value,
      FechaAsignacionFin : this.busquedaFiltro.controls["FAsignacionFin"].value,
      FechaGestionInicial : this.busquedaFiltro.controls["FGestionInicio"].value,
      FechaGestionFin : this.busquedaFiltro.controls["FGestionFin"].value,
      FechaAtencionInicial : this.busquedaFiltro.controls["FAtencionInicio"].value,
      FechaAtencionFin: this.busquedaFiltro.controls["FAtencionFin"].value,
      TipoRequerimiento : +this.busquedaFiltro.controls["TipoRequerimiento"].value,
      Estado : +this.busquedaFiltro.controls["Estado"].value,
    };

    this.reporteService.ObtenerReporteGestion(request).subscribe(result=>{
        console.log(result);

        if(result.response.length > 0)
        {

        this.requerimientoSeguimiento.data = result.response as RequerimientoSeguimiento[];

        const FechaActual = new Date();
        const AnioActual = FechaActual.getFullYear();
        const MesActual = (FechaActual.getMonth()<=9? '0'+ (FechaActual.getMonth()+1): FechaActual.getMonth() + 1); 
        const DiaActual = (FechaActual.getDate()<=9? '0'+FechaActual.getDate(): FechaActual.getDate());
        const HoraActual = (FechaActual.getHours()<=9? '0'+FechaActual.getHours(): FechaActual.getHours()); 
        const MinutoActual = (FechaActual.getMinutes()<=9? '0'+FechaActual.getMinutes(): FechaActual.getMinutes()); 
        const SegundosActual = (FechaActual.getSeconds()<=9? '0'+FechaActual.getSeconds(): FechaActual.getSeconds()); 

        const Archivo = "ReporteGestionRequerimiento" + "_" + AnioActual + MesActual + DiaActual + "T" + HoraActual + MinutoActual + SegundosActual;
        const Sheet = "Hoja1";
        const header = ["NRO REQUERIMIENTO","FECHA REGISTRO","FECHA ASIGNACIÓN","FECHA DE INICIO DE GESTIÓN","Fecha de Derivación a OCM","Fecha de derivación para notificación","FECHA ATENCIÓN","Detalle del reclamo","ESTADO","ORIGEN","USUARIO ASIGNADO","DIAS TRANSCURRIDOS"];
        const header_width = [20,20,20,20,25,30,20,35,20,20,25,20];
        let body = [];
        for (let index = 0; index < result.response.length; index++) {
          const datos = result.response[index];
          var nroRequerimiento = (datos.nroRequerimiento==null?'': datos.nroRequerimiento);
          var fechaRegistro = (datos.fechaRegistro==null?'': moment(datos.fechaRegistro).format("DD/MM/YYYY"));
          var fechaAsignacion = (datos.fechaAsignacion==null?'': moment(datos.fechaAsignacion).format("DD/MM/YYYY")); 
          var fechaGestion = (datos.fechaGestion==null?'': moment(datos.fechaGestion).format("DD/MM/YYYY"));
          var fechaDerivacionOCM = (datos.fechaDerivacionOCM==null?'': moment(datos.fechaDerivacionOCM).format("DD/MM/YYYY")); 
          var fechaDerivacionNotificacion = (datos.fechaDerivacionNotificacion==null?'': moment(datos.fechaDerivacionNotificacion).format("DD/MM/YYYY")); 
          var fechaAtencion = (datos.fechaAtencion==null?'': moment(datos.fechaAtencion).format("DD/MM/YYYY")); 
          var detalleReclamo = (datos.detalleReclamo==null?'': datos.detalleReclamo);
          var estado = datos.estado;
          var origen = datos.origen;
          var usuarioAsignado = datos.usuarioAsignado;
          var diasTranscurrido = datos.diasTranscurrido;
          var datoarray = [nroRequerimiento,fechaRegistro,fechaAsignacion,fechaGestion,fechaDerivacionOCM,fechaDerivacionNotificacion,fechaAtencion,detalleReclamo,estado,origen,usuarioAsignado,diasTranscurrido]
          body.push(datoarray)
        }


        this.excelJsService.DescargarExcelGenerico(Archivo,Sheet,header,body,header_width);
        this.loading = false;
      }
      else{
        this.mensaje = true;
        this.loading = false;
      }
    },(err)=>{
      console.log(err)
    });
    this.loading = false;
  }

}
