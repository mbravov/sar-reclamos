import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import {HomeComponent} from './components/home/home.component';
import { IndexComponent } from './components/redirect/index/index.component';
import { ErrorComponent } from './components/error/error.component';
import { GestionRequerimientoComponent } from './components/gestion-requerimiento/gestion-requerimiento.component';
import { RequerimientoDetalleComponent } from './components/requerimiento-detalle/requerimiento-detalle.component';
import { BandejaConsultaComponent } from './components/bandeja-consulta/bandeja-consulta.component';
import { ConsultaDetalleComponent } from './components/consulta-detalle/consulta-detalle.component';
import { ReporteRquerimientoComponent } from './components/reporte-requerimiento/reporte-requerimiento.component';
import { ReporteGestionComponent } from './components/reporte-gestion/reporte-gestion.component';
import { UsuarioDetalleComponent } from './components/usuario-detalle/usuario-detalle.component'
import { UsuarioNuevoComponent } from './components/usuario-nuevo/usuario-nuevo.component'

import { UsuarioComponent } from './components/usuario/usuario.component';
import { RequerimientoComponent } from './components/requerimiento/requerimiento.component';
import { MAT_DATE_LOCALE, DateAdapter,MAT_DATE_FORMATS , } from '@angular/material/core';
import { RequerimientoSeguimientoComponent } from './components/requerimiento-seguimiento/requerimiento-seguimiento.component';
import { RequerimientoSeguimientoDetalleComponent } from './components/requerimiento-seguimiento-detalle/requerimiento-seguimiento-detalle.component';
import { ConstanciaNoAdeudoComponent } from './components/constancia-no-adeudo/constancia-no-adeudo.component';
import { GestionPlantillaComponent } from './components/gestion-plantilla/gestion-plantilla.component';
import { CnaDetalleComponent } from './components/gestion-cna-minuta/cna-detalle/cna-detalle.component';
import { GestionCnaMinutaComponent } from './components/gestion-cna-minuta/gestion-cna-minuta.component';
import { CnaMinutaSeguimientoComponent } from './components/cna-minuta-seguimiento/cna-minuta-seguimiento.component';
import { CnaRechazarComponent } from './components/gestion-cna-minuta/cna-rechazar/cna-rechazar.component';

const routes: Routes = [

  { path:'home', component: HomeComponent, canActivate: [AuthGuard]},
  { path:'gestion', component: GestionRequerimientoComponent, canActivate: [AuthGuard]},

  { path:'consulta', component: BandejaConsultaComponent, canActivate: [AuthGuard] },
  { path:'consulta/consulta-detalle/:id', component: ConsultaDetalleComponent , canActivate: [AuthGuard]},

  { path:'reportes/reporterequerimiento', component: ReporteRquerimientoComponent, canActivate: [AuthGuard]},
  { path:'reportes/reportegestion', component: ReporteGestionComponent, canActivate: [AuthGuard]},

  //{ path:'home', component: HomeComponent},
  //{ path:'gestion', component: GestionRequerimientoComponent},
  { path:'gestion/requerimiento-detalle/:id', component: RequerimientoDetalleComponent , canActivate: [AuthGuard]},
 // { path:'gestion/:id', component: GestionRequerimientoComponent},

  { path:'gestion/requerimiento', component: RequerimientoComponent , canActivate: [AuthGuard]},
  { path:'seguimiento', component: RequerimientoSeguimientoComponent, canActivate: [AuthGuard]},
  { path:'seguimiento/seguimiento-detalle/:id', component: RequerimientoSeguimientoDetalleComponent, canActivate: [AuthGuard] },

  { path:'constancia-no-adeudo', component: ConstanciaNoAdeudoComponent, canActivate: [AuthGuard]},

  { path:'gestion-plantilla', component: GestionPlantillaComponent , canActivate: [AuthGuard]},

//  // { path:'usuario', component: UsuarioComponent, canActivate: [AuthGuard] },
//   { path:'usuario', component: UsuarioComponent, canActivate: [AuthGuard]  },
//   { path:'usuario/usuario-nuevo', component: UsuarioNuevoComponent , canActivate: [AuthGuard]},
//   { path:'usuario/usuario-detalle/:id', component: UsuarioDetalleComponent , canActivate: [AuthGuard]},
//   { path:'error', component: ErrorComponent },
//   { path:'Redirect/Index', component: IndexComponent },

//   { path:'gestion-cna-minuta/cna-rechazar', component: CnaRechazarComponent   },
//   { path:'gestion-cna-minuta/cna-detalle/:id', component: CnaDetalleComponent },
//   { path:'gestion-cna-minuta', component: GestionCnaMinutaComponent , canActivate: [AuthGuard]  },
//   { path:'cna-minuta-seguimiento', component: CnaMinutaSeguimientoComponent , canActivate: [AuthGuard]  },
//   { path: '**' , pathMatch: 'full', redirectTo: 'home'}

 // { path:'usuario', component: UsuarioComponent, canActivate: [AuthGuard] },
 { path:'usuario', component: UsuarioComponent, canActivate: [AuthGuard]  },
 { path:'usuario/usuario-nuevo', component: UsuarioNuevoComponent, canActivate: [AuthGuard] },
 { path:'usuario/usuario-detalle/:id', component: UsuarioDetalleComponent, canActivate: [AuthGuard]},
 { path:'error', component: ErrorComponent },
 { path:'Redirect/Index', component: IndexComponent },

 { path:'gestion-cna-minuta/cna-rechazar', component: CnaRechazarComponent , canActivate: [AuthGuard] },
 { path:'gestion-cna-minuta/cna-detalle/:id', component: CnaDetalleComponent , canActivate: [AuthGuard]},
 { path:'gestion-cna-minuta', component: GestionCnaMinutaComponent  , canActivate: [AuthGuard] },
 { path:'cna-minuta-seguimiento', component: CnaMinutaSeguimientoComponent  , canActivate: [AuthGuard] },
 { path: '**' , pathMatch: 'full', redirectTo: 'home'}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'en-GB'},

  ]

})
export class AppRoutingModule { }
