import { Injectable } from '@angular/core';
import { SecurityService } from './services/security.service';
import { ajax } from 'rxjs/ajax';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  private keySecret = "4gr3ci4m@ci0n3s";
  public endpoint = '/';
  private UrlBase_ReclamosAPI = environment.UrlBase_ReclamosAPI;
  private xappkey = environment.AppKey;
  private xappcode = environment.AppCode;

  constructor(
    private securityService : SecurityService,
    private http: HttpClient
  ) {}


  public generateHeaders(): any {
    let token = null;

    token = "12345678";
    let httpHeaders = null;
    if (token) {

      httpHeaders = {
        'Content-Type': 'application/json',
        'Token': `Bearer ${ token }`
      }

     // httpHeaders = new HttpHeaders({'Content-Type': 'application/json', "Token": 'Bearer ' + token});
    } else {
      httpHeaders = {
        'Content-Type': 'application/json'
      }
    }
    return httpHeaders;
  }

  public generateTokenReclamacionesAPI(){

    const httpOptions = {
      headers: new HttpHeaders(
        {
          'X-AppKey' : this.decrypt(this.xappkey),
          'X-AppCode': this.decrypt(this.xappcode),
          'Content-Type': 'application/json'
        }
        ),
      observe: 'response' as 'response'
    };
    
   return this.http.post(`${this.UrlBase_ReclamosAPI}/acceso/generarToken`,null, httpOptions);
    
  }

  public generateHeadersSARAPI(): any {
    let token = null;

   // if (this.securityService.leerTokenSeguridad()) {
      token = "12345678"; // this.securityService.leerTokenSeguridad();
  //  }
    let httpHeaders = null;
    if (token) {

      httpHeaders = {
        'Content-Type': 'application/json',
        'Token': `${ token }`
      }

     // httpHeaders = new HttpHeaders({'Content-Type': 'application/json', "Token": 'Bearer ' + token});
    } else {
      httpHeaders = {
        'Content-Type': 'application/json'
      }
    }
    return httpHeaders;
  }

  get<T>(url: any, type?: any): Observable<any> {
    const headers = this.generateHeadersSARAPI();
    const req = ajax.get(url, headers);
    const data$ = new Observable(observer=> {
      req.subscribe(
        (res)=>{
          observer.next(res);
          observer.complete();
        },
        (err)=>{
          observer.error(err);
        }
      );
    });

    return data$;
  }

  post(url: any, object?: any, token?:string): Observable<any> {

    let httpHeaders = {
      'Content-Type': 'application/json',
      'Token': token
    }  

    const req = ajax.post(url, object, httpHeaders);
    const data$ = new Observable(observer=> {
      req.subscribe(
        (res)=>{
          observer.next(res);
          observer.complete();
        },
        (err)=>{
          observer.error(err);
        }
      );
    });
    return data$;
   // return this.http.post(url, object, {headers, responseType: 'json'});
  }


  put(url: any, object: any, type?: any): Observable<any> {
    const headers = this.generateHeadersSARAPI();
    const req = ajax.put(url, object, headers);
    const data$ = new Observable(observer=> {
      req.subscribe(
        (res)=>{
          observer.next(res);
          observer.complete();
        },
        (err)=>{
          observer.error(err);
        }
      );
    });
    return data$;
   // return this.http.put(url, object, {headers, responseType: type});
  }

  delete(url: any, type?: any): Observable<any> {
    const headers = this.generateHeadersSARAPI();
    const req = ajax.delete(url, headers);
    const data$ = new Observable(observer=> {
      req.subscribe(
        (res)=>{
          observer.next(res);
          observer.complete();
        },
        (err)=>{
          observer.error(err);
        }
      );
    });
    return data$;
    //const headers = this.generateHeaders(type);
    //return this.http.delete(url, {headers, responseType: type});
  }


  decrypt(cadena:any) {
    var bytes  = CryptoJS.AES.decrypt(cadena, this.keySecret);
    var originalText = bytes.toString(CryptoJS.enc.Utf8);
    return originalText;
  }

}
