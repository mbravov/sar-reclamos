import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DialogService } from '../../../services/comun/dialog.service';
import { SecurityService } from '../../../services/security.service';
import { CnaMinutaOcmService } from '../../../services/cna-minuta-ocm.service';
import { FiltroAccionService } from '../../../services/filtro-accion.service';
import { ConfiguracionAccionService } from '../../../services/configuracion-accion.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Constantes } from '../../../comun/constantes';
import { forkJoin } from 'rxjs';
import { RequerimientoService } from 'src/app/services/requerimiento.service';

@Component({
  selector: 'app-cna-derivar',
  templateUrl: './cna-derivar.component.html',
  styleUrls: ['./cna-derivar.component.css']
})
export class CnaDerivarComponent implements OnInit {

  form!: FormGroup;
  formSubmitted:boolean = false;

  lengthDescripcion: number  = 300;
  MaxlengthDescripcion: number  = 300;
  MinlengthDescripcion: number  = 5;

  IdConfiguracionAccion:any;
  emision:any;
  agencias!:any[];
  usuariosPorAgencia!:any[];
  FechaEntregaOlva:any;

  loading:boolean = false;

  filtroPerfil!:any[];
  constructor(
    private _formBuilder: FormBuilder,
    private _dialogService: DialogService,
    private _securityService: SecurityService,
    private _cnaMinutaOcmService : CnaMinutaOcmService,
    private _filtroAccionService : FiltroAccionService,
    private _configuracionAccion : ConfiguracionAccionService,
    private _requerimientoService : RequerimientoService,
    public dialogRef: MatDialogRef<CnaDerivarComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {

    this.emision = data.CreditoClienteDetalle;
    this.IdConfiguracionAccion = data.IdConfiguracionAccion;
    this.FechaEntregaOlva = data.FechaEntregaOlva;

    
  }

  ngOnInit(): void {
    this.inicializarFormulario();
    this.pageLoad();
  }

  pageLoad():void{
    let params = {
      Perfil : this._securityService.leerPerfil(),
      UsuarioWeb : this._securityService.leerUsuarioWeb()
    }
    this.loading = true;
    forkJoin([
      this._cnaMinutaOcmService.ObtenerAgencias(params),
      this._filtroAccionService.obtener()      

    ]).subscribe( (data:any) => {
        this.agencias = data[0].response;
        this.filtroPerfil = data[1].response;
        this.loading = false;
        this.obtenerUsuarios(this.agencias[0].codAgencia);
        this.form.controls["CodAgencia"].setValue(""+this.agencias[0].codAgencia);

        if(this.agencias.filter(x=>x.codAgencia==""+this.emision.codAgencia).length){
          this.form.controls["CodAgencia"].setValue(""+this.emision.codAgencia);
        }
    }, (error) =>{
      this.loading = false;
      console.log(error);
    });


  }


  obtenerUsuarios(codAgencia:number){
    var Perfil = {

      Perfil: Constantes.UDA,
      UsuarioWeb: '',
      Estado: true
    };

    this.loading = true;
    this._cnaMinutaOcmService.ObtenerUsuarios(Perfil).subscribe((data:any) => {
      this.usuariosPorAgencia = data.response.filter((x:any) => x.codAgencia == codAgencia);
      console.log(this.usuariosPorAgencia);
      this.loading = false;
    },  (err)=>{
      this.loading = false;
      console.log(err);
    });

  }

  inicializarFormulario() : void {
    console.log(this.emision.codAgencia);
    this.form = this._formBuilder.group({

      CodAgencia:["",  [Validators.required]],
      UsuarioUDA:["",  [Validators.required]],
      Descripcion: [null],

    });



  }


  derivar(){

    this.formSubmitted = true;

    if (this.form.invalid) {
      return;
    }


    var request = {
      "IdConfiguracionAccion" : this.IdConfiguracionAccion,
      "GrupoFiltro" : null,
      "Nombres": this.emision.nombres,
      "TipoDocumento": this.emision.tipoDocumento,
      "NroDocumento": this.emision.numeroDocumento,
      "emision": {
        "IdEmision" : +this.emision.idEmision,
        "IdCreditoCliente"  : +this.emision.idCreditoCliente,
        "Estado"  : this.emision.idEstado,
        "TipoFlujo"  :1,
        "strFechaEntregaOlva" : null,
        "MedioNotificacion": null,
        "Email" : null,
        "Whatsapp" : null,
        "FechaCargoEntrega" : null,
        "UsuarioRegistro" :  this._securityService.leerUsuarioWeb(),
        "UsuarioModificacion" :  this._securityService.leerUsuarioWeb()
      },
      "derivacion": {
        "IdDerivacion" : +this.emision.idDerivacion,
        "IdEmision" : +this.emision.idEmision,
        "Perfil" : this._securityService.leerPerfil(),
        "Estado" : this.emision.idEstado,
        "PerfilDerivado" : Constantes.UDA.toUpperCase(),
        "FechaDerivado" : null,
        "CodAgencia" : +this.form.controls["CodAgencia"].value,
        "Agencia": this.emision.agencias,
        "UsuarioRegistro" : this._securityService.leerUsuarioWeb(),
        "UsuarioModificacion":  this._securityService.leerUsuarioWeb(),
        "CodUsuarioDerivado": +this.form.controls['UsuarioUDA'].value
      },
      "comentarioEmision":
      {
          "IdEmision" : this.emision.idEmision,
          "IdDerivacion" : this.emision.idDerivacion,
          "IdDerivacionMovimiento" :  this.emision.idDerivacionMovimiento,
          "Texto" :  this.form.controls['Descripcion'].value,
          "UsuarioRegistro" :  this._securityService.leerUsuarioWeb(),
          "Archivo" : null,
          "NombreArchivo" : null,
          "CodTabla" : Constantes.ComentarioEmision,
          "DNI": this.emision.numeroDocumento,
          "Perfil": this._securityService.leerPerfil(),
          "CodAgencia":  this.emision.codAgencia,
          "Estado": this.emision.idEstado,
          "IdMotivo" : ''

      }
    };

    this._dialogService
    .openConfirmDialog('¿Está seguro de Derivar la CNA y Minuta al Usuario de Agencia? ')
    .afterClosed()
    .subscribe((res) => {
      if (res) {
          this.loading = true;
          this._configuracionAccion.accion(request).subscribe((data:any)=>{
            this.loading = false;
            console.log(data);
            if(data.response.success.resultado){
              
              this._dialogService.openMsgSuccessDialog("se derivó correctamente");
              this.dialogRef.close({ data: true });
            }
            else{
              this._dialogService.openMsgSuccessDialog("Hubo un problema al derivar");
            }
          });
      }
    });




  }


  eliminarArchivoLaserFiche(idLaserficheCNA:any = null) : void {

    const oBE = {
      "LaserficheID": ""+idLaserficheCNA
      };
      this.loading = true;
      this._requerimientoService.eliminarArchivo(oBE).subscribe(result=>{
        this.loading = false;
        console.log("eliminarArchivo",result);
      },
      (err)=>{
        this.loading = false;
        console.log(err);
      }
      );

   }

}
