﻿using Agrobanco.Control.Security;
using System;
using System.Collections.Generic;
using System.Text;
using static Agrobanco.Control.Util.Constants;

namespace Agrobanco.Control.Base
{
    public class BaseControl
    {
        private readonly ITokenControl _tokencontrol;

        public BaseControl()
        {
            _tokencontrol = new TokenControl();
        }

        #region token
        public void ValidarTokenSesion(string token)
        {
            try
            {
                var paramkeytoken = Util.Util.ObtenerValueFromKey(ConstantesParametros.TokenClave);
                _tokencontrol.IsTokenJWTValid(paramkeytoken, token);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public string ObtenerValorClaimToken(string token, string tipoclaim)
        {
            try
            {
                token = token.Contains("Bearer ") ? token.Replace("Bearer ", "") : token;

                var paramkeytoken = Util.Util.ObtenerValueFromKey(ConstantesParametros.TokenClave);

                var valorclaim = _tokencontrol.GetClaimValueByToken(paramkeytoken, tipoclaim, token);

                return valorclaim;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion
    }
}
