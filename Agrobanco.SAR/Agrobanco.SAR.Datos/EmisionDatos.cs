﻿using Agrobanco.SAR.Comun;
using Agrobanco.SAR.Datos.SqlServer.Acceso;
using Agrobanco.SAR.Entidades.CNA_Minuta;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SAR.Datos
{
    public class EmisionDatos
    {
        public string cadenaConexion;
        public async Task<bool> Insertar(EmisionBE request)
        {
            bool resultado = false;

            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("SP_EMISION_INSERTAR", conn))
                    {

                        try
                        {
                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.Add("@IdCreditoCliente", SqlDbType.Int).Value = request.IdEmision;
                            cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = request.Estado;
                            cmd.Parameters.Add("@TipoFlujo", SqlDbType.Int).Value = request.TipoFlujo;
                            cmd.Parameters.Add("@FechaEntregaOlva", SqlDbType.DateTime).Value = request.FechaEntregaOlva;
                            cmd.Parameters.Add("@MedioNotificacion", SqlDbType.Int).Value = request.MedioNotificacion;
                            cmd.Parameters.Add("@Email", SqlDbType.VarChar, 100).Value = request.Email;
                            cmd.Parameters.Add("@Whatsapp", SqlDbType.VarChar, 100).Value = request.Whatsapp;
                            cmd.Parameters.Add("@FechaCargoEntrega", SqlDbType.DateTime).Value = request.FechaCargoEntrega;
                            cmd.Parameters.Add("@UsuarioRegistro", SqlDbType.VarChar, 50).Value = request.UsuarioRegistro;
                            //SqlParameter pIdComentarioEmision = new SqlParameter("@IdComentarioEmision", SqlDbType.Int);
                            //pIdComentarioEmision.Direction = ParameterDirection.Output;
                            //cmd.Parameters.Add(pIdComentarioEmision);

                            await cmd.ExecuteNonQueryAsync();

                            resultado = true;

                        }

                        catch (Exception ex)
                        {

                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            resultado = false;
                            throw (ex);
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }


                    }
                }
            }
            catch (Exception ex)
            {
                resultado = false;
                throw ex;
            }
            return await Task.Run(() => resultado);
        }
        public async Task<bool> Actualizar(EmisionBE request)
        {
            bool resultado = false;

            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("SP_EMISION_ACTUALIZAR", conn))
                    {

                        try
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@IdEmision", SqlDbType.Int).Value = request.IdEmision;
                            cmd.Parameters.Add("@IdCreditoCliente", SqlDbType.Int).Value = request.IdCreditoCliente;
                            cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = request.Estado;
                            cmd.Parameters.Add("@TipoFlujo", SqlDbType.Int).Value = request.TipoFlujo;

                            if(string.IsNullOrEmpty(request.strFechaEntregaOlva))
                                cmd.Parameters.Add("@FechaEntregaOlva", SqlDbType.DateTime).Value = DBNull.Value;
                            else
                                cmd.Parameters.Add("@FechaEntregaOlva", SqlDbType.DateTime).Value = Util.ConvertStringToDatetime(request.strFechaEntregaOlva);

                            cmd.Parameters.Add("@MedioNotificacion", SqlDbType.Int).Value = request.MedioNotificacion;
                            cmd.Parameters.Add("@Email", SqlDbType.VarChar, 100).Value = request.Email;
                            cmd.Parameters.Add("@Whatsapp", SqlDbType.VarChar, 100).Value = request.Whatsapp;
                            cmd.Parameters.Add("@FechaCargoEntrega", SqlDbType.DateTime).Value = request.FechaCargoEntrega;
                            cmd.Parameters.Add("@UsuarioModificacion", SqlDbType.VarChar, 50).Value = request.UsuarioModificacion;
                            //SqlParameter pIdComentarioEmision = new SqlParameter("@IdComentarioEmision", SqlDbType.Int);
                            //pIdComentarioEmision.Direction = ParameterDirection.Output;
                            //cmd.Parameters.Add(pIdComentarioEmision);

                            await cmd.ExecuteNonQueryAsync();

                            resultado = true;

                        }

                        catch (Exception ex)
                        {

                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            resultado = false;
                            throw (ex);
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }


                    }
                }
            }
            catch (Exception ex)
            {
                resultado = false;
                throw ex;
            }
            return await Task.Run(() => resultado);
        }
    }
}
