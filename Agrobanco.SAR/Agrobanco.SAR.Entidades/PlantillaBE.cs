﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades
{
    public class PlantillaBE
    {
		public int? CodTabla { get; set; }
		public string CodTipoPlantilla { get; set; }
		public Int64? ReferenciaID { get; set; }
		public int? LaserficheID { get; set; }
		public string Descripcion { get; set; }
		public bool Estado { get; set; }
		public string UsuarioRegistro { get; set; }
		public string TipoPlantilla { get; set; }
		public Int64 CodPlantilla { get; set; }
		public string UsuarioModificacion { get; set; }
	}
}
