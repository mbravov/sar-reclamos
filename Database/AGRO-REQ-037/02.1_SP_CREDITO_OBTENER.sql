USE DBAgrSAR
GO

DROP PROCEDURE IF EXISTS dbo.SP_CREDITO_OBTENER; 
GO

 
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 30/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_CREDITO_OBTENER
-- PARAMETOS				: @TipoIDN VARCHAR(20),
--							  @Identificacion VARCHAR(20)
-- FUNCIONALIDAD			:  
--================================================================================================================================

CREATE PROCEDURE SP_CREDITO_OBTENER	 
@TipoIDN VARCHAR(20),
@Identificacion VARCHAR(20)
AS
BEGIN
		SELECT top 1 
		   CodCred
		  ,Saldo
		  ,Moneda
		  ,Estado
		  ,FechaPago
		  ,CodCliente
		  ,TipoIDN
		  ,Identificacion
		  ,NomCliente
	 FROM  Credito
	 WHERE LOWER(TipoIDN) = LOWER(@TipoIDN)  AND
		LOWER(Identificacion) = LOWER(@Identificacion)
	order by  CodCredito desc

END
GO