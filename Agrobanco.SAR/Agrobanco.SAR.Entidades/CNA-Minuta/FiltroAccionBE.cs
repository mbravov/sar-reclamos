﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades.CNA_Minuta
{
    public class FiltroAccionBE
    {
        public int IdConfiguracionAccion { get; set; }
        public int Grupo { get; set; }
        public int TipoFiltro { get; set; }
        public string Valor1 { get; set; }
        public int Valor2 { get; set; }
        public string Valor { get; set; }
    }
}
