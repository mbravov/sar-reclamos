﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades
{
    public class AreaBE
    {
        public int Codigo { get; set; }
        public string Descripcion { get; set; }
    }
}
