﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades.CNA_Minuta
{
    public class CreditoClienteCNABE
    {
        public string TipoIDN { get; set; }
        public string Identificacion { get; set; }
        public string NroPrestamo { get; set; }
        public string UltimoCredito { get; set; }
        public string EstadoActual { get; set; }
        public string SaldoDeudo { get; set; }
        public string Capital { get; set; }
        public string InteresCompensatorio { get; set; }
        public string InteresMoratorio { get; set; }
        public string Otros { get; set; }
        
    }
}
