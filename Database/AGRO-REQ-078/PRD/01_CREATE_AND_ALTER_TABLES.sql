USE DBAgrSAR
GO

IF COL_LENGTH('dbo.Usuario','CodAgencia') IS NULL
BEGIN
	
	ALTER TABLE DBO.USUARIO
	ADD CodAgencia INT NULL

END
GO
IF COL_LENGTH('dbo.Agencia','FlagAgencia') IS NULL
BEGIN
	
	ALTER TABLE DBO.AGENCIA
	ADD FlagAgencia INT NULL

END
GO

IF OBJECT_ID('dbo.RecursoEmision', 'U') IS NOT NULL 
BEGIN
	DROP TABLE dbo.RecursoEmision;
END
GO

IF OBJECT_ID('dbo.EmisionMovimiento', 'U') IS NOT NULL 
BEGIN
	DROP TABLE dbo.EmisionMovimiento;
END
GO

IF OBJECT_ID('dbo.GestionEmision', 'U') IS NOT NULL 
BEGIN
	DROP TABLE dbo.GestionEmision;
END
GO

IF OBJECT_ID('dbo.DerivacionMovimiento', 'U') IS NOT NULL 
BEGIN
	DROP TABLE dbo.DerivacionMovimiento;
END
GO

IF OBJECT_ID('dbo.Derivacion', 'U') IS NOT NULL 
BEGIN
	DROP TABLE dbo.Derivacion;
END
GO

IF OBJECT_ID('dbo.ComentarioEmision', 'U') IS NOT NULL 
BEGIN
	DROP TABLE dbo.ComentarioEmision;
END
GO

IF OBJECT_ID('dbo.Emision', 'U') IS NOT NULL 
BEGIN
	DROP TABLE dbo.Emision;
END
GO

IF OBJECT_ID('dbo.CreditoCliente', 'U') IS NOT NULL 
BEGIN
	DROP TABLE dbo.CreditoCliente;
END
GO

IF OBJECT_ID('dbo.ConfiguracionDerivacion', 'U') IS NOT NULL 
BEGIN
	DROP TABLE dbo.ConfiguracionDerivacion;
END
GO

IF OBJECT_ID('dbo.EstadoEmision', 'U') IS NOT NULL 
BEGIN
	DROP TABLE dbo.EstadoEmision;
END
GO

IF OBJECT_ID('dbo.ConfiguracionDerivacion', 'U') IS NOT NULL 
BEGIN
	DROP TABLE dbo.ConfiguracionDerivacion;
END
GO

IF OBJECT_ID('dbo.FiltroAccion', 'U') IS NOT NULL 
BEGIN
	DROP TABLE dbo.FiltroAccion;
END
GO

IF OBJECT_ID('dbo.ConfiguracionAccion', 'U') IS NOT NULL 
BEGIN
	DROP TABLE dbo.ConfiguracionAccion;
END
GO

IF OBJECT_ID('dbo.Accion', 'U') IS NOT NULL 
BEGIN
	DROP TABLE dbo.Accion;
END
GO

CREATE TABLE dbo.CreditoCliente
(
	IdCreditoCliente INT IDENTITY(1,1) NOT NULL,
	NumeroCredito VARCHAR(20) NOT NULL,
	CodAgencia INT NOT NULL,
	Agencia VARCHAR(100),
	AnalistaProponente VARCHAR(300),
	FechaCancelacion DATETIME NOT NULL,
	Banco VARCHAR(100),
	TipoDocumento VARCHAR(10) NOT NULL,
	NumeroDocumento VARCHAR(20) NOT NULL,
	Nombres VARCHAR(400) NOT NULL,
	Direccion VARCHAR(200) NULL,
	TipoProcesamiento INT NOT NULL,
	IdLaserficheCNA INT NULL,
	Procesado BIT NOT NULL,
	FechaRegistro DATETIME NOT NULL,
	UsuarioRegistro VARCHAR(50) NOT NULL,
	FechaModificacion DATETIME,
	UsuarioModificacion VARCHAR(50)
)
GO

ALTER TABLE dbo.CreditoCliente
ADD CONSTRAINT PK_CreditoCliente PRIMARY KEY (IdCreditoCliente)
GO

CREATE TABLE dbo.Emision
(
	IdEmision INT IDENTITY(1,1) NOT NULL,
	IdCreditoCliente INT NOT NULL,
	Estado INT NOT NULL,
	TipoFlujo INT,
	FechaEntregaOlva DATETIME,
	MedioNotificacion INT,
	Email VARCHAR(100),
	Whatsapp VARCHAR(100),
	FechaCargoEntrega DATETIME,
	FechaRegistro DATETIME NOT NULL,
	UsuarioRegistro VARCHAR(50) NOT NULL,
	FechaModificacion DATETIME,
	UsuarioModificacion VARCHAR(50)
)
GO

ALTER TABLE dbo.Emision
ADD CONSTRAINT PK_Emision PRIMARY KEY (IdEmision)
GO

ALTER TABLE dbo.Emision
ADD CONSTRAINT FK_Emision_CreditoCliente FOREIGN KEY (IdCreditoCliente) REFERENCES CreditoCliente(IdCreditoCliente)
GO

CREATE TABLE dbo.EmisionMovimiento
(
	IdEmisionMovimiento INT IDENTITY(1,1) NOT NULL,
	IdEmision INT NOT NULL,
	Estado INT NOT NULL,
	TipoFlujo INT,
	FechaRegistro DATETIME NOT NULL,
	UsuarioRegistro VARCHAR(50) NOT NULL,
)
GO

ALTER TABLE dbo.EmisionMovimiento
ADD CONSTRAINT PK_EmisionMovimiento PRIMARY KEY (IdEmisionMovimiento)
GO

ALTER TABLE dbo.EmisionMovimiento
ADD CONSTRAINT FK_EmisionMovimiento_Emision FOREIGN KEY (IdEmision) REFERENCES dbo.Emision(IdEmision)
GO


CREATE TABLE dbo.GestionEmision
(
	IdGestionEmision INT IDENTITY(1,1) NOT NULL,
	IdEmision INT NOT NULL,
	Estado INT,
	TipoFlujo INT,
	FechaEntregaOlva DATETIME,
	MedioNotificacion INT,
	Email VARCHAR(100),
	Whatsapp VARCHAR(100),
	FechaCargoEntrega DATETIME,
	FechaRegistro DATETIME NOT NULL,
	UsuarioRegistro VARCHAR(50) NOT NULL
)
GO

ALTER TABLE dbo.GestionEmision
ADD CONSTRAINT PK_GestionEmision PRIMARY KEY (IdGestionEmision)
GO

ALTER TABLE dbo.GestionEmision
ADD CONSTRAINT FK_GestionEmision_Emision FOREIGN KEY (IdEmision) REFERENCES dbo.Emision(IdEmision)
GO

CREATE TABLE dbo.Derivacion
(
	IdDerivacion INT IDENTITY(1,1) NOT NULL,
	IdEmision INT NOT NULL,
	Perfil VARCHAR(5) NOT NULL,
	Estado INT NOT NULL,
	PerfilDerivado VARCHAR(5),
	FechaDerivado DATETIME,
	CodAgencia INT,
	CodUsuarioDerivado INT NULL,
	FechaRegistro DATETIME NOT NULL,
	UsuarioRegistro VARCHAR(50) NOT NULL,
	FechaModificacion DATETIME,
	UsuarioModificacion VARCHAR(50)
)
GO

ALTER TABLE dbo.Derivacion
ADD CONSTRAINT PK_Derivacion PRIMARY KEY (IdDerivacion)
GO

ALTER TABLE dbo.Derivacion
ADD CONSTRAINT FK_Derivacion_Emision FOREIGN KEY (IdEmision) REFERENCES dbo.Emision(IdEmision)
GO

CREATE TABLE dbo.DerivacionMovimiento
(
	IdDerivacionMovimiento INT IDENTITY(1,1) NOT NULL,
	IdDerivacion INT NOT NULL,
	IdEmision INT NOT NULL,
	Perfil VARCHAR(5) NOT NULL,
	Estado INT NOT NULL,
	PerfilDerivado VARCHAR(5),
	CodAgencia INT,
	CodUsuarioDerivado INT NULL,
	FechaRegistro DATETIME NOT NULL,
	UsuarioRegistro VARCHAR(50) NOT NULL
)
GO

ALTER TABLE dbo.DerivacionMovimiento
ADD CONSTRAINT PK_DerivacionMovimiento PRIMARY KEY (IdDerivacionMovimiento)
GO

ALTER TABLE dbo.DerivacionMovimiento
ADD CONSTRAINT FK_DerivacionMovimiento_Derivacion FOREIGN KEY (IdDerivacion) REFERENCES dbo.Derivacion(IdDerivacion)
GO

CREATE TABLE dbo.ComentarioEmision
(
	IdComentarioEmision INT IDENTITY(1,1) NOT NULL,
	IdEmision INT NOT NULL,
	IdDerivacion INT,
	IdDerivacionMovimiento INT,
	IdMotivo INT,
	Motivo VARCHAR(100),
	Texto VARCHAR(300),
	FechaRegistro DATETIME NOT NULL,
	UsuarioRegistro VARCHAR(50) NOT NULL,
	FechaModificacion DATETIME,
	UsuarioModificacion VARCHAR(50)
)
GO

ALTER TABLE dbo.ComentarioEmision
ADD CONSTRAINT PK_ComentarioEmision PRIMARY KEY (IdComentarioEmision)
GO

ALTER TABLE dbo.ComentarioEmision
ADD CONSTRAINT FK_ComentarioEmision_Emision FOREIGN KEY (IdEmision) REFERENCES dbo.Emision(IdEmision)
GO

CREATE TABLE dbo.RecursoEmision
(
	IdRecursoEmision INT IDENTITY(1,1) NOT NULL,	
	TipoRecurso INT,
	IdEmision INT NOT NULL,
	FechaRegistro DATETIME NOT NULL,	
	UsuarioRegistro VARCHAR(50) NOT NULL,
	FechaModificacion DATETIME,
	UsuarioModificacion VARCHAR(50)
)
GO

ALTER TABLE dbo.RecursoEmision
ADD CONSTRAINT PK_RecursoEmision PRIMARY KEY (IdRecursoEmision)
GO

ALTER TABLE dbo.RecursoEmision
ADD CONSTRAINT FK_RecursoEmision_Emision FOREIGN KEY (IdEmision) REFERENCES dbo.Emision(IdEmision)
GO

CREATE TABLE dbo.EstadoEmision
(
	IdEstadoEmision INT IDENTITY(1,1) NOT NULL,
	IdEstado INT NOT NULL,
	TipoEstado CHAR(1) NOT NULL,
	Perfil VARCHAR(5),
	Descripcion VARCHAR(50) NOT NULL,
	FechaRegistro DATETIME NOT NULL,
	UsuarioRegistro VARCHAR(50) NOT NULL
)
GO

ALTER TABLE dbo.EstadoEmision
ADD CONSTRAINT PK_EstadoEmision PRIMARY KEY (IdEstadoEmision)
GO

CREATE TABLE dbo.Accion
(
	IdAccion INT NOT NULL,
	TipoAccion INT NOT NULL,
	Descripcion VARCHAR(50)
)
GO

ALTER TABLE dbo.Accion
ADD CONSTRAINT PK_Accion PRIMARY KEY (IdAccion)
GO

CREATE TABLE dbo.ConfiguracionAccion
(
	IdConfiguracionAccion INT NOT NULL,
	IdAccion INT NOT NULL,
	Perfil VARCHAR(5) NULL,
	IdEstado INT NULL ,
	Activo BIT NOT NULL
)
GO

ALTER TABLE dbo.ConfiguracionAccion
ADD CONSTRAINT PK_ConfiguracionAccion PRIMARY KEY (IdConfiguracionAccion)
GO

ALTER TABLE dbo.ConfiguracionAccion
ADD CONSTRAINT FK_ConfiguracionAccion_Accion FOREIGN KEY (IdAccion) REFERENCES dbo.Accion(IdAccion)
GO

CREATE TABLE dbo.FiltroAccion
(
	IdFiltroAccion INT IDENTITY(1,1) NOT NULL,
	IdConfiguracionAccion INT NOT NULL,
	Grupo INT NOT NULL,
	TipoFiltro INT NOT NULL,
	Valor1 VARCHAR(200),
	Valor2 INT
)
GO

ALTER TABLE dbo.FiltroAccion
ADD CONSTRAINT PK_FiltroAccion PRIMARY KEY (IdFiltroAccion)
GO

ALTER TABLE dbo.FiltroAccion
ADD CONSTRAINT FK_FiltroAccion_ConfiguracionAccion FOREIGN KEY (IdConfiguracionAccion) REFERENCES dbo.ConfiguracionAccion(IdConfiguracionAccion)
GO


CREATE TABLE dbo.ConfiguracionDerivacion
(
	IdConfiguracionDerivacion INT IDENTITY(1,1) NOT NULL,
	IdConfiguracionAccion INT NOT NULL,
	GrupoFiltro INT,
	TipoProcesamiento INT NOT NULL,
	TipoEvento INT NOT NULL,
	Perfil VARCHAR(5),
	IdEstado INT
)
GO

ALTER TABLE dbo.ConfiguracionDerivacion
ADD CONSTRAINT PK_ConfiguracionDerivacion PRIMARY KEY (IdConfiguracionDerivacion)
GO

ALTER TABLE dbo.ConfiguracionDerivacion
ADD CONSTRAINT FK_ConfiguracionDerivacion_ConfiguracionAccion FOREIGN KEY (IdConfiguracionAccion) REFERENCES dbo.ConfiguracionAccion(IdConfiguracionAccion)
GO
