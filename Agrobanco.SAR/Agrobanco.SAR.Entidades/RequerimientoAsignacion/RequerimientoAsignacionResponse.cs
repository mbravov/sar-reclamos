﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades.RequerimientoAsignacion
{
    public class RequerimientoAsignacionResponse
    {
        public Int64 CodRequerimientoAsignacion { get; set; }
        public Int64 CodRequerimiento { get; set; }

        public Int64 CodRequerimientoMovimiento { get; set; }
        public string  UsuarioAsignado  { get; set; }
        public string  UsuarioRegistro  { get; set; }
        public DateTime? FechaRegistro { get; set; }
    }
}
