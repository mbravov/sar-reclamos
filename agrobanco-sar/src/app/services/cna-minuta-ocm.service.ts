import { Injectable } from '@angular/core';
import { AppService } from '../app.service';
import { SecurityService } from './security.service';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CnaMinutaOcmService extends AppService {

  constructor(_securityService : SecurityService) {
    super(_securityService);
  }
  ObtenerCnaMinutaLista<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint =  `${environment.UrlBase_SARAPI}/cna-minuta/listarCnaMinuta`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  ObtenerEstados<T>(datos:any):Observable<any[]>{
     this.endpoint = `${environment.UrlBase_SARAPI}/cna-minuta/listarEstados`;
     console.log("endpoint",this.endpoint);
     return this.post(this.endpoint,datos);
   }

   ObtenerSeguimientoLista<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint =  `${environment.UrlBase_SARAPI}/cna-minuta/listarSeguimiento`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  ObtenerEstadosGeneral<T>():Observable<any[]>{
     this.endpoint = `${environment.UrlBase_SARAPI}/cna-minuta/listarEstadosGen`;
     console.log("endpoint",this.endpoint);
     return this.post(this.endpoint);
   }

   ObtenerAgencias<T>(datos:any):Observable<any[]>{   
    this.endpoint =  `${environment.UrlBase_SARAPI}/cna-minuta/listarAgencias`;
     console.log("endpoint",this.endpoint);
     return this.post(this.endpoint,datos);
   }

   ObtenerUsuarios<T>(datos:any):Observable<any[]>{
    console.log("input",datos);
    //console.log("input",datos);
    this.endpoint = `${environment.UrlBase_SARAPI}/Usuario/listar`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  obtenerDetalle<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint =  `${environment.UrlBase_SARAPI}/cna-minuta/obtener-por-numero-documento`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  derivar<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${environment.UrlBase_SARAPI}/derivar`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  rechazar<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${environment.UrlBase_SARAPI}/ocm/rechazar`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }
  finalizar<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${environment.UrlBase_SARAPI}/ocm/finalizar`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }
  reaperturar<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${environment.UrlBase_SARAPI}/ocm/reaperturar`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }
  subirCartaRespuesta<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${environment.UrlBase_SARAPI}/ocm/subir-carta-respuesta`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  reenviar_correo<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${environment.UrlBase_SARAPI}/correo/reenviar-correo-CRenvioOFN`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }
  reenviar_correo_OCM<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${environment.UrlBase_SARAPI}/correo/reenviar-correo-RechazoOCM`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  GenerarDocumento<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${environment.UrlBase_SARAPI}/documentos/generador-gestion-cna`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }
  DerivarMasicoADMAGaUsuAge<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${environment.UrlBase_SARAPI}/configuracion-derivacion/derivar-masivo-admage-a-usuage`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

}
