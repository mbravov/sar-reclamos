USE DBAgrSAR
GO

--Estructura de datos
ALTER TABLE Parametro
ALTER COLUMN Valor VARCHAR(1000) NULL
GO

ALTER TABLE Usuario
ALTER COLUMN UsuarioWeb VARCHAR(20) NOT NULL
GO

-- Datos
DELETE FROM Parametro where  Grupo IN (1300) OR Parametro IN (1300)
GO

DELETE FROM Parametro where  Grupo IN (1000, 1200)
GO

INSERT INTO Parametro (Grupo,Parametro,Descripcion,Valor,Referencia,Orden,Estado) VALUES (1000,1,'[X_FECHA_ACTUAL_X]','FechaActual','1',1,1)
INSERT INTO Parametro (Grupo,Parametro,Descripcion,Valor,Referencia,Orden,Estado) VALUES (1000,2,'[X_NOMBRES_APELLIDOS_X]','NombresApellidos','2',2,1)
INSERT INTO Parametro (Grupo,Parametro,Descripcion,Valor,Referencia,Orden,Estado) VALUES (1000,3,'[X_CORREO_X]','CorreoElectronico','2',3,1)
INSERT INTO Parametro (Grupo,Parametro,Descripcion,Valor,Referencia,Orden,Estado) VALUES (1000,4,'[X_DIRECCION_X]','Direccion','2',4,1)
INSERT INTO Parametro (Grupo,Parametro,Descripcion,Valor,Referencia,Orden,Estado) VALUES (1000,5,'[X_NRO_REQ_X]','NroRequerimiento','2',5,1)
INSERT INTO Parametro (Grupo,Parametro,Descripcion,Valor,Referencia,Orden,Estado) VALUES (1000,6,'[X_FECHA_REGISTRO_X]','FechaRegistro','3',6,1)
INSERT INTO Parametro (Grupo,Parametro,Descripcion,Valor,Referencia,Orden,Estado) VALUES (1000,7,'[X_NRO_CREDITO_X]','',null,7,1)
INSERT INTO Parametro (Grupo,Parametro,Descripcion,Valor,Referencia,Orden,Estado) VALUES (1000,8,'[X_ESTADO_CREDITO_X]','',null,8,1)
INSERT INTO Parametro (Grupo,Parametro,Descripcion,Valor,Referencia,Orden,Estado) VALUES (1000,9,'[X_SALDO_CREDITO_X]','',null,9,1)

INSERT INTO Parametro (Grupo,Parametro,Descripcion,Valor,Referencia,Orden,Estado) VALUES (1200,1,'Carta de Respuesta',null,null,1,1)


--Rollback de plantillas

DELETE FROM RECURSO 
WHERE CodRecurso IN (SELECT RE.CodRecurso FROM Plantilla PL 
											INNER JOIN  RECURSO RE ON RE.ReferenciaID = PL.CodPlantilla
											WHERE PL.CodTipoPlantilla IN (2, 3) AND  RE.CodTabla = 5)

DELETE FROM Plantilla 
WHERE CodPlantilla IN (SELECT PL.CodPlantilla 
							FROM Plantilla PL 
							INNER JOIN  RECURSO RE ON RE.ReferenciaID = PL.CodPlantilla
							WHERE PL.CodTipoPlantilla IN (2, 3) AND  RE.CodTabla = 5)


-- Stored Procedures

DROP PROCEDURE IF EXISTS SP_CONSULTA_REGISTRAR
GO

DROP PROCEDURE IF EXISTS dbo.SP_CONSULTA_OBTENER_DETALLE; 
GO

DROP PROCEDURE IF EXISTS dbo.SP_CONSULTA_OBTENER; 
GO

DROP PROCEDURE IF EXISTS dbo.SP_REQUERIMIENTO_SEGUIMIENTO; 
GO

DROP PROCEDURE IF EXISTS dbo.SP_REQUERIMIENTO_ACTUALIZAR_PENDIENTE_SIN_ATENCION; 
GO

--drop de los SP nuevos del sprint 2

DROP PROCEDURE IF EXISTS dbo.SP_COMENTARIO_OBTENER; 
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 26/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- PARAMETOS				:
--							  @CodRequerimientoMovimiento
--							  @CodRequerimiento
--							  @Estado
-- FUNCIONALIDAD			: Obtener los comentarios registrado por el movimiento, requerimiento o estado del requerimiento
--================================================================================================================================

CREATE PROCEDURE SP_COMENTARIO_OBTENER
@CodRequerimientoMovimiento BIGINT,
@CodRequerimiento BIGINT,
@Estado INT,
@CodTabla INT
AS
BEGIN
	SELECT DISTINCT  C.CodComentario
		  ,C.CodRequerimientoMovimiento
		  ,C.CodRequerimiento
		  ,C.CodMotivo
		  ,C.CodArea
		  ,A.Descripcion as Area
		  ,C.Descripcion
		  ,C.FechaSolicitud
		  ,C.UsuarioRegistro
		  ,C.FechaRegistro
		  ,R.LaserficheID
		  ,R.CodTabla
		  ,R.ReferenciaID
		  ,RM.Estado
		  ,U.NombresApellidos
	FROM  Comentario C
		LEFT JOIN  RequerimientoMovimiento RM ON C.CodRequerimientoMovimiento = RM.CodRequerimientoMovimiento		
		LEFT JOIN Area A ON C.CodArea = A.Codigo 
		LEFT JOIN Recurso R ON R.ReferenciaID =  C.CodComentario AND R.CodTabla = @CodTabla
		LEFT JOIN Usuario U ON C.UsuarioRegistro = U.UsuarioWeb
	WHERE (ISNULL(@CodRequerimientoMovimiento,0) = 0 OR C.CodRequerimientoMovimiento = @CodRequerimientoMovimiento) 
		AND	(ISNULL(@CodRequerimiento,0) = 0 OR RM.CodRequerimiento = @CodRequerimiento)  
		AND	(ISNULL(@Estado,0) = 0 OR RM.Estado = @Estado) 
	
END
GO

-----SP_COMENTARIO_OBTENER 0, '', '4', '3'
	

--=================================================================================================================================
-- FECHA DE CREACIÓN		: 26/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_REQUERIMIENTO_MOVIMIENTO_OBTENER_DETALLE
-- PARAMETOS				: @CodRequerimiento
-- FUNCIONALIDAD			: 
--================================================================================================================================
DROP PROCEDURE IF EXISTS dbo.SP_REQUERIMIENTO_MOVIMIENTO_OBTENER_DETALLE; 
GO
CREATE PROCEDURE SP_REQUERIMIENTO_MOVIMIENTO_OBTENER_DETALLE
@CodRequerimiento BIGINT
AS
BEGIN
	SELECT RM.CodRequerimientoMovimiento
		  ,RM.CodRequerimiento
		  ,P.Descripcion Estado
		  ,RQ.FechaEntrega
		  ,RC.FechaRegistro FechaCartaRespuesta
		  ,RM.UsuarioRegistro
		  ,RM.UsuarioModificacion
		  ,RM.FechaRegistro
		  ,RM.FechaModificacion
	FROM   RequerimientoMovimiento RM
		INNER JOIN Requerimiento RQ ON RQ.CodRequerimiento = RM.CodRequerimiento
		LEFT JOIN Parametro P ON P.GRUPO = 100 AND P.PARAMETRO = RM.ESTADO
		LEFT JOIN Recurso RC ON  RC.CodTabla = 2 AND RC.ReferenciaID = RM.CodRequerimientoMovimiento
	
	WHERE	(ISNULL(@CodRequerimiento,0) = 0 OR RM.CodRequerimiento = @CodRequerimiento)   
		 
	GROUP BY
		   RM.CodRequerimientoMovimiento
		  ,RM.CodRequerimiento
		  ,P.Descripcion
		  ,RQ.FechaEntrega
		  ,RC.FechaRegistro
		  ,RM.UsuarioRegistro
		  ,RM.UsuarioModificacion
		  ,RM.FechaRegistro
		  ,RM.FechaModificacion

END
GO

 


--=================================================================================================================================
-- FECHA DE CREACIÓN		: 26/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_REQUERIMIENTO_ASIGNACION_OBTENER
-- PARAMETOS				: @CodRequerimientoMovimiento
-- FUNCIONALIDAD			: 
--================================================================================================================================
DROP PROCEDURE IF EXISTS dbo.SP_REQUERIMIENTO_ASIGNACION_OBTENER; 
GO
CREATE PROCEDURE SP_REQUERIMIENTO_ASIGNACION_OBTENER
@CodRequerimientoAsignacion BIGINT
AS
BEGIN
	SELECT CodRequerimientoAsignacion
	  ,CodRequerimiento
      ,CodRequerimientoMovimiento
      ,UsuarioAsignado
      ,UsuarioRegistro
      ,UsuarioModificacion
      ,FechaRegistro
      ,FechaModificacion
	FROM RequerimientoAsignacion RMA
	WHERE	RMA.CodRequerimientoAsignacion = @CodRequerimientoAsignacion
END
GO

---SP_REQUERIMIENTO_ASIGNACION_OBTENER '13'


--=================================================================================================================================
-- FECHA DE CREACIÓN		: 26/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_REQUERIMIENTO_MOVIMIENTO_OBTENER
-- PARAMETOS				: @CodRequerimientoMovimiento BIGINT, @CodRequerimiento BIGINT, @Estado INT
-- FUNCIONALIDAD			: Query para obtener el estado anterior diferente a pendiente o sin atención
--================================================================================================================================

DROP PROCEDURE IF EXISTS dbo.SP_REQUERIMIENTO_MOVIMIENTO_OBTENER; 
GO
CREATE PROCEDURE SP_REQUERIMIENTO_MOVIMIENTO_OBTENER
@CodRequerimientoMovimiento BIGINT,
@CodRequerimiento BIGINT,
@Estado INT
AS
BEGIN
	 
	SELECT TOP (1) RM.CodRequerimientoMovimiento
		  ,RM.CodRequerimiento
		  ,P.Descripcion Estado
		--  ,RM.UsuarioAsignado
		  ,RM.UsuarioRegistro
		  ,RM.UsuarioModificacion
		  ,RM.FechaRegistro
		  ,RM.FechaModificacion
	FROM   RequerimientoMovimiento RM
		LEFT JOIN Parametro P ON P.GRUPO = 100 AND P.PARAMETRO = RM.ESTADO
	
	WHERE (ISNULL(@CodRequerimientoMovimiento,0) = 0 OR RM.CodRequerimientoMovimiento = @CodRequerimientoMovimiento) AND
		(ISNULL(@CodRequerimiento,0) = 0 OR RM.CodRequerimiento = @CodRequerimiento)  AND
		RM.Estado NOT IN (9,10)
	ORDER BY RM.CodRequerimientoMovimiento DESC

END
GO


--=================================================================================================================================
-- FECHA DE CREACIÓN		: 26/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_REQUERIMIENTO_DETALLE_INSERTAR
-- PARAMETOS				: @TipoDocumento INT, @NroDocumento VARCHAR(20), @PrimerApellido  VARCHAR(200),
--							  @SegundoApellido  VARCHAR(200), @Nombres VARCHAR(200), @RazonSocial VARCHAR(200),
--							  @CodUbigeo VARCHAR(6), @Direccion VARCHAR(200), @Referencia VARCHAR(200), 
--							  @CorreoElectronico VARCHAR(100), @Celular VARCHAR(20), @Telefono VARCHAR(20),
--							  @UsuarioRegistro VARCHAR(50)
-- FUNCIONALIDAD			: Genera un registro en la tabla requerimiento detalle
--================================================================================================================================

DROP PROCEDURE IF EXISTS dbo.SP_REQUERIMIENTO_DETALLE_INSERTAR; 
GO
CREATE PROCEDURE SP_REQUERIMIENTO_DETALLE_INSERTAR
@CodRequerimiento BIGINT,
@TipoDocumento INT,
@NroDocumento VARCHAR(20),
@PrimerApellido  VARCHAR(200),
@SegundoApellido  VARCHAR(200),
@Nombres VARCHAR(200),
@RazonSocial VARCHAR(200),
@CodUbigeo VARCHAR(6),
@Direccion VARCHAR(200),
@Referencia VARCHAR(200),
@CorreoElectronico VARCHAR(100),
@Celular VARCHAR(20),
@Telefono VARCHAR(20),
@UsuarioRegistro VARCHAR(50) 

AS
BEGIN
	 
	INSERT INTO RequerimientoDetalle (CodRequerimiento
		   ,TipoDocumento
           ,NroDocumento
           ,PrimerApellido
           ,SegundoApellido
           ,Nombres
           ,NombresApellidos
           ,RazonSocial
           ,CodUbigeo
           ,Direccion
           ,Referencia
           ,CorreoElectronico
           ,Celular
           ,Telefono
           ,UsuarioRegistro
           ,FechaRegistro)
     VALUES (@CodRequerimiento
		   ,@TipoDocumento
           ,@NroDocumento
           ,@PrimerApellido
           ,@SegundoApellido
           ,@Nombres
           ,CONCAT(@Nombres, ' ', @PrimerApellido, ' ', @SegundoApellido)
           ,@RazonSocial
           ,@CodUbigeo
           ,@Direccion
           ,@Referencia
           ,@CorreoElectronico
           ,@Celular
           ,@Telefono
           ,@UsuarioRegistro
           ,GETDATE())

END
GO



--=================================================================================================================================
-- FECHA DE CREACIÓN		: 26/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: Jose Caycho Garcia
-- STORE PROCEDURE			: SP_REQUERIMIENTO_MOVIMIENTO_INSERTAR
-- PARAMETOS				: @CodRequerimiento BIGINT, @Estado INT, @UsuarioRegistro VARCHAR(50) 
-- FUNCIONALIDAD			:  
--================================================================================================================================

DROP PROCEDURE IF EXISTS dbo.SP_REQUERIMIENTO_MOVIMIENTO_INSERTAR; 
GO
CREATE PROCEDURE SP_REQUERIMIENTO_MOVIMIENTO_INSERTAR
@CodRequerimiento BIGINT,
@Estado INT,
@UsuarioRegistro VARCHAR(50),
@CodRequerimientoMovimiento int OUTPUT

AS
BEGIN

	 INSERT INTO RequerimientoMovimiento (CodRequerimiento,Estado,UsuarioRegistro,FechaRegistro)
								  VALUES (@CodRequerimiento,@Estado,@UsuarioRegistro,GETDATE())

     SELECT @CodRequerimientoMovimiento = SCOPE_IDENTITY()

END
GO



--=================================================================================================================================
-- FECHA DE CREACIÓN		: 26/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: Jose Caycho Garcia
-- STORE PROCEDURE			: SP_REQUERIMIENTO_REGISTRAR
-- PARAMETOS				: @NroRequerimiento VARCHAR(20), @CodOficina  INT, @EsMenor INT,
--							  @CodRequerimientoDetalle BIGINT, @CodProducto INT, @CodMotivo INT,
--							  @CodTipoRequerimiento INT, @CodOrigen INT, @CodFormaEnvio INT,
--							  @Descripcion VARCHAR(1500), @Estado INT, @UsuarioRegistro VARCHAR(50)
-- FUNCIONALIDAD			:  
--================================================================================================================================

DROP PROCEDURE IF EXISTS dbo.SP_REQUERIMIENTO_REGISTRAR; 
GO
CREATE PROCEDURE SP_REQUERIMIENTO_REGISTRAR
@CodRequerimiento BIGINT OUTPUT ,
@NroRequerimiento VARCHAR(20) OUTPUT,
@CodAgencia  INT,
@EsMenor INT,
@CodProducto INT,
@CodMotivo INT,
@CodTipoRequerimiento INT,
@CodOrigen INT,
@CodFormaEnvio INT,
@Descripcion VARCHAR(1500),
@Estado INT,
@UsuarioRegistro VARCHAR(50), 
@TipoDocumento INT,
@NroDocumento VARCHAR(20),
@PrimerApellido  VARCHAR(200),
@SegundoApellido  VARCHAR(200),
@Nombres VARCHAR(200),
@RazonSocial VARCHAR(200) NULL,
@CodUbigeo VARCHAR(6),
@Direccion VARCHAR(200),
@Referencia VARCHAR(200),
@CorreoElectronico VARCHAR(100),
@Celular VARCHAR(20),
@Telefono VARCHAR(20)
AS
BEGIN
	 DECLARE @vNRO_REQUERIMIENTO VARCHAR(20) = ''
	 DECLARE @ABREVIATURA VARCHAR(1) = '' 
	 DECLARE @CORRELATIVO VARCHAR(10) = '0'

 	 SET @ABREVIATURA = (SELECT TOP(1) Referencia   FROM Parametro WHERE Grupo = 600 AND Parametro = @CodTipoRequerimiento)
 	 SET @CORRELATIVO = (SELECT TOP(1) Valor   FROM Parametro WHERE Grupo = 600 AND Parametro = @CodTipoRequerimiento)
 	 SET @CORRELATIVO = CAST((CAST(@CORRELATIVO AS INT) + 1) AS VARCHAR(4))

 	 UPDATE Parametro SET Valor = @CORRELATIVO  WHERE Grupo = 600 AND Parametro = @CodTipoRequerimiento
	 
	 SET @vNRO_REQUERIMIENTO = @ABREVIATURA  + '-'+ RIGHT('0000' + Ltrim(Rtrim(@CORRELATIVO)),4) +'-'+ CAST( YEAR(GETDATE()) AS VARCHAR(4))
	 
	 INSERT INTO Requerimiento (NroRequerimiento
           ,CodAgencia
           ,EsMenor        
           ,CodProducto
           ,CodMotivo
           ,CodTipoRequerimiento
           ,CodOrigen
           ,CodFormaEnvio
           ,Descripcion
           ,Estado
           ,UsuarioRegistro
           ,FechaRegistro)
     VALUES (@vNRO_REQUERIMIENTO
           ,@CodAgencia
           ,@EsMenor         
           ,@CodProducto
           ,@CodMotivo
           ,@CodTipoRequerimiento
           ,@CodOrigen
           ,@CodFormaEnvio
           ,@Descripcion
           ,@Estado
           ,@UsuarioRegistro
		   , GETDATE())
 
 
	SET @CodRequerimiento = @@IDENTITY
	SET @NroRequerimiento = @vNRO_REQUERIMIENTO
	EXEC SP_REQUERIMIENTO_DETALLE_INSERTAR @CodRequerimiento,@TipoDocumento,@NroDocumento,@PrimerApellido,@SegundoApellido,@Nombres,@RazonSocial,@CodUbigeo,@Direccion, @Referencia,@CorreoElectronico,@Celular,@Telefono,@UsuarioRegistro  
	EXEC SP_REQUERIMIENTO_MOVIMIENTO_INSERTAR  @CodRequerimiento,@Estado, @UsuarioRegistro, NULL
	
END
GO

 
-- EXEC SP_REQUERIMIENTO_DETALLE_INSERTAR 1,'70145967','CAYCHO','GARCIA','JOSE JAVIER',NULL,'140101','CALLE LAS AMAPOLAS MZ C LT 20','AL LADO DEL COLEGIO MIXTO','josecaycho_1@hotmail.com','942650751',NULL,'JCAYCHO'
 
 
DROP PROCEDURE IF EXISTS dbo.SP_PARAMETRO_OBTENER; 
GO
---=================================================================================================================================
-- FECHA DE CREACIÓN		: 26/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: SP_PARAMETRO_LISTAR
-- PARAMETROS				: @Grupo
-- FUNCIONALIDAD			: Obtener los parametros
--================================================================================================================================

CREATE PROCEDURE SP_PARAMETRO_OBTENER	
@Grupo INT NULL
As
BEGIN

	SELECT Grupo
		,Parametro
		,Descripcion 
		,Valor
		,Referencia
	FROM Parametro
	WHERE (ISNULL(@Grupo,0)=0 OR Grupo = @Grupo )
		AND Estado = 1
	ORDER BY Orden

END
GO


DROP PROCEDURE IF EXISTS dbo.SP_USUARIO_OBTENER; 
GO
---=================================================================================================================================
-- FECHA DE CREACIÓN		: 26/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: SP_USUARIO_LISTAR
-- PARAMETROS				:							  
-- FUNCIONALIDAD			: Obtener la lista de usuarios
--================================================================================================================================

CREATE PROCEDURE SP_USUARIO_OBTENER	
@Perfil VARCHAR(5),
@UsuarioWeb  VARCHAR(20)

AS
BEGIN

	SELECT US.CodUsuario
		,US.UsuarioWeb
		,US.NombresApellidos
		,Perfil
		,CorreoElectronico
	FROM Usuario US
	WHERE  (US.Perfil = @Perfil
		OR @Perfil = '')
		AND (US.UsuarioWeb = @UsuarioWeb OR @UsuarioWeb = '')
	ORDER BY US.CodUsuario

END
GO



DROP PROCEDURE IF EXISTS dbo.SP_AGENCIA_OBTENER; 
GO
---=================================================================================================================================
-- FECHA DE CREACIÓN		: 26/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: SP_AGENCIA_LISTAR
-- PARAMETROS				:							  
-- FUNCIONALIDAD			: Obtener la lista de agencias
--==================================================================================================================================

CREATE PROCEDURE SP_AGENCIA_OBTENER	

AS
BEGIN

	SELECT CodAgencia
		,Oficina
		,Direccion
	FROM Agencia
	ORDER BY codAgencia

END
GO	


DROP PROCEDURE IF EXISTS dbo.SP_REQUERIMIENTO_OBTENER; 
GO
 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 26/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: SP_REQUERIMIENTO_LISTAR
-- PARAMETROS				: @UsuarioAsignado, @Estado, @CodAgencia, @NroDocumento, @NombresApellidos
--							  @FechaRegistroIni, @FechaRegistroFin, @FechaIniAtencion, @FechaFinAtencion 
-- FUNCIONALIDAD			: Obtener la lista de requerimiento
--================================================================================================================================

CREATE PROCEDURE [dbo].[SP_REQUERIMIENTO_OBTENER]
@UsuarioAsignado VARCHAR(50),
@Estado VARCHAR(10),
@CodAgencia VARCHAR(10),
@NroDocumento VARCHAR(20),
@NombresApellidos VARCHAR(100),
@FechaRegistroIni DATETIME,
@FechaRegistroFin DATETIME,
@FechaAtencionIni DATE,
@FechaAtencionFin DATE

AS
BEGIN

;WITH cte_data(CodRequerimiento, Codigo) AS
	(
			SELECT CodRequerimiento, MAX(CodRequerimientoAsignacion) AS Codigo
			FROM  RequerimientoAsignacion RA			
			GROUP BY CodRequerimiento
)
SELECT	RE.CodRequerimiento
		,RE.NroRequerimiento As NroReclamo
		,RE.FechaRegistro
		,(PR.Descripcion + ' - ' + RD.NroDocumento) AS NroDocumento
		,CASE WHEN RD.TipoDocumento <> 2 THEN (RD.Nombres + ' ' + RD.PrimerApellido + ' ' + RD.SegundoApellido) ELSE RD.RazonSocial END  AS NombreApellidos
		,URA.NombresApellidos As UsuarioAsignado
		,MAX(CASE WHEN RM.Estado = 8 THEN CONVERT(VARCHAR,RM.FechaRegistro,103) ELSE NULL END) AS FechaAtencion		
		,RE.CodTipoRequerimiento
		,PRT.Descripcion AS TipoRequerimiento
		,AG.Oficina AS Agencia
		,PRE.Descripcion AS Estado
	FROM Requerimiento RE		
		INNER JOIN RequerimientoDetalle RD ON RD.CodRequerimiento = RE.CodRequerimiento		
		INNER JOIN Agencia AG  ON AG.CodAgencia = RE.CodAgencia 
		LEFT JOIN Parametro PR ON PR.Parametro = RD.TipoDocumento AND PR.Grupo = 300
		LEFT JOIN Parametro PRE ON PRE.Parametro = RE.Estado AND PRE.Grupo = 100
		LEFT JOIN Parametro PRT ON PRT.Parametro = RE.CodTipoRequerimiento AND PRT.Grupo = 600		
		LEFT JOIN cte_data cte  ON cte.CodRequerimiento = RE.CodRequerimiento 
		LEFT JOIN RequerimientoAsignacion RA  ON RA.CodRequerimiento = RE.CodRequerimiento AND RA.CodRequerimientoAsignacion = cte.Codigo
		LEFT JOIN Usuario URA ON URA.UsuarioWeb = RA.UsuarioAsignado
		LEFT JOIN RequerimientoMovimiento RM ON RM.CodRequerimiento = RE.CodRequerimiento AND RM.Estado = 8 --Estado Atendido
		--INNER JOIN Usuario U
		--ON RA.UsuarioAsignado = U.UsuarioWeb
	WHERE
		((@UsuarioAsignado IS NULL OR @UsuarioAsignado = '') OR RA.UsuarioAsignado =  @UsuarioAsignado)		
		AND ((@Estado IS NULL OR @Estado = '') OR RE.Estado = @Estado)
		AND ((@CodAgencia IS NULL OR @CodAgencia = '') OR RE.CodAgencia = CAST(@CodAgencia AS INT))
		AND ((@NroDocumento IS NULL OR @NroDocumento = '') OR RD.NroDocumento LIKE CONCAT('%', @NroDocumento, '%'))
		AND ((@NombresApellidos IS NULL OR @NombresApellidos = '') OR RD.NombresApellidos LIKE CONCAT('%', @NombresApellidos, '%'))
		AND ((@FechaRegistroIni IS NULL OR @FechaRegistroIni = '') OR CAST(RE.FechaRegistro AS DATE) >= CAST(@FechaRegistroIni AS DATE))
		AND ((@FechaRegistroFin IS NULL OR @FechaRegistroFin = '') OR CAST(RE.FechaRegistro AS DATE) <= CAST(@FechaRegistroFin AS DATE))
		AND ((@FechaAtencionIni IS NULL OR @FechaAtencionIni = '') OR (RM.FechaRegistro IS NOT NULL AND CAST(RM.FechaRegistro AS DATE) >= CAST(@FechaAtencionIni AS DATE)))
		AND ((@FechaAtencionIni IS NULL OR @FechaAtencionIni = '') OR (RM.FechaRegistro IS NOT NULL AND CAST(RM.FechaRegistro AS DATE) <= CAST(@FechaAtencionIni AS DATE)))			
	GROUP BY RE.NroRequerimiento 
		,RE.CodRequerimiento
		,RE.FechaRegistro 
		,PR.Descripcion
		,RD.NroDocumento
		,RD.TipoDocumento
		,RD.Nombres 
		,RD.RazonSocial
		,RD.PrimerApellido
		,RD.SegundoApellido
		,URA.NombresApellidos		
		,RE.CodTipoRequerimiento
		,PRT.Descripcion
		,AG.Oficina
		,PRE.Descripcion
	ORDER BY RE.FechaRegistro ASC, RE.NroRequerimiento

END
GO

---EXEC SP_REQUERIMIENTO_OBTENER NULL,'','','','','','','',''


DROP PROCEDURE IF EXISTS dbo.SP_REQUERIMIENTO_BUSCAR_BLOQUEADO; 
GO
---=================================================================================================================================
-- FECHA DE CREACIÓN		: 28/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: SP_REQUERIMIENTO_BUSCAR_BLOQUEADO
-- PARAMETROS				: @CodRequerimiento
-- FUNCIONALIDAD			: Obtener el indicador  si otro usuario se encuentre revisando el requerimiento seleccionado.
--==================================================================================================================================

CREATE PROCEDURE SP_REQUERIMIENTO_BUSCAR_BLOQUEADO	
@NroRequerimiento VARCHAR(20)

AS

BEGIN

	SELECT CASE WHEN ISNULL(UsuarioBloqueo,null) = '' THEN UsuarioBloqueo ELSE UsuarioBloqueo END AS UsuarioBloqueo	
		,EstaBloqueado AS EstaBloqueado
		,CodTipoRequerimiento
		,CodRequerimiento
	FROM Requerimiento
	WHERE NroRequerimiento = @NroRequerimiento

END
GO
---EXEC SP_REQUERIMIENTO_BUSCAR_BLOQUEADO 'R-0006-2021'


DROP PROCEDURE IF EXISTS dbo.SP_REQUERIMIENTO_ACTUALIZAR_BLOQUEADO; 
GO
---==============================================================================================================================================
-- FECHA DE CREACIÓN		: 28/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: SP_REQUERIMIENTO_ACTUALIZAR_BLOQUEADO
-- PARAMETROS				: @CodRequerimiento, @UsuarioRegistro
-- FUNCIONALIDAD			: Actualiza el campo EstaBloqueado  para indicar que otro usuario se encuentre revisando el requerimiento seleccionado.
--                            0 = Sin Bloqueo , 1 = Bloqueado 
--================================================================================================================================================

CREATE PROCEDURE SP_REQUERIMIENTO_ACTUALIZAR_BLOQUEADO	
@Nrorequerimiento VarChar(20),
@UsuarioRegistro VARCHAR(50),
@EstaBloqueado BIT

AS

BEGIN
	 
	UPDATE Requerimiento
		SET UsuarioBloqueo = @UsuarioRegistro,
			EstaBloqueado = @EstaBloqueado
	WHERE NroRequerimiento = @Nrorequerimiento

END
GO


DROP PROCEDURE IF EXISTS dbo.SP_REQUERIMIENTO_UPDATE_ESTADO; 
GO
---=================================================================================================================================
-- FECHA DE CREACIÓN		: 28/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: SP_REQUERIMIENTO_UPDATE_ESTADO
-- PARAMETROS				: @CodRequerimiento, @Estado
-- FUNCIONALIDAD			: Actualizar el estado del requerimiento.
--==================================================================================================================================

CREATE PROCEDURE SP_REQUERIMIENTO_UPDATE_ESTADO	
@CodRequerimiento BIGINT,
@Estado INT

AS

BEGIN

	UPDATE Requerimiento
		SET Estado = @Estado	
	WHERE CodRequerimiento = @CodRequerimiento

END
GO

--=================================================================================================================================
-- FECHA DE CREACIÓN		: 28/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_RECURSO_INSERTAR
-- PARAMETOS				: @CodTabla INT, @ReferenciaID INT, @LaserficheID INT, 
--							  @Descripcion VARCHAR(50), @Estado BIT, @UsuarioRegistro VARCHAR(50)						  
-- FUNCIONALIDAD			: Genera un registro en la tabla recurso
--================================================================================================================================
DROP PROCEDURE IF EXISTS dbo.SP_RECURSO_INSERTAR; 
GO
CREATE PROCEDURE SP_RECURSO_INSERTAR	
@CodTabla INT,
@ReferenciaID INT,
@LaserficheID INT,
@Descripcion VARCHAR(50),
@Estado BIT,
@UsuarioRegistro VARCHAR(50)
AS
BEGIN

	INSERT INTO Recurso 
		   (CodTabla
           ,ReferenciaID
           ,LaserficheID
           ,Descripcion
           ,Estado
           ,UsuarioRegistro
           ,FechaRegistro
           )
     VALUES
           (@CodTabla
           ,@ReferenciaID
           ,@LaserficheID
           ,@Descripcion
           ,@Estado
           ,@UsuarioRegistro
           ,GETDATE())
		
END
GO

--=================================================================================================================================
-- FECHA DE CREACIÓN		: 28/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_CONSULTA_INSERTAR
-- PARAMETOS				: @TipoDocumento INT, @NroDocumento VARCHAR(20), @NombresCompletos VARCHAR(200),
--							  @Direccion VARCHAR(200), @CorreoElectronico VARCHAR(100), @Celular VARCHAR(20),
--							  @Descripcion VARCHAR(1000), @Estado BIT, @UsuarioRegistro VARCHAR(50)						  
-- FUNCIONALIDAD			: Genera un registro en la tabla consulta
--================================================================================================================================
DROP PROCEDURE IF EXISTS dbo.SP_CONSULTA_INSERTAR; 
GO
CREATE PROCEDURE SP_CONSULTA_INSERTAR	
@TipoDocumento INT,
@NroDocumento VARCHAR(20),
@NombresCompletos VARCHAR(200),
@Direccion VARCHAR(200),
@CorreoElectronico VARCHAR(100),
@Celular VARCHAR(20),
@Descripcion VARCHAR(1000),
@Estado BIT,
@UsuarioRegistro VARCHAR(50)
AS
BEGIN

	INSERT INTO Consulta
           (TipoDocumento
           ,NroDocumento
           ,NombresCompletos
           ,Direccion
           ,CorreoElectronico
           ,Celular
           ,Descripcion
           ,Estado
           ,UsuarioRegistro
           ,FechaRegistro
           )
     VALUES
           (@TipoDocumento
           ,@NroDocumento
           ,@NombresCompletos
           ,@Direccion
           ,@CorreoElectronico
           ,@Celular
           ,@Descripcion
           ,@Estado
           ,@UsuarioRegistro
           ,GETDATE())
		
END
GO
 
GO


DROP PROCEDURE IF EXISTS dbo.SP_COMENTARIO_INSERTAR; 
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 28/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WIILIAM QUIROZ
-- STORE PROCEDURE			: SP_COMENTARIO_INSERTAR
-- PARAMETOS				: @CodRequerimiento BIGINT,@CodRequerimientoMovimiento BIGINT,@CodMotivo INT, 
--							  @CodArea INT,@Descripcion VARCHAR(300),@FechaSolicitud DATE,@UsuarioRegistro VARCHAR(50)						  
-- FUNCIONALIDAD			: Genera un registro en la tabla comentario
--=================================================================================================================================

CREATE PROCEDURE SP_COMENTARIO_INSERTAR	
@CodRequerimiento BIGINT,
@Estado INT,
@CodMotivo INT,
@CodArea INT,
@Descripcion VARCHAR(300),
@FechaSolicitud DATE,
@UsuarioRegistro VARCHAR(50),
@CodComentario INT OUTPUT

AS
BEGIN
	DECLARE @EXISTE_COMENTARIO INT = 0
	DECLARE @EXISTE_MOVIMIENTO BIGINT = 0

	DECLARE @vCodRequerimientoMovimiento BIGINT = 0

	SET @EXISTE_COMENTARIO = (SELECT COUNT(CodComentario) FROM Comentario WHERE CodRequerimiento = @CodRequerimiento)

	IF @EXISTE_COMENTARIO = 0
	BEGIN
			INSERT INTO RequerimientoMovimiento (CodRequerimiento,Estado,UsuarioRegistro,FechaRegistro)
								  VALUES (@CodRequerimiento,4,@UsuarioRegistro,GETDATE())

			SET @vCodRequerimientoMovimiento = SCOPE_IDENTITY()

			UPDATE Requerimiento SET
			Estado = 4
			WHERE CodRequerimiento = @CodRequerimiento
	END

	IF @EXISTE_COMENTARIO>0 
	BEGIN 
		SET @vCodRequerimientoMovimiento = (SELECT CodRequerimientoMovimiento FROM RequerimientoMovimiento WHERE CodRequerimiento = @CodRequerimiento AND Estado = @Estado)
		 
	END

	INSERT INTO Comentario 
		   (CodRequerimiento
           ,CodRequerimientoMovimiento
           ,CodMotivo
           ,CodArea
           ,Descripcion
		   ,FechaSolicitud
           ,UsuarioRegistro
           ,FechaRegistro
           )
     VALUES
           (@CodRequerimiento
           ,@vCodRequerimientoMovimiento
           ,@CodMotivo
           ,@CodArea
           ,@Descripcion
		   ,@FechaSolicitud
           ,@UsuarioRegistro
           ,GETDATE())

	SELECT @CodComentario = SCOPE_IDENTITY()
		
END
GO
 
 
DROP PROCEDURE IF EXISTS dbo.SP_REQUERIMIENTO_OBTENER_DETALLE
GO
---=================================================================================================================================
-- FECHA DE CREACIÓN		: 30/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: SP_REQUERIMIENTO_OBTENER_DETALLE
-- PARAMETOS				: @CodRequerimiento
-- FUNCIONALIDAD			: 
--==================================================================================================================================

CREATE PROCEDURE SP_REQUERIMIENTO_OBTENER_DETALLE
@NroReclamo VARCHAR(20)
AS
BEGIN

;WITH cte_data(CodRequerimiento, Codigo) AS
	(
			SELECT CodRequerimiento, MAX(CodRequerimientoAsignacion) AS Codigo
			FROM  RequerimientoAsignacion RA			
			GROUP BY CodRequerimiento
)

	SELECT RE.CodAgencia
		  ,AG.Oficina
		  ,AG.Direccion AS DireccionAgencia	
		  , RE.EsMenor
		  ,RM.CodRequerimiento
		  ,RE.NroRequerimiento		 
		  ,MAX(RM.CodRequerimientoMovimiento) AS CodRequerimientoMovimiento	
		  ,MAX(RA.CodRequerimientoAsignacion) AS CodRequerimientoAsignacion
		  ,RD.TipoDocumento
		  ,PTI.Descripcion TipoDocumentoDescripcion
		  ,RD.NroDocumento 
		  ,RD.Nombres
		  ,RD.PrimerApellido
		  ,RD.SegundoApellido
		  ,RD.NombresApellidos
		  ,RD.RazonSocial
		  ,RD.CodUbigeo		
		  ,RD.Direccion
		  ,RD.Referencia
		  ,RD.CorreoElectronico
		  ,RD.Telefono
		  ,RD.Celular
		  ,RE.CodMotivo
		  ,PRM.Descripcion AS Motivo
		  ,PRT.Descripcion AS TipoReclamo
		  ,RE.Descripcion AS DecripcionReclamo
		  ,RE.CodTipoRequerimiento
		  ,RE.CodProducto
		  ,PRP.Descripcion AS Producto
		  ,RM.UsuarioRegistro
		  ,UB.CodDepartamento
		  ,UB.Departamento
		  ,UB.CodProvincia
		  ,UB.Provincia
		  ,UB.CodDistrito
		  ,UB.Distrito
		  ,RE.Estado
		  ,PRE.Descripcion As DescripcionEstado
		  ,PTR.Descripcion As FormaEnvio
		  ,PRO.Descripcion As Origen		
		  ,RE.FechaRegistro,
		  URA.NombresApellidos UsuarioAsignado
	FROM   Requerimiento RE
		INNER JOIN RequerimientoDetalle RD ON RD.CodRequerimiento = RE.CodRequerimiento		
		INNER JOIN RequerimientoMovimiento RM ON RM.CodRequerimiento = RE.CodRequerimiento
		LEFT JOIN Agencia AG ON AG.CodAgencia = RE.CodAgencia
		LEFT JOIN Parametro PRE ON PRE.Parametro = RE.Estado AND PRE.Grupo = 100
		LEFT JOIN Parametro PRP ON PRP.Parametro = RE.CodProducto AND PRP.Grupo = 400
		LEFT JOIN Parametro PRM ON PRM.Parametro = RE.CodMotivo AND PRM.Grupo = 500
		LEFT JOIN Parametro PRT ON PRT.Parametro = RE.CodTipoRequerimiento AND PRT.Grupo = 600	
		LEFT JOIN Parametro PTI ON PTI.Parametro = RD.TipoDocumento AND PTI.Grupo = 300	
		LEFT JOIN Parametro PTR ON PTR.Parametro = RE.CodFormaEnvio AND PTR.Grupo = 700	
		LEFT JOIN Parametro PRO ON PRO.Parametro = RE.CodOrigen AND PRO.Grupo = 900
		LEFT JOIN UBIGEO UB ON UB.CodUbigeo = RD.CodUbigeo	
		LEFT JOIN cte_data cte ON cte.CodRequerimiento = RE.CodRequerimiento 
		LEFT JOIN RequerimientoAsignacion RA 	ON RA.CodRequerimiento = RE.CodRequerimiento AND RA.CodRequerimientoAsignacion = cte.Codigo
		LEFT JOIN Usuario URA ON RA.UsuarioAsignado = URA.UsuarioWeb
	WHERE	(ISNULL(@NroReclamo,'') = '' OR RE.NroRequerimiento = @NroReclamo)   
	AND RE.Estado = RM.Estado	
	AND RM.CodRequerimientoMovimiento = (SELECT MAX(CodRequerimientoMovimiento) AS Codigo
										 FROM  RequerimientoMovimiento RM where RM.CodRequerimiento = RE.CodRequerimiento)
	
	GROUP BY
		   RE.CodAgencia
		  ,AG.Oficina
		  ,AG.Direccion 	
		  , RE.EsMenor
		  ,RM.CodRequerimiento
		  ,RE.NroRequerimiento		 
		  ,RM.CodRequerimientoMovimiento
		  ,RD.TipoDocumento
		  ,PTI.Descripcion
		  ,RD.NroDocumento
		  ,RD.Nombres
		  ,RD.PrimerApellido
		  ,RD.SegundoApellido
		  ,RD.NombresApellidos
		  ,RD.RazonSocial
		  ,RD.CodUbigeo		  
		  ,RD.Direccion
		  ,RD.Referencia
		  ,RD.CorreoElectronico
		  ,RD.Telefono
		  ,RD.Celular
		  ,RE.CodMotivo
		  ,PRM.Descripcion
		  ,PRT.Descripcion
		  ,RE.Descripcion
		  ,RE.CodProducto
		  ,PRP.Descripcion
		  ,RE.CodTipoRequerimiento
		  ,RM.UsuarioRegistro
		  ,UB.CodDepartamento
		  ,UB.Departamento
		  ,UB.CodProvincia
		  ,UB.Provincia
		  ,UB.CodDistrito
		  ,UB.Distrito
		  ,RE.Estado
		  ,PRE.Descripcion
		  ,PRP.Descripcion
		  ,PTR.Descripcion
		  ,PRO.Descripcion
		  ,RE.FechaRegistro
		  ,RA.CodRequerimientoAsignacion
		  , URA.NombresApellidos

END
GO

---EXEC SP_REQUERIMIENTO_OBTENER_DETALLE 'R-0006-2021'

--=================================================================================================================================
-- FECHA DE CREACIÓN		: 30/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_UBIGEO_OBTENER_DEPARTAMENTO
-- PARAMETOS				:  
-- FUNCIONALIDAD			: 
--================================================================================================================================
DROP PROCEDURE IF EXISTS dbo.SP_UBIGEO_OBTENER_DEPARTAMENTO; 
GO
CREATE PROCEDURE SP_UBIGEO_OBTENER_DEPARTAMENTO
 
AS
BEGIN
	 SELECT CodDepartamento
		, Departamento
	 FROM Ubigeo
	 GROUP BY 
		  CodDepartamento
		, Departamento

END
GO


--=================================================================================================================================
-- FECHA DE CREACIÓN		: 30/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_UBIGEO_OBTENER_PROVINCIA
-- PARAMETOS				: @CodDepartamento VARCHAR(2)
-- FUNCIONALIDAD			: 
--================================================================================================================================
DROP PROCEDURE IF EXISTS dbo.SP_UBIGEO_OBTENER_PROVINCIA

GO
CREATE PROCEDURE SP_UBIGEO_OBTENER_PROVINCIA
 @CodDepartamento VARCHAR(2)
AS
BEGIN
	 SELECT CodProvincia
		, Provincia
	 FROM Ubigeo
	 WHERE CodDepartamento = @CodDepartamento
	 GROUP BY 
		  CodProvincia
		, Provincia

END
GO


--=================================================================================================================================
-- FECHA DE CREACIÓN		: 30/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_UBIGEO_OBTENER_DISTRITO
-- PARAMETOS				: @CodDepartamento VARCHAR(2) ,  @CodProvincia VARCHAR(2)
-- FUNCIONALIDAD			: 
--================================================================================================================================
DROP PROCEDURE IF EXISTS dbo.SP_UBIGEO_OBTENER_DISTRITO

GO
CREATE PROCEDURE SP_UBIGEO_OBTENER_DISTRITO
 @CodDepartamento VARCHAR(2),
 @CodProvincia VARCHAR(2)
AS
BEGIN
	 SELECT CodUbigeo
		, Distrito
	 FROM Ubigeo
	 WHERE CodDepartamento = @CodDepartamento
		AND CodProvincia = @CodProvincia
	 GROUP BY 
		  CodUbigeo
		, Distrito

END
GO


DROP PROCEDURE IF EXISTS dbo.SP_REQUERIMIENTO_ASIGNACION_INSERTAR; 
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 30/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: SP_REQUERIMIENTO_ASIGNACION_OBTENER
-- PARAMETOS				: @CodRequerimientoMovimiento
-- FUNCIONALIDAD			: 
--================================================================================================================================

CREATE PROCEDURE SP_REQUERIMIENTO_ASIGNACION_INSERTAR
@CodRequerimiento BIGINT,
@Estado INT,
@UsuarioAsignado VARCHAR(50),
@UsuarioRegistro VARCHAR(50)
AS
BEGIN
	DECLARE @EXISTE_USUARIO_ASIGNADO INT = 0
	DECLARE @EXITE_MOVIMIENTO INT = 0

	DECLARE @vCodRequerimientoMovimiento BIGINT = 0

	SET @EXISTE_USUARIO_ASIGNADO = (SELECT COUNT(CodRequerimientoAsignacion) FROM RequerimientoAsignacion WHERE CodRequerimiento = @CodRequerimiento)
	
	IF @EXISTE_USUARIO_ASIGNADO=0
	BEGIN
		INSERT INTO RequerimientoMovimiento (CodRequerimiento,Estado,UsuarioRegistro,FechaRegistro)
								  VALUES (@CodRequerimiento,3,@UsuarioRegistro,GETDATE())
								  
		SET @vCodRequerimientoMovimiento = SCOPE_IDENTITY()

		UPDATE Requerimiento SET 
		Estado = 3
		WHERE CodRequerimiento = @CodRequerimiento
			
	END

	IF @EXISTE_USUARIO_ASIGNADO>0
	BEGIN
		SET @EXITE_MOVIMIENTO = (SELECT COUNT(CodRequerimientoMovimiento) FROM RequerimientoMovimiento WHERE CodRequerimiento = @CodRequerimiento AND Estado = @Estado)
		IF @EXITE_MOVIMIENTO = 0 
		BEGIN
		INSERT INTO RequerimientoMovimiento (CodRequerimiento,Estado,UsuarioRegistro,FechaRegistro)
								  VALUES (@CodRequerimiento,@Estado,@UsuarioRegistro,GETDATE())
								   
			SET @vCodRequerimientoMovimiento = SCOPE_IDENTITY()
		END
		IF @EXITE_MOVIMIENTO > 0 
		BEGIN
			SET @vCodRequerimientoMovimiento = (SELECT CodRequerimientoMovimiento FROM RequerimientoMovimiento WHERE CodRequerimiento = @CodRequerimiento AND Estado = @Estado)
		END
		
	END
	

    

	INSERT INTO RequerimientoAsignacion
		   (CodRequerimiento
           ,CodRequerimientoMovimiento
           ,UsuarioAsignado
           ,UsuarioRegistro
           ,UsuarioModificacion
		   ,FechaRegistro
		   ,FechaModificacion
           )
     VALUES
           (@CodRequerimiento
			,@vCodRequerimientoMovimiento			
			,@UsuarioAsignado
			,@UsuarioRegistro 
			,NULL
           ,GETDATE()
		   ,NULL)
		
END
GO

 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 26/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_PARAMETRO_OBTENER_GRUPO
-- PARAMETROS				: @Grupo
-- FUNCIONALIDAD			: Obtener los parametros por grupos
--================================================================================================================================
DROP PROCEDURE IF EXISTS dbo.SP_PARAMETRO_OBTENER_GRUPO; 
GO
 CREATE PROCEDURE SP_PARAMETRO_OBTENER_GRUPO	
@Grupo VARCHAR(50) = NULL

AS
BEGIN

	SELECT Grupo
		,Parametro
		,Descripcion 
		,Valor
		,Referencia
	FROM Parametro
	WHERE Grupo in (select value from STRING_SPLIT(@grupo,','))
		AND Estado = 1
	ORDER BY Grupo,Orden

END
GO


DROP PROCEDURE IF EXISTS dbo.SP_REQUERIMIENTO_MOVIMIENTO_PENULTIMO; 
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 30/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: SP_REQUERIMIENTO_MOVIMIENTO_PENULTIMO
-- PARAMETOS				: @CodRequerimiento
-- FUNCIONALIDAD			: Obtener el estado del penultimo registro 
--================================================================================================================================

CREATE PROCEDURE SP_REQUERIMIENTO_MOVIMIENTO_PENULTIMO
@CodRequerimiento BIGINT
AS
BEGIN
	SELECT TOP 1 RM.Estado 
	FROM RequerimientoMovimiento RM	
	WHERE RM.CodRequerimiento=@CodRequerimiento 
		AND RM.Estado NOT IN(9 ,10) 
	ORDER BY RM.CodRequerimientoMovimiento DESC

END
GO

DROP PROCEDURE IF EXISTS dbo.SP_RECURSO_OBTENER_IMAGENES; 
GO
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 30/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: SP_RECURSO_OBTENER_IMAGENES
-- PARAMETOS				: @CodRequerimiento
-- FUNCIONALIDAD			: Obtener el estado del penultimo registro 
--================================================================================================================================

CREATE PROCEDURE SP_RECURSO_OBTENER_IMAGENES	
@CodRequerimiento BIGINT
AS
BEGIN

	SELECT R.CodTabla, R.ReferenciaID, R.LaserficheID FROM Recurso R
	INNER JOIN Requerimiento RE ON R.ReferenciaID = RE.CodRequerimiento
	LEFT JOIN Parametro PR ON PR.CodParametro = R.CodTabla
	WHERE R.ReferenciaID = @CodRequerimiento 
		
END

GO

DROP PROCEDURE IF EXISTS dbo.SP_RECURSO_OBTENER; 
GO

---SP_RECURSO_OBTENER_IMAGENES '1'
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 30/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_RECURSO_OBTENER
-- PARAMETOS				: @CodRequerimiento
-- FUNCIONALIDAD			: Obtener el estado del penultimo registro 
--================================================================================================================================

CREATE PROCEDURE SP_RECURSO_OBTENER	
@CodTabla BIGINT,
@ReferenciaID BIGINT,
@LaserficheID INT
AS
BEGIN

	SELECT 
		R.CodTabla, 
		R.ReferenciaID, 
		R.LaserficheID,
		R.Descripcion
	FROM Recurso R
	-- INNER JOIN Requerimiento RE ON R.ReferenciaID = RE.CodRequerimiento
	LEFT JOIN Parametro PR ON PR.CodParametro = R.CodTabla
	WHERE R.CodTabla = @CodTabla AND
	(@ReferenciaID IS NULL OR R.ReferenciaID = @ReferenciaID) AND
	(@LaserficheID IS NULL OR R.LaserficheID = @LaserficheID)
		
END
GO

-- exec SP_RECURSO_OBTENER 7, 3, null


DROP PROCEDURE IF EXISTS dbo.SP_AREA_OBTENER; 
GO
---=================================================================================================================================
-- FECHA DE CREACIÓN		: 30/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: SP_AREA_OBTENER
-- PARAMETROS				:							  
-- FUNCIONALIDAD			: Obtener lista de las areas
--================================================================================================================================

CREATE PROCEDURE SP_AREA_OBTENER	

AS
BEGIN

	SELECT A.Codigo
		,A.Descripcion	
	FROM Area A
END
GO

DROP PROCEDURE IF EXISTS dbo.SP_RECURSO_ELIMINAR; 
GO

---SP_RECURSO_ELIMINAR ''
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 30/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: SP_RECURSO_ELIMINAR
-- PARAMETOS				: @LaserficheID
-- FUNCIONALIDAD			: Eliminar  registro de recurso 
--================================================================================================================================

CREATE PROCEDURE SP_RECURSO_ELIMINAR
@LaserficheID INT
AS
BEGIN

	DELETE
	FROM Recurso
	WHERE LaserficheID= @LaserficheID	
			
END
GO




 

DROP PROCEDURE IF EXISTS SP_REQUERIMIENTO_DERIVADO_A_OCM_INSERTAR
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 30/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_REQUERIMIENTO_DERIVADO_A_OCM_INSERTAR
-- PARAMETOS				: @CodRequerimientoMovimiento
-- FUNCIONALIDAD			: 
--================================================================================================================================
GO

CREATE PROCEDURE SP_REQUERIMIENTO_DERIVADO_A_OCM_INSERTAR
@CodRequerimientoMovimiento int OUTPUT,
@CodRequerimiento BIGINT,
@Estado INT,
@UsuarioRegistro VARCHAR(50)

AS
BEGIN
	 INSERT INTO RequerimientoMovimiento (CodRequerimiento,Estado,UsuarioRegistro,FechaRegistro)
								  VALUES (@CodRequerimiento,@Estado,@UsuarioRegistro,GETDATE())

     SELECT @CodRequerimientoMovimiento = SCOPE_IDENTITY()


	 UPDATE Requerimiento SET 
	 Estado = @Estado,
	 UsuarioRegistro = @UsuarioRegistro,
	 FechaModificacion = GETDATE()
	 WHERE CodRequerimiento = @CodRequerimiento
		
END
GO


DROP PROCEDURE IF EXISTS SP_REQUERIMIENTO_DERIVAR_PARA_SU_NOTIFICACION_INSERTAR
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 24/05/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_REQUERIMIENTO_DERIVAR_PARA_SU_NOTIFICACION_INSERTAR
-- PARAMETOS				: @CodRequerimientoMovimiento int OUTPUT,
--							  @CodRequerimiento BIGINT,
--							  @Estado INT,
--							  @UsuarioAsignado VARCHAR(50),
--							  @UsuarioRegistro VARCHAR(50)
-- FUNCIONALIDAD			: 
--================================================================================================================================
GO

CREATE PROCEDURE SP_REQUERIMIENTO_DERIVAR_PARA_SU_NOTIFICACION_INSERTAR
@CodRequerimientoMovimiento int OUTPUT,
@CodRequerimiento BIGINT,
@Estado INT,
@UsuarioAsignado VARCHAR(50),
@UsuarioRegistro VARCHAR(50)

AS
BEGIN
	 INSERT INTO RequerimientoMovimiento (CodRequerimiento,Estado,UsuarioRegistro,FechaRegistro)
								  VALUES (@CodRequerimiento,@Estado,@UsuarioRegistro,GETDATE())

     SELECT @CodRequerimientoMovimiento = SCOPE_IDENTITY()

	 INSERT INTO RequerimientoAsignacion (CodRequerimiento,CodRequerimientoMovimiento,UsuarioAsignado,UsuarioRegistro,FechaRegistro) VALUES
										(@CodRequerimiento,@CodRequerimientoMovimiento,@UsuarioAsignado,@UsuarioRegistro,GETDATE())
	 
	 UPDATE Requerimiento SET 
	 Estado = @Estado,
	 UsuarioModificacion = @UsuarioRegistro,
	 FechaModificacion = GETDATE()
	 WHERE CodRequerimiento = @CodRequerimiento
		
END
GO


DROP PROCEDURE IF EXISTS SP_REQUERIMIENTO_APROBAR_INSERTAR
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 24/05/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_REQUERIMIENTO_APROBAR_INSERTAR
-- PARAMETOS				: @CodRequerimientoMovimiento int OUTPUT,
--							  @CodRequerimiento BIGINT,
--							  @Estado INT,
--							  @UsuarioRegistro VARCHAR(50)
-- FUNCIONALIDAD			: 
--================================================================================================================================
GO

CREATE PROCEDURE SP_REQUERIMIENTO_APROBAR_INSERTAR
@CodRequerimientoMovimiento int OUTPUT,
@CodRequerimiento BIGINT,
@Estado INT,
@UsuarioRegistro VARCHAR(50)

AS
BEGIN

	 INSERT INTO RequerimientoMovimiento (CodRequerimiento,Estado,UsuarioRegistro,FechaRegistro)
								  VALUES (@CodRequerimiento,@Estado,@UsuarioRegistro,GETDATE())

     SELECT @CodRequerimientoMovimiento = SCOPE_IDENTITY()

	 UPDATE Requerimiento SET 
	 Estado = @Estado,
	 UsuarioModificacion = @UsuarioRegistro,
	 FechaModificacion = GETDATE()
	 WHERE CodRequerimiento = @CodRequerimiento
END
GO



DROP PROCEDURE IF EXISTS SP_REQUERIMIENTO_ATENDIDO_INSERTAR
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 24/05/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_REQUERIMIENTO_ATENDIDO_INSERTAR
-- PARAMETOS				: @CodRequerimientoMovimiento int OUTPUT,
--							  @CodRequerimiento BIGINT,
--							  @Estado INT,
--							  @UsuarioRegistro VARCHAR(50)
--							  @FechaEntrega DATETIME
-- FUNCIONALIDAD			: 
--================================================================================================================================
GO

CREATE PROCEDURE SP_REQUERIMIENTO_ATENDIDO_INSERTAR
@CodRequerimientoMovimiento int OUTPUT,
@CodRequerimiento BIGINT,
@Estado INT,
@UsuarioRegistro VARCHAR(50),
@FechaEntrega DATETIME
AS
BEGIN

	 INSERT INTO RequerimientoMovimiento (CodRequerimiento,Estado,UsuarioRegistro,FechaRegistro)
								  VALUES (@CodRequerimiento,@Estado,@UsuarioRegistro,GETDATE())

     SELECT @CodRequerimientoMovimiento = SCOPE_IDENTITY()

	 UPDATE Requerimiento SET 
	 Estado = @Estado,
	 UsuarioModificacion = @UsuarioRegistro,
	 FechaModificacion = GETDATE(),
	 FechaEntrega = @FechaEntrega
	 WHERE CodRequerimiento = @CodRequerimiento
END
GO

DROP PROCEDURE IF EXISTS dbo.SP_PLANTILLA_OBTNER; 
GO

 
--=================================================================================================================================
-- FECHA DE CREACIÓN		: 30/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_PLANTILLA_OBTNER
-- PARAMETOS				: @CodTabla INT,
--							  @CodTipoPlantilla INT
-- FUNCIONALIDAD			:  
--================================================================================================================================

CREATE PROCEDURE SP_PLANTILLA_OBTNER	
@CodTabla INT,
@CodTipoPlantilla VARCHAR(50)
AS
BEGIN
    SELECT 
		R.CodTabla, 
		R.ReferenciaID, 
		R.LaserficheID,
		R.Descripcion
	FROM Recurso R
		INNER JOIN Plantilla P ON P.CodPlantilla = R.ReferenciaID
	WHERE R.CodTabla = @CodTabla  AND 
		P.CodTipoPlantilla in  (select value from STRING_SPLIT(@CodTipoPlantilla,','))
	GROUP BY
		R.CodTabla, 
		R.ReferenciaID, 
		R.LaserficheID,
		R.Descripcion
END
GO