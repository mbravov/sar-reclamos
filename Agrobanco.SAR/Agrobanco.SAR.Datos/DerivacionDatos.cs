﻿using Agrobanco.SAR.Datos.SqlServer.Acceso;
using Agrobanco.SAR.Entidades.CNA_Minuta;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SAR.Datos
{
    public class DerivacionDatos
    {
        public string cadenaConexion;
        public async Task<bool> Insertar(DerivacionBE request)
        {
            bool resultado = false;
            
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("SP_DERIVACION_INSERTAR", conn))
                    {

                        try
                        {
                            
                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.Add("@IdEmision", SqlDbType.Int).Value = request.IdEmision;
                            cmd.Parameters.Add("@Perfil", SqlDbType.VarChar, 5).Value = request.Perfil;
                            cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = request.Estado;
                            cmd.Parameters.Add("@PerfilDerivado", SqlDbType.VarChar, 5).Value = DBNull.Value;
                            cmd.Parameters.Add("@CodUsuarioDerivado", SqlDbType.Int).Value = request.CodUsuarioDerivado;
                            cmd.Parameters.Add("@FechaDerivado", SqlDbType.DateTime).Value = DBNull.Value;
                            cmd.Parameters.Add("@CodAgencia", SqlDbType.Int).Value = request.CodAgencia;
                            cmd.Parameters.Add("@UsuarioRegistro", SqlDbType.VarChar, 50).Value = request.UsuarioRegistro;
                          
                            
                            //SqlParameter pIdComentarioEmision = new SqlParameter("@IdComentarioEmision", SqlDbType.Int);
                            //pIdComentarioEmision.Direction = ParameterDirection.Output;
                            //cmd.Parameters.Add(pIdComentarioEmision);

                            await cmd.ExecuteNonQueryAsync();

                            resultado = true;

                        }

                        catch (Exception ex)
                        {

                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            resultado = false;
                            throw (ex);
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }


                    }
                }
            }
            catch (Exception ex)
            {
                resultado = false;
                throw ex;
            }
            return await Task.Run(() => resultado);
        }
        public async Task<bool> Actualizar(DerivacionBE request)
        {
            bool resultado = false;

            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("SP_DERIVACION_ACTUALIZAR", conn))
                    {

                        try
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@IdDerivacion", SqlDbType.Int).Value = request.IdDerivacion;
                            cmd.Parameters.Add("@IdEmision", SqlDbType.Int).Value = request.IdEmision;
                            cmd.Parameters.Add("@Perfil", SqlDbType.VarChar, 5).Value = request.Perfil;
                            cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = request.Estado;
                            cmd.Parameters.Add("@PerfilDerivado", SqlDbType.VarChar, 5).Value = request.PerfilDerivado;
                            cmd.Parameters.Add("@FechaDerivado", SqlDbType.DateTime).Value = DateTime.Now;
                            cmd.Parameters.Add("@CodAgencia", SqlDbType.Int).Value = request.CodAgencia;
                            cmd.Parameters.Add("@UsuarioModificacion", SqlDbType.VarChar, 50).Value = request.UsuarioModificacion;
                            cmd.Parameters.Add("@IdConfiguracionDerivacion", SqlDbType.Int).Value = request.IdConfiguracionDerivacion;
                            cmd.Parameters.Add("@CodUsuarioDerivado", SqlDbType.Int).Value = request.CodUsuarioDerivado;
                            //SqlParameter pIdComentarioEmision = new SqlParameter("@IdComentarioEmision", SqlDbType.Int);
                            //pIdComentarioEmision.Direction = ParameterDirection.Output;
                            //cmd.Parameters.Add(pIdComentarioEmision);

                            await cmd.ExecuteNonQueryAsync();

                            resultado = true;

                        }

                        catch (Exception ex)
                        {

                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            resultado = false;
                            throw (ex);
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }


                    }
                }
            }
            catch (Exception ex)
            {
                resultado = false;
                throw ex;
            }
            return await Task.Run(() => resultado);
        }
    }
}
