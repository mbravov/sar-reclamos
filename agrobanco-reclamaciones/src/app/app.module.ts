import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RequerimientoComponent } from './components/requerimiento/requerimiento.component';
import { PreguntasFrecuentesComponent } from './components/preguntas-frecuentes/preguntas-frecuentes.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {MatFormFieldModule} from '@angular/material/form-field';

import {MatInputModule} from '@angular/material/input';
import {MatDialogModule} from '@angular/material/dialog';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatSelectModule} from '@angular/material/select';
import {MatRadioModule} from '@angular/material/radio';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatListModule} from '@angular/material/list';

import {MatTooltipModule } from '@angular/material/tooltip';
import {MatTableModule} from '@angular/material/table';

 import {RecaptchaModule } from 'ng-recaptcha';
import { DialogConfirmComponent } from './components/shared/dialog-confirm/dialog-confirm.component';
import { MsgSuccessComponent } from './components/shared/msg-success/msg-success.component';
import { MsgErrorComponent } from './components/shared/msg-error/msg-error.component';
import { LoadingComponent } from './components/shared/loading/loading.component';
import { TerminosCondicionesComponent } from './components/terminos-condiciones/terminos-condiciones.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { NumbersOnlyDirective } from './directivas/numbers-only.directive';

@NgModule({
  declarations: [
    AppComponent,
    RequerimientoComponent,
    PreguntasFrecuentesComponent,
    DialogConfirmComponent,
    MsgSuccessComponent,
    MsgErrorComponent,
    LoadingComponent,
    TerminosCondicionesComponent,
    FooterComponent,
    NumbersOnlyDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatCheckboxModule,
    MatListModule,
    MatSelectModule,
    MatDialogModule,
    MatRadioModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatTableModule,
    RecaptchaModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
