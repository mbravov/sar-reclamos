USE DBAgrSAR
GO
DROP PROCEDURE IF EXISTS SP_CNA_CREDITOS_OBTENER
GO
--=================================================================================================================================
-- FECHA DE CREACI�N		: 15/12/2021
-- FECHA DE MODIFICACI�N	:
-- CREADO POR				: CECILIA MARRUFO
-- STORE PROCEDURE			: SP_CNA_CREDITOS_OBTENER
-- PARAMETOS				: @NumeroDocumento VARCHAR(20),
--							  @TipoDocumento VARCHAR(20) 
-- FUNCIONALIDAD			:  Obtiene lista de cr�ditos de Clientes Agrobanco
--================================================================================================================================

	CREATE PROC SP_CNA_CREDITOS_OBTENER

	 @NumeroDocumento VARCHAR(20) ,
	 @TipoDocumento VARCHAR(20) 


	 AS

	 BEGIN
	   SELECT
			CAST('NRO001' AS varchar) NumeroPrestamo, 
			CAST('EJECUTADO' AS varchar) 	UltimaSituacion,
		    'CANCELADO' EstadoActual,
		   IsNull(CAST('0.01' AS varchar),'') SaldoDeudor ,
			CAST(format(23543656,'N')AS VARCHAR) Capital  ,
			CAST('2.05' AS varchar) InteresCompensatorio,
			CAST('2.00' AS varchar)InteresMoratorio, 
			IsNull(CAST('4.00' AS varchar),'')OtrosCargos
			FROM
			CreditoCliente
			WHERE
				TipoDocumento =  @TipoDocumento
				AND NumeroDocumento = @NumeroDocumento 
	
	
	END

