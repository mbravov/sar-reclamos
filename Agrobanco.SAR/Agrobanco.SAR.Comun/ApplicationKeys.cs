﻿
using Agrobanco.SAR.Control.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Comun
{
    public static class ApplicationKeys
    {
        static BaseControl oBaseControl = new BaseControl();

        public static string APILaserfiche = oBaseControl.ObtenerValorParametro("APILaserfiche");
        public static string FolderConsultaLaserfiche = oBaseControl.ObtenerValorParametro("FolderConsultaLaserfiche");
        public static string RegeditPass = oBaseControl.ObtenerValorParametro("RegeditPass");  
        public static string SQLRegeditFolder = oBaseControl.ObtenerValorParametro("SQLRegeditFolder");
        public static string SQLApplicationNameEnableEncrip = oBaseControl.ObtenerValorParametro("SQLApplicationNameEnableEncrip");
        
        public static string CorreoApplicationNameEnableEncrip = oBaseControl.ObtenerValorParametro("CorreoApplicationNameEnableEncrip");  
        public static string CorreoApplicationName = oBaseControl.ObtenerValorParametro("CorreoApplicationName");

        public static string CorreoApplicationNameCNAEnableEncrip = oBaseControl.ObtenerValorParametro("CorreoApplicationNameCNAEnableEncrip");
        public static string CorreoApplicationNameCNA = oBaseControl.ObtenerValorParametro("CorreoApplicationNameCNA");

        public static string CorreoAsunto = oBaseControl.ObtenerValorParametro("CorreoAsunto");
        public static string CorreoAsuntoAsignar = oBaseControl.ObtenerValorParametro("AsuntoAsignar");
        public static string CorreoAsuntoDerivarOCM = oBaseControl.ObtenerValorParametro("AsuntoDerivarOCM");
        public static string CorreoAsuntoRechazoUDA = oBaseControl.ObtenerValorParametro("AsuntoRechazoUDA");
        public static string CorreoAsuntoRechazoOCM= oBaseControl.ObtenerValorParametro("AsuntoRechazoOCM");
        public static string CorreoAsuntoDerivarNotificación = oBaseControl.ObtenerValorParametro("AsuntoDerivarNotificacion");
        public static string CorreoAsuntoRegistrarConsulta = oBaseControl.ObtenerValorParametro("AsuntoRegistrarConsulta");

        public static string rutaProdHabilitado = oBaseControl.ObtenerValorParametro("rutaProdHabilitado");  
        public static string rutaPlantillaCorreosProd = oBaseControl.ObtenerValorParametro("rutaPlantillaCorreosProd");
        public static string CorreoBuzonAgrobancoConsultas = oBaseControl.ObtenerValorParametro("CorreoBuzonAgrobancoConsultas");

        public static string FolderPlantillaLaserfiche = oBaseControl.ObtenerValorParametro("FolderPlantillaLaserfiche");

        
    }
}
