﻿using Microsoft.Extensions.Configuration;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;

namespace Agrobanco.Control.Util
{
    public static class Util
    {
        public static string ObtenerValueFromKey(string keys)
        {
            var configuration = new ConfigurationBuilder()
                  .SetBasePath(Directory.GetCurrentDirectory())
                  .AddJsonFile("appsettings.json", false)
                  .Build();

            return configuration[keys];
        }

        public static string ReadKey(string value,string folder)
        {
            Registry.LocalMachine.OpenSubKey("SOFTWARE", true);
            RegistryKey masterKey = Registry.LocalMachine.CreateSubKey("SOFTWARE\\AGROBANCO\\" + folder);

            string valueret = "";
            if (masterKey != null)
            {
                valueret = masterKey.GetValue(value).ToString();
            }

            masterKey.Close();
            return valueret;
        }
    }
}
