﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades
{
    public class MotivosCNABE
    {
        public int CodMotivo { get; set; }
        public string Descripcion { get; set; }
      
    }
}
