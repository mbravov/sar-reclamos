﻿using Agrobanco.SAR.Datos.SqlServer.Acceso;
using Agrobanco.SAR.Entidades.CNA_Minuta;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SAR.Datos
{
    public class ConfiguracionAccionDatos
    {
        public string cadenaConexion;
        public async Task<List<ConfiguracionAccionBE>>  Obtener(ConfiguracionAccionBE request)
        {
            ConfiguracionAccionBE oConfiguracionAccionBE = new ConfiguracionAccionBE();
            List<ConfiguracionAccionBE> lstConfiguracionAccionBE = new List<ConfiguracionAccionBE>();
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("SP_CONFIGURACION_ACCION_GENERAR", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@IdEstado", SqlDbType.Int).Value = request.IdEstado;
                        cmd.Parameters.Add("@Perfil", SqlDbType.VarChar, 5).Value = request.Perfil;
                      //  cmd.Parameters.Add("@Tipo", SqlDbType.Int).Value = request.Tipo;
                        
                        try
                        {
                            using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                            {
                                while (reader.Read())
                                {
                                    oConfiguracionAccionBE = new ConfiguracionAccionBE();

                                    if (!reader.IsDBNull(0))
                                        oConfiguracionAccionBE.IdConfiguracionAccion = reader.GetInt32(0);

                                    if (!reader.IsDBNull(1))
                                        oConfiguracionAccionBE.IdAccion = reader.GetInt32(1);

                                    if (!reader.IsDBNull(2))
                                        oConfiguracionAccionBE.Perfil = reader.GetString(2);

                                    if (!reader.IsDBNull(3))
                                        oConfiguracionAccionBE.IdEstado = reader.GetInt32(3);

                                    if (!reader.IsDBNull(4))
                                        oConfiguracionAccionBE.Activo = reader.GetBoolean(4);

                                    if (!reader.IsDBNull(5))
                                        oConfiguracionAccionBE.Metodo = reader.GetString(5);

                                    if (!reader.IsDBNull(6))
                                        oConfiguracionAccionBE.BotonNombre = reader.GetString(6);

                                    if (!reader.IsDBNull(7))
                                        oConfiguracionAccionBE.Tipo = Convert.ToInt32(reader.GetString(7));


                                    if (!reader.IsDBNull(8))
                                        oConfiguracionAccionBE.TipoAccion = reader.GetInt32(8);

                                    lstConfiguracionAccionBE.Add(oConfiguracionAccionBE);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            throw ex;
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return await Task.Run(() => lstConfiguracionAccionBE);
        }

      
    }
}
