import { Component, OnInit,ViewChild, ElementRef } from '@angular/core';
import { forkJoin } from 'rxjs';
import { RequerimientoService } from 'src/app/services/requerimiento.service';
import {MatDialog} from '@angular/material/dialog';
import { Router,ActivatedRoute } from '@angular/router';
import { SecurityService } from 'src/app/services/security.service';
import { ParametroService } from 'src/app/services/parametro.service';
import { UbigeoService } from '../../services/ubigeo.service';
import { DialogService } from 'src/app/services/comun/dialog.service';
import { PopupComentarioComponent } from 'src/app/components/popup-comentario/popup-comentario.component';
import { AnularComponent } from 'src/app/components/anular/anular.component';
import { Constantes } from 'src/app/comun/constantes';
import { HostListener } from '@angular/core';
import { PlatformLocation } from '@angular/common';


@Component({
  selector: 'app-requerimiento-detalle',
  templateUrl: './requerimiento-detalle.component.html',
  styleUrls: ['./requerimiento-detalle.component.css']

})
export class RequerimientoDetalleComponent implements OnInit {

  @ViewChild('CartaRespuestaOCM', { static: false }) InputVar!: ElementRef;
  @ViewChild('CartaRespuestaACM', { static: false }) InputCartaRespuestaACM!: ElementRef;
  @ViewChild('CartaRespuestaACMText', { static: false }) CartaRespuestaACMText!: ElementRef;


  usuarios!:any[];
  loading!:boolean;
  cartaRespuesta!:any;
  esUsuarioAsignado!:boolean;

  /* Variables Sesiones */
  perfil: any;
  usuarioWeb: any;
  UsuarioRegistro: any;

  /* Variables Requerimiento */
  NroReclamo: any;
  RequerimientoDetalle:any;
  UsuarioAsignado:any;
  UsuarioDerivarNotificacion:any;
  Archivo:any;
  NombreArchivo:any;


  MAXIMO_TAMANIO_BYTES = 1024000;

  /* Variables Estado Registrado */
  mostrarBotonAnulado!: boolean;
  mostrarInputAnulado!: boolean;
  mostrarBotonAsignar!: boolean;
  mostrarInputAsignar!: boolean;
  DesabilitarBotonAsignar:boolean = true;

  /* Variables Estado Asignado */
  mostrarBotonComentario!: boolean;
  mostrarInputComentario!: boolean;

  /* Variables Estado En Proceso */
  mostrarBotonDerivar! : boolean;
  mostrarInputDerivar! : boolean;
  disabledBotonDerivar! : boolean;
  /* Variables Estado Derivado a OCM */
  mostrarBotonAprobado! : boolean;
  mostrarInputAprobado! : boolean;
  disabledBotonAprobado! : boolean;

  mostrarBotonEliminar!:boolean;
  mostrarInputCartaRespuesta!:boolean;

//  bCartaRespuesta : boolean = false;
  /* Variables Estado Aprobado */
  mostrarBotonDerivarNotificacion! : boolean;
  mostrarInputDerivarNotificacion! : boolean;

  /* Variables Estado Sin Notificar */
  mostrarInputAtender! : boolean;
  mostrarBotonAtender! : boolean;

  /* Variables Estado Atendido */

  fRespuesta : boolean = false;
  bFechaRespuesta : boolean = true;
  fechaAtencionInicioValor!:Date;
  fechaRegistroFinValor! : Date;

  displayedColumns: string[] = ['CodCred', 'Estado', 'Saldo', 'FechaPago'];

  Solicitud:number = Constantes.Solicitud;

  constructor(
    private securityService:SecurityService,
    private dialog: MatDialog,
    private requerimientoServices : RequerimientoService,
    private dialogService: DialogService,
    private route: ActivatedRoute,
    private router: Router,
    location: PlatformLocation
  ) {
    this.perfil = this.securityService.leerPerfil();
    this.usuarioWeb = this.securityService.leerUsuarioWeb();
    this.NroReclamo = this.route.snapshot.params.id;
    this.UsuarioRegistro = this.securityService.leerUsuarioWeb();


      location.onPopState(() => {
        const params = {
            NroReclamo : this.NroReclamo,
            UsuarioRegistro : this.usuarioWeb,
            EstaBloqueado : Constantes.NoEstaBloqueado
        };
        this.requerimientoServices.ActualizarRequerimientoBloqueado(params).subscribe((result:any) =>{
         console.log("response",result.response);
        });
      });
   }

  ngOnInit(): void {
    this.mostrarBotonAsignar = false;
    this.mostrarInputAsignar = false;
    this.mostrarBotonAnulado = false;

    this.mostrarBotonComentario = false;
    this.mostrarInputComentario = false;

    this.mostrarInputDerivar= false;
    this.mostrarBotonDerivar = false;
    this.disabledBotonDerivar = true;

    this.mostrarBotonAprobado= false;
    this.mostrarInputAprobado = false;
    this.disabledBotonAprobado = true;

    this.mostrarBotonDerivarNotificacion= false;
    this.mostrarInputDerivarNotificacion = false;

    this.mostrarInputAtender= false;
    this.mostrarBotonAtender = false;

    //this.fechaAtencionInicioValor = new Date();
    this.fechaRegistroFinValor = new Date();
    this.esUsuarioAsignado = false;

    this.obtenerRequerimientoDetalle();
    this.obtenerUsuarios();
  }

  /*  Metodos         */
  reset()
  {
    this.InputVar.nativeElement.value = "";

  }

  resetACM(){


    this.InputCartaRespuestaACM.nativeElement.value = "";
    this.CartaRespuestaACMText.nativeElement.value = "";

  }
  obtenerRequerimientoDetalle(): void {
    const nroReclamo = {
      nroReclamo: this.NroReclamo,
    };
    this.requerimientoServices
      .ObtenerRequerimientoDetalle(nroReclamo)
      .subscribe((result) => {
        if(result.response){

          this.RequerimientoDetalle = result.response;
          console.log("this.RequerimientoDetalle",this.RequerimientoDetalle)

          if(this.RequerimientoDetalle.usuarioWebAsignado != null && this.usuarioWeb.toLowerCase() == this.RequerimientoDetalle.usuarioWebAsignado.toLowerCase()){
            this.esUsuarioAsignado = true;
          }

          if(this.RequerimientoDetalle.estado==Constantes.Registrado){
            this.HabilitarEstadoRegistrado();
          }

          if(this.RequerimientoDetalle.estado==Constantes.Asignado){
            this.HabilitarEstadoAsignado();
          }

          if(this.RequerimientoDetalle.estado==Constantes.EnProceso){
            this.HabilitarEstadoEnProceso();
          }

          if(this.RequerimientoDetalle.estado==Constantes.DerivadoaOCM){
            this.HabilitarEstadoDerivadoaOCM();
          }

          if(this.RequerimientoDetalle.estado==Constantes.Aprobado){
            this.HabilitarEstadoAprobado();
          }

          if(this.RequerimientoDetalle.estado==Constantes.SinNotificar){
            this.HabilitarEstadoSinNotificar();
          }
          if(this.RequerimientoDetalle.estado==Constantes.Pendiente || this.RequerimientoDetalle.estado==Constantes.SinAtencion){
            this.EstadoAnterior();
          }
        }

      });
  }
  obtenerUsuarios(): void {
  this.loading = true;
  var Perfil = {
    Perfil: this.perfil,
    UsuarioWeb: this.usuarioWeb,
    Estado: true
  };
  forkJoin([
    this.requerimientoServices.ObtenerUsuarios(Perfil)
  ]).subscribe(
    (data: any) => {
      console.log(data);
      this.usuarios = data[0].response;
      this.loading = false;
    },
    (error) => {
      this.loading = false;
      console.log(error);
    }
  );
  }
  DescargarArchivos(LaserficheID:any): void {
    this.loading = true;
    const params = {
      LaserficheID: ""+LaserficheID
    };
    this.requerimientoServices.DescagarArchivos(params).subscribe((result) => {
      console.log('response', result.response);
      if(result.response!=null){
        if(result.response.length>0){
          for (let index = 0; index < result.response.length; index++) {
            const element = result.response[index];
            debugger;
            this.Descargar(element.archivoBytes,element.nombreDocumento,element.contentType);
          }
        }
      }
      this.loading = false;
    });
  }
  DescargarCartaRespuesta(LaserficheID:any): void {
    this.loading = true;
    const params = {
      LaserficheID: ""+LaserficheID
    };
    this.requerimientoServices.DescagarArchivos(params).subscribe((result) => {
      console.log('response', result.response);
      if(result.response!=null){
        if(result.response.length>0){
          for (let index = 0; index < result.response.length; index++) {
            const element = result.response[index];
            debugger;
            const FechaActual = new Date();
            const AnioActual = FechaActual.getFullYear();
            const MesActual = (FechaActual.getMonth()<=9? '0'+ (FechaActual.getMonth()+1): FechaActual.getMonth() + 1); //FechaActual.getMonth() + 1;
            const DiaActual = (FechaActual.getDate()<=9? '0'+FechaActual.getDate(): FechaActual.getDate());
            const HoraActual = (FechaActual.getHours()<=9? '0'+FechaActual.getHours(): FechaActual.getHours()); //FechaActual.getHours();
            const MinutoActual = (FechaActual.getMinutes()<=9? '0'+FechaActual.getMinutes(): FechaActual.getMinutes()); //FechaActual.getMinutes();
            const SegundosActual = (FechaActual.getSeconds()<=9? '0'+FechaActual.getSeconds(): FechaActual.getSeconds()); //FechaActual.getMinutes(); //FechaActual.getSeconds();

            const NombreArchivo = this.NroReclamo + "_" + AnioActual + MesActual + DiaActual + "T" + HoraActual + MinutoActual + SegundosActual;

            this.Descargar(element.archivoBytes,NombreArchivo,element.contentType);
          }
        }
      }
      this.loading = false;
    });
  }
  Descargar(base64String:any, fileName:any,contentType:any) {

    const blobData = this.convertBase64ToBlobData(base64String,contentType);
    const blob = new Blob([blobData], { type: contentType });
        const url = window.URL.createObjectURL(blob);
        // window.open(url);
        const link = document.createElement('a');
        link.href = url;
        link.download = fileName;
        link.click();
  }
  convertBase64ToBlobData(base64Data: string, contentType: string = 'image/png', sliceSize = 1024) {
    const byteCharacters = atob(base64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);
      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  HabilitarEstadoRegistrado(){

    this.mostrarBotonAsignar = true;
    this.mostrarInputAsignar = true;
    this.mostrarBotonAnulado = true;

    if(this.perfil.toLowerCase()==Constantes.ACM){
      this.mostrarBotonAnulado = false;
    }
  }
  HabilitarEstadoAsignado(){

    if(this.perfil.toLowerCase()==Constantes.OCM || this.esUsuarioAsignado){
      this.mostrarBotonComentario = true;
    }

    if(this.perfil.toLowerCase()==Constantes.OCM){
      this.mostrarBotonAsignar = true;
      this.mostrarInputAsignar = true;
    }
  }
  HabilitarEstadoEnProceso(){

    if(this.esUsuarioAsignado)
    {
      this.mostrarBotonComentario = true;
      this.mostrarInputDerivar= true;
      this.mostrarBotonDerivar = true;
    }

    if(this.perfil.toLowerCase()==Constantes.OCM){
      this.mostrarBotonComentario = true;
      this.mostrarBotonAsignar = true;
      this.mostrarInputAsignar = true;
    }

    if(this.RequerimientoDetalle.laserFicheIDCartaRespuesta){
      this.disabledBotonDerivar = false;
    }
  }
  HabilitarBotonDerivarOCM(value:boolean){
    this.disabledBotonDerivar = value;
  }
  HabilitarEstadoDerivadoaOCM(){

    if(this.perfil.toLowerCase()==Constantes.OCM){
      this.mostrarBotonComentario = true;

      this.mostrarBotonAsignar = true;
      this.mostrarInputAsignar = true;

      this.mostrarBotonAprobado = true;
      this.mostrarInputAprobado = true;
    }

    if(this.esUsuarioAsignado){
      this.mostrarBotonComentario = true;

      this.mostrarBotonEliminar = true;
      this.mostrarInputCartaRespuesta  = true;
    }

    if(this.RequerimientoDetalle.laserFicheIDCartaRespuestaOCM){
      this.disabledBotonAprobado = false;
    }
  }
  HabilitarEstadoAprobado(){

    if(this.esUsuarioAsignado){
      this.mostrarBotonComentario = true;
    }

    if(this.perfil.toLowerCase()==Constantes.OCM){

      this.mostrarBotonComentario = true;

      this.mostrarBotonAsignar = true;
      this.mostrarInputAsignar = true;

      this.mostrarBotonDerivarNotificacion = true;
      this.mostrarInputDerivarNotificacion = true;
    }
  }

  HabilitarEstadoSinNotificar(){

    if(this.esUsuarioAsignado){
      this.mostrarBotonComentario = true;
    }

    if(this.perfil.toLowerCase()==Constantes.OCM){
      this.mostrarBotonComentario = true;

      this.mostrarBotonAsignar = true;
      this.mostrarInputAsignar = true;

      this.mostrarInputAtender = true;
      this.mostrarBotonAtender = true;
    }
  }

  EstadoAnterior(){

    if (this.RequerimientoDetalle.estado == Constantes.Pendiente || this.RequerimientoDetalle.estado == Constantes.SinAtencion) {

          const EstadoAnterior = this.RequerimientoDetalle.estadoAnterior;

          switch (EstadoAnterior) {
            case Constantes.Registrado:
              this.HabilitarEstadoRegistrado();
            break;
            case Constantes.Asignado:
              this.HabilitarEstadoAsignado();
            break;
            case Constantes.EnProceso:
              this.HabilitarEstadoEnProceso();
            break;
            case Constantes.DerivadoaOCM:
              this.HabilitarEstadoDerivadoaOCM();
            break;
            case Constantes.Aprobado:
              this.HabilitarEstadoAprobado();
            break;
            case Constantes.SinNotificar:
              this.HabilitarEstadoSinNotificar();
            break;

            default:
              break;
        }
    }

  }

  /*  Funciones       */
  AsignacionUsuario(estado:any): void {
    const params = {
      CodRequerimiento: +this.RequerimientoDetalle.codRequerimiento,
      UsuarioRegistro: this.UsuarioRegistro,
      UsuarioAsignar: this.UsuarioAsignado,
      NombresApellidos:  this.RequerimientoDetalle.nombresApellidos,
      RazonSocial: this.RequerimientoDetalle.razonSocial,
      CodRequerimientoAsignacion : 0,
      CodRequerimientoMovimiento: this.RequerimientoDetalle.codRequerimientoMovimiento,
      CorreoElectronico: this.RequerimientoDetalle.correoElectronico,
      NroRequerimiento: this.NroReclamo,
      nroReclamo: this.NroReclamo,
      Origen: this.RequerimientoDetalle.origen,
      TipoRequerimiento:  this.RequerimientoDetalle.tipoRequerimiento,
      TipoDocumentoDescripcion: this.RequerimientoDetalle.tipoDocumentoDescripcion,
      NroDocumento: this.RequerimientoDetalle.nroDocumento,
      Motivo: this.RequerimientoDetalle.motivo,
      FechaRegistro:  this.RequerimientoDetalle.fechaRegistro,
      Estado: (estado==null? this.RequerimientoDetalle.estado: estado )
    };
this.loading = true;
    this.requerimientoServices.RegistrarAsignacion(params).subscribe(result=>{

      console.log("response",result);
      this.loading = false;
      if(result.response){

        if(this.RequerimientoDetalle.estado==Constantes.Registrado){
            this.dialogService
            .openMsgSuccessDialog(
              'El estado del requerimiento se ha cambiado de ' +
                this.RequerimientoDetalle.descripcionEstado.toUpperCase() +
                ' a ASIGNADO'
            )
            .afterClosed()
            .subscribe((res) => {
              if(res){
                 this.Regresar();
              }

            });
        }
        else{
          var UsuarioAsignadoNombresApellido = '';
          var objUsuarioAsignado = this.usuarios.filter(x=>x.usuarioWeb == this.UsuarioAsignado);
          if(objUsuarioAsignado!=null){
            if(objUsuarioAsignado.length>0){
              UsuarioAsignadoNombresApellido = objUsuarioAsignado[0].nombresApellidos;
            }
          }

          this.dialogService
          .openMsgSuccessDialog('El usuario ' + UsuarioAsignadoNombresApellido + ' a sido asignado')
          .afterClosed()
          .subscribe((res) => {
            if(res){

              this.ngOnInit();
            }
          });
        }

      }
      else{
        this.dialogService.openMsgErrorDialog("Hubo un error al intentar registrar el requerimiento");
      }

    }, (err)=>{
      this.loading = false;
      console.log(err);
    });


    /*
    if(params.CodRequerimientoAsignacion == 0){

          this.requerimientoServices
            .RegistrarMovimiento(params)
            .subscribe((result) => {
              console.log('response', result.response);

              params.CodRequerimientoMovimiento = result.response;

              if (result.response > 0) {
                //this.loading = false;


              }
            });

        } else {
          params.Estado = Estado;
          params.CodRequerimientoMovimiento = parseInt(
            this.form.controls['CodRequerimiento'].value
          );

            this.requerimientoServices.RegistrarAsignacion(params).subscribe(result=>{
              debugger;
              console.log("response",result);

              //this.loading = false;
              if(result.response){
                this.dialogService
                    .openMsgSuccessDialog(
                      'Se asigno correctamente'
                    )
                    .afterClosed()
                    .subscribe((res) => {
                      (document.location.href = `/gestion`), null, null;
                    });

              }
              else{
                this.dialogService.openMsgErrorDialog("Hubo un error al intentar registrar el requerimiento");
              }

            }, (err)=>{
              this.loading = false;
              console.log(err);
            });

        }*/

  }

  @HostListener('window:beforeunload', ['$event'])
  beforeunloadHandler() {
    let estaBloqueado = 0;
    let parametro = {
      estaBloqueado : estaBloqueado
    };
    if(estaBloqueado == 0)
    {
      const params = {
        NroReclamo : this.NroReclamo,
        UsuarioRegistro : this.usuarioWeb,
        EstaBloqueado : Constantes.NoEstaBloqueado

       };

       this.requerimientoServices.ActualizarRequerimientoBloqueado(params).subscribe((result:any) =>{
           console.log("response",result.response);

           this.router.navigate(['/gestion']);
       });


    }
    return false;
    //I have used return false but you can your other functions or any query or condition
  }

  /*  Eventos Click   */
  Regresar(){
    let estaBloqueado = 0;
    let parametro = {
      estaBloqueado : estaBloqueado
    };
    if(estaBloqueado == 0)
    {
      const params = {
        NroReclamo : this.NroReclamo,
        UsuarioRegistro : this.usuarioWeb,
        EstaBloqueado : Constantes.NoEstaBloqueado

       };

       this.requerimientoServices.ActualizarRequerimientoBloqueado(params).subscribe((result:any) =>{
           console.log("response",result.response);

           this.router.navigate(['/gestion']);
       });


    }



  }
  AnularRequerimiento() {
    //if (this.RequerimientoDetalle.estado == Constantes.Registrado) {
      const params = {
        CodRequerimiento: +this.RequerimientoDetalle.codRequerimiento,
        CodRequerimientoMovimiento:  +this.RequerimientoDetalle.codRequerimientoMovimiento,
        UsuarioRegistro: this.UsuarioRegistro,
        NroRequerimiento: this.NroReclamo,
        Estado: +this.RequerimientoDetalle.estado,
        DescripcionEstado: this.RequerimientoDetalle.descripcionEstado
      };

      console.log("AnularRequerimiento",params);

      const dialogRef = this.dialog.open(AnularComponent, {
        data: { data: params },
      });

      dialogRef.afterClosed().subscribe((result) => {
        console.log(`Dialog result: ${result.data}`);
        if(result.data=='OK')
          this.Regresar();
      });
    ///}
  }
  AsignarRequerimiento() {
    debugger;
    if(this.UsuarioAsignado==null){
      this.dialogService.openMsgErrorDialog("Seleccione un usuario para su asignación");
      return;
    }
    if(this.UsuarioAsignado==""){
      this.dialogService.openMsgErrorDialog("Seleccione un usuario para su asignación");
      return;
    }
    if (this.UsuarioAsignado != null) {
      if (this.RequerimientoDetalle.estado == Constantes.Pendiente || this.RequerimientoDetalle.estado == Constantes.SinAtencion) {
        this.requerimientoServices
          .PenultimoMovimiento(this.RequerimientoDetalle.codRequerimiento)
          .subscribe((res) => {
            console.log('response', res.response);
            const EstadoAnterior = res.response[0].estado;
            if (
              EstadoAnterior == Constantes.Registrado ||
              EstadoAnterior == Constantes.Asignado ||
              EstadoAnterior == Constantes.EnProceso ||
              EstadoAnterior == Constantes.DerivadoaOCM ||
              EstadoAnterior == Constantes.Aprobado ||
              EstadoAnterior == Constantes.SinNotificar
            ) {

              this.dialogService
                .openConfirmDialog(
                  '¿Está seguro que desea asignar este requerimiento?'
                )
                .afterClosed()
                .subscribe((res) => {
                  if (res) {
                    this.AsignacionUsuario(EstadoAnterior);
                  }
                });
            }
          });
      } else if (
        this.RequerimientoDetalle.estado == Constantes.Registrado ||
        this.RequerimientoDetalle.estado == Constantes.Asignado ||
        this.RequerimientoDetalle.estado == Constantes.EnProceso ||
        this.RequerimientoDetalle.estado == Constantes.DerivadoaOCM ||
        this.RequerimientoDetalle.estado == Constantes.Aprobado ||
        this.RequerimientoDetalle.estado == Constantes.SinNotificar
      ) {

        this.dialogService
          .openConfirmDialog('¿Está seguro que desea asignar este requerimiento?')
          .afterClosed()
          .subscribe((res) => {
            if (res) {
              this.AsignacionUsuario(null);
            }
          });
      } else {
        this.dialogService.openMsgErrorDialog('Volver');
      }
    }
  }
  AgregarComentario(): void {
    const params = {
      CodRequerimiento: +this.RequerimientoDetalle.codRequerimiento,
      CodRequerimientoMovimiento: +this.RequerimientoDetalle.codRequerimientoMovimiento,
      UsuarioRegistro: this.UsuarioRegistro,
      NroRequerimiento: this.NroReclamo,
      Estado: +this.RequerimientoDetalle.estado
    };
    const dialogRef = this.dialog.open(PopupComentarioComponent, {
      data: { comentario: params },
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log(`Dialog result: ${result.data}`);

      this.ngOnInit();
      /*
        if(result.data.Estado==Constantes.Asignado){
          this.Regresar();
        }
        else{
          this.ngOnInit();
        }
      */
    });

  }
  DerivadoOCMRequerimiento() {
    this.dialogService
      .openConfirmDialog('¿Está seguro que desea Derivar a OCM?')
      .afterClosed()
      .subscribe((res) => {
        if (res) {
          this.loading = true;
          const oBE = {
            CodRequerimientoMovimiento: 0,
            CodRequerimiento: this.RequerimientoDetalle.codRequerimiento,
            Estado: Constantes.DerivadoaOCM,
            UsuarioRegistro: this.UsuarioRegistro,
            nroReclamo: this.NroReclamo,
            NroRequerimiento: this.NroReclamo,

            UsuarioAsignar: this.RequerimientoDetalle.usuarioAsignar, // this.form.controls['UsuarioAsignar'].value,
            UsuarioAsignado: this.RequerimientoDetalle.usuarioAsignado,
            NombresApellidos: this.RequerimientoDetalle.nombresApellidos, //this.form.controls['NombresApellidos'].value,
            CorreoElectronico: this.RequerimientoDetalle.correoElectronico, //this.form.controls['CorreoElectronico'].value,
            Origen: this.RequerimientoDetalle.origen, //this.form.controls['Origen'].value,
            TipoRequerimiento: this.RequerimientoDetalle.tipoRequerimiento, //this.form.controls['TipoRequerimiento'].value,
            TipoDocumentoDescripcion: this.RequerimientoDetalle.tipoDocumentoDescripcion, //this.form.controls['TipoDocumentoDescripcion'].value,
            NroDocumento: this.RequerimientoDetalle.nroDocumento, // this.form.controls['NroDocumento'].value,
            Motivo: this.RequerimientoDetalle.motivo, //this.form.controls['Motivo'].value,
            FechaRegistro: this.RequerimientoDetalle.fechaRegistro, //this.form.controls['FechaRegistro'].value
            RazonSocial: this.RequerimientoDetalle.razonSocial,

          };

          this.requerimientoServices.DerivarOCM(oBE).subscribe(
            (result) => {
              this.loading = false;
              if (result.response.success.resultado) {
                this.dialogService
                  .openMsgSuccessDialog('Se ha derivado a ocm correctamente')
                  .beforeClosed()
                  .subscribe((res) => {
                    if (res) {
                     // document.location.href = `/gestion/requerimiento-detalle/` + this.NroReclamo;
                     //this.router.navigate(['/gestion/requerimiento-detalle',this.NroReclamo]);
                     this.ngOnInit();
                    }
                  });
              } else {
                this.dialogService.openMsgErrorDialog(result.response.mensaje);
              }
            },
            (err) => {
              console.log(err);
            }
          );
        }
      });
  }

  SubirCartaRespuestaOCM(){

    if(this.Archivo==null || this.Archivo == ''){
      this.dialogService.openMsgErrorDialog("Se debe seleccionar una carta de respuesta OCM");
      return;
    }

    if(this.Archivo)
      {
        var oBE = {
          "CodTabla": Constantes.CartaRespuestaOCM,
          "LaserficheID": null,
          "NroReclamo":this.NroReclamo,
          "Archivo": this.Archivo,
          "NombreArchivo": this.NombreArchivo,
          "CodRequerimiento": this.RequerimientoDetalle.codRequerimiento,
          "UsuarioRegistro": this.securityService.leerUsuarioWeb()
        };

    this.dialogService.openConfirmDialog('¿Está seguro que desea subir este documento?')
    .afterClosed().subscribe(res => {

      if(res){
        this.loading = true;
        this.requerimientoServices.SubirArchivo(oBE).subscribe(result=>{
          debugger;

          if(result.response.resultadoOK){
            //this.bCartaRespuestaOCM = true;

           // this.MostrarSubirCartaRespuesta = false;
            //this.ocultarBotonAprobar = true;
            this.dialogService.openMsgSuccessDialog("Se subió correctamente").afterClosed().subscribe(res=>{
              if(res){
                this.disabledBotonAprobado = false;
                this.RequerimientoDetalle.laserFicheIDCartaRespuestaOCM = result.response.laserficheID;
              }
            });
          }
          else{
         //   this.bCartaRespuestaOCM = false;
            this.dialogService.openMsgErrorDialog("Se presento un problema para subir la carta de resuesta").afterClosed().subscribe(res=>{
              if(res){
                this.disabledBotonAprobado = true;
              }
            });
          }
          this.loading = false;
        },
        (err)=>{
          this.loading = false;
          console.log(err);
        }
        );
      }

    });

    }
    else{
      this.dialogService.openMsgErrorDialog('Se debe seleccionar una carta de respuesta OCM')
    .afterClosed().subscribe(res => {
      if(res){
        this.disabledBotonAprobado = true;
      }
    });

    }
  }
  EliminarCartaRespuestaOCM(){

    this.dialogService
      .openConfirmDialog('¿Seguro que desea eliminar la carta de respuesta?')
      .afterClosed()
      .subscribe((res) => {
        if (res) {
          const oBE = {
            "LaserficheID": ""+this.RequerimientoDetalle.laserFicheIDCartaRespuestaOCM
            };
            this.loading = true;
            this.requerimientoServices.eliminarArchivo(oBE).subscribe(result=>{
              this.loading = false;
              debugger;
              console.log("eliminarArchivo",result);
              if(result.response.resultado==1){

                this.dialogService.openMsgSuccessDialog("Se eliminó correctamente").afterClosed().subscribe(res=>{
                  if(res){
                    this.RequerimientoDetalle.laserFicheIDCartaRespuestaOCM = null;
                    this.disabledBotonAprobado = true;
                  }
                });
              }
              else{

                this.dialogService.openMsgErrorDialog("Se presento un problema para eliminar la carta de resuesta").afterClosed().subscribe(res=>{
                  if(res){
                    this.disabledBotonAprobado = false;
                  }
                });
              }

            },
            (err)=>{
              this.loading = false;
              this.disabledBotonAprobado = false;
              console.log(err);
            }
            );

        }
   });

  }

  AprobadoRequerimiento() {
    const data =   {
      CodRequerimiento : +this.RequerimientoDetalle.codRequerimiento, // this.form.controls['CodRequerimiento'].value,
      UsuarioRegistro :  this.UsuarioRegistro,
      Estado :  Constantes.Aprobado
    }

    this.dialogService.openConfirmDialog('¿Está seguro que desea Aprobar este requerimiento?')
        .afterClosed().subscribe(res => {
          if (res) {

            this.loading = true;
            this.requerimientoServices
            .AprobarRequerimiento(data)
            .subscribe((result) => {
              console.log('response', result.response);
              this.loading = false;
              if(result.response){
                this.dialogService
                .openMsgSuccessDialog(
                  'El estado del requerimiento se ha cambiado de ' + this.RequerimientoDetalle.descripcionEstado.toUpperCase() +
                    ' a APROBADO'
                )
                .afterClosed()
                .subscribe((res) => {
                    if(res){
                      //this.Regresar();

                      this.ngOnInit();
                    }
                });
              }
              else{

              }
            });



          }

          });

  }

  SinNotificarRequerimiento() {
    const oBE = {
      CodRequerimientoMovimiento: 0,
      CodRequerimiento: this.RequerimientoDetalle.codRequerimiento,
      NroRequerimiento: this.NroReclamo,
      UsuarioRegistro : this.UsuarioRegistro,
      UsuarioAsignado: this.RequerimientoDetalle.usuarioAsignado,
      UsuarioAsignadoNotificacion: this.UsuarioDerivarNotificacion,
      Estado: Constantes.SinNotificar,
      nroReclamo: this.NroReclamo,
      RazonSocial: this.RequerimientoDetalle.razonSocial,
      NombresApellidos: this.RequerimientoDetalle.nombresApellidos,
      CorreoElectronico: this.RequerimientoDetalle.correoElectronico,
      Origen: this.RequerimientoDetalle.origen,
      TipoRequerimiento: this.RequerimientoDetalle.tipoRequerimiento, //this.form.controls['TipoRequerimiento'].value,
      TipoDocumentoDescripcion: this.RequerimientoDetalle.tipoDocumentoDescripcion, // this.form.controls['TipoDocumentoDescripcion'].value,
      NroDocumento: this.RequerimientoDetalle.nroDocumento, // this.form.controls['NroDocumento'].value,
      Motivo: this.RequerimientoDetalle.motivo, //this.form.controls['Motivo'].value,
      FechaRegistro: this.RequerimientoDetalle.fechaRegistro,  //this.form.controls['FechaRegistro'].value
    }

    if(this.UsuarioDerivarNotificacion==null){
      this.dialogService.openMsgErrorDialog("Seleccione un usuario para derivar para su notificación");
      return;
    }

    if(this.UsuarioDerivarNotificacion==""){
      this.dialogService.openMsgErrorDialog("Seleccione un usuario para derivar para su notificación");
      return;
    }

    this.dialogService.openConfirmDialog('¿Está seguro que desea derivar para su notificación?')
        .afterClosed().subscribe(res => {
          if (res) {
            this.loading = true;
            this.requerimientoServices
            .DerivarParaSuNotificacion(oBE)
            .subscribe((result) => {
              this.loading = false;
              if (result.response.success.resultado) {
                this.dialogService
                  .openMsgSuccessDialog('Se ha derivado para su notificación')
                  .beforeClosed()
                  .subscribe((res) => {
                    if (res) {
                      this.Regresar();
                      // document.location.href = `/gestion/requerimiento-detalle/` + this.NroReclamo;
                    }
                  });
              } else {
                this.dialogService.openMsgErrorDialog(result.response.mensaje);
              }

            }, (err) => {
              this.loading = false;
              console.log(err);
            });
          }
    });
  }

  AtendidoRequeriminto() {
debugger;
    if(this.fechaAtencionInicioValor != null){
    this.dialogService
      .openConfirmDialog('¿Seguro que desea dar por atendido el requerimiento?')
      .afterClosed()
      .subscribe((res) => {
        if (res) {
          const oBE = {
            CodRequerimiento: +this.RequerimientoDetalle.codRequerimiento,
            Estado: Constantes.Atendido ,
            UsuarioRegistro :  this.UsuarioRegistro,
            FechaEntrega: this.fechaAtencionInicioValor,
            CodRequerimientoMovimiento : this.RequerimientoDetalle.codRequerimientoMovimiento // this.form.controls['CodRequerimientoMovimiento'].value

          };

          this.requerimientoServices
            .AtenderRequeriminto(oBE)
            .subscribe((result) => {
              console.log('response', result.response);
              if(result.response){
                this.dialogService
                .openMsgSuccessDialog(
                  'El estado del requerimiento se ha cambiado de ' +
                    this.RequerimientoDetalle.descripcionEstado.toUpperCase() +
                    ' a ATENDIDO'
                )
                .afterClosed()
                .subscribe((res) => {
                  if (res) {
                    //document.location.href = `/gestion/requerimiento-detalle/` + this.NroReclamo;
                    //this.router.navigate(['/gestion/requerimiento-detalle',this.NroReclamo]);
                   //window.location.reload();
                   this.reload();
                  }
                });

              }
              else{
                this.dialogService.openMsgErrorDialog("Hubo un problema al intentar atender el requerimiento");
              }

            });







        }
      });
    }

    else{
      this.dialogService.openMsgErrorDialog('Se debe seleccionar la fecha de respuesta')
    .afterClosed().subscribe(res => {
    });

    }

  }

  /*  Eventos Change  */
  changeSeletedCartaRespuestaOCM(event:any) {
    const file = event.target.files[0];
   // this.bCartaRespuestaOCM = false;
    if(file==null || file.length <=0){
      this.Archivo = "";
      this.NombreArchivo = "";
      this.Archivo == null;
      this.NombreArchivo == null;
      this.reset();
     return;
   }

   var allowedExtensions = /(.doc|.docx|.pdf)$/i;
   if(!allowedExtensions.exec(file.name)){
    this.dialogService.openMsgErrorDialog("Formato no válido");
    this.Archivo = "";
    this.NombreArchivo = "";
    this.Archivo = null;
    this.NombreArchivo = null;
    this.reset();
    return;
   }

    if (file.size > this.MAXIMO_TAMANIO_BYTES) {
      const tamanioEnMb = this.MAXIMO_TAMANIO_BYTES / 1000000;
      this.Archivo = "";
      this.NombreArchivo = "";
      this.Archivo == null;
      this.NombreArchivo == null;
      this.reset();
      this.dialogService.openMsgErrorDialog(`El tamaño máximo es 1 MB`);
      return;
    } else {
      // Validación pasada. Envía el formulario o haz lo que tengas que hacer
    }

    const FileNamme = file.name;
    this.NombreArchivo = FileNamme;

    const reader = new FileReader();
    console.log("file",file);
    reader.readAsDataURL(file);
    reader.onload = () => {
      console.log("reader",reader);
      this.Archivo = reader.result || null;
      console.log(reader.result);
    };
  }

  onChangeFechaRespuesta(event:any){

    if(this.fechaAtencionInicioValor)
      this.bFechaRespuesta = false;
    else
      this.bFechaRespuesta = true;
  }


  EliminarCartaRespuestaACM(LaserFicheIDCartaRespuesta:string) : void {

    this.dialogService
      .openConfirmDialog('¿Seguro que desea eliminar la carta de respuesta?')
      .afterClosed()
      .subscribe((res) => {
        if (res) {
          const oBE = {
            "LaserficheID": ""+LaserFicheIDCartaRespuesta
            };
            this.loading = true;
            this.requerimientoServices.eliminarArchivo(oBE).subscribe(result=>{
              this.loading = false;
              debugger;
              console.log("eliminarArchivo",result);
              if(result.response.resultado==1){
                //this.bCartaRespuesta = false;
                this.dialogService.openMsgSuccessDialog("Se eliminó correctamente").afterClosed().subscribe(res=>{
                  if(res){

                    //window.location.reload();
                    this.reload();
                  }
                });
              }
              else{
               // this.bCartaRespuesta = false;
                this.dialogService.openMsgErrorDialog("Se presento un problema para eliminar la carta de resuesta").afterClosed().subscribe(res=>{
                  if(res){

                  }
                });
              }

            },
            (err)=>{
              this.loading = false;
              console.log(err);
            }
            );

        }
   });

   }



   changeSeletedCartaRespuestaACM(event:any) {
     debugger;
    const file = event.target.files[0];
   // this.bCartaRespuestaOCM = false;
    if(file==null || file.length <=0){
      this.Archivo = "";
      this.NombreArchivo = "";
      this.Archivo == null;
      this.NombreArchivo == null;

      this.resetACM();
     return;
   }

   var allowedExtensions = /(.doc|.docx)$/i;
   if(!allowedExtensions.exec(file.name)){
    this.dialogService.openMsgErrorDialog("Formato no válido");
    this.Archivo = "";
    this.NombreArchivo = "";
    this.Archivo = null;
    this.NombreArchivo = null;

    this.resetACM();
    return;
   }

    if (file.size > this.MAXIMO_TAMANIO_BYTES) {
      const tamanioEnMb = this.MAXIMO_TAMANIO_BYTES / 1000000;
      this.Archivo = "";
      this.NombreArchivo = "";
      this.Archivo == null;
      this.NombreArchivo == null;

      this.resetACM();
      this.dialogService.openMsgErrorDialog(`El tamaño máximo es 1 MB`);
      return;
    } else {
      // Validación pasada. Envía el formulario o haz lo que tengas que hacer
    }

    const FileNamme = file.name;
    this.NombreArchivo = FileNamme;


    const reader = new FileReader();
    console.log("file",file);
    reader.readAsDataURL(file);
    reader.onload = () => {
      console.log("reader",reader);
      this.Archivo = reader.result || null;
      console.log(reader.result);
    };
  }

   SubirCartaRespuestaACM()  {

    if(this.Archivo==null || this.Archivo == ''){
      this.dialogService.openMsgErrorDialog("Selecciona la carta de respuesta a subir");
      return;
    }
    this.dialogService
        .openConfirmDialog('¿Está seguro que desea subir este documento?')
        .afterClosed()
        .subscribe((res) => {
          if (res) {
            const oBE = {
              "LaserficheID": "", //this.LaserFicheID,
              "NroReclamo": this.RequerimientoDetalle.nroRequerimiento,
              "Archivo": this.Archivo,
              "NombreArchivo": this.NombreArchivo,
              "CodRequerimiento": +this.RequerimientoDetalle.codRequerimiento,
              "UsuarioRegistro": this.securityService.leerUsuarioWeb(),
              "CodTabla": Constantes.CartaRespuesta
              };
              this.loading = true;
              this.requerimientoServices.SubirArchivo(oBE).subscribe(result=>{
                this.loading = false;
                debugger;
                if(result.response.resultadoOK){
                //  this.bCartaRespuesta = true;
                 // this.LaserFicheIDCartaRespuesta = result.response.laserficheID;
                  this.dialogService.openMsgSuccessDialog("Se subió correctamente").afterClosed().subscribe(res=>{
                    if(res){

                      //this.eventDerivadoOCM.emit(false);
                      //window.location.reload();
                      this.reload();
                    }
                  });
                }
                else{
                //  this.bCartaRespuesta = false;
                  this.dialogService.openMsgErrorDialog("Se presento un problema para subir la carta de resuesta").afterClosed().subscribe(res=>{
                    if(res){
                      //this.eventDerivadoOCM.emit(true);
                    }
                  });
                }

              },
              (err)=>{
                this.loading = false;
                console.log(err);
              }
              );

          }
     });

  }

  changeUsuarioAsignado(){
    if(this.UsuarioAsignado==""){
      this.DesabilitarBotonAsignar = true;
    }
    if(this.UsuarioAsignado==null){
      this.DesabilitarBotonAsignar = true;
    }
    this.DesabilitarBotonAsignar = false;
  }

  reload():void {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this.router.navigate([currentUrl]);
        console.log(currentUrl);
    });
  }
}
