﻿using Agrobanco.SAR.Datos;
using Agrobanco.SAR.Entidades.ConstanciaNoAdeudo;
using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.LogicaNegocio
{
    public class ConstanciaNoAdeudoLogica
    {
        private readonly ConstanciaNoAdeudoDatos constanciaNoAdeudoDatos;

        public ConstanciaNoAdeudoLogica()
        {
            constanciaNoAdeudoDatos = new ConstanciaNoAdeudoDatos();
        }
        public ConstanciaNoAdeudoResponse Obtener(ConstanciaNoAdeudoRequest request)
        {
            return constanciaNoAdeudoDatos.Obtener(request);
        }
    }
}
