﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Security;
using System.Threading.Tasks;

namespace Agrobanco.Reclamaciones.API.Comun
{
    public static class Services
    {
        public static T Post<T>(object data, string rutaMetodo, string token)
        {
            string urlRequest = Util.ObtenerValueFromKey("UrlSARAPI") + rutaMetodo;
            var _response = new Object();
            try
            {
                ServicePointManager.ServerCertificateValidationCallback = new
                RemoteCertificateValidationCallback
                (
                   delegate { return true; }
                );
                var jsonData =  JsonConvert.SerializeObject(data);
                // Prueba de obtención de alumno vía HTTP GET
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(urlRequest);
                req.Method = "post";
                req.ContentType = "application/json";
                
                if(token.Length > 0)
                {
                    req.Headers.Add("Token", token);
                }

                using (var streamWriter = new StreamWriter(req.GetRequestStream()))
                {
                    string json = jsonData;

                    streamWriter.Write(json);
                }

                var httpResponse = (HttpWebResponse)req.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    _response = streamReader.ReadToEnd();
                }
                T _lista = JsonConvert.DeserializeObject<T>(_response.ToString());

                return _lista;
            }
            catch (Exception ex )
            {
                throw ex;
            }
        }

        public static T PostHeaderApp<T>(object data, string rutaMetodo, string appkey, string appcode, out string authorizationResponse)
        {
            string urlRequest = Util.ObtenerValueFromKey("UrlSARAPI") + rutaMetodo;
            var _response = new Object();
            try
            {
                ServicePointManager.ServerCertificateValidationCallback = new
                RemoteCertificateValidationCallback
                (
                   delegate { return true; }
                );
                var jsonData = JsonConvert.SerializeObject(data);
                // Prueba de obtención de alumno vía HTTP GET
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(urlRequest);
                req.Method = "post";
                req.ContentType = "application/json";
                req.Headers.Add("X-AppKey", appkey);
                req.Headers.Add("X-AppCode", appcode);


                using (var streamWriter = new StreamWriter(req.GetRequestStream()))
                {
                    string json = jsonData;

                    streamWriter.Write(json);
                }

                var httpResponse = (HttpWebResponse)req.GetResponse();
                var headers = httpResponse.Headers;
                string authorization = headers.GetValues("Authorization").FirstOrDefault();
                authorizationResponse = authorization;
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    _response = streamReader.ReadToEnd();
                    
                }
                T _lista = JsonConvert.DeserializeObject<T>(_response.ToString());

                return _lista;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
