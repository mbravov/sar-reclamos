﻿using Agrobanco.SAR.API.Models;
using Agrobanco.SAR.Control.Filters;
using Agrobanco.SAR.Entidades;
using Agrobanco.SAR.LogicaNegocio;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.SAR.API.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/ubigeo/listar")]
    [ApiController]
    public class UbigeoController : ControllerBase
    {
        private readonly UbigeoLogica ubigeoLogica;

        public UbigeoController()
        {
            ubigeoLogica = new UbigeoLogica();
        }


        [HttpPost("departamento")]
        [ProducesResponseType(200, Type = typeof(List<UbigeoBE>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult Departamento([Required][FromHeader(Name = "Token")] string token)
        {
            try
            {

                var listaResultado = ubigeoLogica.ListarDepartamento();

                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("provincia")]
        [ProducesResponseType(200, Type = typeof(List<UbigeoBE>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult Provincia([Required][FromHeader(Name = "Token")] string token, [FromBody] UbigeoBE oBE)
        {
            try
            {
                var listaResultado = ubigeoLogica.ListarProvincia(oBE);
                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("distrito")]
        [ProducesResponseType(200, Type = typeof(List<UbigeoBE>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult Distrito([Required][FromHeader(Name = "Token")] string token, [FromBody] UbigeoBE oBE)
        {
            try
            {
                var listaResultado = ubigeoLogica.ListarDistrito(oBE);
                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
