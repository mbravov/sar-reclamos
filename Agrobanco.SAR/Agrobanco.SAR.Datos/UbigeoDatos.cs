﻿using Agrobanco.SAR.Datos.SqlServer.Acceso;
using Agrobanco.SAR.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Agrobanco.SAR.Datos
{
    public class UbigeoDatos
    {
        public string cadenaConexion;
        public List<UbigeoBE> ListarDepartamento()
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            UbigeoBE objUbigeoBE = null;
            List<UbigeoBE> lstUbigeoBE = new List<UbigeoBE>();
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_UBIGEO_OBTENER_DEPARTAMENTO]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        //cmd.Parameters.Add("@grupo", SqlDbType.Int).Value = Grupo;
                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    objUbigeoBE = new UbigeoBE();
                                  
                                    if (!Convert.IsDBNull(reader["CodDepartamento"])) objUbigeoBE.CodDepartamento = Convert.ToString(reader["CodDepartamento"]);
                                    if (!Convert.IsDBNull(reader["Departamento"])) objUbigeoBE.Departamento = Convert.ToString(reader["Departamento"]);

                                    lstUbigeoBE.Add(objUbigeoBE);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw ex;
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return lstUbigeoBE;
        }

        public List<UbigeoBE> ListarProvincia(UbigeoBE oBE)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            UbigeoBE objUbigeoBE = null;
            List<UbigeoBE> lstUbigeoBE = new List<UbigeoBE>();
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_UBIGEO_OBTENER_PROVINCIA]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@CodDepartamento", SqlDbType.VarChar,2).Value = oBE.CodDepartamento;
                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    objUbigeoBE = new UbigeoBE();

                                    if (!Convert.IsDBNull(reader["CodProvincia"])) objUbigeoBE.CodProvincia = Convert.ToString(reader["CodProvincia"]);
                                    if (!Convert.IsDBNull(reader["Provincia"])) objUbigeoBE.Provincia = Convert.ToString(reader["Provincia"]);

                                    lstUbigeoBE.Add(objUbigeoBE);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw ex;
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return lstUbigeoBE;
        }

        public List<UbigeoBE> ListarDistrito(UbigeoBE oBE)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            UbigeoBE objUbigeoBE = null;
            List<UbigeoBE> lstUbigeoBE = new List<UbigeoBE>();
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_UBIGEO_OBTENER_DISTRITO]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@CodDepartamento", SqlDbType.VarChar, 2).Value = oBE.CodDepartamento;
                        cmd.Parameters.Add("@CodProvincia", SqlDbType.VarChar, 2).Value = oBE.CodProvincia;
                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    objUbigeoBE = new UbigeoBE();

                                    if (!Convert.IsDBNull(reader["CodUbigeo"])) objUbigeoBE.CodUbigeo = Convert.ToString(reader["CodUbigeo"]);
                                    if (!Convert.IsDBNull(reader["Distrito"])) objUbigeoBE.Distrito = Convert.ToString(reader["Distrito"]);

                                    lstUbigeoBE.Add(objUbigeoBE);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw ex;
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return lstUbigeoBE;
        }
    }
}
