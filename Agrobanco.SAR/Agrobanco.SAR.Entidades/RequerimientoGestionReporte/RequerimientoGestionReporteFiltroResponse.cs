﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades.RequerimientoGestionReporte
{
    public class RequerimientoGestionReporteFiltroResponse
    {
		public DateTime? FechaRegistro { get; set; }
		public DateTime? FechaAsignacion { get; set; }
		public DateTime? FechaGestion { get; set; }
		public DateTime? FechaDerivacionOCM { get; set; }
		public DateTime? FechaSinNotificacion { get; set; }
		public DateTime? FechaAtencion { get; set; }
		public string DetalleReclamo { get; set; }
		public string Estado { get; set; }
		public string Origen { get; set; }
		public string UsuarioAsignado { get; set; }
		public int DiasTranscurrido { get; set; }
		public string NroRequerimiento { get; set; }
	}
}
