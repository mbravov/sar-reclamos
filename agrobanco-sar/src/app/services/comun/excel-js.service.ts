import { Injectable } from '@angular/core';
//import { Workbook } from  'exceljs';
//import { Workbook } from 'exceljs/dist/exceljs';
import * as fs from 'file-saver';
//import { Workbook } from "exceljs/dist/exceljs";
import { Workbook } from 'exceljs';

@Injectable({
  providedIn: 'root'
})
export class ExcelJsService {

  constructor() { }

  DescargarExcelGenerico(title:string,sheetName:string,header:any[],body:any[],header_width:any[]):void{
    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet(sheetName);

      //Add Header Row
    let headerRow = worksheet.addRow(header);

    for (let index = 0; index < header_width.length; index++) {
      const element = header_width[index];
      worksheet.columns[index].width = element;
    }
    // Cell Style : Fill and Border

    headerRow.eachCell((cell:any, number:any) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'E8E8E8' },
        bgColor: { argb: 'E8E8E8' },
      };
      cell.border = {
        top: { style: 'thin' },
        left: { style: 'thin' },
        bottom: { style: 'thin' },
        right: { style: 'thin' },
      };
      cell.font = {
        size: 9,       
        bold: true,
      };
    });
    debugger;
    body.forEach((datos:any) => {
      let row = worksheet.addRow(datos);
 /*
      let qty = row.getCell(5);
      let color = 'FF99FF99';

   if (+qty.value < 500) {
        color = 'FF9999';
      }

      qty.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: color },
      };

    */
    });
    worksheet.addRow([]);
    workbook.xlsx.writeBuffer().then((data:any) => {
      debugger;
      let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, title +'.xlsx');
    });

  }
}
