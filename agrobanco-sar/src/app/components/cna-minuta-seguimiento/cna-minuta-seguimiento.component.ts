import { Component, OnInit, ViewChild, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder, ValidatorFn } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router, ActivatedRoute } from "@angular/router";
import { ExcelJsService } from 'src/app/services/comun/excel-js.service';
import { ParametroService } from 'src/app/services/parametro.service';
import { SecurityService } from 'src/app/services/security.service';

import { environment } from 'src/environments/environment';
import { Observable, Subject, forkJoin} from 'rxjs';
import * as moment from 'moment';

import { DialogService } from 'src/app/services/comun/dialog.service';
import { DomService } from 'src/app/services/dom.service';
import { CnaMinutaOcmService } from 'src/app/services/cna-minuta-ocm.service';
import { CnaSeguimientoConsulta } from '../models/cnaseguimiento.interface';

declare var M:any;
const UrlBase_SARAPI = environment.UrlBase_SARAPI;

@Component({
 selector: 'app-cna-minuta-seguimiento',
  templateUrl: './cna-minuta-seguimiento.component.html',
  styleUrls: ['./cna-minuta-seguimiento.component.css']
})
export class CnaMinutaSeguimientoComponent implements OnInit {

  TokenSGS = localStorage.getItem('TokenSGS');

  busquedaFiltro! : FormGroup;
  ELEMENT_DATA! :CnaSeguimientoConsulta[];
  displayedColumns: string[] = ["fechaCancelacion", "nombresApellidos", "nroDocumento", "banco", 
  "agencia", "analistaProponente",  "estado", "derivadoA","fechaDerivacion" ,'procesamiento'];

  listaCnaMinuta= new MatTableDataSource<CnaSeguimientoConsulta>(this.ELEMENT_DATA);
  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;  
  agencias:any[] = []; 
  estados:any[] = [];  
  fechaCancelacionInicioValor! : Date;
  fechaCancelacionFinValor! : Date;  
  peticionesTerminadas: boolean = true;

  constructor(
    private parametroService: ParametroService,
    private cnaMinutaOcmService : CnaMinutaOcmService,
    private securityService: SecurityService,
    private route: ActivatedRoute,
    private excelJsService:ExcelJsService
  ) {}

  ngOnInit(): void {
    this.inicializarFormulario();       
    this.loadPage();   
    this.listaCnaMinuta.paginator = this.paginator; 
  }
  
  loadPage(): void {
    
    this.peticionesTerminadas = false;
    let params = this.busquedaFiltro.value;
    params.Perfil = this.securityService.leerPerfil();
    params.UsuarioWeb = this.securityService.leerUsuarioWeb();
    debugger;
    let fechaIni = params.FechaCancelacionIni.toLocaleDateString().split('/');
    params.FechaCancelacionIni = fechaIni[2]+'-'+fechaIni[1]+'-'+fechaIni[0];
    if(params.FechaCancelacionFin !=""){
      let fechaFin = params.FechaCancelacionFin.toLocaleDateString().split('/');
      params.FechaCancelacionFin = fechaFin[2]+'-'+fechaFin[1]+'-'+fechaFin[0];
    }
        
    forkJoin([
     // this.parametroService.ObtenerAgencias(),
      this.cnaMinutaOcmService.ObtenerAgencias(params),
      this.cnaMinutaOcmService.ObtenerSeguimientoLista(params),
      this.cnaMinutaOcmService.ObtenerEstadosGeneral()
      
    ]).subscribe( (data:any) => {
      
        console.log(data);
      
        this.agencias = data[0].response;      
        this.listaCnaMinuta.data = data[1].response as CnaSeguimientoConsulta[];
        this.estados = data[2].response;  
        this.peticionesTerminadas = true;
    }, (error) =>{
      console.log(error);
      this.peticionesTerminadas = true;
    });

  }

  private inicializarFormulario()
  {   
    let fechaHoy = new Date();
    let f = new Date();
    let fechaInicioMes = new Date(f.setDate(fechaHoy.getDate() - 1));
    this.fechaCancelacionInicioValor = fechaInicioMes;
    this.fechaCancelacionFinValor = fechaHoy;  

    this.busquedaFiltro = new FormGroup({
      Perfil : new FormControl(''), 
      CodEstado : new FormControl(''),      
      CodAgencia : new FormControl(''),
      NroDocumento : new FormControl('',[ Validators.minLength(8)]),
      FechaCancelacionIni: new FormControl(fechaInicioMes, [Validators.max(this.fechaCancelacionFinValor.getTime()), Validators.required]),      
      FechaCancelacionFin: new FormControl(fechaHoy, [Validators.min(this.fechaCancelacionInicioValor.getTime())]) 
    });    
  } 
  
  
  buscarListaCnaMinuta():void
  {
    
    if(this.busquedaFiltro.invalid)
    {
      return;
    }

    this.peticionesTerminadas = false;
    let params = this.busquedaFiltro.value;
    params.Perfil = this.securityService.leerPerfil();
    debugger;
    if (params.FechaCancelacionIni.toLocaleString().length <= 19){  
      let fechaIni = params.FechaCancelacionIni.toLocaleDateString().split('/');
      params.FechaCancelacionIni = fechaIni[2]+'-'+fechaIni[1]+'-'+fechaIni[0];
    }
  
    if(params.FechaCancelacionFin !=""){
      if (params.FechaCancelacionFin.toLocaleString().length <= 19){  
        let fechaFin = params.FechaCancelacionFin.toLocaleDateString().split('/');
        params.FechaCancelacionFin = fechaFin[2]+'-'+fechaFin[1]+'-'+fechaFin[0];
      }
    }

      this.cnaMinutaOcmService.ObtenerSeguimientoLista(params).subscribe((result:any) =>{
      this.listaCnaMinuta.data = result.response as CnaSeguimientoConsulta[];
      this.peticionesTerminadas = true;

    }, (error)=> {
      console.log(error);
      this.peticionesTerminadas = true;
    });
  }

  
  ExportarExcel():void {
    if (this.busquedaFiltro.invalid) {
      return;
    }
    var request = {
      CodAgencia : this.busquedaFiltro.controls["CodAgencia"].value,
      CodEstado : this.busquedaFiltro.controls["CodEstado"].value,
      NroDocumento: this.busquedaFiltro.controls["NroDocumento"].value,     
      FechaCancelacionIni : this.busquedaFiltro.controls["FechaCancelacionIni"].value,
      FechaCancelacionFin: this.busquedaFiltro.controls["FechaCancelacionFin"].value     
    };
    debugger;
    if (request.FechaCancelacionIni.toLocaleString().length <= 19){  
      let fechaIni = request.FechaCancelacionIni.toLocaleDateString().split('/');
      request.FechaCancelacionIni = fechaIni[2]+'-'+fechaIni[1]+'-'+fechaIni[0];
    }
    if(request.FechaCancelacionFin !=""){
      if (request.FechaCancelacionFin.toLocaleString().length <= 19){  
          let fechaFin = request.FechaCancelacionFin.toLocaleDateString().split('/');
          request.FechaCancelacionFin = fechaFin[2]+'-'+fechaFin[1]+'-'+fechaFin[0];
      }
    }

    this.cnaMinutaOcmService.ObtenerSeguimientoLista(request).subscribe(result=>{ 

        this.listaCnaMinuta.data = result.response as CnaSeguimientoConsulta[];      

        const FechaActual = new Date();
        const AnioActual = FechaActual.getFullYear();
        const MesActual = (FechaActual.getMonth()<=9? '0'+ (FechaActual.getMonth()+1): FechaActual.getMonth() + 1); //FechaActual.getMonth() + 1;
        const DiaActual = (FechaActual.getDate()<=9? '0'+FechaActual.getDate(): FechaActual.getDate());
        const HoraActual = (FechaActual.getHours()<=9? '0'+FechaActual.getHours(): FechaActual.getHours()); //FechaActual.getHours();
        const MinutoActual = (FechaActual.getMinutes()<=9? '0'+FechaActual.getMinutes(): FechaActual.getMinutes()); //FechaActual.getMinutes();
        const SegundosActual = (FechaActual.getSeconds()<=9? '0'+FechaActual.getSeconds(): FechaActual.getSeconds()); //FechaActual.getMinutes(); //FechaActual.getSeconds();

        const Archivo = "CNA_MIN_Seguimiento" + "_" + AnioActual + MesActual + DiaActual + "T" + HoraActual + MinutoActual + SegundosActual;
        const Sheet = "Hoja1";
        const header = ["Fecha Cancelacion","Nombres y Apellidos","N° de Documento","Banco","Agencia","Analista Proponente","Estado","Derivado a","Fecha Derivación","Procesamiento"];
        const header_width = [20,20,20,20,20,20,25,25,20,25];
        let body = [];
        for (let index = 0; index < result.response.length; index++) {
          const datos = result.response[index];
          var fechaCancelacion = (datos.fechaCancelacion==null?'': moment(datos.fechaCancelacion).format("DD/MM/YYYY"));
          var nombreApellidos = (datos.nombresApellidos==null?'': datos.nombresApellidos);          
          var nroDocumento = (datos.nroDocumento==null?'': datos.nroDocumento); 
          var banco = (datos.banco==null?'': datos.banco);          
          var agencia = (datos.agencia==null?'': datos.agencia); 
          var analistaProponente = (datos.analistaProponente==null?'': datos.analistaProponente);          
          var estado = (datos.estado==null?'': datos.estado);
          var derivadoA = (datos.derivadoA==null?'': datos.derivadoA);
          //var fechaDerivacion = (datos.fechaDerivacion==null?'': moment(datos.fechaDerivacion).format("DD/MM/YYYY")); 
          var fechaDerivacion =(datos.fechaDerivacion==null?'': datos.fechaDerivacion);
          var procesamiento = (datos.procesamientoDesc==null?'': datos.procesamientoDesc);
         
          var datoarray = [fechaCancelacion,nombreApellidos,nroDocumento,banco,agencia,analistaProponente,
            estado,derivadoA,fechaDerivacion,procesamiento]
          body.push(datoarray)
        }

        this.excelJsService.DescargarExcelGenerico(Archivo,Sheet,header,body,header_width);
    },(err)=>{
      console.log(err)
    });

    
  }
}
