import { Component, OnInit, ViewChild, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder, ValidatorFn } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router, ActivatedRoute } from "@angular/router";

import { ParametroService } from 'src/app/services/parametro.service';
import { SecurityService } from 'src/app/services/security.service';
import { Constantes } from 'src/app/comun/constantes';
import { RequerimientoService } from 'src/app/services/requerimiento.service';
import { RequerimientoInterface } from '../models/requerimiento.interface';
import { RequerimientoConsulta } from '../models/requerimientoconsulta.interface';
import { environment } from 'src/environments/environment';
import { Observable, Subject, forkJoin} from 'rxjs';
import { DialogService } from 'src/app/services/comun/dialog.service';
import { DomService } from 'src/app/services/dom.service';

declare var M:any;
const UrlBase_SARAPI = environment.UrlBase_SARAPI;
@Component({
  selector: 'app-gestion-requerimiento',
  templateUrl: './gestion-requerimiento.component.html',
  styleUrls: ['./gestion-requerimiento.component.css'],

})
export class GestionRequerimientoComponent implements OnInit {

  TokenSGS = localStorage.getItem('TokenSGS');
  private subject = new Subject<RequerimientoConsulta>();

  Requerimiento_Detalle!: RequerimientoInterface[];
  busquedaFiltro! : FormGroup;
  ELEMENT_DATA! : RequerimientoConsulta[];
  displayedColumns: string[] = ["nroReclamo", "fechaRegistro", "nroDocumento", "nombresApellidos", "usuarioAsignado",
  "fechaAtencion",  "tipoRequerimiento", "agencia","descripcionEstado", "editar"];

  listaRequerimiento = new MatTableDataSource<RequerimientoConsulta>(this.ELEMENT_DATA);
  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;
  parametros:any[] = [];
  agencias:any[] = [];
  usuarios: any[] = [];
  estaBloqueado: Number = 0;
  fechaRegistroInicioValor! : Date;
  fechaRegistroFinValor! : Date;
  fechaAtencionInicioValor!: Date;
  fechaAtencionFinValor!: Date;
  EstaBloqueado: any;

  peticionesTerminadas: boolean = true;

  constructor(
    private parametroService: ParametroService,
    private requerimientoService : RequerimientoService,
    private securityService: SecurityService,
    private dialogService: DialogService,
    private domService: DomService,
    private route: ActivatedRoute,
    private router: Router) {
    this.EstaBloqueado = this.route.snapshot.params.id;

  }

    ngOnInit(): void {

      this.inicializarFormulario();
      this.loadPage();
      this.listaRequerimiento.paginator = this.paginator;
  }

  loadPage(): void {

    this.peticionesTerminadas = false;
    let params = this.busquedaFiltro.value;
    console.log(params);

    forkJoin([
      this.parametroService.ObtenerAgencias(),
      this.parametroService.ObtenerParametro(),
      this.parametroService.ObtenerUsuarios(),
      this.requerimientoService.ObtenerRequerimientoLista(params)

    ]).subscribe( (data:any) => {

        console.log(data);
        this.agencias = data[0].response;
        this.parametros = data[1].response;
        this.usuarios = data[2].response;
        this.listaRequerimiento.data = data[3].response as RequerimientoConsulta[];

        this.peticionesTerminadas = true;
    }, (error) =>{
      console.log(error);
      this.peticionesTerminadas = true;
    });

  }

  private inicializarFormulario()
  {

    let fechaInicioMes = new Date(new Date().getFullYear(), new Date().getMonth(), 1);
    let fechaHoy = new Date();

    this.fechaRegistroInicioValor = fechaInicioMes;
    this.fechaRegistroFinValor = fechaHoy;
    this.fechaAtencionInicioValor = fechaHoy;
    this.fechaAtencionFinValor = fechaHoy;

    this.busquedaFiltro = new FormGroup({
      UsuarioAsignado : new FormControl(''),
      CodigoEstado : new FormControl(''),
      CodAgencia : new FormControl(''),
      NroDocumento : new FormControl(''),
      NombresApellidos : new FormControl(''),
      Fecharegistroini: new FormControl(fechaInicioMes, [Validators.max(this.fechaRegistroFinValor.getTime())]),
      Fecharegistrofin: new FormControl(fechaHoy, [Validators.min(this.fechaRegistroInicioValor.getTime())]),
      Fechaatencionini: new FormControl('', [Validators.max(this.fechaAtencionFinValor.getTime())]),
      Fechaatencionfin: new FormControl('', [Validators.min(this.fechaAtencionInicioValor.getTime())]),
      NroReclamo : new FormControl('')

    });
  }

  buscarListaRequerimiento():void
  {

    if(this.busquedaFiltro.invalid)
    {
      return;
    }

    this.peticionesTerminadas = false;
    let params = this.busquedaFiltro.value;
    console.log(params);

    this.requerimientoService.ObtenerRequerimientoLista(params).subscribe((result:any) =>{
      this.listaRequerimiento.data = result.response as RequerimientoConsulta[];
      this.peticionesTerminadas = true;
      
    }, (error)=> {
      console.log(error);
      this.peticionesTerminadas = true;
    });

  }

  editarRequerimiento(nroReclamo: string):void  {

    debugger;

    this.busquedaFiltro.controls['NroReclamo'].setValue(nroReclamo);

    let usuario = this.securityService.leerUsuarioWeb();
    const headers = {
      'Content-Type': 'application/json',
      'Token': this.TokenSGS
    }
    let parametro = {
      nroReclamo : nroReclamo
    };

    this.requerimientoService.ObtenerBloqueado(parametro).subscribe((result:any) =>{
debugger;
    if (result.response[0].estaBloqueado == 1 && result.response[0].usuarioBloqueo != usuario)
    {
      this.openModalError(nroReclamo,result.response[0].nombresApellidos);
    }
    else{

      const params ={
        NroReclamo : nroReclamo,
        UsuarioRegistro : usuario,
        EstaBloqueado : Constantes.EstaBloqueado

        };

          this.requerimientoService.ActualizarRequerimientoBloqueado(params).subscribe((result:any) =>{
            console.log("response",result.response);
          });

          this.router.navigate(['/gestion/requerimiento-detalle', nroReclamo]);

       }

    });

  }

  openModalError(nroReclamo: string,nombresApellidos:string):void
  {
    debugger;
    if (nroReclamo != "") {

      this.dialogService.openMsgErrorDialog('Requerimiento seleccionado esta siendo revisado por el usuario : ' + nombresApellidos)
      .afterClosed().subscribe(res => {

      });

    }

  }

  onChangeFechaRegistroIni(){
    this.fechaRegistroInicioValor = this.busquedaFiltro.controls['Fecharegistroini'].value;
  }

  onChangeFechaRegistroFin(){
    this.fechaRegistroFinValor = this.busquedaFiltro.controls['Fecharegistrofin'].value;
  }

  onChangeFechaAtencionIni(){
    this.fechaAtencionInicioValor = this.busquedaFiltro.controls['Fechaatencionini'].value;
  }

  onChangeFechaAtencionFin(){
    this.fechaAtencionFinValor = this.busquedaFiltro.controls['Fechaatencionfin'].value;
  }


}




