﻿using Agrobanco.SAR.DAL.Entities;
using Agrobanco.SAR.DAL.Laserfiche;
using Spire.Doc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SAR.DAL.GeneradorDocumento
{
    public class GeneradorDocumento
    {
        public string GeneradorGestionCNA(string ArchivoBytes, CreditoCliente oCreditoCliente, List<Parametros> lstParametro)
        {
            
            try
            {
                var ms = new MemoryStream(Convert.FromBase64String(ArchivoBytes));

                Document document = new Document();
                document.LoadFromStream(ms, FileFormat.Docx);

                foreach (var itemParametro in lstParametro)
                {
                    if (itemParametro.Referencia == "1")
                    {
                        //string mes2 = fechaActual.ToString "MMMM", CultureInfo.CreateSpecificCulture("en-US"));
                        //string FechaString = DateTime.Now.Day.ToString().PadLeft(0, '2') + " " +  DateTime.Now.ToString("MMMM",CultureInfo.CreateSpecificCulture("es-ES")) + " de " + DateTime.Now.Year.ToString();
                        string FechaString = DateTime.Now.Day.ToString().PadLeft(0, '2') + " " + DateTime.Now.ToString("MMMM", CultureInfo.CreateSpecificCulture("es-ES")) + " de " + DateTime.Now.Year.ToString();
                        document.Replace(itemParametro.Descripcion, FechaString, false, true);
                    }
                    else
                    {
                        PropertyInfo[] lst = typeof(CreditoCliente).GetProperties();
                        foreach (PropertyInfo oProperty in lst)
                        {
                            if (itemParametro.Valor == oProperty.Name)
                            {
                                if (itemParametro.Referencia == "2")
                                {
                                    if (oProperty.GetValue(oCreditoCliente) != null)
                                    {
                                        string Valor = oProperty.GetValue(oCreditoCliente).ToString();
                                        document.Replace(itemParametro.Descripcion, Valor, false, true);
                                        break;
                                    }
                                    else
                                    {
                                        document.Replace(itemParametro.Descripcion, "", false, true);
                                    }

                                }
                                else
                                {
                                    if (oProperty.GetValue(oCreditoCliente) != null)
                                    {
                                        DateTime FechaSistema = (DateTime)oProperty.GetValue(oCreditoCliente);
                                        //   DateTime FechaSistema = Convert.ToDateTime(Valor);
                                        string FechaString = FechaSistema.Day.ToString().PadLeft(0, '2') + " " + FechaSistema.ToString("MMMM") + " de " + FechaSistema.Year.ToString();
                                        document.Replace(itemParametro.Descripcion, FechaString, false, true);
                                        break;
                                    }
                                    else
                                    {
                                        document.Replace(itemParametro.Descripcion, "", false, true);
                                    }
                                }
                            }
                        }
                    }
                }

                MemoryStream streamOut = new MemoryStream();
                document.SaveToFile(streamOut, FileFormat.Docx);

                byte[] imageArray = streamOut.ToArray();
                string base64ImageRepresentation = Convert.ToBase64String(imageArray);
                return base64ImageRepresentation;

                
            }
            catch (Exception ex)
            {
                throw ex;

            }



        }
      
    }
}
