import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { forkJoin, Observable, of } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { Usuario } from '../components/models/usuario.interface';
import { DataSeguridad } from '../components/models/dataseguridad.interface';
import { Modulo } from '../components/models/modulo.interface';
import { AuthAccess } from '../components/models/authaccess.interface';

const UrlBase_SGSAPI = environment.UrlBase_SGSAPI;
const UrlBase_SSA = environment.UrlBase_SSA;
const UrlBase_SARAPI = environment.UrlBase_SARAPI;
const APP_CODE = environment.AppCode;
const APP_KEY = environment.AppKey;

@Injectable({
  providedIn: 'root'
})
export class SecurityService {
  //private TokenSGS! : string;
  public modulos: Modulo[] = [];

  constructor() { }

  

  public guardarToken( token: any ): void {
    localStorage.setItem('tokenSGS', token.toString());
  }

  public guardaridPerfil( idPerfil: string | any ): void {
    localStorage.setItem('idperfil', idPerfil.toString());
  }

  public guardarDataSeguridad(data : DataSeguridad)
  {
    localStorage.setItem('vEmail', data.vEmail.toString());
    localStorage.setItem('vNombre',  data.vNombre.toString());
    localStorage.setItem('UsuarioWeb',  data.vUsuarioWeb.toString());    
  }

  public leerEmail(): any {
    
    let email: any;

    if ( localStorage.getItem('vEmail') ) {
      email = localStorage.getItem('vEmail');
    } else {
      email = '';
    }

    return email;
  }

  public leerTokenSeguridad(): any {

    let Token: any;

    if ( localStorage.getItem('tokenSGS') ) {
      Token = localStorage.getItem('tokenSGS');
    } else {
      Token = '';
    }

    return Token;
  }

  public leeridPerfil(): any {

    let idperfil: any;

    if ( localStorage.getItem('idperfil') ) {
      idperfil = localStorage.getItem('idperfil');
    } else {
      idperfil = '';
    }

    return idperfil;
  }

  public leerUsuarioWeb(): any {

    let usuarioWeb: any;

    if ( localStorage.getItem('UsuarioWeb') ) {
      usuarioWeb = localStorage.getItem('UsuarioWeb');
    } else {
      usuarioWeb = '';
    }

    return usuarioWeb;
  }

  public leerNombreUsuario(): any {

    let nombreUsuario: any;

    if ( localStorage.getItem('vNombre') ) {
      nombreUsuario = localStorage.getItem('vNombre');
    } else {
      nombreUsuario = '';
    }

    return nombreUsuario;
  }

  public validarToken(): Observable<any>{
    let tokenSGS = this.leerTokenSeguridad();
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${ tokenSGS }`
    }

    const req = ajax.get(`${UrlBase_SGSAPI}/validate/token`, headers);

    const data$ = new Observable(observer=> {
      req.subscribe(
        (res)=>{
          observer.next(res);
          observer.complete();
        },
        (err)=>{
          observer.error(err);
        }
      );
    });

    return data$;
  }

  public obtenerModulos(): Observable<any>{
    let tokenSGS = this.leerTokenSeguridad();
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${ tokenSGS }`
    }

    let idPerfil = this.leeridPerfil();
    let usuarioWeb = this.leerUsuarioWeb();
    
    console.log(idPerfil);
    console.log(usuarioWeb);

    const req = ajax.get(`${UrlBase_SGSAPI}/modulo/obtenermodulos/${idPerfil}/${usuarioWeb}`, headers);

    const data$ = new Observable(observer=> {
      req.subscribe(
        (res)=>{
          observer.next(res);
          observer.complete();
        },
        (err)=>{
          observer.error(err);
        }
      );
    });

    return data$;
  }
  
  // public guardarModulos(data : Modulo[]) {
  //   //localStorage.setItem('modulos', JSON.stringify(data));
    
  //   if(data.length > 0)
  //   {
  //     this.guardarPerfil(data[0].NombrePerfil);
  //   }
    
  // }

  public guardarPerfil(perfil : string)
  {
    localStorage.setItem("perfil", perfil);
  }

  public leerModulos() {
    let modulos: any;
    var item = localStorage.getItem('modulos');
    if ( item != null ) {      
        modulos = JSON.parse(item);
    } else {
      modulos = null;
    }

    return modulos;
  }

  public tienePermisoModulo():boolean{
    return true;
  }

  public redirectLogin(): void {
    window.location.href = `${UrlBase_SSA}`;
  }
  
  public obtenerUsuario (filtro : Usuario): Observable<any> {
    
    let tokenSGS = this.leerTokenSeguridad();
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${ tokenSGS }`
    }

    const req = ajax.post(`${UrlBase_SGSAPI}/usuario/obtenerusuario`, filtro, headers);

    const data$ = new Observable(observer=> {
      req.subscribe(
        (res)=>{
          observer.next(res);
          observer.complete();
        },
        (err)=>{
          observer.error(err);
        }
      );
    });

    return data$;
  }

  public autenticarSAR(): Observable<any> {
    
    let email = this.leerEmail();
    let usuarioWeb = this.leerUsuarioWeb();
    let nombreUsuario = this.leerNombreUsuario();

    let filtros =  {
      correoelectronico : email,
      nombreusuario : nombreUsuario,
      codigousuario : usuarioWeb,
      numerodocumento : usuarioWeb
    };

    const headers = {
      'Content-Type': 'application/json',
      'X-AppKey' : `${ APP_KEY }`,
      'X-AppCode' : `${ APP_CODE }`
    }

    const req = ajax.post(`${UrlBase_SARAPI}/acceso`, filtros, headers);

    const data$ = new Observable(observer=> {
      req.subscribe(
        (res)=>{
          observer.next(res);
          observer.complete();
        },
        (err)=>{
          observer.error(err);
        }
      );
    });

    return data$;
  }

  guardarTokenSAR(tokenSAR: string){
    localStorage.setItem('tokenSAR', tokenSAR);
  }

  leerTokenSAR(){
    let tokenSAR: any;

    if ( localStorage.getItem('tokenSAR') ) {
      tokenSAR = localStorage.getItem('tokenSAR');
    } else {
      tokenSAR = '';
    }

    return tokenSAR;
  }

  //-----------WQS--------------//
  public leerAgencia(): any {
    let agencia: any;

    if ( localStorage.getItem('usAge') ) {
      agencia = localStorage.getItem('usAge');
    } else {
      agencia = '';
    }

    return agencia;
  }

  public leerPerfil(): any {

    let perfil: any;

    if ( localStorage.getItem('perfil') ) {
      perfil = localStorage.getItem('perfil');
    } else {
      perfil = '';
    }

    return perfil;
  }
  //----------------------------//

}
