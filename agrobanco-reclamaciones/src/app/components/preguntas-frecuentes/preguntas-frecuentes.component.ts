import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import jsPDF from 'jspdf';
import { Constantes } from 'src/app/comun/constantes';
import { DialogService } from 'src/app/services/comun/dialog.service';
import { RequerimientoService } from 'src/app/services/requerimiento.service';
import { TerminosCondicionesComponent } from '../terminos-condiciones/terminos-condiciones.component';
import { forkJoin } from 'rxjs';
import { environment } from 'src/environments/environment';
import * as EmailValidator from 'email-validator';
import { AppService } from 'src/app/app.service';
declare var M: any;

@Component({
  selector: 'app-preguntas-frecuentes',
  templateUrl: './preguntas-frecuentes.component.html',
  styleUrls: ['./preguntas-frecuentes.component.css']
})
export class PreguntasFrecuentesComponent implements OnInit {

  @ViewChild('myInputArchivo1', { static: false }) myInputArchivo1!: ElementRef;
  @ViewChild('myInputArchivo2', { static: false }) myInputArchivo2!: ElementRef;

  @ViewChild('myInputTextArchivo1', { static: false }) myInputTextArchivo1!: ElementRef;
  @ViewChild('myInputTextArchivo2', { static: false }) myInputTextArchivo2!: ElementRef;

  bMostrarArchvo1: boolean = false;
  bMostrarArchvo2: boolean = false;

  form!: FormGroup;
  parametros: any[] = [];
  parametrospreguntas: any[] = [];
  parametrosrespuestas: any[] = [];
  public isCollapsed = false;
  esRuc: boolean = false;
  loading: boolean = false;

  MaxLengthNombres: number = 100;
  MaxlengthDireccion: number = 200;
  MinlengthDireccion: number = 10;
  lengthDescripcion: number = 1000;

  Archivo1: any;
  Archivo2: any;
  Archivo3: any;
  Archivo4: any;

  NombreArchivo1: any;
  NombreArchivo2: any;
  NombreArchivo3: any;
  NombreArchivo4: any;

  bRegistroConsulta: boolean = false;
  siteKey!: string;
  recaptcha!: string;

  checkedTerminos: boolean = false;

  MAXIMO_TAMANIO_BYTES = 1000000; // 1MB = 1 millón de

  formSubmitted: boolean = false;

  /*esEmailValido(email: string):boolean {
    let mailValido = false;
      'use strict';

      var EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

      if (email.match(EMAIL_REGEX)){
        mailValido = true;
      }
    return mailValido;
  }*/

  constructor(
    private requerimientoServices: RequerimientoService,
    private formBuilder: FormBuilder,
    private dialogService: DialogService,
    private dialog: MatDialog,
    private appService: AppService
  ) {
    this.siteKey = environment.recaptcha_siteKey;
    console.log("this.siteKey", this.siteKey);
  }

  ngOnInit(): void {
    this.initForm();
    this.loadPage();
    var elems = document.querySelectorAll('.collapsible');
    var instances = M.Collapsible.init(elems, { accordion: true });

    this.bRegistroConsulta = false;
  }

  onChangeTipoDocumento(): void {
    const TipoDocumento = +this.form.controls['TipoDocumento'].value;
    this.form.controls['NombresCompletos'].setValue(null);

    /*if (TipoDocumento==Constantes.RUC)
     {
       this.esRuc = true;
     }
     else {
       this.esRuc = false;
     }*/

  }

  openDialogoTerminosCondiciones(): void {
    //   const dialogRef = this.dialog.open(TerminosCondicionesComponent);
    const Terminos = this.parametros.filter(x => x.grupo == 800);

    const dialogRef = this.dialog.open(TerminosCondicionesComponent, {
      data: { parametros: Terminos }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result.data}`);

      if (result.data == 1) {
        this.checkedTerminos = true;
      }
      else {
        this.checkedTerminos = false;
      }
    });
  }

  loadPage(): void {
    this.loading = true;

    this.appService.generateTokenReclamacionesAPI().subscribe((data: any) => {
      let token = data.headers.get('authorization');
      forkJoin([

        this.requerimientoServices.ObtenerParametroPreguntasFrecuentes(token),
        this.requerimientoServices.ObtenerParametro(token)

      ]).subscribe((data: any) => {
        debugger;
        console.log(data);
        this.parametrospreguntas = data[0].response;
        this.parametros = data[1].response;
        //this.parametrosrespuestas = data[2].response;

        this.loading = false;
      }, (err) => {
        this.loading = false;
        console.error(err);
      });
    }, (err) => {

      console.error(err);
    });


  }

  initForm(): void {
    this.form = this.formBuilder.group({
      TipoDocumento: [null, Validators.required],
      NroDocumento: [null, Validators.required],
      NombresCompletos: [null, Validators.required],
      Direccion: [null, [Validators.minLength(10)]],
      CorreoElectronico: [null, [Validators.required, Validators.pattern('^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$')]],
      Celular: [null, [Validators.required, Validators.pattern('^[0-9]{9}$')]],
      Descripcion: [null, Validators.required],
      Estado: [null],
      Archivo1: [null],
      Archivo2: [null]

    });
  }


  Registrar(): void {

    this.formSubmitted = true;

    if (this.form.invalid) {
      return;
    }
    const NroDocumento = this.form.controls['NroDocumento'].value;
    const NombresCompletos = this.form.controls['NombresCompletos'].value;
    const Direccion = this.form.controls['Direccion'].value;
    const CorreoElectronico = this.form.controls['CorreoElectronico'].value;
    const Celular = this.form.controls['Celular'].value;
    const Descripcion = this.form.controls['Descripcion'].value;
    // Estado = 1;
    const TipoDocumento = +this.form.controls['TipoDocumento'].value;
    if (TipoDocumento > 0 && NroDocumento == null) {
      this.dialogService.openMsgErrorDialog("Ingrese Número de Documento");
      return;

    }

    /*
        if(!this.esEmailValido(CorreoElectronico)){
          this.dialogService.openMsgErrorDialog("Formato Email Incorrecto");
          return;
      }*/


    if (this.recaptcha == null) {
      this.dialogService.openMsgErrorDialog("debe indicar que ud. no es un robot");
      return;
    }

    if (!this.checkedTerminos) {
      this.dialogService.openMsgErrorDialog("Debe aceptar los terminos y condiciones");
      return;
    }


    this.dialogService.openConfirmDialog('¿Desea registrar la consulta?')
      .afterClosed().subscribe(res => {
        if (res) {
          this.loading = true;
          const Entity = {

            TipoDocumento: +this.form.controls['TipoDocumento'].value,
            NroDocumento: this.form.controls['NroDocumento'].value,
            NombresCompletos: this.form.controls['NombresCompletos'].value,
            Direccion: this.form.controls['Direccion'].value,
            CorreoElectronico: this.form.controls['CorreoElectronico'].value,
            Celular: this.form.controls['Celular'].value,
            Descripcion: this.form.controls['Descripcion'].value,
            Estado: 1,
            UsuarioRegistro: "", //this.securityService.leerUsuarioWeb(),

            Archivo1: this.Archivo1,
            Archivo2: this.Archivo2,

            NombreArchivo1: this.NombreArchivo1,
            NombreArchivo2: this.NombreArchivo2

          }


          console.log("request", Entity);
          this.appService.generateTokenReclamacionesAPI().subscribe((data: any) => {
            let token = data.headers.get('authorization');
            this.requerimientoServices.ConsultaRegistrar(Entity, token).subscribe(result => {
              debugger;
              console.log("response", result);

              this.loading = false;
              if (result) {

                this.bRegistroConsulta = true;

                this.dialogService.openMsgSuccessDialog("Tu consulta fue enviada con éxito")
                  .afterClosed()
                  .subscribe((res) => {
                    if (res) {
                      //document.location.href = `/preguntas-frecuentes/`;
                      location.reload();
                    }

                  });

              }
              else {
                this.dialogService.openMsgErrorDialog("Hubo un error al intentar registrar el requerimiento");
              }

            }, (err) => {
              this.loading = false;
              console.log(err);
            });
          }, (err) => {

            console.error(err);
          });


        }

      });
  }


  handleUpload(event: any, numfile: number) {
    debugger;


    const file = event.target.files[0];
    if (file == null || file.length <= 0) {
      switch (numfile) {
        case 1:
          this.Archivo1 = null;
          this.NombreArchivo1 = null;
          this.form.controls['Archivo1'].setValue(null);
          this.bMostrarArchvo1 = false;
          break;
        case 2:
          this.Archivo2 = null;
          this.NombreArchivo2 = null;
          this.form.controls['Archivo2'].setValue(null);
          this.bMostrarArchvo2 = false;
          break;

        default:
          break;
      }

      return;
    }

    var allowedExtensions = /(.jpg|.jpeg|.png|.pdf)$/i;
    if (!allowedExtensions.exec(file.name)) {
      this.dialogService.openMsgErrorDialog("Formato no válido");
      switch (numfile) {
        case 1:
          this.Archivo1 = null;
          this.NombreArchivo1 = null;
          this.form.controls['Archivo1'].setValue(null);
          this.bMostrarArchvo1 = false;
          break;
        case 2:
          this.Archivo2 = null;
          this.NombreArchivo2 = null;
          this.form.controls['Archivo2'].setValue(null);
          this.bMostrarArchvo2 = false;
          break;


        default:
          break;
      }
      return;
    }
    if (file.size > this.MAXIMO_TAMANIO_BYTES) {
      const tamanioEnMb = this.MAXIMO_TAMANIO_BYTES / 1000000;

      switch (numfile) {
        case 1:
          this.Archivo1 = null;
          this.NombreArchivo1 = null;
          this.form.controls['Archivo1'].setValue(null);
          this.bMostrarArchvo1 = false;
          break;
        case 2:
          this.Archivo2 = null;
          this.NombreArchivo2 = null;
          this.form.controls['Archivo2'].setValue(null);
          this.bMostrarArchvo2 = false;
          break;


        default:
          break;
      }
      this.dialogService.openMsgErrorDialog(`El tamaño máximo es ${tamanioEnMb} MB`);


      // Limpiar
      // $miInput.value = "";
      return;
    } else {
      // Validación pasada. Envía el formulario o haz lo que tengas que hacer
    }

    const FileNamme = file.name;
    switch (numfile) {
      case 1:
        this.NombreArchivo1 = FileNamme;
        this.bMostrarArchvo1 = true;
        break;
      case 2:
        this.NombreArchivo2 = FileNamme;
        this.bMostrarArchvo2 = true;
        break;

      default:
        break;
    }

    const reader = new FileReader();
    console.log("file", file);
    reader.readAsDataURL(file);
    reader.onload = () => {
      console.log("reader", reader);
      switch (numfile) {
        case 1:
          this.Archivo1 = reader.result || null;
          break;
        case 2:
          this.Archivo2 = reader.result || null;
          break;
        default:
          break;
      }

      console.log(reader.result);
    };
  }

  resolved(captchaResponse: string) {

    this.recaptcha = captchaResponse;
    console.log(`Resolved captcha with response: ${captchaResponse}`);
  }

  LimpiarUpload(numfile: number) {

    switch (numfile) {
      case 1:
        this.myInputArchivo1.nativeElement.value = "";
        this.Archivo1 = null;
        this.NombreArchivo1 = null;
        this.form.controls['Archivo1'].setValue(null);
        this.bMostrarArchvo1 = false;
        this.myInputTextArchivo1.nativeElement.value = "";

        break;
      case 2:
        this.myInputArchivo2.nativeElement.value = "";
        this.myInputTextArchivo2.nativeElement.value = "";
        this.Archivo2 = null;
        this.NombreArchivo2 = null;
        this.form.controls['Archivo2'].setValue(null);
        this.bMostrarArchvo2 = false;

        break;


      default:
        break;
    }


  }
}
