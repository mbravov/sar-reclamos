﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades.CNA_Minuta
{
    public class CNAFilterRequest
    {
        public string Perfil { get; set; }
        public string CodEstado { get; set; }
        public string CodAgencia { get; set; }
        public string NroDocumento { get; set; }
        public string FechaCancelacionIni { get; set; }
        public string FechaCancelacionFin { get; set; }
        public string UsuarioWeb { get; set; }
        public string TipoDocumento { get; set; }
        public string Nombres { get; set; }
    }
}
