﻿using Agrobanco.SAR.Comun.Laserfiche;
using Agrobanco.SAR.Datos.SqlServer.Acceso;
using Agrobanco.SAR.Entidades;
using Agrobanco.SAR.Entidades.CNA_Minuta;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SAR.Datos
{
    public class ComentarioEmisionDatos  
    {
        public string cadenaConexion;
        public async Task<List<ComentarioEmisionBE>> Obtener(ComentarioEmisionBE request)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            ComentarioEmisionBE oComentarioEmisionBE = null;
            List<ComentarioEmisionBE> LstComentarioEmisionBE = new List<ComentarioEmisionBE>();
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("SP_COMENTARIO_EMISION_OBTENER", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@IdEmision", SqlDbType.Int).Value = request.IdEmision;
                       
                        try
                        {
                            using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                            {
                                while (reader.Read())
                                {
                                    oComentarioEmisionBE = new ComentarioEmisionBE();

                                    if (!reader.IsDBNull(0))
                                        oComentarioEmisionBE.IdComentarioEmision = reader.GetInt32(0);

                                    if (!reader.IsDBNull(1))
                                        oComentarioEmisionBE.IdEmision = reader.GetInt32(1);

                                    if (!reader.IsDBNull(2))
                                        oComentarioEmisionBE.IdDerivacion = reader.GetInt32(2);

                                    if (!reader.IsDBNull(3))
                                        oComentarioEmisionBE.IdDerivacionMovimiento = reader.GetInt32(3);

                                    if (!reader.IsDBNull(4))
                                        oComentarioEmisionBE.Texto = reader.GetString(4);

                                    if (!reader.IsDBNull(5))
                                        oComentarioEmisionBE.LaserficheID = reader.GetInt32(5).ToString();


                                    if (!reader.IsDBNull(6))
                                        oComentarioEmisionBE.FechaRegistro = reader.GetDateTime(6);


                                    if (!reader.IsDBNull(7))
                                        oComentarioEmisionBE.UsuarioRegistro = reader.GetString(7);

                                    if (!reader.IsDBNull(8))
                                        oComentarioEmisionBE.Motivo = reader.GetString(8);

                                    LstComentarioEmisionBE.Add(oComentarioEmisionBE);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            throw ex;
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return await Task.Run(() => LstComentarioEmisionBE);
        }
        public async Task<bool> InsertarAccion(ComentarioEmisionBE request)
        {
            bool resultado = false;
            int IdComentarioEmision = 0;
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("SP_COMENTARIO_EMISION_INSERTAR_ACCION", conn))
                    {
                        using (SqlTransaction tx = conn.BeginTransaction())
                        {
                            try
                            {
                                cmd.Transaction = tx;

                                cmd.CommandType = CommandType.StoredProcedure;

                                cmd.Parameters.Add("@IdEmision", SqlDbType.Int).Value = request.IdEmision;
                                cmd.Parameters.Add("@IdDerivacion", SqlDbType.Int).Value = request.IdDerivacion;
                                //cmd.Parameters.Add("@IdDerivacionMovimiento", SqlDbType.Int).Value = request.IdDerivacionMovimiento;
                                cmd.Parameters.Add("@Texto", SqlDbType.VarChar, 300).Value = request.Texto;
                                cmd.Parameters.Add("@UsuarioRegistro", SqlDbType.VarChar, 50).Value = request.UsuarioRegistro;

                                cmd.Parameters.Add("@Perfil", SqlDbType.VarChar, 5).Value = request.Perfil;
                                cmd.Parameters.Add("@CodAgencia", SqlDbType.Int).Value = request.CodAgencia;
                                cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = request.Estado;

                                if (string.IsNullOrEmpty(request.IdMotivo))
                                    cmd.Parameters.Add("@IdMotivo", SqlDbType.Int).Value = DBNull.Value;
                                else
                                    cmd.Parameters.Add("@IdMotivo", SqlDbType.Int).Value = Convert.ToInt32(request.IdMotivo);

                                SqlParameter pIdComentarioEmision = new SqlParameter("@IdComentarioEmision", SqlDbType.Int);
                                pIdComentarioEmision.Direction = ParameterDirection.Output;
                                cmd.Parameters.Add(pIdComentarioEmision);

                                await cmd.ExecuteNonQueryAsync();

                                IdComentarioEmision = Convert.ToInt32(cmd.Parameters["@IdComentarioEmision"].Value);

                                LaserficheProxy laserfiche = new LaserficheProxy();
                                DocumentoModel DocumentoModelRequest = new DocumentoModel();
                                DocumentoModel DocumentoModelResponse = new DocumentoModel();

                                DocumentoModelRequest.Folder = $@"\GESTION-CNA\" + request.DNI;



                                List<Documento> lstDocumento = new List<Documento>();
                                int item = 1;
                                string base64 = string.Empty;
                                if (!string.IsNullOrEmpty(request.Archivo))
                                {
                                    base64 = string.Empty;
                                    base64 = request.Archivo.Split(',')[1];

                                    DateTime fechaActual = DateTime.Now;
                                    string Dia = fechaActual.Day.ToString().PadLeft(2, '0');
                                    string Mes = fechaActual.Month.ToString().PadLeft(2, '0');
                                    string Anio = fechaActual.Year.ToString().PadLeft(2, '0');

                                    string Hora = fechaActual.Hour.ToString().PadLeft(2, '0');
                                    string Minutos = fechaActual.Minute.ToString().PadLeft(2, '0');
                                    string Segundos = fechaActual.Second.ToString().PadLeft(2, '0');

                                    Documento oDocumento = new Documento();
                                    oDocumento.Extension = System.IO.Path.GetExtension(request.NombreArchivo).Split('.')[1];
                                    oDocumento.ArchivoBytes = base64;
                                    oDocumento.Nombre = "COMENTARIO_" + request.IdEmision.ToString().PadLeft(3, '0') + "_" + Anio + Mes + Dia + "T" + Hora + Minutos + Segundos;
                                    oDocumento.Folder = "COMENTARIO-EMISION";
                                    lstDocumento.Add(oDocumento);
                                    item++;
                                }

                                if (lstDocumento.Count > 0)
                                {
                                    DocumentoModelRequest.ListaDocumentos = lstDocumento;
                                    DocumentoModelResponse = laserfiche.UploadListaDocumento(DocumentoModelRequest);

                                    RecursoDatos oRecursoDatos = null;
                                    RecursoBE oRecursoBE = null;
                                    foreach (var image in DocumentoModelResponse.ListaDocumentos)
                                    {
                                        if (image.ResultadoOK)
                                        {
                                            oRecursoDatos = new RecursoDatos();
                                            oRecursoBE = new RecursoBE();

                                            oRecursoBE.CodTabla = request.CodTabla;
                                            oRecursoBE.ReferenciaID = IdComentarioEmision;
                                            oRecursoBE.LaserficheID = image.CodigoLaserfiche;
                                            oRecursoBE.Estado = true;
                                            oRecursoBE.UsuarioRegistro = request.UsuarioRegistro;
                                            oRecursoBE.Descripcion = image.Nombre;
                                            Boolean resultad = oRecursoDatos.RegistrarTrans(oRecursoBE, conn, tx);

                                            if (!resultad)
                                                throw new Exception("Se presento un error al intentar registrar los archivos adjuntos");

                                        }
                                        else
                                        {
                                            throw new Exception("Se presento un error al subir el archivo");
                                        }
                                    }
                                    DocumentoModelResponse.ResultadoOK = true;

                                }

                                tx.Commit();
                                resultado = true;

                            }

                            catch (Exception ex)
                            {
                                tx.Rollback();
                                conn.Close();
                                conn.Dispose();
                                cmd.Dispose();
                                resultado = false;
                                throw (ex);
                            }
                            finally
                            {
                                conn.Close();
                                conn.Dispose();
                                cmd.Dispose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                resultado = false;
                throw ex;
            }
            return await Task.Run(() => resultado);
        }
        public async Task<bool> Insertar(ComentarioEmisionBE request)
        {
            bool resultado = false;
            int IdComentarioEmision = 0;
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("SP_COMENTARIO_EMISION_INSERTAR", conn))
                    {
                        using (SqlTransaction tx = conn.BeginTransaction())
                        {
                            try
                            {
                                cmd.Transaction = tx;

                                cmd.CommandType = CommandType.StoredProcedure;

                                cmd.Parameters.Add("@IdEmision", SqlDbType.Int).Value = request.IdEmision;
                                cmd.Parameters.Add("@IdDerivacion", SqlDbType.Int).Value = request.IdDerivacion;
                                //cmd.Parameters.Add("@IdDerivacionMovimiento", SqlDbType.Int).Value = request.IdDerivacionMovimiento;
                                cmd.Parameters.Add("@Texto", SqlDbType.VarChar,300).Value = request.Texto;
                                cmd.Parameters.Add("@UsuarioRegistro", SqlDbType.VarChar, 50).Value = request.UsuarioRegistro;

                                cmd.Parameters.Add("@Perfil", SqlDbType.VarChar, 5).Value = request.Perfil;
                                cmd.Parameters.Add("@CodAgencia", SqlDbType.Int).Value = request.CodAgencia;
                                cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = request.Estado;

                                if (string.IsNullOrEmpty(request.IdMotivo))
                                    cmd.Parameters.Add("@IdMotivo", SqlDbType.Int).Value = DBNull.Value;
                                else
                                    cmd.Parameters.Add("@IdMotivo", SqlDbType.Int).Value = Convert.ToInt32(request.IdMotivo);

                                SqlParameter pIdComentarioEmision = new SqlParameter("@IdComentarioEmision", SqlDbType.Int);
                                pIdComentarioEmision.Direction = ParameterDirection.Output;
                                cmd.Parameters.Add(pIdComentarioEmision);

                                await cmd.ExecuteNonQueryAsync();

                                IdComentarioEmision = Convert.ToInt32(cmd.Parameters["@IdComentarioEmision"].Value);

                                LaserficheProxy laserfiche = new LaserficheProxy();
                                DocumentoModel DocumentoModelRequest = new DocumentoModel();
                                DocumentoModel DocumentoModelResponse = new DocumentoModel();

                                DocumentoModelRequest.Folder = $@"\GESTION-CNA\"+ request.DNI;


                           
                                List<Documento> lstDocumento = new List<Documento>();
                                int item = 1;
                                string base64 = string.Empty;
                                if (!string.IsNullOrEmpty(request.Archivo))
                                {
                                    base64 = string.Empty;
                                    base64 = request.Archivo.Split(',')[1];

                                    DateTime fechaActual = DateTime.Now;
                                    string Dia = fechaActual.Day.ToString().PadLeft(2, '0');
                                    string Mes = fechaActual.Month.ToString().PadLeft(2, '0');
                                    string Anio = fechaActual.Year.ToString().PadLeft(2, '0');

                                    string Hora = fechaActual.Hour.ToString().PadLeft(2, '0');
                                    string Minutos = fechaActual.Minute.ToString().PadLeft(2, '0');
                                    string Segundos = fechaActual.Second.ToString().PadLeft(2, '0');

                                    Documento oDocumento = new Documento();
                                    oDocumento.Extension = System.IO.Path.GetExtension(request.NombreArchivo).Split('.')[1];
                                    oDocumento.ArchivoBytes = base64;
                                    oDocumento.Nombre = "COMENTARIO_" + request.IdEmision.ToString().PadLeft(3,'0') + "_" + Anio + Mes + Dia + "T" + Hora + Minutos + Segundos;
                                    oDocumento.Folder = "COMENTARIO-EMISION";
                                    lstDocumento.Add(oDocumento);
                                    item++;
                                }

                                if (lstDocumento.Count > 0)
                                {
                                    DocumentoModelRequest.ListaDocumentos = lstDocumento;
                                    DocumentoModelResponse = laserfiche.UploadListaDocumento(DocumentoModelRequest);

                                    RecursoDatos oRecursoDatos = null;
                                    RecursoBE oRecursoBE = null;
                                    foreach (var image in DocumentoModelResponse.ListaDocumentos)
                                    {
                                        if (image.ResultadoOK)
                                        {
                                            oRecursoDatos = new RecursoDatos();
                                            oRecursoBE = new RecursoBE();

                                            oRecursoBE.CodTabla = request.CodTabla;
                                            oRecursoBE.ReferenciaID = IdComentarioEmision;
                                            oRecursoBE.LaserficheID = image.CodigoLaserfiche;
                                            oRecursoBE.Estado = true;
                                            oRecursoBE.UsuarioRegistro = request.UsuarioRegistro;
                                            oRecursoBE.Descripcion = image.Nombre;
                                            Boolean resultad = oRecursoDatos.RegistrarTrans(oRecursoBE, conn, tx);

                                            if (!resultad)
                                                throw new Exception("Se presento un error al intentar registrar los archivos adjuntos");

                                        }
                                        else
                                        {
                                            throw new Exception("Se presento un error al subir el archivo");
                                        }
                                    }
                                    DocumentoModelResponse.ResultadoOK = true;

                                }

                                tx.Commit();
                                resultado = true;
                               
                            }

                            catch (Exception ex)
                            {
                                tx.Rollback();
                                conn.Close();
                                conn.Dispose();
                                cmd.Dispose();
                                resultado = false;
                                throw (ex);
                            }
                            finally
                            {
                                conn.Close();
                                conn.Dispose();
                                cmd.Dispose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                resultado = false;
                throw ex;
            }
            return await Task.Run(() => resultado);
        }

       
    }
}
