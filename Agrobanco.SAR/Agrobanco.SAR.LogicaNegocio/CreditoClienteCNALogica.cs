﻿using Agrobanco.SAR.Datos;
using Agrobanco.SAR.Entidades;
using Agrobanco.SAR.Entidades.CNA_Minuta;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SAR.LogicaNegocio
{
    public class CreditoClienteCNALogica
    {
        private readonly CreditoClienteCNADatos creditoClienteCNADatos;

        public CreditoClienteCNALogica()
        {
            creditoClienteCNADatos = new CreditoClienteCNADatos();
        }


        public async Task<List<CreditoClienteCNABE>> ObtenerCreditosCNA(CreditoClienteCNABE request)
        {
            return await creditoClienteCNADatos.ObtenerCreditosCNA(request);
        }

        public async Task<List<GarantiaClienteBE>> ObtenerGarantias(CreditosClienteBE request)
        {
            return await creditoClienteCNADatos.ObtenerGarantias(request);
        }
    }
}
