﻿using Agrobanco.SAR.Datos;
using Agrobanco.SAR.Entidades.CNA_Minuta;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SAR.LogicaNegocio
{
    public class ComentarioEmisionLogica
    {
        private readonly ComentarioEmisionDatos comentarioEmisionDatos;

        public ComentarioEmisionLogica()
        {
            comentarioEmisionDatos = new ComentarioEmisionDatos();
        }

        public async Task<bool> Insertar(ComentarioEmisionBE request)
        {
            return await comentarioEmisionDatos.Insertar(request);
        }
        public async Task<List<ComentarioEmisionBE>> Obtener(ComentarioEmisionBE request)
        {
            return await comentarioEmisionDatos.Obtener(request);
        }

    }
}
