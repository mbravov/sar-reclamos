﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.Reclamaciones.API.Models
{
    public class ParametroBE
    {
        public int? CodParametro { get; set; }
        public int? Parametro { get; set; }
        public int? Grupo { get; set; }
        public string? Descripcion { get; set; }
        public string? Valor { get; set; }
        public string? Referencia { get; set; }
    }
}
