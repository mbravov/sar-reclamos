﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades.CNA_Minuta
{
    public class CNAMasivoBE
    {
		public string NombreApellidos { get; set; }
		public string NroDocumento { get; set; }
		public string TipoDocumento { get; set; }
		public string Agencia { get; set; }
		public string Estado { get; set; }
		public int IdEmision { get; set; }
		public int IdDerivacion { get; set; }
		public int IdEstado { get; set; }
		public string Perfil { get; set; }
		public int IdCreditoCliente { get; set; }
		public int? CodAgencia { get; set; }
		public string CodEstado { get; set; }
		public string FechaCancelacionIni { get; set; }
		public string FechaCancelacionFin { get; set; }
		public string UsuarioWeb { get; set; }


		public int IdConfiguracionAccion { get; set; }
		public string UsuarioRegistro { get; set; }
		public string PerfilDerivado { get; set; }
		public string Texto { get; set; }

		public int? CodUsuarioDerivado { get; set; }

	}
}
