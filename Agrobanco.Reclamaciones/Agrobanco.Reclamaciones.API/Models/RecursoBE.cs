﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.Reclamaciones.API.Models
{
    public class RecursoBE
    {
		public int? CodRecurso { get; set; }

		public int? CodTabla { get; set; }

		public Int64? ReferenciaID { get; set; }
		public int? LaserficheID { get; set; }
		public string Descripcion { get; set; }
		public bool Estado { get; set; }
		public string UsuarioRegistro { get; set; }
	}
}
