﻿using Agrobanco.SAR.API.Models;
using Agrobanco.SAR.Control.Filters;
using Agrobanco.SAR.Entidades.CNA_Minuta;
using Agrobanco.SAR.LogicaNegocio;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.SAR.API.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/configuracion-derivacion")]
    [ApiController]
    public class ConfiguracionDerivacionController : ControllerBase
    {
        private readonly ConfiguracionDerivacionLogica _configuracionDerivacionLogica;
        private IWebHostEnvironment environment;
        public ConfiguracionDerivacionController(IWebHostEnvironment _environment)
        {
            _configuracionDerivacionLogica = new ConfiguracionDerivacionLogica();
            this.environment = _environment;
        }

        [HttpPost("accion")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public async Task<IActionResult> Accion([Required][FromHeader(Name = "Token")] string token, [FromBody] ConfiguracionDerivacionBE request)
        {
            ResponseErrorDetalle oResponseErrorDetalle = new ResponseErrorDetalle();

            ResponseSuccess oResponseSuccess = new ResponseSuccess();
            ResponseSuccessDetalle oResponseSuccessDetalle = new ResponseSuccessDetalle();
            
            try
            {
                string webRootPath = environment.ContentRootPath;
                oResponseSuccessDetalle.resultado = await _configuracionDerivacionLogica.Accion(request, webRootPath); ;
                oResponseSuccess.success = oResponseSuccessDetalle;
                return StatusCode(200, oResponseSuccess);
            }
            catch (Exception ex)
            {
                oResponseErrorDetalle.codigo = string.Empty;
                oResponseErrorDetalle.resultado = false;
                oResponseErrorDetalle.mensaje = ex.Message;

                return StatusCode(100, oResponseErrorDetalle);
            }
        }

        [HttpPost("finalizar")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public async Task<IActionResult> Finalizar([Required][FromHeader(Name = "Token")] string token, [FromBody] ConfiguracionDerivacionBE request)
        {
            ResponseErrorDetalle oResponseErrorDetalle = new ResponseErrorDetalle();

            ResponseSuccess oResponseSuccess = new ResponseSuccess();
            ResponseSuccessDetalle oResponseSuccessDetalle = new ResponseSuccessDetalle();

            try
            {
                string webRootPath = environment.ContentRootPath;
                oResponseSuccessDetalle.resultado = await _configuracionDerivacionLogica.AccionFinal(request, webRootPath); ;
                oResponseSuccess.success = oResponseSuccessDetalle;
                return StatusCode(200, oResponseSuccess);
            }
            catch (Exception ex)
            {
                oResponseErrorDetalle.codigo = string.Empty;
                oResponseErrorDetalle.resultado = false;
                oResponseErrorDetalle.mensaje = ex.Message;

                return StatusCode(100, oResponseErrorDetalle);
            }
        }

        [HttpPost("derivar-masivo-admage-a-usuage")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public async Task<IActionResult> DerivacionMasivaADMAGEaUsuAge([Required][FromHeader(Name = "Token")] string token, [FromBody] CNAMasivoBE request)
        {
            ResponseErrorDetalle oResponseErrorDetalle = new ResponseErrorDetalle();

            ResponseSuccess oResponseSuccess = new ResponseSuccess();
            ResponseSuccessDetalle oResponseSuccessDetalle = new ResponseSuccessDetalle();

            try
            {
                string webRootPath = environment.ContentRootPath;
                oResponseSuccessDetalle.resultado = await _configuracionDerivacionLogica.AccionMasivaADMAG(request, webRootPath); ;
                oResponseSuccess.success = oResponseSuccessDetalle;
                return StatusCode(200, oResponseSuccess);
            }
            catch (Exception ex)
            {
                oResponseErrorDetalle.codigo = string.Empty;
                oResponseErrorDetalle.resultado = false;
                oResponseErrorDetalle.mensaje = ex.Message;

                return StatusCode(100, oResponseErrorDetalle);
            }
        }
    }
}
