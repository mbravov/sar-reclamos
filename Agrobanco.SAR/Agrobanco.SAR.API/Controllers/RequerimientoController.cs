﻿using Agrobanco.SAR.API.Models;
using Agrobanco.SAR.Control.Filters;
using Agrobanco.SAR.Comun.Laserfiche;
using Agrobanco.SAR.Entidades;
using Agrobanco.SAR.LogicaNegocio;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Agrobanco.SAR.Entidades.RequerimientoSeguimiento;
using Agrobanco.SAR.Entidades.RequerimientoSeguimientoDetalle;
using Agrobanco.SAR.Entidades.RequerimientoReporte;
using Agrobanco.SAR.Entidades.RequerimientoGestionReporte;

namespace Agrobanco.SAR.API.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/Requerimiento")]
    [ApiController]
    public class RequerimientoController : ControllerBase
    {
        private readonly RequerimientoLogica requerimientoLogica;
        //private readonly ParametroLogica parametroLogica;
        private IWebHostEnvironment environment;
        public RequerimientoController(IWebHostEnvironment _environment)
        {
            requerimientoLogica = new RequerimientoLogica();
            this.environment = _environment;
        }


        [HttpPost("registrar")]
        //[HttpPost("parametro")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult Requerimiento([Required][FromHeader(Name = "Token")] string token, [FromBody] RequerimientoBE oRequerimientoBE)
        {
            ResponseSuccess oResponseSuccess = new ResponseSuccess();
            ResponseSuccessDetalle oResponseSuccessDetalle = new ResponseSuccessDetalle();
            ResponseErrorDetalle oResponseErrorDetalle = new ResponseErrorDetalle();
            try
            {
                string NroRequerimiento = string.Empty;
                var Resultado = requerimientoLogica.Registrar(oRequerimientoBE, out NroRequerimiento);
              
                oResponseSuccessDetalle.codigo = NroRequerimiento;
                oResponseSuccessDetalle.resultado = Resultado;
                oResponseSuccess.success = oResponseSuccessDetalle;
              
                
                return StatusCode(200, oResponseSuccess);
            }
            catch (Exception ex)
            {
                oResponseErrorDetalle.codigo = string.Empty;
                oResponseErrorDetalle.resultado = false;
                oResponseErrorDetalle.mensaje = ex.Message;
                 
                return StatusCode(100, oResponseErrorDetalle);
            }
        }


        [HttpPost("listarequerimientos")]
        //[HttpPost("parametro")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult RequerimientoLista([Required][FromHeader(Name = "Token")] string token, [FromBody] RequestRequerimiento oRequest)
        {
            try
            {
                ResponseSuccess oResponseSuccess = new ResponseSuccess();
                var Resultado = requerimientoLogica.RequerimientosListar(oRequest);
                return StatusCode(200, Resultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("requerimientobloqueado")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult RequerimientoBloqueado([Required][FromHeader(Name = "Token")] string token, [FromBody] RequerimientoBE oRequest)
        {
            try
            {
                ResponseSuccess oResponseSuccess = new ResponseSuccess();
                var Resultado = requerimientoLogica.RequerimientoBloqueado(oRequest);
                return StatusCode(200, Resultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("requerimientomovimientopenultimo")]
        //[HttpPost("parametro")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult RequerimientoMovimientoPenultimo([Required][FromHeader(Name = "Token")] string token, [FromBody] int CodRequerimiento)
        {
            try
            {
                ResponseSuccess oResponseSuccess = new ResponseSuccess();
                var Resultado = requerimientoLogica.RequerimientoMovimientoPenultimo(CodRequerimiento);
                return StatusCode(200, Resultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("requerimientodetalle")]
        //[HttpPost("parametro")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult RequerimientoDetalle([Required][FromHeader(Name = "Token")] string token, [FromBody] RequerimientoBE oRequest)
        {
            try
            {
                ResponseSuccess oResponseSuccess = new ResponseSuccess();
                var Resultado = requerimientoLogica.RequerimientoDetalle(oRequest);
                return StatusCode(200, Resultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("requerimientoactualizarbloqueado")]
        //[HttpPost("parametro")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult RequerimientoActualizarBloqueado([Required][FromHeader(Name = "Token")] string token, [FromBody] RequerimientoBE oRequerimientoBE)
        {
            try
            {
                ResponseSuccess oResponseSuccess = new ResponseSuccess();
                var Resultado = requerimientoLogica.RequerimientoActualizarBloqueado(oRequerimientoBE);
                return StatusCode(200, Resultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("requerimientomovimientoregistrar")]
        //[HttpPost("parametro")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult RequerimientoMovimientoRegistrar([Required][FromHeader(Name = "Token")] string token, [FromBody] RequerimientoBE oRequerimientoBE)
        {
            try
            {
                ResponseSuccess oResponseSuccess = new ResponseSuccess();
                var Resultado = requerimientoLogica.RequerimientoMovimientoRegistrar(oRequerimientoBE);
                return StatusCode(200, Resultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("requerimientoasignacionregistrar")]
        //[HttpPost("parametro")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult RequerimientoAsignacionRegistrar([Required][FromHeader(Name = "Token")] string token, [FromBody] RequerimientoBE oRequerimientoBE)
        {
            string webRootPath = environment.ContentRootPath;
            try
            {
                ResponseSuccess oResponseSuccess = new ResponseSuccess();
                var Resultado = requerimientoLogica.RequerimientoAsignacionRegistrar(oRequerimientoBE, webRootPath);
                return StatusCode(200, Resultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("requerimientoasignacionobtener")]
        //[HttpPost("parametro")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult RequerimientoAsignacionObtener([Required][FromHeader(Name = "Token")] string token, [FromBody] int CodRequerimientoAsignacion)
        {
            string webRootPath = environment.ContentRootPath;
            try
            {
                ResponseSuccess oResponseSuccess = new ResponseSuccess();
                var Resultado = requerimientoLogica.RequerimientoAsignacionObtener(CodRequerimientoAsignacion);
                return StatusCode(200, Resultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("requerimientoactualizarestado")]
        //[HttpPost("parametro")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult RequerimientoActualizarEstado([Required][FromHeader(Name = "Token")] string token, [FromBody] RequerimientoBE oRequerimientoBE)
        {
            try
            {
                ResponseSuccess oResponseSuccess = new ResponseSuccess();
                var Resultado = requerimientoLogica.RequerimientoActualizarEstado(oRequerimientoBE);
                return StatusCode(200, Resultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
                     

        //[HttpPost("descargararchivos")]
        ////[HttpPost("parametro")]
        //[ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        //[ProducesResponseType(400, Type = typeof(ResponseError))]
        //[ProducesResponseType(401, Type = typeof(ResponseError))]
        //[ProducesResponseType(404, Type = typeof(ResponseError))]
        //[ProducesResponseType(500, Type = typeof(ResponseError))]
        //[ValidateAuthorizationRequest]
        //public IActionResult DescagarArchivos([Required][FromHeader(Name = "Token")] string token, [FromBody] RecursoBE oReRcursoBE)
        //{
        //    try
        //    {
        //        ResponseSuccess oResponseSuccess = new ResponseSuccess();
        //        var Resultado = requerimientoLogica.DescagarArchivos(oReRcursoBE);
        //        return StatusCode(200, Resultado);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
               

        [HttpPost("derivar-ocm")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult DerivarOCM([Required][FromHeader(Name = "Token")] string token, [FromBody] RequerimientoBE oRequerimientoBE)
        {
            ResponseSuccess oResponseSuccess = new ResponseSuccess();
            ResponseSuccessDetalle oResponseSuccessDetalle = new ResponseSuccessDetalle();
            ResponseErrorDetalle oResponseErrorDetalle = new ResponseErrorDetalle();
            string webRootPath = environment.ContentRootPath;
            try
            {
                var Resultado = requerimientoLogica.DerivadoOCM(oRequerimientoBE, webRootPath);

                if (Resultado > 0)
                {
                    oResponseSuccessDetalle.resultado = true;
                    oResponseSuccess.success = oResponseSuccessDetalle;
                }
                else
                {
                    oResponseSuccessDetalle.resultado = false;
                    oResponseSuccess.success = oResponseSuccessDetalle;
                }
                return StatusCode(200, oResponseSuccess);
            }
            catch (Exception ex)
            {
                oResponseErrorDetalle.codigo = string.Empty;
                oResponseErrorDetalle.resultado = false;
                oResponseErrorDetalle.mensaje = ex.Message;

                return StatusCode(100, oResponseErrorDetalle);
            }
        }


        [HttpPost("aprobar")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult AprobarRequerimiento([Required][FromHeader(Name = "Token")] string token, [FromBody] RequerimientoBE oRequerimientoBE)
        {
            try
            {
                var Resultado = requerimientoLogica.AprobarRequerimiento(oRequerimientoBE);
                return StatusCode(200, Resultado);
            }
            catch (Exception ex)
            {
               
                return StatusCode(100,  false);
            }
        }

        [HttpPost("derivar-para-su-notificacion")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult DerivarParaSuNotificacion([Required][FromHeader(Name = "Token")] string token, [FromBody] RequerimientoBE oRequerimientoBE)
        {
            ResponseSuccess oResponseSuccess = new ResponseSuccess();
            ResponseSuccessDetalle oResponseSuccessDetalle = new ResponseSuccessDetalle();
            ResponseErrorDetalle oResponseErrorDetalle = new ResponseErrorDetalle();
            string webRootPath = environment.ContentRootPath;
            try
            {
                var Resultado = requerimientoLogica.DerivarParaSuNotificacion(oRequerimientoBE, webRootPath);

                if (Resultado > 0)
                {
                    oResponseSuccessDetalle.resultado = true;
                    oResponseSuccess.success = oResponseSuccessDetalle;
                }
                else
                {
                    oResponseSuccessDetalle.resultado = false;
                    oResponseSuccess.success = oResponseSuccessDetalle;
                }
                return StatusCode(200, oResponseSuccess);
            }
            catch (Exception ex)
            {
                oResponseErrorDetalle.codigo = string.Empty;
                oResponseErrorDetalle.resultado = false;
                oResponseErrorDetalle.mensaje = ex.Message;

                return StatusCode(100, oResponseErrorDetalle);
            }
        }


        [HttpPost("atender")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult AtenderRequerimiento([Required][FromHeader(Name = "Token")] string token, [FromBody] RequerimientoBE oRequerimientoBE)
        {
            try
            {
                var Resultado = requerimientoLogica.AtenderRequerimiento(oRequerimientoBE);
                return StatusCode(200, Resultado);
            }
            catch (Exception ex)
            {

                return StatusCode(100, false);
            }
        }

        [HttpPost("seguimiento")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult RequerimientoSeguimiento([Required][FromHeader(Name = "Token")] string token, [FromBody] RequerimientoSeguimientoFiltroRequest request)
        {
            try
            {
                var Resultado = requerimientoLogica.ObtenerSeguimiento(request);
                return StatusCode(200, Resultado);
            }
            catch (Exception ex)
            {

                return StatusCode(100, false);
            }
        }

        [HttpPost("seguimiento-detalle")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult RequerimientoSeguimientoDetalle([Required][FromHeader(Name = "Token")] string token, [FromBody] RequerimientoSeguimientoDetalleRequest request)
        {
            try
            {
                var Resultado = requerimientoLogica.ObtenerSeguimientoDetalle(request);
                return StatusCode(200, Resultado);
            }
            catch (Exception ex)
            {

                return StatusCode(100, false);
            }
        }


        [HttpPost("reporte")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult RequerimientoReporte([Required][FromHeader(Name = "Token")] string token, [FromBody] RequerimientoReporteFiltroRequest request)
        {
            try
            {
                var Resultado = requerimientoLogica.ObtenerRequerimientoReporte(request);
                return StatusCode(200, Resultado);
            }
            catch (Exception ex)
            {

                return StatusCode(100, false);
            }
        }

        [HttpPost("reportegestion")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult RequerimientoGestionReporte([Required][FromHeader(Name = "Token")] string token, [FromBody] RequerimientoGestionReporteFiltroRequest request)
        {
            try
            {
                var Resultado = requerimientoLogica.ObtenerGestionRequerimientoReporte(request);
                return StatusCode(200, Resultado);
            }
            catch (Exception ex)
            {

                return StatusCode(100, false);
            }
        }
    }
}
