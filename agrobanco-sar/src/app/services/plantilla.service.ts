import { Injectable } from '@angular/core';
import { AppService } from '../app.service';
import { SecurityService } from './security.service';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PlantillaService extends AppService {

  constructor(securityService : SecurityService) {
    super(securityService);
  }


  Registrar<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${environment.UrlBase_SARAPI}/laserfiche/registrar-plantilla`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }
  Desactivar<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${environment.UrlBase_SARAPI}/plantilla/desactivar`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }
}
