import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CnaMinutaOcmService } from 'src/app/services/cna-minuta-ocm.service';
import { DialogService } from 'src/app/services/comun/dialog.service';
import { SecurityService } from '../../../services/security.service';
import { FiltroAccionService } from '../../../services/filtro-accion.service';
import { forkJoin } from 'rxjs';
import { ConfiguracionAccionService } from '../../../services/configuracion-accion.service';
import { Constantes } from '../../../comun/constantes';

@Component({
  selector: 'app-cna-derivacion-ocm',
  templateUrl: './cna-derivacion-ocm.component.html',
  styleUrls: ['./cna-derivacion-ocm.component.css']
})
export class CnaDerivacionOcmComponent implements OnInit {

  form!: FormGroup;
  formSubmitted:boolean = false;

  lengthDescripcion: number  = 300;
  MaxlengthDescripcion: number  = 300;
  MinlengthDescripcion: number  = 5;

  IdConfiguracionAccion:any;
  emision:any;
  agencias!:any[];
  filtroPerfil!:any[];

  SeletedAgencia:any;

  SeletedPerfil:any;

  loading:boolean = false;

  AgenciaObligatorio: boolean = false;
  constructor(
    private _formBuilder: FormBuilder,
    private _dialogService: DialogService,
    private _securityService: SecurityService,
    private _cnaMinutaOcmService : CnaMinutaOcmService,
    private _filtroAccionService : FiltroAccionService,
    private _configuracionAccion : ConfiguracionAccionService,
    public dialogRef: MatDialogRef<CnaDerivacionOcmComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.emision = data.CreditoClienteDetalle;
    this.IdConfiguracionAccion = data.IdConfiguracionAccion;
   }

   ngOnInit(): void {
    this.inicializarFormulario();
    this.pageLoad();
  }

  pageLoad():void{
    let params = {
      Perfil : this._securityService.leerPerfil(),
      UsuarioWeb : this._securityService.leerUsuarioWeb()
    }
    this.loading = true;
    forkJoin([
      this._cnaMinutaOcmService.ObtenerAgencias(params),
      this._filtroAccionService.obtener()

    ]).subscribe( (data:any) => {
        this.agencias = data[0].response;
        this.filtroPerfil = data[1].response;
        this.loading = false;
    }, (error) =>{
      this.loading = false;
      console.log(error);
    });

  }
  inicializarFormulario() : void {
    console.log(this.emision.codAgencia);

    this.form = this._formBuilder.group({
      NombreCompletos:[this.emision.nombres],
      nroDocumento:[this.emision.numeroDocumento],
      CodAgencia:[""+this.emision.codAgencia,  [Validators.required]],
      Perfil:[null,  [Validators.required]],
      Descripcion: [null,  [Validators.required, Validators.minLength(5)]],

    });

    this.form.controls['NombreCompletos'].disable();
    this.form.controls['nroDocumento'].disable();
    this.form.controls['CodAgencia'].disable();
}

  derivar(){

    this.formSubmitted = true;

    if (this.form.invalid) {
      return;
    }


    var request = {
      "IdConfiguracionAccion" : this.IdConfiguracionAccion,
      "GrupoFiltro" : +this.form.controls["Perfil"].value,
      "Nombres": this.emision.nombres,
      "TipoDocumento": this.emision.tipoDocumento,
      "NroDocumento": this.emision.numeroDocumento,
      "emision": {
        "IdEmision" : +this.emision.idEmision,
        "IdCreditoCliente"  : +this.emision.idCreditoCliente,
        "Estado"  : this.emision.idEstado,
        "TipoFlujo"  :1,
        "FechaEntregaOlva" : null,
        "MedioNotificacion": null,
        "Email" : null,
        "Whatsapp" : null,
        "FechaCargoEntrega" : null,
        "UsuarioRegistro" :  this._securityService.leerUsuarioWeb(),
        "UsuarioModificacion" :  this._securityService.leerUsuarioWeb()
      },
      "derivacion": {
        "IdDerivacion" : +this.emision.idDerivacion,
        "IdEmision" : +this.emision.idEmision,
        "Perfil" : this._securityService.leerPerfil(),
        "Estado" : this.emision.idEstado,
        "PerfilDerivado" : this.SeletedPerfil,
        "FechaDerivado" : null,
        "CodAgencia" : (this.form.controls["CodAgencia"].value=="")? null: +this.form.controls["CodAgencia"].value,
        "UsuarioRegistro" : this._securityService.leerUsuarioWeb(),
        "UsuarioModificacion":  this._securityService.leerUsuarioWeb()
      },
      "comentarioEmision":
      {
          "IdEmision" : this.emision.idEmision,
          "IdDerivacion" : this.emision.idDerivacion,
          "IdDerivacionMovimiento" :  this.emision.idDerivacionMovimiento,
          "Texto" :  this.form.controls['Descripcion'].value,
          "UsuarioRegistro" :  this._securityService.leerUsuarioWeb(),
          "Archivo" : null,
          "NombreArchivo" : null,
          "CodTabla" : Constantes.ComentarioEmision,
          "DNI": this.emision.numeroDocumento,
          "Perfil": this._securityService.leerPerfil(),
          "CodAgencia":  this.emision.codAgencia,
          "Estado": this.emision.idEstado,
          "IdMotivo" : ''

      }
    };

    this._dialogService
    .openConfirmDialog('¿Está seguro de derivar la emisión de la carta de no adeudo?')
    .afterClosed()
    .subscribe((res) => {
      if (res) {
        this.loading = true;
          this._configuracionAccion.accion(request).subscribe((data:any)=>{
            this.loading = false;
            if(data.response.success.resultado){
              this._dialogService.openMsgSuccessDialog("Se derivó correctamente la emisión de la carta de no adeudo");
              this.dialogRef.close({ data: true });
            }
            else{
              this._dialogService.openMsgSuccessDialog("Hubo un problema al derivar la emisión de la carta de no adeudo");
            }
          }, (err)=>{
            this.loading = false;
            console.log(err);
          });
      }
    });




  }

  onChangePerfil(){


    var perfil = +this.form.controls["Perfil"].value;

    if(+perfil == Constantes.TipoFiltroGAR){
      this.form.controls['CodAgencia'].disable();
      this.form.controls['CodAgencia'].setValue("");
      this.SeletedPerfil = Constantes.GAR.toUpperCase() ;

    }

    if(+perfil  == Constantes.TipoFiltroADMAG){
      this.form.controls['CodAgencia'].enable();
      this.SeletedPerfil = Constantes.ADMAG.toUpperCase() ;

      this.form.controls['CodAgencia'].setValue(""+this.emision.codAgencia);
    }
  }
}
