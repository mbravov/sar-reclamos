import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { forkJoin } from 'rxjs';
import { Constantes } from 'src/app/comun/constantes';
import { CnaMinutaOcmService } from 'src/app/services/cna-minuta-ocm.service';
import { DialogService } from 'src/app/services/comun/dialog.service';
import { ConfiguracionAccionService } from 'src/app/services/configuracion-accion.service';
import { FiltroAccionService } from 'src/app/services/filtro-accion.service';
import { RequerimientoconsultaService } from 'src/app/services/requerimientoconsulta.service';
import { SecurityService } from 'src/app/services/security.service';

@Component({
  selector: 'app-cna-derivar-uda',
  templateUrl: './cna-derivar-uda.component.html',
  styleUrls: ['./cna-derivar-uda.component.css']
})
export class CnaDerivarUdaComponent implements OnInit {

  IdConfiguracionAccion:any;
  emision:any;
  
  form!: FormGroup;
  formSubmitted:boolean = false;

  loading:boolean = false;

  usuarios:any[] = [];
  agencias!:any[];

  constructor(
    private _formBuilder: FormBuilder,
    private _dialogService: DialogService,
    private _securityService: SecurityService,
    private _cnaMinutaOcmService : CnaMinutaOcmService,
    private _filtroAccionService : FiltroAccionService,
    private _configuracionAccion : ConfiguracionAccionService,
    private _requerimientoServices : RequerimientoconsultaService,
    public dialogRef: MatDialogRef<CnaDerivarUdaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.emision = data.CreditoClienteDetalle;
    this.IdConfiguracionAccion = data.IdConfiguracionAccion;
  }

  ngOnInit(): void {
    this.inicializarFormulario();
    this.pageLoad();
  }

  inicializarFormulario() : void {
    console.log(this.emision.codAgencia);

    this.form = this._formBuilder.group({
      // Agencia:[this.emision.agencia],
      CodAgencia:["",  [Validators.required]],
      CodUsuario:["", [Validators.required]]

    });

 
    // this.form.controls['Agencia'].disable();
}

 
pageLoad():void{
  var params = {
    Perfil: Constantes.UDA,
    UsuarioWeb: '',
    Estado: true
  };
  this.loading = true;
  forkJoin([

    this._cnaMinutaOcmService.ObtenerAgencias(params),
    this._filtroAccionService.obtener()

  ]).subscribe( (data:any) => {
    this.agencias = data[0].response;
      
      if(data[0].response!=null && data[0].response.length>0){        
        this.form.controls["CodAgencia"].setValue(""+this.emision.codAgencia);
        this.obtenerUsuarios(this.form.controls["CodAgencia"].value);
      }
      //this.filtroPerfil = data[1].response;
      this.loading = false;
  }, (error) =>{
    this.loading = false;
    console.log(error);
  });

}

onChangeAgencia(event:any):void{
  let codAgencia : number = event.value;
  this.obtenerUsuarios(codAgencia);

}

obtenerUsuarios(codAgencia:number){
  var Perfil = {

    Perfil: Constantes.UDA,
    UsuarioWeb: '',
    Estado: true
  };

  this.loading = true;
  this._cnaMinutaOcmService.ObtenerUsuarios(Perfil).subscribe((data:any) => {
    this.usuarios = data.response.filter((x:any) => x.codAgencia == codAgencia);
    console.log(this.usuarios);
    this.loading = false;
  },  (err)=>{
    this.loading = false;
    console.log(err);
  });

}

  derivar():void{
    this.formSubmitted = true;

    if (this.form.invalid) {
      return;
    }
  

  var codAgencia = '';
  //codAgencia = this.emision.codAgenciaDerivacion;
  codAgencia = this.form.controls["CodAgencia"].value

  if(codAgencia = '0')  codAgencia = codAgencia = this.form.controls["CodAgencia"].value

  this._dialogService.openConfirmDialog('¿Está seguro de enviar al Usuario de Agencia?')
  .afterClosed().subscribe(res => {
    if (res) {
      this.loading = true;
      var reques = {
        "IdConfiguracionAccion": this.IdConfiguracionAccion,
        "NroDocumento": this.emision.numeroDocumento,
        "TipoDocumento": this.emision.tipoDocumento,
        "Nombres": this.emision.nombres,
        "Perfil" : Constantes.UDA.toUpperCase(),

        "emision": {"IdEmision": this.emision.idEmision,
                    "IdCreditoCliente":this.emision.idCreditoCliente,
                    "UsuarioModificacion": this._securityService.leerUsuarioWeb()
                   },
        "derivacion":{"IdEmision": this.emision.idEmision,
                      "IdDerivacion":this.emision.idDerivacion,
                      "Perfil": this._securityService.leerPerfil(),
                      "UsuarioRegistro": this._securityService.leerUsuarioWeb(),
                      "UsuarioModificacion": this._securityService.leerUsuarioWeb(),
                      "CodAgencia": +this.form.controls["CodAgencia"].value,
                      "PerfilDerivado" : Constantes.UDA.toUpperCase(),
                      "CodUsuarioDerivado": +this.form.controls['CodUsuario'].value
                     }
      };
debugger;
      this._configuracionAccion.accion(reques).subscribe((data:any) => {
        debugger;
        this.loading = false;
        if(data.response.success.resultado){
          this._dialogService.openMsgSuccessDialog("Se realizó el envío de la CR al Usuario de Agencia.");
          this.dialogRef.close({ data: true });
        }
        else{
          this._dialogService.openMsgSuccessDialog("Hubo un problema en el envío de la CR.");
        }
         
      },
        (err) => {
          this._dialogService.openMsgErrorDialog("Hubo un error en el envío de la CR");
          this.loading = false;
      });
    }
  });

  }

}
