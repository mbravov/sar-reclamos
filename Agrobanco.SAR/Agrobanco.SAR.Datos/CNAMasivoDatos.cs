﻿using Agrobanco.SAR.Datos.SqlServer.Acceso;
using Agrobanco.SAR.Entidades.CNA_Minuta;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SAR.Datos
{
    public class CNAMasivoDatos
    {
        public string cadenaConexion;
        public CNAMasivoDatos() { }
        public async Task<List<CNAMasivoBE>> Listar(CNAMasivoBE oRequest)
        {//RequestRequerimiento oBE

            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            CNAMasivoBE oCNAMasivoBE = null;
            List<CNAMasivoBE> LstCNAMasivoBE = new List<CNAMasivoBE>();

            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("SP_CREEDITO_CLIENTE_OBTENER_DERIVACION_MASIVA_ADMAG", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        if (string.IsNullOrEmpty(oRequest.Perfil))
                            cmd.Parameters.Add("@Perfil", SqlDbType.VarChar, 200).Value = DBNull.Value;
                        else
                            cmd.Parameters.Add("@Perfil", SqlDbType.VarChar, 200).Value = oRequest.Perfil;

                        cmd.Parameters.Add("@CodEstado", SqlDbType.VarChar).Value = (oRequest.CodEstado == null ? "" : oRequest.CodEstado);
                        cmd.Parameters.Add("@CodAgencia", SqlDbType.VarChar).Value = oRequest.CodAgencia;
                        cmd.Parameters.Add("@NroDocumento", SqlDbType.VarChar).Value = (oRequest.NroDocumento == null ? "" : oRequest.NroDocumento);
                        cmd.Parameters.Add("@FechaCancelacionIni", SqlDbType.VarChar).Value = oRequest.FechaCancelacionIni == null ? string.Empty : oRequest.FechaCancelacionIni;
                        cmd.Parameters.Add("@FechaCancelacionFin", SqlDbType.VarChar).Value = oRequest.FechaCancelacionFin == null ? string.Empty : oRequest.FechaCancelacionFin;
                        cmd.Parameters.Add("@UsuarioWeb", SqlDbType.VarChar, 10).Value = oRequest.UsuarioWeb;

                        try
                        {
                            using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                            {

                                while (reader.Read())
                                {
                                    oCNAMasivoBE = new CNAMasivoBE();

                                    if (!reader.IsDBNull(0))
                                        oCNAMasivoBE.NombreApellidos = reader.GetString(0);
                                    if (!reader.IsDBNull(1))
                                        oCNAMasivoBE.TipoDocumento = reader.GetString(1);
                                    if (!reader.IsDBNull(2))
                                        oCNAMasivoBE.NroDocumento = reader.GetString(2);
                                    if (!reader.IsDBNull(3))
                                        oCNAMasivoBE.Agencia = reader.GetString(3);
                                    if (!reader.IsDBNull(4))
                                        oCNAMasivoBE.Estado = reader.GetString(4);
                                    if (!reader.IsDBNull(5))
                                        oCNAMasivoBE.IdEmision = reader.GetInt32(5);
                                    if (!reader.IsDBNull(6))
                                        oCNAMasivoBE.IdDerivacion = reader.GetInt32(6);
                                    if (!reader.IsDBNull(7))
                                        oCNAMasivoBE.IdEstado = reader.GetInt32(7);
                                    if (!reader.IsDBNull(8))
                                        oCNAMasivoBE.Perfil = reader.GetString(8);
                                    if (!reader.IsDBNull(9))
                                        oCNAMasivoBE.IdCreditoCliente = reader.GetInt32(9);

                                    if (!reader.IsDBNull(10))
                                        oCNAMasivoBE.CodAgencia = reader.GetInt32(10);

                                    LstCNAMasivoBE.Add(oCNAMasivoBE);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw ex;
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return await Task.Run(() => LstCNAMasivoBE);
             
        }
    }
}
