﻿using Agrobanco.SAR.API.Models;
using Agrobanco.SAR.Control.DTO;
using Agrobanco.SAR.Control.Filters;
using Agrobanco.SAR.Control.Implementation;
using Agrobanco.SAR.Control.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.SAR.API.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/acceso")]
    [ApiController]
    public class AccessController : ControllerBase
    {
        private readonly IAccessControl _accesscontrol;

        public AccessController()
        {
            _accesscontrol = new AccessControl();
        }

        #region Access 

        /// <summary>
        /// Método para autenticar al usuario y generar token de sesión
        /// </summary>
        /// <param name="appkey">AppKey de aplicación</param>
        /// <param name="appcode">AppCode de aplicación</param>
        /// <param name="access">Objeto con correo, nombre y codigo del usuario</param>
        /// <response code="200">Ok - Retorna objeto con los datos del token</response> 
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="500">Server Error - Errores no controlados</response>  
        /// <response code="502">Bad Gateway - Servidor remoto sin conexión o no disponible</response>  
        [HttpPost]
        [ProducesResponseType(200, Type = typeof(AuthAccessResponse))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ProducesResponseType(502, Type = typeof(ResponseError))]
        [ValidateAppHeadersRequest]
        public object GenerarToken(
            [Required][FromHeader(Name = "X-AppKey")] string appkey,
            [Required][FromHeader(Name = "X-AppCode")] string appcode,
            [Required][FromBody] AuthAccessRequest access)
        {
            try
            {

                string token = "";

                var authAccessdto = new AccessDTO {
                    CorreoElectronico = access.CorreoElectronico,
                    CodigoUsuario = access.CodigoUsuario,
                    NombreUsuario = access.NombreUsuario, 
                    NumeroDocumento = access.NumeroDocumento
                }; 
                
                //ApiModelControlMapper.Mapper.Map<AccessDto>(access);


                AccessDTO result = _accesscontrol.generateToken(authAccessdto,
                    appkey,
                    appcode,
                    ref token
                );

                //var accessresponsedata = ApiModelControlMapper.Mapper.Map<AuthAccessResponseData>(result);

                AuthAccessResponseData dataResponse = new AuthAccessResponseData();
                dataResponse.FechaInicioVigencia = result.FechaInicioVigencia;
                dataResponse.FechaFinVigencia = result.FechaFinVigencia;
                dataResponse.NombreUsuario = result.NombreUsuario;
                dataResponse.NumeroDocumento = result.NumeroDocumento;
                dataResponse.CodigoUsuario = result.CodigoUsuario;
                dataResponse.CorreoElectronico = result.CorreoElectronico;


                var response = new AuthAccessResponse
                {
                    AuthAccess = dataResponse
                };

                Response.Headers.Add("Access-Control-Expose-Headers", "Authorization");
                Response.Headers.Add("Authorization", "Bearer " + token);

                return Ok(response);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


    }
}
