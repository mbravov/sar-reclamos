import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'Fecharegistroini'
})
export class FormatoFechasPipe implements PipeTransform {

  transform(date: Date): Date {
      debugger;
    let day = date.getDate();
    let month = date.getMonth() + 1;
    let year = date.getFullYear();

    let fechaActual = new Date();
    let primerDiaMes = new Date(fechaActual.getFullYear(), fechaActual.getMonth(), 1);

    primerDiaMes = primerDiaMes;   

  return primerDiaMes;
}

}
