import { Component, OnInit } from '@angular/core';
import { DomService } from 'src/app/services/dom.service';
import { SecurityService } from 'src/app/services/security.service';
import { Modulo } from '../../models/modulo.interface';
declare var M: any;

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  //ListaOpciones : Modulo[] = [];

  constructor(public domService: DomService, public secService: SecurityService) { }

  ngOnInit(): void {
    //this.ListaOpciones = this.secService.leerModulos();
    this.inicializarControles();
  }
  
  inicializarControles(): void {    
    var elems = document.querySelectorAll('.collapsible');
    var instances = M.Collapsible.init(elems, {});    
  }

  clickSlideOut(): void {
    this.domService.ShowSideBar();
  }

  filtrarOpcionesPorModulo(IdOpcion: string) {
    let opciones = this.secService.modulos.filter(
      (x) => x.IdRelacion === IdOpcion
    );
    return opciones;
  }

  filtrarTieneOpciones(IdOpcion: string)
  {
    let opciones = this.secService.modulos.filter(
      (x) => x.IdRelacion === IdOpcion
    );
    return opciones.length;
  }

  obtenerRutaOpcion(Opcion:Modulo){
    return Opcion.Controller +'/'+ Opcion.Action;
  }

}
