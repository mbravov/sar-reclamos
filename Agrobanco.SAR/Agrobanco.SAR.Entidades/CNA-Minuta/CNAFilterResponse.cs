﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades.CNA_Minuta
{
    public class CNAFilterResponse
    {
        public DateTime? FechaCancelacion { get; set; }
        public string NombresApellidos { get; set; }
        public string NroDocumento { get; set; }
        public string Banco { get; set; }
        public string Agencia { get; set; }
        public string AnalistaProponente { get; set; }
        public string Estado { get; set; }
        public string DerivadoA { get; set; }
        public DateTime? FechaDerivacion { get; set; }
        public string procesamiento { get; set; }
        public string procesamientoDesc { get; set; }
        public int IdEmision { get; set; }

    }
}
