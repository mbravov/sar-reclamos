﻿using Agrobanco.SAR.Control.Base;
using Agrobanco.SAR.Datos.SqlServer.Acceso;
using Agrobanco.SAR.Entidades;
using Agrobanco.SAR.Entidades.UsuarioMantenimiento;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Agrobanco.SAR.Datos
{
    public class ParametroDatos
    {
        public string cadenaConexion;
        public ParametroDatos()
        {
           
        }

     
        public List<ParametroBE> ListarParametro(int Grupo)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            ParametroBE objParametro = null;
            List<ParametroBE> lstParametro = new List<ParametroBE>();
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_PARAMETRO_OBTENER]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@grupo", SqlDbType.Int).Value = Grupo;
                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    objParametro = new ParametroBE();
                                    if (!Convert.IsDBNull(reader["Grupo"])) objParametro.Grupo = Convert.ToInt32(reader["Grupo"]);
                                    if (!Convert.IsDBNull(reader["Parametro"])) objParametro.Parametro = Convert.ToInt32(reader["Parametro"]);
                                    if (!Convert.IsDBNull(reader["Descripcion"])) objParametro.Descripcion = Convert.ToString(reader["Descripcion"]);
                                    if (!Convert.IsDBNull(reader["Valor"])) objParametro.Valor = Convert.ToString(reader["Valor"]);
                                    if (!Convert.IsDBNull(reader["Referencia"])) objParametro.Referencia = Convert.ToString(reader["Referencia"]);
                                    if (!Convert.IsDBNull(reader["Referencia2"])) objParametro.Referencia2 = Convert.ToString(reader["Referencia2"]);

                                    lstParametro.Add(objParametro);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw (ex);
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw (ex);
            }
           
            return lstParametro;
        }
        public List<ParametroBE> ListarParametroGrupo(string Grupo)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            ParametroBE objParametro = null;
            List<ParametroBE> lstParametro = new List<ParametroBE>();
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_PARAMETRO_OBTENER_GRUPO]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@grupo", SqlDbType.VarChar,50).Value = Grupo;
                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    objParametro = new ParametroBE();
                                    if (!Convert.IsDBNull(reader["Grupo"])) objParametro.Grupo = Convert.ToInt32(reader["Grupo"]);
                                    if (!Convert.IsDBNull(reader["Parametro"])) objParametro.Parametro = Convert.ToInt32(reader["Parametro"]);
                                    if (!Convert.IsDBNull(reader["Descripcion"])) objParametro.Descripcion = Convert.ToString(reader["Descripcion"]);
                                    if (!Convert.IsDBNull(reader["Valor"])) objParametro.Valor = Convert.ToString(reader["Valor"]);
                                    if (!Convert.IsDBNull(reader["Referencia"])) objParametro.Referencia = Convert.ToString(reader["Referencia"]);
                                    if (!Convert.IsDBNull(reader["Referencia2"])) objParametro.Referencia2 = Convert.ToString(reader["Referencia2"]);
                                    lstParametro.Add(objParametro);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw (ex);
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw (ex);
            }

            return lstParametro;
        }

        public List<Usuario> ListarUsuario(Usuario usuario )
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();     
            Usuario objUsuario = null;
            List<Usuario> lstUsuario= new List<Usuario>();
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_USUARIO_OBTENER]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@perfil", SqlDbType.VarChar).Value = (usuario.Perfil == null ? "" : usuario.Perfil);
                        cmd.Parameters.Add("@usuarioWeb", SqlDbType.VarChar).Value = (usuario.UsuarioWeb== null ? "" : usuario.UsuarioWeb);
                        cmd.Parameters.Add("@Estado", SqlDbType.Bit).Value = (!usuario.Estado.HasValue ? (bool?)null : usuario.Estado.Value);

                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    objUsuario = new Usuario();
                                    if (!Convert.IsDBNull(reader["CodUsuario"])) objUsuario.CodUsuario = Convert.ToInt32(reader["CodUsuario"]);
                                    if (!Convert.IsDBNull(reader["UsuarioWeb"])) objUsuario.UsuarioWeb = Convert.ToString(reader["UsuarioWeb"]);
                                    if (!Convert.IsDBNull(reader["NombresApellidos"])) objUsuario.NombresApellidos = Convert.ToString(reader["NombresApellidos"]);
                                    if (!Convert.IsDBNull(reader["Perfil"])) objUsuario.Perfil = Convert.ToString(reader["Perfil"]);
                                    if (!Convert.IsDBNull(reader["CorreoElectronico"])) objUsuario.CorreoElectronico = Convert.ToString(reader["CorreoElectronico"]);
                                    if (!Convert.IsDBNull(reader["Estado"])) objUsuario.Estado = Convert.ToBoolean(reader["Estado"]);
                                    if (!Convert.IsDBNull(reader["CodAgencia"])) objUsuario.CodAgencia = Convert.ToInt32(reader["CodAgencia"]);
                                    lstUsuario.Add(objUsuario);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw (ex);
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception("Error al Obtener el Listado de Usuarios");
            }
           
            return lstUsuario;
        }

        public List<AgenciaBE> ListarAgencia()
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            AgenciaBE objAgencia = null;
            List<AgenciaBE> lstAgencia= new List<AgenciaBE>();
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_AGENCIA_OBTENER]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        
                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    objAgencia = new AgenciaBE();
                                    if (!Convert.IsDBNull(reader["CodAgencia"])) objAgencia.CodAgencia = Convert.ToInt32(reader["CodAgencia"]);
                                    if (!Convert.IsDBNull(reader["Oficina"])) objAgencia.Oficina = Convert.ToString(reader["Oficina"]);
                                    if (!Convert.IsDBNull(reader["Direccion"])) objAgencia.Direccion = Convert.ToString(reader["Direccion"]);
                                    lstAgencia.Add(objAgencia);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw (ex);
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw (ex);
            }
           
            return lstAgencia;
        }

      

        public List<AreaBE> ListarArea()
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            AreaBE AgenciaBE = null;
            List<AreaBE> listaArea = new List<AreaBE>();
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_AREA_OBTENER]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    AgenciaBE = new AreaBE();
                                    if (!Convert.IsDBNull(reader["Codigo"])) AgenciaBE.Codigo = Convert.ToInt32(reader["Codigo"]);
                                    if (!Convert.IsDBNull(reader["Descripcion"])) AgenciaBE.Descripcion = Convert.ToString(reader["Descripcion"]);
                                    listaArea.Add(AgenciaBE);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw (ex);
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw (ex);
            }

            return listaArea;
        }

        public List<ParametroBE> ListarPreguntasFrecuentes(string Grupo)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            ParametroBE objParametro = null;
            List<ParametroBE> lstParametro = new List<ParametroBE>();
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_PREGUNTAS_FRECUENTES]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@grupo", SqlDbType.VarChar, 50).Value = Grupo;

                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    objParametro = new ParametroBE();
                                    //if (!Convert.IsDBNull(reader["Grupo"])) objParametro.Grupo = Convert.ToInt32(reader["Grupo"]);
                                    if (!Convert.IsDBNull(reader["Parametro"])) objParametro.Parametro = Convert.ToInt32(reader["Parametro"]);
                                    if (!Convert.IsDBNull(reader["Descripcion"])) objParametro.Descripcion = Convert.ToString(reader["Descripcion"]);
                                    if (!Convert.IsDBNull(reader["Valor"])) objParametro.Valor = Convert.ToString(reader["Valor"]);
                                    if (!Convert.IsDBNull(reader["Referencia"])) objParametro.Referencia = Convert.ToString(reader["Referencia"]);

                                    lstParametro.Add(objParametro);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw (ex);
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw (ex);
            }

            return lstParametro;
        }

        public List<UsuarioMantenimientoResponse> ListarUsuarioMantenimiento(UsuarioMantenimientoFiltroResquest usuario)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            UsuarioMantenimientoResponse objUsuario = null;
            List<UsuarioMantenimientoResponse> lstUsuario = new List<UsuarioMantenimientoResponse>();
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_USUARIO_MANTENIMIENTO_OBTENER]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@perfil", SqlDbType.VarChar).Value = (usuario.Perfil == null ? "" : usuario.Perfil);
                        cmd.Parameters.Add("@NombresApellidos", SqlDbType.VarChar).Value = (usuario.NombresApellidos == null ? "" : usuario.NombresApellidos);
                        cmd.Parameters.Add("@usuarioWeb", SqlDbType.VarChar).Value = (usuario.UsuarioWeb == null ? "" : usuario.UsuarioWeb);
                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    objUsuario = new UsuarioMantenimientoResponse();
                                    if (!Convert.IsDBNull(reader["CodUsuario"])) objUsuario.CodUsuario = Convert.ToInt32(reader["CodUsuario"]);
                                    if (!Convert.IsDBNull(reader["Perfil"])) objUsuario.Perfil = Convert.ToString(reader["Perfil"]);
                                    if (!Convert.IsDBNull(reader["DescripcionPerfil"])) objUsuario.DescripcionPerfil = Convert.ToString(reader["DescripcionPerfil"]);
                                    if (!Convert.IsDBNull(reader["CorreoElectronico"])) objUsuario.CorreoElectronico = Convert.ToString(reader["CorreoElectronico"]);
                                    if (!Convert.IsDBNull(reader["UsuarioWeb"])) objUsuario.UsuarioWeb = Convert.ToString(reader["UsuarioWeb"]);
                                    if (!Convert.IsDBNull(reader["NombresApellidos"])) objUsuario.NombresApellidos = Convert.ToString(reader["NombresApellidos"]);
                                    if (!Convert.IsDBNull(reader["Estado"])) objUsuario.Estado = Convert.ToString(reader["Estado"]);
                                    if (!Convert.IsDBNull(reader["UsuarioRegistro"])) objUsuario.UsuarioRegistro = Convert.ToString(reader["UsuarioRegistro"]);
                                    if (!Convert.IsDBNull(reader["FechaRegistro"])) objUsuario.FechaRegistro = Convert.ToString(reader["FechaRegistro"]);

                                    lstUsuario.Add(objUsuario);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw (ex);
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw (ex);
            }

            return lstUsuario;
        }


        public string UsuarioRegistrar(UsuarioMantenimientoResponse oBE)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            string Mensaje = string.Empty;
            
            if (oBE.CodAgencia == "") oBE.CodAgencia = null;
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {

                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_USUARIO_REGISTRAR]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@Perfil", SqlDbType.VarChar).Value = oBE.Perfil;
                        cmd.Parameters.Add("@DescripcionPerfil", SqlDbType.VarChar).Value = oBE.DescripcionPerfil;
                        cmd.Parameters.Add("@CorreoElectronico", SqlDbType.VarChar).Value = oBE.CorreoElectronico;
                        cmd.Parameters.Add("@UsuarioWeb", SqlDbType.VarChar).Value = oBE.UsuarioWeb;
                        cmd.Parameters.Add("@NombresApellidos", SqlDbType.VarChar).Value = oBE.NombresApellidos;                       
                        cmd.Parameters.Add("@UsuarioRegistro", SqlDbType.VarChar).Value = oBE.UsuarioRegistro;
                        cmd.Parameters.Add("@CodAgencia", SqlDbType.VarChar).Value =  oBE.CodAgencia; 

                        cmd.Parameters.Add("@MensajeOutput", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                        cmd.ExecuteNonQuery();
                        Mensaje = cmd.Parameters["@MensajeOutput"].Value.ToString();

                    }
                }
            }
            catch (Exception ex)
            {

                throw (ex);
            }

            return Mensaje;
        }

        public UsuarioMantenimientoResponse UsuarioDetalle(UsuarioMantenimientoResponse oBE)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            UsuarioMantenimientoResponse objUsuario = null;
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_USUARIO_OBTENER_DETALLE]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@CodUsuario", SqlDbType.Int).Value =  oBE.CodUsuario;

                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    objUsuario = new UsuarioMantenimientoResponse();


                                    if (!Convert.IsDBNull(reader["CodUsuario"])) objUsuario.CodUsuario = Convert.ToInt32(reader["CodUsuario"]);
                                    if (!Convert.IsDBNull(reader["CodPerfil"])) objUsuario.CodPerfil = Convert.ToInt32(reader["CodPerfil"]);
                                    if (!Convert.IsDBNull(reader["DescripcionPerfil"])) objUsuario.DescripcionPerfil = Convert.ToString(reader["DescripcionPerfil"]);
                                    if (!Convert.IsDBNull(reader["CorreoElectronico"])) objUsuario.CorreoElectronico = Convert.ToString(reader["CorreoElectronico"]);
                                    if (!Convert.IsDBNull(reader["UsuarioWeb"])) objUsuario.UsuarioWeb = Convert.ToString(reader["UsuarioWeb"]);
                                    if (!Convert.IsDBNull(reader["NombresApellidos"])) objUsuario.NombresApellidos = Convert.ToString(reader["NombresApellidos"]);
                                    if (!Convert.IsDBNull(reader["Estado"])) objUsuario.Estado = Convert.ToString(reader["Estado"]);
                                    if (!Convert.IsDBNull(reader["UsuarioRegistro"])) objUsuario.UsuarioRegistro = Convert.ToString(reader["UsuarioRegistro"]);
                                    if (!Convert.IsDBNull(reader["CodAgencia"])) objUsuario.CodAgencia = Convert.ToString(reader["CodAgencia"]);

                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw (ex);
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw (ex);
            }

            return objUsuario;
        }

        public string UsuarioActualizar(UsuarioMantenimientoResponse oBE)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            string Mensaje = string.Empty;

            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {

                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_USUARIO_ACTUALIZAR]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@CodUsuario", SqlDbType.Int).Value = oBE.CodUsuario;
                        cmd.Parameters.Add("@Perfil", SqlDbType.VarChar).Value = oBE.Perfil;
                        cmd.Parameters.Add("@DescripcionPerfil", SqlDbType.VarChar).Value = oBE.DescripcionPerfil;
                        cmd.Parameters.Add("@CorreoElectronico", SqlDbType.VarChar).Value = oBE.CorreoElectronico;
                        cmd.Parameters.Add("@UsuarioWeb", SqlDbType.VarChar).Value = oBE.UsuarioWeb;
                        cmd.Parameters.Add("@NombresApellidos", SqlDbType.VarChar).Value = oBE.NombresApellidos;
                        cmd.Parameters.Add("@UsuarioRegistro", SqlDbType.VarChar).Value = oBE.UsuarioRegistro;
                        cmd.Parameters.Add("@Estado", SqlDbType.VarChar).Value = oBE.Estado;
                        cmd.Parameters.Add("@CodAgencia", SqlDbType.VarChar).Value =  oBE.CodAgencia ;

                        cmd.Parameters.Add("@MensajeOutput", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                        cmd.ExecuteNonQuery();
                        Mensaje = cmd.Parameters["@MensajeOutput"].Value.ToString();

                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception("Error al actualizar el usuario");
            }

            return Mensaje;
        }
    }
}
