﻿using Agrobanco.Control.DTO;
using Agrobanco.Control.Filter;
using Agrobanco.Reclamaciones.API.Models;
using Agrobanco.Reclamaciones.API.Providers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.Reclamaciones.API.Controllers
{
    [Route("api/accesoSAR")]
    [ApiController]
    public class AccessSARController : ControllerBase
    {
        [HttpPost]
        [ProducesResponseType(200, Type = typeof(AuthAccessResponse))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAppHeadersRequest]
        public object GenerarToken(
            [Required][FromHeader(Name = "X-AppKey")] string appkey,
            [Required][FromHeader(Name = "X-AppCode")] string appcode,
            [Required][FromBody] AuthAccessRequest access)
        {
            try
            {
                ProxySARAPI proxi = new ProxySARAPI();
                var accessDTO = new AccessDTO
                {
                    CorreoElectronico = access.CorreoElectronico,
                    CodigoUsuario = access.CodigoUsuario,
                    NombreUsuario = access.NombreUsuario,
                    NumeroDocumento =access.NumeroDocumento
                };
                var response = proxi.TokenSARApi(accessDTO, appkey,appcode, out string authorizationResponse);
                Response.Headers.Add("Access-Control-Expose-Headers", "Authorization");
                Response.Headers.Add("Authorization", authorizationResponse);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return ex;
            }
        }
    }
}
