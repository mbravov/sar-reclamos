﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.Reclamaciones.API.Models
{
    public class UbigeoBE
    {
        public string CodUbigeo { get; set; }
        public string CodDepartamento { get; set; }
        public string CodProvincia { get; set; }
        public string CodDistrito { get; set; }
        public string Departamento { get; set; }
        public string Provincia { get; set; }
        public string Distrito { get; set; }
    }
}
