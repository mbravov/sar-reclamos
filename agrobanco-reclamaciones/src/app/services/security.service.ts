import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

// const UrlBase_SARAPI = environment.UrlBase_SARAPI;
const APP_CODE = environment.AppCode;
const APP_KEY = environment.AppKey;

@Injectable({
  providedIn: 'root'
})
export class SecurityService {

  constructor() { }

}
