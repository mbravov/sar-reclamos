﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades.RequerimientoAsignacion
{
    public class RequerimientoAsignacionRequest
    {
        public Int64 CodRequerimientoMovimiento { get; set; }
        public Int64 CodRequerimiento { get; set; }
    }
}
