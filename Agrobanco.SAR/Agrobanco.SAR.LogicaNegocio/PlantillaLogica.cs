﻿using Agrobanco.SAR.Datos;
using Agrobanco.SAR.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.LogicaNegocio
{
    public class PlantillaLogica
    {
        private readonly PlantillaDatos plantillaDatos;
        public PlantillaLogica() {
            plantillaDatos = new PlantillaDatos();
        }

        public List<PlantillaBE> PlantillaObtener(PlantillaBE oBE)
        {
            return plantillaDatos.PlantillaObtener(oBE);
        }
        public Int64 Registrar(PlantillaBE oBE)
        {
            return plantillaDatos.Registrar(oBE);
        }
        public bool Desactivar(PlantillaBE oBE)
        {
            return plantillaDatos.Desactivar(oBE);
        }
    }
}
