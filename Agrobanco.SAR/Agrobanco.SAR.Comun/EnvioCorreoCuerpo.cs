﻿using Agrobanco.SAR.Entidades;
using Agrobanco.SAR.Entidades.CNA_Minuta;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Agrobanco.SAR.Comun
{
    public static class EnvioCorreoCuerpo
    {
      //  private string rutaProdHabilitado = ApplicationKeys.rutaProdHabilitado;
        
        public static string CuerpoCorreoRegistrarDocumento(RequerimientoBE oRequerimientoBE, string webRootPath)
        {
            string body = string.Empty;
            var ruta = "";

            if (ApplicationKeys.rutaProdHabilitado == "0")
            {
                ruta = webRootPath + "\\Plantillas\\DocumentoExterno.html";
            }
            else {
                ruta = ApplicationKeys.rutaPlantillaCorreosProd + "DocumentoExterno.html";
            }
          
            //var _stPri_Baja = "<label style='color:#248ef3'>Baja</label>";
            //var _stPri_Media = "<label style='color:#41af4f'>Media</label>";
            //var _stPri_Alta = "<label style='color:#f5a623'>Alta</label>";
            //var _stPri_Urgente = "<label style='color:#ff005a'>Urgente</label>";
            //var _stPrioridad = _stPri_Baja;



            try
            {
                using (StreamReader reader = new StreamReader(ruta))
                {
                    body = reader.ReadToEnd();
                }

                string NombresApellidosRazonSocialPrefijo = "Nombres y Apellidos: ";
                string NombresApellidosRazonSocialDato = oRequerimientoBE.Nombres + " " + oRequerimientoBE.PrimerApellido + " " + oRequerimientoBE.SegundoApellido;

                if (oRequerimientoBE.TipoDocumentoDescripcion == ConstantesTipoDocumento.TipoDocumentoRUC)
                {
                    NombresApellidosRazonSocialPrefijo = "Razón Social: ";
                    NombresApellidosRazonSocialDato = oRequerimientoBE.RazonSocial;
                }

                body = body.Replace("{NroRequerimiento}", oRequerimientoBE.TipoRequerimiento + " "+ oRequerimientoBE.NroRequerimiento);
                body = body.Replace("{Agencia}", oRequerimientoBE.Agencia);
                body = body.Replace("{DireccionAgencia}", oRequerimientoBE.DireccionAgencia);
                body = body.Replace("{EsMenor}",  (oRequerimientoBE.EsMenor==0?"No":"Si"));
                body = body.Replace("{TipoDocumento}", oRequerimientoBE.TipoDocumentoDescripcion);
                body = body.Replace("{NumDocumento}", oRequerimientoBE.NroDocumento);
                
                body = body.Replace("{NombreApellidosRazonSocial}", string.Concat(NombresApellidosRazonSocialPrefijo, NombresApellidosRazonSocialDato));

                body = body.Replace("{Departamento}", oRequerimientoBE.Departamento);
                body = body.Replace("{Provincia}", oRequerimientoBE.Provincia);
                body = body.Replace("{Distrito}", oRequerimientoBE.Distrito);

                body = body.Replace("{Producto}", oRequerimientoBE.Producto);
                body = body.Replace("{Motivo}", oRequerimientoBE.Motivo);
                body = body.Replace("{Descripcion}", oRequerimientoBE.Descripcion);
                body = body.Replace("{FormaEnvio}", oRequerimientoBE.FormaEnvio);
                
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return body;
        }

        public static string CuerpoCorreoRequerimientoAsignado(RequerimientoBE oRequerimientoBE , string webRootPath)
        {
            string body = string.Empty;
            var ruta = "";

            if (ApplicationKeys.rutaProdHabilitado == "0")
            {
                ruta = webRootPath + "\\Plantillas\\PLantillaRequerimientoAsignado.html";
            }
            else
            {
                ruta = ApplicationKeys.rutaPlantillaCorreosProd + "PLantillaRequerimientoAsignado.html";
            }

            try
            {
                using (StreamReader reader = new StreamReader(ruta))
                {
                    body = reader.ReadToEnd();
                }
                DateTime fecha = DateTime.Today;
                string Documento = oRequerimientoBE.TipoDocumentoDescripcion + "-" + oRequerimientoBE.NroDocumento;                
                string fechaAsignacion = fecha.ToString("dd/MM/yyyy");

                string NombresApellidosRazonSocialPrefijo = "Nombres y Apellidos: ";
                string NombresApellidosRazonSocialDato = oRequerimientoBE.NombresApellidos;

                body = body.Replace("{NroRequerimiento}", oRequerimientoBE.NroRequerimiento);
                body = body.Replace("{Origen}", oRequerimientoBE.Origen);
                body = body.Replace("{TipoRequerimiento}", oRequerimientoBE.TipoRequerimiento);
                body = body.Replace("{TipoNroDocumento}", Documento);

                if (oRequerimientoBE.TipoDocumentoDescripcion == ConstantesTipoDocumento.TipoDocumentoRUC)
                {
                    NombresApellidosRazonSocialPrefijo = "Razón Social: ";
                    NombresApellidosRazonSocialDato = oRequerimientoBE.RazonSocial;
                }

                body = body.Replace("{NombreApellidosRazonSocial}", string.Concat(NombresApellidosRazonSocialPrefijo, NombresApellidosRazonSocialDato));
                body = body.Replace("{Motivo}", oRequerimientoBE.Motivo);
                body = body.Replace("{FechaRegistro}", oRequerimientoBE.FechaRegistro.ToString());
                body = body.Replace("{UsuarioAsignado}", oRequerimientoBE.UsuarioAsignado);
                body = body.Replace("{FechaAsignacion}", fechaAsignacion);           
            }
            catch (Exception e)
            {
                throw e;
            }
            return body;
        }

        public static string CuerpoCorreoRequerimientoDerivadarOCM(RequerimientoBE oRequerimientoBE, string webRootPath)
        {
            string body = string.Empty;
            var ruta = "";

            if (ApplicationKeys.rutaProdHabilitado == "0")
            {
                ruta = webRootPath + "\\Plantillas\\PLantillaRequerimientoDerivarOCM.html";
            }
            else
            {
                ruta = ApplicationKeys.rutaPlantillaCorreosProd + "PLantillaRequerimientoDerivarOCM.html";
            }

            try
            {
                using (StreamReader reader = new StreamReader(ruta))
                {
                    body = reader.ReadToEnd();
                }
                DateTime fecha = DateTime.Today;
                string Documento = oRequerimientoBE.TipoDocumentoDescripcion + "-" + oRequerimientoBE.NroDocumento;
                string fechaAsignacion = fecha.ToString("dd/MM/yyyy");

                string NombresApellidosRazonSocialPrefijo = "Nombres y Apellidos: ";
                string NombresApellidosRazonSocialDato = oRequerimientoBE.NombresApellidos;

                body = body.Replace("{NroRequerimiento}", oRequerimientoBE.NroRequerimiento);
                body = body.Replace("{Origen}", oRequerimientoBE.Origen);
                body = body.Replace("{TipoRequerimiento}", oRequerimientoBE.TipoRequerimiento);
                body = body.Replace("{TipoNroDocumento}", Documento);
                body = body.Replace("{NombreApellidos}", oRequerimientoBE.NombresApellidos);
                body = body.Replace("{RazonSocial}", oRequerimientoBE.RazonSocial);

                if (oRequerimientoBE.TipoDocumentoDescripcion == ConstantesTipoDocumento.TipoDocumentoRUC)
                {
                    NombresApellidosRazonSocialPrefijo = "Razón Social: ";
                    NombresApellidosRazonSocialDato = oRequerimientoBE.RazonSocial;
                }

                body = body.Replace("{NombreApellidosRazonSocial}", string.Concat(NombresApellidosRazonSocialPrefijo, NombresApellidosRazonSocialDato));
                body = body.Replace("{Motivo}", oRequerimientoBE.Motivo);
                body = body.Replace("{FechaRegistro}", oRequerimientoBE.FechaRegistro.ToString());
                body = body.Replace("{UsuarioAsignado}", oRequerimientoBE.UsuarioAsignado);
                body = body.Replace("{FechaAsignacion}", fechaAsignacion);
            }
            catch (Exception e)
            {
                throw e;
            }
            return body;
        }

        public static string CuerpoCorreoRequerimientoDerivadarNotificacion(RequerimientoBE oRequerimientoBE, string webRootPath)
        {
            string body = string.Empty;
            var ruta = "";

            if (ApplicationKeys.rutaProdHabilitado == "0")
            {
                ruta = webRootPath + "\\Plantillas\\PLantillaRequerimientoDerivarParaNotificacion.html";
            }
            else
            {
                ruta = ApplicationKeys.rutaPlantillaCorreosProd + "PLantillaRequerimientoDerivarParaNotificacion.html";
            }

            try
            {
                using (StreamReader reader = new StreamReader(ruta))
                {
                    body = reader.ReadToEnd();
                }
                DateTime fecha = DateTime.Today;
                string Documento = oRequerimientoBE.TipoDocumentoDescripcion + "-" + oRequerimientoBE.NroDocumento;
                string fechaAsignacion = fecha.ToString("dd/MM/yyyy");

                string NombresApellidosRazonSocialPrefijo = "Nombres y Apellidos: ";
                string NombresApellidosRazonSocialDato = oRequerimientoBE.NombresApellidos;

                body = body.Replace("{NroRequerimiento}", oRequerimientoBE.NroRequerimiento);
                body = body.Replace("{Origen}", oRequerimientoBE.Origen);
                body = body.Replace("{TipoRequerimiento}", oRequerimientoBE.TipoRequerimiento);
                body = body.Replace("{TipoNroDocumento}", Documento);


                if (oRequerimientoBE.TipoDocumentoDescripcion == ConstantesTipoDocumento.TipoDocumentoRUC)
                {
                    NombresApellidosRazonSocialPrefijo = "Razón Social: ";
                    NombresApellidosRazonSocialDato = oRequerimientoBE.RazonSocial;
                }                               

                body = body.Replace("{NombreApellidosRazonSocial}", string.Concat(NombresApellidosRazonSocialPrefijo, NombresApellidosRazonSocialDato));

                body = body.Replace("{Motivo}", oRequerimientoBE.Motivo);
                body = body.Replace("{FechaRegistro}", oRequerimientoBE.FechaRegistro.ToString());
                body = body.Replace("{UsuarioAsignado}", oRequerimientoBE.UsuarioAsignadoNotificacion);
                body = body.Replace("{FechaAsignacion}", fechaAsignacion);
            }
            catch (Exception e)
            {
                throw e;
            }
            return body;
        }

        public static string CuerpoCorreoConsulta(ConsultaBE oConsultaBE, string webRootPath)
        {
            string body = string.Empty;
            var ruta = "";

            if (ApplicationKeys.rutaProdHabilitado == "0")
            {
                ruta = webRootPath + "\\Plantillas\\PlantillaConsultaRegistrado.html";
            }
            else
            {
                ruta = ApplicationKeys.rutaPlantillaCorreosProd + "PlantillaConsultaRegistrado.html";
            }

            try
            {
                using (StreamReader reader = new StreamReader(ruta))
                {
                    body = reader.ReadToEnd();
                }
                DateTime fecha = DateTime.Today;            
                string FechaRegistro = fecha.ToString("dd/MM/yyyy");
                string TipoDocumento = "";

                switch (oConsultaBE.TipoDocumento)
                {
                    case 1:
                        TipoDocumento = "DNI";
                        break;
                    case 2:
                        TipoDocumento = "RUC";
                        break;
                    case 3:
                        TipoDocumento = "Pasaporte";
                        break;
                    case 4:
                        TipoDocumento = "CE";
                        break;
                    case 5:
                        TipoDocumento = "Otros"; ;
                        break;
                }

                body = body.Replace("{CodConsulta}", oConsultaBE.CodConsulta);
                body = body.Replace("{TipoDocumento}", TipoDocumento);
                body = body.Replace("{NroDocumento}", oConsultaBE.NroDocumento);
                body = body.Replace("{NombresCompletos}", oConsultaBE.NombresCompletos);
              
                body = body.Replace("{Direccion}", oConsultaBE.Direccion);
                body = body.Replace("{CorreoElectronico}", oConsultaBE.CorreoElectronico);
                body = body.Replace("{Celular}", oConsultaBE.Celular);
                body = body.Replace("{Descripcion}", oConsultaBE.Descripcion);               
                body = body.Replace("{UsuarioRegistro}", oConsultaBE.UsuarioRegistro);
                body = body.Replace("{FechaRegistro}", FechaRegistro);
            }
            catch (Exception e)
            {
                throw e;
            }
            return body;
        }



        public static string GenerarCorreoConfiguracionAccion(
            string strCuerpo, 
            string webRootPath)
        {
            string body = string.Empty;
            var ruta = "";

            if (ApplicationKeys.rutaProdHabilitado == "0")
            {
                ruta = webRootPath + "\\Plantillas\\PlantillaConfiguracionAccion.html";
            }
            else
            {
                ruta = ApplicationKeys.rutaPlantillaCorreosProd + "PlantillaConfiguracionAccion.html";
            }

            try
            {
                using (StreamReader reader = new StreamReader(ruta))
                {
                    body = reader.ReadToEnd();
                }

                body = body.Replace("{strCuerpo}", strCuerpo);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return body;
        }

    }
}
