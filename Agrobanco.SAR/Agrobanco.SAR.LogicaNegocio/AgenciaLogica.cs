﻿using Agrobanco.SAR.Datos;
using Agrobanco.SAR.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.LogicaNegocio
{      
    public class AgenciaLogica
    {
        private readonly ParametroDatos parametroDatos;

        public AgenciaLogica()
        {
            parametroDatos = new ParametroDatos();
        }

        public List<AgenciaBE> ObtenerListarAgencias()
        {
            return parametroDatos.ListarAgencia();
        }
        
    }


}
