(function(window) {
    window["env"] = window["env"] || {};
    window["env"]["UrlBase_SGSAPI"] = 'http://10.241.161.121/SGS.API/api';
    window["env"]["UrlBase_SSA"] = 'http://10.241.161.121/SSA';
    window["env"]["UrlBase_SARAPI"] = 'http://localhost:11695/api';
    // window["env"]["UrlBase_SARAPI"] = 'http://10.240.147.65/SAR.API.SP3/api';

    window["env"]["AppKey"] = 'QWdyb2JhbmNvX1JlY2xhbW9zX0FwbGljYWNpb24=';
    window["env"]["AppCode"] = '7A9C69BD-F6FE-46B2-9A87-F4C380CB1B40';
    window["env"]["recaptcha_siteKey"] = '6LeWg6waAAAAAMEE6wzwaCN4i9Ve23u4B1klSUE3';
    window["env"]["number_whatsapp"] = '995808307';
    window["env"]["preguntas_frecuentes"] = 'http://localhost:54352/preguntas-frecuentes';
    window["env"]["redireccion"] = 'http://localhost:4250/';
})(this);
