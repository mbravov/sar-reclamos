export interface RequerimientoInterface {
  CodRequerimiento: number;
  NroRequerimiento: string;
  CodAgencia: number;
  DireccionAgencia : string;
  EsMenor: number;
  CodProducto: number;
  CodMotivo: number;
  CodTipoRequerimiento: number;
  CodOrigen: number;
  CodFormaEnvio: number;
  FormaEnvio: string;
  Descripcion: string;
  Estado: number;
  DescripcionEstado: string;
  UsuarioRegistro: string;
  CodRequerimientoDetalle : number;
  TipoDocumento : number;
  NroDocumento : string;
  PrimerApellido : string;
	SegundoApellido : string;
	Nombres : string;
	NombresApellidos :string;
	RazonSocial : string;
	CodUbigeo : string;
	Direccion : string;
	Referencia : string;
	CorreoElectronico : string;
	Celular : string;
	Telefono : string;
 
  Archivo1: any;
  Archivo2: any;
  Archivo3: any;
  Archivo4: any;
}
