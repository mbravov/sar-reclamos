export interface RequerimientoConsulta{
    NumeroRequerimiento : string,
    NumeroDocumento : string,
    FechaRegistro : Date,
    NombreApellidos : string,
    UsuarioAsignado : string,
    FechaAtencion : Date,
    CodTipoRequerimiento : string,
    DescTipoRequerimiento : string,
    Agencia : string,
    Estado : string
}