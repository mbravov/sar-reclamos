﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Threading.Tasks;
using System.Configuration;
using System.ComponentModel.Composition;
using Agrobanco.Security.Criptography;
using System.Data.SqlClient;
using System.ComponentModel.Composition.Hosting;
//using Agrobanco.SIGD.Comun;
using Microsoft.Win32;
using Agrobanco.SAR.Control.Base;
using Agrobanco.SAR.Comun;

namespace Agrobanco.SAR.Datos.SqlServer.Acceso
{
    public class SqlServerAccess
    {
        private string _connectionString;
        [Import(typeof(IValueReader), AllowRecomposition = true)]
        private IValueReader _decriptService;
        private int SQLApplicationNameEnableEncrip =  Convert.ToInt16(ApplicationKeys.SQLApplicationNameEnableEncrip);
        private int EnableEncriptHab = 1;
        private int EnableEncriptDes = 0;

        
        public SqlServerAccess()
        {
            
        }

        public string ClaveConnectionStringSQL()
        {
            try
            {
                Compose();
                var conectionStringBuilder = new SqlConnectionStringBuilder();
                _decriptService.Application =   ApplicationKeys.SQLRegeditFolder;
                _decriptService.ClaveEncriptado =  ApplicationKeys.RegeditPass;
                if (SQLApplicationNameEnableEncrip == EnableEncriptHab)
                {
                    conectionStringBuilder = new SqlConnectionStringBuilder
                    {
                        DataSource = _decriptService.ReadValue("Server"),
                        InitialCatalog = _decriptService.ReadValue("Database"),
                        UserID = _decriptService.ReadValue("Usuario"),
                        Password = _decriptService.ReadValue("Clave")
                    };
                }
                else if (SQLApplicationNameEnableEncrip == EnableEncriptDes)
                {
                    conectionStringBuilder = new SqlConnectionStringBuilder
                    {
                        DataSource = ReadKey("Server"),
                        InitialCatalog = ReadKey("Database"),
                        UserID = ReadKey("Usuario"),
                        Password = ReadKey("Clave")
                    };
                }
                _connectionString = conectionStringBuilder.ConnectionString;
            }
            catch (Exception e)
            {
                throw e;
                //GenerarLog objLOG = new GenerarLog();
                //string detalleError = "Conexion SQL SERVER" + " - " +
                //    DateTime.Now + "\n" + "\n" +
                //    e.Message + "\n" +
                //    "-------------------------------------------------------------------------------------------------------------";
                //objLOG.GenerarArchivoLog(detalleError);
            }
            return _connectionString;
        }
        private void Compose()
        {
            var encryptCatalog = new AssemblyCatalog(System.Reflection.Assembly.GetAssembly(typeof(IValueReader)));
            var catalog = new AggregateCatalog(encryptCatalog);
            var container = new CompositionContainer(catalog);
            container.ComposeParts(this);
        }


        private string ReadKey(string value)
        {
            Registry.LocalMachine.OpenSubKey("SOFTWARE", true);
            RegistryKey masterKey = Registry.LocalMachine.CreateSubKey("SOFTWARE\\AGROBANCO\\" + ApplicationKeys.SQLRegeditFolder); 
            string valueret = "";
            if (masterKey != null)
            {
                valueret = masterKey.GetValue(value).ToString();
            }
            masterKey.Close();
            return valueret;
        }
    }
}
