﻿using Agrobanco.SAR.Datos;
using Agrobanco.SAR.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.LogicaNegocio
{
    public class RecursoLogica
    {
        private readonly RecursoDatos recursoDatos;

        public RecursoLogica()
        {
            recursoDatos = new RecursoDatos();
        }
        public List<RecursoBE> Listar(RecursoBE oBE)
        {

            return recursoDatos.Listar(oBE);
        }

        public List<RecursoBE> RecursoObtener(RecursoBE oBE)
        {
            return recursoDatos.RecursoObtener(oBE);
        }

        public bool RecursoEliminar(RecursoBE oBE)
        {
            return recursoDatos.RecursoEliminar(oBE);
        }

    }
}
