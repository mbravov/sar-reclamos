﻿using Agrobanco.Control.DTO;
using Agrobanco.Control.Filter;
using Agrobanco.Control.Implementation;
using Agrobanco.Control.Interface;
using Agrobanco.Control.Security;
using Agrobanco.Reclamaciones.API.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.Reclamaciones.API.Controllers
{
    [Route("api/acceso")]
    [ApiController]
    public class AccessController : ControllerBase
    {
        private readonly IAccessControl _accesscontrol;

        public AccessController()
        {
            _accesscontrol = new AccessControl();
        }


        [HttpPost("generarToken")]
        [ProducesResponseType(200, Type = typeof(AuthAccessResponse))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ProducesResponseType(502, Type = typeof(ResponseError))]
        [ValidateAppHeadersRequest]
        public object GenerarToken(
            [Required][FromHeader(Name = "X-AppKey")] string appkey,
            [Required][FromHeader(Name = "X-AppCode")] string appcode)
        {
            try
            {

                string token = "";

                AccessDTO result = _accesscontrol.generateToken(GetAccess(),
                    appkey,
                    appcode,
                    ref token
                );

                AuthAccessResponseData dataResponse = new AuthAccessResponseData();
                dataResponse.FechaInicioVigencia = result.FechaInicioVigencia;
                dataResponse.FechaFinVigencia = result.FechaFinVigencia;
                dataResponse.NombreUsuario = result.NombreUsuario;
                dataResponse.NumeroDocumento = result.NumeroDocumento;
                dataResponse.CodigoUsuario = result.CodigoUsuario;
                dataResponse.CorreoElectronico = result.CorreoElectronico;


                var response = new AuthAccessResponse
                {
                    AuthAccess = dataResponse
                };

                Response.Headers.Add("Access-Control-Expose-Headers", "Authorization");
                Response.Headers.Add("Authorization", "Bearer " + token);

                return Ok(response);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private AccessDTO GetAccess()
        {
            return AuthAccess.getInstance().getAccess();
        }
    }
}
