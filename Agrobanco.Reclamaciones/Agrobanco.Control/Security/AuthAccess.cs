﻿using Agrobanco.Control.DTO;
using Agrobanco.Security.Criptography;
using Microsoft.Extensions.Configuration;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.Control.Security
{
    public class AuthAccess : IAuthAccess
    {
        private static AuthAccess _InstanceAutAccess = null;
        [Import(typeof(IValueReader), AllowRecomposition = true)]
        private IValueReader _decriptService;
        [NonSerialized]
        private bool _isAutoConnectionMgmt = false;

        private int AuthorizeEnableEncrip = Convert.ToInt16(Util.Util.ObtenerValueFromKey("AccessEnableEncryption"));
        private int EnableEncriptHab = 1;
        private int EnableEncriptDes = 0;
        public static AuthAccess getInstance()
        {
            if (_InstanceAutAccess == null)
            {
                _InstanceAutAccess = new AuthAccess();
            }
            return _InstanceAutAccess;
        }
        public AccessDTO getAccess()
        {
            AccessDTO objAccessDTO = null;
            try
            {
                var folder = Util.Util.ObtenerValueFromKey("AccessFolder");

                _isAutoConnectionMgmt = true;
                Compose();
                _decriptService.Application = folder;
                _decriptService.ClaveEncriptado = Util.Util.ObtenerValueFromKey("RegeditPass");

                if (AuthorizeEnableEncrip == EnableEncriptHab)
                {
                    objAccessDTO = new AccessDTO
                    {
                        CorreoElectronico = _decriptService.ReadValue("CorreoElectronico"),
                        CodigoUsuario = _decriptService.ReadValue("CodigoUsuario"),
                        NombreUsuario = _decriptService.ReadValue("NombreUsuario"),
                        NumeroDocumento = _decriptService.ReadValue("NumeroDocumento")
                    };
                }
                else if (AuthorizeEnableEncrip == EnableEncriptDes)
                {
                    
                    objAccessDTO = new AccessDTO
                    {
                        CorreoElectronico = Util.Util.ReadKey("CorreoElectronico", folder),
                        CodigoUsuario = Util.Util.ReadKey("CodigoUsuario", folder),
                        NombreUsuario = Util.Util.ReadKey("NombreUsuario", folder),
                        NumeroDocumento = Util.Util.ReadKey("NumeroDocumento", folder),
                    };
                }

            }
            catch (Exception e)
            {
                throw e;
            }
            return objAccessDTO;
        }

        private void Compose()
        {
            var encryptCatalog = new AssemblyCatalog(System.Reflection.Assembly.GetAssembly(typeof(IValueReader)));
            var catalog = new AggregateCatalog(encryptCatalog);
            var container = new CompositionContainer(catalog);
            container.ComposeParts(this);
        }
    }
}
