﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades.RequerimientoReporte
{
    public class RequerimientoReporteFiltroResponse
    {
		public string NroRequerimiento { get; set; }
		public string EsMenor { get; set; }
		public string TipoDocumento { get; set; }
		public string NroDocumento { get; set; }
		public string NombresApellidos { get; set; }
		public string RazonSocial { get; set; }
		public string Descripcion { get; set; }
		public string Departamento { get; set; }
		public string Provincia { get; set; }
		public string Distrito { get; set; }
		public string Telefono { get; set; }
		public string Celular { get; set; }
		public string Producto { get; set; }
		public string Motivo { get; set; }
		public string TipoRequerimiento { get; set; }
		public string Origen { get; set; }
		public string Agencia { get; set; }
		public DateTime? FechaRegistro { get; set; }		
		public string Estado { get; set; }
		public string FormaEnvio { get; set; }


	}
}
