﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades.RequerimientoSeguimiento
{
    public class RequerimientoSeguimientoFiltroResponse
    {
		public DateTime? FechaRegistro { get; set; }
		public DateTime? FechaAsignacion { get; set; }
		public DateTime? FechaGestion { get; set; }
		public DateTime? FechaAtencion { get; set; }
		public string Estado { get; set; }
		public string TipoRequerimiento { get; set; }
		public string UsuarioAsignado { get; set; }
		public int DiasTranscurrido { get; set; }
		public string NroRequerimiento { get; set; }
	}
}
