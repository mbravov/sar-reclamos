USE DBAgrSAR
GO

DROP PROCEDURE IF EXISTS SP_CNA_GARANTIAS_OBTENER
GO
--=================================================================================================================================
-- FECHA DE CREACI�N		: 15/12/2021
-- FECHA DE MODIFICACI�N	:
-- CREADO POR				: CECILIA MARRUFO
-- STORE PROCEDURE			: SP_CNA_GARANTIAS_OBTENER
-- PARAMETOS				: @NumeroDocumento VARCHAR(20),
--							  @TipoDocumento VARCHAR(20) 
-- FUNCIONALIDAD			:  Obtiene lista de garantias
--================================================================================================================================

	CREATE PROC SP_CNA_GARANTIAS_OBTENER

	@NumeroDocumento VARCHAR(20) ,
	@TipoDocumento VARCHAR(20) 


	 AS

	 BEGIN
	  SELECT --ROCREF CODGARANTIA , 
	   --ROCTYP TIPOGARANTIA,
	   'Garant�a Hipotecaria' DescripcionGarantia,
	   'AlbertoEdizabal' NombreGarante,
	   '45456545' DNIGarante, 
	   --ROCAPA IMPORTEGRAVAMEN ,
	   CAST(format(454545,'N') as varchar) MontoGravamen ,
	   CAST('888777' AS varchar) NumeroPartida,
	   CAST(COALESCE('SURCO','')AS varchar) OficinaRegistral,
	   CAST(COALESCE('PR000','0')AS varchar) NumeroPrestamo
		FROM  CreditoCliente
		WHERE NumeroDocumento=@NumeroDocumento AND TipoDocumento=@TipoDocumento


	 END