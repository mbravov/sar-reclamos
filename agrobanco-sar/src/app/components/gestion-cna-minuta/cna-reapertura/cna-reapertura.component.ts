import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FiltroAccionService } from './../../../services/filtro-accion.service';
import { CnaMinutaOcmService } from './../../../services/cna-minuta-ocm.service';
import { ConfiguracionAccionService } from './../../../services/configuracion-accion.service';
import { SecurityService } from './../../../services/security.service';
import { DialogService } from './../../../services/comun/dialog.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Inject } from '@angular/core';
import { Constantes } from 'src/app/comun/constantes';

@Component({
  selector: 'app-cna-reapertura',
  templateUrl: './cna-reapertura.component.html',
  styleUrls: ['./cna-reapertura.component.css']
})
export class CnaReaperturaComponent implements OnInit {

  loading:boolean = false;
  form! : FormGroup;
  parametros!: any[];
  formSubmitted:boolean = false;
  lengthDescripcion: number  = 300;
  emision: any;
  IdConfiguracionAccion:any;

  constructor(
    private _formBuilder: FormBuilder,
    private _dialogService: DialogService,
    private _securityService: SecurityService,
    private _cnaMinutaOcmService : CnaMinutaOcmService,
    private _filtroAccionService : FiltroAccionService,
    private _configuracionAccion : ConfiguracionAccionService,
    public dialogRef: MatDialogRef<CnaReaperturaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.emision = data.CreditoClienteDetalle;
    this.IdConfiguracionAccion = data.IdConfiguracionAccion;
   }

  ngOnInit(): void {
    this.inicializarFormulario();
  }

  inicializarFormulario() : void {

    this.form = this._formBuilder.group({
      Descripcion: [null,  [Validators.required, Validators.minLength(5)]],
    });


  }

  registrar(){
    this.formSubmitted = true;

    if (this.form.invalid) {
      return;
    }


    var request = {
      "IdConfiguracionAccion" : this.IdConfiguracionAccion,
      "GrupoFiltro" : null,
      "Nombres": this.emision.nombres,
      "TipoDocumento": this.emision.tipoDocumento,
      "NroDocumento": this.emision.numeroDocumento,
      "emision": {
        "IdEmision" : +this.emision.idEmision,
        "IdCreditoCliente"  : +this.emision.idCreditoCliente,
        "Estado"  : this.emision.idEstado,
        "TipoFlujo"  :1,
        "strFechaEntregaOlva" : null,
        "MedioNotificacion": null,
        "Email" : null,
        "Whatsapp" : null,
        "FechaCargoEntrega" : null,
        "UsuarioRegistro" :  this._securityService.leerUsuarioWeb(),
        "UsuarioModificacion" :  this._securityService.leerUsuarioWeb()
      },
      "derivacion": {
        "IdDerivacion" : +this.emision.idDerivacion,
        "IdEmision" : +this.emision.idEmision,
        "Perfil" : this._securityService.leerPerfil(),
        "Estado" : this.emision.idEstado,
        "PerfilDerivado" : null,
        "FechaDerivado" : null,
        "CodAgencia" : null,
        "Agencia": this.emision.agencias,
        "UsuarioRegistro" : this._securityService.leerUsuarioWeb(),
        "UsuarioModificacion":  this._securityService.leerUsuarioWeb()
      },
      "comentarioEmision":
      {
          "IdEmision" : this.emision.idEmision,
          "IdDerivacion" : this.emision.idDerivacion,
          "IdDerivacionMovimiento" :  this.emision.idDerivacionMovimiento,
          "Texto" :  this.form.controls['Descripcion'].value,
          "UsuarioRegistro" :  this._securityService.leerUsuarioWeb(),
          "Archivo" : null,
          "NombreArchivo" : null,
          "CodTabla" : Constantes.ComentarioEmision,
          "DNI": this.emision.numeroDocumento,
          "Perfil": this._securityService.leerPerfil(),
          "CodAgencia":  this.emision.codAgencia,
          "Estado": this.emision.idEstado,
          "IdMotivo" : ''

      }
    };

    this._dialogService
    .openConfirmDialog('¿Está seguro de reapertura la emisión de la carta de no adeudo y minuta?')
    .afterClosed()
    .subscribe((res) => {
      if (res) {
          this._configuracionAccion.accion(request).subscribe((data:any)=>{
            console.log(data);
            debugger;
            if(data.response.success.resultado){
              this._dialogService.openMsgSuccessDialog("reaperturó correctamente");
              this.dialogRef.close({ data: true });
            }
            else{
              this._dialogService.openMsgSuccessDialog("Hubo un problema al derivar");
            }
          });
      }
    });
  }
}
