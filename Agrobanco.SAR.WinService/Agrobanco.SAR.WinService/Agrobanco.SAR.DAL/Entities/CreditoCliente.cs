﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SAR.DAL.Entities
{
    public class CreditoCliente
    {
        public int IdCreditoCliente { get; set; }
        public string NumeroCredito { get; set; }
        public int CodAgencia { get; set; }
        public string Agencia { get; set; }
        public string AnalistaProponente { get; set; }
        public string FechaCancelacion { get; set; }
        public string Banco { get; set; }
        public string TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
        public string Nombres { get; set; }
        public string Direccion { get; set; }
        public int TipoProcesamiento { get; set; }
        public int IdLaserficheCNA { get; set; }
        public bool Procesado { get; set; }
        public string UsuarioRegistro { get; set; }
        public string UsuarioModificacion { get; set; }

    }
}
