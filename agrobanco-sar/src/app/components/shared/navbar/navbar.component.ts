import { AfterViewInit, Component, OnInit } from '@angular/core';
import { DomService } from 'src/app/services/dom.service';
import { SecurityService } from 'src/app/services/security.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, AfterViewInit  {

  nombreUsuario!: string;
  constructor(public domService : DomService, public secService: SecurityService) {
    this.nombreUsuario = "";
  }
  ngAfterViewInit(): void {
    this.nombreUsuario = this.secService.leerNombreUsuario();

  }

  ngOnInit(): void {

    this.nombreUsuario = this.secService.leerNombreUsuario();

  }

  MenuOpenClick(): void{
    this.domService.ShowSideBar();
  }

}
