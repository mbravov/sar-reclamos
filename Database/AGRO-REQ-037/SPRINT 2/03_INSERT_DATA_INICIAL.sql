USE DBAgrSAR
GO

 
DELETE FROM Parametro where  Grupo IN (1000, 1200)
GO

INSERT INTO Parametro (Grupo,Parametro,Descripcion,Valor,Referencia,Orden,Estado) VALUES (0,1300,'Preguntas Frecuentes',null,null,0,1)
INSERT INTO Parametro (Grupo,Parametro,Descripcion,Valor,Referencia,Orden,Estado) VALUES (1300,1,'�Cu�les son los requisitos para solicitar un cr�dito agr�cola?','Agrobanco, otorga financiamiento para capital de trabajo, sostenimiento de cultivo, acopio y comercializaci�n de productos. Los requisitos generales son:  
<br>
<br> <b>Cr�dito Individual</b>:  
<ul>
<li>Copia de DNI del titular y c�nyuge.</li>
<li>Constancia de posesi�n y/o copia de t�tulo de propiedad.</li>
<li>Experiencia m�nima como agricultor de 3 a�os.</li>
<li>Cronograma de pagos, si tuviese alguna deuda con otras entidades financieras (no debe tener endeudamiento en m�s de 02 entidades).</li>
<li>Contar con buena calificaci�n en el sistema financiero.</li>              
</ul> 
<br> <b>Cr�dito Asociativo</b>:
<ul>
<li>Ser usuario inscrito, h�bil y reconocido por la Junta de su jurisdicci�n.</li>
<li>Copia del DNI vigente del titular.</li>
<li>Contar m�nimo con 02 hect�reas (propias/conducci�n) trabajadas durante la operaci�n.</li>
<li>�ltimo recibo de luz o agua.</li>			
<li>Experiencia de 03 a�os en el manejo del cultivo a financiar.</li>
<li>Cronograma de pagos, si tuviese alguna deuda con otras entidades financieras (no debe tener endeudamiento en m�s de 02 entidades).</li>
<li>Contar con buena calificaci�n en el sistema financiero, ni deudas comerciales vencidas.</li>
</ul> 
 
Para solicitar un cr�dito puede contactarse con el Oficial de Negocios de la oficina m�s cercana a su localidad. Ver directorio aqu�.  <a href="https://www.agrobanco.com.pe/ubica-nuestras-oficinas/">https://www.agrobanco.com.pe/ubica-nuestras-oficinas/</a>
 ','1',1,1)
INSERT INTO Parametro (Grupo,Parametro,Descripcion,Valor,Referencia,Orden,Estado) VALUES (1300,2,'�Cu�les son los requisitos para solicitar un cr�dito pecuario?','Agrobanco, otorga financiamiento para la compra de ganado y otros semovientes y mejoramiento gen�tico. Los requisitos generales son:
<ul>
<li>Copia de DNI.</li>
<li>Copia de recibo de agua y/o luz.</li>
<li>Productores con experiencia en la actividad pecuaria, que cuenten con terrenos propios o en posesi�n.</li>
<li>Contar con buena calificaci�n en el sistema financiero.</li>
 </ul>
Para solicitar un cr�dito puede contactarse con el Oficial de Negocios de la oficina m�s cercana a su localidad. Ver directorio aqu�. <a href="https://www.agrobanco.com.pe/ubica-nuestras-oficinas/">https://www.agrobanco.com.pe/ubica-nuestras-oficinas/</a>','2',2,1)
INSERT INTO Parametro (Grupo,Parametro,Descripcion,Valor,Referencia,Orden,Estado) VALUES (1300,3,'�C�mo solicito una reprogramaci�n?','Recuerde que una reprogramaci�n debe ser solicitada antes del vencimiento de su fecha de pago, para lo cual deber� comunicarse con su Oficial de Negocios para presentar su solicitud y copia de DNI. Asimismo, tiene la posibilidad de enviarnos su solicitud de reprogramaci�n a trav�s de nuestra Plataforma de Atenci�n al Usuario, ingrese a <a href="mailto:www.agrobanco.com.pe/atencion-al-cliente/solicitudes-de-informacion/">www.agrobanco.com.pe/atencion-al-cliente/solicitudes-de-informacion/</a>','3',3,1)
INSERT INTO Parametro (Grupo,Parametro,Descripcion,Valor,Referencia,Orden,Estado) VALUES (1300,4,'�D�nde ubico una oficina de Agrobanco?','Para ubicar la oficina de Agrobanco m�s cercana a tu localidad, ingresa qu�: <a href="https://www.agrobanco.com.pe/ubica-nuestras-oficinas/">https://www.agrobanco.com.pe/ubica-nuestras-oficinas/</a>','4',4,1)
INSERT INTO Parametro (Grupo,Parametro,Descripcion,Valor,Referencia,Orden,Estado) VALUES (1300,5,'�Cu�nto es mi deuda actual?','Para conocer el monto de su cuota, fecha de pr�ximo pago � saldo deudor debe ingresar a ','<a href="https://www.agrobanco.com.pe">https://www.agrobanco.com.pe/</a>',5,1)
INSERT INTO Parametro (Grupo,Parametro,Descripcion,Valor,Referencia,Orden,Estado) VALUES (1300,6,'�D�nde pago las cuotas de mi cr�dito?','Puedes realizar el pago de las cuotas en cualquier ventanilla del Banco de la Naci�n, Banco de Cr�dito o Banco Interbank, previa presentaci�n del n�mero de DNI del titular del cr�dito. Te recomendamos guardar los recibos de tus pagos.','6',6,1)


INSERT INTO Parametro (Grupo,Parametro,Descripcion,Valor,Referencia,Orden,Estado) VALUES (1000,1,'[X_FECHA_ACTUAL_X]','FechaActual','1',1,1)
INSERT INTO Parametro (Grupo,Parametro,Descripcion,Valor,Referencia,Orden,Estado) VALUES (1000,2,'[X_NOMBRES_APELLIDOS_X]','NombresApellidos','2',2,1)
INSERT INTO Parametro (Grupo,Parametro,Descripcion,Valor,Referencia,Orden,Estado) VALUES (1000,3,'[X_CORREO_X]','CorreoElectronico','2',3,1)
INSERT INTO Parametro (Grupo,Parametro,Descripcion,Valor,Referencia,Orden,Estado) VALUES (1000,4,'[X_DIRECCION_X]','Direccion','2',4,1)
INSERT INTO Parametro (Grupo,Parametro,Descripcion,Valor,Referencia,Orden,Estado) VALUES (1000,5,'[X_NRO_REQ_X]','NroRequerimiento','2',5,1)
INSERT INTO Parametro (Grupo,Parametro,Descripcion,Valor,Referencia,Orden,Estado) VALUES (1000,6,'[X_FECHA_REGISTRO_X]','FechaRegistro','3',6,1)
INSERT INTO Parametro (Grupo,Parametro,Descripcion,Valor,Referencia,Orden,Estado) VALUES (1000,7,'[X_NRO_CREDITO_X]','NroCredito','2',7,1)
INSERT INTO Parametro (Grupo,Parametro,Descripcion,Valor,Referencia,Orden,Estado) VALUES (1000,8,'[X_ESTADO_CREDITO_X]','EstadoCredito','2',8,1)
INSERT INTO Parametro (Grupo,Parametro,Descripcion,Valor,Referencia,Orden,Estado) VALUES (1000,9,'[X_SALDO_CREDITO_X]','SaldoCredito','2',9,1)
INSERT INTO Parametro (Grupo,Parametro,Descripcion,Valor,Referencia,Orden,Estado) VALUES (1000,10,'[X_NRO_DOCUMENTO_X]','NroDocumento','2',10,1) --SPRINT 2

INSERT INTO Parametro (Grupo,Parametro,Descripcion,Valor,Referencia,Orden,Estado) VALUES (1200,1,'Carta de Respuesta',null,null,1,1)
INSERT INTO Parametro (Grupo,Parametro,Descripcion,Valor,Referencia,Orden,Estado) VALUES (1200,2,'Constancia de no adeudo',null,null,2,1)
INSERT INTO Parametro (Grupo,Parametro,Descripcion,Valor,Referencia,Orden,Estado) VALUES (1200,3,'Constancia de deuda vigente',null,null,3,1)
