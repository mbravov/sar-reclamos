﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades
{
    public class ResponseReqBloqueado
    {
        public string UsuarioBloqueo { get; set; }
        public int EstaBloqueado { get; set; }
        public int CodTipoRequerimiento { get; set; }

        public int CodRequerimiento { get; set; }
        public string NombresApellidos { get; set; }
    }
}
