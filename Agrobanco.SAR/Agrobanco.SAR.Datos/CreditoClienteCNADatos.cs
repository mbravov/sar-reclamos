﻿using Agrobanco.SAR.Datos.SqlServer.Acceso;
using Agrobanco.SAR.Entidades;
using Agrobanco.SAR.Entidades.CNA_Minuta;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SAR.Datos
{
    public class CreditoClienteCNADatos
    {
        public string cadenaConexion;
        public CreditoClienteCNADatos()
        {

        }

        public async Task<List<CreditoClienteCNABE>> ObtenerCreditosCNA(CreditoClienteCNABE oBE)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            CreditoClienteCNABE oCreditosClienteBE = null;
            List<CreditoClienteCNABE> LstCreditosClienteBE = new List<CreditoClienteCNABE>();

            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("SP_CNA_CREDITOS_OBTENER", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@NumeroDocumento", SqlDbType.VarChar, 20).Value = oBE.Identificacion;
                        cmd.Parameters.Add("@TipoDocumento", SqlDbType.VarChar, 20).Value = oBE.TipoIDN;

                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    oCreditosClienteBE = new CreditoClienteCNABE();


                                    if (!reader.IsDBNull(0))
                                        oCreditosClienteBE.NroPrestamo = reader.GetString(0);

                                    if (!reader.IsDBNull(1))
                                        oCreditosClienteBE.UltimoCredito = reader.GetString(1);

                                    if (!reader.IsDBNull(2))
                                        oCreditosClienteBE.EstadoActual = reader.GetString(2);

                                    if (!reader.IsDBNull(3))
                                        oCreditosClienteBE.SaldoDeudo = reader.GetString(3);

                                    if (!reader.IsDBNull(4))
                                        oCreditosClienteBE.Capital = reader.GetString(4);

                                    if (!reader.IsDBNull(5))
                                        oCreditosClienteBE.InteresCompensatorio = reader.GetString(5);

                                    if (!reader.IsDBNull(6))
                                        oCreditosClienteBE.InteresMoratorio = reader.GetString(6);

                                    if (!reader.IsDBNull(7))
                                        oCreditosClienteBE.Otros = reader.GetString(7);



                                    LstCreditosClienteBE.Add(oCreditosClienteBE);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw (ex);
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw (ex);
            }
            return await Task.Run(() => LstCreditosClienteBE);

        }

        public async Task<List<GarantiaClienteBE>> ObtenerGarantias(CreditosClienteBE oBE)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            GarantiaClienteBE oGarantiaClienteBE = null;
            List<GarantiaClienteBE> LstGarantiaClienteBE = new List<GarantiaClienteBE>();

            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("SP_CNA_GARANTIAS_OBTENER", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@NumeroDocumento", SqlDbType.VarChar, 20).Value = oBE.Identificacion;
                        cmd.Parameters.Add("@TipoDocumento", SqlDbType.VarChar, 20).Value = oBE.TipoIDN;

                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    oGarantiaClienteBE = new GarantiaClienteBE();


                                    if (!reader.IsDBNull(0))
                                        oGarantiaClienteBE.Descripcion = reader.GetString(0);

                                    if (!reader.IsDBNull(1))
                                        oGarantiaClienteBE.NombreGarante = reader.GetString(1);

                                    if (!reader.IsDBNull(2))
                                        oGarantiaClienteBE.DNI = reader.GetString(2);

                                    if (!reader.IsDBNull(3))
                                        oGarantiaClienteBE.MontoGravamen = reader.GetString(3);

                                    if (!reader.IsDBNull(4))
                                        oGarantiaClienteBE.NroPartida = reader.GetString(4);

                                    if (!reader.IsDBNull(5))
                                        oGarantiaClienteBE.OficinaRegistral = reader.GetString(5);

                                    if (!reader.IsDBNull(6))
                                        oGarantiaClienteBE.NroPrestamo = reader.GetString(6);



                                    LstGarantiaClienteBE.Add(oGarantiaClienteBE);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw (ex);
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw (ex);
            }
            return await Task.Run(() => LstGarantiaClienteBE);
            
        }
    }
}
