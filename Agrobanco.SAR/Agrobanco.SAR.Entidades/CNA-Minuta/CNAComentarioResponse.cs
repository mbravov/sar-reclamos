﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades.CNA_Minuta
{
    public class CNAComentarioResponse
    {
        public Int64 CodComentario { get; set; }
        public Int64 CodCNAMinuta { get; set; }
        public string Descripcion { get; set; }
        public string UsuarioRegistro { get; set; }
        public DateTime? FechaRegistro { get; set; }
        public string LaserficheID { get; set; }
    }
}
