﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades.CNA_Minuta
{
    public class ConfiguracionDerivacionBE
    {
		public int IdConfiguracionAccion { get; set; }
		public int? GrupoFiltro { get; set; }
		public int TipoProcesamiento { get; set; }
		public int TipoEvento { get; set; }
		public string Perfil { get; set; }
		public int  IdEstado { get; set; }
		public EmisionBE emision { get; set; }
		public DerivacionBE derivacion { get; set; }
		public ComentarioEmisionBE comentarioEmision { get; set; }
		public int IdConfiguracionDerivacion { get; set; }
		public string Nombres { get; set; }
		public string TipoDocumento { get; set; }
		public string NroDocumento { get; set; }
		public int EsAsociadoGarantia { get; set; }
	}
}
