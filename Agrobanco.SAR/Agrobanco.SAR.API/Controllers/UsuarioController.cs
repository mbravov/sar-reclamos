﻿using Agrobanco.SAR.API.Models;
using Agrobanco.SAR.Control.Filters;
using Agrobanco.SAR.Entidades;
using Agrobanco.SAR.Entidades.UsuarioMantenimiento;
using Agrobanco.SAR.LogicaNegocio;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.SAR.API.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/Usuario")]
    [ApiController]

    public class UsuarioController : ControllerBase
    {
        private readonly UsuarioLogica usuarioLogica;
        public UsuarioController()
        {
            usuarioLogica = new UsuarioLogica();

        }

        [HttpPost("listar")]
        [ProducesResponseType(200, Type = typeof(List<Usuario>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult Usuarios([Required][FromHeader(Name = "Token")] string token, [FromBody] Usuario oUsuario)
        {
            try
            {
                if (oUsuario.Perfil == "OCM")
                {
                    oUsuario.Perfil = null;
                    oUsuario.UsuarioWeb = null;
                }
                //if (oUsuario.Perfil == "ACM")
                //{
                //    oUsuario.Perfil = oUsuario.UsuarioWeb;
                //}

             
                var listaResultado = usuarioLogica.ObtenerListaUsuarios(oUsuario);

                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost("listarusuariosmantenimiento")]
        [ProducesResponseType(200, Type = typeof(List<Usuario>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult UsuariosObtener([Required][FromHeader(Name = "Token")] string token, [FromBody] UsuarioMantenimientoFiltroResquest oUsuario)
        {
            try
            {
                if (oUsuario.Perfil == "1")
                {
                    oUsuario.Perfil = "OCM";
                }
                if (oUsuario.Perfil == "2")
                {
                    oUsuario.Perfil = "ACM";
                }
                if (oUsuario.Perfil == "3")
                {
                    oUsuario.Perfil = "GAR";
                }
                if (oUsuario.Perfil == "4")
                {
                    oUsuario.Perfil = "LEG";
                }
                if (oUsuario.Perfil == "5")
                {
                    oUsuario.Perfil = "ADMAG";
                }
                if (oUsuario.Perfil == "6")
                {
                    oUsuario.Perfil = "UDA";
                }

                var listaResultado = usuarioLogica.ObtenerListaUsuariosMantenimiento(oUsuario);

                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost("registrarusuario")]
        [ProducesResponseType(200, Type = typeof(List<Usuario>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult UsuarioRegistrar([Required][FromHeader(Name = "Token")] string token, [FromBody] UsuarioMantenimientoResponse oUsuario)
        {
            try
            {
                if (oUsuario.Perfil == "1")
                {
                    oUsuario.Perfil = "OCM";

                }
                if (oUsuario.Perfil == "2")
                {
                    oUsuario.Perfil = "ACM";
                }

                if (oUsuario.Perfil == "3")
                {
                    oUsuario.Perfil = "GAR";

                }
                if (oUsuario.Perfil == "4")
                {
                    oUsuario.Perfil = "LEG";
                }

                if (oUsuario.Perfil == "5")
                {
                    oUsuario.Perfil = "ADMAG";

                }
                if (oUsuario.Perfil == "6")
                {
                    oUsuario.Perfil = "UDA";
                }

                var listaResultado = usuarioLogica.UsuarioRegistrar(oUsuario);

                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost("usuariodetalle")]
        [ProducesResponseType(200, Type = typeof(List<Usuario>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult UsuarioDetalle([Required][FromHeader(Name = "Token")] string token, [FromBody] UsuarioMantenimientoResponse oUsuario)
        {
            try
            {
               
                var listaResultado = usuarioLogica.UsuarioDetalle(oUsuario);

                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost("actualizarusuario")]
        [ProducesResponseType(200, Type = typeof(List<Usuario>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult UsuarioActualizar([Required][FromHeader(Name = "Token")] string token, [FromBody] UsuarioMantenimientoResponse oUsuario)
        {
            try
            {
                if (oUsuario.CodPerfil == 1)
                {
                    oUsuario.Perfil = "OCM";

                }
                if (oUsuario.CodPerfil == 2)
                {
                    oUsuario.Perfil = "ACM";
                }
                if (oUsuario.CodPerfil == 3)
                {
                    oUsuario.Perfil = "GAR";
                }
                if (oUsuario.CodPerfil == 4)
                {
                    oUsuario.Perfil = "LEG";
                }
                if (oUsuario.CodPerfil == 5)
                {
                    oUsuario.Perfil = "ADMAG";
                }
                if (oUsuario.CodPerfil == 6)
                {
                    oUsuario.Perfil = "UDA";
                }
                var listaResultado = usuarioLogica.UsuarioActualizar(oUsuario);

                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
    }
}
