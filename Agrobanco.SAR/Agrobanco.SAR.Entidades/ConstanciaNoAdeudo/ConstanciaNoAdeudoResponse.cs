﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades.ConstanciaNoAdeudo
{
    public class ConstanciaNoAdeudoResponse
    {
        public string NombresCompletos { get; set; }
        public List<CreditosClienteBE> CreditosClienteBE { get; set; }
        public List<RequerimientoBE> RequerimientoBE { get; set; }
    }
}
