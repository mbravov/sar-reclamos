﻿using Agrobanco.SAR.API.Models;
using Agrobanco.SAR.Control.Filters;
using Agrobanco.SAR.Entidades;
using Agrobanco.SAR.LogicaNegocio;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.SAR.API.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/plantilla")]
    [ApiController]
    public class PlantillaController : ControllerBase
    {
        private readonly PlantillaLogica plantillaLogica;
        public PlantillaController()
        {
            plantillaLogica = new PlantillaLogica();

        }

        [HttpPost("listar")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult PlantillaObtener([Required][FromHeader(Name = "Token")] string token, [FromBody] PlantillaBE oPlantillaBE)
        {
            try
            {
                var Resultado = plantillaLogica.PlantillaObtener(oPlantillaBE);
                return StatusCode(200, Resultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost("desactivar")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult PlantillaDesactivar([Required][FromHeader(Name = "Token")] string token, [FromBody] PlantillaBE oPlantillaBE)
        {
            try
            {
                var Resultado = plantillaLogica.Desactivar(oPlantillaBE);
                return StatusCode(200, Resultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
