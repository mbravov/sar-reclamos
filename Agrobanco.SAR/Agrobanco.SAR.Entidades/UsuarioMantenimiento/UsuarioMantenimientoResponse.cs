﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades.UsuarioMantenimiento
{
    public class UsuarioMantenimientoResponse
    {
        public int CodUsuario { get; set; }
        public string Perfil { get; set; }
        public int CodPerfil { get; set; }
        public string DescripcionPerfil { get; set; }
        public string CorreoElectronico { get; set; }
        public string UsuarioWeb { get; set; }
        public string NombresApellidos { get; set; }
        public string Estado { get; set; }
        public string UsuarioRegistro { get; set; }
        public string FechaRegistro { get; set; }
        public int ValorRegistro { get; set; }
        public string CodAgencia { get; set; }
        
    }
}
