﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades
{
    public class Parametro
    {
        public int CodParametro { get; set; }
        public int Parametro_ { get; set; }
        public int Grupo { get; set; }
        public string strGrupo { get; set; }
        
        public string Descripcion { get; set; }

    }   

}
