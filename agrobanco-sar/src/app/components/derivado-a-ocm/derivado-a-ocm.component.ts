import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { RequerimientoService } from '../../services/requerimiento.service';
import { forkJoin } from 'rxjs';
import { DialogService } from '../../services/comun/dialog.service';
import { SecurityService } from '../../services/security.service';
import { Constantes } from 'src/app/comun/constantes';
import { Router } from '@angular/router';

@Component({
  selector: 'app-derivado-a-ocm',
  templateUrl: './derivado-a-ocm.component.html',
  styleUrls: ['./derivado-a-ocm.component.css']
})
export class DerivadoAOcmComponent implements OnInit {
  @Input() RequerimientoDetalle!: any;

  @Output() eventDerivadoOCM = new EventEmitter();

  @ViewChild('CartaRespuesta', { static: false })  InputVar!: ElementRef;
  @ViewChild('CartaRespuestaInput', { static: false }) InputCartaRespuesta!: ElementRef;


  recursos!: any[];
  //cartaRespuesta!: any[];
  LaserFicheID!: string;

  LaserFicheIDCartaRespuesta!: string;

  MAXIMO_TAMANIO_BYTES = 1024000;

  Archivo:any;
  NombreArchivo:any;

  bCartaRespuesta:boolean = false;
  loading:boolean = false;
  constructor(
    private securityService:SecurityService,
    private dialogService: DialogService,
    private requerimientoServices : RequerimientoService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loadPage();
  }
  reset()
  {
    this.InputVar.nativeElement.value = "";


  }
  /* Metodos */
  loadPage(): void {
    var CodTipoPlantilla = '';
    if (this.RequerimientoDetalle.codTipoRequerimiento == Constantes.Solicitud)
      CodTipoPlantilla = Constantes.PlantillaConstanciaNoAdeudo+","+Constantes.PlantillaConstanciaDeudaVigente;
    else
      CodTipoPlantilla = ""+Constantes.PlantillaCartaRespuesta;

    var oRecursoBE = {
      "CodTabla" : Constantes.Plantilla,
      "CodTipoPlantilla" :  CodTipoPlantilla
      }



      this.loading = true;
      forkJoin([
        this.requerimientoServices.ObtenerPlantillas(oRecursoBE)
      ]).subscribe( (data: any) => {

          console.log("ObtenerRecursos",data);
          this.recursos = data[0].response;

          console.log(this.recursos);


          if(this.RequerimientoDetalle.laserFicheIDCartaRespuesta!=null){
            this.bCartaRespuesta = true;
            this.LaserFicheIDCartaRespuesta = this.RequerimientoDetalle.laserFicheIDCartaRespuesta;
          }

          this.loading = false;
      }, (err) => {
        this.loading = false;
        console.error(err);
      });
  }
  Descargar(base64String:any, fileName:any) {
    const blobData = this.convertBase64ToBlobData(base64String.response);
    const blob = new Blob([blobData], { type: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' });
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement('a');
        link.href = url;
        link.download = fileName;
        link.click();

  }
  convertBase64ToBlobData(base64Data: string, contentType: string='image/png', sliceSize=512) {
    const byteCharacters = atob(base64Data);
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }


/* Funciones */

/* Evento Change */
changePlantilla(event:any) : void {
  const file = event.target.files[0];
  this.bCartaRespuesta = false;
  if(file==null || file.length <=0){
    this.Archivo = null;
    this.NombreArchivo = null;
    this.InputVar.nativeElement.value = "";
    this.InputCartaRespuesta.nativeElement.value = "";
   // this.reset();
   return;
 }

   var allowedExtensions = /(.doc|.docx)$/i;
   if(!allowedExtensions.exec(file.name)){
    this.dialogService.openMsgErrorDialog("Formato no válido");
    this.Archivo = null;
    this.NombreArchivo = null;
    this.InputVar.nativeElement.value = "";
    this.InputCartaRespuesta.nativeElement.value = "";
    //this.reset();
    return;
   }


  if (file.size > this.MAXIMO_TAMANIO_BYTES) {
    const tamanioEnMb = this.MAXIMO_TAMANIO_BYTES / 1000000;
    this.Archivo = null;
    this.NombreArchivo = null;
   // this.reset();
   this.InputVar.nativeElement.value = "";
   this.InputCartaRespuesta.nativeElement.value = "";
    this.dialogService.openMsgErrorDialog(`El tamaño máximo es 1 MB`);
    return;
  } else {
    // Validación pasada. Envía el formulario o haz lo que tengas que hacer
  }

  const FileNamme = file.name;
  this.NombreArchivo = FileNamme;

  const reader = new FileReader();
  console.log("file",file);
  reader.readAsDataURL(file);
  reader.onload = () => {
    console.log("reader",reader);
    this.Archivo = reader.result || null;
    console.log(reader.result);
  };
}

/* Evento Click */
DescargarPlantilla(): void
{
  if(this.LaserFicheID==null){
    this.dialogService.openMsgErrorDialog("Seleccione una plantilla");
    return;
  }

  if(this.LaserFicheID==""){
    this.dialogService.openMsgErrorDialog("Seleccione una plantilla");
    return;
  }

  let SaldoCredito;
  let NroCredito;
  let EstadoCredito;

  if(this.RequerimientoDetalle.creditosClientes!=null){
    if(this.RequerimientoDetalle.creditosClientes.length>0){
      for (let index = 0; index < this.RequerimientoDetalle.creditosClientes.length; index++) {
        const element = this.RequerimientoDetalle.creditosClientes[index];

        SaldoCredito = element.saldo;
        NroCredito = element.codCred;
        EstadoCredito = element.estado;
        break;

      }
    }
  }
  var oBE = {
    "LaserficheID": this.LaserFicheID,
    "NroReclamo":  this.RequerimientoDetalle.nroRequerimiento,
    "SaldoCredito": SaldoCredito,
    "NroCredito": NroCredito,
    "EstadoCredito":  EstadoCredito
  };
  this.loading = true;
  this.requerimientoServices.GenerarDocumento(oBE).subscribe(result=>{
    this.loading = false;
    const FechaActual = new Date();
    const AnioActual = FechaActual.getFullYear();
    const MesActual = (FechaActual.getMonth()<=9? '0'+ (FechaActual.getMonth()+1): FechaActual.getMonth() + 1); //FechaActual.getMonth() + 1;
    const DiaActual = (FechaActual.getDate()<=9? '0'+FechaActual.getDate(): FechaActual.getDate());
    const HoraActual = (FechaActual.getHours()<=9? '0'+FechaActual.getHours(): FechaActual.getHours()); //FechaActual.getHours();
    const MinutoActual = (FechaActual.getMinutes()<=9? '0'+FechaActual.getMinutes(): FechaActual.getMinutes()); //FechaActual.getMinutes();
    const SegundosActual = (FechaActual.getSeconds()<=9? '0'+FechaActual.getSeconds(): FechaActual.getSeconds()); //FechaActual.getMinutes(); //FechaActual.getSeconds();

    const NombreArchivo = this.RequerimientoDetalle.nroRequerimiento + "_" + AnioActual + MesActual + DiaActual + "T" + HoraActual + MinutoActual + SegundosActual;
    if(result==""){
      this.dialogService.openMsgErrorDialog("Hubo un problema al intentar generar el documento");
    }
    else{
      this.Descargar(result, NombreArchivo);
    }

  },
    (err)=>{
      console.log(err);
    }
  );
}

SubirCartaRespuesta()  {

  if(this.Archivo==null || this.Archivo == ''){
    this.dialogService.openMsgErrorDialog("Selecciona la carta de respuesta a subir");
    return;
  }
  this.dialogService
      .openConfirmDialog('¿Está seguro que desea subir este documento?')
      .afterClosed()
      .subscribe((res) => {
        if (res) {
          const oBE = {
            "LaserficheID": this.LaserFicheID,
            "NroReclamo": this.RequerimientoDetalle.nroRequerimiento,
            "Archivo": this.Archivo,
            "NombreArchivo": this.NombreArchivo,
            "CodRequerimiento": +this.RequerimientoDetalle.codRequerimiento,
            "UsuarioRegistro": this.securityService.leerUsuarioWeb(),
            "CodTabla": Constantes.CartaRespuesta
            };
            this.loading = true;
            this.requerimientoServices.SubirArchivo(oBE).subscribe(result=>{
              this.loading = false;
              debugger;
              if(result.response.resultadoOK){
                this.bCartaRespuesta = true;
                this.LaserFicheIDCartaRespuesta = result.response.laserficheID;
                this.dialogService.openMsgSuccessDialog("Se subió correctamente").afterClosed().subscribe(res=>{
                  if(res){

                    this.eventDerivadoOCM.emit(false);
                  }
                });
              }
              else{
                this.bCartaRespuesta = false;
                this.dialogService.openMsgErrorDialog("Se presento un problema para subir la carta de resuesta").afterClosed().subscribe(res=>{
                  if(res){
                    this.eventDerivadoOCM.emit(true);
                  }
                });
              }

            },
            (err)=>{
              this.loading = false;
              console.log(err);
            }
            );

        }
   });

}

   EliminarCartaRespuesta() : void {

    this.dialogService
      .openConfirmDialog('¿Está seguro que desea eliminar la carta de respuesta?')
      .afterClosed()
      .subscribe((res) => {
        if (res) {
          const oBE = {
            "LaserficheID": ""+this.LaserFicheIDCartaRespuesta
            };
            this.loading = true;
            this.requerimientoServices.eliminarArchivo(oBE).subscribe(result=>{
              this.loading = false;

              this.Archivo = "";
              this.Archivo = null;

              console.log("eliminarArchivo",result);
              if(result.response.resultado==1){
                this.bCartaRespuesta = false;
                this.dialogService.openMsgSuccessDialog("Se eliminó correctamente").afterClosed().subscribe(res=>{
                  if(res){

                    this.eventDerivadoOCM.emit(true);
                  }
                });
              }
              else{
                this.bCartaRespuesta = false;
                this.dialogService.openMsgErrorDialog("Se presento un problema para eliminar la carta de resuesta").afterClosed().subscribe(res=>{
                  if(res){
                    this.eventDerivadoOCM.emit(false);
                  }
                });
              }

            },
            (err)=>{
              this.loading = false;
              console.log(err);
            }
            );

        }
   });

   }

   DescargarCartaRespuesta(): void {
    debugger;
    this.loading = true;

    const FechaActual = new Date();
    const AnioActual = FechaActual.getFullYear();
    const MesActual = (FechaActual.getMonth()<=9? '0'+ (FechaActual.getMonth()+1): FechaActual.getMonth() + 1); //FechaActual.getMonth() + 1;
    const DiaActual = (FechaActual.getDate()<=9? '0'+FechaActual.getDate(): FechaActual.getDate());
    const HoraActual = (FechaActual.getHours()<=9? '0'+FechaActual.getHours(): FechaActual.getHours()); //FechaActual.getHours();
    const MinutoActual = (FechaActual.getMinutes()<=9? '0'+FechaActual.getMinutes(): FechaActual.getMinutes()); //FechaActual.getMinutes();
    const SegundosActual = (FechaActual.getSeconds()<=9? '0'+FechaActual.getSeconds(): FechaActual.getSeconds()); //FechaActual.getMinutes(); //FechaActual.getSeconds();

    const NombreArchivo = this.RequerimientoDetalle.nroRequerimiento + "_" + AnioActual + MesActual + DiaActual + "T" + HoraActual + MinutoActual + SegundosActual;


    const params = {
      CodTabla: Constantes.CartaRespuesta,
      ReferenciaID: +this.RequerimientoDetalle.codRequerimiento,
      LaserficheID: this.LaserFicheIDCartaRespuesta
    };



    this.requerimientoServices.RecursoObtener(params).subscribe((result) => {
      console.log('response', result.response);
      ///this.ocultarSubirCartaRespuesta = true;
      if (result.response.length > 0) {
        for (let index = 0; index < result.response.length; index++) {
          const element = result.response[index];

          const blobData = this.convertBase64ToBlobData(
            element.archivoBytes,
            element.contentType
          );
          const blob = new Blob([blobData], { type: element.contentType });
          const url = window.URL.createObjectURL(blob);
          // window.open(url);
          const link = document.createElement('a');
          link.href = url;
          link.download =  NombreArchivo; //element.nombreDocumento;
          link.click();
        }
      }
      this.loading = false;
    });
  }

  reload():void {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this.router.navigate([currentUrl]);
        console.log(currentUrl);
    });
  }
}




