﻿using Agrobanco.SAR.API.Models;
using Agrobanco.SAR.Control.Filters;
using Agrobanco.SAR.Entidades.CNA_Minuta;
using Agrobanco.SAR.LogicaNegocio;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.SAR.API.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/configuracion-accion")]
    public class ConfiguracionAccionController : ControllerBase
    {
        private readonly ConfiguracionAccionLogica _configuracionAccionLogica;
        public ConfiguracionAccionController()
        {
            _configuracionAccionLogica = new ConfiguracionAccionLogica();

        }

        [HttpPost("obtener")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public async Task<IActionResult> Obtener([Required][FromHeader(Name = "Token")] string token, [FromBody] ConfiguracionAccionBE request)
        {
            ResponseErrorDetalle oResponseErrorDetalle = new ResponseErrorDetalle();
            try
            {
                var resultado = await _configuracionAccionLogica.Obtener(request); ;
                return StatusCode(200, resultado);
            }
            catch (Exception ex)
            {
                oResponseErrorDetalle.codigo = string.Empty;
                oResponseErrorDetalle.resultado = false;
                oResponseErrorDetalle.mensaje = ex.Message;

                return StatusCode(100, oResponseErrorDetalle);
            }
        }

    }
}
