import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AppService } from '../app.service';
import { SecurityService } from './security.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FiltroAccionService  extends AppService {

  constructor(_securityService : SecurityService) {
    super(_securityService);
  }

  obtener<T>():Observable<any>{
    this.endpoint = `${environment.UrlBase_SARAPI}/filtro-accion/obtener`;
    return this.post(this.endpoint);
  }
}
