﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SAR.Comun.Laserfiche
{
    public class LaserficheProxy
    {
        //BaseControl oBaseControl = null;

        public LaserficheProxy()
        {
         //   oBaseControl = new BaseControl();
        }

        // new BaseControl().ObtenerValorParametro("SQLApplicationNameEnableEncrip")
        public string ConsultaArchivoSimple(Documento document)
        {
            string resultado = string.Empty;
            decimal peso;
            return resultado;
        }
        public String ConsultarDocumento(Int32 codigoLaser)
        {

            DocumentoRequest documento = new DocumentoRequest();
            documento.CodigoLaserfiche = codigoLaser;

            //int ldDocumentId = -1;
            Documento docRespuesta = new Documento();
            ServiceResponse<Documento> servicioResponse = new ServiceResponse<Documento>();

            servicioResponse.Data = docRespuesta;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(ApplicationKeys.APILaserfiche);

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = new HttpResponseMessage();
                StringContent sc = new StringContent(JsonConvert.SerializeObject(documento), Encoding.UTF8, "application/json");

                response = client.PostAsync("api/file/get", sc).Result;

                response.EnsureSuccessStatusCode();

                if (response.IsSuccessStatusCode)
                {
                    var message = response.Content.ReadAsStringAsync();
                    servicioResponse = JsonConvert.DeserializeObject<ServiceResponse<Documento>>(message.Result);

                    docRespuesta = servicioResponse.Data;
                }

            }

            return docRespuesta.ArchivoBytes;
        }
        public List<Documento> ConsultarDocumentoLista(string codigoLaserAnidado)
        {

            DocumentoRequest documento = new DocumentoRequest();
            documento.CodigoLaserficheAnidados = codigoLaserAnidado;

            //int ldDocumentId = -1;
            List<Documento> docRespuesta = new List<Documento>();
            ServiceResponse<Documento> servicioResponse = new ServiceResponse<Documento>();

            servicioResponse.DataList = docRespuesta;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(ApplicationKeys.APILaserfiche);

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = new HttpResponseMessage();

                StringContent sc = new StringContent(JsonConvert.SerializeObject(documento), Encoding.UTF8, "application/json");

                response = client.PostAsync("api/file/getList", sc).Result;

                response.EnsureSuccessStatusCode();

                if (response.IsSuccessStatusCode)
                {
                    var message = response.Content.ReadAsStringAsync();
                    servicioResponse = JsonConvert.DeserializeObject<ServiceResponse<Documento>>(message.Result);

                    docRespuesta = servicioResponse.DataList;
                }

            }

            return docRespuesta;
        }

        public Documento ConsultarImagenLista(int codigoLaserAnidado)
        {

            DocumentoRequest documento = new DocumentoRequest();
            documento.CodigoLaserfiche = Convert.ToInt32(codigoLaserAnidado);

            //int ldDocumentId = -1;
            Documento docRespuesta = new Documento();
            ServiceResponse<Documento> servicioResponse = new ServiceResponse<Documento>();

            servicioResponse.Data = docRespuesta;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(ApplicationKeys.APILaserfiche);

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = new HttpResponseMessage();

                StringContent sc = new StringContent(JsonConvert.SerializeObject(documento), Encoding.UTF8, "application/json");

                response = client.PostAsync("api/file/get", sc).Result;

                response.EnsureSuccessStatusCode();

                if (response.IsSuccessStatusCode)
                {
                    var message = response.Content.ReadAsStringAsync();
                    servicioResponse = JsonConvert.DeserializeObject<ServiceResponse<Documento>>(message.Result);

                    docRespuesta = servicioResponse.Data;
                }

            }

            return docRespuesta;
        }
        public Documento UploadDocumento(Documento document)
        {
            //int ldDocumentId = -1;
            Documento docRespuesta = new Documento();
            ServiceResponse<Documento> servicioResponse = new ServiceResponse<Documento>();

            servicioResponse.Data = docRespuesta;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(ApplicationKeys.APILaserfiche);

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = new HttpResponseMessage();
                StringContent sc = new StringContent(JsonConvert.SerializeObject(document), Encoding.UTF8, "application/json");
                // var json = JsonConvert.SerializeObject(document)


                response = client.PostAsync("api/file/save", sc).Result;

                response.EnsureSuccessStatusCode();

                if (response.IsSuccessStatusCode)
                {
                    var message = response.Content.ReadAsStringAsync();
                    servicioResponse = JsonConvert.DeserializeObject<ServiceResponse<Documento>>(message.Result);

                    docRespuesta = servicioResponse.Data;
                }

            }

            return docRespuesta;
        }

        public ImagenesModel UploadListaDocumento(ImagenesModel document)
        {
            //int ldDocumentId = -1;
            ImagenesModel docRespuesta = new ImagenesModel();
            ServiceResponse<ImagenesModel> servicioResponse = new ServiceResponse<ImagenesModel>();

            servicioResponse.Data = docRespuesta;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(ApplicationKeys.APILaserfiche);

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = new HttpResponseMessage();
                StringContent sc = new StringContent(JsonConvert.SerializeObject(document), Encoding.UTF8, "application/json");

                response = client.PostAsync("api/file/savelist", sc).Result;

                response.EnsureSuccessStatusCode();

                if (response.IsSuccessStatusCode)
                {
                    var message = response.Content.ReadAsStringAsync();
                    servicioResponse = JsonConvert.DeserializeObject<ServiceResponse<ImagenesModel>>(message.Result);

                    docRespuesta = servicioResponse.Data;
                }

            }

            return docRespuesta;
        }

        
        public DocumentoModel UploadListaDocumento(DocumentoModel document)
        {
            //int ldDocumentId = -1;
            DocumentoModel docRespuesta = new DocumentoModel();
            ServiceResponse<DocumentoModel> servicioResponse = new ServiceResponse<DocumentoModel>();

            servicioResponse.Data = docRespuesta;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(ApplicationKeys.APILaserfiche);

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = new HttpResponseMessage();
                // var serializer = new JavaScriptSerializer() { MaxJsonLength = 86753090 };
               // var serializer = new JavaScriptSerializer { MaxJsonLength = Int32.MaxValue, RecursionLimit = 100 };
                 
                StringContent sc = new StringContent(JsonConvert.SerializeObject(document), Encoding.UTF8, "application/json");

                response = client.PostAsync("api/file/savelistdocumentos", sc).Result;

                response.EnsureSuccessStatusCode();

                if (response.IsSuccessStatusCode)
                {
                    var message = response.Content.ReadAsStringAsync();
                    servicioResponse = JsonConvert.DeserializeObject<ServiceResponse<DocumentoModel>>(message.Result);

                    docRespuesta = servicioResponse.Data;
                }

            }

            return docRespuesta;
        }

        public ServiceResponse<Documento> RemoveFile(Documento document)
        {
            //int ldDocumentId = -1;
           
            ServiceResponse<Documento> servicioResponse = new ServiceResponse<Documento>();

             

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(ApplicationKeys.APILaserfiche);

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = new HttpResponseMessage();
                StringContent sc = new StringContent(JsonConvert.SerializeObject(document), Encoding.UTF8, "application/json");
                // var json = JsonConvert.SerializeObject(document)


                response = client.PostAsync("api/file/removeFile", sc).Result;

                response.EnsureSuccessStatusCode();

                if (response.IsSuccessStatusCode)
                {
                    var message = response.Content.ReadAsStringAsync();
                    servicioResponse = JsonConvert.DeserializeObject<ServiceResponse<Documento>>(message.Result);

                    //docRespuesta = servicioResponse.Data;
                    //docRespuesta.ResultadoOK = (servicioResponse.Resultado>0? true: false);
                }

            }

            return servicioResponse;
        }

        public async Task<List<Documento>> ConsultarDocumentoListaAsync(string codigoLaserAnidado)
        {

            DocumentoRequest documento = new DocumentoRequest();
            documento.CodigoLaserficheAnidados = codigoLaserAnidado;

            //int ldDocumentId = -1;
            List<Documento> docRespuesta = new List<Documento>();
            ServiceResponse<Documento> servicioResponse = new ServiceResponse<Documento>();

            servicioResponse.DataList = docRespuesta;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(ApplicationKeys.APILaserfiche);

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = new HttpResponseMessage();

                StringContent sc = new StringContent(JsonConvert.SerializeObject(documento), Encoding.UTF8, "application/json");

                response = await client.PostAsync("api/file/getList", sc);

                response.EnsureSuccessStatusCode();

                if (response.IsSuccessStatusCode)
                {
                    var message =  response.Content.ReadAsStringAsync();
                    servicioResponse = JsonConvert.DeserializeObject<ServiceResponse<Documento>>(message.Result);
                    
                    docRespuesta =  servicioResponse.DataList;
                }

            }

            return docRespuesta;
        }

    }
}
