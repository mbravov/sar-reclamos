import { Injectable } from '@angular/core';
import { AppService } from '../app.service';
import { SecurityService } from './security.service';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConstanciaNoAdeudoService extends AppService {

  constructor(securityService : SecurityService) {
    super(securityService);
  }

  ObtenerConstanciaNoAdeudo<T>(datos:any):Observable<any>{
    this.endpoint = `${environment.UrlBase_SARAPI}/constancia-no-adeudo/listar`;
    return this.post(this.endpoint,datos);
  }
}
