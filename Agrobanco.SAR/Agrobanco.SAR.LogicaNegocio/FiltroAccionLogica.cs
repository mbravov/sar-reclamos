﻿using Agrobanco.SAR.Datos;
using Agrobanco.SAR.Entidades.CNA_Minuta;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SAR.LogicaNegocio
{
    public class FiltroAccionLogica
    {
        private readonly FiltroAccionDatos _filtroAccionDatos;
        public FiltroAccionLogica()
        {
            _filtroAccionDatos = new FiltroAccionDatos();
        }

        public async Task<List<FiltroAccionBE>> Obtener()
        {
            return await _filtroAccionDatos.Obtener();
        }
    }
}
