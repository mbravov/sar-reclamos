import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { SeguimientoService } from 'src/app/services/seguimiento.service';

@Component({
  selector: 'app-requerimiento-seguimiento-detalle',
  templateUrl: './requerimiento-seguimiento-detalle.component.html',
  styleUrls: ['./requerimiento-seguimiento-detalle.component.css']
})
export class RequerimientoSeguimientoDetalleComponent implements OnInit {

  isLinear = false;
  firstFormGroup!: FormGroup;
  secondFormGroup!: FormGroup;

  NroReclamo!:string;
  requerimientoSeguimiento!:any[];


  alternate: boolean = false;
  toggle: boolean = false;
  size: number = 40;
  expandEnabled: boolean = false;
  side = 'right';

  diasTranscurridos:number = 0;
  constructor(
    private route: ActivatedRoute,
    private seguimientoService:SeguimientoService
  ) {
    this.NroReclamo = this.route.snapshot.params.id;
   }

  ngOnInit(): void {
    this.obtenerRequerimientoSeguimientoDetalle();


  }

  obtenerRequerimientoSeguimientoDetalle():void {
    var oBE = {
      NroReclamo:this.NroReclamo
    };
    this.seguimientoService.ObtenerSeguimientoDetalle(oBE).subscribe(result=>{
      debugger;

      if(result.response){
       this.requerimientoSeguimiento =result.response;


       for (let index = 0; index <  this.requerimientoSeguimiento.length; index++) {
         const element =  this.requerimientoSeguimiento[index];
         if(element.diasTranscurrido!=null){
           this.diasTranscurridos = this.diasTranscurridos + element.diasTranscurrido;
         }
       }

       console.log("this.requerimientoSeguimiento",this.requerimientoSeguimiento);
      }
    },(err)=>{
      console.log(err);
    });
  }

  onHeaderClick(event:any) {
      event.stopPropagation();
    }

}
