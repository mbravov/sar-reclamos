﻿using Agrobanco.SAR.Datos.SqlServer.Acceso;
using Agrobanco.SAR.Entidades;
using Agrobanco.SAR.Entidades.CNA_Minuta;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SAR.Datos
{
    public class CNADatos
    {
        public string cadenaConexion;
        public CNADatos() { }
        public List<CNAFilterResponse> Listar(CNAFilterRequest oRequest)
        {//RequestRequerimiento oBE
            
                this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
                CNAFilterResponse objRequerimiento = null;
                List<CNAFilterResponse> lstCnaMinuta = new List<CNAFilterResponse>();

                try
                {
                    using (SqlConnection conn = new SqlConnection(cadenaConexion))
                    {
                        conn.Open();
                        using (SqlCommand cmd = new SqlCommand("[dbo].[SP_CNA_MINUTA_LISTAR]", conn))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            if (string.IsNullOrEmpty(oRequest.Perfil))
                                cmd.Parameters.Add("@Perfil", SqlDbType.VarChar, 200).Value = DBNull.Value;
                            else
                                cmd.Parameters.Add("@Perfil", SqlDbType.VarChar, 200).Value = oRequest.Perfil;

                            cmd.Parameters.Add("@CodEstado", SqlDbType.VarChar).Value = (oRequest.CodEstado == null ? "" : oRequest.CodEstado);
                            cmd.Parameters.Add("@CodAgencia", SqlDbType.VarChar).Value = oRequest.CodAgencia;
                            cmd.Parameters.Add("@NroDocumento", SqlDbType.VarChar).Value = (oRequest.NroDocumento == null ? "" : oRequest.NroDocumento);                            
                            cmd.Parameters.Add("@FechaCancelacionIni", SqlDbType.VarChar).Value = oRequest.FechaCancelacionIni == null ? string.Empty : oRequest.FechaCancelacionIni;
                            cmd.Parameters.Add("@FechaCancelacionFin", SqlDbType.VarChar).Value = oRequest.FechaCancelacionFin == null ? string.Empty : oRequest.FechaCancelacionFin;
                            cmd.Parameters.Add("@UsuarioWeb", SqlDbType.VarChar, 10).Value = oRequest.UsuarioWeb;

                        try
                            {
                                using (SqlDataReader reader = cmd.ExecuteReader())
                                {

                                    while (reader.Read())
                                    {
                                        objRequerimiento = new CNAFilterResponse();
                                        if (!Convert.IsDBNull(reader["FechaCancelacion"])) objRequerimiento.FechaCancelacion = reader.GetDateTime("FechaCancelacion");
                                        if (!Convert.IsDBNull(reader["NombreApellidos"])) objRequerimiento.NombresApellidos = Convert.ToString(reader["NombreApellidos"]);
                                        if (!Convert.IsDBNull(reader["NroDocumento"])) objRequerimiento.NroDocumento = Convert.ToString(reader["NroDocumento"]);
                                        if (!Convert.IsDBNull(reader["Banco"])) objRequerimiento.Banco = Convert.ToString(reader["Banco"]);                                        
                                        if (!Convert.IsDBNull(reader["Agencia"])) objRequerimiento.Agencia = Convert.ToString(reader["Agencia"]);
                                        if (!Convert.IsDBNull(reader["AnalistaProponente"])) objRequerimiento.AnalistaProponente = Convert.ToString(reader["AnalistaProponente"]);                                    
                                        if (!Convert.IsDBNull(reader["Estado"])) objRequerimiento.Estado = Convert.ToString(reader["Estado"]);
                                        if (!Convert.IsDBNull(reader["DerivadoA"])) objRequerimiento.DerivadoA = Convert.ToString(reader["DerivadoA"]);
                                        if (!Convert.IsDBNull(reader["FechaDerivacion"])) objRequerimiento.FechaDerivacion = reader.GetDateTime("FechaDerivacion");
                                        if (!Convert.IsDBNull(reader["procesamiento"])) objRequerimiento.procesamiento = Convert.ToString(reader["procesamiento"]);
                                        if (!Convert.IsDBNull(reader["procesamientoDesc"])) objRequerimiento.procesamientoDesc = Convert.ToString(reader["procesamientoDesc"]);
                                        if (!Convert.IsDBNull(reader["IdEmision"])) objRequerimiento.IdEmision = Convert.ToInt32(reader["IdEmision"]);

                                        lstCnaMinuta.Add(objRequerimiento);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                conn.Close();
                                conn.Dispose();
                                cmd.Dispose();
                                //Generando Archivo Log
                                // new LogWriter(ex.Message); caycho
                                throw ex;
                            }
                            finally
                            {
                                conn.Close();
                                conn.Dispose();
                                cmd.Dispose();

                            }
                        }
                    }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return lstCnaMinuta;
        }
        public List<EstadosCNAMinBE> ListarEstados(CNAFilterRequest oRequest)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            EstadosCNAMinBE objEstado = null;
            List<EstadosCNAMinBE> lstEstado = new List<EstadosCNAMinBE>();
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_CNA_ESTADOS_LISTAR]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        if (string.IsNullOrEmpty(oRequest.Perfil))
                            cmd.Parameters.Add("@Perfil", SqlDbType.VarChar, 200).Value = DBNull.Value;
                        else
                            cmd.Parameters.Add("@Perfil", SqlDbType.VarChar, 200).Value = oRequest.Perfil;
                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    objEstado = new EstadosCNAMinBE();
                                    if (!Convert.IsDBNull(reader["CodEstado"])) objEstado.CodEstado = Convert.ToInt32(reader["CodEstado"]);
                                    if (!Convert.IsDBNull(reader["Descripcion"])) objEstado.Descripcion = Convert.ToString(reader["Descripcion"]);

                                    lstEstado.Add(objEstado);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            throw (ex);
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw (ex);
            }

            return lstEstado;
        }
        public List<CNAFilterResponse> ListarSeguimiento(CNAFilterRequest oRequest)
        {//RequestRequerimiento oBE

            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            CNAFilterResponse objRequerimiento = null;
            List<CNAFilterResponse> lstCnaMinuta = new List<CNAFilterResponse>();

            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_CNA_GENERAL_LISTAR]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        if (string.IsNullOrEmpty(oRequest.Perfil))
                            cmd.Parameters.Add("@Perfil", SqlDbType.VarChar, 200).Value = DBNull.Value;
                        else
                            cmd.Parameters.Add("@Perfil", SqlDbType.VarChar, 200).Value = oRequest.Perfil;

                        cmd.Parameters.Add("@CodEstado", SqlDbType.VarChar).Value = (oRequest.CodEstado == null ? "" : oRequest.CodEstado);
                        cmd.Parameters.Add("@CodAgencia", SqlDbType.VarChar).Value = oRequest.CodAgencia;
                        cmd.Parameters.Add("@NroDocumento", SqlDbType.VarChar).Value = (oRequest.NroDocumento == null ? "" : oRequest.NroDocumento);
                        cmd.Parameters.Add("@FechaCancelacionIni", SqlDbType.VarChar).Value = oRequest.FechaCancelacionIni == null ? string.Empty : oRequest.FechaCancelacionIni;
                        cmd.Parameters.Add("@FechaCancelacionFin", SqlDbType.VarChar).Value = oRequest.FechaCancelacionFin == null ? string.Empty : oRequest.FechaCancelacionFin;


                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    objRequerimiento = new CNAFilterResponse();
                                    if (!Convert.IsDBNull(reader["FechaCancelacion"])) objRequerimiento.FechaCancelacion = reader.GetDateTime("FechaCancelacion");
                                    if (!Convert.IsDBNull(reader["NombreApellidos"])) objRequerimiento.NombresApellidos = Convert.ToString(reader["NombreApellidos"]);
                                    if (!Convert.IsDBNull(reader["NroDocumento"])) objRequerimiento.NroDocumento = Convert.ToString(reader["NroDocumento"]);
                                    if (!Convert.IsDBNull(reader["Banco"])) objRequerimiento.Banco = Convert.ToString(reader["Banco"]);
                                    if (!Convert.IsDBNull(reader["Agencia"])) objRequerimiento.Agencia = Convert.ToString(reader["Agencia"]);
                                    if (!Convert.IsDBNull(reader["AnalistaProponente"])) objRequerimiento.AnalistaProponente = Convert.ToString(reader["AnalistaProponente"]);
                                    if (!Convert.IsDBNull(reader["Estado"])) objRequerimiento.Estado = Convert.ToString(reader["Estado"]);
                                    if (!Convert.IsDBNull(reader["DerivadoA"])) objRequerimiento.DerivadoA = Convert.ToString(reader["DerivadoA"]);
                                    if (!Convert.IsDBNull(reader["FechaDerivacion"])) objRequerimiento.FechaDerivacion = reader.GetDateTime("FechaDerivacion");
                                    if (!Convert.IsDBNull(reader["procesamiento"])) objRequerimiento.procesamiento = Convert.ToString(reader["procesamiento"]);
                                    if (!Convert.IsDBNull(reader["procesamientoDesc"])) objRequerimiento.procesamientoDesc = Convert.ToString(reader["procesamientoDesc"]);

                                    lstCnaMinuta.Add(objRequerimiento);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw ex;
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return lstCnaMinuta;
        }
        public List<EstadosCNAMinBE> ListarEstadosGeneral()
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            EstadosCNAMinBE objEstado = null;
            List<EstadosCNAMinBE> lstEstado = new List<EstadosCNAMinBE>();
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_CNA_ESTADOS_GENERAL_LISTAR]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                       
                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    objEstado = new EstadosCNAMinBE();
                                    if (!Convert.IsDBNull(reader["CodEstado"])) objEstado.CodEstado = Convert.ToInt32(reader["CodEstado"]);
                                    if (!Convert.IsDBNull(reader["Descripcion"])) objEstado.Descripcion = Convert.ToString(reader["Descripcion"]);

                                    lstEstado.Add(objEstado);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            throw (ex);
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw (ex);
            }

            return lstEstado;
        }
        public List<AgenciaBE> ListarAgenciaCNA(CNAFilterRequest oRequest)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            AgenciaBE objAgencia = null;
            List<AgenciaBE> lstAgencia = new List<AgenciaBE>();
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_CNA_AGENCIA_LISTAR]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@UsuarioWeb", SqlDbType.VarChar, 10).Value = string.IsNullOrEmpty(oRequest.UsuarioWeb) ? (object)DBNull.Value : oRequest.UsuarioWeb;
                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    objAgencia = new AgenciaBE();
                                    if (!Convert.IsDBNull(reader["CodAgencia"])) objAgencia.CodAgencia = Convert.ToInt32(reader["CodAgencia"]);
                                    if (!Convert.IsDBNull(reader["Oficina"])) objAgencia.Oficina = Convert.ToString(reader["Oficina"]);                                    
                                    lstAgencia.Add(objAgencia);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            throw (ex);
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw (ex);
            }

            return lstAgencia;
        }


        public async Task<CNADetalleResponse> ObtenerPorNumeroDocumento(CNADetalleRequest request)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            CNADetalleResponse oCNADetalleResponse = null;
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("SP_CREDITO_CLIENTE_OBTENER_DETALLE", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@IdEmision", SqlDbType.Int).Value = request.IdEmision;
                        cmd.Parameters.Add("@Perfil", SqlDbType.VarChar,5).Value = request.Perfil;
                        try
                        {
                            using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                            {

                                while (reader.Read())
                                {
                                    oCNADetalleResponse = new CNADetalleResponse();
                                    if (!reader.IsDBNull(0))
                                        oCNADetalleResponse.TipoProcesamiento = reader.GetString(0);

                                    if (!reader.IsDBNull(1))
                                        oCNADetalleResponse.Estado = reader.GetString(1);

                                    if (!reader.IsDBNull(2))
                                        oCNADetalleResponse.TipoDocumento = reader.GetString(2);

                                    if (!reader.IsDBNull(3))
                                        oCNADetalleResponse.NumeroDocumento = reader.GetString(3);

                                    if (!reader.IsDBNull(4))
                                        oCNADetalleResponse.Nombres = reader.GetString(4);

                                    if (!reader.IsDBNull(5))
                                        oCNADetalleResponse.Agencia = reader.GetString(5);

                                    if (!reader.IsDBNull(6))
                                        oCNADetalleResponse.AnalistaProponente = reader.GetString(6);


                                    if (!reader.IsDBNull(7))
                                        oCNADetalleResponse.IdEmision = reader.GetInt32(7);

                                    if (!reader.IsDBNull(8))
                                        oCNADetalleResponse.IdDerivacion = reader.GetInt32(8);

                                    if (!reader.IsDBNull(9))
                                        oCNADetalleResponse.IdDerivacionMovimiento = reader.GetInt32(9);

                                    if (!reader.IsDBNull(10))
                                        oCNADetalleResponse.CodAgencia = reader.GetInt32(10);

                                    if (!reader.IsDBNull(11))
                                        oCNADetalleResponse.IdEstado = reader.GetInt32(11);

                                    if (!reader.IsDBNull(12))
                                        oCNADetalleResponse.Perfil = reader.GetString(12);

                                    if (!reader.IsDBNull(13))
                                        oCNADetalleResponse.IdCreditoCliente = reader.GetInt32(13);

                                    if (!reader.IsDBNull(14))
                                        oCNADetalleResponse.IdLaserficheCNA = reader.GetInt32(14);

                                    if (!reader.IsDBNull(15))
                                        oCNADetalleResponse.IdLaserficheCR = reader.GetInt32(15);

                                    if (!reader.IsDBNull(16))
                                        oCNADetalleResponse.PerfilDerivado = reader.GetString(16);

                                    if (!reader.IsDBNull(17))
                                        oCNADetalleResponse.FechaEntregaOlva = reader.GetDateTime(17);

                                    if (!reader.IsDBNull(18))
                                        oCNADetalleResponse.CodAgenciaDerivacion = reader.GetInt32(18);

                                    if (!reader.IsDBNull(19))
                                        oCNADetalleResponse.IdLaserficheCE = reader.GetInt32(19);

                                    if (!reader.IsDBNull(20))
                                        oCNADetalleResponse.FechaRegistroCE = reader.GetDateTime(20);  

                                    if (!reader.IsDBNull(21))
                                        oCNADetalleResponse.Direccion = reader.GetString(21);

                                    if (!reader.IsDBNull(22))
                                        oCNADetalleResponse.DesMedioNot = reader.GetString(22);

                                    if (!reader.IsDBNull(23))
                                        oCNADetalleResponse.CodMedioNot = reader.GetInt32(23);

                                    if (!reader.IsDBNull(24))
                                        oCNADetalleResponse.ValorMedioNot = reader.GetString(24);

                                    if (!reader.IsDBNull(25))
                                        oCNADetalleResponse.NroCredito = reader.GetString(25);

                                    if (!reader.IsDBNull(26))
                                        oCNADetalleResponse.MotivoRechazo = reader.GetString(26);
                                    
                                    if (!reader.IsDBNull(27))
                                        oCNADetalleResponse.FechaRechazo = reader.GetString(27);

                                    RecursoEmisionBE oRecursoEmisionBE = new RecursoEmisionBE();
                                    RecursoEmisionDatos oRecursoEmisionDatos = new RecursoEmisionDatos();
                                    oRecursoEmisionBE.IdEmision = request.IdEmision;
                                    oCNADetalleResponse.RecursoEmision = await oRecursoEmisionDatos.Obtener(oRecursoEmisionBE);

                                    CreditoClienteCNADatos oCreditoClienteDatos = new CreditoClienteCNADatos();
                                    CreditoClienteCNABE oCreditoClienteCNABE = new CreditoClienteCNABE();
                                    oCreditoClienteCNABE.TipoIDN = oCNADetalleResponse.TipoDocumento;
                                    oCreditoClienteCNABE.Identificacion = oCNADetalleResponse.NumeroDocumento;
                                    oCNADetalleResponse.Creditos = await oCreditoClienteDatos.ObtenerCreditosCNA(oCreditoClienteCNABE);

                                    oCreditoClienteDatos = new CreditoClienteCNADatos();
                                    CreditosClienteBE oCreditosClienteBE = new CreditosClienteBE();
                                    oCreditosClienteBE.TipoIDN = oCNADetalleResponse.TipoDocumento;
                                    oCreditosClienteBE.Identificacion = oCNADetalleResponse.NumeroDocumento;
                                    oCNADetalleResponse.Garantias = await oCreditoClienteDatos.ObtenerGarantias(oCreditosClienteBE);

                                    ComentarioEmisionDatos oComentarioEmisionDatos = new ComentarioEmisionDatos();
                                    ComentarioEmisionBE oComentarioEmisionBE = new ComentarioEmisionBE();
                                    oComentarioEmisionBE.IdEmision = request.IdEmision;
                                    oCNADetalleResponse.Comentarios = await  oComentarioEmisionDatos.Obtener(oComentarioEmisionBE);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw ex;
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return await Task.Run(() => oCNADetalleResponse);
            //return oCNADetalleResponse;
        }
         
        
        
          
                   
        
        
    }
}
