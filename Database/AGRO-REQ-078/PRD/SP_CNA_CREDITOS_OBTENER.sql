USE DBAgrSAR
GO

DROP PROCEDURE IF EXISTS SP_CNA_CREDITOS_OBTENER
GO
--=================================================================================================================================
-- FECHA DE CREACI�N		: 15/12/2021
-- FECHA DE MODIFICACI�N	:
-- CREADO POR				: CECILIA MARRUFO
-- STORE PROCEDURE			: SP_CNA_CREDITOS_OBTENER
-- PARAMETOS				: @NumeroDocumento VARCHAR(20),
--							  @TipoDocumento VARCHAR(20) 
-- FUNCIONALIDAD			:  Obtiene lista de cr�ditos de Clientes Agrobanco
--================================================================================================================================

	CREATE PROC SP_CNA_CREDITOS_OBTENER 

	 @NumeroDocumento VARCHAR(20) ,
	 @TipoDocumento VARCHAR(20) 


	 AS

	 BEGIN
	   SELECT CAST(DEAACC AS varchar) NumeroPrestamo, 
		   CASE WHEN SUBSTRING(CAST(DEAGLN AS varchar(38)), 1, 4)='1411' THEN  'VIGENTE'
						  WHEN SUBSTRING(CAST(DEAGLN AS varchar(38)), 1, 4)='1421' THEN  'VIGENTE' 
						  WHEN SUBSTRING(CAST(DEAGLN AS varchar(38)), 1, 4)='1413' THEN  'REESTRUCTURADO'
						  WHEN SUBSTRING(CAST(DEAGLN AS varchar(38)), 1, 4)='1423' THEN  'REESTRUCTURADO'
						  WHEN SUBSTRING(CAST(DEAGLN AS varchar(38)), 1, 4)='1414' THEN  'REFINANCIADO'
						  WHEN SUBSTRING(CAST(DEAGLN AS varchar(38)), 1, 4)='1424' THEN  'REFINANCIADO'
						  WHEN SUBSTRING(CAST(DEAGLN AS varchar(38)), 1, 4)='1415' THEN  'VENCIDO'
						  WHEN SUBSTRING(CAST(DEAGLN AS varchar(38)), 1, 4)='1425' THEN  'VENCIDO'
						  WHEN SUBSTRING(CAST(DEAGLN AS varchar(38)), 1, 4)='1416' THEN  'JUDICIAL'
						  WHEN SUBSTRING(CAST(DEAGLN AS varchar(38)), 1, 4)='1426' THEN  'JUDICIAL'
						  WHEN SUBSTRING(CAST(DEAGLN AS varchar(38)), 1, 4)='8113' THEN  'CASTIGADO'
						  WHEN SUBSTRING(CAST(DEAGLN AS varchar(38)), 1, 4)='8123' THEN  'CASTIGADO'
						  WHEN SUBSTRING(CAST(DEAGLN AS varchar(38)), 1, 4)='8119' THEN  'CASTIGADO'
						  WHEN SUBSTRING(CAST(DEAGLN AS varchar(38)), 1, 4)='8129' THEN  'CASTIGADO'
						  ELSE ''
						  END 	UltimaSituacion,
		   CASE WHEN  DEASTS= 'C' THEN 'CANCELADO' ELSE DEASTS END EstadoActual,
		   IsNull(CAST(format((DEAMEP+DEAMEI+DEAMEM+OtrosCargos),'N')AS varchar),'') SaldoDeudor ,
	CAST(format(DEAMEP,'N')AS VARCHAR) Capital  ,CAST(DEAMEI AS varchar) InteresCompensatorio,
	CAST(DEAMEM AS varchar)InteresMoratorio, 
	IsNull(CAST(OtrosCargos AS varchar),'')OtrosCargos 
	FROM( 
			SELECT SUM(OTROS)OtrosCargos, CRED FROM(
			SELECT SUM((DLSAMT - DLSPAD))OTROS , DLSACC cred
			FROM PROPBD425.DBAgrIBS.DBO.dlsde GROUP BY 	DLSACC 
					 
			UNION ALL
			SELECT GHISG, GHACC FROM PROPBD425.DBAgrIBS.DBO.GHmst 
			)TJ GROUP BY TJ.cred
			
			)TH RIGHT join PROPBD425.DBAgrIBS.DBO.CDTM33/*DEALS*/ on CRED = DEAACC
				RIGHT join PROPBD425.DBAgrIBS.DBO.CUMST ON CUSCUN = DEACUN

				where CUSIDN=@NumeroDocumento AND CUSTID=@TipoDocumento
				GROUP BY DEAACC, DEASTS, DEAGLN, DEAMEP, DEAMEI, DEAMEM, TH.OtrosCargos


	 END

