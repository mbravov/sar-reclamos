﻿using Agrobanco.SAR.Comun;
using Agrobanco.SAR.Comun.Laserfiche;
using Agrobanco.SAR.Datos.SqlServer.Acceso;
using Agrobanco.SAR.Entidades;
using Agrobanco.SAR.Entidades.CNA_Minuta;
using Spire.Pdf.General.Paper.Font.Common.Environment;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Agrobanco.SAR.Datos
{
   public class ComentarioDatos
    {

        public string cadenaConexion;



        public ComentarioDatos()
        {

        }

        public bool ComentarioRegistrar(ComentarioBE oBE)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            int CodComentario = 0;

            using (SqlConnection cnn = new SqlConnection(cadenaConexion))
            {
                cnn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[SP_COMENTARIO_INSERTAR]", cnn))
                {
                    using (SqlTransaction tx = cnn.BeginTransaction())
                    {
                        try
                        {
                            cmd.Transaction = tx;

                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.Add("@CodRequerimiento", SqlDbType.Int).Value = oBE.CodRequerimiento;
                            cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = oBE.Estado;

                            if(oBE.CodMotivo!=null)
                                cmd.Parameters.Add("@CodMotivo", SqlDbType.Int).Value = oBE.CodMotivo;
                            else
                                cmd.Parameters.Add("@CodMotivo", SqlDbType.Int).Value = DBNull.Value;

                            if(oBE.CodArea!=null)
                                cmd.Parameters.Add("@CodArea", SqlDbType.Int).Value = oBE.CodArea;
                            else
                                cmd.Parameters.Add("@CodArea", SqlDbType.Int).Value = DBNull.Value;

                            cmd.Parameters.Add("@Descripcion", SqlDbType.VarChar, 300).Value = oBE.Descripcion;

                            if (oBE.FechaSolicitud == null)
                                cmd.Parameters.Add("@FechaSolicitud", SqlDbType.Date).Value = DBNull.Value;
                            else
                                cmd.Parameters.Add("@FechaSolicitud", SqlDbType.Date).Value =  oBE.FechaSolicitud;
                            
                            cmd.Parameters.Add("@UsuarioRegistro", SqlDbType.VarChar, 50).Value = oBE.UsuarioRegistro;

                            SqlParameter paramId = new SqlParameter("@CodComentario", SqlDbType.Int);
                            paramId.Direction = ParameterDirection.Output;
                            cmd.Parameters.Add(paramId);

                            int rowsAffected = cmd.ExecuteNonQuery();

                            if (rowsAffected > 0)
                            {
                                CodComentario = Convert.ToInt32(cmd.Parameters["@CodComentario"].Value);

                            }

                            LaserficheProxy laserfiche = new LaserficheProxy();
                            DocumentoModel DocumentoModelRequest = new DocumentoModel();
                            DocumentoModel DocumentoModelResponse = new DocumentoModel();

                            DocumentoModelRequest.Folder = oBE.NroRequerimiento;
                            List<Documento> lstDocumento = new List<Documento>();
                            int item = 1;
                            string base64 = string.Empty;
                            if (!string.IsNullOrEmpty(oBE.Archivo))
                            {
                                base64 = string.Empty;
                                base64 = oBE.Archivo.Split(',')[1];

                                DateTime fechaActual = DateTime.Now;
                                string Dia = fechaActual.Day.ToString().PadLeft(2,'0');
                                string Mes = fechaActual.Month.ToString().PadLeft(2, '0');
                                string Anio = fechaActual.Year.ToString().PadLeft(2, '0');

                                string Hora = fechaActual.Hour.ToString().PadLeft(2, '0');
                                string Minutos = fechaActual.Minute.ToString().PadLeft(2, '0');
                                string Segundos = fechaActual.Second.ToString().PadLeft(2, '0');

                                Documento oDocumento = new Documento();
                                oDocumento.Extension = System.IO.Path.GetExtension(oBE.NombreArchivo).Split('.')[1];
                                oDocumento.ArchivoBytes = base64;
                                oDocumento.Nombre = oBE.NroRequerimiento + "_COMENTARIO_" + Anio + Mes + Dia + "T" + Hora + Minutos + Segundos;
                                oDocumento.Folder = oBE.NroRequerimiento;
                                lstDocumento.Add(oDocumento);
                                item++;
                            }

                            if (lstDocumento.Count > 0)
                            {
                                DocumentoModelRequest.ListaDocumentos = lstDocumento;
                                DocumentoModelResponse = laserfiche.UploadListaDocumento(DocumentoModelRequest);

                                RecursoDatos oRecursoDatos = null;
                                RecursoBE oRecursoBE = null;
                                foreach (var image in DocumentoModelResponse.ListaDocumentos)
                                {
                                    if (image.ResultadoOK)
                                    {
                                        oRecursoDatos = new RecursoDatos();
                                        oRecursoBE = new RecursoBE();

                                        oRecursoBE.CodTabla = oBE.CodTabla;
                                        oRecursoBE.ReferenciaID = CodComentario;
                                        oRecursoBE.LaserficheID = image.CodigoLaserfiche;
                                        oRecursoBE.Estado = true;
                                        oRecursoBE.UsuarioRegistro = oBE.UsuarioRegistro;
                                        oRecursoBE.Descripcion = image.Nombre;
                                        Boolean resultad = oRecursoDatos.RegistrarTrans(oRecursoBE, cnn, tx);

                                        if (!resultad)
                                            throw new Exception("Se presento un error al intentar registrar los archivos adjuntos");

                                    }
                                    else
                                    {
                                        throw new Exception("Se presento un error al subir el archivo");
                                    }
                                }
                                DocumentoModelResponse.ResultadoOK = true;

                            }

                            tx.Commit();
                            return true;
                        }

                        catch (Exception ex)
                        {
                            tx.Rollback();
                            cnn.Close();
                            cnn.Dispose();
                            cmd.Dispose();
                            throw (ex);
                        }
                        finally
                        {
                            cnn.Close();
                            cnn.Dispose();
                            cmd.Dispose();
                        }
                    }

                }

            }

            return false;
        }

        public List<ComentarioBE> ComentarioObtener(ComentarioBE oBE)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            ComentarioBE oComentarioBE = null;
            List<ComentarioBE> LstComentarioBE = new List<ComentarioBE>();

            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_COMENTARIO_OBTENER]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        if(oBE.CodRequerimientoMovimiento==null)
                            cmd.Parameters.Add("@CodRequerimientoMovimiento", SqlDbType.BigInt).Value = DBNull.Value;
                        else
                            cmd.Parameters.Add("@CodRequerimientoMovimiento", SqlDbType.BigInt).Value = oBE.CodRequerimientoMovimiento;

                        if (oBE.CodRequerimiento == null)
                            cmd.Parameters.Add("@CodRequerimiento", SqlDbType.BigInt).Value = DBNull.Value;
                        else
                            cmd.Parameters.Add("@CodRequerimiento", SqlDbType.BigInt).Value = oBE.CodRequerimiento;

                        if (oBE.Estado == null)
                            cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = DBNull.Value;
                        else
                            cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = oBE.Estado;

                        if (oBE.CodTabla == null)
                            cmd.Parameters.Add("@CodTabla", SqlDbType.Int).Value = DBNull.Value;
                        else
                            cmd.Parameters.Add("@CodTabla", SqlDbType.Int).Value = oBE.CodTabla;

                  

                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    oComentarioBE = new ComentarioBE();

                                    if (!reader.IsDBNull(0))
                                        oComentarioBE.CodComentario = reader.GetInt64(0);

                                    if (!reader.IsDBNull(1))
                                        oComentarioBE.CodRequerimientoMovimiento = reader.GetInt64(1);
                                    
                                    if (!reader.IsDBNull(2))
                                        oComentarioBE.CodRequerimiento = reader.GetInt64(2);

                                    if (!reader.IsDBNull(3))
                                        oComentarioBE.CodMotivo = reader.GetInt32(3);

                                    if (!reader.IsDBNull(4))
                                        oComentarioBE.CodArea = reader.GetInt32(4);

                                    if (!reader.IsDBNull(5))
                                        oComentarioBE.Area = reader.GetString(5);

                                    if (!reader.IsDBNull(6))
                                        oComentarioBE.Descripcion = reader.GetString(6);

                                    if (!reader.IsDBNull(7))
                                        oComentarioBE.FechaSolicitud = reader.GetDateTime(7);

                                    if (!reader.IsDBNull(8))
                                        oComentarioBE.UsuarioRegistro = reader.GetString(8);

                                    if (!reader.IsDBNull(9))
                                        oComentarioBE.FechaRegistro = reader.GetDateTime(9);

                                    if (!reader.IsDBNull(10))
                                        oComentarioBE.LaserficheID = reader.GetInt32(10).ToString();

                                    if (!reader.IsDBNull(11))
                                        oComentarioBE.CodTabla = reader.GetInt32(11);

                                    if (!reader.IsDBNull(14))
                                        oComentarioBE.NombresApellidos = reader.GetString(14);

                                    if (!reader.IsDBNull(15))
                                        oComentarioBE.Motivo = reader.GetString(15);

                                    LstComentarioBE.Add(oComentarioBE);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw new Exception("Error al Obtener el Detalle del Requerimiento");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception("Error al Obtener el Detalle dekl Requerimiento");
            }

            return LstComentarioBE;
        }

        public bool ComentarioRegistrarAnular(ComentarioBE oBE)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
        
            using (SqlConnection cnn = new SqlConnection(cadenaConexion))
            {
                cnn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[SP_COMENTARIO_INSERTAR_ANULAR]", cnn))
                {
                   // using (SqlTransaction tx = cnn.BeginTransaction())
                    //{
                        try
                        {
                            ///cmd.Transaction = tx;

                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("CodRequerimiento", SqlDbType.Int).Value = oBE.CodRequerimiento;
                            cmd.Parameters.Add("@CodRequerimientoMovimiento", SqlDbType.Int).Value = oBE.CodRequerimientoMovimiento;
                            if (oBE.CodMotivo != null)
                                cmd.Parameters.Add("@CodMotivo", SqlDbType.Int).Value = oBE.CodMotivo;
                            else
                                cmd.Parameters.Add("@CodMotivo", SqlDbType.Int).Value = DBNull.Value;

                            if (oBE.CodArea != null)
                                cmd.Parameters.Add("@CodArea", SqlDbType.Int).Value = oBE.CodArea;
                            else
                                cmd.Parameters.Add("@CodArea", SqlDbType.Int).Value = DBNull.Value;

                            cmd.Parameters.Add("@Descripcion", SqlDbType.VarChar, 300).Value = oBE.Descripcion;
                            if (oBE.FechaSolicitud == null)
                                cmd.Parameters.Add("@FechaSolicitud", SqlDbType.Date).Value = DBNull.Value;
                            else
                                cmd.Parameters.Add("@FechaSolicitud", SqlDbType.Date).Value = oBE.FechaSolicitud;

                            cmd.Parameters.Add("@UsuarioRegistro", SqlDbType.VarChar, 50).Value = oBE.UsuarioRegistro;

                            SqlParameter paramId = new SqlParameter("@CodComentario", SqlDbType.Int);
                            paramId.Direction = ParameterDirection.Output;
                            cmd.Parameters.Add(paramId);

                            cmd.ExecuteNonQuery();

                        return true;
                        }

                        catch (Exception ex)
                        {
                            //tx.Rollback();
                            cnn.Close();
                            cnn.Dispose();
                            cmd.Dispose();
                        }
                        finally
                        {
                            cnn.Close();
                            cnn.Dispose();
                            cmd.Dispose();
                        }
                    //}

                }

            }

            return false;
        }


        public List<CNAComentarioResponse> ComentarioCNAObtener()
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            CNAComentarioResponse oCNAComentarioResponse = null;
            List<CNAComentarioResponse> LstCNAComentarioResponse = new List<CNAComentarioResponse>();

            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("SP_COMENTARIO_OBTENER_CNA", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        //if (oBE.CodRequerimientoMovimiento == null)
                        //    cmd.Parameters.Add("@CodRequerimientoMovimiento", SqlDbType.BigInt).Value = DBNull.Value;
                        //else
                        //    cmd.Parameters.Add("@CodRequerimientoMovimiento", SqlDbType.BigInt).Value = oBE.CodRequerimientoMovimiento;

                        //if (oBE.CodRequerimiento == null)
                        //    cmd.Parameters.Add("@CodRequerimiento", SqlDbType.BigInt).Value = DBNull.Value;
                        //else
                        //    cmd.Parameters.Add("@CodRequerimiento", SqlDbType.BigInt).Value = oBE.CodRequerimiento;

                        //if (oBE.Estado == null)
                        //    cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = DBNull.Value;
                        //else
                        //    cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = oBE.Estado;

                        //if (oBE.CodTabla == null)
                        //    cmd.Parameters.Add("@CodTabla", SqlDbType.Int).Value = DBNull.Value;
                        //else
                        //    cmd.Parameters.Add("@CodTabla", SqlDbType.Int).Value = oBE.CodTabla;



                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    oCNAComentarioResponse = new CNAComentarioResponse();

                                    if (!reader.IsDBNull(0))
                                        oCNAComentarioResponse.CodComentario = reader.GetInt64(0);

                                    if (!reader.IsDBNull(1))
                                        oCNAComentarioResponse.CodCNAMinuta = reader.GetInt64(1);

                                    
                                    if (!reader.IsDBNull(2))
                                        oCNAComentarioResponse.Descripcion = reader.GetString(2);

                           

                                    if (!reader.IsDBNull(3))
                                        oCNAComentarioResponse.UsuarioRegistro = reader.GetString(3);

                                    if (!reader.IsDBNull(4))
                                        oCNAComentarioResponse.FechaRegistro = reader.GetDateTime(4);

                                    if (!reader.IsDBNull(5))
                                        oCNAComentarioResponse.LaserficheID = reader.GetInt32(5).ToString();

                                    

                                    LstCNAComentarioResponse.Add(oCNAComentarioResponse);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw ex;
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return LstCNAComentarioResponse;
        }
    }
}
