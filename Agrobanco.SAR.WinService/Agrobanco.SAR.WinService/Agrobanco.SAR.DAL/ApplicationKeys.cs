﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SAR.DAL
{
    public static class ApplicationKeys
    {
        public static string APILaserfiche = ConfigurationManager.AppSettings["APILaserfiche"];
        public static string SQLApplicationNameEnableEncrip => ConfigurationManager.AppSettings["config:SQLApplicationNameEnableEncrip"];
        public static string SQLRegeditFolder => ConfigurationManager.AppSettings["SQLRegeditFolder"];
        public static string RegeditPass => ConfigurationManager.AppSettings["config:Regeditkey"];

        public static string idlaserfichePlantilla = ConfigurationManager.AppSettings["idlaserfichePlantilla"];

    }
}
