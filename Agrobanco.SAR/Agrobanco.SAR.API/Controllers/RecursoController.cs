﻿using Agrobanco.SAR.API.Models;
using Agrobanco.SAR.Comun.Laserfiche;
using Agrobanco.SAR.Control.Filters;
using Agrobanco.SAR.Entidades;
using Agrobanco.SAR.LogicaNegocio;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.SAR.API.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/recurso")]
    [ApiController]
    public class RecursoController : ControllerBase
    {
        private readonly RecursoLogica recursoLogica;
        public RecursoController()
        {
            recursoLogica = new RecursoLogica();

        }

        [HttpPost("listar")]
        [ProducesResponseType(200, Type = typeof(List<RecursoBE>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult Agencia([Required][FromHeader(Name = "Token")] string token, [FromBody] RecursoBE oBE)
        {
            try
            {
                var listaResultado = recursoLogica.Listar(oBE);
                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("recursoobtener")]
        //[HttpPost("parametro")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult RecursoObtener([Required][FromHeader(Name = "Token")] string token, [FromBody] RecursoBE oRecursoBE)
        {
            try
            {
                LaserficheProxy laserfiche = new LaserficheProxy();
                ResponseSuccess oResponseSuccess = new ResponseSuccess();
                List<Documento> lstDocumento = new List<Documento>();

                var Resultado = recursoLogica.RecursoObtener(oRecursoBE);
                string LaserficheID = string.Empty;

                if (Resultado != null)
                {
                    if (Resultado.Count > 0)
                    {
                        foreach (var item in Resultado)
                        {
                            LaserficheID += "|" + item.LaserficheID;
                        }
                        if (LaserficheID.Length > 0)
                        {
                            LaserficheID = LaserficheID.Substring(1, LaserficheID.Length - 1);
                        }
                        lstDocumento = laserfiche.ConsultarDocumentoLista(LaserficheID);
                    }
                }


                return StatusCode(200, lstDocumento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost("recursoeliminar")]
        //[HttpPost("parametro")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult RecursoEliminar([Required][FromHeader(Name = "Token")] string token, [FromBody] RecursoBE oRecursoBE)
        {
            try
            {
                ResponseSuccess oResponseSuccess = new ResponseSuccess();
                var Resultado = recursoLogica.RecursoEliminar(oRecursoBE);
                return StatusCode(200, Resultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
