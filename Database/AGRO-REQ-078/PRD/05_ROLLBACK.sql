USE DBAgrSAR
GO


IF COL_LENGTH('dbo.Usuario','CodAgencia') IS NOT NULL
BEGIN
	
	ALTER TABLE DBO.USUARIO
	DROP COLUMN CodAgencia

END
GO

IF COL_LENGTH('Agencia','FlagAgencia') IS NOT NULL
BEGIN

	DELETE FROM  Agencia WHERE FlagAgencia = 1
	
	ALTER TABLE DBO.AGENCIA
	DROP COLUMN FlagAgencia  

END
GO

IF OBJECT_ID('dbo.RecursoEmision', 'U') IS NOT NULL 
BEGIN
	DROP TABLE dbo.RecursoEmision;
END
GO

IF OBJECT_ID('dbo.EmisionMovimiento', 'U') IS NOT NULL 
BEGIN
	DROP TABLE EmisionMovimiento
END
GO

IF OBJECT_ID('dbo.GestionEmision', 'U') IS NOT NULL 
BEGIN
	DROP TABLE dbo.GestionEmision;
END
GO

IF OBJECT_ID('dbo.DerivacionMovimiento', 'U') IS NOT NULL 
BEGIN
	DROP TABLE dbo.DerivacionMovimiento;
END
GO

IF OBJECT_ID('dbo.Derivacion', 'U') IS NOT NULL 
BEGIN
	DROP TABLE dbo.Derivacion;
END
GO

IF OBJECT_ID('dbo.ComentarioEmision', 'U') IS NOT NULL 
BEGIN
	DROP TABLE dbo.ComentarioEmision;
END
GO

IF OBJECT_ID('dbo.Emision', 'U') IS NOT NULL 
BEGIN
	DROP TABLE dbo.Emision;
END
GO

IF OBJECT_ID('dbo.CreditoCliente', 'U') IS NOT NULL 
BEGIN
	DROP TABLE dbo.CreditoCliente;
END
GO

IF OBJECT_ID('dbo.ConfiguracionDerivacion', 'U') IS NOT NULL 
BEGIN
	DROP TABLE dbo.ConfiguracionDerivacion;
END
GO

IF OBJECT_ID('dbo.EstadoEmision', 'U') IS NOT NULL 
BEGIN
	DROP TABLE dbo.EstadoEmision;
END
GO

IF OBJECT_ID('dbo.ConfiguracionDerivacion', 'U') IS NOT NULL 
BEGIN
	DROP TABLE dbo.ConfiguracionDerivacion;
END
GO

IF OBJECT_ID('dbo.FiltroAccion', 'U') IS NOT NULL 
BEGIN
	DROP TABLE dbo.FiltroAccion;
END
GO

IF OBJECT_ID('dbo.ConfiguracionAccion', 'U') IS NOT NULL 
BEGIN
	DROP TABLE dbo.ConfiguracionAccion;
END
GO

IF OBJECT_ID('dbo.Accion', 'U') IS NOT NULL 
BEGIN
	DROP TABLE dbo.Accion;
END
GO


DELETE FROM Recurso where CodTabla in (8,9,10)
DELETE FROM dbo.Parametro WHERE Grupo = 1400 AND Parametro IN (3, 4, 5, 6)

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Parametro = 1500 OR Grupo = 1500)
BEGIN
	DELETE FROM dbo.Parametro WHERE Parametro = 1500 OR Grupo = 1500
END
GO

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Parametro = 1600 OR Grupo = 1600)
BEGIN
	DELETE FROM Parametro WHERE Parametro = 1600 OR Grupo = 1600
END
GO

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Parametro = 1700 OR Grupo = 1700)
BEGIN
	DELETE FROM Parametro WHERE Parametro = 1700 OR Grupo = 1700
END
GO

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Parametro = 1800 OR Grupo = 1800)
BEGIN
	DELETE FROM Parametro WHERE Parametro = 1800 OR Grupo = 1800
END
GO

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Parametro = 1900 OR Grupo = 1900)
BEGIN
	DELETE FROM Parametro WHERE Parametro = 1900 OR Grupo = 1900
END
GO

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Parametro = 2000 OR Grupo = 2000)
BEGIN
	DELETE FROM Parametro WHERE Parametro = 2000 OR Grupo = 2000
END
GO

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Parametro = 2100 OR Grupo = 2100)
BEGIN
	DELETE FROM Parametro WHERE Parametro = 2100 OR Grupo = 2100
END
GO

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Parametro = 2200 OR Grupo = 2200)
BEGIN
	DELETE FROM Parametro WHERE Parametro = 2200 OR Grupo = 2200
END
GO

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Parametro = 2300 OR Grupo = 2300)
BEGIN
	DELETE FROM Parametro WHERE Parametro = 2300 OR Grupo = 2300
END
GO

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Parametro = 2400 OR Grupo = 2400)
BEGIN
	DELETE FROM dbo.Parametro WHERE Parametro = 2400 OR Grupo = 2400
END
GO

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Grupo = 200 AND Parametro IN (8, 9, 10))
BEGIN
	DELETE FROM dbo.Parametro WHERE Grupo = 200 AND Parametro IN (8, 9, 10)
END
GO

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Parametro = 2500 OR Grupo = 2500)
BEGIN
	DELETE FROM dbo.Parametro WHERE Parametro = 2500 OR Grupo = 2500
END
GO

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Parametro in (4,5,6) AND Grupo = 1200)
BEGIN
	DELETE FROM dbo.Parametro WHERE Parametro in (4,5,6) AND Grupo = 1200
END
GO

IF EXISTS(SELECT * FROM dbo.Parametro WHERE Parametro in (13,14,15,16,17) AND Grupo = 1000)
BEGIN
	DELETE FROM dbo.Parametro WHERE Parametro in (13,14,15,16,17) AND Grupo = 1000
END
GO


DELETE Usuario WHERE Perfil IN ('GAR','LEG','ADMAG','UDA')

DELETE FROM Plantilla WHERE CodTipoPlantilla in (4,5,6)
DELETE FROM  Recurso WHERE CodTabla = 5 AND LaserficheID in (40587,40406,40761)


	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia=NULL WHERE CodAgencia='60'
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia=NULL WHERE CodAgencia='59'
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia=NULL WHERE CodAgencia='61'
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia=NULL WHERE CodAgencia='62'
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia=NULL WHERE CodAgencia='63'
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia=NULL WHERE CodAgencia='65'
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia=NULL WHERE CodAgencia='66'
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia=NULL WHERE CodAgencia='64'
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia=NULL WHERE CodAgencia='67'
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia=NULL WHERE CodAgencia='68'

	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia=NULL WHERE CodAgencia='35'
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia=NULL WHERE CodAgencia='3'
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia=NULL WHERE CodAgencia='11'
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia=NULL WHERE CodAgencia='16'
 	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia=NULL WHERE CodAgencia='51'
 	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia=NULL WHERE CodAgencia='8'
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia=NULL WHERE CodAgencia='15'
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia=NULL WHERE CodAgencia='17'
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia=NULL WHERE CodAgencia='30'
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia=NULL WHERE CodAgencia='27'
	UPDATE [DBAgrSAR].[dbo].[Agencia] SET CodAgenciaReferencia=NULL WHERE CodAgencia='39'



	DROP PROCEDURE IF EXISTS dbo.SP_CNA_ESTADOS_GENERAL_LISTAR
	DROP PROCEDURE IF EXISTS dbo.SP_CNA_ESTADOS_LISTAR
	DROP PROCEDURE IF EXISTS dbo.SP_CNA_MINUTA_LISTAR
	DROP PROCEDURE IF EXISTS dbo.SP_CNA_AGENCIA_LISTAR
	DROP PROCEDURE IF EXISTS dbo.SP_CNA_GENERAL_LISTAR 
	DROP PROCEDURE IF EXISTS SP_COMENTARIO_EMISION_OBTENER
	DROP PROCEDURE IF EXISTS SP_COMENTARIO_EMISION_INSERTAR
	DROP PROCEDURE IF EXISTS SP_CREDITO_CLIENTE_OBTENER_DETALLE
 
	DROP PROCEDURE IF EXISTS SP_CONFIGURACION_ACCION_GENERAR
	DROP PROCEDURE IF EXISTS SP_CONFIGURACION_DERIVACION_OBTENER
	DROP PROCEDURE IF EXISTS SP_DERIVACION_INSERTAR
	DROP PROCEDURE IF EXISTS SP_DERIVACION_ACTUALIZAR
	DROP PROCEDURE IF EXISTS SP_EMISION_INSERTAR
	DROP PROCEDURE IF EXISTS SP_EMISION_ACTUALIZAR
	DROP PROCEDURE IF EXISTS SP_FILTRO_ACCION_OBTENER
	DROP PROCEDURE IF EXISTS SP_OBTENER_CONFIGURACION_ACCION
	DROP PROCEDURE IF EXISTS SP_LISTAR_CONFIGURACION_DERIVACION
	DROP PROCEDURE IF EXISTS SP_LISTAR_FILTRO_ACCION
	DROP PROCEDURE IF EXISTS SP_LISTAR_FILTRO_ACCION
	DROP PROCEDURE IF EXISTS SP_EMISION_INSERTAR_LISTA
	DROP PROCEDURE IF EXISTS SP_RECURSOEMISION_INSERT_LISTA
	DROP PROCEDURE IF EXISTS SP_DERIVACION_INSERTAR_LISTA
	DROP PROCEDURE IF EXISTS SP_EJECUTAR_PROCESAMIENTO_AUTOMATICO
	DROP PROCEDURE IF EXISTS SP_RECURSO_EMISION_OBTENER
	DROP PROCEDURE IF EXISTS SP_RECURSO_EMISION_INSERTAR
 
	DROP PROCEDURE IF EXISTS SP_COMENTARIO_EMISION_INSERTAR_ACCION
	DROP PROCEDURE IF EXISTS [dbo].[SP_CREEDITO_CLIENTE_OBTENER_DERIVACION_MASIVA_ADMAG]
	DROP PROCEDURE IF EXISTS [dbo].[SP_CREDITO_CLIENTE_LISTAR_GENERACION_CNA]
	DROP PROCEDURE IF EXISTS SP_RECURSO_EMISION_ACTUALIZAR_TIPO_RECURSO
	DROP PROCEDURE IF EXISTS SP_CREDITO_CLIENTE_ACTUALIZAR_IDLASERFICHE
 

	IF TYPE_ID('UT_Emision') IS NOT NULL
	BEGIN
		DROP TYPE UT_Emision
	END

	IF TYPE_ID('UT_Derivacion') IS NOT NULL
	BEGIN
		DROP TYPE UT_Derivacion
	END
	GO

	IF TYPE_ID('UT_RecursoEmision') IS NOT NULL
	BEGIN
		DROP TYPE UT_RecursoEmision
	END

	GO

	
ALTER PROCEDURE dbo.SP_USUARIO_ACTUALIZAR
@CodUsuario INT,
@Perfil VARCHAR(5) ,
@DescripcionPerfil VARCHAR(200),
@CorreoElectronico VARCHAR(100),
@UsuarioWeb  VARCHAR(50),
@NombresApellidos VARCHAR(200),
@UsuarioRegistro VARCHAR(50),
@Estado INT,
@MensajeOutput VARCHAR(100) OUTPUT

AS
BEGIN
	 DECLARE @Mensaje VARCHAR(100)= ''		
	 DECLARE @ExistePerfilOCM INT = 0
	 DECLARE @ExistePerfilACM INT = 0
	 DECLARE @PerfilOCM VARCHAR(5)= 'OCM'
	 DECLARE @PerfilACM VARCHAR(5)= 'ACM'
	 DECLARE @EstadoActual INT = 0
	 DECLARE @ExisteUsuarioWeb INT = 0

	SET @ExisteUsuarioWeb = (SELECT COUNT(UsuarioWeb) FROM Usuario WHERE UsuarioWeb = @UsuarioWeb AND CodUsuario <> @CodUsuario )


	 SET @EstadoActual = (SELECT Estado FROM Usuario WHERE Perfil = @Perfil AND CodUsuario=@CodUsuario)	 
	 	 
	 IF @PerfilOCM = @Perfil
	 BEGIN
		SET @ExistePerfilOCM = (SELECT COUNT(Perfil) FROM Usuario WHERE Perfil = @Perfil AND Estado = 1)		
	 END
	 
	  IF @PerfilACM = @Perfil
	 BEGIN
		SET @ExistePerfilACM = (SELECT COUNT(Perfil) FROM Usuario WHERE Perfil = @Perfil AND CodUsuario=@CodUsuario )		
	 END	


	 IF @ExistePerfilOCM > 0 
	 BEGIN
		SET @Mensaje = 'Ya existe un usuario con perfil OCM en estado Activo'
	 END

	  IF @EstadoActual = 0 AND @ExistePerfilOCM = 1
	 BEGIN
		SET @Mensaje = 'Ya existe un usuario con perfil OCM en estado Activo'
	 END 

	 IF @ExisteUsuarioWeb > 0
	 BEGIN
		SET @Mensaje = 'El usuario web ya existe'
	 END

	 IF( @ExisteUsuarioWeb = 0 )
	 BEGIN
	 IF (@ExistePerfilOCM = 1 AND @EstadoActual = 1 ) OR @ExistePerfilACM = 1
	 BEGIN
		 
		 UPDATE Usuario
			SET Perfil = @Perfil
				,DescripcionPerfil = @DescripcionPerfil
				,CorreoElectronico = @CorreoElectronico
				,UsuarioWeb = @UsuarioWeb
				,NombresApellidos = @NombresApellidos
				,UsuarioModificacion = @UsuarioRegistro
				,FechaModificacion = GETDATE()
				,Estado = @Estado
		 WHERE CodUsuario = @CodUsuario				
		
		 SET @Mensaje = 'Se actualiz� correctamente el usuario.'
	 END 
	 END
	  
   SET @MensajeOutput = @Mensaje  	

END

GO

ALTER PROCEDURE dbo.SP_USUARIO_OBTENER_DETALLE
@CodUsuario INT
AS
BEGIN

	SELECT US.CodUsuario
		  ,PRP.Parametro  AS CodPerfil	
		  ,US.DescripcionPerfil
		  ,US.CorreoElectronico
		  ,US.UsuarioWeb
		  ,US.NombresApellidos		  
		  ,(CASE WHEN US.Estado = 1 THEN '1' ELSE '0' END) AS Estado
		  ,US.UsuarioRegistro		 
	FROM   Usuario US	
	LEFT JOIN Parametro PRP ON PRP.Descripcion = US.Perfil AND PRP.Grupo = 1400
	WHERE	(ISNULL(@CodUsuario,'') = '' OR US.CodUsuario = @CodUsuario) 
END
GO

ALTER PROCEDURE dbo.SP_USUARIO_REGISTRAR
@Perfil VARCHAR(5) ,
@DescripcionPerfil VARCHAR(200),
@CorreoElectronico VARCHAR(100),
@UsuarioWeb  VARCHAR(50),
@NombresApellidos VARCHAR(200),
@UsuarioRegistro VARCHAR(50),
@MensajeOutput VARCHAR(100) OUTPUT

AS
BEGIN
	 DECLARE @Mensaje VARCHAR(100)= ''	
	 DECLARE @ExisteUsuarioWeb INT = 0
	 DECLARE @ExistePerfilOCM INT = 0
	 DECLARE @PerfilOCM VARCHAR(5)= 'OCM'
	 DECLARE @UsuarioActivo INT = 0

	 SET @ExisteUsuarioWeb = (SELECT COUNT(UsuarioWeb) FROM Usuario WHERE UsuarioWeb = @UsuarioWeb)

	 SET @UsuarioActivo = (SELECT COUNT(Estado) FROM Usuario WHERE Perfil = @Perfil)

	 IF @PerfilOCM = @Perfil
	 BEGIN
		SET @ExistePerfilOCM = (SELECT COUNT(Perfil) FROM Usuario WHERE Perfil = @Perfil AND Estado = 1)
	 END

	 IF @ExistePerfilOCM > 0
	 BEGIN
		SET @Mensaje = 'Ya existe un usuario con perfil OCM en estado Activo'
	 END
	 
	 IF @ExisteUsuarioWeb > 0
	 BEGIN
		SET @Mensaje = 'El usuario web ya existe'
	 END

	
	 IF @ExistePerfilOCM = 0 AND @ExisteUsuarioWeb = 0 OR @PerfilOCM = 'ACM'
	 BEGIN
		
		INSERT INTO Usuario(Perfil
			   ,DescripcionPerfil
			   ,CorreoElectronico        
			   ,UsuarioWeb
			   ,NombresApellidos
			   ,Estado
			   ,UsuarioRegistro         
			   ,FechaRegistro
			  )
		 VALUES (@Perfil
			   ,@DescripcionPerfil
			   ,@CorreoElectronico         
			   ,@UsuarioWeb
			   ,@NombresApellidos
			   ,1       
			   ,@UsuarioRegistro
			   , GETDATE())

		 SET @Mensaje = 'Se registr� correctamente el usuario.'
	 END 		 
	  

   SET @MensajeOutput = @Mensaje  	

END
GO

ALTER PROCEDURE [dbo].[SP_USUARIO_OBTENER]	
@Perfil VARCHAR(5),
@UsuarioWeb  VARCHAR(20),
@Estado BIT = NULL
AS
BEGIN

	SELECT US.CodUsuario
		,US.UsuarioWeb
		,US.NombresApellidos
		,US.Perfil
		,US.CorreoElectronico
		,US.Estado
	FROM Usuario US
	WHERE  ((@Perfil IS NULL OR @Perfil = '') OR (@Perfil IS NOT NULL AND US.Perfil = @Perfil))
		AND ((@UsuarioWeb IS NULL OR @UsuarioWeb = '') OR (@UsuarioWeb IS NOT NULL AND US.UsuarioWeb = @UsuarioWeb))
		AND ((@Estado IS NULL) OR (@Estado IS NOT NULL AND US.Estado = @Estado))
	ORDER BY US.CodUsuario

END
GO


 
ALTER PROCEDURE dbo.SP_AGENCIA_OBTENER	

AS
BEGIN

	SELECT CodAgencia
		,Oficina
		,Direccion
	FROM Agencia
	ORDER BY codAgencia

END
GO	