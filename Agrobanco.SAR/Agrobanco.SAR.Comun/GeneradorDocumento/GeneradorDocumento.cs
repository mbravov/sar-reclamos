﻿using Agrobanco.SAR.Comun.Laserfiche;
using Agrobanco.SAR.Entidades;
using Agrobanco.SAR.Entidades.CNA_Minuta;
using Spire.Doc;
using Spire.Doc.Documents;
using Spire.Doc.Fields;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SAR.Comun.GeneradorDocumento
{
    public class GeneradorDocumento
    {
        public string GenerarBeneficioLey30893ultimo(string LaserficheID)
        {
            LaserficheProxy laserfiche = new LaserficheProxy();
            List<Documento> lstDocumento = new List<Documento>();
            Documento oDocumento = new Documento();
            lstDocumento = laserfiche.ConsultarDocumentoLista(LaserficheID);

            if (lstDocumento != null)
            {
                if (lstDocumento.Count > 0)
                {
                    oDocumento = lstDocumento[0];
                    var ms = new MemoryStream(Convert.FromBase64String(oDocumento.ArchivoBytes));

                    Document document = new Document();
                    document.LoadFromStream(ms, FileFormat.Docx);
                    document.Replace("[X_FECHA_ACTUAL_X]", "27 de Julio de Cerro Azul", false, true);
                    document.Replace("[X_NOMBRES_APELLIDOS_X]", "Jose Javier Caycho Garcia", false, true);
                    document.Replace("[X_CORREO_X]", "Josecaycho_1@hotmail.com", false, true);
                    document.Replace("[X_DIRECCION_X]", "Av. Canada 1710 - San Borja", false, true);
                    document.Replace("[X_NRO_REQ_X]", "R-0001-2021", false, true);
                    document.Replace("[X_NRO_CREDITO_X]", "002-284601597-24415", false, true);

                    Section section = document.Sections[0];
                    //Paragraph paragraph = section.Paragraphs.Count > 0 ? section.Paragraphs[0] : section.AddParagraph();
                    //    paragraph.AppendText("The sample demonstrates how to insert an image into a document.");
                    //   paragraph.ApplyStyle(BuiltinStyle.Heading2);
                    Paragraph paragraph = section.AddParagraph();
                    Bitmap p = new Bitmap(Image.FromFile(ApplicationKeys.rutaPlantillaCorreosProd+"pngwing.com.png"));
                    p.RotateFlip(RotateFlipType.Rotate90FlipX);
                    DocPicture picture = document.Sections[0].Paragraphs[0].AppendPicture(p);
                    //picture.HorizontalPosition = 50.0F;
                    //picture.VerticalPosition = 60.0F;
                    picture.Width = 200;
                    picture.Height = 200;


                    MemoryStream streamOut = new MemoryStream();
                    document.SaveToFile(streamOut, FileFormat.Docx);

                    byte[] imageArray = streamOut.ToArray();
                    string base64ImageRepresentation = Convert.ToBase64String(imageArray);
                    return base64ImageRepresentation;

                }
            }


            return "";
        }

        public string Generador(string LaserficheID, RequerimientoBE oRequerimientoBE, List<ParametroBE> lstParametro)
        {
            LaserficheProxy laserfiche = new LaserficheProxy();
            var lstDocumento = laserfiche.ConsultarDocumentoLista(LaserficheID);
            try
            {
                if (lstDocumento != null)
                {
                    if (lstDocumento.Count > 0)
                    {

                        var ms = new MemoryStream(Convert.FromBase64String(lstDocumento[0].ArchivoBytes));

                        Document document = new Document();
                        document.LoadFromStream(ms, FileFormat.Docx);

                        foreach (var itemParametro in lstParametro)
                        {
                            if (itemParametro.Referencia == "1")
                            {
                                //string mes2 = fechaActual.ToString "MMMM", CultureInfo.CreateSpecificCulture("en-US"));
                                //string FechaString = DateTime.Now.Day.ToString().PadLeft(0, '2') + " " +  DateTime.Now.ToString("MMMM",CultureInfo.CreateSpecificCulture("es-ES")) + " de " + DateTime.Now.Year.ToString();
                                string FechaString = DateTime.Now.Day.ToString().PadLeft(0, '2') + " " + DateTime.Now.ToString("MMMM", CultureInfo.CreateSpecificCulture("es-ES")) + " de " + DateTime.Now.Year.ToString();
                                document.Replace(itemParametro.Descripcion, FechaString, false, true);
                            }
                            else
                            {
                                PropertyInfo[] lst = typeof(RequerimientoBE).GetProperties();
                                foreach (PropertyInfo oProperty in lst)
                                {
                                    if (itemParametro.Valor == oProperty.Name)
                                    {
                                        if (itemParametro.Referencia == "2")
                                        {
                                            if (oProperty.GetValue(oRequerimientoBE) != null)
                                            {
                                                string Valor = oProperty.GetValue(oRequerimientoBE).ToString();
                                                document.Replace(itemParametro.Descripcion, Valor, false, true);
                                                break;
                                            }
                                            else
                                            {
                                                document.Replace(itemParametro.Descripcion, "", false, true);
                                            }

                                        }
                                        else
                                        {
                                            if (oProperty.GetValue(oRequerimientoBE) != null)
                                            {
                                                DateTime FechaSistema = (DateTime)oProperty.GetValue(oRequerimientoBE);
                                                //   DateTime FechaSistema = Convert.ToDateTime(Valor);
                                                string FechaString = FechaSistema.Day.ToString().PadLeft(0, '2') + " " + FechaSistema.ToString("MMMM") + " de " + FechaSistema.Year.ToString();
                                                document.Replace(itemParametro.Descripcion, FechaString, false, true);
                                                break;
                                            }
                                            else
                                            {
                                                document.Replace(itemParametro.Descripcion, "", false, true);
                                            }
                                        }
                                    }
                                }
                            }
                        }


                      //  Section section = document.Sections[0];
                      // // Paragraph paragraph = section.Paragraphs.Count > 0 ? section.Paragraphs[0] : section.AddParagraph();
                      //  //    paragraph.AppendText("The sample demonstrates how to insert an image into a document.");
                      //  //   paragraph.ApplyStyle(BuiltinStyle.Heading2);
                      //  Paragraph paragraph = section.AddParagraph();
                      //  Bitmap p = new Bitmap(Image.FromFile(ApplicationKeys.rutaPlantillaCorreosProd + "pngwing.com.png"));
                      ////  p.RotateFlip(RotateFlipType.Rotate90FlipX);
                      //  DocPicture picture = document.Sections[0].Paragraphs[0].AppendPicture(p);
                      //  picture.HorizontalAlignment = ShapeHorizontalAlignment.Center;
                      //  //picture.HorizontalPosition = 0;
                      //  picture.VerticalPosition = 700;
                       
                      //  picture.Width = 100;
                      //  picture.Height = 100;

                      //  //picture.
                      //  //       picture.HorizontalAlignment = ShapeHorizontalAlignment.Center;
                      //  // picture.TextWrappingStyle = TextWrappingStyle.Through;
                      //  picture.TextWrappingStyle = TextWrappingStyle.InFrontOfText;

                        MemoryStream streamOut = new MemoryStream();
                        document.SaveToFile(streamOut, FileFormat.Docx);

                        byte[] imageArray = streamOut.ToArray();
                        string base64ImageRepresentation = Convert.ToBase64String(imageArray);
                        return base64ImageRepresentation;

                    }
                }

                return string.Empty;

            }
            catch (Exception ex)
            {
                throw ex;

            }



        }

        public string GeneradorGestionCNA(string LaserficheID, CNADetalleResponse oCNADetalleResponse, List<ParametroBE> lstParametro)
        {
            LaserficheProxy laserfiche = new LaserficheProxy();
            var lstDocumento = laserfiche.ConsultarDocumentoLista(LaserficheID);
            try
            {
                if (lstDocumento != null)
                {
                    if (lstDocumento.Count > 0)
                    {

                        var ms = new MemoryStream(Convert.FromBase64String(lstDocumento[0].ArchivoBytes));

                        Document document = new Document();
                        document.LoadFromStream(ms, FileFormat.Docx);

                        foreach (var itemParametro in lstParametro)
                        {
                            if (itemParametro.Referencia == "1")
                            {
                                //string mes2 = fechaActual.ToString "MMMM", CultureInfo.CreateSpecificCulture("en-US"));
                                //string FechaString = DateTime.Now.Day.ToString().PadLeft(0, '2') + " " +  DateTime.Now.ToString("MMMM",CultureInfo.CreateSpecificCulture("es-ES")) + " de " + DateTime.Now.Year.ToString();
                                string FechaString = DateTime.Now.Day.ToString().PadLeft(0, '2') + " " + DateTime.Now.ToString("MMMM", CultureInfo.CreateSpecificCulture("es-ES")) + " de " + DateTime.Now.Year.ToString();
                                document.Replace(itemParametro.Descripcion, FechaString, false, true);
                            }
                            else
                            {
                                PropertyInfo[] lst = typeof(CNADetalleResponse).GetProperties();
                                foreach (PropertyInfo oProperty in lst)
                                {
                                    if (itemParametro.Valor == oProperty.Name)
                                    {
                                        if (itemParametro.Referencia == "2")
                                        {
                                            if (oProperty.GetValue(oCNADetalleResponse) != null)
                                            {
                                                string Valor = oProperty.GetValue(oCNADetalleResponse).ToString();
                                                document.Replace(itemParametro.Descripcion, Valor, false, true);
                                                break;
                                            }
                                            else
                                            {
                                                document.Replace(itemParametro.Descripcion, "", false, true);
                                            }

                                        }
                                        else
                                        {
                                            if (oProperty.GetValue(oCNADetalleResponse) != null)
                                            {
                                                DateTime FechaSistema = (DateTime)oProperty.GetValue(oCNADetalleResponse);
                                                //   DateTime FechaSistema = Convert.ToDateTime(Valor);
                                                string FechaString = FechaSistema.Day.ToString().PadLeft(0, '2') + " " + FechaSistema.ToString("MMMM") + " de " + FechaSistema.Year.ToString();
                                                document.Replace(itemParametro.Descripcion, FechaString, false, true);
                                                break;
                                            }
                                            else
                                            {
                                                document.Replace(itemParametro.Descripcion, "", false, true);
                                            }
                                        }
                                    }
                                }
                            }
                        }


                        //Section section = document.Sections[0];
                        //// Paragraph paragraph = section.Paragraphs.Count > 0 ? section.Paragraphs[0] : section.AddParagraph();
                        ////    paragraph.AppendText("The sample demonstrates how to insert an image into a document.");
                        ////   paragraph.ApplyStyle(BuiltinStyle.Heading2);
                        //Paragraph paragraph = section.AddParagraph();
                        //Bitmap p = new Bitmap(Image.FromFile(ApplicationKeys.rutaPlantillaCorreosProd + "pngwing.com.png"));
                        ////  p.RotateFlip(RotateFlipType.Rotate90FlipX);
                        //DocPicture picture = document.Sections[0].Paragraphs[0].AppendPicture(p);
                        //picture.HorizontalAlignment = ShapeHorizontalAlignment.Center;
                        ////picture.HorizontalPosition = 0;
                        //picture.VerticalPosition = 700;

                        //picture.Width = 100;
                        //picture.Height = 100;

                        ////picture.
                        ////       picture.HorizontalAlignment = ShapeHorizontalAlignment.Center;
                        //// picture.TextWrappingStyle = TextWrappingStyle.Through;
                        //picture.TextWrappingStyle = TextWrappingStyle.InFrontOfText;

                        MemoryStream streamOut = new MemoryStream();
                        document.SaveToFile(streamOut, FileFormat.Docx);

                        byte[] imageArray = streamOut.ToArray();
                        string base64ImageRepresentation = Convert.ToBase64String(imageArray);
                        return base64ImageRepresentation;

                    }
                }

                return string.Empty;

            }
            catch (Exception ex)
            {
                throw ex;

            }



        }


        public  string ConvertWordtoPDF(string LaserficheID, out string LaserFicheName)
        {
            LaserficheProxy laserfiche = new LaserficheProxy();
            var lstDocumento = laserfiche.ConsultarDocumentoLista(LaserficheID);
            try
            {
                if (lstDocumento != null)
                {
                    if (lstDocumento.Count > 0)
                    {
                        LaserFicheName = System.IO.Path.GetFileNameWithoutExtension(lstDocumento[0].Nombre); 
                        var ms = new MemoryStream(Convert.FromBase64String(lstDocumento[0].ArchivoBytes));

                        Document document = new Document();
                        document.LoadFromStream(ms, FileFormat.Docx);

                        MemoryStream streamOut = new MemoryStream();
                        document.SaveToFile(streamOut, FileFormat.PDF);

                        byte[] imageArray = streamOut.ToArray();
                        string base64ImageRepresentation = Convert.ToBase64String(imageArray);
                        return base64ImageRepresentation;

                    }
                }

                LaserFicheName = string.Empty;
                return string.Empty;

            }
            catch (Exception ex)
            {
                throw ex;

            }



        }
    }
}
