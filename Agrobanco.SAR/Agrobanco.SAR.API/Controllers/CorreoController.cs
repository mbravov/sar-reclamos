﻿using Agrobanco.SAR.API.Models;
using Agrobanco.SAR.Comun;
using Agrobanco.SAR.Control.Filters;
using Agrobanco.SAR.Entidades;
using Agrobanco.SAR.Entidades.CNA_Minuta;
using Agrobanco.SAR.LogicaNegocio;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.SAR.API.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/correo")]
    [ApiController]
    public class CorreoController : ControllerBase
    {
        private readonly RequerimientoLogica requerimientoLogica;
        private IWebHostEnvironment environment;
        public CorreoController(IWebHostEnvironment _environment)
        {
            requerimientoLogica = new RequerimientoLogica();
            this.environment = _environment;
        }


        [HttpPost("reenviar-correo-requerimiento")]
        //[HttpPost("parametro")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult Requerimiento([Required][FromHeader(Name = "Token")] string token, [FromBody] RequerimientoBE oRequerimientoBE)
        {
            ResponseSuccess oResponseSuccess = new ResponseSuccess();
            ResponseSuccessDetalle oResponseSuccessDetalle = new ResponseSuccessDetalle();
            ResponseErrorDetalle oResponseErrorDetalle = new ResponseErrorDetalle();
            try
            {
                EnvioCorreo oEnviarCorreo = new EnvioCorreo();
                List<string> listaCorreo = new List<string>();
                listaCorreo.Add(oRequerimientoBE.CorreoElectronico);

                List<string> listaCorreocopia = new List<string>();

                var listado = requerimientoLogica.RequerimientoDetalle(oRequerimientoBE);
                string cuerpocorreo = string.Empty;
                bool resultado = false;
                string webRootPath = environment.ContentRootPath;
                cuerpocorreo = EnvioCorreoCuerpo.CuerpoCorreoRegistrarDocumento(listado, webRootPath);
                resultado = oEnviarCorreo.EnviarCorreo(listaCorreo, listaCorreocopia, cuerpocorreo,ApplicationKeys.CorreoAsunto);
              
                oResponseSuccessDetalle.resultado = resultado;
                oResponseSuccess.success = oResponseSuccessDetalle;

                return StatusCode(200, oResponseSuccess);
            }
            catch (Exception ex)
            {
                oResponseErrorDetalle.codigo = string.Empty;
                oResponseErrorDetalle.resultado = false;
                oResponseErrorDetalle.mensaje = ex.Message;

                return StatusCode(100, oResponseErrorDetalle);
            }
        }


    }
}
