export interface ConsultaInterface {
    IdConsulta : string;
    FechaRegistroIni : string;
    FechaRegistroFin : string;
    TipoDocumento:string;
    NroDocumento : string;
    NombresApellidos : string;
    CorreoElectronico : string;
    NroCelular : string;
}
