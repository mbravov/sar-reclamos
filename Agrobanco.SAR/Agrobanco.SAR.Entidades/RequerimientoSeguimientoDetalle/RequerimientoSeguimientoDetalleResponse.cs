﻿using Agrobanco.SAR.Entidades.RequerimientoAsignacion;
using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades.RequerimientoSeguimientoDetalle
{
    public class RequerimientoSeguimientoDetalleResponse
    {
		public Int64 CodRequerimientoMovimiento { get; set; }
		public Int64 CodRequerimiento { get; set; }
		public string Estado { get; set; }
		public DateTime? FechaEntrega { get; set; }
		public DateTime? FechaCartaRespuesta { get; set; }
		public DateTime? FechaCartaRespuestaOcm { get; set; }
		public string UsuarioRegistro { get; set; }
		public DateTime? FechaRegistro { get; set; }
		public int DiasTranscurrido { get; set; }

		public List<RequerimientoAsignacionResponse> RequerimientoAsignacion { get; set; }
	}
}
