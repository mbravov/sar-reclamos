DROP PROCEDURE IF EXISTS dbo.SP_REQUERIMIENTO_ACTUALIZAR_PENDIENTE_SIN_ATENCION; 
GO
--=================================================================================================================================
-- FECHA DE CREACI�N		: 31/05/2021
-- FECHA DE MODIFICACI�N	:
-- CREADO POR				: WILLIAM QUIROZ
-- STORE PROCEDURE			: SP_REQUERIMIENTO_ACTUALIZAR_PENDIENTE_SIN_ATENCION
-- PARAMETOS				: 
-- FUNCIONALIDAD			: 
--================================================================================================================================

CREATE PROCEDURE SP_REQUERIMIENTO_ACTUALIZAR_PENDIENTE_SIN_ATENCION

AS
BEGIN		

--------1. Requerimiento ha pasado 7 d�as----------------------------------------------------------------------------------------

		CREATE TABLE #TRequerimientoMovimiento (CodRequerimiento BIGINT, FechaRegistro DATETIME)

		INSERT INTO #TRequerimientoMovimiento
		SELECT  RM.CodRequerimiento, MAX(RM.FechaRegistro) AS FechaRegistro
		FROM  Requerimiento RE
		INNER JOIN RequerimientoMovimiento RM ON RM.CodRequerimiento = RE.CodRequerimiento
		WHERE RE.Estado <> 8 AND  RE.Estado <> 9 AND RE.Estado <> 10					
		GROUP BY RM.CodRequerimiento

		;WITH cte_data(CodRequerimiento, FechaRegistro) AS
		(
			SELECT C.CodRequerimiento, MAX( C.FechaRegistro)
			FROM #TRequerimientoMovimiento D
			INNER JOIN  Comentario C ON C.CodRequerimiento = D.CodRequerimiento
			GROUP BY C.CodRequerimiento
		)

		UPDATE RM
		SET fechaRegistro =  CASE WHEN RM.FechaRegistro > C.FechaRegistro THEN RM.FechaRegistro ELSE C.FechaRegistro END
		FROM #TRequerimientoMovimiento RM
		INNER JOIN cte_data C ON C.CodRequerimiento = RM.CodRequerimiento

		DELETE #TRequerimientoMovimiento
		WHERE CONVERT(DATE,DATEADD(d,7,FechaRegistro),103)  >= CONVERT(DATE,GETDATE(),103)

		--SELECT CONVERT(DATE,DATEADD(d,7,FechaRegistro),103), * FROM #TRequerimientoMovimiento

		INSERT INTO RequerimientoMovimiento (CodRequerimiento,Estado,UsuarioRegistro,FechaRegistro)
		SELECT CodRequerimiento, 9, 'SAR',GETDATE()
		FROM #TRequerimientoMovimiento

		UPDATE R 
		SET Estado = 9,			
		FechaModificacion = GETDATE()			
		FROM #TRequerimientoMovimiento RM
		INNER JOIN Requerimiento R ON RM.CodRequerimiento = R.CodRequerimiento		

		DROP TABLE #TRequerimientoMovimiento


--------2. Requerimiento transcurrido 15 d�as----------------------------------------------------------------------------------------

		CREATE TABLE #TRequerimiento (CodRequerimiento BIGINT, FechaRegistro DATETIME)

		INSERT INTO #TRequerimiento
		SELECT  Re.CodRequerimiento, MAX(Re.FechaRegistro) AS FechaRegistro
		FROM  Requerimiento RE
		INNER JOIN RequerimientoMovimiento RM ON RM.CodRequerimiento = RE.CodRequerimiento
		WHERE RE.Estado <> 8 AND  RE.Estado <> 9 AND RE.Estado <> 10						
		GROUP BY Re.CodRequerimiento

		DELETE #TRequerimiento
		WHERE CONVERT(DATE,DATEADD(d,15,FechaRegistro),103) >= CONVERT(DATE,GETDATE(),103)

		--SELECT CONVERT(DATE,DATEADD(d,15,FechaRegistro),103), * FROM #TRequerimiento

		INSERT INTO RequerimientoMovimiento (CodRequerimiento,Estado,UsuarioRegistro,FechaRegistro)
		SELECT CodRequerimiento, 10, 'SAR',GETDATE()
		FROM #TRequerimiento
			
		UPDATE RE 
		SET Estado = 10,			
		FechaModificacion = GETDATE()			
		FROM #TRequerimiento TR
		INNER JOIN Requerimiento RE ON TR.CodRequerimiento = RE.CodRequerimiento	

		DROP TABLE #TRequerimiento

END
GO




			

