
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AppService } from '../app.service';
import { SecurityService } from './security.service';

const UrlBase_SARAPI = environment.UrlBase_SARAPI;

@Injectable({
  providedIn: 'root'
})
export class RequerimientoService extends AppService {

  constructor(securityService : SecurityService) {
    super(securityService);
  }

  ObtenerAgencia<T>():Observable<any[]>{
    this.endpoint = `${UrlBase_SARAPI}/Agencia/listar`;
    return this.post(this.endpoint);
  }

  ObtenerParametro<T>():Observable<any[]>{

    const datos = {
      "strGrupo" : '300,400,500,600,700'
    };


    this.endpoint = `${UrlBase_SARAPI}/parametro/lista-grupo`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  Registrar<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${UrlBase_SARAPI}/Requerimiento/registrar`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  ObtenerRequerimientoLista<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${UrlBase_SARAPI}/Requerimiento/listarequerimientos`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  ObtenerBloqueado<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${UrlBase_SARAPI}/Requerimiento/requerimientobloqueado`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  ObtenerRequerimientoDetalle<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${UrlBase_SARAPI}/Requerimiento/requerimientodetalle`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  ActualizarRequerimientoBloqueado<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${UrlBase_SARAPI}/Requerimiento/requerimientoactualizarbloqueado`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  PenultimoMovimiento<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${UrlBase_SARAPI}/Requerimiento/requerimientomovimientopenultimo`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  RegistrarMovimiento<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${UrlBase_SARAPI}/Requerimiento/requerimientomovimientoregistrar`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  RegistrarAsignacion<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${UrlBase_SARAPI}/Requerimiento/requerimientoasignacionregistrar`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  ObtenerAsignacion<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${UrlBase_SARAPI}/Requerimiento/requerimientoasignacionobtener`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  ActualizarEstados<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${UrlBase_SARAPI}/Requerimiento/requerimientoactualizarestado`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  public RecursoObtener<T>(datos:any):Observable<any> {
    console.log("input",datos);
    this.endpoint = `${UrlBase_SARAPI}/recurso/recursoobtener`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  RemoveFile<T>(datos:any):Observable<any> {
    console.log("input",datos);
    this.endpoint = `${UrlBase_SARAPI}/laserfiche/eliminar`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  RecursoEliminar<T>(datos:any):Observable<any> {
    console.log("input",datos);
    this.endpoint = `${UrlBase_SARAPI}/recurso/recursoeliminar`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  DescagarArchivos<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${UrlBase_SARAPI}/laserfiche/obtener`;  //  /laserfiche/obtener
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  reenviar_correo<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${UrlBase_SARAPI}/correo/reenviar-correo-requerimiento`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  RegistrarComentario<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${UrlBase_SARAPI}/comentario/comentarioregistrar`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  GenerarDocumento<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${UrlBase_SARAPI}/documentos/generador`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  ObtenerComentario<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${UrlBase_SARAPI}/comentario/comentarioobtener`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  ObtenerRecursos<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${UrlBase_SARAPI}/recurso/listar`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }
  SubirArchivo<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${UrlBase_SARAPI}/laserfiche/registrar`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  ObtenerUsuarios<T>(datos:any):Observable<any[]>{
    console.log("input",datos);
    //console.log("input",datos);
    this.endpoint = `${UrlBase_SARAPI}/Usuario/listar`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  DerivarOCM<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${UrlBase_SARAPI}/Requerimiento/derivar-ocm`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }
  eliminarArchivo<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${UrlBase_SARAPI}/laserfiche/eliminar`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  AprobarRequerimiento<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${UrlBase_SARAPI}/Requerimiento/aprobar`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  DerivarParaSuNotificacion<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${UrlBase_SARAPI}/Requerimiento/derivar-para-su-notificacion`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }
  AtenderRequeriminto<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${UrlBase_SARAPI}/Requerimiento/atender`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  ObtenerPlantillas<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${UrlBase_SARAPI}/plantilla/listar`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  ObtenerConsultaLista<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${UrlBase_SARAPI}/Consulta/listar`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  ObtenerConsultaDetalle<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${UrlBase_SARAPI}/Consulta/consultadetalle`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  RegistrarComentarioAnular<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${UrlBase_SARAPI}/comentario/comentarioregistraranular`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  ObtenerListaUsuarios<T>(datos:any):Observable<any[]>{
   
    this.endpoint = `${UrlBase_SARAPI}/Usuario/listarusuariosmantenimiento`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  RegistrarUsuario<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${UrlBase_SARAPI}/Usuario/registrarusuario`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  ObtenerUsuarioDetalle<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${UrlBase_SARAPI}/Usuario/usuariodetalle`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  ActualizarUsuario<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${UrlBase_SARAPI}/Usuario/actualizarusuario`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

}
