﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Comun
{
    public class Constantes
    {
    }

    public static class ConstantesTabla
    {
        public static int Requerimiento = 1;
        public static int RequerimientoMovimiento = 2;
        public static int Comentario = 3;
        public static int Consultas = 4;
        public static int Plantillas = 5;
        public static int CartaRespuesta = 6;
        public static int CartaRespuestaOCM = 7;
    }


    public static class ConstantesFormaEnvio
    {
        public static int Whatsapp = 1;
        public static int Email = 2;
        public static int Agencia = 3;
        public static int Domicilio = 4;
    }
    public static class ConstantesTipoRequerimiento
    {
        public static int Solicitud = 1;
        public static int Reclamo = 2;
        public static int Queja = 3;
   
    }

    public static class ConstantesTipoDocumento
    {
        public static string TipoDocumentoDNI = "DNI";
        public static string TipoDocumentoRUC = "RUC";
    }

    public static class ConstantesPerfil
    {
        public static string UDA = "UDA";
        public static string ADMAG = "ADMAG";
    }

    public static class ConstantesTipoEvento
    {
        public static int RegistroEmision = 1;
        public static int RegistroDerivacion = 2;
        public static int ActualizacionEmision = 3;
        public static int ActualizacionDerivacion = 4;
        public static int NotificacionCorreo = 5;
        public static int RegistroComentarioEmision = 6;
        public static int CNAConvertirWordToPDF = 7;
        public static int ActualizarRecursoEmision = 8;
    }

    public static class ConstantesEstadoGestionCNA
    {
        public static int DerivarGAR =  16;
        public static int DerivarADMAG =    17;
        public static int DerivarLEG = 27;
        public static int RechazarGAR = 26;
        public static int RechazarLEG = 30;
        public static int RechazarADMAGE = 34;
        public static int RechazarUDA = 40;
        public static int EnvarCRUDA = 19;

        public static int DerivarADMAGDesdeLegal = 31;
        public static int DerivarAFN = 35;

        public static int EstadoAtendidoEmision = 12;
        public static int EstadoObservadoEmision = 13;
    }

    public static class ConstantesTipoRecursoEmision
    {
        public static int CartadeNoAdeudo = 1;
        public static int CartadeNoAdeudodeADMAGE = 2;
        public static int CartadeRespuesta = 3;
        public static int DocumentosEvaluadosporGarantia = 4;
        public static int Minuta = 5;
        public static int CartadeEntregaMinuta = 6;
        public static int CargodeEntrega = 7;
 
    }
    public static class ConstantesMedioNotificacion
    {
        public static int Email = 2;
        public static int Whatsapp = 3;
    }

}


