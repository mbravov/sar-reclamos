﻿using Agrobanco.SAR.Comun.Laserfiche;
using Agrobanco.SAR.Datos;
using Agrobanco.SAR.Entidades;
using Agrobanco.SAR.Entidades.RequerimientoGestionReporte;
using Agrobanco.SAR.Entidades.RequerimientoReporte;
using Agrobanco.SAR.Entidades.RequerimientoSeguimiento;
using Agrobanco.SAR.Entidades.RequerimientoSeguimientoDetalle;
using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.LogicaNegocio
{
    public class RequerimientoLogica
    {
        private readonly RequerimientoDatos requerimientoDatos;

        public RequerimientoLogica()
        {
            requerimientoDatos = new RequerimientoDatos();
        }

        public bool Registrar(RequerimientoBE oBE, out string NroRequerimiento)
        {
            return requerimientoDatos.Registrar(oBE, out NroRequerimiento);
        }

        public List<RequerimientoBE> RequerimientosListar(RequestRequerimiento oBE)
        {
            return requerimientoDatos.RequerimientosListar(oBE);
        }

        public RequerimientoBE RequerimientoDetalle(RequerimientoBE oBE)
        {
            return requerimientoDatos.RequerimientoDetalle(oBE);
        }

        public List<ResponseReqBloqueado> RequerimientoBloqueado(RequerimientoBE oBE)
        {
            return requerimientoDatos.RequerimientoBloqueado(oBE);
        }

        public List<RequerimientoBE> RequerimientoMovimientoPenultimo(int CodRequerimiento)
        {
            return requerimientoDatos.RequerimientoMovimientoPenultimo(CodRequerimiento);
        }
                
        public bool RequerimientoActualizarBloqueado(RequerimientoBE oBE)
        {
            return requerimientoDatos.RequerimientoActualizarBloqueado(oBE);
        }

        public int RequerimientoMovimientoRegistrar(RequerimientoBE oBE)
        {
            return requerimientoDatos.RequerimientoMovimientoRegistrar(oBE);
        }

        public bool RequerimientoAsignacionRegistrar(RequerimientoBE oBE, string webRootPath)
        {
            return requerimientoDatos.RequerimientoAsignacionRegistrar(oBE, webRootPath);
        }

        public List<RequerimientoBE> RequerimientoAsignacionObtener(int CodRequerimientoAsignacion)
        {
            return requerimientoDatos.RequerimientoAsignacionObtener(CodRequerimientoAsignacion);
        }        

        public bool RequerimientoActualizarEstado(RequerimientoBE oBE)
        {
            return requerimientoDatos.RequerimientoActualizarEstado(oBE);
        }
      
        //public Documento DescagarArchivos(RecursoBE oBE)
        //{
        //    return requerimientoDatos.DescagarArchivos(oBE);
        //}            

        public int DerivadoOCM(RequerimientoBE oBE, string webRootPath)
        {
            return requerimientoDatos.DerivadoOCM(oBE, webRootPath);
        }

        public bool AprobarRequerimiento(RequerimientoBE oBE)
        {
            return requerimientoDatos.AprobarRequerimiento(oBE);
        }

        public int DerivarParaSuNotificacion(RequerimientoBE oBE, string webRootPath)
        {
            return requerimientoDatos.DerivarParaSuNotificacion(oBE, webRootPath);
        }

        public bool AtenderRequerimiento(RequerimientoBE oBE)
        {
            return requerimientoDatos.AtenderRequerimiento(oBE);
        }

        public List<RequerimientoSeguimientoFiltroResponse> ObtenerSeguimiento(RequerimientoSeguimientoFiltroRequest request)
        {
            return requerimientoDatos.ObtenerSeguimiento(request);
        }

        public List<RequerimientoSeguimientoDetalleResponse> ObtenerSeguimientoDetalle(RequerimientoSeguimientoDetalleRequest request)
        {
            return requerimientoDatos.ObtenerSeguimientoDetalle(request);
        }

        public List<RequerimientoReporteFiltroResponse> ObtenerRequerimientoReporte(RequerimientoReporteFiltroRequest request)
        {
            return requerimientoDatos.ObtenerRequerimientoReporte(request);
        }

        public List<RequerimientoGestionReporteFiltroResponse> ObtenerGestionRequerimientoReporte(RequerimientoGestionReporteFiltroRequest request)
        {
            return requerimientoDatos.ObtenerRequerimientoGestionReporte(request);
        }
        
    }
}

