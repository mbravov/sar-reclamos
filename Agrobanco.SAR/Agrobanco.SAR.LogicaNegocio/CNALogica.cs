﻿using Agrobanco.SAR.Datos;
using Agrobanco.SAR.Entidades;
using Agrobanco.SAR.Entidades.CNA_Minuta;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SAR.LogicaNegocio
{
    public class CNALogica
    {
        private readonly CNADatos cnaDatos;
        public CNALogica()
        {
            cnaDatos = new CNADatos();
        }
        public List<CNAFilterResponse> Listar(CNAFilterRequest oRequest)
        {
            try
            {
                return cnaDatos.Listar(oRequest);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CNAFilterResponse> ListarSeguimiento(CNAFilterRequest oRequest)
        {
            try
            {
                return cnaDatos.ListarSeguimiento(oRequest);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<EstadosCNAMinBE> ListarEstados(CNAFilterRequest oRequest)
        {
            try
            {
                return cnaDatos.ListarEstados(oRequest);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<EstadosCNAMinBE> ListarEstadosGeneral()
        {
            try
            {
                return cnaDatos.ListarEstadosGeneral();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       

       
        public async Task<CNADetalleResponse> ObtenerPorNumeroDocumento(CNADetalleRequest oRequest)
        {
            try
            {
                return await cnaDatos.ObtenerPorNumeroDocumento(oRequest);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<AgenciaBE> ListarAgenciaCNA(CNAFilterRequest oRequest)
        {
            try
            {
                return cnaDatos.ListarAgenciaCNA(oRequest);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
    }
}
