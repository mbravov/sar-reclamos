﻿using Agrobanco.SAR.Datos;
using Agrobanco.SAR.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.LogicaNegocio
{
    public class ConsultaLogica
    {
        private readonly ConsultaDatos consultaDatos;

        public ConsultaLogica()
        {
            consultaDatos = new ConsultaDatos();
        }

        public List<ConsultaBE> ConsultasListar(ConsultaBE oBE)
        {
            return consultaDatos.ConsultasListar(oBE);
        }

        public ConsultaBE ConsultaDetalle(ConsultaBE oBE)
        {
            return consultaDatos.ConsultaDetalle(oBE);
        }

        public bool ConsultaRegistrar(ConsultaBE oBE, string webRootPath)
        {
            return consultaDatos.ConsultaRegistrar(oBE, webRootPath);
        }

        

    }
}
