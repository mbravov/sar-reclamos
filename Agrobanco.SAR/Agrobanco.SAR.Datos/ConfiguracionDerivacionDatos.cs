﻿using Agrobanco.SAR.Datos.SqlServer.Acceso;
using Agrobanco.SAR.Entidades.CNA_Minuta;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SAR.Datos
{
    public class ConfiguracionDerivacionDatos
    {
        public string cadenaConexion;
        public async Task<List<ConfiguracionDerivacionBE>> Obtener(ConfiguracionDerivacionBE request)
        {
            ConfiguracionDerivacionBE oConfiguracionDerivacionBE = new ConfiguracionDerivacionBE();
            List<ConfiguracionDerivacionBE> LstConfiguracionDerivacionBE = new List<ConfiguracionDerivacionBE>();
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("SP_CONFIGURACION_DERIVACION_OBTENER", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@IdConfiguracionAccion", SqlDbType.Int).Value = request.IdConfiguracionAccion;
                        cmd.Parameters.Add("@GrupoFiltro", SqlDbType.Int).Value = request.GrupoFiltro;
                        try
                        {
                            using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                            {
                                while (reader.Read())
                                {
                                    oConfiguracionDerivacionBE = new ConfiguracionDerivacionBE();

                                    if (!reader.IsDBNull(0))
                                        oConfiguracionDerivacionBE.IdConfiguracionAccion = reader.GetInt32(0);

                                    if (!reader.IsDBNull(1))
                                        oConfiguracionDerivacionBE.GrupoFiltro = reader.GetInt32(1);

                                    if (!reader.IsDBNull(2))
                                        oConfiguracionDerivacionBE.TipoProcesamiento = reader.GetInt32(2);

                                    if (!reader.IsDBNull(3))
                                        oConfiguracionDerivacionBE.TipoEvento = reader.GetInt32(3);

                                    if (!reader.IsDBNull(4))
                                        oConfiguracionDerivacionBE.Perfil = reader.GetString(4);

                                    if (!reader.IsDBNull(5))
                                        oConfiguracionDerivacionBE.IdEstado = reader.GetInt32(5);

                                    if (!reader.IsDBNull(6))
                                        oConfiguracionDerivacionBE.IdConfiguracionDerivacion = reader.GetInt32(6);

                                    LstConfiguracionDerivacionBE.Add(oConfiguracionDerivacionBE);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            throw ex;
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return await Task.Run(() => LstConfiguracionDerivacionBE);
        }
    }
}
