﻿using Agrobanco.SAR.Datos;
using Agrobanco.SAR.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.LogicaNegocio
{
    public class UbigeoLogica
    {
        private readonly UbigeoDatos _ubigeoDatos;
        public UbigeoLogica() {
            _ubigeoDatos = new UbigeoDatos();
        }

        public List<UbigeoBE> ListarDepartamento()
        {
            try
            {
                return _ubigeoDatos.ListarDepartamento();
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public List<UbigeoBE> ListarProvincia(UbigeoBE oBE)
        {
            try
            {
                return _ubigeoDatos.ListarProvincia(oBE);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public List<UbigeoBE> ListarDistrito(UbigeoBE oBE)
        {
            try
            {
                return _ubigeoDatos.ListarDistrito(oBE);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
    }
}
