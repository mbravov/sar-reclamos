USE DBAgrSAR
GO
DROP PROCEDURE IF EXISTS dbo.SP_CNA_ESTADOS_GENERAL_LISTAR
GO
 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 23/11/2021
-- FECHA DE MODIFICACIÓN	: 
-- CREADO POR				: KARINA TORRES
-- STORE PROCEDURE			: SP_CNA_ESTADOS_GENERAL_LISTAR
-- FUNCIONALIDAD			: Obtener la lista estados para para bandeja de seguimientos de Cna y Minutas
--================================================================================================================================

CREATE PROCEDURE [dbo].[SP_CNA_ESTADOS_GENERAL_LISTAR] 
AS
BEGIN
	SELECT  EMI.IdEstado CodEstado 
	       ,EMI.Descripcion 
	FROM    EstadoEmision EMI WITH(NOLOCK)
	WHERE   EMI.Perfil IS NULL	
END
GO
 
DROP PROCEDURE IF EXISTS dbo.SP_CNA_ESTADOS_LISTAR
GO
 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 23/11/2021
-- FECHA DE MODIFICACIÓN	: 
-- CREADO POR				: KARINA TORRES
-- STORE PROCEDURE			: SP_CNAMINUTA_ESTADOS_LISTAR
-- FUNCIONALIDAD			: Obtener la lista de estados de Cna y Minutas por perfiles
--================================================================================================================================

CREATE PROCEDURE [dbo].[SP_CNA_ESTADOS_LISTAR] 
@Perfil VARCHAR(50)
AS
BEGIN
	 SELECT  EMI.IdEstado CodEstado 
	        ,EMI.Descripcion 
	 FROM EstadoEmision EMI WITH(NOLOCK)
	 WHERE EMI.Perfil = @Perfil	
END
GO

DROP PROCEDURE IF EXISTS dbo.SP_CNA_MINUTA_LISTAR
GO
 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 23/11/2021
-- FECHA DE MODIFICACIÓN	: 
-- CREADO POR				: KARINA TORRES
-- MODIFICADO POR			: MIGUEL GÜERE DÍAZ
-- STORE PROCEDURE			: SP_CNA_MINUTA_LISTAR
-- FUNCIONALIDAD			: Obtener la lista de Cna y Minutas para la bandeja principal
--================================================================================================================================

CREATE PROCEDURE [dbo].[SP_CNA_MINUTA_LISTAR] 
@Perfil VARCHAR(50),
@CodEstado VARCHAR(10),
@CodAgencia VARCHAR(10) = NULL,
@NroDocumento VARCHAR(20) = NULL,
@FechaCancelacionIni DATETIME,
@FechaCancelacionFin DATETIME = NULL,
@UsuarioWeb VARCHAR (50)

AS
BEGIN   
	
	DECLARE @PERFIL_UDA VARCHAR(5) = 'UDA'
	DECLARE @CodUsuario INT

	SELECT @CodUsuario = CodUsuario
	FROM Usuario
	WHERE UsuarioWeb = @UsuarioWeb

	SELECT
		 CC.FechaCancelacion
		,CC.Nombres AS NombreApellidos
		,CC.TipoDocumento+' - '+CC.NumeroDocumento  AS NroDocumento
		,CC.Banco
		,(CASE  WHEN  D.CodAgencia IS NULL THEN CC.Agencia  ELSE  AG.Oficina END ) Agencia
		,CC.AnalistaProponente
		,ES.Descripcion AS Estado
		,D.PerfilDerivado AS DerivadoA
		,D.FechaDerivado AS FechaDerivacion
		,TP.Parametro  AS procesamiento
		,TP.Descripcion AS procesamientoDesc
		,E.IdEmision
	FROM   
		CreditoCliente CC WITH(NOLOCK) 
		INNER JOIN Emision E WITH(NOLOCK) ON CC.IdCreditoCliente = E.IdCreditoCliente
		INNER JOIN Derivacion D WITH(NOLOCK) ON D.IdEmision = E.IdEmision
		INNER JOIN PARAMETRO TP WITH(NOLOCK) ON TP.Parametro = CC.TipoProcesamiento  AND TP.Grupo = 1500
		INNER JOIN ESTADOEMISION ES WITH(NOLOCK) ON ES.IdEstado = D.Estado
		LEFT JOIN AGENCIA AG  WITH(NOLOCK) ON D.CodAgencia = AG.CodAgencia
	WHERE
		((@NroDocumento IS NULL OR @NroDocumento = '') OR CC.NumeroDocumento LIKE CAST(@NroDocumento AS VARCHAR)+'%')	
		AND  ((@CodEstado IS NULL OR @CodEstado = '') OR D.Estado = @CodEstado)
		AND  ((@Perfil IS NULL OR @Perfil = '') OR D.Perfil = @Perfil)	
		AND  ((@CodAgencia IS NULL OR @CodAgencia = '') OR (CASE  WHEN  D.CodAgencia IS NULL THEN CC.codAgencia  ELSE  D.CodAgencia END ) = CAST(@CodAgencia AS INT))
		AND  ((D.Perfil != @PERFIL_UDA) OR (D.Perfil = @PERFIL_UDA AND D.CodUsuarioDerivado = @CodUsuario))
		AND ((@FechaCancelacionIni IS NULL OR @FechaCancelacionIni = '') OR CAST(CC.FechaCancelacion AS DATE) >= CAST(@FechaCancelacionIni AS DATE))
		AND ((@FechaCancelacionFin IS NULL OR @FechaCancelacionFin = '') OR CAST(CC.FechaCancelacion AS DATE) <= CAST(@FechaCancelacionFin AS DATE))
	ORDER BY 
		CAST(CC.FechaCancelacion AS DATE) DESC,
		(CASE  WHEN  D.CodAgencia IS NULL THEN CC.Agencia  ELSE  AG.Oficina END ),
		CC.Nombres		
END
GO

 
DROP PROCEDURE IF EXISTS dbo.SP_CNA_AGENCIA_LISTAR
GO

---=================================================================================================================================  
-- FECHA DE CREACIÓN     : 30/11/2021  
-- FECHA DE MODIFICACIÓN :  
-- CREADO POR			 : KARINA TORRES  
-- STORE PROCEDURE		 : SP_CNA_AGENCIA_LISTAR           
-- FUNCIONALIDAD		 : Obtener la lista de agencias  
--==================================================================================================================================  
  
CREATE PROCEDURE dbo.SP_CNA_AGENCIA_LISTAR   
@UsuarioWeb varchar(10) = null
AS  
BEGIN  

	 DECLARE @CodAgencia INT

	 SELECT @CodAgencia = CodAgencia 
	 FROM   Usuario
	 where  UsuarioWeb = @UsuarioWeb

	 SELECT CodAgencia  
	       ,Oficina   
	 FROM  Agencia WITH(NOLOCK)  
	 WHERE 
		  @CodAgencia  IS NULL OR @CodAgencia = 0 OR CodAgencia = @CodAgencia
	 ORDER BY Oficina    
END  
GO

 
DROP PROCEDURE IF EXISTS dbo.SP_CNA_GENERAL_LISTAR 
GO
 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 23/11/2021
-- FECHA DE MODIFICACIÓN	: 
-- CREADO POR				: KARINA TORRES
-- STORE PROCEDURE			: SP_CNA_GENERAL_LISTAR
-- FUNCIONALIDAD			: Obtener la lista de Cna y Minutas para bandeja de Seguimiento
--================================================================================================================================

CREATE PROCEDURE [dbo].[SP_CNA_GENERAL_LISTAR] 
@Perfil VARCHAR(50),
@CodEstado VARCHAR(10) = NULL,
@CodAgencia VARCHAR(10) = NULL,
@NroDocumento VARCHAR(20) = NULL,
@FechaCancelacionIni DATETIME,
@FechaCancelacionFin DATETIME = NULL
AS
BEGIN

	SELECT
		 CC.FechaCancelacion
		,CC.Nombres AS NombreApellidos
		,CC.TipoDocumento+' - '+CC.NumeroDocumento  AS NroDocumento
		,CC.Banco
		,CC.Agencia
		,CC.AnalistaProponente
		,ES.Descripcion AS Estado	
		,(SELECT  P.valor FROM PARAMETRO P 
		  WHERE P.Grupo=1400 AND P.Descripcion = (case  E.Estado when 1 then 'OCM'
																when 2 then 'OCM' 
																when 3 then 'GAR' 
																when 4 then 'GAR'
																when 5 then 'LEG' 
																when 6 then 'LEG'
																when 7 then 'ADMAG' 
																when 8 then 'ADMAG'
																when 9 then 'UDA' 
																when 10 then case E.MedioNotificacion when '2' then 'OCM' when '3' then 'OCM' when '1' then'UDA' else '' end
																when 11 then 'OCM'
																when 12 then 'OCM'
																when 13 then 'OCM'
																else 'OCM' end)) DerivadoA
		,(select max(mv.FechaRegistro) from EmisionMovimiento mv where IdEmision =  E.IdEmision) FechaDerivacion
		,TP.Parametro  AS procesamiento
		,TP.Descripcion AS procesamientoDesc
	FROM    
		CreditoCliente CC WITH(NOLOCK)
		INNER JOIN Emision E WITH(NOLOCK) ON CC.IdCreditoCliente = E.IdCreditoCliente		
		INNER JOIN PARAMETRO TP WITH(NOLOCK) ON TP.Parametro = CC.TipoProcesamiento  AND TP.Grupo = 1500		
		INNER JOIN ESTADOEMISION ES WITH(NOLOCK) ON ES.IdEstado = E.Estado
	WHERE
		((@NroDocumento IS NULL OR @NroDocumento = '') OR CC.NumeroDocumento LIKE CAST(@NroDocumento AS VARCHAR)+'%')	
		AND  ((@CodEstado IS NULL OR @CodEstado = '') OR E.Estado = @CodEstado	)
		AND  ((@CodAgencia IS NULL OR @CodAgencia = '') OR CC.codAgencia = CAST(@CodAgencia AS INT))
		AND ((@FechaCancelacionIni IS NULL OR @FechaCancelacionIni = '') OR CAST(CC.FechaCancelacion AS DATE) >= CAST(@FechaCancelacionIni AS DATE))
		AND ((@FechaCancelacionFin IS NULL OR @FechaCancelacionFin = '') OR CAST(CC.FechaCancelacion AS DATE) <= CAST(@FechaCancelacionFin AS DATE))
	ORDER BY 
	    CAST(CC.FechaCancelacion AS DATE) DESC,
		CC.Agencia,
		CC.Nombres			
		
END
GO


 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 23/11/2021
-- FECHA DE MODIFICACIÓN	: 13/12/2021
-- CREADO POR				: JOSE CAYCHO
-- MODIFICADO POR			: KARINA TORRES
-- STORE PROCEDURE			: SP_COMENTARIO_EMISION_OBTENER
-- FUNCIONALIDAD			: Obtener la lista de comentarios
--================================================================================================================================

DROP PROCEDURE IF EXISTS SP_COMENTARIO_EMISION_OBTENER
GO

CREATE PROCEDURE SP_COMENTARIO_EMISION_OBTENER
( 
@IdEmision INT
)
AS
BEGIN

	SELECT 
		CE.IdComentarioEmision,
		CE.IdEmision,
		CE.IdDerivacion,
		CE.IdDerivacionMovimiento,				
		CE.Texto  AS Texto,
		R.LaserficheID,
		CE.FechaRegistro,
		U.NombresApellidos UsuarioRegistro,
		(CASE  ISNULL(CE.IdMotivo,0)  WHEN   0  THEN  NULL  ELSE  'Motivo: '+ P.Descripcion  END) as Motivo
	FROM ComentarioEmision CE WITH(NOLOCK)
	LEFT JOIN Recurso R WITH(NOLOCK) ON R.ReferenciaID =  CE.IdComentarioEmision AND R.CodTabla = 9
	LEFT JOIN Usuario U WITH(NOLOCK) ON CE.UsuarioRegistro = U.UsuarioWeb
	LEFT JOIN Parametro P WITH(NOLOCK) ON P.Parametro = CE.IdMotivo AND P.Grupo ='1900' AND P.Estado = 1
	WHERE CE.IdEmision = @IdEmision

END
GO

 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 23/11/2021
-- FECHA DE MODIFICACIÓN	: 09/12/2021
-- CREADO POR				: JOSE CAYCHO
-- MODIFICADO POR			: KARINA TORRES CAHUANA
-- STORE PROCEDURE			: SP_COMENTARIO_EMISION_INSERTAR
-- FUNCIONALIDAD			: Realiza un registro en la tabla ComentarioEmision
--================================================================================================================================

DROP PROCEDURE IF EXISTS SP_COMENTARIO_EMISION_INSERTAR
GO
CREATE PROCEDURE SP_COMENTARIO_EMISION_INSERTAR
(
	@IdEmision INT,
	@IdDerivacion INT = NULL,
--	@IdDerivacionMovimiento INT = NULL,
	@Texto VARCHAR(300),
	@UsuarioRegistro VARCHAR(50),
	@Perfil VARCHAR(5),
	@CodAgencia INT,
	@Estado INT,
	@IdMotivo INT = NULL,
	@IdComentarioEmision INT OUTPUT
)
AS
BEGIN

	DECLARE @EXISTE_COMENTARIO INT = 0
	DECLARE @EXISTE_MOVIMIENTO BIGINT = 0

	DECLARE @V_IdDerivacion INT = 0
	DECLARE @V_IdDerivacionMovimiento INT = 0

	DECLARE @ESTADO_PENDIENTE_P INT = 14
	DECLARE @ESTADO_EN_PROCESO_P INT = 15
	
	DECLARE @ESTADO_EN_PROCESO_OCM_G INT = 2

	IF @IdMotivo IS NULL
	BEGIN

		SET @EXISTE_COMENTARIO = (SELECT COUNT(IdComentarioEmision) FROM ComentarioEmision WHERE IdDerivacion = @IdDerivacion)




		IF @EXISTE_COMENTARIO = 0 
		BEGIN
				update Derivacion set
				IdEmision = @IdEmision,
				Perfil = @Perfil,
				Estado = @ESTADO_EN_PROCESO_P,
				CodAgencia = @CodAgencia,
				FechaModificacion = GETDATE(),
				UsuarioModificacion =@UsuarioRegistro
				WHERE IdDerivacion = @IdDerivacion

			--	SET @V_IdDerivacion = SCOPE_IDENTITY()

			

				INSERT INTO DerivacionMovimiento(IdDerivacion, IdEmision,Perfil,Estado,CodAgencia,FechaRegistro,UsuarioRegistro)
	     		VALUES (@IdDerivacion,@IdEmision,@Perfil,@ESTADO_EN_PROCESO_P,@CodAgencia,GETDATE(),@UsuarioRegistro)

				 SET @V_IdDerivacionMovimiento = SCOPE_IDENTITY()

				 UPDATE Emision SET
				Estado = @ESTADO_EN_PROCESO_OCM_G
				WHERE IdEmision = @IdEmision

			 
		END

		IF @EXISTE_COMENTARIO>0
		BEGIN 
			SET @V_IdDerivacionMovimiento = (SELECT TOP 1 IdDerivacionMovimiento FROM DerivacionMovimiento WHERE IdDerivacion = @IdDerivacion AND Estado = @Estado)
		END

	END

	INSERT INTO ComentarioEmision (IdEmision,IdDerivacion,IdDerivacionMovimiento,Texto,FechaRegistro,UsuarioRegistro,IdMotivo)
    VALUES (@IdEmision,@IdDerivacion,@V_IdDerivacionMovimiento,@Texto,GETDATE(),@UsuarioRegistro,@IdMotivo)

	 SET @IdComentarioEmision = SCOPE_IDENTITY()
END
GO


 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 23/11/2021
-- FECHA DE MODIFICACIÓN	: 
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_CREDITO_CLIENTE_OBTENER_DETALLE
-- FUNCIONALIDAD			: Obtene el detalle de un credito del cliente
--================================================================================================================================

DROP PROCEDURE IF EXISTS SP_CREDITO_CLIENTE_OBTENER_DETALLE
GO
CREATE PROCEDURE SP_CREDITO_CLIENTE_OBTENER_DETALLE
(
@IdEmision INT,
@Perfil VARCHAR(5)
)
AS
BEGIN

	 SELECT 
		
		PTP.Descripcion TipoProcesamiento,
		SE.Descripcion Estado,
		CC.TipoDocumento TipoDocumento,
		CC.NumeroDocumento,
		CC.Nombres,
		CC.Agencia,
		CC.AnalistaProponente,
		E.IdEmision,
		D.IdDerivacion,
		DM.IdDerivacionMovimiento,
		CC.CodAgencia,
		D.Estado IdEstado,
		D.Perfil,
		CC.IdCreditoCliente,
		R.LaserficheID IdLaserficheCNA,
		RCR.LaserficheID AS IdLaserficheCR,
		PP.Valor PerfilDerivado,
		E.FechaEntregaOlva,
		D.CodAgencia AS CodAgenciaDerivacion,
		RCE.LaserficheID AS IdLaserficheCE,
		RCE.FechaRegistro AS FechaRegistroCE,
		CC.Direccion,
		MN.Descripcion AS DesMedioNot,
		E.MedioNotificacion AS CodMedioNot,
		(CASE E.MedioNotificacion WHEN 2 THEN E.Email 
								  WHEN 3 THEN E.Whatsapp END) ValorMedioNot,
        CC.NumeroCredito AS NroCredito,
	    ( SELECT TOP 1 MR.Descripcion 
		  FROM ComentarioEmision CE INNER JOIN 
			   Parametro MR ON Grupo = '1900' AND MR.Parametro = CE.IdMotivo 
			                                  AND CE.IdEmision = E.IdEmision
		  ORDER BY CE.IdComentarioEmision DESC) AS MotivoRechazo,	
		( SELECT TOP 1 convert(varchar(10), CE.FechaRegistro, 103) 
		  FROM ComentarioEmision CE INNER JOIN 
			Parametro MR ON Grupo = '1900' AND MR.Parametro = CE.IdMotivo 
			                               AND CE.IdEmision = E.IdEmision
		  ORDER BY CE.IdComentarioEmision DESC) AS FechaRechazo	
	 FROM 
	 CreditoCliente CC WITH(NOLOCK)
	 INNER JOIN Emision E WITH(NOLOCK) ON E.IdCreditoCliente = CC.IdCreditoCliente
	 LEFT JOIN RecursoEmision RE  WITH(NOLOCK) ON RE.IdEmision = E.IdEmision AND RE.TipoRecurso = 1
	 LEFT JOIN Recurso R  WITH(NOLOCK) ON R.CodTabla = 8 AND R.ReferenciaID = RE.IdRecursoEmision
	 LEFT JOIN Derivacion D  WITH(NOLOCK) ON D.IdEmision = E.IdEmision
	 LEFT JOIN Parametro PP  WITH(NOLOCK) ON D.PerfilDerivado = PP.Descripcion AND PP.Grupo = 1400
	 LEFT JOIN EstadoEmision SE WITH(NOLOCK) ON D.Estado = SE.IdEstado -- AND SE.TipoEstado = 'G'
	 LEFT JOIN DerivacionMovimiento DM WITH(NOLOCK) ON DM.IdDerivacion = D.IdDerivacion
	 LEFT JOIN Parametro PTP  WITH(NOLOCK) ON PTP.Parametro = CC.TipoProcesamiento AND PTP.Grupo = 1500
	 LEFT JOIN RecursoEmision RER  WITH(NOLOCK) ON RER.IdEmision = E.IdEmision AND RER.TipoRecurso = 3
	 LEFT JOIN Recurso RCR  WITH(NOLOCK) ON RCR.CodTabla = 8 AND RCR.ReferenciaID = RER.IdRecursoEmision
	 LEFT JOIN RecursoEmision REE  WITH(NOLOCK) ON REE.IdEmision = E.IdEmision AND REE.TipoRecurso = 7
	 LEFT JOIN Recurso RCE  WITH(NOLOCK) ON RCE.CodTabla = 8 AND RCE.ReferenciaID = REE.IdRecursoEmision
	 LEFT JOIN Parametro MN  WITH(NOLOCK) ON MN.Parametro = E.MedioNotificacion AND MN.Grupo = 1700
 	 WHERE
	 E.IdEmision = @IdEmision AND
	 D.Perfil = @Perfil

END
GO


 

GO


 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 14/12/2021
-- FECHA DE MODIFICACIÓN	: 
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_CONFIGURACION_ACCION_GENERAR
-- FUNCIONALIDAD			: Obtiene las acciones segun la configuracion
--================================================================================================================================

DROP PROCEDURE IF EXISTS SP_CONFIGURACION_ACCION_GENERAR
GO
CREATE PROCEDURE SP_CONFIGURACION_ACCION_GENERAR
(
	@IdEstado INT,
	@Perfil VARCHAR(5) 
)
AS
BEGIN
	
	
	SELECT 
		CA.IdConfiguracionAccion,
		CA.IdAccion,
		CA.Perfil,
		CA.IdEstado,
		CA.Activo,
		PA.Valor Metodo ,
		PA.Descripcion Nombre,
		PA.Referencia2 Tipo,
		A.TipoAccion
	FROM ConfiguracionAccion CA WITH(NOLOCK)
	INNER JOIN Accion A  WITH(NOLOCK) ON CA.IdAccion = A.IdAccion
	LEFT JOIN Parametro PA WITH(NOLOCK) ON CA.IdAccion = CAST(PA.Referencia AS INTEGER) AND PA.Grupo = 2400
	WHERE
		((@Perfil IS NULL AND Perfil IS NULL) OR (@Perfil IS NOT NULL AND Perfil = @Perfil)) AND
		((@IdEstado IS NULL AND IdEstado IS NULL) OR (@IdEstado IS NOT NULL AND IdEstado = @IdEstado)) AND
		Activo = 1
	ORDER BY PA.Orden
END
GO

 
DROP PROCEDURE IF EXISTS dbo.SP_USUARIO_ACTUALIZAR
GO
 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 08/07/2021
-- FECHA DE MODIFICACIÓN	: 02/12/2021 
-- CREADO POR				: William Quiroz Salazar 
-- MODIFICADO POR			: Karina Torres Cahuana
-- STORE PROCEDURE			: SP_USUARIO_ACTUALIZAR
-- FUNCIONALIDAD			: Actualizar los datos del usuario
--=================================================================================================================================
CREATE PROCEDURE dbo.SP_USUARIO_ACTUALIZAR  
@CodUsuario INT,  
@Perfil VARCHAR(5) ,  
@DescripcionPerfil VARCHAR(200),  
@CorreoElectronico VARCHAR(100),  
@UsuarioWeb  VARCHAR(50),  
@NombresApellidos VARCHAR(200),  
@UsuarioRegistro VARCHAR(50),  
@Estado INT,  
@CodAgencia VARCHAR(3) = NULL ,
@MensajeOutput VARCHAR(100) OUTPUT  
  
AS  
BEGIN  
  DECLARE @Mensaje VARCHAR(100)= ''    
  DECLARE @ExistePerfilOCM INT = 0  
  DECLARE @ExistePerfilACM INT = 0  
  DECLARE @PerfilOCM VARCHAR(5)= 'OCM'  
  DECLARE @PerfilACM VARCHAR(5)= 'ACM'  
  DECLARE @EstadoActual INT = 0  
  DECLARE @ExisteUsuarioWeb INT = 0  
  
  SET @ExisteUsuarioWeb = (SELECT COUNT(UsuarioWeb) FROM Usuario WHERE UsuarioWeb =  @UsuarioWeb AND CodUsuario <> @CodUsuario )      
      
  IF (@PerfilOCM = @Perfil) 
  BEGIN  
     SET @ExistePerfilOCM = (SELECT COUNT(Perfil) FROM Usuario WHERE Perfil = @Perfil AND Estado = 1 AND CodUsuario<>@CodUsuario)    
  END  
    
  IF @PerfilACM = @Perfil  
  BEGIN  
     SET @ExistePerfilACM = (SELECT COUNT(Perfil) FROM Usuario WHERE Perfil = @Perfil AND CodUsuario<>@CodUsuario AND Estado = 1 )    
  END     
  
  IF @ExistePerfilOCM > 0   
  BEGIN  
     SET @Mensaje = 'Ya existe un usuario con perfil '+@Perfil+' en estado Activo'  
  END  

  --IF @ExistePerfilACM > 0   
  --BEGIN  
  --   SET @Mensaje = 'Ya existe un usuario con perfil ACM en estado Activo'  
  --END  
  
  IF @ExisteUsuarioWeb > 0  
  BEGIN  
     SET @Mensaje = 'El usuario web ya existe'  
  END  
  
 IF( @Mensaje = '' )  
 BEGIN       
   UPDATE Usuario  
   SET Perfil = @Perfil  
    ,DescripcionPerfil = @DescripcionPerfil  
    ,CorreoElectronico = @CorreoElectronico  
    ,UsuarioWeb = @UsuarioWeb  
    ,NombresApellidos = @NombresApellidos  
    ,UsuarioModificacion = @UsuarioRegistro  
    ,FechaModificacion = GETDATE()  
    ,Estado = @Estado  
	,CodAgencia = (CASE WHEN @CodAgencia IS NULL OR @CodAgencia = 0 THEN NULL ELSE @CodAgencia END)
   WHERE CodUsuario = @CodUsuario      
    
   SET @Mensaje = 'Se actualizó correctamente el usuario.'  
  END 
   SET @MensajeOutput = @Mensaje 
   
END  
GO

DROP PROCEDURE IF EXISTS dbo.SP_USUARIO_OBTENER_DETALLE
GO
 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 08/07/2021
-- FECHA DE MODIFICACIÓN	: 02/12/2021 
-- CREADO POR				: William Quiroz Salazar 
-- MODIFICADO POR			: Karina Torres Cahuana
-- STORE PROCEDURE			: SP_USUARIO_OBTENER_DETALLE
-- FUNCIONALIDAD			: Obtener la lista de usuario
--=================================================================================================================================  
CREATE PROCEDURE dbo.SP_USUARIO_OBTENER_DETALLE  
@CodUsuario INT  
AS  
BEGIN  
  
 SELECT US.CodUsuario  
    ,PRP.Parametro  AS CodPerfil   
    ,US.DescripcionPerfil  
    ,US.CorreoElectronico  
    ,US.UsuarioWeb  
    ,US.NombresApellidos      
    ,(CASE WHEN US.Estado = 1 THEN '1' ELSE '0' END) AS Estado  
    ,US.UsuarioRegistro   
	,US.CodAgencia
 FROM   Usuario US   
       LEFT JOIN Parametro PRP ON PRP.Descripcion = US.Perfil AND PRP.Grupo = 1400  
 WHERE (ISNULL(@CodUsuario,'') = '' OR US.CodUsuario = @CodUsuario)   
END  
GO

DROP PROCEDURE IF EXISTS dbo.SP_USUARIO_REGISTRAR
GO
 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 08/07/2021
-- FECHA DE MODIFICACIÓN	: 02/12/2021 
-- CREADO POR				: William Quiroz Salazar 
-- MODIFICADO POR			: Karina Torres Cahuana
-- STORE PROCEDURE			: SP_USUARIO_REGISTRAR
-- FUNCIONALIDAD			: Registrar los datos del usuario
--=================================================================================================================================  
CREATE PROCEDURE dbo.SP_USUARIO_REGISTRAR 
@Perfil VARCHAR(5) ,  
@DescripcionPerfil VARCHAR(200),  
@CorreoElectronico VARCHAR(100),  
@UsuarioWeb  VARCHAR(50),  
@NombresApellidos VARCHAR(200),  
@UsuarioRegistro VARCHAR(50),  
@CodAgencia VARCHAR(3)  = NULL , 
@MensajeOutput VARCHAR(100) OUTPUT  
  
AS  
BEGIN  
  DECLARE @Mensaje VARCHAR(100)= ''   
  DECLARE @ExisteUsuarioWeb INT = 0  
  DECLARE @ExistePerfilOCM INT = 0  
  DECLARE @PerfilOCM VARCHAR(5)= 'OCM'  
  DECLARE @PerfilACM VARCHAR(5)= 'ACM'

  DECLARE @UsuarioActivo INT = 0  
  
  SET @ExisteUsuarioWeb = (SELECT COUNT(UsuarioWeb) FROM Usuario WHERE UsuarioWeb = @UsuarioWeb)  
  
  SET @UsuarioActivo = (SELECT COUNT(Estado) FROM Usuario WHERE Perfil = @Perfil)  
  
  IF (@PerfilOCM = @Perfil) 
  BEGIN  
     SET @ExistePerfilOCM = (SELECT COUNT(Perfil) FROM Usuario WHERE Perfil = @Perfil AND Estado = 1)  
  END  
  
  IF @ExistePerfilOCM > 0  
  BEGIN  
     SET @Mensaje = 'Ya existe un usuario con perfil '+@Perfil+' en estado Activo'  
  END  
    
  IF @ExisteUsuarioWeb > 0  
  BEGIN  
     SET @Mensaje = 'El usuario web ya existe'  
  END  
  
  IF( @Mensaje = '' )  
  BEGIN     
	  INSERT INTO Usuario(Perfil  
		  ,DescripcionPerfil  
		  ,CorreoElectronico          
		  ,UsuarioWeb  
		  ,NombresApellidos  
		  ,Estado  
		  ,UsuarioRegistro           
		  ,FechaRegistro 
		  ,CodAgencia
		 )  
	   VALUES (@Perfil  
		  ,@DescripcionPerfil  
		  ,@CorreoElectronico           
		  ,@UsuarioWeb  
		  ,@NombresApellidos  
		  ,1         
		  ,@UsuarioRegistro  
		  , GETDATE()
		  ,(CASE WHEN @CodAgencia IS NULL OR @CodAgencia = 0 THEN NULL ELSE @CodAgencia END))  
  
	   SET @Mensaje = 'Se registró correctamente el usuario.'  
  END    

  SET @MensajeOutput = @Mensaje     
  
END  
GO

 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 14/12/2021
-- FECHA DE MODIFICACIÓN	: 
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_CONFIGURACION_DERIVACION_OBTENER
-- FUNCIONALIDAD			: Obtiene las acciones segun la configuracion
--================================================================================================================================
DROP PROCEDURE IF EXISTS SP_CONFIGURACION_DERIVACION_OBTENER
GO
CREATE PROCEDURE SP_CONFIGURACION_DERIVACION_OBTENER 
(
	@IdConfiguracionAccion INT,
	@GrupoFiltro INT = NULL
)
AS
BEGIN

		SELECT
			CD.IdConfiguracionAccion,
			CD.GrupoFiltro,
			CD.TipoProcesamiento,
			CD.TipoEvento,
			CD.Perfil,
			CD.IdEstado,
			CD.IdConfiguracionDerivacion
		FROM ConfiguracionDerivacion  CD
		WHERE IdConfiguracionAccion = @IdConfiguracionAccion AND
		((@GrupoFiltro IS NULL AND CD.GrupoFiltro IS NULL) OR (@GrupoFiltro IS NOT NULL AND CD.GrupoFiltro = @GrupoFiltro)) 

END
GO

 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 14/12/2021
-- FECHA DE MODIFICACIÓN	: 
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_DERIVACION_INSERTAR
-- FUNCIONALIDAD			: Realiza un registro en la tabla derivacion
--================================================================================================================================

DROP PROCEDURE IF EXISTS SP_DERIVACION_INSERTAR
GO
CREATE PROCEDURE SP_DERIVACION_INSERTAR
(
	@IdEmision INT,
	@Perfil VARCHAR(5)  = NULL,
	@Estado INT  = NULL,
	@PerfilDerivado VARCHAR(5)  = NULL,
	@CodUsuarioDerivado INT = NULL,
	@FechaDerivado DATETIME = NULL,
	@CodAgencia INT  = NULL,
	@UsuarioRegistro VARCHAR(50) 
	
)
AS
BEGIN
	DECLARE @IdDerivacion INT = 0
	DECLARE @CodAgenciaVariable INT = NULL

	DECLARE @EXISTE INT = 0


	SET @EXISTE = (SELECT COUNT(IdDerivacion) FROM Derivacion WHERE IdEmision = @IdEmision AND Perfil = @Perfil)

	IF @EXISTE>0
	BEGIN
		 --UPDATE Derivacion SET
		 --Estado = @Estado
		 --WHERE IdEmision = @IdEmision AND Perfil = @Perfil
 
 		UPDATE D SET
			D.Estado = @Estado,
			D.PerfilDerivado =(CASE @PerfilDerivado WHEN D.Perfil THEN  NULL ELSE @PerfilDerivado END),
			D.FechaDerivado = (CASE WHEN @PerfilDerivado IS NULL OR @PerfilDerivado = '' OR @PerfilDerivado =  D.Perfil  THEN NULL ELSE GETDATE() END),
			--D.CodAgencia = (CASE WHEN @CodAgencia IS NULL OR @CodAgencia = 0 THEN D.CodAgencia ELSE @CodAgencia END),
			@CodAgenciaVariable = (CASE WHEN D.CodAgencia IS NULL 
								THEN NULL 
								WHEN D.CodAgencia IS NOT NULL 
								THEN (CASE WHEN @CodAgencia IS NULL OR @CodAgencia = 0 THEN D.CodAgencia ELSE @CodAgencia END)
							END),
			D.CodAgencia = (CASE WHEN D.CodAgencia IS NULL 
								THEN NULL 
								WHEN D.CodAgencia IS NOT NULL 
								THEN (CASE WHEN @CodAgencia IS NULL OR @CodAgencia = 0 THEN D.CodAgencia ELSE @CodAgencia END)
							END),
			D.CodUsuarioDerivado = (CASE WHEN @CodUsuarioDerivado IS NULL
									THEN D.CodUsuarioDerivado
									ELSE
									@CodUsuarioDerivado END),
			D.FechaModificacion = GETDATE(),
			D.UsuarioModificacion = @UsuarioRegistro  
		FROM Derivacion D  
			INNER JOIN   ConfiguracionDerivacion CD ON D.Perfil = CD.Perfil
		WHERE 
			CD.Perfil = @Perfil   AND 
		--	CD.IdConfiguracionDerivacion = @IdConfiguracionDerivacion AND 
			D.IdEmision = @IdEmision -- AND 


		SET @IdDerivacion = (SELECT TOP 1 IdDerivacion FROM Derivacion WHERE IdEmision = @IdEmision AND Perfil = @Perfil)
	END
	ELSE
	BEGIN
		INSERT INTO Derivacion (IdEmision,Perfil,Estado,PerfilDerivado,FechaDerivado,CodAgencia,CodUsuarioDerivado,FechaRegistro,UsuarioRegistro)
				  VALUES (@IdEmision,@Perfil,@Estado,@PerfilDerivado,@FechaDerivado,@CodAgencia,@CodUsuarioDerivado,GETDATE(),@UsuarioRegistro)

		SET @IdDerivacion = @@IDENTITY
	END



	

	INSERT INTO DerivacionMovimiento(IdDerivacion,IdEmision,Perfil,Estado,PerfilDerivado,CodAgencia,CodUsuarioDerivado,FechaRegistro,UsuarioRegistro)
     VALUES (@IdDerivacion,@IdEmision,@Perfil,@Estado,@PerfilDerivado,@CodAgencia,@CodUsuarioDerivado,GETDATE(),@UsuarioRegistro)



END
GO

 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 14/12/2021
-- FECHA DE MODIFICACIÓN	: 
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_DERIVACION_ACTUALIZAR
-- FUNCIONALIDAD			: Realiza una actualización en la tabla derivacion
--================================================================================================================================

DROP PROCEDURE IF EXISTS SP_DERIVACION_ACTUALIZAR
GO
CREATE PROCEDURE SP_DERIVACION_ACTUALIZAR
(
	@IdDerivacion INT,
	@IdEmision INT  = NULL,
	@Perfil VARCHAR(5)  = NULL,
	@Estado INT  = NULL,
	@PerfilDerivado VARCHAR(5)  = NULL,
	@FechaDerivado DATETIME  = NULL,
	@CodAgencia INT  = NULL,
	@UsuarioModificacion VARCHAR(50)  = NULL,
	@IdConfiguracionDerivacion INT,
	@CodUsuarioDerivado INT = NULL
)
AS
BEGIN
		DECLARE @EXISTE_ESTADO INT  = 0
		DECLARE @CodAgenciaVariable INT = NULL
		DECLARE @USUARIO_DERIVADO INT = NULL

		SET  @EXISTE_ESTADO = 
		(
		 SELECT 
			COUNT(IdDerivacion)
		 FROM Derivacion D  
		 WHERE 
			D.Perfil = @Perfil   AND 
			D.Estado = @Estado AND
			D.IdEmision = @IdEmision
		)		

		UPDATE D SET
			D.Estado = @Estado,
			D.PerfilDerivado =(CASE @PerfilDerivado WHEN D.Perfil THEN  NULL ELSE @PerfilDerivado END),
			D.FechaDerivado = (CASE WHEN @PerfilDerivado IS NULL OR @PerfilDerivado = '' OR @PerfilDerivado =  D.Perfil  THEN NULL ELSE GETDATE() END),
			--D.CodAgencia = (CASE WHEN @CodAgencia IS NULL OR @CodAgencia = 0 THEN D.CodAgencia ELSE @CodAgencia END),
			@CodAgenciaVariable = (CASE WHEN D.CodAgencia IS NULL 
								THEN NULL 
								WHEN D.CodAgencia IS NOT NULL 
								THEN (CASE WHEN @CodAgencia IS NULL OR @CodAgencia = 0 THEN D.CodAgencia ELSE @CodAgencia END)
							END),
			D.CodAgencia = (CASE WHEN D.CodAgencia IS NULL 
								THEN NULL 
								WHEN D.CodAgencia IS NOT NULL 
								THEN (CASE WHEN @CodAgencia IS NULL OR @CodAgencia = 0 THEN D.CodAgencia ELSE @CodAgencia END)
							END),
			D.CodUsuarioDerivado = (CASE WHEN @CodUsuarioDerivado IS NULL
									THEN D.CodUsuarioDerivado
									ELSE
									@CodUsuarioDerivado END),
			D.FechaModificacion = GETDATE(),
			D.UsuarioModificacion = @UsuarioModificacion  
		FROM Derivacion D  
			INNER JOIN   ConfiguracionDerivacion CD ON D.Perfil = CD.Perfil
		WHERE 
			CD.Perfil = @Perfil   AND 
			CD.IdConfiguracionDerivacion = @IdConfiguracionDerivacion AND 
			D.IdEmision = @IdEmision -- AND 
		--	D.IdDerivacion =@IdDerivacion

		SET @USUARIO_DERIVADO = (SELECT CodUsuarioDerivado FROM Derivacion WHERE IdDerivacion = @IdDerivacion);


		IF @EXISTE_ESTADO = 0 
		BEGIN
			INSERT INTO DerivacionMovimiento(IdDerivacion,IdEmision,Perfil,Estado,PerfilDerivado,CodAgencia,CodUsuarioDerivado,FechaRegistro,UsuarioRegistro)
			VALUES (@IdDerivacion,@IdEmision,@Perfil,@Estado,@PerfilDerivado,@CodAgenciaVariable,@USUARIO_DERIVADO,GETDATE(),@UsuarioModificacion)
		END
END
GO


 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 14/12/2021
-- FECHA DE MODIFICACIÓN	: 
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_EMISION_INSERTAR
-- FUNCIONALIDAD			: Realiza un registro en la tabla emision
--================================================================================================================================
DROP PROCEDURE IF EXISTS SP_EMISION_INSERTAR
GO

CREATE PROCEDURE SP_EMISION_INSERTAR
(
	@IdCreditoCliente INT  = NULL,
	@Estado INT  = NULL,
	@TipoFlujo INT  = NULL,
	@FechaEntregaOlva DATETIME  = NULL,
	@MedioNotificacion INT  = NULL,
	@Email VARCHAR(100)  = NULL,
	@Whatsapp VARCHAR(100)  = NULL,
	@FechaCargoEntrega DATETIME  = NULL,
	@UsuarioRegistro VARCHAR(50)   = NULL
)
AS
BEGIN
	INSERT INTO Emision (IdCreditoCliente,Estado,TipoFlujo,FechaEntregaOlva,MedioNotificacion,Email,Whatsapp,FechaCargoEntrega,FechaRegistro,UsuarioRegistro) VALUES
		(@IdCreditoCliente,@Estado,@TipoFlujo,@FechaEntregaOlva,@MedioNotificacion,@Email,@Whatsapp,@FechaCargoEntrega,GETDATE(),@UsuarioRegistro)
END


GO

 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 14/12/2021
-- FECHA DE MODIFICACIÓN	: 15/12/2021
-- CREADO POR				: JOSE CAYCHO
-- MODIFICADO POR		    : KARINA TORRES
-- STORE PROCEDURE			: SP_EMISION_ACTUALIZAR
-- FUNCIONALIDAD			: Realiza una actualizacion en la tabla emision
--================================================================================================================================

DROP PROCEDURE IF EXISTS SP_EMISION_ACTUALIZAR
GO

CREATE PROCEDURE SP_EMISION_ACTUALIZAR
(
	@IdEmision INT  = NULL,
	@IdCreditoCliente INT  = NULL,
	@Estado INT  = NULL,
	@TipoFlujo INT  = NULL,
	@FechaEntregaOlva DATETIME  = NULL,
	@MedioNotificacion INT  = NULL,
	@Email VARCHAR(100)  = NULL,
	@Whatsapp VARCHAR(100)  = NULL,
	@FechaCargoEntrega DATETIME  = NULL,
	@UsuarioModificacion VARCHAR(50)  = NULL
)
AS
BEGIN
	UPDATE Emision SET 
	IdCreditoCliente = @IdCreditoCliente,
	Estado = @Estado,
	TipoFlujo = @TipoFlujo,
	FechaEntregaOlva = (CASE WHEN @FechaEntregaOlva IS NULL THEN FechaEntregaOlva ELSE @FechaEntregaOlva END),
	MedioNotificacion = (CASE WHEN @MedioNotificacion IS NULL THEN MedioNotificacion ELSE @MedioNotificacion END),
	Email =  (CASE WHEN @Email IS NULL THEN Email ELSE @Email END),
	Whatsapp = (CASE WHEN @Whatsapp IS NULL THEN Whatsapp ELSE @Whatsapp END),
	FechaCargoEntrega = @FechaCargoEntrega,
	FechaModificacion = GETDATE(),
	UsuarioModificacion = @UsuarioModificacion 
	WHERE IdEmision = @IdEmision

	INSERT INTO EmisionMovimiento(IdEmision, Estado, TipoFlujo, FechaRegistro, UsuarioRegistro)
	VALUES	(@IdEmision,@Estado,@TipoFlujo,GETDATE(),@UsuarioModificacion)

END


GO

 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 14/12/2021
-- FECHA DE MODIFICACIÓN	: 
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_FILTRO_ACCION_OBTENER
-- FUNCIONALIDAD			: Obtene un listado de la tabla FiltroAccion
--================================================================================================================================
DROP PROCEDURE IF EXISTS SP_FILTRO_ACCION_OBTENER
GO
CREATE PROCEDURE SP_FILTRO_ACCION_OBTENER
AS
BEGIN
	SELECT
	 FA.IdConfiguracionAccion,
	 FA.Grupo,
	 FA.TipoFiltro,
	 FA.Valor1,
	 FA.Valor2,
	 PP.Valor 
	FROM FiltroAccion FA WITH(NOLOCK)
	INNER JOIN Parametro PP WITH(NOLOCK) ON FA.Valor1 = PP.Descripcion AND PP.Grupo = 1400
END
GO
 

 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 14/12/2021
-- FECHA DE MODIFICACIÓN	: 
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_OBTENER_CONFIGURACION_ACCION
-- FUNCIONALIDAD			: Obtene un listado de la tabla ConfiguracionAccion por acción
--================================================================================================================================
DROP PROCEDURE IF EXISTS SP_OBTENER_CONFIGURACION_ACCION
GO

CREATE PROCEDURE SP_OBTENER_CONFIGURACION_ACCION
(
	@IdAccion INT
)
AS
BEGIN
	
	
	SELECT 
		CA.IdConfiguracionAccion,
		CA.IdAccion,
		CA.Perfil,
		CA.IdEstado,
		CA.Activo		
	FROM ConfiguracionAccion CA WITH(NOLOCK)
	INNER JOIN Accion A WITH(NOLOCK) ON CA.IdAccion = A.IdAccion
	WHERE Activo = 1
		AND CA.IdAccion = @IdAccion
END
GO

DROP PROCEDURE IF EXISTS SP_LISTAR_CONFIGURACION_DERIVACION
GO

CREATE PROCEDURE SP_LISTAR_CONFIGURACION_DERIVACION
(
	@IdConfiguracionAccion INT
)
AS
BEGIN
	
	SELECT
			CD.IdConfiguracionAccion,
			CD.GrupoFiltro,
			CD.TipoProcesamiento,
			CD.TipoEvento,
			CD.Perfil,
			CD.IdEstado
		FROM ConfiguracionDerivacion CD WITH(NOLOCK)
		WHERE IdConfiguracionAccion = @IdConfiguracionAccion
END
GO

DROP PROCEDURE IF EXISTS SP_LISTAR_FILTRO_ACCION
GO

CREATE PROCEDURE SP_LISTAR_FILTRO_ACCION
(
	@IdConfiguracionAccion INT
)
AS
BEGIN

	SELECT
	 FA.IdConfiguracionAccion,
	 FA.Grupo,
	 FA.TipoFiltro,
	 FA.Valor1,
	 FA.Valor2
	FROM FiltroAccion FA WITH(NOLOCK)
	WHERE FA.IdConfiguracionAccion = @IdConfiguracionAccion

END
GO

DROP PROCEDURE IF EXISTS SP_EMISION_INSERTAR_LISTA
GO

DROP PROCEDURE IF EXISTS SP_RECURSOEMISION_INSERT_LISTA
GO

DROP PROCEDURE IF EXISTS SP_DERIVACION_INSERTAR_LISTA
GO

IF TYPE_ID('UT_Emision') IS NOT NULL
BEGIN
	DROP TYPE UT_Emision
END
GO

CREATE TYPE UT_Emision AS TABLE
(
	IdCreditoCliente INT NOT NULL,
	Estado INT NOT NULL,
	TipoFlujo INT,
	IdLaserficheCNA INT,
	UsuarioRegistro VARCHAR(50) NOT NULL
)
GO

IF TYPE_ID('UT_Derivacion') IS NOT NULL
BEGIN
	DROP TYPE UT_Derivacion
END
GO

CREATE TYPE UT_Derivacion AS TABLE
(
	IdEmision INT NOT NULL,
	Perfil VARCHAR(5) NOT NULL,
	Estado INT,
	PerfilDerivado VARCHAR(5),
	FechaDerivado DATETIME,
	CodAgencia INT,
	UsuarioRegistro VARCHAR(50) NOT NULL
)
GO

IF TYPE_ID('UT_RecursoEmision') IS NOT NULL
BEGIN
	DROP TYPE UT_RecursoEmision
END
GO

CREATE TYPE UT_RecursoEmision AS TABLE
(
	IdEmision INT NOT NULL,
	TipoRecurso INT,
	UsuarioRegistro VARCHAR(50),
	IdLaserfiche INT NOT NULL,
	Descripcion VARCHAR(100)
)
GO

CREATE PROCEDURE SP_EMISION_INSERTAR_LISTA
(
	@ListaEmision UT_Emision READONLY
)
AS
BEGIN
	
	INSERT INTO Emision  
	(IdCreditoCliente, Estado, TipoFlujo, FechaEntregaOlva, MedioNotificacion, Email, Whatsapp, FechaCargoEntrega,FechaRegistro,UsuarioRegistro)
	SELECT LE.IdCreditoCliente, LE.Estado, LE.TipoFlujo, NULL, NULL, NULL, NULL, NULL, GETDATE(), LE.UsuarioRegistro
	FROM @ListaEmision LE

	INSERT INTO EmisionMovimiento
	(IdEmision, Estado, TipoFlujo, FechaRegistro, UsuarioRegistro)
	SELECT E.IdEmision, LE.Estado, LE.TipoFlujo, GETDATE(), LE.UsuarioRegistro
	FROM @ListaEmision LE
		INNER JOIN Emision E WITH(NOLOCK)
		ON LE.IdCreditoCliente = E.IdCreditoCliente

END
GO


CREATE PROCEDURE SP_RECURSOEMISION_INSERT_LISTA
(
	@ListaRecursoEmision UT_RecursoEmision READONLY
)
AS
BEGIN
	
	INSERT INTO RecursoEmision (TipoRecurso, IdEmision, FechaRegistro, UsuarioRegistro)
	SELECT
		LRE.TipoRecurso,
		LRE.IdEmision,
		GETDATE(),
		LRE.UsuarioRegistro
	FROM @ListaRecursoEmision LRE

	INSERT INTO Recurso (CodTabla, ReferenciaID, LaserficheID, Descripcion, Estado, FechaRegistro, UsuarioRegistro)

	SELECT DISTINCT
		8 AS CodTabla,
		RE.IdRecursoEmision,
		LRE.IdLaserfiche,
		LRE.Descripcion AS Descripcion,
		1,
		GETDATE(),
		LRE.UsuarioRegistro
	FROM @ListaRecursoEmision LRE
		INNER JOIN Emision E WITH(NOLOCK)
		ON LRE.IdEmision = E.IdEmision
		INNER JOIN RecursoEmision RE WITH(NOLOCK)
		ON E.IdEmision = RE.IdEmision
	WHERE LRE.IdLaserfiche NOT IN (SELECT LaserficheID FROM Recurso WHERE CodTabla = 8)
		AND RE.IdRecursoEmision NOT IN (SELECT ReferenciaID FROM Recurso WHERE CodTabla = 8)

END
GO

CREATE PROCEDURE SP_DERIVACION_INSERTAR_LISTA
(
	@ListaDerivacion UT_Derivacion READONLY
)
AS
BEGIN

	INSERT INTO Derivacion (IdEmision, Perfil, Estado, PerfilDerivado, FechaDerivado, CodAgencia, FechaRegistro, UsuarioRegistro)
	SELECT
		LD.IdEmision,
		LD.Perfil,
		LD.Estado,
		LD.PerfilDerivado,
		LD.FechaDerivado,
		LD.CodAgencia,
		GETDATE(),
		LD.UsuarioRegistro
	FROM @ListaDerivacion LD

	INSERT INTO DerivacionMovimiento (IdDerivacion, IdEmision, Perfil, Estado, PerfilDerivado, CodAgencia, FechaRegistro, UsuarioRegistro)
	SELECT
		D.IdDerivacion,
		D.IdEmision,
		D.Perfil,
		D.Estado,
		D.PerfilDerivado,
		D.CodAgencia,
		GETDATE(),
		LD.UsuarioRegistro
	FROM @ListaDerivacion LD
		INNER JOIN Derivacion D WITH(NOLOCK)
		ON LD.IdEmision = D.IdEmision
			AND LD.Perfil = D.Perfil 
			AND LD.Estado = D.Estado

END
GO

DROP PROCEDURE IF EXISTS SP_EJECUTAR_PROCESAMIENTO_AUTOMATICO
GO

CREATE PROCEDURE SP_EJECUTAR_PROCESAMIENTO_AUTOMATICO
AS
BEGIN
	
	DECLARE @IdAccion INT = 90 --Accion de procesamiento automático
	DECLARE @IdConfiguracionAccion INT
	DECLARE @ListaEmision AS UT_Emision
	DECLARE @ListaRecursoEmision AS UT_RecursoEmision
	DECLARE @ListaDerivacion AS UT_Derivacion

	DECLARE @TbConfiguracionAccion AS TABLE(
		IdConfiguracionAccion INT,
		IdAccion INT,
		Perfil VARCHAR(5),
		IdEstado INT,
		Activo BIT	
	)
	
	DECLARE @TbConfiguracionDerivacion AS TABLE
	(
		IdConfiguracionAccion INT,
		GrupoFiltro INT,
		TipoProcesamiento INT,
		TipoEvento INT,
		Perfil VARCHAR(5),
		IdEstado INT
	)

	DECLARE @TbFiltroAccion AS TABLE
	(
		IdConfiguracionAccion INT,
		Grupo INT,
		TipoFiltro INT,
		Valor1 VARCHAR(200),
		Valor2 INT
	)
	
	INSERT INTO @TbConfiguracionAccion (IdConfiguracionAccion, IdAccion, Perfil, IdEstado, Activo)
	EXEC SP_OBTENER_CONFIGURACION_ACCION @IdAccion

	SELECT TOP 1 @IdConfiguracionAccion = IdConfiguracionAccion FROM @TbConfiguracionAccion

	INSERT INTO @TbConfiguracionDerivacion (IdConfiguracionAccion, GrupoFiltro, TipoProcesamiento, TipoEvento, Perfil, IdEstado)
	EXEC SP_LISTAR_CONFIGURACION_DERIVACION @IdConfiguracionAccion

	INSERT INTO @TbFiltroAccion (IdConfiguracionAccion, Grupo, TipoFiltro, Valor1, Valor2)
	EXEC SP_LISTAR_FILTRO_ACCION @IdConfiguracionAccion
		
	/*Tipo Evento 1: 'Registro Emisión'*/
	INSERT INTO @ListaEmision (IdCreditoCliente, Estado, TipoFlujo, IdLaserficheCNA, UsuarioRegistro)
	SELECT
		CC.IdCreditoCliente,
		CD.IdEstado,
		(CASE WHEN FA.Valor2 = 1 THEN NULL
			WHEN FA.Valor2 = 2 OR FA.Valor2 = 3  THEN 1
		END) AS TipoFlujo,
		CC.IdLaserficheCNA,
		'JOB PROC. AUTOMÁTICO' AS UsuarioRegistro
	FROM @TbConfiguracionDerivacion CD
		INNER JOIN @TbFiltroAccion FA
		ON (CD.IdConfiguracionAccion = FA.IdConfiguracionAccion 
		AND CD.GrupoFiltro = FA.Grupo)
		INNER JOIN CreditoCliente CC WITH(NOLOCK)
		ON CC.Procesado = 0 
			AND	CC.TipoProcesamiento = FA.Valor2
			--AND CC.NumeroCredito IN ('CRED001', 'CRED002', 'CRED003', 'CRED004', 'CRED005', 'CRED006')/*PARA TEST*/
	WHERE CD.TipoEvento = 1
	
	--SELECT * FROM @ListaEmision
	EXEC SP_EMISION_INSERTAR_LISTA @ListaEmision

	INSERT INTO @ListaRecursoEmision(IdEmision, TipoRecurso, UsuarioRegistro, IdLaserfiche, Descripcion)
	SELECT 
		E.IdEmision,
		1, /*Tipo CNA*/
		'JOB PROC. AUTOMÁTICO' AS UsuarioRegistro,
		LE.IdLaserficheCNA,
		'Carta de no adeudo'
	FROM @ListaEmision LE
		INNER JOIN Emision E WITH(NOLOCK)
		ON LE.IdCreditoCliente = E.IdCreditoCliente
	WHERE LE.IdLaserficheCNA IS NOT NULL

	--SELECT * FROM @ListaRecursoEmision
	EXEC SP_RECURSOEMISION_INSERT_LISTA @ListaRecursoEmision

	/*Tipo Evento 2: 'Registro Derivación'*/
	INSERT INTO @ListaDerivacion (IdEmision, Perfil, Estado, PerfilDerivado, FechaDerivado, CodAgencia, UsuarioRegistro)
	SELECT
		E.IdEmision,
		CD.Perfil,
		CD.IdEstado,
		(CASE 
			WHEN CD.Perfil = 'OCM' AND CD.IdEstado = 14 THEN NULL
			WHEN CD.Perfil = 'OCM' AND CD.IdEstado = 16 THEN 'GAR'
			WHEN CD.Perfil = 'OCM' AND CD.IdEstado = 17 THEN 'ADMAG'
			ELSE NULL
		END) AS PerfilDerivado,
		(CASE 
			WHEN CD.Perfil = 'OCM' AND CD.IdEstado = 14 THEN NULL
			WHEN CD.Perfil = 'OCM' AND CD.IdEstado = 16 THEN GETDATE()
			WHEN CD.Perfil = 'OCM' AND CD.IdEstado = 17 THEN GETDATE()
			ELSE NULL
		END) AS FechaDerivado,
		(CASE
			WHEN CD.Perfil IN ('OCM', 'LEG', 'GAR') THEN NULL
			ELSE CC.CodAgencia
		END) AS Agencia,
		'JOB PROC. AUTOMÁTICO' AS UsuarioRegistro
	FROM @TbConfiguracionDerivacion CD
		INNER JOIN @TbFiltroAccion FA
		ON (CD.IdConfiguracionAccion = FA.IdConfiguracionAccion 
		AND CD.GrupoFiltro = FA.Grupo)
		INNER JOIN CreditoCliente CC WITH(NOLOCK)
		ON CC.Procesado = 0 
			AND	CC.TipoProcesamiento = FA.Valor2
			--AND CC.NumeroCredito IN ('CRED001', 'CRED002', 'CRED003', 'CRED004', 'CRED005', 'CRED006')/*PARA TEST*/
		INNER JOIN @ListaEmision LE
		ON CC.IdCreditoCliente = LE.IdCreditoCliente
		INNER JOIN Emision E WITH(NOLOCK)
		ON LE.IdCreditoCliente = E.IdCreditoCliente
	WHERE CD.TipoEvento = 2
	
	--SELECT * FROM @ListaDerivacion
	EXEC SP_DERIVACION_INSERTAR_LISTA @ListaDerivacion

	/*Actualización de Procesado*/
	UPDATE CreditoCliente
	SET Procesado = 1,
		FechaModificacion = GETDATE(),
		UsuarioModificacion = 'JOB PROC. AUTOMÁTICO'
	WHERE Procesado = 0
		AND IdCreditoCliente IN (SELECT IdCreditoCliente FROM @ListaEmision)

END
GO


--=================================================================================================================================
-- FECHA DE CREACIÓN		: 14/12/2021
-- FECHA DE MODIFICACIÓN	: 
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_RECURSO_EMISION_OBTENER
-- FUNCIONALIDAD			: OBTIENE UN LISTADO DE RECURSOS POR EMISION
--================================================================================================================================
DROP PROCEDURE IF EXISTS SP_RECURSO_EMISION_OBTENER
GO

CREATE PROCEDURE SP_RECURSO_EMISION_OBTENER
(
	@IdEmision INT
)
AS
BEGIN
		SELECT 
			R.CodRecurso,
			R.CodTabla,
			R.ReferenciaID,
			R.LaserficheID,
			RE.IdRecursoEmision,
			RE.IdEmision,
			PTR.Descripcion TipoRecurso,
			RE.TipoRecurso TipoRecursoID
		FROM Recurso R WITH(NOLOCK)
		INNER JOIN RecursoEmision RE WITH(NOLOCK) ON R.CodTabla = 8 AND R.ReferenciaID = RE.IdRecursoEmision
		LEFT JOIN Parametro PTR WITH(NOLOCK) ON PTR.Parametro = RE.TipoRecurso AND PTR.Grupo = 1800
		WHERE RE.IdEmision = @IdEmision
END
GO

--=================================================================================================================================
-- FECHA DE CREACIÓN		: 14/12/2021
-- FECHA DE MODIFICACIÓN	: 
-- CREADO POR				: JOSE CAYCHO
-- STORE PROCEDURE			: SP_RECURSO_EMISION_OBTENER
-- FUNCIONALIDAD			: REALIZA UN REGISTRO DE RECURSO DE MISION
--================================================================================================================================
DROP PROCEDURE IF EXISTS SP_RECURSO_EMISION_INSERTAR
GO

CREATE PROCEDURE SP_RECURSO_EMISION_INSERTAR
(
	@CodTabla INT,
	@LaserficheID INT,
	@TipoRecurso INT,
	@Descripcion VARCHAR(100),
	@IdEmision INT,
	@UsuarioRegistro VARCHAR(50)
)
AS

BEGIN
	DECLARE @IdRecursoEmision INT = 0
	INSERT INTO RecursoEmision (TipoRecurso,IdEmision,FechaRegistro,UsuarioRegistro) VALUES (@TipoRecurso,@IdEmision,GETDATE(),@UsuarioRegistro)

	SET @IdRecursoEmision = @@IDENTITY

	INSERT INTO Recurso (CodTabla,ReferenciaID,LaserficheID,Descripcion,Estado,UsuarioRegistro,FechaRegistro) VALUES (@CodTabla,@IdRecursoEmision,@LaserficheID,@Descripcion,1,@UsuarioRegistro,GETDATE())
END
GO

 
 

 --=================================================================================================================================
-- FECHA DE CREACIÓN		: 23/11/2021
-- FECHA DE MODIFICACIÓN	: 09/12/2021
-- CREADO POR				: JOSE CAYCHO
-- MODIFICADO POR			: KARINA TORRES CAHUANA
-- STORE PROCEDURE			: SP_COMENTARIO_EMISION_INSERTAR_ACCION
-- FUNCIONALIDAD			: Realiza un registro en la tabla ComentarioEmision
--================================================================================================================================

DROP PROCEDURE IF EXISTS SP_COMENTARIO_EMISION_INSERTAR_ACCION
GO
CREATE PROCEDURE SP_COMENTARIO_EMISION_INSERTAR_ACCION
(
	@IdEmision INT,
	@IdDerivacion INT = NULL,
	@Texto VARCHAR(300)  = NULL,
	@UsuarioRegistro VARCHAR(50),
	@Perfil VARCHAR(5),
	@CodAgencia INT,
	@Estado INT,
	@IdMotivo INT = NULL,
	@IdComentarioEmision INT OUTPUT
)
AS
BEGIN
	DECLARE @v_IdDerivacionMovimiento INT 
	SET @v_IdDerivacionMovimiento = (
		select 
			TOP 1
			IdDerivacionMovimiento 
		from DerivacionMovimiento
		where IdEmision = @IdEmision and 
		Perfil = @Perfil  
	)

	INSERT INTO ComentarioEmision (IdEmision,IdDerivacion,IdDerivacionMovimiento,Texto,FechaRegistro,UsuarioRegistro,IdMotivo)
    VALUES (@IdEmision,@IdDerivacion,@v_IdDerivacionMovimiento,@Texto,GETDATE(),@UsuarioRegistro,@IdMotivo)

	 SET @IdComentarioEmision = SCOPE_IDENTITY()
END
GO


DROP PROCEDURE IF EXISTS [dbo].[SP_USUARIO_OBTENER]
GO
---=================================================================================================================================
-- FECHA DE CREACIÓN		: 26/04/2021
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				: WILLIAM QUIROZ
-- Modificado POR			: Jose Caycho
-- STORE PROCEDURE			: [dbo].[SP_USUARIO_OBTENER]
-- PARAMETROS				:							  
-- FUNCIONALIDAD			: Obtener la lista de usuarios
--================================================================================================================================

CREATE PROCEDURE [dbo].[SP_USUARIO_OBTENER]	
@Perfil VARCHAR(5),
@UsuarioWeb  VARCHAR(20),
@Estado BIT = NULL
AS
BEGIN

	SELECT US.CodUsuario
		,US.UsuarioWeb
		,US.NombresApellidos
		,US.Perfil
		,US.CorreoElectronico
		,US.Estado
		,US.CodAgencia
	FROM Usuario US
	WHERE  ((@Perfil IS NULL OR @Perfil = '') OR (@Perfil IS NOT NULL AND US.Perfil = @Perfil))
		AND ((@UsuarioWeb IS NULL OR @UsuarioWeb = '') OR (@UsuarioWeb IS NOT NULL AND US.UsuarioWeb = @UsuarioWeb))
		AND ((@Estado IS NULL) OR (@Estado IS NOT NULL AND US.Estado = @Estado))
	ORDER BY US.CodUsuario

END
GO



DROP PROCEDURE IF EXISTS [dbo].[SP_CREEDITO_CLIENTE_OBTENER_DERIVACION_MASIVA_ADMAG]
GO
---=================================================================================================================================
-- FECHA DE CREACIÓN		:  
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				:  Jose Caycho
-- Modificado POR			: 
-- STORE PROCEDURE			: [dbo].[SP_CREEDITO_CLIENTE_OBTENER_DERIVACION_MASIVA_ADMAG]
-- PARAMETROS				:							  
-- FUNCIONALIDAD			: 
--================================================================================================================================


CREATE PROCEDURE [dbo].[SP_CREEDITO_CLIENTE_OBTENER_DERIVACION_MASIVA_ADMAG]
@Perfil VARCHAR(50),
@CodEstado VARCHAR(10),
@CodAgencia VARCHAR(10),
@NroDocumento VARCHAR(20),
@FechaCancelacionIni DATETIME,
@FechaCancelacionFin DATETIME,
@UsuarioWeb varchar(10)

AS
BEGIN

		IF (@CodAgencia ='' AND (@Perfil = 'ADMAG' OR @Perfil = 'UDA'))
		BEGIN
			SELECT @CodAgencia = CodAgencia 
			FROM   Usuario
			where  UsuarioWeb =  @UsuarioWeb
		END

 
	SELECT
	
		 CC.Nombres AS NombreApellidos,
		 CC.TipoDocumento TipoDocumento,
		 CC.NumeroDocumento
 
		,(CASE  WHEN  D.CodAgencia IS NULL THEN CC.Agencia  ELSE  AG.Oficina END ) Agencia
		,ES.Descripcion AS Estado
 
		
		,E.IdEmision,

 
		D.IdDerivacion,
 
		D.Estado IdEstado,
		D.Perfil,
		CC.IdCreditoCliente
 
		-- TP.Parametro  AS procesamiento
		--,TP.Descripcion AS procesamientoDesc
		, CC.CodAgencia
		--, D.CodAgencia AS Agenciaderivada
	FROM   
		CreditoCliente CC WITH(NOLOCK) 
		INNER JOIN Emision E WITH(NOLOCK) ON CC.IdCreditoCliente = E.IdCreditoCliente
		INNER JOIN Derivacion D WITH(NOLOCK) ON D.IdEmision = E.IdEmision
		INNER JOIN PARAMETRO TP WITH(NOLOCK) ON TP.Parametro = CC.TipoProcesamiento  AND TP.Grupo = 1500
		INNER JOIN ESTADOEMISION ES WITH(NOLOCK) ON ES.IdEstado = D.Estado AND ES.IdEstado IN (32,33)
		LEFT JOIN AGENCIA AG  WITH(NOLOCK) ON D.CodAgencia = AG.CodAgencia
	WHERE
			 ((@NroDocumento IS NULL OR @NroDocumento = '') OR CC.NumeroDocumento LIKE CAST(@NroDocumento AS VARCHAR)+'%')	
		AND  ((@CodEstado IS NULL OR @CodEstado = '') OR D.Estado = @CodEstado)
		AND  ((@Perfil IS NULL OR @Perfil = '') OR D.Perfil = @Perfil)	
		AND  ((@CodAgencia IS NULL OR @CodAgencia = '') OR (CASE  WHEN  D.CodAgencia IS NULL THEN CC.codAgencia  ELSE  D.CodAgencia END ) = CAST(@CodAgencia AS INT))
		AND ((@FechaCancelacionIni IS NULL OR @FechaCancelacionIni = '') OR CAST(CC.FechaCancelacion AS DATE) >= CAST(@FechaCancelacionIni AS DATE))
		AND ((@FechaCancelacionFin IS NULL OR @FechaCancelacionFin = '') OR CAST(CC.FechaCancelacion AS DATE) <= CAST(@FechaCancelacionFin AS DATE))
	 	AND  ((@CodAgencia IS NULL OR @CodAgencia = '') OR (CASE  WHEN  D.CodAgencia IS NULL THEN CC.codAgencia  ELSE  D.CodAgencia END ) = CAST(@CodAgencia AS INT))

		AND CC.TipoProcesamiento = 3
	ORDER BY 
		CAST(CC.FechaCancelacion AS DATE) DESC,
		(CASE  WHEN  D.CodAgencia IS NULL THEN CC.Agencia  ELSE  AG.Oficina END ),
		CC.Nombres


END
GO

DROP PROCEDURE IF EXISTS [dbo].[SP_CREDITO_CLIENTE_LISTAR_GENERACION_CNA]
GO
---=================================================================================================================================
-- FECHA DE CREACIÓN		:  03/01/2022
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				:  Miguel Güere Díaz
-- Modificado POR			: 
-- STORE PROCEDURE			: [dbo].[SP_CREDITO_CLIENTE_LISTAR_GENERACION_CNA]
-- PARAMETROS				:							  
-- FUNCIONALIDAD			: 
--================================================================================================================================

CREATE PROCEDURE [dbo].[SP_CREDITO_CLIENTE_LISTAR_GENERACION_CNA]
AS
BEGIN

	SELECT
		IdCreditoCliente,
		TipoDocumento,
		NumeroDocumento,
		Nombres,
		Direccion,
		Procesado,
		IdLaserficheCNA
	FROM CreditoCliente
	WHERE IdLaserficheCNA IS NULL
		AND Procesado = 0
	
END
GO


DROP PROCEDURE IF EXISTS SP_RECURSO_EMISION_ACTUALIZAR_TIPO_RECURSO
GO
---=================================================================================================================================
-- FECHA DE CREACIÓN		:  03/01/2022
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				:  Miguel Güere Díaz
-- Modificado POR			: 
-- STORE PROCEDURE			: [dbo].[SP_CREDITO_CLIENTE_LISTAR_GENERACION_CNA]
-- PARAMETROS				:							  
-- FUNCIONALIDAD			: 
--================================================================================================================================

CREATE PROCEDURE SP_RECURSO_EMISION_ACTUALIZAR_TIPO_RECURSO
@IdRecursoEmision INT,
@TipoRecurso INT
AS
BEGIN

	 UPDATE RecursoEmision SET
	 TipoRecurso = @TipoRecurso
	 WHERE IdRecursoEmision = @IdRecursoEmision
	
END
GO






DROP PROCEDURE IF EXISTS SP_CREDITO_CLIENTE_ACTUALIZAR_IDLASERFICHE
GO
---=================================================================================================================================
-- FECHA DE CREACIÓN		:  03/01/2022
-- FECHA DE MODIFICACIÓN	:
-- CREADO POR				:  Miguel Güere Díaz
-- Modificado POR			: 
-- STORE PROCEDURE			: [dbo].[SP_CREDITO_CLIENTE_LISTAR_GENERACION_CNA]
-- PARAMETROS				:							  
-- FUNCIONALIDAD			: 
--================================================================================================================================

CREATE PROCEDURE SP_CREDITO_CLIENTE_ACTUALIZAR_IDLASERFICHE
@IdCreditoCliente INT,
@IdLaserficheCNA INT
AS
BEGIN

	UPDATE CreditoCliente SET
	IdLaserficheCNA = @IdLaserficheCNA
	WHERE IdCreditoCliente = @IdCreditoCliente
	
END
go

DROP PROCEDURE IF EXISTS SP_AGENCIA_OBTENER
GO
---=================================================================================================================================  
-- FECHA DE CREACIÓN  : 26/04/2021  
-- FECHA DE MODIFICACIÓN :  18/01/2022
-- CREADO POR    : WILLIAM QUIROZ  
-- STORE PROCEDURE   : SP_AGENCIA_LISTAR  
-- PARAMETROS    :           
-- FUNCIONALIDAD   : Obtener la lista de agencias para reclamos 
--==================================================================================================================================  
  
CREATE PROCEDURE SP_AGENCIA_OBTENER  
 
AS  
BEGIN  
  
 SELECT CodAgencia  
  ,Oficina  
  ,Direccion  
 FROM Agencia  
 WHERE 
 FlagAgencia IS NULL
 ORDER BY codAgencia  
  
END  

GO
 