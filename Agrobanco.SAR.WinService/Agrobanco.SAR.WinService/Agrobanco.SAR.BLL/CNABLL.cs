﻿using Agrobanco.SAR.DAL;
using Agrobanco.SAR.DAL.Entities;
using Agrobanco.SAR.DAL.GeneradorDocumento;
using Agrobanco.SAR.DAL.Laserfiche;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SAR.BLL
{
    public class CNABLL
    {
        private readonly CNADA cnaDA;

        public CNABLL()
        {
            cnaDA = new CNADA();
        }

        public List<CreditoCliente> ListarCreditoClienteGeneracionCNA()
        {
            return cnaDA.ListarCreditoClienteGeneracionCNA();
        }

        public List<Documento> ConsultarDocumentoLista(string codigoLaserAnidado)
        {
            LaserficheProxy laserfiche = new LaserficheProxy();
            return laserfiche.ConsultarDocumentoLista(codigoLaserAnidado);  
        }
        public string GeneradorGestionCNA(string ArchivoBytes, CreditoCliente oCreditoCliente, List<Parametros> lstParametro)
        {
            GeneradorDocumento oGeneradorDocumento = new GeneradorDocumento();
            return oGeneradorDocumento.GeneradorGestionCNA(ArchivoBytes, oCreditoCliente, lstParametro); ;
        }
        public List<Parametros> ObtenerListaParametro(int Grupo)
        {
            return cnaDA.ListarParametro(Grupo);
        }

        public bool ActualizarCreditoClienteGeneracionCNA(CreditoCliente oCreditoCliente)
        {
            return cnaDA.ActualizarCreditoClienteGeneracionCNA(oCreditoCliente);
        }
    }
}
