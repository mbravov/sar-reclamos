﻿using Agrobanco.Control.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.Control.Security
{
    public interface IAuthAccess
    {
        AccessDTO getAccess();
    }
}
