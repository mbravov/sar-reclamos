﻿using Agrobanco.SAR.Datos;
using Agrobanco.SAR.Entidades.CNA_Minuta;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SAR.LogicaNegocio
{
    public class RecursoEmisionLogica
    {
        private readonly RecursoEmisionDatos recursoEmisionDatos;

        public RecursoEmisionLogica()
        {
            recursoEmisionDatos = new RecursoEmisionDatos();
        }

        public async Task<bool> Insertar(RecursoEmisionBE request)
        {
            return await recursoEmisionDatos.Insertar(request);
        }
        public async Task<List<RecursoEmisionBE>> Obtener(RecursoEmisionBE request)
        {
            return await recursoEmisionDatos.Obtener(request);
        }

    }
}
