namespace Agrobanco.LaserFiche.API.Models
{
    public class DocumentModel
    {
        public int CodigoLaserfiche { get; set; }
        public string CodigoLaserficheAnidados { get; set; }
        public string ArchivoBytes { get; set; }
        public string Extension { get; set; }
        public string Nombre { get; set; }
        public string NombreDocumento { get; set; }
        public string Folder { get; set; }
        public string Tamanio { get; set; }
        public string ContentType { get; set; }
        public bool ResultadoOK { get; set; }
    }

}