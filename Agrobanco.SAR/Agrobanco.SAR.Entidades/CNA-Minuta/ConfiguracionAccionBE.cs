﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades.CNA_Minuta
{
    public class ConfiguracionAccionBE
    {
        public int IdEstado { get; set; }
        public string Perfil { get; set; }
        public int IdConfiguracionAccion { get; set; }
        public int IdAccion { get; set; }
        public bool Activo { get; set; }
        public string Metodo { get; set; }
        public string BotonNombre { get; set; }

        public int Tipo { get; set; }
        public int TipoAccion { get; set; }
    }
}
