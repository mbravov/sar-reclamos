﻿using Agrobanco.SAR.API.Models;
using Agrobanco.SAR.Comun;
using Agrobanco.SAR.Control.Filters;
using Agrobanco.SAR.Entidades;
using Agrobanco.SAR.LogicaNegocio;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.SAR.API.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/agrobanco/reclamos")]
    [ApiController]
    public class AgrReclamoController : ControllerBase
    {
        private readonly UbigeoLogica ubigeoLogica;
        private readonly RequerimientoLogica requerimientoLogica;
        private readonly AgenciaLogica agenciaLogica;
        private readonly ParametroLogica parametroLogica;
        private readonly RecursoLogica recursoLogica;
        private readonly ConsultaLogica consultaLogica;

        private IWebHostEnvironment environment;
        public AgrReclamoController(IWebHostEnvironment _environment)
        {
            ubigeoLogica = new UbigeoLogica();
            requerimientoLogica = new RequerimientoLogica();
            agenciaLogica = new AgenciaLogica();
            parametroLogica = new ParametroLogica();
            recursoLogica = new RecursoLogica();
            consultaLogica = new ConsultaLogica();

            this.environment = _environment;
        }

        [HttpPost("departamento")]
        [ProducesResponseType(200, Type = typeof(List<UbigeoBE>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult Departamento([Required][FromHeader(Name = "Token")] string token)
        {
            try
            {

                var listaResultado = ubigeoLogica.ListarDepartamento();

                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("provincia")]
        [ProducesResponseType(200, Type = typeof(List<UbigeoBE>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult Provincia([Required][FromHeader(Name = "Token")] string token, [FromBody] UbigeoBE oBE)
        {
            try
            {
                var listaResultado = ubigeoLogica.ListarProvincia(oBE);
                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("distrito")]
        [ProducesResponseType(200, Type = typeof(List<UbigeoBE>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult Distrito([Required][FromHeader(Name = "Token")] string token, [FromBody] UbigeoBE oBE)
        {
            try
            {
                var listaResultado = ubigeoLogica.ListarDistrito(oBE);
                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost("requerimiento")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult Requerimiento([Required][FromHeader(Name = "Token")] string token, [FromBody] RequerimientoBE oRequerimientoBE)
        {
            ResponseSuccess oResponseSuccess = new ResponseSuccess();
            ResponseSuccessDetalle oResponseSuccessDetalle = new ResponseSuccessDetalle();
            ResponseErrorDetalle oResponseErrorDetalle = new ResponseErrorDetalle();
            try
            {
                string NroRequerimiento = string.Empty;
                var Resultado = requerimientoLogica.Registrar(oRequerimientoBE, out NroRequerimiento);

                oResponseSuccessDetalle.codigo = NroRequerimiento;
                oResponseSuccessDetalle.resultado = Resultado;
                oResponseSuccess.success = oResponseSuccessDetalle;


                return StatusCode(200, oResponseSuccess);
            }
            catch (Exception ex)
            {
                oResponseErrorDetalle.codigo = string.Empty;
                oResponseErrorDetalle.resultado = false;
                oResponseErrorDetalle.mensaje = ex.Message;

                return StatusCode(100, oResponseErrorDetalle);
            }
        }

        [HttpPost("requerimiento/detalle")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult RequerimientoDetalle([Required][FromHeader(Name = "Token")] string token, [FromBody] RequerimientoBE oRequest)
        {
            try
            {
                ResponseSuccess oResponseSuccess = new ResponseSuccess();
                var Resultado = requerimientoLogica.RequerimientoDetalle(oRequest);
                return StatusCode(200, Resultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("agencia")]
        [ProducesResponseType(200, Type = typeof(List<AgenciaBE>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult Agencia([Required][FromHeader(Name = "Token")] string token)
        {
            try
            {

                var listaResultado = agenciaLogica.ObtenerListarAgencias();

                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("parametros")]
        [ProducesResponseType(200, Type = typeof(List<ParametroBE>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult ParametroGrupo([Required][FromHeader(Name = "Token")] string token, [FromBody] Parametro oParametro)
        {
            try
            {

                var listaResultado = parametroLogica.ObtenerListarParametroGrupo(oParametro.strGrupo);

                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("recurso")]
        [ProducesResponseType(200, Type = typeof(List<RecursoBE>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult recurso([Required][FromHeader(Name = "Token")] string token, [FromBody] RecursoBE oBE)
        {
            try
            {
                var listaResultado = recursoLogica.Listar(oBE);
                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost("requerimiento/reenviar/correo")]
        //[HttpPost("parametro")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult RequerimientoEnvio([Required][FromHeader(Name = "Token")] string token, [FromBody] RequerimientoBE oRequerimientoBE)
        {
            ResponseSuccess oResponseSuccess = new ResponseSuccess();
            ResponseSuccessDetalle oResponseSuccessDetalle = new ResponseSuccessDetalle();
            ResponseErrorDetalle oResponseErrorDetalle = new ResponseErrorDetalle();
            try
            {
                EnvioCorreo oEnviarCorreo = new EnvioCorreo();
                List<string> listaCorreo = new List<string>();
                listaCorreo.Add(oRequerimientoBE.CorreoElectronico);

                List<string> listaCorreocopia = new List<string>();

                var listado = requerimientoLogica.RequerimientoDetalle(oRequerimientoBE);
                string cuerpocorreo = string.Empty;
                bool resultado = false;
                string webRootPath = environment.ContentRootPath;
                cuerpocorreo = EnvioCorreoCuerpo.CuerpoCorreoRegistrarDocumento(listado, webRootPath);
                resultado = oEnviarCorreo.EnviarCorreo(listaCorreo, listaCorreocopia, cuerpocorreo, ApplicationKeys.CorreoAsunto);

                oResponseSuccessDetalle.resultado = resultado;
                oResponseSuccess.success = oResponseSuccessDetalle;

                return StatusCode(200, oResponseSuccess);
            }
            catch (Exception ex)
            {
                oResponseErrorDetalle.codigo = string.Empty;
                oResponseErrorDetalle.resultado = false;
                oResponseErrorDetalle.mensaje = ex.Message;

                return StatusCode(100, oResponseErrorDetalle);
            }
        }

        [HttpPost("consultaregistrar")]
        [ProducesResponseType(200, Type = typeof(List<Usuario>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult ConsultaRegistrar([Required][FromHeader(Name = "Token")] string token, [FromBody] ConsultaBE oConsulta)
        {
            string webRootPath = environment.ContentRootPath;
            try
            {

                var listaResultado = consultaLogica.ConsultaRegistrar(oConsulta, webRootPath);

                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost("preguntasfrecuentes")]
        [ProducesResponseType(200, Type = typeof(List<ParametroBE>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult PreguntasFrecuentes([Required][FromHeader(Name = "Token")] string token, [FromBody] Parametro oParametro)
        {
            try
            {

                var listaResultado = parametroLogica.PreguntasFrecuentes(oParametro.strGrupo);

                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
