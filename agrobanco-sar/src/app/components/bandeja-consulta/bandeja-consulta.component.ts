import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ConsultaInterface } from '../models/consulta.interface';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { RequerimientoService } from 'src/app/services/requerimiento.service';
import { Router } from "@angular/router";
import { forkJoin} from 'rxjs';

@Component({
  selector: 'app-bandeja-consulta',
  templateUrl: './bandeja-consulta.component.html',
  styleUrls: ['./bandeja-consulta.component.css']
})
export class BandejaConsultaComponent implements OnInit {

  busquedaFiltro! : FormGroup;
  ELEMENT_DATA! : ConsultaInterface[];
  displayedColumns: string[] = ["codConsulta", "fechaRegistro", "tipoDocumento","nroDocumento", "nombresCompletos","editar"];

  listaConsulta = new MatTableDataSource<ConsultaInterface>(this.ELEMENT_DATA);
  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;

  fechaRegistroInicioValor! : Date;
  fechaRegistroFinValor! : Date;
  fechaAtencionInicioValor!: Date;
  fechaAtencionFinValor!: Date;

  loading!:boolean;

  constructor(
    private requerimientoService : RequerimientoService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.inicializarFormulario();
      this.loadPage();
      this.listaConsulta.paginator = this.paginator;
  }

  loadPage(): void {

    //this.loading = false;
    let params = this.busquedaFiltro.value;
    console.log(params);

    forkJoin([
      this.requerimientoService.ObtenerConsultaLista(params)

    ]).subscribe( (data:any) => {

        console.log("consul",data);
        this.listaConsulta.data = data[0].response as ConsultaInterface[];

        //this.loading = true;
    }, (error) =>{
      console.log(error);
      //this.loading = true;
    });

  }

  private inicializarFormulario()
  {

    let fechaInicioMes = new Date(new Date().getFullYear(), new Date().getMonth(), 1);
    let fechaHoy = new Date();

    this.fechaRegistroInicioValor = fechaInicioMes;
    this.fechaRegistroFinValor = fechaHoy;
    this.fechaAtencionInicioValor = fechaHoy;
    this.fechaAtencionFinValor = fechaHoy;

    this.busquedaFiltro = new FormGroup({
      IdConsulta : new FormControl(''),
      NroDocumento : new FormControl(''),
      NombresApellidos : new FormControl(''),
      CorreoElectronico : new FormControl(''),
      Fecharegistroini: new FormControl(fechaInicioMes, [Validators.max(this.fechaRegistroFinValor.getTime())]),
      Fecharegistrofin: new FormControl(fechaHoy, [Validators.min(this.fechaRegistroInicioValor.getTime())]),
      Fechaatencionini: new FormControl('', [Validators.max(this.fechaAtencionFinValor.getTime())]),
      Fechaatencionfin: new FormControl('', [Validators.min(this.fechaAtencionInicioValor.getTime())]),
      NroCelular : new FormControl('')

    });
  }

  buscarListaConsultas():void
  {

    if(this.busquedaFiltro.invalid)
    {
      return;
    }

    this.loading = true;
    let params = this.busquedaFiltro.value;
    console.log(params);

    this.requerimientoService.ObtenerConsultaLista(params).subscribe((result:any) =>{
      this.listaConsulta.data = result.response as ConsultaInterface[];
      this.loading = false;


    }, (error)=> {
      console.log(error);
      //this.loading = true;
    });


  }

  editarConsulta(codConsulta: string):void  {

    debugger;
    //this.busquedaFiltro.controls['CodConsulta'].setValue(codConsulta);
    this.router.navigate(['/consulta/consulta-detalle', codConsulta]);

  }

  onChangeFechaRegistroIni(){
    this.fechaRegistroInicioValor = this.busquedaFiltro.controls['Fecharegistroini'].value;
  }

  onChangeFechaRegistroFin(){
    this.fechaRegistroFinValor = this.busquedaFiltro.controls['Fecharegistrofin'].value;
  }

  onChangeFechaAtencionIni(){
    this.fechaAtencionInicioValor = this.busquedaFiltro.controls['Fechaatencionini'].value;
  }

  onChangeFechaAtencionFin(){
    this.fechaAtencionFinValor = this.busquedaFiltro.controls['Fechaatencionfin'].value;
  }

}
