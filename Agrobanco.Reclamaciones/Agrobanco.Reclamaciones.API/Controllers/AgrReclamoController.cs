﻿using Agrobanco.Control.Util;
using Agrobanco.Reclamaciones.API.Models;
using Agrobanco.Reclamaciones.API.Providers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Agrobanco.Reclamaciones.API.Comun;
using Agrobanco.Control.Filter;
using Agrobanco.Control.Security;
using Agrobanco.Control.DTO;

namespace Agrobanco.Reclamaciones.API.Controllers
{
    [Route("api/agrobanco/reclamos")]
    [ApiController]
    public class AgrReclamoController : ControllerBase
    {

        static string AppKey = Control.Util.Util.ObtenerValueFromKey("AppKey");
        static string AppCode = Control.Util.Util.ObtenerValueFromKey("AppCode");

        [HttpPost("departamento")]
        [ProducesResponseType(200, Type = typeof(List<UbigeoBE>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object Departamento([Required][FromHeader(Name = "Token")] string Token)
        {
            try
            {
                ProxySARAPI proxi = new ProxySARAPI();
                var authorization = proxi.TokenSARApi(GetAccess(), AppKey, AppCode, out string authorizationResponse);
                var response = proxi.Departamento(authorizationResponse);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return ex;
            }
        }

        [HttpPost("provincia")]
        [ProducesResponseType(200, Type = typeof(List<UbigeoBE>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object Provincia([Required][FromHeader(Name = "Token")] string token, [FromBody]  UbigeoBE oBE)
        {
            try
            {
                ProxySARAPI proxi = new ProxySARAPI();
                var authorization = proxi.TokenSARApi(GetAccess(), AppKey, AppCode, out string authorizationResponse);
                var response = proxi.Provincia(authorizationResponse, oBE);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return ex;
            }
        }

        [HttpPost("distrito")]
        [ProducesResponseType(200, Type = typeof(List<UbigeoBE>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object Distrito([Required][FromHeader(Name = "Token")] string token, [FromBody]  UbigeoBE oBE)
        {
            try
            {
                ProxySARAPI proxi = new ProxySARAPI();
                var authorization = proxi.TokenSARApi(GetAccess(), AppKey, AppCode, out string authorizationResponse);
                var response = proxi.Distrito(authorizationResponse, oBE);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return ex;
            }
        }

        [HttpPost("requerimiento")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object Requerimiento([Required][FromHeader(Name = "Token")] string token, [FromBody]  RequerimientoBE oRequerimientoB)
        {
            try
            {
                ProxySARAPI proxi = new ProxySARAPI();
                var authorization = proxi.TokenSARApi(GetAccess(), AppKey, AppCode, out string authorizationResponse);
                var response = proxi.Requerimiento(authorizationResponse, oRequerimientoB);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return ex;
            }
        }

        [HttpPost("requerimiento/detalle")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object RequerimientoDetalle([Required][FromHeader(Name = "Token")] string token, [FromBody]  RequerimientoBE oRequest)
        {
            try
            {
                ProxySARAPI proxi = new ProxySARAPI();
                var authorization = proxi.TokenSARApi(GetAccess(), AppKey, AppCode, out string authorizationResponse);
                var response = proxi.RequerimientoDetalle(authorizationResponse, oRequest);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return ex;
            }
        }

        [HttpPost("agencia")]
        [ProducesResponseType(200, Type = typeof(List<AgenciaBE>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object Agencia([Required][FromHeader(Name = "Token")] string token)
        {
            try
            {
                ProxySARAPI proxi = new ProxySARAPI();
                var authorization = proxi.TokenSARApi(GetAccess(), AppKey, AppCode, out string authorizationResponse);
                var response = proxi.Agencia(authorizationResponse);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return ex;
            }
        }

        [HttpPost("parametros")]
        [ProducesResponseType(200, Type = typeof(List<ParametroBE>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object ParametroGrupo([Required][FromHeader(Name = "Token")] string token, [FromBody]  Parametro oParametro)
        {
            try
            {
                ProxySARAPI proxi = new ProxySARAPI();
                var authorization = proxi.TokenSARApi(GetAccess(), AppKey, AppCode, out string authorizationResponse);
                var response = proxi.ParametroGrupo(authorizationResponse, oParametro);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return ex;
            }
        }

        [HttpPost("recurso")]
        [ProducesResponseType(200, Type = typeof(List<RecursoBE>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object recurso([Required][FromHeader(Name = "Token")] string token, [FromBody]  RecursoBE oBE)
        {
            try
            {
                ProxySARAPI proxi = new ProxySARAPI();
                var authorization = proxi.TokenSARApi(GetAccess(), AppKey, AppCode, out string authorizationResponse);
                var response = proxi.Recurso(authorizationResponse, oBE);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return ex;
            }
        }

        [HttpPost("requerimiento/reenviar/correo")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object RequerimientoEnvio([Required][FromHeader(Name = "Token")] string token, [FromBody]  RequerimientoBE oRequerimientoBE)
        {
            try
            {
                ProxySARAPI proxi = new ProxySARAPI();
                var authorization = proxi.TokenSARApi(GetAccess(), AppKey, AppCode, out string authorizationResponse);
                var response = proxi.RequerimientoEnvio(authorizationResponse, oRequerimientoBE);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return ex;
            }
        }

        [HttpPost("consultaregistrar")]
        [ProducesResponseType(200, Type = typeof(List<Usuario>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object ConsultaRegistrar([Required][FromHeader(Name = "Token")] string token, [FromBody]  ConsultaBE oConsulta)
        {
            try
            {
                ProxySARAPI proxi = new ProxySARAPI();
                var authorization = proxi.TokenSARApi(GetAccess(), AppKey, AppCode, out string authorizationResponse);
                var response = proxi.ConsultaRegistrar(authorizationResponse, oConsulta);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return ex;
            }
        }

        [HttpPost("preguntasfrecuentes")]
        [ProducesResponseType(200, Type = typeof(List<ParametroBE>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object PreguntasFrecuentes([Required][FromHeader(Name = "Token")] string token, [FromBody]  Parametro oParametro)
        {
            try
            {
                ProxySARAPI proxi = new ProxySARAPI();
                var authorization = proxi.TokenSARApi(GetAccess(), AppKey, AppCode, out string authorizationResponse);
                var response = proxi.PreguntasFrecuentes(authorizationResponse, oParametro);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return ex;
            }
        }


        private AccessDTO GetAccess()
        {
           return AuthAccess.getInstance().getAccess();
        }
    }
}
