﻿using Agrobanco.SAR.Comun;
using Agrobanco.SAR.Comun.Laserfiche;
using Agrobanco.SAR.Datos.SqlServer.Acceso;
using Agrobanco.SAR.Entidades;
using Agrobanco.SAR.Entidades.RequerimientoAsignacion;
using Agrobanco.SAR.Entidades.RequerimientoGestionReporte;
using Agrobanco.SAR.Entidades.RequerimientoReporte;
using Agrobanco.SAR.Entidades.RequerimientoSeguimiento;
using Agrobanco.SAR.Entidades.RequerimientoSeguimientoDetalle;
using Spire.Pdf.General.Paper.Font.Common.Environment;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Agrobanco.SAR.Datos
{
    public class RequerimientoDatos
    {
        public string cadenaConexion;


        public bool Registrar(RequerimientoBE oBE, out string NroRequerimiento)
        {

            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            using (SqlConnection cnn = new SqlConnection(cadenaConexion))
            {
                cnn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[SP_REQUERIMIENTO_REGISTRAR]", cnn))
                {
                    using (SqlTransaction tx = cnn.BeginTransaction())
                    {
                        try
                        {
                            cmd.Transaction = tx;
                            cmd.CommandType = CommandType.StoredProcedure;

                            SqlParameter pCodRequerimiento = new SqlParameter("@CodRequerimiento", SqlDbType.BigInt);
                            pCodRequerimiento.Direction = System.Data.ParameterDirection.Output;
                            pCodRequerimiento.Value = oBE.CodRequerimiento;
                            cmd.Parameters.Add(pCodRequerimiento);

                            SqlParameter pNroRequerimiento = new SqlParameter("@NroRequerimiento", SqlDbType.VarChar, 20);
                            pNroRequerimiento.Direction = System.Data.ParameterDirection.Output;
                            pNroRequerimiento.Value = string.Empty;
                            cmd.Parameters.Add(pNroRequerimiento);

                            cmd.Parameters.Add("@CodAgencia", SqlDbType.Int).Value = oBE.CodAgencia;
                            cmd.Parameters.Add("@EsMenor", SqlDbType.Int).Value = oBE.EsMenor;
                            cmd.Parameters.Add("@CodProducto", SqlDbType.Int).Value = oBE.CodProducto;
                            cmd.Parameters.Add("@CodMotivo", SqlDbType.Int).Value = oBE.CodMotivo;
                            cmd.Parameters.Add("@CodTipoRequerimiento", SqlDbType.Int).Value = oBE.CodTipoRequerimiento;
                            cmd.Parameters.Add("@CodOrigen", SqlDbType.Int).Value = oBE.CodOrigen;
                            cmd.Parameters.Add("@CodFormaEnvio", SqlDbType.Int).Value = oBE.CodFormaEnvio;
                            cmd.Parameters.Add("@Descripcion", SqlDbType.VarChar, 1500).Value = oBE.Descripcion;
                            cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = oBE.Estado;
                            cmd.Parameters.Add("@UsuarioRegistro", SqlDbType.VarChar, 50).Value = oBE.UsuarioRegistro;
                            cmd.Parameters.Add("@TipoDocumento", SqlDbType.Int).Value = oBE.TipoDocumento;
                            cmd.Parameters.Add("@NroDocumento", SqlDbType.VarChar, 20).Value = oBE.NroDocumento;

                            if (string.IsNullOrEmpty(oBE.PrimerApellido))
                                cmd.Parameters.Add("@PrimerApellido", SqlDbType.VarChar, 200).Value = DBNull.Value;
                            else
                                cmd.Parameters.Add("@PrimerApellido", SqlDbType.VarChar, 200).Value = oBE.PrimerApellido;


                            if (string.IsNullOrEmpty(oBE.SegundoApellido))
                                cmd.Parameters.Add("@SegundoApellido", SqlDbType.VarChar, 200).Value = DBNull.Value;
                            else
                                cmd.Parameters.Add("@SegundoApellido", SqlDbType.VarChar, 200).Value = oBE.SegundoApellido;


                            if (string.IsNullOrEmpty(oBE.Nombres))
                                cmd.Parameters.Add("@Nombres", SqlDbType.VarChar, 200).Value = DBNull.Value;
                            else
                                cmd.Parameters.Add("@Nombres", SqlDbType.VarChar, 200).Value = oBE.Nombres;


                            if (string.IsNullOrEmpty(oBE.RazonSocial))
                                cmd.Parameters.Add("@RazonSocial", SqlDbType.VarChar, 200).Value = DBNull.Value;
                            else
                                cmd.Parameters.Add("@RazonSocial", SqlDbType.VarChar, 200).Value = oBE.RazonSocial;

                            cmd.Parameters.Add("@CodUbigeo", SqlDbType.VarChar, 6).Value = oBE.CodUbigeo;
                            cmd.Parameters.Add("@Direccion", SqlDbType.VarChar, 200).Value = oBE.Direccion;
                            cmd.Parameters.Add("@Referencia", SqlDbType.VarChar, 200).Value = oBE.Referencia;

                            if (!string.IsNullOrEmpty(oBE.CorreoElectronico))
                                cmd.Parameters.Add("@CorreoElectronico", SqlDbType.VarChar, 100).Value = oBE.CorreoElectronico;
                            else
                                cmd.Parameters.Add("@CorreoElectronico", SqlDbType.VarChar, 100).Value = DBNull.Value;

                            cmd.Parameters.Add("@Celular", SqlDbType.VarChar, 20).Value = oBE.Celular;

                            if (!string.IsNullOrEmpty(oBE.Telefono))
                                cmd.Parameters.Add("@Telefono", SqlDbType.VarChar, 20).Value = oBE.Telefono;
                            else
                                cmd.Parameters.Add("@Telefono", SqlDbType.VarChar, 20).Value = DBNull.Value;


                            cmd.ExecuteNonQuery();


                            oBE.CodRequerimiento = Convert.ToInt64(pCodRequerimiento.Value);
                            oBE.NroRequerimiento = pNroRequerimiento.Value.ToString();



                            LaserficheProxy laserfiche = new LaserficheProxy();
                            DocumentoModel DocumentoModelRequest = new DocumentoModel();
                            DocumentoModel DocumentoModelResponse = new DocumentoModel();

                            DocumentoModelRequest.Folder = oBE.NroRequerimiento;
                            //List<ItemImagen> listIma = new List<ItemImagen>();
                            List<Documento> lstDocumento = new List<Documento>();
                            int item = 1;
                            string base64 = string.Empty;
                            if (!string.IsNullOrEmpty(oBE.Archivo1))
                            {
                                base64 = string.Empty;
                                base64 = oBE.Archivo1.Split(',')[1];

                                Documento oDocumento = new Documento();
                                oDocumento.Extension = System.IO.Path.GetExtension(oBE.NombreArchivo1).Split('.')[1];
                                oDocumento.ArchivoBytes = base64;
                                oDocumento.Nombre = oBE.NroRequerimiento + "-ARCHIVO" + item.ToString().PadLeft(2, '0');
                                oDocumento.Folder = oBE.NroRequerimiento;
                                lstDocumento.Add(oDocumento);
                                item++;
                            }


                            if (!string.IsNullOrEmpty(oBE.Archivo2))
                            {
                                base64 = string.Empty;
                                base64 = oBE.Archivo2.Split(',')[1];

                                Documento oDocumento = new Documento();
                                oDocumento.Extension = System.IO.Path.GetExtension(oBE.NombreArchivo2).Split('.')[1];
                                oDocumento.ArchivoBytes = base64;
                                oDocumento.Nombre = oBE.NroRequerimiento + "-ARCHIVO" + item.ToString().PadLeft(2, '0');
                                oDocumento.Folder = oBE.NroRequerimiento;
                                lstDocumento.Add(oDocumento);
                                item++;
                            }

                            if (!string.IsNullOrEmpty(oBE.Archivo3))
                            {
                                base64 = string.Empty;
                                base64 = oBE.Archivo3.Split(',')[1];

                                Documento oDocumento = new Documento();
                                oDocumento.Extension = System.IO.Path.GetExtension(oBE.NombreArchivo3).Split('.')[1];
                                oDocumento.ArchivoBytes = base64;
                                oDocumento.Nombre = oBE.NroRequerimiento + "-ARCHIVO" + item.ToString().PadLeft(2, '0');
                                oDocumento.Folder = oBE.NroRequerimiento;
                                lstDocumento.Add(oDocumento);
                                item++;
                            }
                            if (!string.IsNullOrEmpty(oBE.Archivo4))
                            {
                                base64 = string.Empty;
                                base64 = oBE.Archivo4.Split(',')[1];

                                Documento oDocumento = new Documento();
                                oDocumento.Extension = System.IO.Path.GetExtension(oBE.NombreArchivo4).Split('.')[1];
                                oDocumento.ArchivoBytes = base64;
                                oDocumento.Nombre = oBE.NroRequerimiento + "-ARCHIVO" + item.ToString().PadLeft(2, '0');
                                oDocumento.Folder = oBE.NroRequerimiento;
                                lstDocumento.Add(oDocumento);
                                item++;
                            }

                            if (lstDocumento.Count > 0)
                            {
                                DocumentoModelRequest.ListaDocumentos = lstDocumento;
                                DocumentoModelResponse = laserfiche.UploadListaDocumento(DocumentoModelRequest);

                                RecursoDatos oRecursoDatos = null;
                                RecursoBE oRecursoBE = null;
                                foreach (var image in DocumentoModelResponse.ListaDocumentos)
                                {
                                    if (image.ResultadoOK)
                                    {
                                        oRecursoDatos = new RecursoDatos();
                                        oRecursoBE = new RecursoBE();

                                        oRecursoBE.CodTabla = ConstantesTabla.Requerimiento;
                                        oRecursoBE.ReferenciaID = oBE.CodRequerimiento;
                                        oRecursoBE.LaserficheID = image.CodigoLaserfiche;
                                        oRecursoBE.Estado = true;
                                        oRecursoBE.UsuarioRegistro = oBE.UsuarioRegistro;
                                        oRecursoBE.Descripcion = image.Nombre;
                                        Boolean resultad = oRecursoDatos.RegistrarTrans(oRecursoBE, cnn, tx);

                                        if (!resultad)
                                            throw new Exception("Se presento un error al intentar registrar los archivos adjuntos");

                                    }
                                    else
                                    {
                                        throw new Exception("Se presento un error al subir el archivo");
                                    }
                                }
                            }
                            NroRequerimiento = oBE.NroRequerimiento;
                            tx.Commit();
                            return true;
                        }

                        catch (Exception ex)
                        {
                            tx.Rollback();
                            cnn.Close();
                            cnn.Dispose();
                            cmd.Dispose();
                            throw (ex);
                        }
                        finally
                        {
                            cnn.Close();
                            cnn.Dispose();
                            cmd.Dispose();
                        }
                    }


                }

            }

            NroRequerimiento = oBE.NroRequerimiento;
            return false;
        }

        public List<RequerimientoBE> RequerimientosListar(RequestRequerimiento oBE)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            RequerimientoBE objRequerimiento = null;
            List<RequerimientoBE> lstRequerimiento = new List<RequerimientoBE>();

            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_REQUERIMIENTO_OBTENER]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        if (string.IsNullOrEmpty(oBE.UsuarioAsignado))
                            cmd.Parameters.Add("usuarioasignado", SqlDbType.VarChar, 200).Value = DBNull.Value;
                        else
                            cmd.Parameters.Add("usuarioasignado", SqlDbType.VarChar, 200).Value = oBE.UsuarioAsignado;

                        cmd.Parameters.Add("@estado", SqlDbType.VarChar).Value = (oBE.CodigoEstado == null ? "" : oBE.CodigoEstado);
                        cmd.Parameters.Add("@codagencia", SqlDbType.VarChar).Value = oBE.CodAgencia;
                        cmd.Parameters.Add("@nrodocumento", SqlDbType.VarChar).Value = (oBE.NroDocumento == null ? "" : oBE.NroDocumento);
                        cmd.Parameters.Add("@nombresapellidos", SqlDbType.VarChar).Value = oBE.NombresApellidos;
                        cmd.Parameters.Add("@fecharegistroini", SqlDbType.VarChar).Value = oBE.FechaRegistroIni == null ? string.Empty : oBE.FechaRegistroIni;
                        cmd.Parameters.Add("@fecharegistrofin", SqlDbType.VarChar).Value = oBE.FechaRegistroFin == null ? string.Empty : oBE.FechaRegistroFin;
                        cmd.Parameters.Add("@fechaatencionini", SqlDbType.VarChar).Value = (oBE.FechaAtencionIni == null ? "" : oBE.FechaAtencionIni);
                        cmd.Parameters.Add("@fechaatencionfin", SqlDbType.VarChar).Value = (oBE.FechaAtencionFin == null ? "" : oBE.FechaAtencionFin);

                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    objRequerimiento = new RequerimientoBE();
                                    if (!Convert.IsDBNull(reader["NroReclamo"])) objRequerimiento.NroReclamo = Convert.ToString(reader["NroReclamo"]);
                                    if (!Convert.IsDBNull(reader["FechaRegistro"])) objRequerimiento.FechaRegistro = reader.GetDateTime("FechaRegistro");
                                    if (!Convert.IsDBNull(reader["NroDocumento"])) objRequerimiento.NroDocumento = Convert.ToString(reader["NroDocumento"]);
                                    if (!Convert.IsDBNull(reader["NombreApellidos"])) objRequerimiento.NombresApellidos = Convert.ToString(reader["NombreApellidos"]);
                                    if (!Convert.IsDBNull(reader["UsuarioAsignado"])) objRequerimiento.UsuarioAsignado = Convert.ToString(reader["UsuarioAsignado"]);
                                    if (!Convert.IsDBNull(reader["FechaAtencion"])) objRequerimiento.FechaAtencion = Convert.ToString(reader["FechaAtencion"]);
                                    if (!Convert.IsDBNull(reader["CodTipoRequerimiento"])) objRequerimiento.CodTipoRequerimiento = Convert.ToInt32(reader["CodTipoRequerimiento"]);
                                    if (!Convert.IsDBNull(reader["TipoRequerimiento"])) objRequerimiento.TipoRequerimiento = Convert.ToString(reader["TipoRequerimiento"]);
                                    if (!Convert.IsDBNull(reader["Agencia"])) objRequerimiento.Agencia = Convert.ToString(reader["Agencia"]);
                                    if (!Convert.IsDBNull(reader["Estado"])) objRequerimiento.DescripcionEstado = Convert.ToString(reader["Estado"]);

                                    lstRequerimiento.Add(objRequerimiento);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw ex;
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return lstRequerimiento;
        }

        public RequerimientoBE RequerimientoDetalle(RequerimientoBE oBE)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            RequerimientoBE objRequerimiento = null;
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_REQUERIMIENTO_OBTENER_DETALLE]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@nroreclamo", SqlDbType.VarChar).Value = (oBE.NroReclamo == null ? "" : oBE.NroReclamo);

                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    objRequerimiento = new RequerimientoBE();


                                    if (!Convert.IsDBNull(reader["CodAgencia"])) objRequerimiento.CodAgencia = Convert.ToInt32(reader["CodAgencia"]);
                                    if (!Convert.IsDBNull(reader["Oficina"])) objRequerimiento.Agencia = Convert.ToString(reader["Oficina"]);
                                    if (!Convert.IsDBNull(reader["DireccionAgencia"])) objRequerimiento.DireccionAgencia = Convert.ToString(reader["DireccionAgencia"]);
                                    if (!Convert.IsDBNull(reader["EsMenor"])) objRequerimiento.EsMenor = Convert.ToInt32(reader["EsMenor"]);
                                    if (!Convert.IsDBNull(reader["CodRequerimiento"])) objRequerimiento.CodRequerimiento = Convert.ToInt32(reader["CodRequerimiento"]);
                                    if (!Convert.IsDBNull(reader["CodRequerimientoMovimiento"])) objRequerimiento.CodRequerimientoMovimiento = Convert.ToInt32(reader["CodRequerimientoMovimiento"]);
                                    if (!Convert.IsDBNull(reader["NroRequerimiento"])) objRequerimiento.NroRequerimiento = Convert.ToString(reader["NroRequerimiento"]);
                                    if (!Convert.IsDBNull(reader["TipoDocumento"])) objRequerimiento.TipoDocumento = Convert.ToInt32(reader["TipoDocumento"]);
                                    if (!Convert.IsDBNull(reader["TipoDocumentoDescripcion"])) objRequerimiento.TipoDocumentoDescripcion = Convert.ToString(reader["TipoDocumentoDescripcion"]);
                                    if (!Convert.IsDBNull(reader["NroDocumento"])) objRequerimiento.NroDocumento = Convert.ToString(reader["NroDocumento"]);
                                    if (!Convert.IsDBNull(reader["Nombres"])) objRequerimiento.Nombres = Convert.ToString(reader["Nombres"]);
                                    if (!Convert.IsDBNull(reader["PrimerApellido"])) objRequerimiento.PrimerApellido = Convert.ToString(reader["PrimerApellido"]);
                                    if (!Convert.IsDBNull(reader["SegundoApellido"])) objRequerimiento.SegundoApellido = Convert.ToString(reader["SegundoApellido"]);
                                    if (!Convert.IsDBNull(reader["NombresApellidos"])) objRequerimiento.NombresApellidos = Convert.ToString(reader["NombresApellidos"]);
                                    if (!Convert.IsDBNull(reader["RazonSocial"])) objRequerimiento.RazonSocial = Convert.ToString(reader["RazonSocial"]);
                                    if (!Convert.IsDBNull(reader["CodUbigeo"])) objRequerimiento.CodUbigeo = Convert.ToString(reader["CodUbigeo"]);
                                    if (!Convert.IsDBNull(reader["Direccion"])) objRequerimiento.Direccion = Convert.ToString(reader["Direccion"]);
                                    if (!Convert.IsDBNull(reader["Referencia"])) objRequerimiento.Referencia = Convert.ToString(reader["Referencia"]);
                                    if (!Convert.IsDBNull(reader["CorreoElectronico"])) objRequerimiento.CorreoElectronico = Convert.ToString(reader["CorreoElectronico"]);
                                    if (!Convert.IsDBNull(reader["Telefono"])) objRequerimiento.Telefono = Convert.ToString(reader["Telefono"]);
                                    if (!Convert.IsDBNull(reader["Celular"])) objRequerimiento.Celular = Convert.ToString(reader["Celular"]);
                                    if (!Convert.IsDBNull(reader["CodMotivo"])) objRequerimiento.CodMotivo = Convert.ToInt32(reader["CodMotivo"]);
                                    if (!Convert.IsDBNull(reader["Motivo"])) objRequerimiento.Motivo = Convert.ToString(reader["Motivo"]);
                                    if (!Convert.IsDBNull(reader["TipoReclamo"])) objRequerimiento.TipoRequerimiento = Convert.ToString(reader["TipoReclamo"]);
                                    if (!Convert.IsDBNull(reader["DecripcionReclamo"])) objRequerimiento.Descripcion = Convert.ToString(reader["DecripcionReclamo"]);
                                    if (!Convert.IsDBNull(reader["CodTipoRequerimiento"])) objRequerimiento.CodTipoRequerimiento = Convert.ToInt32(reader["CodTipoRequerimiento"]);
                                    if (!Convert.IsDBNull(reader["CodProducto"])) objRequerimiento.CodProducto = Convert.ToInt32(reader["CodProducto"]);
                                    if (!Convert.IsDBNull(reader["Producto"])) objRequerimiento.Producto = Convert.ToString(reader["Producto"]);
                                    if (!Convert.IsDBNull(reader["UsuarioRegistro"])) objRequerimiento.UsuarioRegistro = Convert.ToString(reader["UsuarioRegistro"]);
                                    if (!Convert.IsDBNull(reader["CodDepartamento"])) objRequerimiento.CodDepartamento = Convert.ToString(reader["CodDepartamento"]);
                                    if (!Convert.IsDBNull(reader["Departamento"])) objRequerimiento.Departamento = Convert.ToString(reader["Departamento"]);
                                    if (!Convert.IsDBNull(reader["CodProvincia"])) objRequerimiento.CodProvincia = Convert.ToString(reader["CodProvincia"]);
                                    if (!Convert.IsDBNull(reader["Provincia"])) objRequerimiento.Provincia = Convert.ToString(reader["Provincia"]);
                                    if (!Convert.IsDBNull(reader["CodDistrito"])) objRequerimiento.CodDistrito = Convert.ToString(reader["CodDistrito"]);
                                    if (!Convert.IsDBNull(reader["Distrito"])) objRequerimiento.Distrito = Convert.ToString(reader["Distrito"]);
                                    if (!Convert.IsDBNull(reader["Estado"])) objRequerimiento.Estado = Convert.ToInt32(reader["Estado"]);
                                    if (!Convert.IsDBNull(reader["DescripcionEstado"])) objRequerimiento.DescripcionEstado = Convert.ToString(reader["DescripcionEstado"]);
                                    if (!Convert.IsDBNull(reader["FormaEnvio"])) objRequerimiento.FormaEnvio = Convert.ToString(reader["FormaEnvio"]);
                                    if (!Convert.IsDBNull(reader["Origen"])) objRequerimiento.Origen = Convert.ToString(reader["Origen"]);
                                    if (!Convert.IsDBNull(reader["FechaRegistro"])) objRequerimiento.FechaRegistro = reader.GetDateTime("FechaRegistro");
                                    if (!Convert.IsDBNull(reader["CodRequerimientoAsignacion"])) objRequerimiento.CodRequerimientoAsignacion = Convert.ToInt32(reader["CodRequerimientoAsignacion"]);
                                    if (!Convert.IsDBNull(reader["UsuarioAsignado"])) objRequerimiento.UsuarioAsignado = Convert.ToString(reader["UsuarioAsignado"]);
                                    if (!Convert.IsDBNull(reader["EstadoAnterior"])) objRequerimiento.EstadoAnterior = Convert.ToInt32(reader["EstadoAnterior"]);
                                    if (!Convert.IsDBNull(reader["UsuarioWebAsignado"])) objRequerimiento.UsuarioWebAsignado = Convert.ToString(reader["UsuarioWebAsignado"]);

                                    ComentarioDatos oComentarioDatos = new ComentarioDatos();
                                    ComentarioBE oComentarioBE = new ComentarioBE();
                                    oComentarioBE.CodRequerimiento = objRequerimiento.CodRequerimiento;
                                    oComentarioBE.CodTabla = ConstantesTabla.Comentario;
                                    objRequerimiento.Comentarios = oComentarioDatos.ComentarioObtener(oComentarioBE);

                                    RecursoDatos oRecursoDatos = new RecursoDatos();
                                    RecursoBE oRecursoBE = new RecursoBE();
                                    oRecursoBE.CodTabla = ConstantesTabla.Requerimiento;
                                    oRecursoBE.ReferenciaID = objRequerimiento.CodRequerimiento;
                                    List<RecursoBE> lstRecursoBE = oRecursoDatos.RecursoObtener(oRecursoBE);

                                    if (lstRecursoBE != null && lstRecursoBE.Count > 0)
                                    {
                                        string LaserFicheAnidados = string.Empty;
                                        foreach (var item in lstRecursoBE)
                                        {
                                            LaserFicheAnidados += "|" + item.LaserficheID;
                                        }
                                        LaserFicheAnidados = LaserFicheAnidados.Substring(1, LaserFicheAnidados.Length - 1);
                                        objRequerimiento.LaserFicheRequerimiento = LaserFicheAnidados;
                                    }


                                    oRecursoDatos = new RecursoDatos();
                                    oRecursoBE = new RecursoBE();
                                    oRecursoBE.CodTabla = ConstantesTabla.CartaRespuesta;
                                    oRecursoBE.ReferenciaID = objRequerimiento.CodRequerimiento;
                                    lstRecursoBE = oRecursoDatos.RecursoObtener(oRecursoBE);

                                    if (lstRecursoBE != null && lstRecursoBE.Count > 0)
                                    {
                                        foreach (var item in lstRecursoBE)
                                        {
                                            objRequerimiento.LaserFicheIDCartaRespuesta = item.LaserficheID.ToString();
                                            break;
                                        }
                                    }



                                    oRecursoDatos = new RecursoDatos();
                                    oRecursoBE = new RecursoBE();
                                    oRecursoBE.CodTabla = ConstantesTabla.CartaRespuestaOCM;
                                    oRecursoBE.ReferenciaID = objRequerimiento.CodRequerimiento;
                                    lstRecursoBE = oRecursoDatos.RecursoObtener(oRecursoBE);

                                    if (lstRecursoBE != null && lstRecursoBE.Count > 0)
                                    {
                                        foreach (var item in lstRecursoBE)
                                        {
                                            objRequerimiento.LaserFicheIDCartaRespuestaOCM = item.LaserficheID.ToString();
                                            break;
                                        }
                                    }

                                    if (objRequerimiento.CodTipoRequerimiento == ConstantesTipoRequerimiento.Solicitud)
                                    {
                                        CreditoClienteDatos oCreditoClienteDatos = new CreditoClienteDatos();
                                        CreditosClienteBE oCreditosClienteBE = new CreditosClienteBE();
                                        oCreditosClienteBE.TipoIDN = objRequerimiento.TipoDocumentoDescripcion;
                                        oCreditosClienteBE.Identificacion = objRequerimiento.NroDocumento;
                                        objRequerimiento.creditosClientes = oCreditoClienteDatos.ObtenerCreditos(oCreditosClienteBE);
                                    }

                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw ex;
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return objRequerimiento;
        }

        public List<ResponseReqBloqueado> RequerimientoBloqueado(RequerimientoBE oBE)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            ResponseReqBloqueado objRequerimiento = null;
            List<ResponseReqBloqueado> lstRequerimiento = new List<ResponseReqBloqueado>();

            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_REQUERIMIENTO_BUSCAR_BLOQUEADO]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@nrorequerimiento", SqlDbType.VarChar).Value = oBE.NroReclamo;

                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    objRequerimiento = new ResponseReqBloqueado();
                                    if (!Convert.IsDBNull(reader["UsuarioBloqueo"])) objRequerimiento.UsuarioBloqueo = Convert.ToString(reader["UsuarioBloqueo"]);
                                    if (!Convert.IsDBNull(reader["EstaBloqueado"])) objRequerimiento.EstaBloqueado = Convert.ToInt32(reader["EstaBloqueado"]);
                                    if (!Convert.IsDBNull(reader["CodTipoRequerimiento"])) objRequerimiento.CodTipoRequerimiento = Convert.ToInt32(reader["CodTipoRequerimiento"]);
                                    if (!Convert.IsDBNull(reader["CodRequerimiento"])) objRequerimiento.CodRequerimiento = Convert.ToInt32(reader["CodRequerimiento"]);
                                    if (!Convert.IsDBNull(reader["NombresApellidos"])) objRequerimiento.NombresApellidos = Convert.ToString(reader["NombresApellidos"]);
                                    lstRequerimiento.Add(objRequerimiento);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw ex;
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return lstRequerimiento;
        }

        public bool RequerimientoActualizarBloqueado(RequerimientoBE oBE)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            using (SqlConnection cnn = new SqlConnection(cadenaConexion))
            {
                cnn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[SP_REQUERIMIENTO_ACTUALIZAR_BLOQUEADO]", cnn))
                {
                    try
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@nrorequerimiento", SqlDbType.VarChar).Value = oBE.NroReclamo;
                        cmd.Parameters.Add("@usuarioregistro", SqlDbType.VarChar).Value = (oBE.UsuarioRegistro == null ? "" : oBE.UsuarioRegistro);
                        cmd.Parameters.Add("@EstaBloqueado", SqlDbType.Int).Value = oBE.EstaBloqueado;

                        cmd.ExecuteNonQuery();

                        return true;
                    }

                    catch (Exception ex)
                    {
                        cnn.Close();
                        cnn.Dispose();
                        cmd.Dispose();
                        throw ex;
                    }
                    finally
                    {
                        cnn.Close();
                        cnn.Dispose();
                        cmd.Dispose();
                    }

                }

            }

            return false;
        }

        public List<RequerimientoBE> RequerimientoMovimientoPenultimo(int CodRequerimiento)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            RequerimientoBE objRequerimiento = null;
            List<RequerimientoBE> lstRequerimiento = new List<RequerimientoBE>();

            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_REQUERIMIENTO_MOVIMIENTO_PENULTIMO]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@codRequerimiento", SqlDbType.Int).Value = CodRequerimiento;

                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    objRequerimiento = new RequerimientoBE();

                                    if (!Convert.IsDBNull(reader["Estado"])) objRequerimiento.Estado = Convert.ToInt32(reader["Estado"]);


                                    lstRequerimiento.Add(objRequerimiento);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw ex;
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return lstRequerimiento;
        }

        public int RequerimientoMovimientoRegistrar(RequerimientoBE oBE)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            List<RequerimientoBE> lstRequerimiento = new List<RequerimientoBE>();
            int codRequerimientoMovimiento = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_REQUERIMIENTO_MOVIMIENTO_INSERTAR]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        SqlParameter paramId = new SqlParameter("CodRequerimientoMovimiento", SqlDbType.Int);
                        paramId.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(paramId);

                        cmd.Parameters.Add("@codRequerimiento", SqlDbType.Int).Value = oBE.CodRequerimiento;
                        cmd.Parameters.Add("@estado", SqlDbType.Int).Value = oBE.Estado;
                        cmd.Parameters.Add("@usuarioRegistro", SqlDbType.VarChar).Value = oBE.UsuarioRegistro;

                        try
                        {
                            int rowsAffected = cmd.ExecuteNonQuery();

                            if (rowsAffected > 0)
                            {
                                return codRequerimientoMovimiento = Convert.ToInt32(cmd.Parameters["CodRequerimientoMovimiento"].Value);

                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw ex;
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return codRequerimientoMovimiento;
        }

        public bool RequerimientoAsignacionRegistrar(RequerimientoBE oBE, string webRootPath)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            using (SqlConnection cnn = new SqlConnection(cadenaConexion))
            {
                cnn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[SP_REQUERIMIENTO_ASIGNACION_INSERTAR]", cnn))
                {
                    try
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@CodRequerimiento", SqlDbType.Int).Value = oBE.CodRequerimiento;
                        cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = oBE.Estado;
                        cmd.Parameters.Add("@UsuarioAsignado", SqlDbType.VarChar).Value = oBE.UsuarioAsignar;
                        cmd.Parameters.Add("@UsuarioRegistro", SqlDbType.VarChar).Value = oBE.UsuarioRegistro;
                        cmd.ExecuteNonQuery();

                        ParametroDatos oParametroDatos = new ParametroDatos();
                        Usuario oUsuario = new Usuario();
                        oUsuario.UsuarioWeb = oBE.UsuarioAsignar;
                        var lstUsuario = oParametroDatos.ListarUsuario(oUsuario);
                        if (lstUsuario.Count > 0)
                        {
                            EnvioCorreo objEnvioCorreo = new EnvioCorreo();
                            oBE.UsuarioAsignado = lstUsuario[0].NombresApellidos;
                            string cuerpocorreo = EnvioCorreoCuerpo.CuerpoCorreoRequerimientoAsignado(oBE, webRootPath);

                            List<string> listaCorreo = new List<string>();
                            listaCorreo.Add(lstUsuario[0].CorreoElectronico);

                            List<string> listaCorreocopia = new List<string>();

                            objEnvioCorreo.EnviarCorreo(listaCorreo, listaCorreocopia, cuerpocorreo, ApplicationKeys.CorreoAsuntoAsignar);


                        }
                        else
                        {
                            throw new Exception("Error no se ha encontrado el correo del usuario asignado");
                        }

                        //if (!string.IsNullOrEmpty(oBE.CorreoElectronico))
                        //{
                        //    try
                        //    {
                        //        DateTime fecha = DateTime.Today;
                        //        //string fechaAsignacion = fecha.ToString("yyyy-MM-dd");
                        //        //string Documento = oBE.TipoDocumentoDescripcion + "-" + oBE.NroDocumento;

                        //        EnvioCorreo objEnvioCorreo = new EnvioCorreo();
                        //        string cuerpocorreo = EnvioCorreoCuerpo.CuerpoCorreoRequerimientoAsignado(oBE, webRootPath);

                        //        List<string> listaCorreo = new List<string>();
                        //        listaCorreo.Add(oBE.CorreoElectronico);

                        //        List<string> listaCorreocopia = new List<string>();

                        //        objEnvioCorreo.EnviarCorreo(listaCorreo, listaCorreocopia, cuerpocorreo,ApplicationKeys.CorreoAsuntoAsignar);
                        //    }
                        //    catch (Exception ex)
                        //    {


                        //    }
                        //}


                        return true;
                    }

                    catch (Exception ex)
                    {
                        cnn.Close();
                        cnn.Dispose();
                        cmd.Dispose();
                        throw ex;
                    }
                    finally
                    {
                        cnn.Close();
                        cnn.Dispose();
                        cmd.Dispose();
                    }

                }

            }


            return false;
        }

        public List<RequerimientoBE> RequerimientoAsignacionObtener(int CodRequerimientoAsignacion)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            RequerimientoBE objRequerimiento = null;
            List<RequerimientoBE> lstRequerimiento = new List<RequerimientoBE>();

            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_REQUERIMIENTO_ASIGNACION_OBTENER]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@CodRequerimientoAsignacion", SqlDbType.Int).Value = CodRequerimientoAsignacion;

                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    objRequerimiento = new RequerimientoBE();

                                    if (!Convert.IsDBNull(reader["CodRequerimientoMovimiento"])) objRequerimiento.CodRequerimientoMovimiento = Convert.ToInt32(reader["CodRequerimientoMovimiento"]);
                                    if (!Convert.IsDBNull(reader["UsuarioAsignado"])) objRequerimiento.UsuarioAsignado = Convert.ToString(reader["UsuarioAsignado"]);
                                    if (!Convert.IsDBNull(reader["UsuarioRegistro"])) objRequerimiento.UsuarioRegistro = Convert.ToString(reader["UsuarioRegistro"]);
                                    if (!Convert.IsDBNull(reader["FechaRegistro"])) objRequerimiento.FechaRegistro = reader.GetDateTime("FechaRegistro");

                                    lstRequerimiento.Add(objRequerimiento);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw ex;
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return lstRequerimiento;
        }


        public bool RequerimientoActualizarEstado(RequerimientoBE oBE)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            using (SqlConnection cnn = new SqlConnection(cadenaConexion))
            {
                cnn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[SP_REQUERIMIENTO_UPDATE_ESTADO]", cnn))
                {
                    try
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@CodRequerimiento", SqlDbType.Int).Value = oBE.CodRequerimiento;
                        cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = oBE.Estado;
                        cmd.ExecuteNonQuery();

                        return true;
                    }

                    catch (Exception ex)
                    {
                        cnn.Close();
                        cnn.Dispose();
                        cmd.Dispose();
                        throw ex;
                    }
                    finally
                    {
                        cnn.Close();
                        cnn.Dispose();
                        cmd.Dispose();
                    }

                }

            }


            return false;
        }

        //public Documento DescagarArchivos(RecursoBE oBE)
        //{
        //    this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
        //    Documento imagenesModelResponse = new Documento();
        //    using (SqlConnection cnn = new SqlConnection(cadenaConexion))
        //    {
        //        cnn.Open();

        //        try
        //        {

        //            LaserficheProxy laserfiche = new LaserficheProxy();
        //            Documento imagenesModelRequest = new Documento();


        //            int LaserficheID = Convert.ToInt32(oBE.LaserficheID);
        //            List<ItemImagen> listIma = new List<ItemImagen>();                   

        //            if (LaserficheID > 0)
        //            {
        //                string codigoLaserAnidado = string.Empty;
        //                imagenesModelResponse = laserfiche.ConsultarImagenLista(LaserficheID);

        //            }

        //           return imagenesModelResponse;
        //        }

        //        catch (Exception ex)
        //        {
        //            //cnn.Close();
        //            //cnn.Dispose();
        //            //cmd.Dispose();
        //        }
        //        finally
        //        {
        //            cnn.Close();
        //            cnn.Dispose();
        //            //cmd.Dispose();
        //        }

        //        //}

        //    }

        //    return imagenesModelResponse;
        //}

        public int DerivadoOCM(RequerimientoBE oBE, string webRootPath)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            int codRequerimientoMovimiento = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    SqlTransaction transaction;
                    conn.Open();
                    transaction = conn.BeginTransaction();
                    using (SqlCommand cmd = new SqlCommand("SP_REQUERIMIENTO_DERIVADO_A_OCM_INSERTAR", conn, transaction))
                    {
                        try
                        {
                            cmd.CommandType = CommandType.StoredProcedure;

                            SqlParameter paramId = new SqlParameter("@CodRequerimientoMovimiento", SqlDbType.Int);
                            paramId.Direction = ParameterDirection.Output;
                            cmd.Parameters.Add(paramId);

                            cmd.Parameters.Add("@CodRequerimiento", SqlDbType.Int).Value = oBE.CodRequerimiento;
                            cmd.Parameters.Add("@estado", SqlDbType.Int).Value = oBE.Estado;
                            cmd.Parameters.Add("@usuarioRegistro", SqlDbType.VarChar).Value = oBE.UsuarioRegistro;


                            cmd.ExecuteNonQuery();

                            codRequerimientoMovimiento = Convert.ToInt32(cmd.Parameters["@CodRequerimientoMovimiento"].Value);


                            ParametroDatos oParametroDatos = new ParametroDatos();
                            Usuario oUsuario = new Usuario();
                            oUsuario.Perfil = "OCM";
                            var lstUsuario = oParametroDatos.ListarUsuario(oUsuario);
                            if (lstUsuario.Count > 0 && lstUsuario.Where(u => u.Estado.HasValue && u.Estado.Value).ToList().Count > 0)
                            {

                                EnvioCorreo objEnvioCorreo = new EnvioCorreo();
                                string cuerpocorreo = EnvioCorreoCuerpo.CuerpoCorreoRequerimientoDerivadarOCM(oBE, webRootPath);

                                var usuarioOCMActivo = lstUsuario.FirstOrDefault(u => u.Estado.HasValue && u.Estado.Value);

                                List<string> listaCorreo = new List<string>();
                                listaCorreo.Add(usuarioOCMActivo.CorreoElectronico);

                                List<string> listaCorreocopia = new List<string>();

                                objEnvioCorreo.EnviarCorreo(listaCorreo, listaCorreocopia, cuerpocorreo, ApplicationKeys.CorreoAsuntoDerivarOCM);
                            }



                            transaction.Commit();
                            return codRequerimientoMovimiento;
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw ex;
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool AprobarRequerimiento(RequerimientoBE oBE)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    SqlTransaction transaction;
                    conn.Open();
                    transaction = conn.BeginTransaction();
                    using (SqlCommand cmd = new SqlCommand("SP_REQUERIMIENTO_APROBAR_INSERTAR", conn, transaction))
                    {
                        try
                        {
                            cmd.CommandType = CommandType.StoredProcedure;

                            SqlParameter paramId = new SqlParameter("@CodRequerimientoMovimiento", SqlDbType.Int);
                            paramId.Direction = ParameterDirection.Output;
                            cmd.Parameters.Add(paramId);

                            cmd.Parameters.Add("@CodRequerimiento", SqlDbType.BigInt).Value = oBE.CodRequerimiento;
                            cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = oBE.Estado;
                            cmd.Parameters.Add("@UsuarioRegistro", SqlDbType.VarChar, 50).Value = oBE.UsuarioRegistro;
                            cmd.ExecuteNonQuery();
                            transaction.Commit();
                            return true;
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw ex;
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return false;
        }

        public int DerivarParaSuNotificacion(RequerimientoBE oBE, string webRootPath)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            int codRequerimientoMovimiento = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    SqlTransaction transaction;
                    conn.Open();
                    transaction = conn.BeginTransaction();
                    using (SqlCommand cmd = new SqlCommand("SP_REQUERIMIENTO_DERIVAR_PARA_SU_NOTIFICACION_INSERTAR", conn, transaction))
                    {
                        try
                        {
                            cmd.CommandType = CommandType.StoredProcedure;

                            SqlParameter paramId = new SqlParameter("@CodRequerimientoMovimiento", SqlDbType.Int);
                            paramId.Direction = ParameterDirection.Output;
                            cmd.Parameters.Add(paramId);

                            cmd.Parameters.Add("@CodRequerimiento", SqlDbType.Int).Value = oBE.CodRequerimiento;
                            cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = oBE.Estado;
                            cmd.Parameters.Add("@UsuarioAsignado", SqlDbType.VarChar).Value = oBE.UsuarioAsignadoNotificacion;
                            cmd.Parameters.Add("@UsuarioRegistro", SqlDbType.VarChar).Value = oBE.UsuarioRegistro;

                            cmd.ExecuteNonQuery();

                            codRequerimientoMovimiento = Convert.ToInt32(cmd.Parameters["@CodRequerimientoMovimiento"].Value);


                            ParametroDatos oParametroDatos = new ParametroDatos();
                            Usuario oUsuario = new Usuario();
                            oUsuario.UsuarioWeb = oBE.UsuarioAsignadoNotificacion;
                            var lstUsuario = oParametroDatos.ListarUsuario(oUsuario);

                            if (lstUsuario.Count > 0 && lstUsuario.Where(u => u.Estado.HasValue && u.Estado.Value).ToList().Count > 0)
                            {
                                EnvioCorreo objEnvioCorreo = new EnvioCorreo();
                                oBE.UsuarioAsignadoNotificacion = lstUsuario[0].NombresApellidos;
                                string cuerpocorreo = EnvioCorreoCuerpo.CuerpoCorreoRequerimientoDerivadarNotificacion(oBE, webRootPath);

                                List<string> listaCorreo = new List<string>();
                                listaCorreo.Add(lstUsuario[0].CorreoElectronico);

                                List<string> listaCorreocopia = new List<string>();

                                objEnvioCorreo.EnviarCorreo(listaCorreo, listaCorreocopia, cuerpocorreo, ApplicationKeys.CorreoAsuntoDerivarNotificación);

                            }
                            else
                            {
                                throw new Exception("Error no se ha encontrado el correo del usuario asignado");
                            }

                            transaction.Commit();
                            return codRequerimientoMovimiento;
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw ex;
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool AtenderRequerimiento(RequerimientoBE oBE)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    SqlTransaction transaction;
                    conn.Open();
                    transaction = conn.BeginTransaction();
                    using (SqlCommand cmd = new SqlCommand("SP_REQUERIMIENTO_ATENDIDO_INSERTAR", conn, transaction))
                    {
                        try
                        {
                            cmd.CommandType = CommandType.StoredProcedure;

                            SqlParameter paramId = new SqlParameter("@CodRequerimientoMovimiento", SqlDbType.Int);
                            paramId.Direction = ParameterDirection.Output;
                            cmd.Parameters.Add(paramId);

                            cmd.Parameters.Add("@CodRequerimiento", SqlDbType.BigInt).Value = oBE.CodRequerimiento;
                            cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = oBE.Estado;
                            cmd.Parameters.Add("@UsuarioRegistro", SqlDbType.VarChar, 50).Value = oBE.UsuarioRegistro;
                            cmd.Parameters.Add("@FechaEntrega", SqlDbType.DateTime).Value = oBE.FechaEntrega;
                            cmd.ExecuteNonQuery();
                            transaction.Commit();
                            return true;
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw ex;
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return false;
        }

        public List<RequerimientoSeguimientoFiltroResponse> ObtenerSeguimiento(RequerimientoSeguimientoFiltroRequest request)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            RequerimientoSeguimientoFiltroResponse oRequerimientoSeguimientoFiltroResponse = null;
            List<RequerimientoSeguimientoFiltroResponse> LstRequerimientoSeguimientoFiltroResponse = new List<RequerimientoSeguimientoFiltroResponse>();

            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("SP_REQUERIMIENTO_SEGUIMIENTO", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        if (request.FechaRegistroInicial == null || request.FechaRegistroFin == null)
                        {
                            cmd.Parameters.Add("@FechaRegistroInicio", SqlDbType.Date).Value = DBNull.Value;
                            cmd.Parameters.Add("@FechaRegistroFin", SqlDbType.Date).Value = DBNull.Value;
                        }
                        else
                        {
                            cmd.Parameters.Add("@FechaRegistroInicio", SqlDbType.Date).Value = request.FechaRegistroInicial;
                            cmd.Parameters.Add("@FechaRegistroFin", SqlDbType.Date).Value = request.FechaRegistroFin;
                        }

                        if (request.FechaAsignacionInicial == null || request.FechaAsignacionFin == null)
                        {
                            cmd.Parameters.Add("@FechaAsignacionInicio", SqlDbType.Date).Value = DBNull.Value;
                            cmd.Parameters.Add("@FechaAsignacionFin", SqlDbType.Date).Value = DBNull.Value;
                        }
                        else
                        {
                            cmd.Parameters.Add("@FechaAsignacionInicio", SqlDbType.Date).Value = request.FechaAsignacionInicial;
                            cmd.Parameters.Add("@FechaAsignacionFin", SqlDbType.Date).Value = request.FechaAsignacionFin;
                        }

                        if (request.FechaGestionInicial == null || request.FechaGestionFin == null)
                        {
                            cmd.Parameters.Add("@FechaGestionInicio", SqlDbType.Date).Value = DBNull.Value;
                            cmd.Parameters.Add("@FechaGestionFin", SqlDbType.Date).Value = DBNull.Value;
                        }
                        else
                        {
                            cmd.Parameters.Add("@FechaGestionInicio", SqlDbType.Date).Value = request.FechaGestionInicial;
                            cmd.Parameters.Add("@FechaGestionFin", SqlDbType.Date).Value = request.FechaGestionFin;
                        }

                        if (request.FechaAtencionInicial == null || request.FechaAtencionFin == null)
                        {
                            cmd.Parameters.Add("@FechaAtencionInicio", SqlDbType.Date).Value = DBNull.Value;
                            cmd.Parameters.Add("@FechaAtencionFin", SqlDbType.Date).Value = DBNull.Value;
                        }
                        else
                        {
                            cmd.Parameters.Add("@FechaAtencionInicio", SqlDbType.Date).Value = request.FechaAtencionInicial;
                            cmd.Parameters.Add("@FechaAtencionFin", SqlDbType.Date).Value = request.FechaAtencionFin;
                        }

                        if (request.TipoRequerimiento == null || request.TipoRequerimiento == 0)
                            cmd.Parameters.Add("@TipoRequeriminto", SqlDbType.Int).Value = DBNull.Value;
                        else
                            cmd.Parameters.Add("@TipoRequeriminto", SqlDbType.Int).Value = request.TipoRequerimiento;


                        if (request.Estado == null || request.Estado == 0)
                            cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = DBNull.Value;
                        else
                            cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = request.Estado;

                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    oRequerimientoSeguimientoFiltroResponse = new RequerimientoSeguimientoFiltroResponse();


                                    if (!reader.IsDBNull(0))
                                        oRequerimientoSeguimientoFiltroResponse.FechaRegistro = reader.GetDateTime(0);

                                    if (!reader.IsDBNull(1))
                                        oRequerimientoSeguimientoFiltroResponse.FechaAsignacion = reader.GetDateTime(1);

                                    if (!reader.IsDBNull(2))
                                        oRequerimientoSeguimientoFiltroResponse.FechaGestion = reader.GetDateTime(2);

                                    if (!reader.IsDBNull(3))
                                        oRequerimientoSeguimientoFiltroResponse.FechaAtencion = reader.GetDateTime(3);

                                    if (!reader.IsDBNull(4))
                                        oRequerimientoSeguimientoFiltroResponse.Estado = reader.GetString(4);

                                    if (!reader.IsDBNull(5))
                                        oRequerimientoSeguimientoFiltroResponse.TipoRequerimiento = reader.GetString(5);

                                    if (!reader.IsDBNull(6))
                                        oRequerimientoSeguimientoFiltroResponse.UsuarioAsignado = reader.GetString(6);

                                    if (!reader.IsDBNull(7))
                                        oRequerimientoSeguimientoFiltroResponse.DiasTranscurrido = reader.GetInt32(7);

                                    if (!reader.IsDBNull(8))
                                        oRequerimientoSeguimientoFiltroResponse.NroRequerimiento = reader.GetString(8);


                                    LstRequerimientoSeguimientoFiltroResponse.Add(oRequerimientoSeguimientoFiltroResponse);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw ex;
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return LstRequerimientoSeguimientoFiltroResponse;
        }

        public List<RequerimientoSeguimientoDetalleResponse> ObtenerSeguimientoDetalle(RequerimientoSeguimientoDetalleRequest request)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            RequerimientoSeguimientoDetalleResponse oRequerimientoSeguimientoDetalleResponse = null;
            List<RequerimientoSeguimientoDetalleResponse> LstRequerimientoSeguimientoDetalleResponse = new List<RequerimientoSeguimientoDetalleResponse>();

            List<RequerimientoAsignacionResponse> LstRequerimientoAsignacionResponse = new List<RequerimientoAsignacionResponse>();

            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("SP_REQUERIMIENTO_MOVIMIENTO_OBTENER_DETALLE", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@NroReclamo", SqlDbType.VarChar).Value = request.NroReclamo;

                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    oRequerimientoSeguimientoDetalleResponse = new RequerimientoSeguimientoDetalleResponse();


                                    if (!reader.IsDBNull(0))
                                        oRequerimientoSeguimientoDetalleResponse.CodRequerimientoMovimiento = reader.GetInt64(0);

                                    if (!reader.IsDBNull(1))
                                        oRequerimientoSeguimientoDetalleResponse.CodRequerimiento = reader.GetInt64(1);

                                    if (!reader.IsDBNull(2))
                                        oRequerimientoSeguimientoDetalleResponse.Estado = reader.GetString(2);

                                    if (!reader.IsDBNull(3))
                                        oRequerimientoSeguimientoDetalleResponse.FechaEntrega = reader.GetDateTime(3);

                                    if (!reader.IsDBNull(4))
                                        oRequerimientoSeguimientoDetalleResponse.FechaCartaRespuesta = reader.GetDateTime(4);

                                    if (!reader.IsDBNull(5))
                                        oRequerimientoSeguimientoDetalleResponse.FechaCartaRespuestaOcm = reader.GetDateTime(5);

                                    if (!reader.IsDBNull(6))
                                        oRequerimientoSeguimientoDetalleResponse.UsuarioRegistro = reader.GetString(6);

                                    if (!reader.IsDBNull(7))
                                        oRequerimientoSeguimientoDetalleResponse.FechaRegistro = reader.GetDateTime(7);

                                    if (!reader.IsDBNull(8))
                                        oRequerimientoSeguimientoDetalleResponse.DiasTranscurrido = reader.GetInt32(8);

                                    RequerimientoAsignacionRequest oRequerimientoAsignacionRequest = new RequerimientoAsignacionRequest();
                                    oRequerimientoAsignacionRequest.CodRequerimientoMovimiento = oRequerimientoSeguimientoDetalleResponse.CodRequerimientoMovimiento;
                                    oRequerimientoAsignacionRequest.CodRequerimiento = oRequerimientoSeguimientoDetalleResponse.CodRequerimiento;

                                    oRequerimientoSeguimientoDetalleResponse.RequerimientoAsignacion = ObtenerRequerimientoAsignacion(oRequerimientoAsignacionRequest);


                                    LstRequerimientoSeguimientoDetalleResponse.Add(oRequerimientoSeguimientoDetalleResponse);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw ex;
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return LstRequerimientoSeguimientoDetalleResponse;
        }


        public List<RequerimientoAsignacionResponse> ObtenerRequerimientoAsignacion(RequerimientoAsignacionRequest request)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            RequerimientoAsignacionResponse oRequerimientoAsignacionResponse = null;
            List<RequerimientoAsignacionResponse> LstRequerimientoAsignacionResponse = new List<RequerimientoAsignacionResponse>();

            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("SP_RQUERIMEINTO_ASIGNACION_OBTENER_COD_MOVIMIENTO", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@CodRequerimientoMovimiento", SqlDbType.BigInt).Value = request.CodRequerimientoMovimiento;
                        cmd.Parameters.Add("@CodRequerimiento", SqlDbType.BigInt).Value = request.CodRequerimiento;
                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    oRequerimientoAsignacionResponse = new RequerimientoAsignacionResponse();


                                    if (!reader.IsDBNull(0))
                                        oRequerimientoAsignacionResponse.CodRequerimientoAsignacion = reader.GetInt64(0);

                                    if (!reader.IsDBNull(1))
                                        oRequerimientoAsignacionResponse.CodRequerimiento = reader.GetInt64(1);

                                    if (!reader.IsDBNull(2))
                                        oRequerimientoAsignacionResponse.CodRequerimientoMovimiento = reader.GetInt64(2);

                                    if (!reader.IsDBNull(3))
                                        oRequerimientoAsignacionResponse.UsuarioAsignado = reader.GetString(3);

                                    if (!reader.IsDBNull(4))
                                        oRequerimientoAsignacionResponse.UsuarioRegistro = reader.GetString(4);

                                    if (!reader.IsDBNull(5))
                                        oRequerimientoAsignacionResponse.FechaRegistro = reader.GetDateTime(5);



                                    LstRequerimientoAsignacionResponse.Add(oRequerimientoAsignacionResponse);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw ex;
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return LstRequerimientoAsignacionResponse;
        }


        public List<RequerimientoReporteFiltroResponse> ObtenerRequerimientoReporte(RequerimientoReporteFiltroRequest request)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            RequerimientoReporteFiltroResponse oRequerimientoReporteFiltroResponse = null;
            List<RequerimientoReporteFiltroResponse> LstRequerimientoReporteFiltroResponse = new List<RequerimientoReporteFiltroResponse>();

            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("SP_REQUERIMIENTO_OBTENER_REPORTE", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        if (request.CodTipoRequerimiento == null || request.CodTipoRequerimiento == 0)
                            cmd.Parameters.Add("@CodTipoRequerimiento", SqlDbType.Int).Value = DBNull.Value;
                        else
                            cmd.Parameters.Add("@CodTipoRequerimiento", SqlDbType.Int).Value = request.CodTipoRequerimiento;


                        if (request.Estado == null || request.Estado == 0)
                            cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = DBNull.Value;
                        else
                            cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = request.Estado;


                        //if (request.FechaRegistroInicio == null || request.FechaRegistroFin == null)
                        //{
                        //    cmd.Parameters.Add("@FechaRegistroInicio", SqlDbType.Date).Value = DBNull.Value;
                        //    cmd.Parameters.Add("@FechaRegistroFin", SqlDbType.Date).Value = DBNull.Value;
                        //}
                        //else
                        //{
                        //    cmd.Parameters.Add("@FechaRegistroInicio", SqlDbType.Date).Value = request.FechaRegistroInicio;
                        //    cmd.Parameters.Add("@FechaRegistroFin", SqlDbType.Date).Value = request.FechaRegistroFin;
                        //}
                        //
                        cmd.Parameters.Add("@FechaRegistroInicio", SqlDbType.VarChar).Value = request.FechaRegistroInicio == null ? string.Empty : request.FechaRegistroInicio;
                        cmd.Parameters.Add("@FechaRegistroFin", SqlDbType.VarChar).Value = request.FechaRegistroFin == null ? string.Empty : request.FechaRegistroFin;


                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    oRequerimientoReporteFiltroResponse = new RequerimientoReporteFiltroResponse();


                                    if (!reader.IsDBNull(0))
                                        oRequerimientoReporteFiltroResponse.NroRequerimiento = reader.GetString(0);

                                    if (!reader.IsDBNull(1))
                                        oRequerimientoReporteFiltroResponse.EsMenor = reader.GetString(1);

                                    if (!reader.IsDBNull(2))
                                        oRequerimientoReporteFiltroResponse.TipoDocumento = reader.GetString(2);

                                    if (!reader.IsDBNull(3))
                                        oRequerimientoReporteFiltroResponse.NroDocumento = reader.GetString(3);

                                    if (!reader.IsDBNull(4))
                                        oRequerimientoReporteFiltroResponse.NombresApellidos = reader.GetString(4);

                                    if (!reader.IsDBNull(5))
                                        oRequerimientoReporteFiltroResponse.RazonSocial = reader.GetString(5);

                                    if (!reader.IsDBNull(6))
                                        oRequerimientoReporteFiltroResponse.Descripcion = reader.GetString(6);

                                    if (!reader.IsDBNull(7))
                                        oRequerimientoReporteFiltroResponse.Departamento = reader.GetString(7);

                                    if (!reader.IsDBNull(8))
                                        oRequerimientoReporteFiltroResponse.Provincia = reader.GetString(8);

                                    if (!reader.IsDBNull(9))
                                        oRequerimientoReporteFiltroResponse.Distrito = reader.GetString(9);

                                    if (!reader.IsDBNull(10))
                                        oRequerimientoReporteFiltroResponse.Telefono = reader.GetString(10);

                                    if (!reader.IsDBNull(11))
                                        oRequerimientoReporteFiltroResponse.Celular = reader.GetString(11);

                                    if (!reader.IsDBNull(12))
                                        oRequerimientoReporteFiltroResponse.Producto = reader.GetString(12);

                                    if (!reader.IsDBNull(13))
                                        oRequerimientoReporteFiltroResponse.Motivo = reader.GetString(13);

                                    if (!reader.IsDBNull(14))
                                        oRequerimientoReporteFiltroResponse.TipoRequerimiento = reader.GetString(14);

                                    if (!reader.IsDBNull(15))
                                        oRequerimientoReporteFiltroResponse.Origen = reader.GetString(15);

                                    if (!reader.IsDBNull(16))
                                        oRequerimientoReporteFiltroResponse.Agencia = reader.GetString(16);

                                    if (!reader.IsDBNull(17))
                                        oRequerimientoReporteFiltroResponse.FechaRegistro = reader.GetDateTime(17);

                                    if (!reader.IsDBNull(18))
                                        oRequerimientoReporteFiltroResponse.Estado = reader.GetString(18);

                                    if (!reader.IsDBNull(19))
                                        oRequerimientoReporteFiltroResponse.FormaEnvio = reader.GetString(19);


                                    LstRequerimientoReporteFiltroResponse.Add(oRequerimientoReporteFiltroResponse);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw ex;
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return LstRequerimientoReporteFiltroResponse;
        }


        public List<RequerimientoGestionReporteFiltroResponse> ObtenerRequerimientoGestionReporte(RequerimientoGestionReporteFiltroRequest request)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            RequerimientoGestionReporteFiltroResponse oRequerimientoGestionReporteFiltroResponse = null;
            List<RequerimientoGestionReporteFiltroResponse> LstRequerimientoGestionReporteFiltroResponse = new List<RequerimientoGestionReporteFiltroResponse>();

            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("SP_REQUERIMIENTO_GESTION_REPORTE", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        //if (request.FechaRegistroInicial == null || request.FechaRegistroFin == null)
                        //{
                        //    cmd.Parameters.Add("@FechaRegistroInicio", SqlDbType.Date).Value = DBNull.Value;
                        //    cmd.Parameters.Add("@FechaRegistroFin", SqlDbType.Date).Value = DBNull.Value;
                        //}
                        //else
                        //{
                        //    cmd.Parameters.Add("@FechaRegistroInicio", SqlDbType.Date).Value = request.FechaRegistroInicial;
                        //    cmd.Parameters.Add("@FechaRegistroFin", SqlDbType.Date).Value = request.FechaRegistroFin;
                        //}
                        cmd.Parameters.Add("@FechaRegistroInicio", SqlDbType.VarChar).Value = request.FechaRegistroInicial == null ? string.Empty : request.FechaRegistroInicial;
                        cmd.Parameters.Add("@FechaRegistroFin", SqlDbType.VarChar).Value = request.FechaRegistroFin == null ? string.Empty : request.FechaRegistroFin;

                        if (request.FechaAsignacionInicial == null || request.FechaAsignacionFin == null)
                        {
                            cmd.Parameters.Add("@FechaAsignacionInicio", SqlDbType.Date).Value = DBNull.Value;
                            cmd.Parameters.Add("@FechaAsignacionFin", SqlDbType.Date).Value = DBNull.Value;
                        }
                        else
                        {
                            cmd.Parameters.Add("@FechaAsignacionInicio", SqlDbType.Date).Value = request.FechaAsignacionInicial;
                            cmd.Parameters.Add("@FechaAsignacionFin", SqlDbType.Date).Value = request.FechaAsignacionFin;
                        }

                        if (request.FechaGestionInicial == null || request.FechaGestionFin == null)
                        {
                            cmd.Parameters.Add("@FechaGestionInicio", SqlDbType.Date).Value = DBNull.Value;
                            cmd.Parameters.Add("@FechaGestionFin", SqlDbType.Date).Value = DBNull.Value;
                        }
                        else
                        {
                            cmd.Parameters.Add("@FechaGestionInicio", SqlDbType.Date).Value = request.FechaGestionInicial;
                            cmd.Parameters.Add("@FechaGestionFin", SqlDbType.Date).Value = request.FechaGestionFin;
                        }

                        if (request.FechaAtencionInicial == null || request.FechaAtencionFin == null)
                        {
                            cmd.Parameters.Add("@FechaAtencionInicio", SqlDbType.Date).Value = DBNull.Value;
                            cmd.Parameters.Add("@FechaAtencionFin", SqlDbType.Date).Value = DBNull.Value;
                        }
                        else
                        {
                            cmd.Parameters.Add("@FechaAtencionInicio", SqlDbType.Date).Value = request.FechaAtencionInicial;
                            cmd.Parameters.Add("@FechaAtencionFin", SqlDbType.Date).Value = request.FechaAtencionFin;
                        }

                        if (request.TipoRequerimiento == null || request.TipoRequerimiento == 0)
                            cmd.Parameters.Add("@TipoRequeriminto", SqlDbType.Int).Value = DBNull.Value;
                        else
                            cmd.Parameters.Add("@TipoRequeriminto", SqlDbType.Int).Value = request.TipoRequerimiento;


                        if (request.Estado == null || request.Estado == 0)
                            cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = DBNull.Value;
                        else
                            cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = request.Estado;


                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    oRequerimientoGestionReporteFiltroResponse = new RequerimientoGestionReporteFiltroResponse();


                                    if (!reader.IsDBNull(0))
                                        oRequerimientoGestionReporteFiltroResponse.NroRequerimiento = reader.GetString(0);

                                    if (!reader.IsDBNull(1))
                                        oRequerimientoGestionReporteFiltroResponse.FechaRegistro = reader.GetDateTime(1);

                                    if (!reader.IsDBNull(2))
                                        oRequerimientoGestionReporteFiltroResponse.FechaAsignacion = reader.GetDateTime(2);

                                    if (!reader.IsDBNull(3))
                                        oRequerimientoGestionReporteFiltroResponse.FechaGestion = reader.GetDateTime(3);

                                    if (!reader.IsDBNull(4))
                                        oRequerimientoGestionReporteFiltroResponse.FechaDerivacionOCM = reader.GetDateTime(4);

                                    if (!reader.IsDBNull(5))
                                        oRequerimientoGestionReporteFiltroResponse.FechaSinNotificacion = reader.GetDateTime(5);

                                    if (!reader.IsDBNull(6))
                                        oRequerimientoGestionReporteFiltroResponse.FechaAtencion = reader.GetDateTime(6);

                                    if (!reader.IsDBNull(7))
                                        oRequerimientoGestionReporteFiltroResponse.DetalleReclamo = reader.GetString(7);

                                    if (!reader.IsDBNull(8))
                                        oRequerimientoGestionReporteFiltroResponse.Estado = reader.GetString(8);

                                    if (!reader.IsDBNull(9))
                                        oRequerimientoGestionReporteFiltroResponse.Origen = reader.GetString(9);

                                    if (!reader.IsDBNull(10))
                                        oRequerimientoGestionReporteFiltroResponse.UsuarioAsignado = reader.GetString(10);

                                    if (!reader.IsDBNull(11))
                                        oRequerimientoGestionReporteFiltroResponse.DiasTranscurrido = reader.GetInt32(11);


                                    LstRequerimientoGestionReporteFiltroResponse.Add(oRequerimientoGestionReporteFiltroResponse);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw ex;
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return LstRequerimientoGestionReporteFiltroResponse;
        }



        public List<RequerimientoBE> RequerimientosListarCartaNoAdeudo(RequestRequerimiento oBE)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            RequerimientoBE objRequerimiento = null;
            List<RequerimientoBE> lstRequerimiento = new List<RequerimientoBE>();

            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_REQUERIMIENTO_OBTENER_CARTA_NO_ADEUDO]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@TipoDocumento", SqlDbType.Int).Value = oBE.TipoDocumento;
                        cmd.Parameters.Add("@NroDocumento", SqlDbType.VarChar, 20).Value = (oBE.NroDocumento == null ? "" : oBE.NroDocumento);

                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    objRequerimiento = new RequerimientoBE();
                                    if (!Convert.IsDBNull(reader["NroReclamo"])) objRequerimiento.NroReclamo = Convert.ToString(reader["NroReclamo"]);
                                    if (!Convert.IsDBNull(reader["FechaRegistro"])) objRequerimiento.FechaRegistro = reader.GetDateTime("FechaRegistro");
                                    if (!Convert.IsDBNull(reader["NroDocumento"])) objRequerimiento.NroDocumento = Convert.ToString(reader["NroDocumento"]);
                                    if (!Convert.IsDBNull(reader["NombreApellidos"])) objRequerimiento.NombresApellidos = Convert.ToString(reader["NombreApellidos"]);
                                    if (!Convert.IsDBNull(reader["UsuarioAsignado"])) objRequerimiento.UsuarioAsignado = Convert.ToString(reader["UsuarioAsignado"]);
                                    if (!Convert.IsDBNull(reader["FechaAtencion"])) objRequerimiento.FechaAtencion = Convert.ToString(reader["FechaAtencion"]);
                                    if (!Convert.IsDBNull(reader["CodTipoRequerimiento"])) objRequerimiento.CodTipoRequerimiento = Convert.ToInt32(reader["CodTipoRequerimiento"]);
                                    if (!Convert.IsDBNull(reader["TipoRequerimiento"])) objRequerimiento.TipoRequerimiento = Convert.ToString(reader["TipoRequerimiento"]);
                                    if (!Convert.IsDBNull(reader["Agencia"])) objRequerimiento.Agencia = Convert.ToString(reader["Agencia"]);
                                    if (!Convert.IsDBNull(reader["Estado"])) objRequerimiento.DescripcionEstado = Convert.ToString(reader["Estado"]);

                                    lstRequerimiento.Add(objRequerimiento);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw ex;
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return lstRequerimiento;
        }
    }
}
