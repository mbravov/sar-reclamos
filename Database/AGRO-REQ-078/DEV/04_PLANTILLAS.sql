USE DBAgrSAR
GO

DELETE FROM Plantilla WHERE CodTipoPlantilla in (4,5,6)
DELETE FROM  Recurso WHERE CodTabla = 5 AND LaserficheID in (40587, 40406, 41165)
  
INSERT INTO Plantilla (CodTipoPlantilla, Estado, UsuarioRegistro, FechaRegistro ) VALUES 
(4, 1, USER, GETDATE());

INSERT INTO Recurso (CodTabla, ReferenciaID, LaserficheID, Descripcion, Estado, UsuarioRegistro, FechaRegistro) VALUES 
(5, @@IDENTITY, 40587, 'Modelo CNA 2021_Ley 31143', 1, USER, GETDATE());

INSERT INTO Plantilla (CodTipoPlantilla, Estado, UsuarioRegistro, FechaRegistro ) VALUES 
(5, 1, USER, GETDATE());

INSERT INTO Recurso (CodTabla, ReferenciaID, LaserficheID, Descripcion, Estado, UsuarioRegistro, FechaRegistro) VALUES 
(5, @@IDENTITY, 40406, 'Modelo LEV GART_Ley 31143', 1, USER, GETDATE());

INSERT INTO Plantilla (CodTipoPlantilla, Estado, UsuarioRegistro, FechaRegistro ) VALUES 
(6, 1, USER, GETDATE());

INSERT INTO Recurso (CodTabla, ReferenciaID, LaserficheID, Descripcion, Estado, UsuarioRegistro, FechaRegistro) VALUES 
(5, @@IDENTITY, 41165, 'Modelo no procede CNA_aval a terceros', 1, USER, GETDATE());
