﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades
{
    public class RequerimientoDetalleBE : RquerimientoMovimiento
	{
		public int? CodRequerimientoDetalle { get; set; }
		//public int? CodRequerimiento { get; set; }
		public int TipoDocumento { get; set; }
		public string TipoDocumentoDescripcion { get; set; }
		public string NroDocumento { get; set; }
		public string PrimerApellido { get; set; }
		public string SegundoApellido { get; set; }
		public string Nombres { get; set; }
		public string NombresApellidos { get; set; }
		public string RazonSocial { get; set; }
		public string CodUbigeo { get; set; }
		public string Direccion { get; set; }
		public string Referencia { get; set; }
		public string CorreoElectronico { get; set; }
		public string Celular { get; set; }
		public string Telefono { get; set; }
		public string UsuarioAsignado { get; set; }
        public string UsuarioWebAsignado { get; set; }
        public string CodDepartamento { get; set; }
		public string CodProvincia { get; set; }
		public string CodDistrito { get; set; }
		public string Departamento { get; set; }
		public string Provincia { get; set; }
		public string Distrito { get; set; }
		public string UsuarioAsignar { get; set; }
		public string UsuarioAsignadoNotificacion { get; set; }

	}
}
