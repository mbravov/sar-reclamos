  USE DBAgrSARCalidad
  GO
  
  --
  INSERT INTO Plantilla (CodTipoPlantilla, Estado, UsuarioRegistro, FechaRegistro )
  VALUES (1, 1, USER, GETDATE());

  INSERT INTO Recurso (CodTabla, ReferenciaID, LaserficheID, Descripcion, Estado, UsuarioRegistro, FechaRegistro)
  VALUES (5, @@IDENTITY, 33229, 'Resoluci�n 424-2019-MINAGRI �ltimo', 1, USER, GETDATE());
  --
  INSERT INTO Plantilla (CodTipoPlantilla, Estado, UsuarioRegistro, FechaRegistro )
  VALUES (1, 1, USER, GETDATE());

  INSERT INTO Recurso (CodTabla, ReferenciaID, LaserficheID, Descripcion, Estado, UsuarioRegistro, FechaRegistro)
  VALUES (5, @@IDENTITY, 33228, 'Resoluci�n 059-2020-MINAGRI �ltimo', 1, USER, GETDATE());
  --
  INSERT INTO Plantilla (CodTipoPlantilla, Estado, UsuarioRegistro, FechaRegistro )
  VALUES (1, 1, USER, GETDATE());

  INSERT INTO Recurso (CodTabla, ReferenciaID, LaserficheID, Descripcion, Estado, UsuarioRegistro, FechaRegistro)
  VALUES (5, @@IDENTITY, 33227, 'No procede levantamiento de hipoteca', 1, USER, GETDATE());
  --
  INSERT INTO Plantilla (CodTipoPlantilla, Estado, UsuarioRegistro, FechaRegistro )
  VALUES (1, 1, USER, GETDATE());

  INSERT INTO Recurso (CodTabla, ReferenciaID, LaserficheID, Descripcion, Estado, UsuarioRegistro, FechaRegistro)
  VALUES (5, @@IDENTITY, 33226, 'En blanco', 1, USER, GETDATE());
  --
  INSERT INTO Plantilla (CodTipoPlantilla, Estado, UsuarioRegistro, FechaRegistro )
  VALUES (1, 1, USER, GETDATE());

  INSERT INTO Recurso (CodTabla, ReferenciaID, LaserficheID, Descripcion, Estado, UsuarioRegistro, FechaRegistro)
  VALUES (5, @@IDENTITY, 33225, 'Campa�a de Refinanciaci�n de Condiciones Especiales', 1, USER, GETDATE());
  --
  INSERT INTO Plantilla (CodTipoPlantilla, Estado, UsuarioRegistro, FechaRegistro )
  VALUES (1, 1, USER, GETDATE());

  INSERT INTO Recurso (CodTabla, ReferenciaID, LaserficheID, Descripcion, Estado, UsuarioRegistro, FechaRegistro)
  VALUES (5, @@IDENTITY, 33224, 'Beneficiario de la Ley 30893 �ltimo', 1, USER, GETDATE());
   --
  INSERT INTO Plantilla (CodTipoPlantilla, Estado, UsuarioRegistro, FechaRegistro )
  VALUES (2, 1, USER, GETDATE());

  INSERT INTO Recurso (CodTabla, ReferenciaID, LaserficheID, Descripcion, Estado, UsuarioRegistro, FechaRegistro)
  VALUES (5, @@IDENTITY, 33222, '01-Constancia de No Adeudo �ltimo', 1, USER, GETDATE());
  
  --
  INSERT INTO Plantilla (CodTipoPlantilla, Estado, UsuarioRegistro, FechaRegistro )
  VALUES (3, 1, USER, GETDATE());

  INSERT INTO Recurso (CodTabla, ReferenciaID, LaserficheID, Descripcion, Estado, UsuarioRegistro, FechaRegistro)
  VALUES (5, @@IDENTITY, 33223, '02-No procede Constancia de No Adeudo �ltimo', 1, USER, GETDATE());
 
