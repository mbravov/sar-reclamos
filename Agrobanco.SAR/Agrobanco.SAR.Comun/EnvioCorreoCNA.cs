﻿using Agrobanco.SAR.Comun.Laserfiche;
using Agrobanco.Security.Criptography;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;

namespace Agrobanco.SAR.Comun
{
    public class EnvioCorreoCNA
    {
        [Import(typeof(IValueReader), AllowRecomposition = true)]
        private IValueReader _decriptService;
        private int CorreoEnableEncript =   Convert.ToInt16(ApplicationKeys.CorreoApplicationNameCNAEnableEncrip);
        private int EnableEncriptHab = 1;
        private int EnableEncriptDes = 0;

        public void CorreoAcces(out string _from, out string _userName, out string _password, out string _hostName, out int _port)
        {
            _from = "";
            _userName = "";
            _password = "";
            _hostName = "";
            _port = 0;
            try
            {
                Compose();
                _decriptService.Application = ApplicationKeys.CorreoApplicationNameCNA;
                _decriptService.ClaveEncriptado = ApplicationKeys.RegeditPass;
                if (CorreoEnableEncript == EnableEncriptHab)
                {
                    _from = _decriptService.ReadValue("From");// _decriptService.ReadValue("Usuario");
                    _userName = _decriptService.ReadValue("Usuario");// _decriptService.ReadValue("Usuario");
                    _password = _decriptService.ReadValue("Clave"); //_decriptService.ReadValue("Clave");
                    _hostName = _decriptService.ReadValue("Host");//_decriptService.ReadValue("Server"); // "41.50.13.193";
                    _port = Convert.ToInt32(_decriptService.ReadValue("Puerto"));

                }
                else if (CorreoEnableEncript == EnableEncriptDes)
                {
                    _from = ReadKey("From", ApplicationKeys.CorreoApplicationNameCNA);// _decriptService.ReadValue("Usuario");
                    _userName = ReadKey("Usuario", ApplicationKeys.CorreoApplicationNameCNA);// _decriptService.ReadValue("Usuario");
                    _password = ReadKey("Clave", ApplicationKeys.CorreoApplicationNameCNA); //_decriptService.ReadValue("Clave");
                    _hostName = ReadKey("Host", ApplicationKeys.CorreoApplicationNameCNA);//_decriptService.ReadValue("Server"); // "41.50.13.193";
                    _port = Convert.ToInt32(ReadKey("Puerto", ApplicationKeys.CorreoApplicationNameCNA));
                }
            }
            catch (Exception e)
            {
                //GenerarLog objLOG = new GenerarLog();
                //string detalleError = "Conexion CORREO" + " - " +
                //    DateTime.Now + "\n" + "\n" +
                //    e.Message + "\n" +
                //    "-------------------------------------------------------------------------------------------------------------";
                //objLOG.GenerarArchivoLog(detalleError);
                throw e;
            }
        }

        private void Compose()
        {
            var encryptCatalog = new AssemblyCatalog(System.Reflection.Assembly.GetAssembly(typeof(IValueReader)));
            var catalog = new AggregateCatalog(encryptCatalog);
            var container = new CompositionContainer(catalog);
            container.ComposeParts(this);
        }



        public bool EnviarCorreo(List<String> para, List<String> copia, string cuerpo,string Asunto)
        {
            CorreoAcces(out string _from, out string _userName, out string _password, out string _hostName, out int _port);

            bool rpta = false;
            MailMessage message = new MailMessage();
            message.From = new MailAddress(_from);

            try
            {
                var body = cuerpo;
                foreach (var item in para)
                {
                    message.To.Add(new MailAddress(item));
                }
                if (copia != null)
                {
                    foreach (var item in copia)
                    {
                        message.CC.Add(new MailAddress(item));
                    }
                }

                message.Subject = Asunto;
                message.Body = body;
                message.IsBodyHtml = true;
                message.Subject = Asunto;

                var smtpClient = new SmtpClient(_hostName)
                {
                    Port = _port,
                    Credentials = new NetworkCredential(_userName, _password)
                };
                smtpClient.Send(message);
                rpta = true;
            }
            catch (Exception e)
            {
                //GenerarLog objLOG = new GenerarLog();
                //string detalleError = "Envio Correo" + " - " +
                //    DateTime.Now + "\n" + "\n" +
                //    e.Message + "\n" +
                //    "-------------------------------------------------------------------------------------------------------------";
                //objLOG.GenerarArchivoLogApi(detalleError);
                return rpta = false;
            }
            return rpta;
        }

        public bool EnviarCorreoConsulta(List<String> para, List<String> copia, string cuerpo, string Asunto, List<Documento> lstDocumento)
        {
            CorreoAcces(out string _from, out string _userName, out string _password, out string _hostName, out int _port);

            bool rpta = false;
            MailMessage message = new MailMessage();
            message.From = new MailAddress(_from);

            MemoryStream filename = new MemoryStream();

            try
            {
                var body = cuerpo;
                foreach (var item in para)
                {
                    message.To.Add(new MailAddress(item));
                }
                if (copia != null)
                {
                    foreach (var item in copia)
                    {
                        message.CC.Add(new MailAddress(item));
                    }
                }

                message.Subject = Asunto;
                message.Body = body;
                message.IsBodyHtml = true;
                message.Subject = Asunto;


                if (lstDocumento.Count >= 1)
                {
                    message.Attachments.Add(new Attachment(HelperIT.Base64ToMemoryStream(lstDocumento[0].ArchivoBytes), lstDocumento[0].Nombre + "." + lstDocumento[0].Extension, TypesConstants.GetTypeMIME(lstDocumento[0].Extension)));
                }
                if (lstDocumento.Count >= 2)
                {
                    message.Attachments.Add(new Attachment(HelperIT.Base64ToMemoryStream(lstDocumento[1].ArchivoBytes), lstDocumento[1].Nombre + "." + lstDocumento[1].Extension, TypesConstants.GetTypeMIME(lstDocumento[1].Extension)));
                }

                var smtpClient = new SmtpClient(_hostName)
                {
                    Port = _port,
                    Credentials = new NetworkCredential(_userName, _password)
                };
                smtpClient.Send(message);
                rpta = true;
            }
            catch (Exception e)
            {
                //GenerarLog objLOG = new GenerarLog();
                //string detalleError = "Envio Correo" + " - " +
                //    DateTime.Now + "\n" + "\n" +
                //    e.Message + "\n" +
                //    "-------------------------------------------------------------------------------------------------------------";
                //objLOG.GenerarArchivoLogApi(detalleError);
                return rpta = false;
            }
            return rpta;
        }



        private string ReadKey(string value, string folder)
        {
            Registry.LocalMachine.OpenSubKey("SOFTWARE", true);
            RegistryKey masterKey = Registry.LocalMachine.CreateSubKey("SOFTWARE\\AGROBANCO\\" + folder);
            string valueret = "";
            if (masterKey != null)
            {
                valueret = masterKey.GetValue(value).ToString();
            }
            masterKey.Close();
            return valueret;
        }
    }
}
