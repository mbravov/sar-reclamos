﻿using Agrobanco.Control.DTO;
using Agrobanco.Reclamaciones.API.Comun;
using Agrobanco.Reclamaciones.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.Reclamaciones.API.Providers
{
    public class ProxySARAPI
    {
        public List<UbigeoBE> Departamento(string token)
        {
            var respuesta =  Services.Post<List<UbigeoBE>>(null, "agrobanco/reclamos/departamento", token);
            return respuesta;
        }

        public List<UbigeoBE> Provincia(string token, UbigeoBE oBE)
        {
            var respuesta = Services.Post<List<UbigeoBE>>(oBE, "agrobanco/reclamos/provincia", token);
            return respuesta;
        }

        public List<UbigeoBE> Distrito(string token, UbigeoBE oBE)
        {
            var respuesta = Services.Post<List<UbigeoBE>>(oBE, "agrobanco/reclamos/distrito", token);
            return respuesta;
        }

        public ResponseSuccess Requerimiento(string token, RequerimientoBE oRequerimientoBE)
        {
            var respuesta = Services.Post<ResponseSuccess>(oRequerimientoBE, "agrobanco/reclamos/requerimiento", token);
            return respuesta;
        }

        public RequerimientoBE RequerimientoDetalle(string token, RequerimientoBE oRequerimientoBE)
        {
            var respuesta = Services.Post<RequerimientoBE>(oRequerimientoBE, "agrobanco/reclamos/requerimiento/detalle", token);
            return respuesta;
        }

        public List<AgenciaBE> Agencia(string token)
        {
            var respuesta = Services.Post<List<AgenciaBE>>(null, "agrobanco/reclamos/agencia", token);
            return respuesta;
        }

        public List<ParametroBE> ParametroGrupo(string token, Parametro oParametro)
        {
            var respuesta = Services.Post<List<ParametroBE>>(oParametro, "agrobanco/reclamos/parametros", token);
            return respuesta;
        }

        public List<RecursoBE> Recurso(string token, RecursoBE oBE)
        {
            var respuesta = Services.Post<List<RecursoBE>>(oBE, "agrobanco/reclamos/recurso", token);
            return respuesta;
        }

        public ResponseSuccess RequerimientoEnvio(string token, RequerimientoBE oRequerimientoBE)
        {
            var respuesta = Services.Post<ResponseSuccess>(oRequerimientoBE, "agrobanco/reclamos/requerimiento/reenviar/correo", token);
            return respuesta;
        }

        public List<Usuario> ConsultaRegistrar(string token, ConsultaBE oConsulta)
        {
            var respuesta = Services.Post<List<Usuario>>(oConsulta, "agrobanco/reclamos/consultaregistrar", token);
            return respuesta;
        }

        public List<ParametroBE> PreguntasFrecuentes(string token, Parametro oParametro)
        {
            var respuesta = Services.Post<List<ParametroBE>>(oParametro, "agrobanco/reclamos/preguntasfrecuentes", token);
            return respuesta;
        }
        public AuthAccessResponse TokenSARApi(AccessDTO access, string appKey, string appCode, out string authorizationResponse)
        {
            var respuesta = Services.PostHeaderApp<AuthAccessResponse>(access, "acceso", appKey, appCode,out authorizationResponse);
            return respuesta;
        }
    }
}
