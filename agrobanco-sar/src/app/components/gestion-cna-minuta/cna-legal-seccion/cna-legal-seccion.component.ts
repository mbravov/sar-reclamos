import { Component, Input, OnInit, ElementRef, ViewChild, Output, EventEmitter } from '@angular/core';
import { RequerimientoService } from '../../../services/requerimiento.service';
import { RecursoEmisionService } from '../../../services/recurso-emision.service';
import { SecurityService } from '../../../services/security.service';
import { DialogService } from '../../../services/comun/dialog.service';
import { ConfiguracionAccionService } from '../../../services/configuracion-accion.service';
import { Constantes } from 'src/app/comun/constantes';
import { forkJoin, Subject } from 'rxjs';
import { CnaMinutaOcmService } from '../../../services/cna-minuta-ocm.service';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import * as moment from 'moment';

@Component({
  selector: 'app-cna-legal-seccion',
  templateUrl: './cna-legal-seccion.component.html',
  styleUrls: ['./cna-legal-seccion.component.css']
})
export class CnaLegalSeccionComponent implements OnInit {


  @ViewChild('inputfileMinuta', {static: false}) inputfileMinuta!: ElementRef;
  @ViewChild('inputtextMinuta', {static: false}) inputtextMinuta!: ElementRef;

  @ViewChild('inputFileCE', {static: false}) inputFileCE!: ElementRef;
  @ViewChild('inputTextCE', {static: false}) inputTextCE!: ElementRef;

  recursos!:any[];

  @Input() creditoClienteDetalle!: any;
  @Input() items!: any[];
  @Input() permisoLegal!:number;
  @Input() documentosMinuta:any;
  @Input() documentosCartaEntrega:any;

  @Output() cargarPaginaLegalOut = new EventEmitter();
  @Output() FechaEntregaOlvaOut = new EventEmitter();

  loading:boolean = false;

  selectedPlantilla: any;

  MAXIMO_TAMANIO_BYTES = 2560000
  Archivo1:any;
  NombreArchivo1:any;

  Archivo2:any;
  NombreArchivo2:any;


  list  = new Array();
  list$ = new Subject<any[]>();

  IdConfiguracionAccion:any;

  seccionLegal:any;


  totalRecurso:any;
  permiso:any;

  events: string[] = [];


  constructor(
    private _cnaMinutaOCMService : CnaMinutaOcmService,
    private _requerimientoService : RequerimientoService,
    private _recursoEmisionService: RecursoEmisionService,
    private _securityService : SecurityService,
    private _dialogService: DialogService,
    private _configuracionAccion : ConfiguracionAccionService,
  ) { }

  ngOnInit(): void {
    this.pageLoad();

    this.totalRecurso = this.creditoClienteDetalle.recursoEmision.filter((x:any)=>x.tipoRecursoID==5 || x.tipoRecursoID==6).length;
   // this.permiso = this.items.filter((x:any) => x.tipoAccion==10).length;
    console.log("permiiso",this.permiso);
  }

  addEvent(event: MatDatepickerInputEvent<Date>) {

    //const FechaEntregaOlva = moment(event.value).format("DD/MM/yyyy");
    this.FechaEntregaOlvaOut.emit(moment(event.value).format("DD/MM/yyyy"));
  //  this.events.push(`${type}: ${event.value}`);
  }


  pageLoad(){
    this.loading = true;
    var oRecursoBE = {
      "CodTabla" : Constantes.Plantilla,
      "CodTipoPlantilla" :   ""+Constantes.PlantillaGestionCNAMinuta
    };
    forkJoin([
      this._requerimientoService.ObtenerPlantillas(oRecursoBE)
    ]).subscribe( (data: any) => {
        this.recursos = data[0].response;
        this.loading = false;
    }, (err) => {
      this.loading = false;
      console.log(err);
    });
  }

  DescargarPlantilla(): void
{
debugger;
  if(this.selectedPlantilla==null){
    this._dialogService.openMsgErrorDialog("Seleccione una plantilla");
    return;
  }

  // if(this.selectedPlantilla==""){
  //   this._dialogService.openMsgErrorDialog("Seleccione una plantilla");
  //   return;
  // }

  var oBE = {
    "LaserficheID": this.selectedPlantilla,
    "IdEmision":  +this.creditoClienteDetalle.idEmision,
    "Perfil" : this._securityService.leerPerfil(),
    "NroReclamo":  "",
    "SaldoCredito": 0,
    "NroCredito": "",
    "EstadoCredito":  ""
  };
  this.loading = true;
  this._cnaMinutaOCMService.GenerarDocumento(oBE).subscribe(result=>{
    this.loading = false;
    const FechaActual = new Date();
    const AnioActual = FechaActual.getFullYear();
    const MesActual = (FechaActual.getMonth()<=9? '0'+ (FechaActual.getMonth()+1): FechaActual.getMonth() + 1); //FechaActual.getMonth() + 1;
    const DiaActual = (FechaActual.getDate()<=9? '0'+FechaActual.getDate(): FechaActual.getDate());
    const HoraActual = (FechaActual.getHours()<=9? '0'+FechaActual.getHours(): FechaActual.getHours()); //FechaActual.getHours();
    const MinutoActual = (FechaActual.getMinutes()<=9? '0'+FechaActual.getMinutes(): FechaActual.getMinutes()); //FechaActual.getMinutes();
    const SegundosActual = (FechaActual.getSeconds()<=9? '0'+FechaActual.getSeconds(): FechaActual.getSeconds()); //FechaActual.getMinutes(); //FechaActual.getSeconds();

    const NombreArchivo = "Minuta_" + AnioActual + MesActual + DiaActual + "T" + HoraActual + MinutoActual + SegundosActual;
    if(result==""){
      this._dialogService.openMsgErrorDialog("Hubo un problema al intentar generar el documento");
    }
    else{
      //this.Descargar(result, NombreArchivo);
      this.Descargar(result.response,NombreArchivo,'application/vnd.openxmlformats-officedocument.wordprocessingml.document');
    }

  },
    (err)=>{
      this.loading = false;
      console.log(err);
    }
  );
}

Descargar(base64String:any, fileName:any,contentType:any) {

  const blobData = this.convertBase64ToBlobData(base64String,contentType);
  const blob = new Blob([blobData], { type: contentType });
      const url = window.URL.createObjectURL(blob);
      // window.open(url);
      const link = document.createElement('a');
      link.href = url;
      link.download = fileName;
      link.click();
}

convertBase64ToBlobData(base64Data: string, contentType: string = 'image/png', sliceSize = 2024) {
  const byteCharacters = atob(base64Data);
  const byteArrays = [];
  for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    const slice = byteCharacters.slice(offset, offset + sliceSize);
    const byteNumbers = new Array(slice.length);
    for (let i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    byteArrays.push(byteArray);
  }


  const blob = new Blob(byteArrays, { type: contentType });
  return blob;
}

handleUpload2(event:any) {
  const file = event.target.files[0];
  if(file==null || file.length <=0){
     this.Archivo1 = null;
     this.NombreArchivo1 = null;
    return;
  }

  if (file.size > this.MAXIMO_TAMANIO_BYTES) {
     const tamanioEnMb = this.MAXIMO_TAMANIO_BYTES / 1024000;

     this.Archivo1 =  null;
     this.NombreArchivo1 = null;
     this.inputfileMinuta.nativeElement.value = "";
     this.inputtextMinuta.nativeElement.value = "";
     this._dialogService.openMsgErrorDialog(`El tamaño máximo es ${tamanioEnMb} MB`);
     return;
   }

   var allowedExtensions = /(.pdf)$/i;
   if(!allowedExtensions.exec(file.name)){
    this._dialogService.openMsgErrorDialog("Formato no válido");
    this.Archivo1 = null;
    this.NombreArchivo1 = null;
    this.inputfileMinuta.nativeElement.value = "";
    this.inputtextMinuta.nativeElement.value = "";
    return;
   }

   const FileNamme = file.name;
   this.NombreArchivo1 = FileNamme;

   const reader = new FileReader();
   console.log("file",file);
   reader.readAsDataURL(file);
   reader.onload = () => {
     console.log("reader",reader);
     this.Archivo1 = reader.result || null;

       console.log(reader.result);
   };
}


subirMinuta(){
  debugger;
    if(!this.Archivo1){
      this._dialogService.openMsgErrorDialog("Debe adjuntar la Minuta.");
      return;
    }
    let request = {
    "CodRecurso" : 0,
    "CodTabla" : 8,
    "ReferenciaID" : 0,
    "LaserficheID" : 0,
    "IdRecursoEmision" : 0,
    "IdEmision" : +this.creditoClienteDetalle.idEmision,
    "TipoRecursoID" : 5,
    "UsuarioRegistro" : this._securityService.leerUsuarioWeb(),
    "DNI" : ""+this.creditoClienteDetalle.numeroDocumento,
    "TipoDocumento": ""+this.creditoClienteDetalle.tipoDocumento,
    "Archivo" : this.Archivo1,
    "NombreArchivo" : this.NombreArchivo1
    };

          this._dialogService
            .openConfirmDialog('¿Está seguro de subir el documento de Minuta? ')
            .afterClosed()
            .subscribe((res) => {
              if (res) {
                this.loading = true;
                  this._recursoEmisionService.registrar(request).subscribe((data:any)=>{

                    if(data.response.success.resultado){

                      var request = {
                        "IdConfiguracionAccion" : this.IdConfiguracionAccion,
                        "GrupoFiltro" : null,
                        "emision": {
                          "IdEmision" : +this.creditoClienteDetalle.idEmision,
                          "IdCreditoCliente"  : +this.creditoClienteDetalle.idCreditoCliente,
                          "Estado"  : this.creditoClienteDetalle.idEstado,
                          "TipoFlujo"  :1,
                          "FechaEntregaOlva" : null,
                          "MedioNotificacion": null,
                          "Email" : null,
                          "Whatsapp" : null,
                          "FechaCargoEntrega" : null,
                          "UsuarioRegistro" :  this._securityService.leerUsuarioWeb(),
                          "UsuarioModificacion" :  this._securityService.leerUsuarioWeb()
                        },
                        "derivacion": {
                          "IdDerivacion" : +this.creditoClienteDetalle.idDerivacion,
                          "IdEmision" : +this.creditoClienteDetalle.idEmision,
                          "Perfil" : this._securityService.leerPerfil(),
                          "Estado" : this.creditoClienteDetalle.idEstado,
                          "PerfilDerivado" : null,
                          "FechaDerivado" : null,
                          "CodAgencia" : null,
                          "UsuarioRegistro" : this._securityService.leerUsuarioWeb(),
                          "UsuarioModificacion":  this._securityService.leerUsuarioWeb()
                        },
                        "comentarioEmision": {

                        }
                      };

                      this._configuracionAccion.accion(request).subscribe((data:any)=>{
                        console.log("this._configuracionAccion.accion - Subir Archivo",data);
                        this.loading = false;
                        this.Archivo1 = null;
                        this.NombreArchivo1 = null;
                        this.cargarPaginaLegalOut.emit();
                        this._dialogService.openMsgSuccessDialog('Se subió correctamente el archivo');
                      }, (error) =>{
                        console.log(error);
                        this.Archivo1 = null;
                        this.NombreArchivo1 = null;
                        this.cargarPaginaLegalOut.emit();
                        this._dialogService.openMsgErrorDialog('Hubo un problema en la configuración');
                      } );

                    }
                    else{
                      this.Archivo1 = null;
                      this.NombreArchivo1 = null;
                      this._dialogService.openMsgErrorDialog("Hubo un problema al intentar subir el archivo")
                    }

                  });
              }
            });
  }


  handleUpload(event:any) {
    const file = event.target.files[0];
    if(file==null || file.length <=0){
       this.Archivo2 = null;
       this.NombreArchivo2 = null;

       this.inputFileCE.nativeElement.value = "";
       this.inputTextCE.nativeElement.value = "";

      return;
    }

    if (file.size > this.MAXIMO_TAMANIO_BYTES) {
       const tamanioEnMb = this.MAXIMO_TAMANIO_BYTES / 1024000;

       this.Archivo2 =  null;
       this.NombreArchivo2 = null;
       this.inputFileCE.nativeElement.value = "";
       this.inputTextCE.nativeElement.value = "";
       this._dialogService.openMsgErrorDialog(`El tamaño máximo es ${tamanioEnMb} MB`);
       return;
     }

     var allowedExtensions = /(.pdf)$/i;
     if(!allowedExtensions.exec(file.name)){
      this._dialogService.openMsgErrorDialog("Formato no válido");
      this.Archivo2 = null;
      this.NombreArchivo2 = null;
      this.inputFileCE.nativeElement.value = "";
      this.inputTextCE.nativeElement.value = "";
      return;
     }

     const FileNamme = file.name;
     this.NombreArchivo2 = FileNamme;

     const reader = new FileReader();
     console.log("file",file);
     reader.readAsDataURL(file);
     reader.onload = () => {
       console.log("reader",reader);
       this.Archivo2 = reader.result || null;

         console.log(reader.result);
     };
  }

  subirCartaEntrega(){
    debugger;
      if(!this.Archivo2){
        this._dialogService.openMsgErrorDialog("Debe adjuntar la carta de entrega.");
        return;
      }
      let request = {
      "CodRecurso" : 0,
      "CodTabla" : 8,
      "ReferenciaID" : 0,
      "LaserficheID" : 0,
      "IdRecursoEmision" : 0,
      "IdEmision" : +this.creditoClienteDetalle.idEmision,
      "TipoRecursoID" : 6,
      "UsuarioRegistro" : this._securityService.leerUsuarioWeb(),
      "DNI" : ""+this.creditoClienteDetalle.numeroDocumento,
      "Archivo" : this.Archivo2,
      "NombreArchivo" : this.NombreArchivo2
      };

            this._dialogService
              .openConfirmDialog('¿Está seguro de subir el documento de carta de entrega? ')
              .afterClosed()
              .subscribe((res) => {
                if (res) {
                  this.loading = true;
                    this._recursoEmisionService.registrar(request).subscribe((data:any)=>{

                      if(data.response.success.resultado){

                        var request = {
                          "IdConfiguracionAccion" : this.IdConfiguracionAccion,
                          "GrupoFiltro" : null,
                          "emision": {
                            "IdEmision" : +this.creditoClienteDetalle.idEmision,
                            "IdCreditoCliente"  : +this.creditoClienteDetalle.idCreditoCliente,
                            "Estado"  : this.creditoClienteDetalle.idEstado,
                            "TipoFlujo"  :1,
                            "FechaEntregaOlva" : null,
                            "MedioNotificacion": null,
                            "Email" : null,
                            "Whatsapp" : null,
                            "FechaCargoEntrega" : null,
                            "UsuarioRegistro" :  this._securityService.leerUsuarioWeb(),
                            "UsuarioModificacion" :  this._securityService.leerUsuarioWeb()
                          },
                          "derivacion": {
                            "IdDerivacion" : +this.creditoClienteDetalle.idDerivacion,
                            "IdEmision" : +this.creditoClienteDetalle.idEmision,
                            "Perfil" : this._securityService.leerPerfil(),
                            "Estado" : this.creditoClienteDetalle.idEstado,
                            "PerfilDerivado" : null,
                            "FechaDerivado" : null,
                            "CodAgencia" : null,
                            "UsuarioRegistro" : this._securityService.leerUsuarioWeb(),
                            "UsuarioModificacion":  this._securityService.leerUsuarioWeb()
                          },
                          "comentarioEmision": {

                          }
                        };

                        this._configuracionAccion.accion(request).subscribe((data:any)=>{
                          console.log("this._configuracionAccion.accion - Subir Archivo",data);
                          this.loading = false;
                          this.Archivo2 = null;
                          this.NombreArchivo2 = null;
                          this.cargarPaginaLegalOut.emit();
                          this._dialogService.openMsgSuccessDialog('Se subió correctamente el archivo');
                        }, (error) =>{
                          console.log(error);
                          this.Archivo2 = null;
                          this.NombreArchivo2 = null;
                          this.cargarPaginaLegalOut.emit();
                          this._dialogService.openMsgSuccessDialog('Se subió correctamente el archivo');
                        } );

                      }
                      else{
                        this.cargarPaginaLegalOut.emit();
                        this._dialogService.openMsgErrorDialog("Hubo un problema al intentar subir el archivo")
                      }

                    });
                }
              });
    }


    eliminarArchivoLaserFiche(idLaserficheCNA:any = null) : void {

      this._dialogService
        .openConfirmDialog('¿Seguro que desea eliminar el documento?')
        .afterClosed()
        .subscribe((res) => {
          if (res) {
            // if(this.CNALaserFicheID!=null)
            //     idLaserficheCNA = this.CNALaserFicheID;

            const oBE = {
              "LaserficheID": ""+idLaserficheCNA
              };
              this.loading = true;
              this._requerimientoService.eliminarArchivo(oBE).subscribe(result=>{
                this.loading = false;

                console.log("eliminarArchivo",result);
                if(result.response.resultado==1){


                  this.cargarPaginaLegalOut.emit();

                  this._dialogService.openMsgSuccessDialog("Se eliminó correctamente");
                }
                else{
                  this.cargarPaginaLegalOut.emit();
                  this._dialogService.openMsgErrorDialog("Se presentó un problema para eliminar el documento");
                }

              },
              (err)=>{
                this.loading = false;
                console.log(err);
              }
              );

          }
     });

     }


     DescargarArchivoLaserFiche(LaserficheID:any,NameDocumento:any): void {
      this.loading = true;
      const params = {
        LaserficheID: ""+LaserficheID
      };
      this._requerimientoService.DescagarArchivos(params).subscribe((result) => {
        console.log('response DescargarArchivoLaserFiche', result.response);
        if(result.response!=null){
          if(result.response.length>0){
            for (let index = 0; index < result.response.length; index++) {
              const element = result.response[index];

              this.Descargar(element.archivoBytes,element.nombreDocumento,element.contentType);
            }
          }
        }
        this.loading = false;
      });
    }


    onClick(valor:any, valor2:any = null){
      console.log("onClick",valor);
      console.log("onClick",valor2);
      this.IdConfiguracionAccion = valor.idConfiguracionAccion;
      eval("this."+valor.metodo);
    }

}
