﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades.CNA_Minuta
{
    public class EmisionBE
    {
        public int IdEmision { get; set; }
        public int IdCreditoCliente { get; set; }
        public int? Estado { get; set; }
        public int? TipoFlujo { get; set; }
        public DateTime? FechaEntregaOlva { get; set; }
        public string strFechaEntregaOlva { get; set; }
        public int? MedioNotificacion { get; set; }
        public string Email { get; set; }
        public string Whatsapp { get; set; }
        public DateTime? FechaCargoEntrega { get; set; }
        public DateTime? FechaRegistro { get; set; }
        public string UsuarioRegistro { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public string UsuarioModificacion { get; set; }
    }
}
