﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.Reclamaciones.API.Models
{
    public class AgenciaBE
    {
        public int CodAgencia { get; set; }
        public string Oficina { get; set; }
        public string Direccion { get; set; }
    }
}
