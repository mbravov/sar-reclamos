import { Injectable } from '@angular/core';
import { AppService } from '../app.service';
import { SecurityService } from './security.service';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RecursoEmisionService extends AppService {

  constructor(_securityService : SecurityService) {
    super(_securityService);
  }

  obtener<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${environment.UrlBase_SARAPI}/recurso-emision/obtener`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

  registrar<T>(datos:any):Observable<any>{
    console.log("input",datos);
    this.endpoint = `${environment.UrlBase_SARAPI}/recurso-emision/insertar`;
    console.log("endpoint",this.endpoint);
    return this.post(this.endpoint,datos);
  }

}
