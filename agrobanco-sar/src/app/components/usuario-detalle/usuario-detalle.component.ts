import { Component, OnInit } from '@angular/core';
import { RequerimientoService } from 'src/app/services/requerimiento.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DialogService } from 'src/app/services/comun/dialog.service'
import { ParametroService } from 'src/app/services/parametro.service';;
import { SecurityService } from 'src/app/services/security.service';
import { ActivatedRoute } from '@angular/router';
import { forkJoin } from 'rxjs';
import { CnaMinutaOcmService } from 'src/app/services/cna-minuta-ocm.service';

@Component({
  selector: 'app-usuario-detalle',
  templateUrl: './usuario-detalle.component.html',
  styleUrls: ['./usuario-detalle.component.css']
})
export class UsuarioDetalleComponent implements OnInit {

  form!: FormGroup;
  parametros:any[] = [];
  agencias:any[] = [];
  UsuarioDetalle:any;
  codUsuario: any;
  accion :string ="";
  loading:boolean = false;

  MaxLengthNombres: number = 100

  formSubmitted:boolean = false;

  constructor(
    private requerimientoServices : RequerimientoService,
    private parametroService: ParametroService,
    private dialogService: DialogService,
    private formBuilder: FormBuilder,
    private securityService:SecurityService,
    private cnaMinutaOcmService : CnaMinutaOcmService,
    private route: ActivatedRoute
  ) {
    this.codUsuario = this.route.snapshot.params.id;
    if(this.codUsuario != null || this.codUsuario != undefined){
      this.accion = "actualizar";
    }else{
      this.accion = "registrar";
    }
   }

  ngOnInit(): void {
    this.initForm();
    this.loadPage();
    this.obtenerUsuarioDetalle();

  }

  loadPage(): void {
    this.loading = true;
    var oBE = {};

    forkJoin([
      this.parametroService.ObtenerParametro(),
      this.cnaMinutaOcmService.ObtenerAgencias(oBE)
    ]).subscribe( (data: any) => {

        console.log(data);
        this.parametros = data[0].response;
        this.agencias = data[1].response;
        this.loading = false;

    }, (err) => {
      this.loading = false;
      console.error(err);
    });

  }


  initForm() : void {
    /*const perfil = null;
    if(this.form.controls['Perfil'] == null){

        perfil: null;

    }*/

    this.form = this.formBuilder.group({

      Perfil: [null, Validators.required],
      DescripcionPerfil: [{value: null, disabled: true}],
      CorreoElectronico: [null, [Validators.required,Validators.email]],
      UsuarioWeb: [null, Validators.required],
      NombresApellidos: [null, Validators.required],
      Estado: [{value: null, disabled: false}, Validators.required],
      //UsuarioRegistro: [null, Validators.required]
      CodAgencia : [null, Validators.required]
    });
  }



  obtenerUsuarioDetalle(): void {
    debugger;
    const codUsuario = {
      codUsuario: +this.codUsuario,
    };

    this.requerimientoServices
      .ObtenerUsuarioDetalle(codUsuario)
      .subscribe((result) => {
        if(result.response){

          this.UsuarioDetalle = result.response;



          this.form.patchValue({
            Perfil : this.UsuarioDetalle.codPerfil
          });
        if(this.UsuarioDetalle.codPerfil == "1")
         {
          this.form.controls['Perfil'].disable();

         }

         if((this.UsuarioDetalle.codPerfil == "1" ||
             this.UsuarioDetalle.codPerfil == "3" ||
             this.UsuarioDetalle.codPerfil == "4" ||
             this.UsuarioDetalle.codPerfil == "5" ||
             this.UsuarioDetalle.codPerfil == "6" ) &&
             this.UsuarioDetalle.estado  == "1")
         {
          this.form.controls['Estado'].setValue(this.UsuarioDetalle.estado);
          this.form.controls['DescripcionPerfil'].setValue(this.UsuarioDetalle.descripcionPerfil);
          if(this.UsuarioDetalle.codPerfil == "1")
             this.form.controls['Estado'].disable();
         }



         if(this.UsuarioDetalle.codPerfil == "2")
         {
          this.form.controls['Estado'].setValue(this.UsuarioDetalle.estado);
          this.form.controls['DescripcionPerfil'].setValue(this.UsuarioDetalle.descripcionPerfil);
         }

         if((this.UsuarioDetalle.codPerfil == "1" ||
             this.UsuarioDetalle.codPerfil == "3" ||
             this.UsuarioDetalle.codPerfil == "4" ||
             this.UsuarioDetalle.codPerfil == "5" ||
             this.UsuarioDetalle.codPerfil == "6" )  &&
             this.UsuarioDetalle.estado  == "0")
         {
          this.form.controls['Estado'].setValue(this.UsuarioDetalle.estado);
          this.form.controls['DescripcionPerfil'].setValue(this.UsuarioDetalle.descripcionPerfil);
         }

          console.log("this.UsuarioDetalle",this.UsuarioDetalle);

         if(this.UsuarioDetalle.codPerfil == "1" || this.UsuarioDetalle.codPerfil == "2"|| this.UsuarioDetalle.codPerfil == "3"|| this.UsuarioDetalle.codPerfil == "4")
         {
          this.form.controls['CodAgencia'].disable();
         }

         if(this.UsuarioDetalle.codPerfil == "5" || this.UsuarioDetalle.codPerfil == "6")
         {
          this.form.controls['CodAgencia'].enable();
          this.form.controls['CodAgencia'].setValue(this.UsuarioDetalle.codAgencia);
         }

        }

      });
  }

  onChangePerfil():void {

    const CodPerfil = this.form.controls['Perfil'].value;

    const Grupo = this.parametros.filter(x=>x.grupo==1400);
    if(CodPerfil ==  "1")
    {
      this.form.controls['DescripcionPerfil'].setValue(Grupo[0].valor);
    }
    if(CodPerfil ==  "2")
    {
      this.form.controls['DescripcionPerfil'].setValue(Grupo[1].valor);
    }
    if(CodPerfil ==  "3")
    {
      this.form.controls['DescripcionPerfil'].setValue(Grupo[2].valor);
    }
    if(CodPerfil ==  "4")
    {
      this.form.controls['DescripcionPerfil'].setValue(Grupo[3].valor);
    }
    if(CodPerfil ==  "5")
    {
      this.form.controls['DescripcionPerfil'].setValue(Grupo[4].valor);
    }
    if(CodPerfil ==  "6")
    {
      this.form.controls['DescripcionPerfil'].setValue(Grupo[5].valor);
    }

    if(CodPerfil == "1" || CodPerfil == "2"|| CodPerfil == "3"|| CodPerfil == "4")
    {
      this.form.controls['CodAgencia'].setValue("");
      this.form.controls['CodAgencia'].disable();
    }

    if(CodPerfil == "5" || CodPerfil == "6")
    {
      this.form.controls['CodAgencia'].setValue("");
      this.form.controls['CodAgencia'].enable();
    }

  }

  registrar():void{

    debugger;
    this.formSubmitted = true;

    if (this.form.invalid) {
      return;
    }
    const CodUsuario = this.codUsuario;
    const Perfil = this.form.controls['Perfil'].value;
    const DescripcionPerfil = this.form.controls['DescripcionPerfil'].value;
    const CorreoElectronico = this.form.controls['CorreoElectronico'].value;
    const NombresApellidos = this.form.controls['NombresApellidos'].value;
    const UsuarioWeb = this.form.controls['UsuarioWeb'].value;
    const Estado = this.form.controls['Estado'].value;
    const CodAgencia = this.form.controls['CodAgencia'].value;

    this.dialogService.openConfirmDialog('¿Desea ' + this.accion +' el usuario?')
    .afterClosed().subscribe(res => {
      if (res) {
        this.loading = true;
        const Entity =   {
          CodUsuario : +this.codUsuario,
          CodPerfil : this.form.controls['Perfil'].value,
          DescripcionPerfil : this.form.controls['DescripcionPerfil'].value,
          CorreoElectronico : this.form.controls['CorreoElectronico'].value,
          NombresApellidos : this.form.controls['NombresApellidos'].value,
          UsuarioWeb : this.form.controls['UsuarioWeb'].value,
          Estado : this.form.controls['Estado'].value,
          UsuarioRegistro : this.securityService.leerUsuarioWeb(),
          CodAgencia : this.form.controls['CodAgencia'].value
        }

        console.log("request",Entity);
        this.requerimientoServices.ActualizarUsuario(Entity).subscribe(result=>{
          debugger;
          console.log("response",result);

          this.loading = false;
          if(result.response){

            if(result.response == 'Se actualizó correctamente el usuario.'){
              this.form.disable();

              this.dialogService.openMsgSuccessDialogWithActionRedirect(result.response, '/usuario/');

            }
            else{
              this.dialogService.openMsgErrorDialog(result.response);

            }

          }
          else{
            this.dialogService.openMsgErrorDialog("Hubo un error al intentar actualizar el usuario");
          }


        }, (err)=>{
          this.loading = false;
          console.log(err);
        });

      }

    });

  }

}
