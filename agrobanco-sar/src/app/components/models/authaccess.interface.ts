export interface AuthAccess {
    correoelectronico : string,
    nombreusuario : string,
    codigousuario : string,
    numerodocumento : string
}