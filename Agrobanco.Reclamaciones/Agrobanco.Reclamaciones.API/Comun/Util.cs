﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.Reclamaciones.API.Comun
{
    public static class Util
    {
        public static string ObtenerValueFromKey(string keys)
        {
            var configuration = new ConfigurationBuilder()
                  .SetBasePath(Directory.GetCurrentDirectory())
                  .AddJsonFile("appsettings.json", false)
                  .Build();

            return configuration[keys];
        }
    }
}
