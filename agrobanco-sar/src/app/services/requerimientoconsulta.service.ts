import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { RequerimientoConsulta } from '../components/models/requerimientoconsulta.interface';
import { ajax } from 'rxjs/ajax';
import { Observable, ObservedValuesFromArray } from 'rxjs';
import { map } from 'rxjs/operators';

const UrlBase_SARAPI = environment.UrlBase_SARAPI;


@Injectable({
  providedIn: 'root'
})
export class RequerimientoconsultaService {

  constructor() { }
  TokenSGV = localStorage.getItem('TokenSGV');

  public buscarRequerimiento(filtros: RequerimientoConsulta){ 

    const headers = {
      'Content-Type': 'application/json',
      'Token': this.TokenSGV
    }

    const req = ajax.post(`${UrlBase_SARAPI}/requerimientoconsulta/listabusqueda`, filtros, headers);

    const data$ = new Observable(observer =>{
      req.subscribe(
        (res)=>{
          observer.next(res);
          observer.complete();
        },
        (err)=>{
          observer.error(err);
        }
      );
    });

    return data$;

  }

}
