import { Injectable } from '@angular/core';
import { AppService } from '../app.service';
import { SecurityService } from './security.service';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SeguimientoService extends AppService {

  constructor(securityService : SecurityService) {
    super(securityService);
  }

  ObtenerSeguimiento<T>(datos:any):Observable<any>{
    this.endpoint = `${environment.UrlBase_SARAPI}/Requerimiento/seguimiento`;
    return this.post(this.endpoint,datos);
  }

  ObtenerSeguimientoDetalle<T>(datos:any):Observable<any>{
    this.endpoint = `${environment.UrlBase_SARAPI}/Requerimiento/seguimiento-detalle`;
    return this.post(this.endpoint,datos);
  }
}
