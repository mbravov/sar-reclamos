import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { forkJoin } from 'rxjs';
import { Constantes } from 'src/app/comun/constantes';
import { ParametroService } from 'src/app/services/parametro.service';
import { RequerimientoService } from '../../services/requerimiento.service';
import { MatPaginator } from '@angular/material/paginator';
import { DialogService } from 'src/app/services/comun/dialog.service';
import { PlantillaService } from 'src/app/services/plantilla.service';
import { SecurityService } from 'src/app/services/security.service';

@Component({
  selector: 'app-gestion-plantilla',
  templateUrl: './gestion-plantilla.component.html',
  styleUrls: ['./gestion-plantilla.component.css']
})
export class GestionPlantillaComponent implements OnInit {

  @ViewChild('fileplantilla', { static: false }) InputVar!: ElementRef;
  @ViewChild('fileplantillaName', { static: false }) fileplantillaName!: ElementRef;


  loading:boolean = false;
  form! : FormGroup;
  parametros!: any[];

  ELEMENT_DATA! : any[]; //RequerimientoConsulta[];
  displayedColumns: string[] = ["TipoPlantilla", "Archivo" , "Opcion"];

  listaRequerimiento = new MatTableDataSource<any[]>(this.ELEMENT_DATA);
  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;

  Archivo:any;
  NombreArchivo:any;
  MAXIMO_TAMANIO_BYTES = 1024000;

  constructor(
    private dialogService: DialogService,
    private formBuilder: FormBuilder,
    private requerimientoServices : RequerimientoService,
    private parametroService : ParametroService,
    private securityService:SecurityService,
    private plantillaService : PlantillaService
  ) { }

  ngOnInit(): void {
    this.initForm();
    this.loadPage();
    this.listaRequerimiento.paginator = this.paginator;
  }

  initForm() : void {
    this.form = this.formBuilder.group({
      CodTipoPlantilla: ['', Validators.required],
    });
  }
  onChangeTipoPlantilla() {
    var CodTipoPlantilla = this.form.controls["CodTipoPlantilla"].value;

    var oRecursoBE = {
      "CodTabla" : Constantes.Plantilla,
      "CodTipoPlantilla" : (CodTipoPlantilla==''? null:CodTipoPlantilla)
    }

    this.requerimientoServices.ObtenerPlantillas(oRecursoBE).subscribe(result=>{

      this.listaRequerimiento.data = result.response;
    //  this.listaRequerimiento.paginator = this.paginator;
    },(err)=>{
      console.log(err);
    });
  }


  loadPage(): void {
    this.loading = true;

    var oRecursoBE = {
      "CodTabla" : Constantes.Plantilla,
      "CodTipoPlantilla" : null
    }
    forkJoin([
      this.parametroService.ObtenerParametroByGrupo("1000,1200"),
      this.requerimientoServices.ObtenerPlantillas(oRecursoBE)
    ]).subscribe( (data: any) => {
      debugger;
        console.log(data);
        this.parametros = data[0].response;
        this.listaRequerimiento.data =  data[1].response;
       // this.listaRequerimiento.paginator = this.paginator;


        this.loading = false;
    }, (err) => {
      this.loading = false;
      console.error(err);
    });

  }

  DescargarArchivos(LaserficheID:any): void {
    this.loading = true;
    const params = {
      LaserficheID: ""+LaserficheID
    };
    this.requerimientoServices.DescagarArchivos(params).subscribe((result) => {
      console.log('response', result.response);
      if(result.response!=null){
        if(result.response.length>0){
          for (let index = 0; index < result.response.length; index++) {
            const element = result.response[index];
            debugger;
            this.Descargar(element.archivoBytes,element.nombreDocumento,element.contentType);
          }
        }
      }
      this.loading = false;
    });
  }
  Descargar(base64String:any, fileName:any,contentType:any) {
    debugger;
    const blobData = this.convertBase64ToBlobData(base64String,contentType);
    const blob = new Blob([blobData], { type: contentType });
        const url = window.URL.createObjectURL(blob);
        // window.open(url);
        const link = document.createElement('a');
        link.href = url;
        link.download = fileName;
        link.click();
  }

  convertBase64ToBlobData(base64Data: string, contentType: string = 'image/png', sliceSize = 1024) {
    const byteCharacters = atob(base64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);
      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  Registar():void{
    if (this.form.invalid) {
      return;
    }

    if(this.Archivo==null){
      this.dialogService.openMsgErrorDialog("Seleccione una plantilla");
      return;
    }
    //this.Archivo
    this.dialogService.openConfirmDialog('¿Desea registrar su plantilla?')
    .afterClosed().subscribe(res => {
      if (res) {

        const oBE = {
          "LaserficheID": "0",
          "Archivo": this.Archivo,
          "NombreArchivo": this.NombreArchivo,
          "CodTipoPlantilla": +this.form.controls["CodTipoPlantilla"].value,
          "UsuarioRegistro": this.securityService.leerUsuarioWeb(),
          "CodTabla": Constantes.Plantilla
          };
          this.loading = true;
          this.plantillaService.Registrar(oBE).subscribe(result=>{
            this.loading = false;
            this.Archivo = null;
            this.NombreArchivo = null;
            this.InputVar.nativeElement.value = "";
            this.fileplantillaName.nativeElement.value = "";
            if(result.response.resultadoOK){
              this.onChangeTipoPlantilla();
              this.dialogService.openMsgSuccessDialog("Se registró correctamente");
            }
            else{

              this.dialogService.openMsgErrorDialog("Se presento un problema al intentar registrar la plantilla");
            }

          },
          (err)=>{
            this.loading = false;
            console.log(err);
          }
          );

      }
    });
  }

  DarBaja(element:any):void {
    this.dialogService.openConfirmDialog('¿Desea dar de baja está plantilla?')
    .afterClosed().subscribe(res => {
      if (res) {

        const oBE = {
          "CodPlantilla": element.codPlantilla,
          "UsuarioRegistro": this.securityService.leerUsuarioWeb(),
          "UsuarioModificacion": this.securityService.leerUsuarioWeb(),
          "CodTabla": Constantes.Plantilla
          };
          this.loading = true;
          this.plantillaService.Desactivar(oBE).subscribe(result=>{
            this.loading = false;

            this.onChangeTipoPlantilla();
            this.dialogService.openMsgSuccessDialog("Se dio de baja correctamente");

          },
          (err)=>{
            this.loading = false;
            console.log(err);
          }
          );

      }
    });
  }

  changePlantilla(event:any) : void {
    const file = event.target.files[0];
    if(file==null || file.length <=0){
      this.Archivo = null;
      this.NombreArchivo = null;
      this.InputVar.nativeElement.value = "";
      this.fileplantillaName.nativeElement.value = "";
     return;
   }

     var allowedExtensions = /(.doc|.docx)$/i;
     if(!allowedExtensions.exec(file.name)){
      this.dialogService.openMsgErrorDialog("Formato no válido");
      this.Archivo = null;
      this.NombreArchivo = null;
      this.InputVar.nativeElement.value = "";
      this.fileplantillaName.nativeElement.value = "";
      return;
     }


    if (file.size > this.MAXIMO_TAMANIO_BYTES) {
      const tamanioEnMb = this.MAXIMO_TAMANIO_BYTES / 1024000;
      this.Archivo = null;
      this.NombreArchivo = null;
      this.InputVar.nativeElement.value = "";
      this.fileplantillaName.nativeElement.value = "";
      this.dialogService.openMsgErrorDialog(`El tamaño máximo es 1 MB`);
      return;
    } else {
      // Validación pasada. Envía el formulario o haz lo que tengas que hacer
    }

    const FileNamme = file.name;
    this.NombreArchivo = FileNamme;

    const reader = new FileReader();
    console.log("file",file);
    reader.readAsDataURL(file);
    reader.onload = () => {
      console.log("reader",reader);
      this.Archivo = reader.result || null;
      console.log(reader.result);
    };
  }
}
