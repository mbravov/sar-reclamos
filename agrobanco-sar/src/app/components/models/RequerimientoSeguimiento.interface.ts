export interface RequerimientoSeguimiento
{
  NroRequerimiento:string,
  FechaRegistro : Date,
  FechaAsignacion : Date,
  FechaGestion : Date,
  FechaAtencion : Date,
  Estado : string,
  TipoRequerimiento : string,
  UsuarioAsignado : string,
  DiasTranscurrido : number,
  accion : string
}
