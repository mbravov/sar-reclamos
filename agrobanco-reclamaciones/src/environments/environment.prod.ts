export const environment = {
  production: true,  
  UrlBase_ReclamosAPI:  (window as any)["env"]["UrlBase_ReclamosAPI"],
  AppKey:  (window as any)["env"]["AppKey"],
  AppCode :  (window as any)["env"]["AppCode"],
  recaptcha_siteKey :  (window as any)["env"]["recaptcha_siteKey"],
  number_whatsapp :  (window as any)["env"]["number_whatsapp"],
  preguntas_frecuentes :  (window as any)["env"]["preguntas_frecuentes"]
};
