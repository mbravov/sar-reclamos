﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.Reclamaciones.API.Models
{
    public class AuthAccessRequest
    {
        [JsonProperty("correoelectronico")]
        public string CorreoElectronico { get; set; }

        [JsonProperty("nombreusuario")]
        public string NombreUsuario { get; set; }

        [JsonProperty("codigousuario")]
        public string CodigoUsuario { get; set; }

        [JsonProperty("numerodocumento")]
        public string NumeroDocumento { get; set; }
    }


    #region response

    public class AuthAccessResponse
    {
        [JsonProperty("authaccess")]
        public AuthAccessResponseData AuthAccess { get; set; }
    }

    public class AuthAccessResponseData
    {
        [JsonProperty("nombreusuario")]
        public string NombreUsuario { get; set; }

        [JsonProperty("correoelectronico")]
        public string CorreoElectronico { get; set; }

        [JsonProperty("codigousuario")]
        public string CodigoUsuario { get; set; }

        [JsonProperty("numerodocumento")]
        public string NumeroDocumento { get; set; }

        [JsonProperty("fechainiciovigencia")]
        public DateTime? FechaInicioVigencia { get; set; }

        [JsonProperty("fechafinvigencia")]
        public DateTime? FechaFinVigencia { get; set; }
    }



    #endregion
}
