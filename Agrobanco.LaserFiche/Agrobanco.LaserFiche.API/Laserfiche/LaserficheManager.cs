﻿using Agrobanco.LaserFiche.API.Laserfiche;
using Agrobanco.LaserFiche.API.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Agrobanco.LaserFiche.API
{
    public class LaserficheManager
    {
        public string ConsultaArchivoSimple(int codigoSustento, out string fileName, out string extension)
        {
            string resultado = string.Empty;
            decimal peso;

            using (var laserFicheDocumentAccess = new LaserFicheDocumentAccess(ApplicationKeys.LaserFicheRepository))
            {
                var stream = laserFicheDocumentAccess.GetDocument(codigoSustento, out fileName, out extension, out peso);
                resultado = HelperIT.MemoryStreamToBase64(stream);

                laserFicheDocumentAccess.Dispose();
            }

            return resultado;
        }


        public string ConsultaImagen(int codigoSustento, out string fileName)
        {
            string resultado = string.Empty;

            using (var laserFicheDocumentAccess = new LaserFicheDocumentAccess(ApplicationKeys.LaserFicheRepository))
            {
                var stream = laserFicheDocumentAccess.GetImage(codigoSustento, out fileName);
                resultado = HelperIT.MemoryStreamToBase64(stream);

                laserFicheDocumentAccess.Dispose();
            }

            return resultado;
        }

        public List<string> ConsultaImagenLista(string codigoSustento, out string fileName)
        {
            List<string> resultado = new List<string>();
            List<int> lstdemo = new List<int>();
            foreach(var e in codigoSustento.Split('|'))
            {
                lstdemo.Add(Convert.ToInt32(e));
            }
            

            using (var laserFicheDocumentAccess = new LaserFicheDocumentAccess(ApplicationKeys.LaserFicheRepository))
            {
                var stream = laserFicheDocumentAccess.GetImageList(lstdemo, out fileName);
                Parallel.ForEach(stream, (line, state, index) =>
                {
                   var resultado2 = HelperIT.MemoryStreamToBase64(stream[Convert.ToInt32(index)]);
                    resultado.Add(resultado2);
                });

                laserFicheDocumentAccess.Dispose();
            }

            return resultado;
        }

        public int CargarDocumentoLaserFiche(Stream stream, string fileExtension, string nombreArchivo, string folder, out string fileName)
        {
            int ldDocumentId = -1;
            using (var lfhelper = new LaserFicheDocumentAccess(ApplicationKeys.LaserFicheRepository))
            {
                stream.Position = 0;
                var ms = new MemoryStream();
                stream.CopyTo(ms);
                ms.Position = 0;
                var destinPath = "";
                var documentPath = "";

                destinPath = $@"\{ApplicationKeys.LaserFicheFolderBase}\{folder}";
                lfhelper.GetOrCreateFolderInfo(destinPath);
                documentPath = $@"{destinPath}\{nombreArchivo}.{fileExtension}";
                string volumen = ApplicationKeys.VolumenLaserfiche;

                ldDocumentId = lfhelper.RegisterFileWithoutMetaData(ms, documentPath, volumen, out fileName, contenType: TypesConstants.GetTypeMIME(fileExtension));

                lfhelper.Dispose();
            }
            return ldDocumentId;
        }

        public ImagenesModel ObtenerResultadoImportarcionImagenes( List<ItemImagen> list, string folder)
        {
            ImagenesModel ldDocumentId = new ImagenesModel();

            using (var lfhelper = new LaserFicheDocumentAccess(ApplicationKeys.LaserFicheRepository))
            {
               
                var destinPath = "";
           

                destinPath = $@"\{ApplicationKeys.LaserFicheFolderBase}\{folder}";
                lfhelper.GetOrCreateFolderInfo(destinPath);

                string volumen = ApplicationKeys.VolumenLaserfiche;
                ldDocumentId =  lfhelper.RegisterDocumentWithoutMetaDataVisitas(list, destinPath, volumen);

                lfhelper.Dispose();
            }
            return ldDocumentId;
        }

        public bool EliminarArchivo(int LaserficheID)
        {
            try
            {
                LaserFicheDocumentAccess oLaserficheDocumentAccess = new LaserFicheDocumentAccess(ApplicationKeys.LaserFicheRepository);
                 oLaserficheDocumentAccess.DeleteDocumentIfExistv2(LaserficheID);
                return true;
            }
            catch (Exception)
            {
               return false;
            }
   
        }
    }
}