﻿using Agrobanco.SAR.Datos.SqlServer.Acceso;
using Agrobanco.SAR.Entidades.CNA_Minuta;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SAR.Datos
{
    public class FiltroAccionDatos
    {
        public string cadenaConexion;
        public async Task<List<FiltroAccionBE>> Obtener()
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            FiltroAccionBE oFiltroAccionBE = null;
            List<FiltroAccionBE> LstFiltroAccionBE = new List<FiltroAccionBE>();
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("SP_FILTRO_ACCION_OBTENER", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        try
                        {
                            using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                            {
                                while (reader.Read())
                                {
                                    oFiltroAccionBE = new FiltroAccionBE();

                                    if (!reader.IsDBNull(0))
                                        oFiltroAccionBE.IdConfiguracionAccion = reader.GetInt32(0);

                                    if (!reader.IsDBNull(1))
                                        oFiltroAccionBE.Grupo = reader.GetInt32(1);

                                    if (!reader.IsDBNull(2))
                                        oFiltroAccionBE.TipoFiltro = reader.GetInt32(2);

                                    if (!reader.IsDBNull(3))
                                        oFiltroAccionBE.Valor1 = reader.GetString(3);

                                    if (!reader.IsDBNull(4))
                                        oFiltroAccionBE.Valor2 = reader.GetInt32(4);

                                    if (!reader.IsDBNull(5))
                                        oFiltroAccionBE.Valor = reader.GetString(5);

                                    LstFiltroAccionBE.Add(oFiltroAccionBE);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            throw ex;
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return await Task.Run(() => LstFiltroAccionBE);
        }
    }
}
