﻿using Agrobanco.SAR.DAL.Entities;
using Agrobanco.SAR.DAL.SqlServer.Acceso;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SAR.DAL
{
    public class CNADA
    {
        public string cadenaConexion;

        public List<CreditoCliente> ListarCreditoClienteGeneracionCNA()
        {
            try
            {
                cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();

                List<CreditoCliente> listaResultado = new List<CreditoCliente>();

                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_CREDITO_CLIENTE_LISTAR_GENERACION_CNA]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CreditoCliente creditoCliente = new CreditoCliente();

                                if (!Convert.IsDBNull(reader["IdCreditoCliente"]))
                                {
                                    creditoCliente.IdCreditoCliente = Convert.ToInt32(reader["IdCreditoCliente"]);
                                }

                                if (!Convert.IsDBNull(reader["TipoDocumento"]))
                                {
                                    creditoCliente.TipoDocumento = Convert.ToString(reader["TipoDocumento"]);
                                }

                                if (!Convert.IsDBNull(reader["NumeroDocumento"]))
                                {
                                    creditoCliente.NumeroDocumento = Convert.ToString(reader["NumeroDocumento"]);
                                }

                                if (!Convert.IsDBNull(reader["Nombres"]))
                                {
                                    creditoCliente.Nombres = Convert.ToString(reader["Nombres"]);
                                }

                                if (!Convert.IsDBNull(reader["Direccion"]))
                                {
                                    creditoCliente.Direccion = Convert.ToString(reader["Direccion"]);
                                }

                                if (!Convert.IsDBNull(reader["Procesado"]))
                                {
                                    creditoCliente.Procesado = Convert.ToBoolean(reader["Procesado"]);
                                }

                                if (!Convert.IsDBNull(reader["IdLaserficheCNA"]))
                                {
                                    creditoCliente.IdLaserficheCNA = Convert.ToInt32(reader["IdLaserficheCNA"]);
                                }

                                listaResultado.Add(creditoCliente);
                            }
                        }
                    }
                }

                return listaResultado;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Parametros> ListarParametro(int Grupo)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            Parametros objParametro = null;
            List<Parametros> lstParametro = new List<Parametros>();
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_PARAMETRO_OBTENER]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@grupo", SqlDbType.Int).Value = Grupo;
                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    objParametro = new Parametros();
                                    if (!Convert.IsDBNull(reader["Grupo"])) objParametro.Grupo = Convert.ToInt32(reader["Grupo"]);
                                    if (!Convert.IsDBNull(reader["Parametro"])) objParametro.Parametro = Convert.ToInt32(reader["Parametro"]);
                                    if (!Convert.IsDBNull(reader["Descripcion"])) objParametro.Descripcion = Convert.ToString(reader["Descripcion"]);
                                    if (!Convert.IsDBNull(reader["Valor"])) objParametro.Valor = Convert.ToString(reader["Valor"]);
                                    if (!Convert.IsDBNull(reader["Referencia"])) objParametro.Referencia = Convert.ToString(reader["Referencia"]);
                                    if (!Convert.IsDBNull(reader["Referencia2"])) objParametro.Referencia2 = Convert.ToString(reader["Referencia2"]);

                                    lstParametro.Add(objParametro);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw (ex);
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw (ex);
            }

            return lstParametro;
        }


        public bool ActualizarCreditoClienteGeneracionCNA(CreditoCliente oCreditoCliente)
        {
            Boolean resultado = false;
            try
            {
                cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();

                    
                    using (SqlCommand cmd = new SqlCommand("SP_CREDITO_CLIENTE_ACTUALIZAR_IDLASERFICHE", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@IdCreditoCliente", SqlDbType.Int).Value = oCreditoCliente.IdCreditoCliente;
                        cmd.Parameters.Add("@IdLaserficheCNA", SqlDbType.Int).Value = oCreditoCliente.IdLaserficheCNA;
                        cmd.ExecuteNonQuery();
                        resultado = true;
                    }
                }

                return resultado;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
