﻿using Agrobanco.SAR.API.Models;
using Agrobanco.SAR.Control.Filters;
using Agrobanco.SAR.Entidades.CNA_Minuta;
using Agrobanco.SAR.LogicaNegocio;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.SAR.API.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/recurso-emision")]
    public class RecursoEmisionController : ControllerBase
    {
        private readonly RecursoEmisionLogica recursoEmisionLogica;
        public RecursoEmisionController()
        {
            recursoEmisionLogica = new RecursoEmisionLogica();
        }



        [HttpPost("insertar")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public async Task<IActionResult> Insertar([Required][FromHeader(Name = "Token")] string token, [FromBody] RecursoEmisionBE request)
        {
            ResponseSuccess oResponseSuccess = new ResponseSuccess();
            ResponseSuccessDetalle oResponseSuccessDetalle = new ResponseSuccessDetalle();
            ResponseErrorDetalle oResponseErrorDetalle = new ResponseErrorDetalle();
            try
            {
                oResponseSuccessDetalle.resultado = await recursoEmisionLogica.Insertar(request); ;
                oResponseSuccess.success = oResponseSuccessDetalle;

                return StatusCode(200, oResponseSuccess);
            }
            catch (Exception ex)
            {
                oResponseErrorDetalle.codigo = string.Empty;
                oResponseErrorDetalle.resultado = false;
                oResponseErrorDetalle.mensaje = ex.Message;

                return StatusCode(100, oResponseErrorDetalle);
            }
        }


        [HttpPost("obtener")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public async Task<IActionResult> Obtener([Required][FromHeader(Name = "Token")] string token, [FromBody] RecursoEmisionBE request)
        {
            ResponseErrorDetalle oResponseErrorDetalle = new ResponseErrorDetalle();
            try
            {
                var resultado = await recursoEmisionLogica.Obtener(request); ;
                return StatusCode(200, resultado);
            }
            catch (Exception ex)
            {
                oResponseErrorDetalle.codigo = string.Empty;
                oResponseErrorDetalle.resultado = false;
                oResponseErrorDetalle.mensaje = ex.Message;

                return StatusCode(100, oResponseErrorDetalle);
            }
        }

    }
}
