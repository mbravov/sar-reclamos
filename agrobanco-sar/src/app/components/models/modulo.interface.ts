export interface Modulo{
    Action : string,
    Controller: string,
    CorrelativoOpcion: string,
    DescripcionAplicacion: string,
    EstadoOpcion: string,
    IdAplicacion: string,
    IdOpcion: string,
    IdPerfil: string,
    IdRelacion: string,
    NombreAplicacion: string,
    NombreOpcion: string,
    NombrePerfil: string,
    TipoIconoCodigo: string,
    TipoIconoDescripcion: string,
    TipoOpcion: string,
    UrlAplicacion: string,
    UrlOpcion: string
}