﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades
{
    public class EstadosCNAMinBE
    {
        public int CodEstado { get; set; }
        public string Descripcion { get; set; }
      
    }
}
