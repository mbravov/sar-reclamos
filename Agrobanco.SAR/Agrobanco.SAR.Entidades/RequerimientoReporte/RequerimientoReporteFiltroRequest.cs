﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades.RequerimientoReporte
{
    public class RequerimientoReporteFiltroRequest
    {
        public int? CodTipoRequerimiento { get; set; }
        public int? Estado { get; set; }
        public string FechaRegistroInicio { get; set; }
        public string FechaRegistroFin { get; set; }


    }
}
