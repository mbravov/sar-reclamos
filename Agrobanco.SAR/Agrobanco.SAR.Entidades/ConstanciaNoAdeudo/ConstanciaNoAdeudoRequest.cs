﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades.ConstanciaNoAdeudo
{
    public class ConstanciaNoAdeudoRequest
    {
        public string TipoIDN { get; set; }
        public string Identificacion { get; set; }
        public int TipoDocumento { get; set; }


    }
}
