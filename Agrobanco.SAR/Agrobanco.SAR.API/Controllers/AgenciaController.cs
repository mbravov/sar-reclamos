﻿using Agrobanco.SAR.API.Models;
using Agrobanco.SAR.Control.Filters;
using Agrobanco.SAR.Entidades;
using Agrobanco.SAR.LogicaNegocio;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.SAR.API.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/Agencia")]
    [ApiController]

    public class AgenciaController : Controller
    {
        private readonly AgenciaLogica agenciaLogica;
        public AgenciaController()
        {
            agenciaLogica = new AgenciaLogica();

        }

        [HttpPost("listar")]
        [ProducesResponseType(200, Type = typeof(List<AgenciaBE>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult Agencia([Required][FromHeader(Name = "Token")] string token)
        {
            try
            {

                var listaResultado = agenciaLogica.ObtenerListarAgencias();

                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

         
    }
}
