import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { RequerimientoSeguimiento } from '../models/RequerimientoSeguimiento.interface';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { ExcelJsService } from 'src/app/services/comun/excel-js.service';
import { forkJoin } from 'rxjs';
import { ParametroService } from 'src/app/services/parametro.service';
import { ReporteService } from 'src/app/services/reporte.service';
import * as moment from 'moment';

@Component({
  selector: 'app-reporte-requerimiento',
  templateUrl: './reporte-requerimiento.component.html',
  styleUrls: ['./reporte-requerimiento.component.css']
})
export class ReporteRquerimientoComponent implements OnInit {

  busquedaFiltro! : FormGroup;
  parametros!:any[];

  loading!:boolean;
  ELEMENT_DATA! : RequerimientoSeguimiento[];
  displayedColumns: string[] = ['NroRequerimiento','FechaRegistro', 'EsmenorEdad', 'NroDocumento', 'NombresApellidos','RazonSocial','TipoRequerimiento','Estado'];
  //requerimientoSeguimiento!:any[];
  requerimientoReporte = new MatTableDataSource<RequerimientoSeguimiento>(this.ELEMENT_DATA);
  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;

  fechaRegistroInicioValor! : Date;
  fechaRegistroFinValor! : Date;

  mensaje!: boolean;

  constructor(
    private parametroService: ParametroService,
    private reporteService:ReporteService,
    private excelJsService:ExcelJsService
  ) { }

  ngOnInit(): void {
    this.initFormGroup();
    this.loadPage();
  }

  loadPage(): void {

     this.loading = true;
     forkJoin([
       this.parametroService.ObtenerParametro()
     ]).subscribe( (data:any) => {

         console.log(data);

         this.parametros = data[0].response;


       this.loading = false;

     }, (error) =>{
       console.log(error);
       this.loading = false;
     });

   }

   initFormGroup():void {

    let fechaInicioMes = new Date(new Date().getFullYear(), new Date().getMonth(), 1);
    let fechaHoy = new Date();


    this.fechaRegistroInicioValor = fechaInicioMes;
    this.fechaRegistroFinValor = fechaHoy;

    this.busquedaFiltro = new FormGroup({

      CodTipoRequerimiento : new FormControl(0) ,
      Estado : new FormControl(0),
      Fecharegistroinicio: new FormControl(''),
      Fecharegistrofin: new FormControl(''),
      Mensaje: new FormControl('')

    });
  }


  ExportarExcel():void {

this.loading = true;
  this.mensaje = false;
    var request = {
      CodTipoRequerimiento : +this.busquedaFiltro.controls["CodTipoRequerimiento"].value,
      Estado : +this.busquedaFiltro.controls["Estado"].value,
      Fecharegistroinicio : this.busquedaFiltro.controls["Fecharegistroinicio"].value,
      Fecharegistrofin : this.busquedaFiltro.controls["Fecharegistrofin"].value

    };
    //this.loading = true;
    this.reporteService.ObtenerReporte(request).subscribe(result=>{
        console.log(result);

        if(result.response.length > 0)
        {

        this.requerimientoReporte.data = result.response as RequerimientoSeguimiento[];

        const FechaActual = new Date();
        const AnioActual = FechaActual.getFullYear();
        const MesActual = (FechaActual.getMonth()<=9? '0'+ (FechaActual.getMonth()+1): FechaActual.getMonth() + 1);
        const DiaActual = (FechaActual.getDate()<=9? '0'+FechaActual.getDate(): FechaActual.getDate());
        const HoraActual = (FechaActual.getHours()<=9? '0'+FechaActual.getHours(): FechaActual.getHours());
        const MinutoActual = (FechaActual.getMinutes()<=9? '0'+FechaActual.getMinutes(): FechaActual.getMinutes());
        const SegundosActual = (FechaActual.getSeconds()<=9? '0'+FechaActual.getSeconds(): FechaActual.getSeconds());

        const Archivo = "ReporteRequerimiento" + "_" + AnioActual + MesActual + DiaActual + "T" + HoraActual + MinutoActual + SegundosActual;
        const Sheet = "Hoja1";
        const header = ["NRO REQ","ES MENOR DE EDAD?","TIPO DOCUMENTO","NUMERO DOCUMENTO","APELLIDOS Y NOMBRES","RAZON SOCIAL", "DETALLE DEL REQUERIMIENTO","DEPARTAMENTO","PROVINCIA","DISTRITO","TELEFONO","CELULAR","PRODUCTO","MOTIVO","TIPO REQUERIMIENTO","ORIGEN","AGENCIA", "FECHA REGISTRO", "ESTADO ACTUAL", "MEDIO DE RESPUESTA"];
        const header_width = [20,20,20,20,30,30,50,20,20,20,20,20,35,35,20,20,25,20,20,20];
        let body = [];
        for (let index = 0; index < result.response.length; index++) {
          const datos = result.response[index];
          var nroRequerimiento = (datos.nroRequerimiento==null?'': datos.nroRequerimiento);
          var esMenor = (datos.esMenor==null?'': datos.esMenor);
          var tipoDocumento = datos.tipoDocumento;
          var nroDocumento = datos.nroDocumento;
          var nombresApellidos = datos.nombresApellidos;
          var razonSocial = datos.razonSocial;
          var descripcion = datos.descripcion;
          var departamento = datos.departamento;
          var provincia = datos.provincia;
          var distrito = datos.distrito;
          var telefono = datos.telefono;
          var celular = datos.celular;
          var producto = datos.producto;
          var motivo = datos.motivo;
          var tipoRequerimiento = datos.tipoRequerimiento;
          var origen = datos.origen;
          var agencia = datos.agencia;
          var fechaRegistro = (datos.fechaRegistro==null?'': moment(datos.fechaRegistro).format("DD/MM/YYYY"));
          var estado = datos.estado;
          var formaEnvio = datos.formaEnvio;

          var datoarray = [nroRequerimiento,esMenor,tipoDocumento,nroDocumento,nombresApellidos,razonSocial,descripcion,departamento,provincia,distrito,
            telefono,celular,producto,motivo,tipoRequerimiento,origen,agencia,fechaRegistro,estado,formaEnvio]
          body.push(datoarray)
        }

        this.excelJsService.DescargarExcelGenerico(Archivo,Sheet,header,body,header_width);
        this.loading = false;
      }
      else{
        this.mensaje = true;
        this.loading = false;
      }

    },(err)=>{
      console.log(err)
    });
    this.loading = false;

  }

  onChangeFechaRegistroIni(){
    this.fechaRegistroInicioValor = this.busquedaFiltro.controls['Fecharegistroinicio'].value;
  }

  onChangeFechaRegistroFin(){
    this.fechaRegistroFinValor = this.busquedaFiltro.controls['Fecharegistrofin'].value;
  }


}
