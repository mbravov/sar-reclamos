export interface CnaMinutaConsulta {
    FechaCancelacion : Date,
    NombreApellidos : string,
    NumeroDocumento : string,
    Banco : string,
    Agencia : string,
    AnalistaProponente : string,
    Estado : string,
    DerivadoA : string,
    FechaDerivacion : Date,
    procesamiento : string
}
