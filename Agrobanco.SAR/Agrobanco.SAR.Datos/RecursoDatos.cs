﻿using Agrobanco.SAR.Datos.SqlServer.Acceso;
using Agrobanco.SAR.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Agrobanco.SAR.Datos
{
    public class RecursoDatos
    {
        public string cadenaConexion;

    

        public RecursoDatos()
        {
      
        }
        public bool Registrar(RecursoBE oBE)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            using (SqlConnection cnn = new SqlConnection(cadenaConexion))
            {
                cnn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[SP_RECURSO_INSERTAR]", cnn))
                {
                    try
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@CodTabla", SqlDbType.Int).Value = oBE.CodTabla;
                        cmd.Parameters.Add("@ReferenciaID", SqlDbType.Int).Value = oBE.ReferenciaID;
                        cmd.Parameters.Add("@LaserficheID", SqlDbType.Int).Value = oBE.LaserficheID;

                        if(string.IsNullOrEmpty(oBE.Descripcion))
                            cmd.Parameters.Add("@Descripcion", SqlDbType.VarChar, 100).Value = DBNull.Value;
                        else
                        cmd.Parameters.Add("@Descripcion", SqlDbType.VarChar, 100).Value = oBE.Descripcion;
                        
                        cmd.Parameters.Add("@Estado", SqlDbType.Bit).Value = oBE.Estado;
                        cmd.Parameters.Add("@UsuarioRegistro", SqlDbType.VarChar,50).Value = oBE.UsuarioRegistro;
                       
                        cmd.ExecuteNonQuery();
                        return true;
                    }

                    catch (Exception ex)
                    {
                        cnn.Close();
                        cnn.Dispose();
                        cmd.Dispose();
                        throw (ex);
                    }
                    finally
                    {
                        cnn.Close();
                        cnn.Dispose();
                        cmd.Dispose();
                    }

                }

            }


            return false;
        }

        public bool RegistrarTrans(RecursoBE oBE, SqlConnection cnn, SqlTransaction oSqlTransaction)
        {
          
     
                using (SqlCommand cmd = new SqlCommand("[dbo].[SP_RECURSO_INSERTAR]", cnn, oSqlTransaction))
                {
                    try
                    {
                        //if (tx != null) cmd.Transaction = tx; //tx.Connection.BeginTransaction();

                        cmd.CommandType = CommandType.StoredProcedure;


                        cmd.Parameters.Add("@CodTabla", SqlDbType.Int).Value = oBE.CodTabla;
                        cmd.Parameters.Add("@ReferenciaID", SqlDbType.Int).Value = oBE.ReferenciaID;
                        cmd.Parameters.Add("@LaserficheID", SqlDbType.Int).Value = oBE.LaserficheID;

                        if (string.IsNullOrEmpty(oBE.Descripcion))
                            cmd.Parameters.Add("@Descripcion", SqlDbType.VarChar, 100).Value = DBNull.Value;
                        else
                            cmd.Parameters.Add("@Descripcion", SqlDbType.VarChar, 50).Value = oBE.Descripcion;

                        cmd.Parameters.Add("@Estado", SqlDbType.Bit).Value = oBE.Estado;
                        cmd.Parameters.Add("@UsuarioRegistro", SqlDbType.VarChar, 50).Value = oBE.UsuarioRegistro;

                        cmd.ExecuteNonQuery();
                        return true;
                    }

                    catch (Exception ex)
                    {
                        //cnn.Close();
                        //cnn.Dispose();
                        cmd.Dispose();
                    throw (ex);
                }
                    finally
                    {
                        //cnn.Close();
                        //cnn.Dispose();
                        cmd.Dispose();
                    }

                }

 


            return false;
        }


        public List<RecursoBE> Listar(RecursoBE oBE)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            RecursoBE oRecursoBE = null;
            List<RecursoBE> LstRecursoBE = new List<RecursoBE>();
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("SP_RECURSO_OBTENER", conn))
                    {
                        
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@CodTabla", SqlDbType.Int).Value = oBE.CodTabla;
                    

                        if (oBE.ReferenciaID == null)
                            cmd.Parameters.Add("@ReferenciaID", SqlDbType.Int).Value = DBNull.Value;
                        else
                            cmd.Parameters.Add("@ReferenciaID", SqlDbType.Int).Value = oBE.ReferenciaID;


                        if (oBE.LaserficheID==null)
                            cmd.Parameters.Add("@LaserficheID", SqlDbType.Int).Value = DBNull.Value;
                        else
                            cmd.Parameters.Add("@LaserficheID", SqlDbType.Int).Value = oBE.LaserficheID;

                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    oRecursoBE = new RecursoBE();

                                    if (!reader.IsDBNull(0))
                                        oRecursoBE.CodTabla = reader.GetInt32(0);

                                    if (!reader.IsDBNull(1))
                                        oRecursoBE.ReferenciaID = reader.GetInt64(1);

                                    if (!reader.IsDBNull(2))
                                        oRecursoBE.LaserficheID = reader.GetInt32(2);

                                    if (!reader.IsDBNull(3))
                                        oRecursoBE.Descripcion = reader.GetString(3);

                                    LstRecursoBE.Add(oRecursoBE);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw (ex);
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw (ex);
            }

            return LstRecursoBE;
        }

        public bool Eliminar(RecursoBE oBE)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            using (SqlConnection cnn = new SqlConnection(cadenaConexion))
            {
                cnn.Open();
                using (SqlCommand cmd = new SqlCommand("SP_RECURSO_ELIMINAR", cnn))
                {
                    try
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@LaserficheID", SqlDbType.Int).Value = oBE.LaserficheID;
                        cmd.ExecuteNonQuery();
                        return true;
                    }

                    catch (Exception ex)
                    {
                        cnn.Close();
                        cnn.Dispose();
                        cmd.Dispose();
                        throw (ex);
                    }
                    finally
                    {
                        cnn.Close();
                        cnn.Dispose();
                        cmd.Dispose();
                    }

                }

            }


            return false;
        }

        public List<RecursoBE> RecursoObtener(RecursoBE oBE)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            RecursoBE oRecursoBE = null;
            List<RecursoBE> LstRecursoBE = new List<RecursoBE>();

            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[SP_RECURSO_OBTENER]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@codTabla", SqlDbType.Int).Value = oBE.CodTabla;

                        if (oBE.ReferenciaID == null)
                            cmd.Parameters.Add("@ReferenciaID", SqlDbType.Int).Value = DBNull.Value;
                        else
                            cmd.Parameters.Add("@ReferenciaID", SqlDbType.Int).Value = oBE.ReferenciaID;


                        if (oBE.LaserficheID == null)
                            cmd.Parameters.Add("@LaserficheID", SqlDbType.Int).Value = DBNull.Value;
                        else
                            cmd.Parameters.Add("@LaserficheID", SqlDbType.Int).Value = oBE.LaserficheID;


                        try
                        {
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    oRecursoBE = new RecursoBE();

                                    if (!reader.IsDBNull(0))
                                        oRecursoBE.CodTabla = reader.GetInt32(0);

                                    if (!reader.IsDBNull(1))
                                        oRecursoBE.ReferenciaID = reader.GetInt64(1);

                                    if (!reader.IsDBNull(2))
                                        oRecursoBE.LaserficheID = reader.GetInt32(2);

                                    if (!reader.IsDBNull(3))
                                        oRecursoBE.Descripcion = reader.GetString(3);

                                    LstRecursoBE.Add(oRecursoBE);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            // new LogWriter(ex.Message); caycho
                            throw (ex);
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw (ex);
            }

            return LstRecursoBE;
        }


        public bool RecursoEliminar(RecursoBE oBE)
        {
            this.cadenaConexion = new SqlServerAccess().ClaveConnectionStringSQL();
            using (SqlConnection cnn = new SqlConnection(cadenaConexion))
            {
                cnn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[SP_RECURSO_ELIMINAR]", cnn))
                {
                    try
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        if (oBE.LaserficheID == null)
                            cmd.Parameters.Add("@LaserficheID", SqlDbType.Int).Value = DBNull.Value;
                        else
                            cmd.Parameters.Add("@LaserficheID", SqlDbType.Int).Value = oBE.LaserficheID;

                        cmd.ExecuteNonQuery();

                        return true;
                    }

                    catch (Exception ex)
                    {
                        cnn.Close();
                        cnn.Dispose();
                        cmd.Dispose();
                        throw (ex);
                    }
                    finally
                    {
                        cnn.Close();
                        cnn.Dispose();
                        cmd.Dispose();
                    }

                }

            }


            return false;
        }
    }
}
