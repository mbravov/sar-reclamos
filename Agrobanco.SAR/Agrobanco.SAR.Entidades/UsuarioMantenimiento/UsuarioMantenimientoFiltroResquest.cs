﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades.UsuarioMantenimiento
{
    public class UsuarioMantenimientoFiltroResquest
    {
        public string Perfil { get; set; }
        public string NombresApellidos { get; set; }
        public string UsuarioWeb { get; set; }
       
    }
}
