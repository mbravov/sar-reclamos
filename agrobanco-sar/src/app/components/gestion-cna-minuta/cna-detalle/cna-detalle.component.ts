import { CnaReaperturaComponent } from './../cna-reapertura/cna-reapertura.component';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CnaMinutaGarantiaService } from 'src/app/services/cna-minuta-garantia.service';
import { CnaMinutaOcmService } from 'src/app/services/cna-minuta-ocm.service';
import { CnaComentarioComponent } from '../cna-comentario/cna-comentario.component';
import { ActivatedRoute } from '@angular/router';
import { ConfiguracionAccionService } from '../../../services/configuracion-accion.service';
import { CnaRechazarComponent } from '../cna-rechazar/cna-rechazar.component';
import { CnaDerivacionOcmComponent } from '../cna-derivacion-ocm/cna-derivacion-ocm.component';
import { SecurityService } from 'src/app/services/security.service';
import { DialogService } from 'src/app/services/comun/dialog.service';
import { Constantes } from 'src/app/comun/constantes';
import { RecursoEmisionService } from 'src/app/services/recurso-emision.service';
import { RequerimientoService } from '../../../services/requerimiento.service';
import { CnaDerivarLegalComponent } from '../cna-derivar-legal/cna-derivar-legal.component';
import { forkJoin } from 'rxjs';
import { CnaDerivarAdmageComponent } from '../cna-derivar-admage/cna-derivar-admage.component';
import { CnaDerivarComponent } from '../cna-derivar/cna-derivar.component';
import { ParametroService } from 'src/app/services/parametro.service';
import { constants } from 'buffer';
import { CnaDerivarUdaComponent } from '../cna-derivar-uda/cna-derivar-uda.component';

@Component({
  selector: 'app-cna-detalle',
  templateUrl: './cna-detalle.component.html',
  styleUrls: ['./cna-detalle.component.css']
})
export class CnaDetalleComponent implements OnInit {

  @ViewChild('InputCNA', {static: false}) InputCNA!: ElementRef;

  form!: FormGroup;
  formSubmitted:boolean = false;
  mostrarEmail:boolean = false;
  mostrarWhatsapp:boolean = false;
  displayedColumns_: string[] = ['Descripcion', 'Nombre', 'DNI', 'Monto','NroPartida','OficinaRegistral','NroPrestamo'];
  displayedColumns: string[] = ['nroPrestamo', 'ultimoCredito','estadoActual', 'saldoDeudo','capital','interesCompensatorio','interesMoratorio', 'otros' ];

  CreditoClienteDetalle:any;
  IdEmision:number;
  IdConfiguracionAccion:number;

  MAXIMO_TAMANIO_BYTES = 2560000;
  MAXIMO_TAMANIO_BYTES_CR = 5120000; // 5MB = 5 millón de bytes
  MAXIMO_TAMANIO_BYTES_CE = 5120000;
  Archivo1:any;
  NombreArchivo1:any;
  ArchivoCR:any;
  NombreArchivoCR:any;
  ArchivoCE:any;
  NombreArchivoCE:any;
  loading:boolean = false;

  botones:any;

  bLaserficheCNA:boolean = false;
  bLaserficheCR:boolean = false;
  bLaserficheCE:boolean = false;
  bLaserficheCNAAdmAge:boolean = false;

  recursos!:any[];
  recursosCR!:any[];

  LaserFicheID:any;

  documentosEvaluadosGarantia!:any[];

  CNALaserFicheID!:any;
  

  documentosMinuta:any;
  documentosCartaEntrega:any;
  cnaADMAGE:any;

  permisoLegal:number = 0;

  FechaEntregaOlva:any;

  MedioNotificaciones!:any[];

  documentos_reaperturado!:any[];

  constructor(
    private _dialog: MatDialog,
    private _cnaMinutaOCMService : CnaMinutaOcmService,
    private _cnaMinutaGaratiaService : CnaMinutaGarantiaService,
    private _configuracionAccion : ConfiguracionAccionService,
    private _recursoEmisionService: RecursoEmisionService,
    private _requerimientoService : RequerimientoService,
    private _securityService : SecurityService,
    private _activateRoute: ActivatedRoute,
    private _dialogService: DialogService,
    private _formBuilder: FormBuilder,
    private _parametroService : ParametroService
  ) {
    this.IdEmision = this._activateRoute.snapshot.params.id;
    this.IdConfiguracionAccion =0;
  }

  ngOnInit(): void {
    this.inicializarFormulario();
    this.pageLoad();
  }

  inicializarFormulario() : void {
      this.form = this._formBuilder.group({
      FileCR:[null],
      FileCNA:[null],
      FileCE:[null],
      plantilla:[null],
      plantillaCR:[null],
      MedioNotificacion:[null,  [Validators.required]],
      Email:[null,  [Validators.required]],
      Whatsapp:[null,  [Validators.required]]
    });
  }

  /* ****************************************************************************************
  Metodos
  *******************************************************************************************/

  pageLoad(){
    var request = {
      "IdEmision": +this.IdEmision,
      "Perfil": this._securityService.leerPerfil()
    };
    this.loading = true;
    var oRecursoBE = {
      "CodTabla" : Constantes.Plantilla,
      "CodTipoPlantilla" :   ""+Constantes.PlantillaGestionCNA
    };
    var oRecursoCR = {
      "CodTabla" : Constantes.Plantilla,
      "CodTipoPlantilla" :   ""+Constantes.PlantillaGestionCNACartaRespuesta
      //"CodTipoPlantilla" :   ""+Constantes.PlantillaGestionCNA
    };

    forkJoin([
      this._cnaMinutaOCMService.obtenerDetalle(request),
      this._requerimientoService.ObtenerPlantillas(oRecursoBE),
      this._parametroService.ObtenerParametroByGrupo("1700"),
      this._requerimientoService.ObtenerPlantillas(oRecursoCR)
    ]).subscribe( (data: any) => {

        console.log("ObtenerRecursos",data);
        this.CreditoClienteDetalle = data[0].response;

        this.recursos = data[1].response;
        this.documentosEvaluadosGarantia = this.CreditoClienteDetalle.recursoEmision.filter((x:any)=>x.tipoRecursoID==4);
        this.documentosMinuta = this.CreditoClienteDetalle.recursoEmision.filter((x:any)=>x.tipoRecursoID==5);
        this.documentosCartaEntrega = this.CreditoClienteDetalle.recursoEmision.filter((x:any)=>x.tipoRecursoID==6);
        this.cnaADMAGE = this.CreditoClienteDetalle.recursoEmision.filter((x:any)=>x.tipoRecursoID==2);

        this.documentos_reaperturado = this.CreditoClienteDetalle.recursoEmision.filter((x:any)=>x.tipoRecursoID>=8);

        var requestCA = {
        "IdEstado": this.CreditoClienteDetalle.idEstado,
        "Perfil": this.CreditoClienteDetalle.perfil
        };

        if(this.CreditoClienteDetalle.idLaserficheCNA){
          this.bLaserficheCNA = true;
        }
        else{
            this.bLaserficheCNA = false;
        }

        if(this.CreditoClienteDetalle.idLaserficheCR){
            this.bLaserficheCR = true;
        }
        else{
            this.bLaserficheCR = false;
        }

        if(this.CreditoClienteDetalle.idLaserficheCE){
          this.bLaserficheCE = true;
        }
        else{
            this.bLaserficheCE = false;
        }



        if(this.cnaADMAGE != null && this.cnaADMAGE.length>0){
          this.bLaserficheCNAAdmAge = true;
        }
        else{
          this.bLaserficheCNAAdmAge = false;
        }
      this._configuracionAccion.obtener(requestCA).subscribe((data:any) => {
        this.botones = data.response;
        this.permisoLegal = data.response.filter((x:any) => x.tipoAccion==10).length;
        console.log("Acciones configuracion", this.botones)
        this.loading = false;
      }, (err) => {
        this.loading = false;
        console.log(err);
      });
      console.log('data[2].response :', data[2].response);
      this.MedioNotificaciones = data[2].response;
      this.recursosCR = data[3].response;

    }, (err) => {
      this.loading = false;
      console.log(err);
    });
  }
  ObtenerDetalle(loading:boolean = false):void{
    var request = {
      "IdEmision": +this.IdEmision,
      "Perfil": this._securityService.leerPerfil()
    };

    if(loading){
      this.loading = true;
    }
    forkJoin([
      this._cnaMinutaOCMService.obtenerDetalle(request)
    ]).subscribe( (data: any) => {
        debugger;
        this.CreditoClienteDetalle = data[0].response;
        this.documentosEvaluadosGarantia = this.CreditoClienteDetalle.recursoEmision.filter((x:any)=>x.tipoRecursoID==4);

        this.documentosMinuta = this.CreditoClienteDetalle.recursoEmision.filter((x:any)=>x.tipoRecursoID==5);
        this.documentosCartaEntrega = this.CreditoClienteDetalle.recursoEmision.filter((x:any)=>x.tipoRecursoID==6);

        this.cnaADMAGE = this.CreditoClienteDetalle.recursoEmision.filter((x:any)=>x.tipoRecursoID==2);

        this.documentos_reaperturado = this.CreditoClienteDetalle.recursoEmision.filter((x:any)=>x.tipoRecursoID>=8);


        var requestCA = {
        "IdEstado": this.CreditoClienteDetalle.idEstado,
        "Perfil": this.CreditoClienteDetalle.perfil
        };

        if(this.CreditoClienteDetalle.idLaserficheCNA){
          this.bLaserficheCNA = true;
        }
        else{
            this.bLaserficheCNA = false;
        }

        if(this.CreditoClienteDetalle.idLaserficheCR){
            this.bLaserficheCR = true;
        }
        else{
            this.bLaserficheCR = false;
        }

        if(this.CreditoClienteDetalle.idLaserficheCE){
          this.bLaserficheCE = true;
        }
        else{
            this.bLaserficheCE = false;
        }

        if(this.cnaADMAGE != null && this.cnaADMAGE.length>0){
          this.bLaserficheCNAAdmAge = true;
        }
        else{
          this.bLaserficheCNAAdmAge = false;
        }

      this._configuracionAccion.obtener(requestCA).subscribe((data:any) => {
        debugger;
        this.botones = data.response;
        this.permisoLegal = data.response.filter((x:any) => x.tipoAccion==10).length;
        this.Archivo1 =  null;
        this.NombreArchivo1 = null;
        this.ArchivoCR =  null;
        this.NombreArchivoCR = null;
        this.ArchivoCE =  null;
        this.NombreArchivoCE = null;
        this.form.controls['FileCR'].setValue(null);
        this.form.controls['FileCNA'].setValue(null);
        this.form.controls['FileCE'].setValue(null);
        this.loading = false;
      }, (err) => {
        this.loading = false;
        console.log(err);
      });



    }, (err) => {
      this.loading = false;
      console.log(err);
    });


  }

  DescargarPlantilla(): void
{

  if(this.form.controls["plantilla"].value==null){
    this._dialogService.openMsgErrorDialog("Seleccione una plantilla");
    return;
  }

  if(this.form.controls["plantilla"].value==""){
    this._dialogService.openMsgErrorDialog("Seleccione una plantilla");
    return;
  }

  let SaldoCredito;
  let NroCredito;
  let EstadoCredito;

  // if(this.CreditoClienteDetalle.creditos!=null){
  //   if(this.CreditoClienteDetalle.creditos.length>0){
  //     for (let index = 0; index < this.CreditoClienteDetalle.creditos.length; index++) {
  //       const element = this.CreditoClienteDetalle.creditos[index];

  //       SaldoCredito = element.saldo;
  //       NroCredito = element.codCred;
  //       EstadoCredito = element.estado;
  //       break;

  //     }
  //   }
  // }
  var oBE = {
    "LaserficheID": this.form.controls["plantilla"].value,
    "IdEmision":  +this.IdEmision,
    "Perfil" : this._securityService.leerPerfil(),
    "NroReclamo":  "",
    "SaldoCredito": 0,
    "NroCredito": "",
    "EstadoCredito":  ""
  };
  this.loading = true;
  this._cnaMinutaOCMService.GenerarDocumento(oBE).subscribe(result=>{
    this.loading = false;
    const FechaActual = new Date();
    const AnioActual = FechaActual.getFullYear();
    const MesActual = (FechaActual.getMonth()<=9? '0'+ (FechaActual.getMonth()+1): FechaActual.getMonth() + 1); //FechaActual.getMonth() + 1;
    const DiaActual = (FechaActual.getDate()<=9? '0'+FechaActual.getDate(): FechaActual.getDate());
    const HoraActual = (FechaActual.getHours()<=9? '0'+FechaActual.getHours(): FechaActual.getHours()); //FechaActual.getHours();
    const MinutoActual = (FechaActual.getMinutes()<=9? '0'+FechaActual.getMinutes(): FechaActual.getMinutes()); //FechaActual.getMinutes();
    const SegundosActual = (FechaActual.getSeconds()<=9? '0'+FechaActual.getSeconds(): FechaActual.getSeconds()); //FechaActual.getMinutes(); //FechaActual.getSeconds();

    const NombreArchivo = "CNA_" + AnioActual + MesActual + DiaActual + "T" + HoraActual + MinutoActual + SegundosActual;
    //const NombreArchivo = valor + AnioActual + MesActual + DiaActual + "T" + HoraActual + MinutoActual + SegundosActual;
    if(result==""){
      this._dialogService.openMsgErrorDialog("Hubo un problema al intentar generar el documento");
    }
    else{
      //this.Descargar(result, NombreArchivo);
      this.Descargar(result.response,NombreArchivo,'application/vnd.openxmlformats-officedocument.wordprocessingml.document');
    }

  },
    (err)=>{
      this.loading = false;
      console.log(err);
    }
  );
}

DescargarPlantillaCR(): void
{

  if(this.form.controls["plantillaCR"].value==null){
    this._dialogService.openMsgErrorDialog("Seleccione una plantilla");
    return;
  }

  if(this.form.controls["plantillaCR"].value==""){
    this._dialogService.openMsgErrorDialog("Seleccione una plantilla");
    return;
  }

  let SaldoCredito;
  let NroCredito;
  let EstadoCredito;

  var oBE = {
    "LaserficheID": this.form.controls["plantillaCR"].value,
    "IdEmision":  +this.IdEmision,
    "Perfil" : this._securityService.leerPerfil(),
    "NroReclamo":  "",
    "SaldoCredito": 0,
    "NroCredito": "",
    "EstadoCredito":  ""
  };
  this.loading = true;
  this._cnaMinutaOCMService.GenerarDocumento(oBE).subscribe(result=>{
    this.loading = false;
    const FechaActual = new Date();
    const AnioActual = FechaActual.getFullYear();
    const MesActual = (FechaActual.getMonth()<=9? '0'+ (FechaActual.getMonth()+1): FechaActual.getMonth() + 1); //FechaActual.getMonth() + 1;
    const DiaActual = (FechaActual.getDate()<=9? '0'+FechaActual.getDate(): FechaActual.getDate());
    const HoraActual = (FechaActual.getHours()<=9? '0'+FechaActual.getHours(): FechaActual.getHours()); //FechaActual.getHours();
    const MinutoActual = (FechaActual.getMinutes()<=9? '0'+FechaActual.getMinutes(): FechaActual.getMinutes()); //FechaActual.getMinutes();
    const SegundosActual = (FechaActual.getSeconds()<=9? '0'+FechaActual.getSeconds(): FechaActual.getSeconds()); //FechaActual.getMinutes(); //FechaActual.getSeconds();

    const NombreArchivo = "CR_" + AnioActual + MesActual + DiaActual + "T" + HoraActual + MinutoActual + SegundosActual;
    //const NombreArchivo = valor + AnioActual + MesActual + DiaActual + "T" + HoraActual + MinutoActual + SegundosActual;
    if(result==""){
      this._dialogService.openMsgErrorDialog("Hubo un problema al intentar generar el documento");
    }
    else{
      //this.Descargar(result, NombreArchivo);
      this.Descargar(result.response,NombreArchivo,'application/vnd.openxmlformats-officedocument.wordprocessingml.document');
    }

  },
    (err)=>{
      this.loading = false;
      console.log(err);
    }
  );
}

  /* ****************************************************************************************
  Eventos Click
  *******************************************************************************************/
  AgregarComentario(): void {

    const dialogRef = this._dialog.open(CnaComentarioComponent, {
      width: '40%',
      data: this.CreditoClienteDetalle,
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log(`Dialog result: ${result.data}`);

      if(result){
        if(result.data){
          this.ObtenerDetalle();
        }
      }
      // this.ngOnInit();

    });

  }


  onClick(valor:any, valor2:any = null){
    console.log("onClick",valor);
    console.log("onClick",valor2);
    this.IdConfiguracionAccion = valor.idConfiguracionAccion;
    this.CNALaserFicheID = valor2;
    eval("this."+valor.metodo);
  }
  abrir_popup(){
    if(!this.bLaserficheCNA){
      this._dialogService.openMsgErrorDialog("Debe ingresar la carta de no adeudo");
      return;
    }
    const dialogRef = this._dialog.open(CnaDerivacionOcmComponent, {
      width: '40%',
      data:  {
               "CreditoClienteDetalle" : this.CreditoClienteDetalle,
               "IdConfiguracionAccion" : this.IdConfiguracionAccion
            }
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log(`Dialog result: ${result.data}`);

      if(result){
        if(result.data){
          this.ObtenerDetalle();
        }
      }
      // this.ngOnInit();

    });

  }
 /* ****************************************************************************************
  Evento Rechazar
  *******************************************************************************************/
  rechazar(): void {
    debugger;

    const request = {

    };
    this.CreditoClienteDetalle.IdConfiguracionAccion = this.IdConfiguracionAccion;

    this.form.controls["MedioNotificacion"].setValue('sdsd');

    const dialogRef = this._dialog.open(CnaRechazarComponent, {
      width: '40%',
      data: this.CreditoClienteDetalle,
    });

    dialogRef.afterClosed().subscribe((result) => {
        if(result.data){
          this.ObtenerDetalle();
        }
    });

  }

  popup_derivar_legal():void{

    if(this.documentosEvaluadosGarantia.length==0){
      this._dialogService.openMsgErrorDialog("Debe ingresar algún documento evaluado por Garantía");
      return;
    }

    const dialogRef = this._dialog.open(CnaDerivarLegalComponent, {
      width: '40%',
      data:  {
               "CreditoClienteDetalle" : this.CreditoClienteDetalle,
               "IdConfiguracionAccion" : this.IdConfiguracionAccion
            }
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log(`Dialog result: ${result.data}`);

      if(result){
        if(result.data){
          this.ObtenerDetalle();
        }
      }
      // this.ngOnInit();

    });
  }
  handleUpload2(event:any) {
    const file = event.target.files[0];
    if(file==null || file.length <=0){
       this.Archivo1 = null;
       this.NombreArchivo1 = null;
      return;
    }

    if (file.size > this.MAXIMO_TAMANIO_BYTES) {
       const tamanioEnMb = this.MAXIMO_TAMANIO_BYTES / 1024000;

       this.Archivo1 =  null;
       this.NombreArchivo1 = null;
       this.form.controls['FileCNA'].setValue(null);
       this.InputCNA.nativeElement.value = "";
       this._dialogService.openMsgErrorDialog(`El tamaño máximo es ${tamanioEnMb} MB`);


       return;
     }

     var allowedExtensions = /(.doc|.docx)$/i;
     if(!allowedExtensions.exec(file.name)){
      this._dialogService.openMsgErrorDialog("Formato no válido");
      this.Archivo1 = null;
      this.NombreArchivo1 = null;
      this.InputCNA.nativeElement.value = "";
      this.form.controls['FileCNA'].setValue(null);

      return;
     }

     const FileNamme = file.name;
     this.NombreArchivo1 = FileNamme;

     const reader = new FileReader();
     console.log("file",file);
     reader.readAsDataURL(file);
     reader.onload = () => {
       console.log("reader",reader);
       this.Archivo1 = reader.result || null;

         console.log(reader.result);
     };
}

 /* ****************************************************************************************
  Evento handleUpload : adjuntar de archivo para Carta de Respuesta
  *******************************************************************************************/
  handleUpload(event:any) {
    debugger;
    const file = event.target.files[0];
    if(file==null || file.length <=0){
       this.ArchivoCR = null;
       this.NombreArchivoCR = null;
      return;
    }

    var allowedExtensions = /(.pdf)$/i;
    if(!allowedExtensions.exec(file.name)){
     this.ArchivoCR =  null;
     this.NombreArchivoCR = null;
     this.form.controls['FileCR'].setValue(null);
     this._dialogService.openMsgErrorDialog("Formato no válido");
     return;
    }

    if (file.size > this.MAXIMO_TAMANIO_BYTES_CR) {
       const tamanioEnMb = this.MAXIMO_TAMANIO_BYTES_CR / 1024000;

       this.ArchivoCR =  null;
       this.NombreArchivoCR = null;
       this.form.controls['FileCR'].setValue(null);
       this._dialogService.openMsgErrorDialog(`El tamaño máximo es ${tamanioEnMb} MB`);


       return;
     } else {
       // Validación pasada. Envía el formulario o haz lo que tengas que hacer
     }

     const FileNamme = file.name;
     this.NombreArchivoCR = FileNamme;

     const reader = new FileReader();
     console.log("file",file);
     reader.readAsDataURL(file);
     reader.onload = () => {
       console.log("reader",reader);
       this.ArchivoCR = reader.result || null;

         console.log(reader.result);
     };
}


 /* ****************************************************************************************
  Evento subirCartaRespuesta : Subir carta de respuesta
  *******************************************************************************************/
subirCartaRespuesta(){
debugger;
  if(!this.ArchivoCR){
    this._dialogService.openMsgErrorDialog("Debe adjuntar la carta de respuesta.");
    return;
  }

  if( this.CreditoClienteDetalle.idLaserficheCNA != null || this.bLaserficheCNAAdmAge)
  {
     this._dialogService.openMsgErrorDialog("Debe eliminar la Carta de no adeudo, antes de subir una Carta de Respuesta.");
     return;
  }

        let request = {
          "CodRecurso" : 0,
          "CodTabla" : 8,
          "ReferenciaID" : 0,
          "LaserficheID" : 0,
          "IdRecursoEmision" : 0,
          "IdEmision" : +this.CreditoClienteDetalle.idEmision,
          "TipoRecursoID" : 3,
          "UsuarioRegistro" : this._securityService.leerUsuarioWeb(),
          "DNI" : ""+this.CreditoClienteDetalle.numeroDocumento,
          "Archivo" : this.ArchivoCR,
          "NombreArchivo" : this.NombreArchivoCR
        };

        this._dialogService
          .openConfirmDialog('¿Seguro que desea subir el documento?')
          .afterClosed()
          .subscribe((res) => {
            if (res) {
              this.loading = true;
                this._recursoEmisionService.registrar(request).subscribe((data:any)=>{
                  var resultado = data.response;
                  if(resultado.success.resultado){
                   // this.loading = false;
                    console.log("_recursoEmisionService.registrar",data);
                    this.ObtenerDetalle();
                    this.loading = false;
                  }
                  // else{
                  //   this.loading = true;
                  // }
                });
            }
          });
}

 /* ****************************************************************************************
  Evento enviarUDA : derivar a UDA la carta de respuesta
  *******************************************************************************************/
enviarUDA():void {
 debugger;

   if(!this.bLaserficheCR){
      this._dialogService.openMsgErrorDialog("Debe subir la carta de respuesta, antes de Enviar al Usuario de Agencia.");
      return;
    }

    const dialogRef = this._dialog.open(CnaDerivarUdaComponent, {
      width: '40%',
      data:  {
               "CreditoClienteDetalle" : this.CreditoClienteDetalle,
               "IdConfiguracionAccion" : this.IdConfiguracionAccion 
            }
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log(`Dialog result: ${result.data}`);

      if(result){
        if(result.data){
          this.ObtenerDetalle();
        }
      }
      // this.ngOnInit();

    });

  // this.CreditoClienteDetalle.IdConfiguracionAccion = this.IdConfiguracionAccion;

  // var codAgencia = '';
  // codAgencia = this.CreditoClienteDetalle.codAgenciaDerivacion;
  // if(codAgencia = '0')  codAgencia = this.CreditoClienteDetalle.codAgencia;

  // this._dialogService.openConfirmDialog('¿Está seguro de enviar al Usuario de Agencia?')
  // .afterClosed().subscribe(res => {
  //   if (res) {
  //     this.loading = true;
  //     var reques = {
  //       "IdConfiguracionAccion": this.CreditoClienteDetalle.IdConfiguracionAccion,
  //       "NroDocumento": this.CreditoClienteDetalle.numeroDocumento,
  //       "TipoDocumento": this.CreditoClienteDetalle.tipoDocumento,
  //       "Nombres": this.CreditoClienteDetalle.nombres,
  //       "Perfil" : Constantes.UDA.toUpperCase(),

  //       "emision": {"IdEmision": this.CreditoClienteDetalle.idEmision,
  //                   "IdCreditoCliente":this.CreditoClienteDetalle.idCreditoCliente,
  //                   "UsuarioModificacion": this._securityService.leerUsuarioWeb()
  //                  },
  //       "derivacion":{"IdEmision": this.CreditoClienteDetalle.idEmision,
  //                     "IdDerivacion":this.CreditoClienteDetalle.idDerivacion,
  //                     "Perfil": this._securityService.leerPerfil(),
  //                     "UsuarioRegistro": this._securityService.leerUsuarioWeb(),
  //                     "UsuarioModificacion": this._securityService.leerUsuarioWeb(),
  //                     "CodAgencia": codAgencia,
  //                     "PerfilDerivado" : Constantes.UDA.toUpperCase(),
  //                    }
  //     };

  //     this._configuracionAccion.accion(reques).subscribe((data:any) => {

  //       if(data.response.success.resultado){
  //         this.ObtenerDetalle(this.loading);
  //         this._dialogService.openMsgSuccessDialog("Se realizó el envío de la CR al Usuario de Agencia.");
  //       }
  //       else{
  //         this.ObtenerDetalle(this.loading);
  //         this._dialogService.openMsgSuccessDialog("Hubo un problema en el envío de la CR.");
  //       }
  //     },
  //       (err) => {
  //         this._dialogService.openMsgErrorDialog("Hubo un error en el envío de la CR");
  //         this.loading = false;
  //     });
  //   }
  // });
}

  subirCartaNoAdeudo(){
    if(!this.Archivo1){
      this._dialogService.openMsgErrorDialog("Debe de seleccionar una carta de no adeudo");
      return;
    }

    if(this.CreditoClienteDetalle.idLaserficheCR){
      this._dialogService.openMsgErrorDialog("Debe eliminar la Carta de Respuesta, antes de subir una Carta de no adeudo");
      return;
    }

    let request = {
      "CodRecurso" : 0,
      "CodTabla" : 8,
      "ReferenciaID" : 0,
      "LaserficheID" : 0,
      "IdRecursoEmision" : 0,
      "IdEmision" : +this.CreditoClienteDetalle.idEmision,
      "TipoRecursoID" : 1,
      "UsuarioRegistro" : this._securityService.leerUsuarioWeb(),
      "DNI" : ""+this.CreditoClienteDetalle.numeroDocumento,
      "TipoDocumento": ""+this.CreditoClienteDetalle.tipoDocumento,
      "Archivo" : this.Archivo1,
      "NombreArchivo" : this.NombreArchivo1
    };

    this._dialogService
      .openConfirmDialog('¿Seguro que desea subir el documento?')
      .afterClosed()
      .subscribe((res) => {
        if (res) {

          this.loading = true;
            this._recursoEmisionService.registrar(request).subscribe((data:any)=>{
              console.log("_recursoEmisionService.registrar",data);
             // this.ObtenerDetalle();

              var request = {
                "IdConfiguracionAccion" : this.IdConfiguracionAccion,
                "GrupoFiltro" : null,
                "emision": {
                  "IdEmision" : +this.CreditoClienteDetalle.idEmision,
                  "IdCreditoCliente"  : +this.CreditoClienteDetalle.idCreditoCliente,
                  "Estado"  : this.CreditoClienteDetalle.idEstado,
                  "TipoFlujo"  :1,
                  "FechaEntregaOlva" : null,
                  "MedioNotificacion": null,
                  "Email" : null,
                  "Whatsapp" : null,
                  "FechaCargoEntrega" : null,
                  "UsuarioRegistro" :  this._securityService.leerUsuarioWeb(),
                  "UsuarioModificacion" :  this._securityService.leerUsuarioWeb()
                },
                "derivacion": {
                  "IdDerivacion" : +this.CreditoClienteDetalle.idDerivacion,
                  "IdEmision" : +this.CreditoClienteDetalle.idEmision,
                  "Perfil" : this._securityService.leerPerfil(),
                  "Estado" : this.CreditoClienteDetalle.idEstado,
                  "PerfilDerivado" : null,
                  "FechaDerivado" : null,
                  "CodAgencia" : null,
                  "UsuarioRegistro" : this._securityService.leerUsuarioWeb(),
                  "UsuarioModificacion":  this._securityService.leerUsuarioWeb()
                },
                "comentarioEmision": {

                }
              };

              this._configuracionAccion.accion(request).subscribe((data:any)=>{
                console.log("this._configuracionAccion.accion - Subir Archivo",data);
                debugger;

                this.ObtenerDetalle(this.loading);
                this._dialogService.openMsgSuccessDialog("Se subió correctamente la CNA");
              }, (error) =>{
                console.log(error);
              //  this.loading = false;
                this.ObtenerDetalle(this.loading);
                this._dialogService.openMsgSuccessDialog("Hubo un error al subir la CNA");
              } );

             // this.loading = false;
            }, (error) =>{
              console.log(error);
              this._dialogService.openMsgErrorDialog("Hubo un error al subir la CNA");
             this.loading = false;
            });
        }
      });

  }

eliminarArchivoLaserFiche(idLaserficheCNA:any = null) : void {

    this._dialogService
      .openConfirmDialog('¿Seguro que desea eliminar el documento?')
      .afterClosed()
      .subscribe((res) => {
        if (res) {
          debugger;
          if(this.CNALaserFicheID!=null)
              idLaserficheCNA = this.CNALaserFicheID;

          const oBE = {
            "LaserficheID": ""+idLaserficheCNA
            };
            this.loading = true;
            this._requerimientoService.eliminarArchivo(oBE).subscribe(result=>{
              this.loading = false;

             
              if(result.response.resultado==1){

                this.loading = true;
                var request = {
                  "IdConfiguracionAccion" : this.IdConfiguracionAccion,
                  "GrupoFiltro" : null,
                  "emision": {
                    "IdEmision" : +this.CreditoClienteDetalle.idEmision,
                    "IdCreditoCliente"  : +this.CreditoClienteDetalle.idCreditoCliente,
                    "Estado"  : this.CreditoClienteDetalle.idEstado,
                    "TipoFlujo"  :1,
                    "FechaEntregaOlva" : null,
                    "MedioNotificacion": null,
                    "Email" : null,
                    "Whatsapp" : null,
                    "FechaCargoEntrega" : null,
                    "UsuarioRegistro" :  this._securityService.leerUsuarioWeb(),
                    "UsuarioModificacion" :  this._securityService.leerUsuarioWeb()
                  },
                  "derivacion": {
                    "IdDerivacion" : +this.CreditoClienteDetalle.idDerivacion,
                    "IdEmision" : +this.CreditoClienteDetalle.idEmision,
                    "Perfil" : this._securityService.leerPerfil(),
                    "Estado" : this.CreditoClienteDetalle.idEstado,
                    "PerfilDerivado" : null,
                    "FechaDerivado" : null,
                    "CodAgencia" : null,
                    "UsuarioRegistro" : this._securityService.leerUsuarioWeb(),
                    "UsuarioModificacion":  this._securityService.leerUsuarioWeb()
                  },
                  "comentarioEmision": {

                  }
                };

                this._configuracionAccion.accion(request).subscribe((data:any)=>{
                  this._dialogService.openMsgSuccessDialog("Se eliminó correctamente").afterClosed().subscribe(res=>{
                  });
                  this.loading = false;
                  this.ObtenerDetalle();

                }, (error) =>{
                  console.log(error);
                  this.loading = false;
                  this.ObtenerDetalle();
                } );


              }
              else{
               // this.bCartaRespuesta = false;
                this._dialogService.openMsgErrorDialog("Se presentó un problema para eliminar el documento");
              }

            },
            (err)=>{
              this.loading = false;
              console.log(err);
            }
            );

        }
   });

   }

   
eliminarArchivoLaserFicheCE(idLaserficheCNA:any = null) : void {

  this._dialogService
    .openConfirmDialog('¿Seguro que desea eliminar el documento?')
    .afterClosed()
    .subscribe((res) => {
      debugger;
      if (res) {
        if(this.CNALaserFicheID!=null)
            idLaserficheCNA = this.CNALaserFicheID;

         const oBE = {
          "LaserficheID": ""+idLaserficheCNA
          };
          this.loading = true;
          this._requerimientoService.eliminarArchivo(oBE).subscribe((data:any)=>{
                this._dialogService.openMsgSuccessDialog("Se eliminó correctamente").afterClosed().subscribe(res=>{
                });
                this.loading = false;
                this.ObtenerDetalle();

              }, (error) =>{
                console.log(error);
                this.loading = false;
                this.ObtenerDetalle();
              } );
            }            
          },
          (err)=>{
            this.loading = false;
          });
      
 }

 /* ****************************************************************************************
  Evento handleUpload3 : adjuntar de archivo para Cargo de Entrega
  *******************************************************************************************/
  handleUpload3(event:any) {

     const file = event.target.files[0];
       if(file==null || file.length <=0){
          this.ArchivoCE = null;
          this.NombreArchivoCE = null;
         return;
       }

       var allowedExtensions = /(.jpg|.jpeg|.pdf|.png|.tif)$/i;
       if(!allowedExtensions.exec(file.name)){
        this.ArchivoCE =  null;
        this.NombreArchivoCE = null;
        this.form.controls['FileCE'].setValue(null);
        this._dialogService.openMsgErrorDialog("Formato no válido");
        return;
       }

       if (file.size > this.MAXIMO_TAMANIO_BYTES_CE) {
          const tamanioEnMb = this.MAXIMO_TAMANIO_BYTES_CE / 1024000;

          this.ArchivoCE =  null;
          this.NombreArchivoCE = null;
          this.form.controls['FileCE'].setValue(null);
          this._dialogService.openMsgErrorDialog(`El tamaño máximo es ${tamanioEnMb} MB`);


          return;
        } else {
          // Validación pasada. Envía el formulario o haz lo que tengas que hacer
        }


     const FileNamme = file.name;
     this.NombreArchivoCE = FileNamme;

     const reader = new FileReader();

     reader.readAsDataURL(file);
     reader.onload = () => {

       this.ArchivoCE = reader.result || null;

     };
}


 /* ****************************************************************************************
  Evento subirCargoEntrega : Subir cargo de entrega
  *******************************************************************************************/
  subirCargoEntrega(){
    debugger;
      if(!this.ArchivoCE){
        this._dialogService.openMsgErrorDialog("Debe adjuntar el cargo de entrega.");
        return;
      }


        let request = {
          "CodRecurso" : 0,
          "CodTabla" : 8,
          "ReferenciaID" : 0,
          "LaserficheID" : 0,
          "IdRecursoEmision" : 0,
          "IdEmision" : +this.CreditoClienteDetalle.idEmision,
          "TipoRecursoID" : 7,
          "UsuarioRegistro" : this._securityService.leerUsuarioWeb(),
          "DNI" : ""+this.CreditoClienteDetalle.numeroDocumento,
          "Archivo" : this.ArchivoCE,
          "NombreArchivo" : this.NombreArchivoCE
        };

        this._dialogService
          .openConfirmDialog('¿Seguro que desea subir el documento?')
          .afterClosed()
          .subscribe((res) => {
            if (res) {
              this.loading = true;
                this._recursoEmisionService.registrar(request).subscribe((data:any)=>{
                  var resultado = data.response;
                  if(resultado.success.resultado){
                    // this.loading = false;
                    console.log("_recursoEmisionService.registrar",data);
                    this.ObtenerDetalle();
                    this.loading = false;
                  }
                });
            }
          });
    }

/* ****************************************************************************************
  Evento enviarParaNotificacion :enviar para su notificacion
  *******************************************************************************************/
  enviarParaNotificacion():void {
    debugger;
    this.formSubmitted = true;

    if( this.form.controls["MedioNotificacion"].value == "" ||
        this.form.controls["MedioNotificacion"].value == null ||
      ((this.form.controls["Email"].value == "" || this.form.controls["Email"].value == null ) && this.mostrarEmail) ||
      ((this.form.controls["Whatsapp"].value == "" || this.form.controls["Whatsapp"].value ==null )&& this.mostrarWhatsapp) ) {
       return;
     }

     this.CreditoClienteDetalle.IdConfiguracionAccion = this.IdConfiguracionAccion;

     this._dialogService.openConfirmDialog('¿Está seguro de enviar para su notificación?')
     .afterClosed().subscribe(res => {
       if (res) {
         this.loading = true;
         var reques = {
           "IdConfiguracionAccion": this.CreditoClienteDetalle.IdConfiguracionAccion,
           "Nombres": this.CreditoClienteDetalle.nombres,

           "emision": {"IdEmision": this.CreditoClienteDetalle.idEmision,
                       "IdCreditoCliente":this.CreditoClienteDetalle.idCreditoCliente,
                       "UsuarioModificacion": this._securityService.leerUsuarioWeb(),
                       "MedioNotificacion": +this.form.controls["MedioNotificacion"].value,
                       "Email": this.form.controls["Email"].value,
                       "Whatsapp": this.form.controls["Whatsapp"].value
                      },
           "derivacion":{"IdEmision": this.CreditoClienteDetalle.idEmision,
                         "IdDerivacion":this.CreditoClienteDetalle.idDerivacion,
                         "Perfil": this._securityService.leerPerfil(),
                         "UsuarioRegistro": this._securityService.leerUsuarioWeb(),
                         "UsuarioModificacion": this._securityService.leerUsuarioWeb(),
                         "CodAgencia": 0
                        }
         };

         this._configuracionAccion.accion(reques).subscribe((data:any) => {
          if(data.response.success.resultado){
            this.ObtenerDetalle(this.loading);
            this._dialogService.openMsgSuccessDialog("Se realizó el envio para la notificación.");
          }
          else{
            this.ObtenerDetalle(this.loading);
            this._dialogService.openMsgSuccessDialog("Hubo un problema en el envío para la notificación.");
          }
        },
          (err) => {
            this._dialogService.openMsgErrorDialog("Hubo un error en el envío para la notificación.");
            this.loading = false;

         });
       }
     });
   }


/* ****************************************************************************************
  Evento confirmarCargoEntrega : cofirmar el cargo de entrega
  *******************************************************************************************/
  confirmarCargoEntrega():void {
    debugger;
    this.formSubmitted = true;

    if( this._securityService.leerPerfil() == Constantes.UDA.toUpperCase() ){

      if(this.CreditoClienteDetalle.codMedioNot != Constantes.CNAFisico){
          this._dialogService.openMsgErrorDialog("El medio notificación no es fisico, el OCM debe confirmar la entrega.");
          return;
      }
      if(this.CreditoClienteDetalle.codMedioNot == Constantes.CNAFisico && !this.bLaserficheCE ){
          this._dialogService.openMsgErrorDialog("Debe subir el cargo de entrega.");
          return;
      }
    }

    if( this._securityService.leerPerfil() == Constantes.OCM.toUpperCase() ){

      if(this.CreditoClienteDetalle.codMedioNot == Constantes.CNAFisico){
          this._dialogService.openMsgErrorDialog("El medio notificación es fisico, el UDA debe confirmar la entrega.");
          return;
      }
      if(this.CreditoClienteDetalle.codMedioNot != Constantes.CNAFisico && !this.bLaserficheCE ){
          this._dialogService.openMsgErrorDialog("Debe subir el cargo de entrega.");
          return;
      }
    }

     this.CreditoClienteDetalle.IdConfiguracionAccion = this.IdConfiguracionAccion;

     this._dialogService.openConfirmDialog('¿Está seguro de confirmar la entrega?')
     .afterClosed().subscribe(res => {
       if (res) {
         this.loading = true;
         var reques = {
           "IdConfiguracionAccion": this.CreditoClienteDetalle.IdConfiguracionAccion,

           "emision": {"IdEmision": this.CreditoClienteDetalle.idEmision,
                       "IdCreditoCliente":this.CreditoClienteDetalle.idCreditoCliente,
                       "UsuarioModificacion": this._securityService.leerUsuarioWeb()
                      //  "MedioNotificacion": +this.form.controls["MedioNotificacion"].value,
                      //  "Email": this.form.controls["Email"].value,
                      //  "Whatsapp": this.form.controls["Whatsapp"].value
                      },
           "derivacion":{"IdEmision": this.CreditoClienteDetalle.idEmision,
                         "IdDerivacion":this.CreditoClienteDetalle.idDerivacion,
                         "Perfil": this._securityService.leerPerfil(),
                         "UsuarioRegistro": this._securityService.leerUsuarioWeb(),
                         "UsuarioModificacion": this._securityService.leerUsuarioWeb(),
                         "CodAgencia": this.CreditoClienteDetalle.codAgencia
                        }
         };

         this._configuracionAccion.accion(reques).subscribe((data:any) => {
          if(data.response.success.resultado){
            this.ObtenerDetalle(this.loading);
            this._dialogService.openMsgSuccessDialog("Se realizó el envio de cargo de entrega.");
          }
          else{
            this.ObtenerDetalle(this.loading);
            this._dialogService.openMsgSuccessDialog("Hubo un problema en el envío del cargo de entrega.");
          }
        },
          (err) => {
            this._dialogService.openMsgErrorDialog("Hubo un error en el envío del cargo de entrega.");
            this.loading = false;

         });
       }
     });
   }


   onChangeMN():void{

    this.mostrarEmail = false;
    this.mostrarWhatsapp = false;

      if ( this.form.controls["MedioNotificacion"].value == Constantes.CNAEmail){
          this.mostrarEmail = true;
          this.formSubmitted = false;
      }

      if ( this.form.controls["MedioNotificacion"].value == Constantes.CNAWhatsapp){
        this.mostrarWhatsapp = true;
        this.formSubmitted = false;
      }
   }
/* ****************************************************************************************
  Evento finalizarAtencion
  *******************************************************************************************/
  finalizarAtencion():void {
    debugger;
    var estadoEmision = 0;
    var estadoDerivacion = 0;
    var esAsociadoGarantia = 0;
    debugger;
    if (this.documentosEvaluadosGarantia.length > 0 ) esAsociadoGarantia = 1;

    // if(this.CreditoClienteDetalle.idLaserficheCNA && !this.CreditoClienteDetalle.idLaserficheCR)
    //   {
    //     estadoEmision = Constantes.EstadoEmisionAtendido;
    //     estadoDerivacion = Constantes.EstadoDerivacionAtendido;
    //   }
      if(this.bLaserficheCNAAdmAge && !this.CreditoClienteDetalle.idLaserficheCR)
      {
        estadoEmision = Constantes.EstadoEmisionAtendido;
        estadoDerivacion = Constantes.EstadoDerivacionAtendido;
      }
      

     if(this.CreditoClienteDetalle.idLaserficheCR && !this.bLaserficheCNAAdmAge)
      {
        estadoEmision = Constantes.EstadoEmisionObservado;
        estadoDerivacion = Constantes.EstadoDerivacionObservado;
      }

     this.CreditoClienteDetalle.IdConfiguracionAccion = this.IdConfiguracionAccion;

     this._dialogService.openConfirmDialog('¿Está seguro de Finalizar la atención?')
     .afterClosed().subscribe(res => {
       if (res) {
         this.loading = true;
         var reques = {
           "IdConfiguracionAccion": this.CreditoClienteDetalle.IdConfiguracionAccion,
           "NroDocumento": this.CreditoClienteDetalle.numeroDocumento,
           "TipoDocumento": this.CreditoClienteDetalle.tipoDocumento,
           "Nombres": this.CreditoClienteDetalle.nombres,
           "EsAsociadoGarantia": esAsociadoGarantia,

           "emision": {"IdEmision": this.CreditoClienteDetalle.idEmision,
                       "IdCreditoCliente":this.CreditoClienteDetalle.idCreditoCliente,
                       "UsuarioModificacion": this._securityService.leerUsuarioWeb(),
                       "Estado": estadoEmision
                      },
           "derivacion":{"IdEmision": this.CreditoClienteDetalle.idEmision,
                         "IdDerivacion":this.CreditoClienteDetalle.idDerivacion,
                         "Perfil": this._securityService.leerPerfil(),
                         "UsuarioRegistro": this._securityService.leerUsuarioWeb(),
                         "UsuarioModificacion": this._securityService.leerUsuarioWeb(),
                         "CodAgencia": this.CreditoClienteDetalle.codAgencia,
                         "Estado" : estadoDerivacion
                        }
         };

         this._configuracionAccion.finalizar(reques).subscribe((data:any) => {
          if(data.response.success.resultado){
            this.ObtenerDetalle(this.loading);
          }
        },
          (err) => {
            this._dialogService.openMsgErrorDialog("Hubo un error en finalizar.");
            this.loading = false;

         });
       }
     });
   }

   DescargarArchivoLaserFiche(LaserficheID:any,NameDocumento:any): void {
    this.loading = true;
    const params = {
      LaserficheID: ""+LaserficheID
    };
    this._requerimientoService.DescagarArchivos(params).subscribe((result) => {
      console.log('response DescargarArchivoLaserFiche', result.response);
      if(result.response!=null){
        if(result.response.length>0){
          for (let index = 0; index < result.response.length; index++) {
            const element = result.response[index];
            // debugger;
            // const FechaActual = new Date();
            // const AnioActual = FechaActual.getFullYear();
            // const MesActual = (FechaActual.getMonth()<=9? '0'+ (FechaActual.getMonth()+1): FechaActual.getMonth() + 1); //FechaActual.getMonth() + 1;
            // const DiaActual = (FechaActual.getDate()<=9? '0'+FechaActual.getDate(): FechaActual.getDate());
            // const HoraActual = (FechaActual.getHours()<=9? '0'+FechaActual.getHours(): FechaActual.getHours()); //FechaActual.getHours();
            // const MinutoActual = (FechaActual.getMinutes()<=9? '0'+FechaActual.getMinutes(): FechaActual.getMinutes()); //FechaActual.getMinutes();
            // const SegundosActual = (FechaActual.getSeconds()<=9? '0'+FechaActual.getSeconds(): FechaActual.getSeconds()); //FechaActual.getMinutes(); //FechaActual.getSeconds();

            // const NombreArchivo = NameDocumento + "_" + AnioActual + MesActual + DiaActual + "T" + HoraActual + MinutoActual + SegundosActual;

            this.Descargar(element.archivoBytes,element.nombreDocumento,element.contentType);
          }
        }
      }
      this.loading = false;
    });
  }



  Descargar(base64String:any, fileName:any,contentType:any) {

    const blobData = this.convertBase64ToBlobData(base64String,contentType);
    const blob = new Blob([blobData], { type: contentType });
        const url = window.URL.createObjectURL(blob);
        // window.open(url);
        const link = document.createElement('a');
        link.href = url;
        link.download = fileName;
        link.click();
  }
  convertBase64ToBlobData(base64Data: string, contentType: string = 'image/png', sliceSize = 2024) {
    const byteCharacters = atob(base64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);
      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
   const allowedFileTypes = ["image/png", "image/jpeg", "image/gif","image/Tif", "application/pdf"];

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  cargarPaginaOut(){

    this.ObtenerDetalle();
  }
  cargarPaginaLegalOut(){
    this.ObtenerDetalle();
  }

  AgregarComentarioAccionDerivacion(){

    const dialogRef = this._dialog.open(CnaComentarioComponent, {
      width: '40%',
      data:  {
        "CreditoClienteDetalle" : this.CreditoClienteDetalle,
        "IdConfiguracionAccion" : this.IdConfiguracionAccion
     }
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log(`Dialog result: ${result.data}`);

      if(result){
        if(result.data){
          this.ObtenerDetalle();
        }
      }
      // this.ngOnInit();

    });
  }

  popup_derivar_ADMAGE(){

    if(!this.documentosMinuta || !this.documentosCartaEntrega){
      this._dialogService.openMsgErrorDialog("Debe de ingresar los documentos: Minuta y la carta de Entrega");
      return;
    }

    if(this.documentosMinuta.length==0 || this.documentosCartaEntrega.length==0){
      this._dialogService.openMsgErrorDialog("Debe de ingresar los documentos: Minuta y la carta de Entrega");
      return;
    }

    if(!this.FechaEntregaOlva){
      this._dialogService.openMsgErrorDialog("Debe ingresar la fecha de entrega de Olva");
      return;
    }




    const dialogRef = this._dialog.open(CnaDerivarAdmageComponent, {
      width: '40%',
      data:  {
               "CreditoClienteDetalle" : this.CreditoClienteDetalle,
               "IdConfiguracionAccion" : this.IdConfiguracionAccion,
               "FechaEntregaOlva" : this.FechaEntregaOlva
            }
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log(`Dialog result: ${result.data}`);

      if(result){
        if(result.data){
          this.ObtenerDetalle();
        }
      }
      // this.ngOnInit();

    });

  }

  FechaEntregaOlvaOut(valor:any){
    this.FechaEntregaOlva = valor;
  }

  popup_derivar_usuario_agencia(){
    const dialogRef = this._dialog.open(CnaDerivarComponent, {
      width: '40%',
      data:  {
               "CreditoClienteDetalle" : this.CreditoClienteDetalle,
               "IdConfiguracionAccion" : this.IdConfiguracionAccion,
               "FechaEntregaOlva" : this.FechaEntregaOlva
            }
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log(`Dialog result: ${result.data}`);

      if(result){
        if(result.data){
          this.ObtenerDetalle();
        }
      }
      // this.ngOnInit();

    });
  }

  reaperturar(){
    const dialogRef = this._dialog.open(CnaReaperturaComponent, {
      width: '40%',
      data:  {
               "CreditoClienteDetalle" : this.CreditoClienteDetalle,
               "IdConfiguracionAccion" : this.IdConfiguracionAccion
            }
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log(`Dialog result: ${result.data}`);

      if(result){
        if(result.data){
          this.ObtenerDetalle();       
        }
      }
      // this.ngOnInit();

    });
  }
}


