import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { RequerimientoService } from 'src/app/services/requerimiento.service';
import { ActivatedRoute } from '@angular/router';
import { forkJoin} from 'rxjs';

@Component({
  selector: 'app-consulta-detalle',
  templateUrl: './consulta-detalle.component.html',
  styleUrls: ['./consulta-detalle.component.css']
})
export class ConsultaDetalleComponent implements OnInit {
 
  loading!:boolean;


   /* Variables Consulta */
   CodConsulta: any;
   ConsultaDetalle:any;

  constructor(
    private requerimientoServices : RequerimientoService,
    private route: ActivatedRoute
  ) { 
    this.CodConsulta = this.route.snapshot.params.id;
  }

  ngOnInit(): void {

    this.ObtenerConsultaDetalle();
  }


  ObtenerConsultaDetalle(): void {
  debugger;
    const params = {
      CodConsulta: this.CodConsulta,
      CodTabla : 4
    };
    this.requerimientoServices
      .ObtenerConsultaDetalle(params)
      .subscribe((result) => {
        if(result.response){

          this.ConsultaDetalle = result.response;
          console.log("this.ConsultaDetalle",this.ConsultaDetalle)

        }

      })

  }


  
  DescargarArchivos(LaserficheID:any): void {
    this.loading = true;
    const params = {
      LaserficheID: ""+LaserficheID
    };
    this.requerimientoServices.DescagarArchivos(params).subscribe((result) => {
      console.log('response', result.response);
      if(result.response!=null){
        if(result.response.length>0){
          for (let index = 0; index < result.response.length; index++) {
            const element = result.response[index];
            debugger;
            this.Descargar(element.archivoBytes,element.nombreDocumento,element.contentType);
          }
        }
      }
      this.loading = false;
    });
  }

  Descargar(base64String:any, fileName:any,contentType:any) {
    debugger;
    const blobData = this.convertBase64ToBlobData(base64String,contentType);
    const blob = new Blob([blobData], { type: contentType });
        const url = window.URL.createObjectURL(blob);
        // window.open(url);
        const link = document.createElement('a');
        link.href = url;
        link.download = fileName;
        link.click();
  }

  convertBase64ToBlobData(base64Data: string, contentType: string = 'image/png', sliceSize = 1024) {
    const byteCharacters = atob(base64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);
      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }
}
