﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.LaserFiche.API.Models
{
    public class Respuesta
    {
        public int Resultado { get; set; }
        public string Mensaje { get; set; }
        public dynamic Data { get; set; }

        public List<DocumentModel> DataList { get; set; }
    }
}