import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DialogService } from 'src/app/services/comun/dialog.service';
import { SecurityService } from 'src/app/services/security.service';
import { Constantes } from 'src/app/comun/constantes';
import { ComentarioEmisionService } from 'src/app/services/comentario-emision.service';
import { ConfiguracionAccionService } from '../../../services/configuracion-accion.service';

@Component({
  selector: 'app-cna-comentario',
  templateUrl: './cna-comentario.component.html',
  styleUrls: ['./cna-comentario.component.css']
})
export class CnaComentarioComponent implements OnInit {

  form!: FormGroup;
  formSubmitted:boolean = false;

  lengthDescripcion: number  = 300;
  MaxlengthDescripcion: number  = 300;
  MinlengthDescripcion: number  = 5;
  MAXIMO_TAMANIO_BYTES = 5120000; // 5MB = 5 millón de bytes
  Archivo1:any;
  NombreArchivo1:any;

  loading:boolean = false;
  CreditoClienteDetalle: any;
  emision:any;
  IdConfiguracionAccion:any;

  constructor(
    private _formBuilder: FormBuilder,
    private _dialogService: DialogService,
    private _securityService: SecurityService,
    private _comentarioEmisionService : ComentarioEmisionService,
    private _configuracionAccion : ConfiguracionAccionService,
    public dialogRef: MatDialogRef<CnaComentarioComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    //this.CreditoClienteDetalle = data;
    this.emision = data.CreditoClienteDetalle;
    this.IdConfiguracionAccion = data.IdConfiguracionAccion;
   }

  ngOnInit(): void {
    this.inicializarFormulario();
  }
  inicializarFormulario() : void {
    this.form = this._formBuilder.group({
    CodArea:[null],
    Descripcion: [null,  [Validators.required, Validators.minLength(5)]],
    Archivo1:[null],
    NombreArchivo1:this.NombreArchivo1,

  });

  }

  handleUpload(event:any) {
       const file = event.target.files[0];
       if(file==null || file.length <=0){
          this.Archivo1 = null;
          this.NombreArchivo1 = null;
         return;
       }

       var allowedExtensions = /(.jpg|.jpeg|.pdf|.png|.tif|.eml|.msg)$/i;
       if(!allowedExtensions.exec(file.name)){
        this.Archivo1 =  null;
        this.NombreArchivo1 = null;
        this.form.controls['Archivo1'].setValue(null);
        this._dialogService.openMsgErrorDialog("Formato no válido");
        return;
       }

       if (file.size > this.MAXIMO_TAMANIO_BYTES) {
          const tamanioEnMb = this.MAXIMO_TAMANIO_BYTES / 1024000;

          this.Archivo1 =  null;
          this.NombreArchivo1 = null;
          this.form.controls['Archivo1'].setValue(null);
          this._dialogService.openMsgErrorDialog(`El tamaño máximo es ${tamanioEnMb} MB`);


          return;
        } else {
          // Validación pasada. Envía el formulario o haz lo que tengas que hacer
        }

        const FileNamme = file.name;
        this.NombreArchivo1 = FileNamme;

        const reader = new FileReader();
        console.log("file",file);
        reader.readAsDataURL(file);
        reader.onload = () => {
          console.log("reader",reader);
          this.Archivo1 = reader.result || null;

            console.log(reader.result);
        };
  }

  registrar():void {
    debugger;
     this.formSubmitted = true;

     if (this.form.invalid) {
       return;
     }

     this._dialogService.openConfirmDialog('¿Está seguro de agregar este comentario?')
     .afterClosed().subscribe(res => {
       if (res) {
         const perfil = this._securityService.leerUsuarioWeb();
        this.loading = true;


         var request = {
          "IdConfiguracionAccion" : this.IdConfiguracionAccion,
          "GrupoFiltro" : null,
          "Nombres": this.emision.nombres,
          "TipoDocumento": this.emision.tipoDocumento,
          "NroDocumento": this.emision.numeroDocumento,
          "emision": {
            "IdEmision" : +this.emision.idEmision,
            "IdCreditoCliente"  : +this.emision.idCreditoCliente,
            "Estado"  : this.emision.idEstado,
            "TipoFlujo"  :1,
            "FechaEntregaOlva" : null,
            "MedioNotificacion": null,
            "Email" : null,
            "Whatsapp" : null,
            "FechaCargoEntrega" : null,
            "UsuarioRegistro" :  this._securityService.leerUsuarioWeb(),
            "UsuarioModificacion" :  this._securityService.leerUsuarioWeb()
          },
          "derivacion": {
            "IdDerivacion" : +this.emision.idDerivacion,
            "IdEmision" : +this.emision.idEmision,
            "Perfil" : this._securityService.leerPerfil(),
            "Estado" : this.emision.idEstado,
            "PerfilDerivado" : null,
            "FechaDerivado" : null,
            "CodAgencia" : 0,
            "UsuarioRegistro" : this._securityService.leerUsuarioWeb(),
            "UsuarioModificacion":  this._securityService.leerUsuarioWeb()
          },
          "comentarioEmision":
          {
              "IdEmision" : this.emision.idEmision,
              "IdDerivacion" : this.emision.idDerivacion,
              "IdDerivacionMovimiento" :  this.emision.idDerivacionMovimiento,
              "Texto" :  this.form.controls['Descripcion'].value,
              "UsuarioRegistro" :  this._securityService.leerUsuarioWeb(),
              "Archivo" : this.Archivo1,
              "NombreArchivo" :  this.NombreArchivo1,
              "CodTabla" : Constantes.ComentarioEmision,
              "DNI": this.emision.numeroDocumento,
              "Perfil": this._securityService.leerPerfil(),
              "CodAgencia":  this.emision.codAgencia,
              "Estado": this.emision.idEstado,
              "IdMotivo" : ''
          }
        };



        this._configuracionAccion.accion(request).subscribe((data:any)=>{
          console.log(data);
          debugger;
          this.loading = false;
          if(data.response.success.resultado){
            this._dialogService.openMsgSuccessDialog("Se registró correctamente el comentario");
            this.dialogRef.close({ data: true });
          }
          else{

            this._dialogService.openMsgSuccessDialog("Hubo un problema al registrar el comentario");
            this.dialogRef.close({ data: true });
          }
        },
          (err) => {
            this.loading = false;
            console.error(err);
        });


        //  this._comentarioEmisionService.registrar(request).subscribe(resultado=>{
        //    console.log("response",resultado);
        //    this.loading = false;
        //    var result = resultado.response;

        //    if(result.success.resultado){
        //      this._dialogService
        //    .openMsgSuccessDialog('Se registró correctamente el comentario')
        //    .afterClosed()
        //    .subscribe((res) => {
        //      if(res){
        //       this.dialogRef.close({ data: true });
        //      }
        //    });

        //    }
        //    else{
        //     this.dialogRef.close({ data: false });
        //    }


        //  }, (err) => {
        //   this.loading = false;
        //   console.error(err);
        // });

       }

     });

   }
}
