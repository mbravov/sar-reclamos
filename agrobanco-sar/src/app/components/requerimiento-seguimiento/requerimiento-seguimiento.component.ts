import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ExcelJsService } from 'src/app/services/comun/excel-js.service';
import { SeguimientoService } from 'src/app/services/seguimiento.service';
import { forkJoin } from 'rxjs';
import { ParametroService } from 'src/app/services/parametro.service';
import * as moment from 'moment';
import { RequerimientoSeguimiento } from '../models/RequerimientoSeguimiento.interface';

@Component({
  selector: 'app-requerimiento-seguimiento',
  templateUrl: './requerimiento-seguimiento.component.html',
  styleUrls: ['./requerimiento-seguimiento.component.css']
})
export class RequerimientoSeguimientoComponent implements OnInit {

  busquedaFiltro! : FormGroup;

  parametros!:any[];


  loading!:boolean;
  ELEMENT_DATA! : RequerimientoSeguimiento[];
  displayedColumns: string[] = ['NroRequerimiento','FechaRegistro', 'FechaAsignacion', 'FechaGestion', 'FechaAtencion','Estado','TipoRequerimiento','UsuarioAsignado','DiasTranscurrido','accion'];
  //requerimientoSeguimiento!:any[];
  requerimientoSeguimiento = new MatTableDataSource<RequerimientoSeguimiento>(this.ELEMENT_DATA);
  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;

  fechaRegistroInicioValor! : Date;
  fechaRegistroFinValor! : Date;
  fechaAtencionInicioValor!: Date;
  fechaAtencionFinValor!: Date;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private parametroService: ParametroService,
    private seguimientoService:SeguimientoService,
    private excelJsService:ExcelJsService
  ) { }

  ngOnInit(): void {
    this.initFormGroup();
    this.loadPage();
    this.buscarSeguimientos();

    this.requerimientoSeguimiento.paginator = this.paginator;
  }

  loadPage(): void {

   // this.peticionesTerminadas = false;
   // let params = this.busquedaFiltro.value;
   // console.log(params);
    this.loading = true;
    forkJoin([
      this.parametroService.ObtenerParametro()
    ]).subscribe( (data:any) => {

        console.log(data);

        this.parametros = data[0].response;


      this.loading = false;

    }, (error) =>{
      console.log(error);
      this.loading = false;
    });

  }
  initFormGroup():void {

    let fechaInicioMes = new Date(new Date().getFullYear(), new Date().getMonth(), 1);
    let fechaHoy = new Date();

    this.fechaRegistroInicioValor = fechaInicioMes;
    this.fechaRegistroFinValor = fechaHoy;
    this.fechaAtencionInicioValor = fechaHoy;
    this.fechaAtencionFinValor = fechaHoy;

    this.busquedaFiltro = new FormGroup({

      FRegistroInicial: new FormControl(null, [Validators.max(this.fechaRegistroFinValor.getTime())]),
      FRegistroFin: new FormControl(null, [Validators.min(this.fechaRegistroInicioValor.getTime())]),
      FAsignacionInicio: new FormControl(null, [Validators.max(this.fechaAtencionFinValor.getTime())]),
      FAsignacionFin: new FormControl(null, [Validators.min(this.fechaAtencionInicioValor.getTime())]),

      FGestionInicio: new FormControl(null, [Validators.max(this.fechaRegistroFinValor.getTime())]),
      FGestionFin: new FormControl(null, [Validators.min(this.fechaRegistroInicioValor.getTime())]),
      FAtencionInicio: new FormControl(null, [Validators.max(this.fechaAtencionFinValor.getTime())]),
      FAtencionFin: new FormControl(null, [Validators.min(this.fechaAtencionInicioValor.getTime())]),
      TipoRequerimiento : new FormControl(0) ,
      Estado : new FormControl(0)
    //  FRegistroInicial: [null],
    // FRegistroFin:[null],
    //  FAsignacionInicio: [null],
    //  FAsignacionInicio:[null],
    //  FGestionInicio: [null],
    //  FGestionFin:[null],
    //  FAtencionInicio: [null],
    //  FAtencionFin:[null],
    //  TipoRequerimiento:[0],
     // Estado:[0]
    });
  }
  buscarSeguimientos():void{
    var request = {

      FechaRegistroInicial : this.busquedaFiltro.controls["FRegistroInicial"].value,
      FechaRegistroFin : this.busquedaFiltro.controls["FRegistroFin"].value,
      FechaAsignacionInicial: this.busquedaFiltro.controls["FAsignacionInicio"].value,
      FechaAsignacionFin : this.busquedaFiltro.controls["FAsignacionFin"].value,
      FechaGestionInicial : this.busquedaFiltro.controls["FGestionInicio"].value,
      FechaGestionFin : this.busquedaFiltro.controls["FGestionFin"].value,
      FechaAtencionInicial : this.busquedaFiltro.controls["FAtencionInicio"].value,
      FechaAtencionFin: this.busquedaFiltro.controls["FAtencionFin"].value,
      TipoRequerimiento : +this.busquedaFiltro.controls["TipoRequerimiento"].value,
      Estado : +this.busquedaFiltro.controls["Estado"].value,
    };

    console.log("Busqueda",request);
    this.seguimientoService.ObtenerSeguimiento(request).subscribe(result=>{
        console.log(result);
       // this.requerimientoSeguimiento = result.response;
        this.requerimientoSeguimiento.data = result.response as RequerimientoSeguimiento[];

//        this.requerimientoSeguimiento = new MatTableDataSource<any[]>(result.response);

    },(err)=>{
      console.log(err)
    });
  }

  ExportarExcel():void {

    var request = {
      FechaRegistroInicial : this.busquedaFiltro.controls["FRegistroInicial"].value,
      FechaRegistroFin : this.busquedaFiltro.controls["FRegistroFin"].value,
      FechaAsignacionInicial: this.busquedaFiltro.controls["FAsignacionInicio"].value,
      FechaAsignacionFin : this.busquedaFiltro.controls["FAsignacionFin"].value,
      FechaGestionInicial : this.busquedaFiltro.controls["FGestionInicio"].value,
      FechaGestionFin : this.busquedaFiltro.controls["FGestionFin"].value,
      FechaAtencionInicial : this.busquedaFiltro.controls["FAtencionInicio"].value,
      FechaAtencionFin: this.busquedaFiltro.controls["FAtencionFin"].value,
      TipoRequerimiento : +this.busquedaFiltro.controls["TipoRequerimiento"].value,
      Estado : +this.busquedaFiltro.controls["Estado"].value,
    };

    this.seguimientoService.ObtenerSeguimiento(request).subscribe(result=>{
        console.log(result);
        //this.requerimientoSeguimiento = result.response;
        this.requerimientoSeguimiento.data = result.response as RequerimientoSeguimiento[];

        const FechaActual = new Date();
        const AnioActual = FechaActual.getFullYear();
        const MesActual = (FechaActual.getMonth()<=9? '0'+ (FechaActual.getMonth()+1): FechaActual.getMonth() + 1); //FechaActual.getMonth() + 1;
        const DiaActual = (FechaActual.getDate()<=9? '0'+FechaActual.getDate(): FechaActual.getDate());
        const HoraActual = (FechaActual.getHours()<=9? '0'+FechaActual.getHours(): FechaActual.getHours()); //FechaActual.getHours();
        const MinutoActual = (FechaActual.getMinutes()<=9? '0'+FechaActual.getMinutes(): FechaActual.getMinutes()); //FechaActual.getMinutes();
        const SegundosActual = (FechaActual.getSeconds()<=9? '0'+FechaActual.getSeconds(): FechaActual.getSeconds()); //FechaActual.getMinutes(); //FechaActual.getSeconds();

        const Archivo = "ReporteSeguimiento" + "_" + AnioActual + MesActual + DiaActual + "T" + HoraActual + MinutoActual + SegundosActual;
        const Sheet = "Hoja1";
        const header = ["Nro. Requerimiento","Fecha Registro","Fecha Asignación","Fecha Gestión","Fecha Atención","Estado","Tipo Requerimiento","Usuario Asignado","Días Transcurridos"];
        const header_width = [20,20,20,20,20,20,25,25,20];
        let body = [];
        for (let index = 0; index < result.response.length; index++) {
          const datos = result.response[index];
          var nroRequerimiento = (datos.nroRequerimiento==null?'': datos.nroRequerimiento);
          var fechaRegistro = (datos.fechaRegistro==null?'': moment(datos.fechaRegistro).format("DD/MM/YYYY"));
          var fechaAsignacion = (datos.fechaAsignacion==null?'': moment(datos.fechaAsignacion).format("DD/MM/YYYY")); // moment(datos.fechaAsignacion).format("DD/MM/YYYY");
          var fechaGestion = (datos.fechaGestion==null?'': moment(datos.fechaGestion).format("DD/MM/YYYY")); // moment(datos.fechaGestion).format("DD/MM/YYYY");
          var fechaAtencion = (datos.fechaAtencion==null?'': moment(datos.fechaAtencion).format("DD/MM/YYYY")); // moment(datos.fechaAtencion).format("DD/MM/YYYY");
          var estado = datos.estado;
          var tipoRequerimiento = datos.tipoRequerimiento;
          var usuarioAsignado = datos.usuarioAsignado;
          var diasTranscurrido = datos.diasTranscurrido;
          var datoarray = [nroRequerimiento,fechaRegistro,fechaAsignacion,fechaGestion,fechaAtencion,estado,tipoRequerimiento,usuarioAsignado,diasTranscurrido]
          body.push(datoarray)
        }


        this.excelJsService.DescargarExcelGenerico(Archivo,Sheet,header,body,header_width);
    },(err)=>{
      console.log(err)
    });

    //this.excelJsService.DescargarExcelGenerico(Archivo,Sheet,header,this.requerimientoSeguimiento);
  }

  revisar(element:any):void{
    this.router.navigate(['/seguimiento/seguimiento-detalle', element.nroRequerimiento]);
  }
}
