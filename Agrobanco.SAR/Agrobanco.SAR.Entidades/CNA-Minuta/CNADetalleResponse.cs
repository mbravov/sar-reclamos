﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agrobanco.SAR.Entidades.CNA_Minuta
{
    public class CNADetalleResponse
    {
        public int IdEmision { get; set; }
        public int IdCreditoCliente { get; set; }
        public int IdDerivacion { get; set; }
        public int IdDerivacionMovimiento { get; set; }
        public int? IdLaserficheCNA { get; set; }
        public int? IdLaserficheCR { get; set; }
        public int? IdLaserficheCE { get; set; }
        public DateTime? FechaRegistroCE { get; set; }
        public string TipoProcesamiento { get; set; }
        public string Estado { get; set; }
        public string TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
        public string Nombres { get; set; }
        public int CodAgencia { get; set; }
        public string Agencia { get; set; }
        public string AnalistaProponente { get; set; }
        public List<CreditoClienteCNABE> Creditos { get; set; }
        public List<GarantiaClienteBE> Garantias { get; set; }
        public List<ComentarioEmisionBE> Comentarios { get; set; } 
        public List<RecursoEmisionBE> RecursoEmision { get; set; }
        public string CartaNoAdeudo { get; set; }
        public int IdEstado { get; set; }
        public string Perfil { get; set; }
        public string PerfilDerivado { get; set; }
        public string CorreoElectronico { get; set; }
        public DateTime? FechaEntregaOlva { get; set; }
        public int CodAgenciaDerivacion { get; set; }
        public string Direccion { get; set; }
        public string DesMedioNot { get; set; }
        public int CodMedioNot { get; set; }
        public string ValorMedioNot { get; set; }
        public string MotivoRechazo { get; set; }
        public string NroCredito { get; set; }
        public string FechaRechazo { get; set; }
    }
}
