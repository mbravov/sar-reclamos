import { Component, OnInit } from '@angular/core';
import { RequerimientoService } from 'src/app/services/requerimiento.service';
import { ParametroService } from 'src/app/services/parametro.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { forkJoin } from 'rxjs';

import { DialogService } from 'src/app/services/comun/dialog.service';
import { SecurityService } from 'src/app/services/security.service';
import { Router } from '@angular/router';
import { TOUCH_BUFFER_MS } from '@angular/cdk/a11y';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { CnaMinutaOcmService } from 'src/app/services/cna-minuta-ocm.service';

@Component({
  selector: 'app-usuario-nuevo',
  templateUrl: './usuario-nuevo.component.html',
  styleUrls: ['./usuario-nuevo.component.css']
})
export class UsuarioNuevoComponent implements OnInit {

  form!: FormGroup;
  parametros:any[] = [];
  agencias:any[] = [];
  UsuarioDetalle:any;
  codUsuario: any;
  accion :string ="";
  loading:boolean = false;

  MaxLengthNombres: number = 70

  formSubmitted:boolean = false;
  bRegistroUsuario : boolean =false;
  disabled:boolean = false;

  constructor(

    private requerimientoServices : RequerimientoService,
    private parametroService: ParametroService,
    private formBuilder: FormBuilder,
    private dialogService: DialogService,
    private securityService:SecurityService,
    private cnaMinutaOcmService : CnaMinutaOcmService,
    private router:Router
  ) { }

  ngOnInit(): void {
    this.initForm();
    this.loadPage();
    if(this.codUsuario != null || this.codUsuario != undefined){
      this.accion = "actualizar";
    }else{
      this.accion = "registrar";
    }
    this.form.controls['CodAgencia'].disable();
  }

  loadPage(): void {
    this.loading = true;
    var oRecursoBE = {
       "CodTabla" : 5,
       "ReferenciaID" : 1,
		   "LaserficheID" : null
    }
    var oBE = {};
    forkJoin([
      this.parametroService.ObtenerParametro(),
      this.cnaMinutaOcmService.ObtenerAgencias(oBE)
    ]).subscribe( (data: any) => {
      debugger;
        console.log(data);
        this.parametros = data[0].response;
        this.agencias = data[1].response;
        this.loading = false;
    }, (err) => {
      this.loading = false;
      console.error(err);
    });

  }


  initForm() : void {
    this.form = this.formBuilder.group({

      Perfil: [null, Validators.required],
      DescripcionPerfil: [{value: null, disabled: true}],
      CorreoElectronico: [null, [Validators.required, Validators.email]],
      NombresApellidos: [null, Validators.required],
      UsuarioWeb: [null, Validators.required],
      Estado: ["Activo"],
      UsuarioRegistro: [null],
      CodAgencia : [null, Validators.required]
    });


  }



  obtenerUsuarioDetalle(): void {
    const codUsuario = {
      codUsuario: this.codUsuario,
    };
    this.requerimientoServices
      .ObtenerRequerimientoDetalle(codUsuario)
      .subscribe((result) => {
        if(result.response){

          this.UsuarioDetalle = result.response;
          console.log("this.UsuarioDetalle",this.UsuarioDetalle)

        }

      });
  }

  onChangePerfil():void {

    debugger;
    const CodPerfil = this.form.controls['Perfil'].value;

    const Grupo = this.parametros.filter(x=>x.grupo==1400);
    if(CodPerfil ==  "1")
    {
      this.form.controls['DescripcionPerfil'].setValue(Grupo[0].valor);
    }
    if(CodPerfil ==  "2")
    {
      this.form.controls['DescripcionPerfil'].setValue(Grupo[1].valor);
    }
    if(CodPerfil ==  "3")
    {
      this.form.controls['DescripcionPerfil'].setValue(Grupo[2].valor);
    }
    if(CodPerfil ==  "4")
    {
      this.form.controls['DescripcionPerfil'].setValue(Grupo[3].valor);
    }
    if(CodPerfil ==  "5")
    {
      this.form.controls['DescripcionPerfil'].setValue(Grupo[4].valor);
    }
    if(CodPerfil ==  "6")
    {
      this.form.controls['DescripcionPerfil'].setValue(Grupo[5].valor);
    }

    if(CodPerfil == "1" || CodPerfil == "2"|| CodPerfil == "3"|| CodPerfil == "4")
    {
      this.form.controls['CodAgencia'].setValue("");
      this.form.controls['CodAgencia'].disable();
    }

    if(CodPerfil == "5" || CodPerfil == "6")
    {
      this.form.controls['CodAgencia'].setValue("");
      this.form.controls['CodAgencia'].enable();
    }

  }

  registrar():void{

    debugger;
    this.formSubmitted = true;

    if (this.form.invalid) {
      return this.form.markAllAsTouched();
    }
    const Perfil = this.form.controls['Perfil'].value;
    const DescripcionPerfil = this.form.controls['DescripcionPerfil'].value;
    const CorreoElectronico = this.form.controls['CorreoElectronico'].value;
    const NombresApellidos = this.form.controls['NombresApellidos'].value;
    const UsuarioWeb = this.form.controls['UsuarioWeb'].value;
    const Estado = this.form.controls['Estado'].value;
    const ValorRegistro = 1;
    const CodAgencia = this.form.controls['CodAgencia'].value;

    this.dialogService.openConfirmDialog('¿Desea ' + this.accion + ' el usuario?')
    .afterClosed().subscribe(res => {
      if (res) {
        this.loading = true;
        const Entity =   {
          Perfil : this.form.controls['Perfil'].value,
          DescripcionPerfil : this.form.controls['DescripcionPerfil'].value,
          CorreoElectronico : this.form.controls['CorreoElectronico'].value,
          NombresApellidos : this.form.controls['NombresApellidos'].value,
          UsuarioWeb : this.form.controls['UsuarioWeb'].value,
          Estado : this.form.controls['Estado'].value,
          UsuarioRegistro : this.securityService.leerUsuarioWeb(),
          CodAgencia : this.form.controls['CodAgencia'].value

        }

        console.log("request",Entity);
        this.requerimientoServices.RegistrarUsuario(Entity).subscribe(result=>{
          debugger;
          console.log("response",result);

          this.loading = false;
          if(result.response){

            if(result.response == 'Se registró correctamente el usuario.'){
              this.form.disable();
              this.bRegistroUsuario = true;
              this.disabled = true;
              this.dialogService.openMsgSuccessDialogWithActionRedirect(result.response, '/usuario/');


            }
            else{
              this.dialogService.openMsgErrorDialog(result.response);

            }

          }
          else{
            this.dialogService.openMsgErrorDialog("Hubo un error al intentar registrar el usuario");
          }


        }, (err)=>{
          this.loading = false;
          console.log(err);
        });

      }

    });

  }
}
