﻿using Agrobanco.SAR.API.Models;
using Agrobanco.SAR.Comun.Laserfiche;
using Agrobanco.SAR.Control.Filters;
using Agrobanco.SAR.Entidades;
using Agrobanco.SAR.LogicaNegocio;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.SAR.API.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/comentario")]
    [ApiController]
    public class ComentarioController : ControllerBase
    {
        private readonly ComentarioLogica comentarioLogica;
        public ComentarioController()
        {
            comentarioLogica = new ComentarioLogica();

        }

        [HttpPost("comentarioregistrar")]
        //[HttpPost("parametro")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult ComentarioRegistrar([Required][FromHeader(Name = "Token")] string token, [FromBody] ComentarioBE oRequerimientoBE)
        {
            try
            {
                ResponseSuccess oResponseSuccess = new ResponseSuccess();
                var Resultado = comentarioLogica.ComentarioRegistrar(oRequerimientoBE);
                return StatusCode(200, Resultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost("comentarioobtener")]
        //[HttpPost("parametro")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult ComentarioObtener([Required][FromHeader(Name = "Token")] string token, [FromBody] ComentarioBE oRequerimientoBE)
        {
            try
            {
                ResponseSuccess oResponseSuccess = new ResponseSuccess();
                var Resultado = comentarioLogica.ComentarioObtener(oRequerimientoBE);
                return StatusCode(200, Resultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost("comentarioregistraranular")]
        //[HttpPost("parametro")]
        [ProducesResponseType(200, Type = typeof(ResponseSuccess))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public IActionResult ComentarioRegistrarAnular([Required][FromHeader(Name = "Token")] string token, [FromBody] ComentarioBE oRequerimientoBE)
        {
            try
            {
                ResponseSuccess oResponseSuccess = new ResponseSuccess();
                var Resultado = comentarioLogica.ComentarioRegistrarAnular(oRequerimientoBE);
                return StatusCode(200, Resultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
    }
}
