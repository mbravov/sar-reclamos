
import { Component, OnInit, Inject } from '@angular/core';
import { FormControl,FormBuilder, FormGroup, Validators } from '@angular/forms';
import { forkJoin } from 'rxjs';
import { Router, ActivatedRoute } from "@angular/router";
import { ParametroService } from 'src/app/services/parametro.service';
import { DialogService } from 'src/app/services/comun/dialog.service';
import { SecurityService } from 'src/app/services/security.service';
import { ComentarioEmisionService } from 'src/app/services/comentario-emision.service';
import { MatDialogRef,MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CnaComentarioComponent } from '../cna-comentario/cna-comentario.component';
import { ConfiguracionAccionService } from 'src/app/services/configuracion-accion.service';
import { CnaMinutaOcmService } from 'src/app/services/cna-minuta-ocm.service';

@Component({
  selector: 'app-cna-rechazar',
  templateUrl: './cna-rechazar.component.html',
  styleUrls: ['./cna-rechazar.component.css']
})

export class CnaRechazarComponent implements OnInit {

  loading:boolean = false;
  form! : FormGroup;
  parametros!: any[];
  formSubmitted:boolean = false;
  lengthDescripcion: number  = 300;
  CreditoClienteDetalle: any;

  constructor(
    private router: Router,
    private dialogService: DialogService,
    private formBuilder: FormBuilder,    
    private parametroService : ParametroService,
    private _dialogService: DialogService,
    private securityService:SecurityService,
    private _comentarioEmisionService : ComentarioEmisionService,
    private _configuracionAccion : ConfiguracionAccionService,
    public dialogRef: MatDialogRef<CnaComentarioComponent>,
    private _cnaMinutaOCMService : CnaMinutaOcmService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    ) 
    {
      this.CreditoClienteDetalle = data;
    console.log('data123: '+this.CreditoClienteDetalle.idEmision);
    console.log('data123 IdConfiguracionAccion: '+this.CreditoClienteDetalle.IdConfiguracionAccion);
    
    }

  ngOnInit(): void {
    this.initForm();
    this.loadPage();
  }

  initForm() : void {
    this.form = this.formBuilder.group({
      CodMotivo : [null, Validators.required],
      Descripcion: [null, Validators.required] 
    });
  }

  loadPage(): void {
    this.loading = true;
   
    forkJoin([
      this.parametroService.ObtenerParametroByGrupo("1900")
      
    ]).subscribe( (data: any) => {
        this.parametros = data[0].response;
        this.loading = false;
    }, (err) => {
      this.loading = false;
      console.error(err);
    });

  }
   

  registrar():void {
       debugger;
    this.formSubmitted = true;

    if (this.form.invalid) {
      return;
    }

    var reques = {
      "IdConfiguracionAccion": this.CreditoClienteDetalle.IdConfiguracionAccion,
      "NroDocumento": this.CreditoClienteDetalle.numeroDocumento,
      "TipoDocumento": this.CreditoClienteDetalle.tipoDocumento,
      "Nombres": this.CreditoClienteDetalle.nombres,          

      "emision": {"IdEmision": this.CreditoClienteDetalle.idEmision,
                  "IdCreditoCliente":this.CreditoClienteDetalle.idCreditoCliente,
                  "UsuarioModificacion": this.securityService.leerUsuarioWeb()
                 },
      "derivacion":{"IdEmision": this.CreditoClienteDetalle.idEmision,
                    "IdDerivacion":this.CreditoClienteDetalle.idDerivacion,                        
                    "UsuarioModificacion": this.securityService.leerUsuarioWeb(),
                    "Perfil": this.securityService.leerPerfil(),
                    "CodAgencia":this.CreditoClienteDetalle.codAgenciaDerivacion
                   },
      "comentarioEmision":{ "IdEmision" : this.CreditoClienteDetalle.idEmision,
                    "IdDerivacion" : this.CreditoClienteDetalle.idDerivacion,
                    "IdDerivacionMovimiento" :  this.CreditoClienteDetalle.idDerivacionMovimiento,
                    "Texto" :  this.form.controls['Descripcion'].value,
                    "UsuarioRegistro" :  this.securityService.leerUsuarioWeb(),       
                    "DNI": this.CreditoClienteDetalle.numeroDocumento,
                    "Perfil": this.securityService.leerPerfil(),
                    "CodAgencia": this.CreditoClienteDetalle.codAgencia,
                    "Estado": this.CreditoClienteDetalle.idEstado,
                    "IdMotivo" :  this.form.controls['CodMotivo'].value
      }
    };

    this._dialogService
    .openConfirmDialog('¿Está seguro de rechazar la emisión de la carta de no adeudo?')
    .afterClosed()
    .subscribe((res) => {
    
      if (res) {
        this.loading = true;
        this._configuracionAccion.accion(reques).subscribe((data:any)=>{
          
          this.loading = false;
          if(data.response.success.resultado){
            this._dialogService.openMsgSuccessDialog("Se registró correctamente el rechazo");
            this.dialogRef.close({ data: true });
          }
          else{

            this._dialogService.openMsgSuccessDialog("Hubo un problema al registrar el rechazo");
            this.dialogRef.close({ data: true });
          }
        },
          (err) => {
            this.loading = false;
            console.error(err);
        });
    }


    });
   }

   reload():void {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this.router.navigate([currentUrl]);
        console.log(currentUrl);
    });
  }
}
