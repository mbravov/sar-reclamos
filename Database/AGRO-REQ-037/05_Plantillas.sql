  USE DBAgrSAR
  GO
  --
  INSERT INTO Plantilla (CodTipoPlantilla, Estado, UsuarioRegistro, FechaRegistro )
  VALUES (1, 1, USER, GETDATE());

  INSERT INTO Recurso (CodTabla, ReferenciaID, LaserficheID, Descripcion, Estado, UsuarioRegistro, FechaRegistro)
  VALUES (5, @@IDENTITY, 33880, 'Resoluci�n 424-2019-MINAGRI �ltimo', 1, USER, GETDATE());
  --
  INSERT INTO Plantilla (CodTipoPlantilla, Estado, UsuarioRegistro, FechaRegistro )
  VALUES (1, 1, USER, GETDATE());

  INSERT INTO Recurso (CodTabla, ReferenciaID, LaserficheID, Descripcion, Estado, UsuarioRegistro, FechaRegistro)
  VALUES (5, @@IDENTITY, 33879, 'Resoluci�n 059-2020-MINAGRI �ltimo', 1, USER, GETDATE());
  --
  INSERT INTO Plantilla (CodTipoPlantilla, Estado, UsuarioRegistro, FechaRegistro )
  VALUES (1, 1, USER, GETDATE());

  INSERT INTO Recurso (CodTabla, ReferenciaID, LaserficheID, Descripcion, Estado, UsuarioRegistro, FechaRegistro)
  VALUES (5, @@IDENTITY, 33878, 'No procede levantamiento de hipoteca', 1, USER, GETDATE());
  --
  INSERT INTO Plantilla (CodTipoPlantilla, Estado, UsuarioRegistro, FechaRegistro )
  VALUES (1, 1, USER, GETDATE());

  INSERT INTO Recurso (CodTabla, ReferenciaID, LaserficheID, Descripcion, Estado, UsuarioRegistro, FechaRegistro)
  VALUES (5, @@IDENTITY, 33877, 'En blanco', 1, USER, GETDATE());
  --
  INSERT INTO Plantilla (CodTipoPlantilla, Estado, UsuarioRegistro, FechaRegistro )
  VALUES (1, 1, USER, GETDATE());

  INSERT INTO Recurso (CodTabla, ReferenciaID, LaserficheID, Descripcion, Estado, UsuarioRegistro, FechaRegistro)
  VALUES (5, @@IDENTITY, 33876, 'Campa�a de Refinanciaci�n de Condiciones Especiales', 1, USER, GETDATE());
  --
  INSERT INTO Plantilla (CodTipoPlantilla, Estado, UsuarioRegistro, FechaRegistro )
  VALUES (1, 1, USER, GETDATE());

  INSERT INTO Recurso (CodTabla, ReferenciaID, LaserficheID, Descripcion, Estado, UsuarioRegistro, FechaRegistro)
  VALUES (5, @@IDENTITY, 33875, 'Beneficiario de la Ley 30893 �ltimo', 1, USER, GETDATE());